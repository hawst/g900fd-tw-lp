.class public Lcom/google/android/apps/books/model/BreakIteratorManager;
.super Ljava/lang/Object;
.source "BreakIteratorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;,
        Lcom/google/android/apps/books/model/BreakIteratorManager$BasicBreakIteratorWrapper;
    }
.end annotation


# static fields
.field private static final cachedSentenceBreakIterator:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/google/android/apps/books/model/BreakIteratorManager$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/BreakIteratorManager$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/BreakIteratorManager;->cachedSentenceBreakIterator:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static getSentenceIterator(Ljava/util/Locale;)Ljava/text/BreakIterator;
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 25
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    .line 27
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/BreakIteratorManager;->cachedSentenceBreakIterator:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;

    .line 28
    .local v0, "wrapper":Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;
    iget-object v1, v0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;->locale:Ljava/util/Locale;

    invoke-virtual {v1, p0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    iget-object v1, v0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;->breakIterator:Ljava/text/BreakIterator;

    .line 33
    :goto_0
    return-object v1

    .line 31
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;

    .end local v0    # "wrapper":Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;
    invoke-direct {v0, p0}, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;-><init>(Ljava/util/Locale;)V

    .line 32
    .restart local v0    # "wrapper":Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;
    sget-object v1, Lcom/google/android/apps/books/model/BreakIteratorManager;->cachedSentenceBreakIterator:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 33
    iget-object v1, v0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;->breakIterator:Ljava/text/BreakIterator;

    goto :goto_0
.end method
