.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageContentFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

.field final synthetic val$forImage:Z

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;ZLjava/lang/String;Lcom/google/android/apps/books/model/Page;)V
    .locals 0

    .prologue
    .line 1275
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    iput-boolean p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$forImage:Z

    iput-object p3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$volumeId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSaved(Lcom/google/android/apps/books/model/EncryptedContent;)V
    .locals 6
    .param p1, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;

    .prologue
    const/4 v0, 0x0

    .line 1278
    iget-boolean v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$forImage:Z

    if-eqz v1, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 1279
    .local v3, "imageSaved":Ljava/lang/Boolean;
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$forImage:Z

    if-eqz v1, :cond_1

    move-object v4, v0

    .line 1280
    .local v4, "structureSaved":Ljava/lang/Boolean;
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/EncryptedContent;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v5

    # invokes: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalPageState(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/books/model/SessionKeyId;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$100(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/books/model/SessionKeyId;)V

    .line 1282
    return-void

    .end local v3    # "imageSaved":Ljava/lang/Boolean;
    .end local v4    # "structureSaved":Ljava/lang/Boolean;
    :cond_0
    move-object v3, v0

    .line 1278
    goto :goto_0

    .line 1279
    .restart local v3    # "imageSaved":Ljava/lang/Boolean;
    :cond_1
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1
.end method
