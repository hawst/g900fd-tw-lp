.class public Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
.super Ljava/lang/Object;
.source "SyncPlan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/SyncPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VolumeSyncData"
.end annotation


# instance fields
.field public final downloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

.field public final localData:Lcom/google/android/apps/books/model/LocalVolumeData;

.field public final volumeData:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;)V
    .locals 1
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "localData"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p3, "downloadProgress"    # Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeData;

    iput-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->volumeData:Lcom/google/android/apps/books/model/VolumeData;

    .line 32
    invoke-static {p2}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    iput-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->localData:Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 33
    invoke-static {p3}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    iput-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->downloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 34
    return-void
.end method


# virtual methods
.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->volumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
