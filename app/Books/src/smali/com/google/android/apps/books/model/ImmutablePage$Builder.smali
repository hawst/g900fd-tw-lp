.class public Lcom/google/android/apps/books/model/ImmutablePage$Builder;
.super Ljava/lang/Object;
.source "ImmutablePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutablePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mIsViewable:Z

.field private mPageId:Ljava/lang/String;

.field private mPageOrder:I

.field private mRemoteUrl:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageId:Ljava/lang/String;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageOrder:I

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mIsViewable:Z

    .line 13
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mTitle:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mRemoteUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/Page;
    .locals 7

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/books/model/ImmutablePage;

    iget-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageOrder:I

    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mIsViewable:Z

    iget-object v4, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mRemoteUrl:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/model/ImmutablePage;-><init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutablePage$1;)V

    return-object v0
.end method

.method public isViewable(Z)Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 0
    .param p1, "isViewable"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mIsViewable:Z

    .line 28
    return-object p0
.end method

.method public pageId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 0
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageId:Ljava/lang/String;

    .line 18
    return-object p0
.end method

.method public pageOrder(I)Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageOrder:I

    .line 23
    return-object p0
.end method

.method public remoteUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mRemoteUrl:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageId:Ljava/lang/String;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mPageOrder:I

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mIsViewable:Z

    .line 51
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mTitle:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mRemoteUrl:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->mTitle:Ljava/lang/String;

    .line 33
    return-object p0
.end method
