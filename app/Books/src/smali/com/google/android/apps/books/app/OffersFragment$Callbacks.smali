.class public interface abstract Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract onOfferAccepted()V
.end method

.method public abstract onOfferDismissed(Ljava/lang/String;)V
.end method

.method public abstract onUserAcknowledgesFailure()V
.end method
