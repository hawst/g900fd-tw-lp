.class public Lcom/google/android/apps/books/util/Holder;
.super Ljava/lang/Object;
.source "Holder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 11
    .local p0, "this":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/google/android/apps/books/util/Holder;->mValue:Ljava/lang/Object;

    .line 13
    return-void
.end method

.method public static make(Ljava/lang/Object;)Lcom/google/android/apps/books/util/Holder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(TU;)",
            "Lcom/google/android/apps/books/util/Holder",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "init":Ljava/lang/Object;, "TU;"
    new-instance v0, Lcom/google/android/apps/books/util/Holder;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/Holder;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/Holder;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/Holder;->mValue:Ljava/lang/Object;

    .line 17
    return-void
.end method
