.class abstract Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;
.super Ljava/lang/Object;
.source "JavaScriptExecutors.java"

# interfaces
.implements Lcom/google/android/apps/books/render/JavaScriptExecutor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/JavaScriptExecutors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AbstractExecutor"
.end annotation


# instance fields
.field private mDestroyed:Z

.field private final mWebView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Landroid/webkit/WebView;)V
    .locals 1
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->mDestroyed:Z

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->mWebView:Landroid/webkit/WebView;

    .line 23
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->mDestroyed:Z

    .line 41
    return-void
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->mDestroyed:Z

    return v0
.end method

.method public run(Ljava/lang/String;)V
    .locals 2
    .param p1, "script"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 30
    :cond_0
    const-string v0, "JSExecutors"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    const-string v0, "JSExecutors"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;->runScript(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract runScript(Landroid/webkit/WebView;Ljava/lang/String;)V
.end method
