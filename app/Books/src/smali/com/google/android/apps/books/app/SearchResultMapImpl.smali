.class public Lcom/google/android/apps/books/app/SearchResultMapImpl;
.super Ljava/lang/Object;
.source "SearchResultMapImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SearchResultMap;


# instance fields
.field private final mLocationToSearchMatch:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            "Lcom/google/android/apps/books/model/SearchMatchTextRange;",
            ">;"
        }
    .end annotation
.end field

.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mSearchHighlightColor:I

.field private mSearchResultCounter:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;I)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "searchHighlightColor"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 33
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/TextLocation;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v0

    .line 34
    .local v0, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    invoke-static {v0}, Lcom/google/common/collect/Maps;->newTreeMap(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    .line 35
    iput p2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchHighlightColor:I

    .line 36
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchResultCounter:I

    .line 38
    return-void
.end method


# virtual methods
.method public addSearchResult(Lcom/google/android/apps/books/model/SearchResult;)V
    .locals 6
    .param p1, "searchResult"    # Lcom/google/android/apps/books/model/SearchResult;

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getMatchRanges()Ljava/util/List;

    move-result-object v2

    .line 43
    .local v2, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 44
    .local v3, "textLocationRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    new-instance v1, Lcom/google/android/apps/books/model/SearchMatchTextRange;

    iget v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchResultCounter:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchResultCounter:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchHighlightColor:I

    invoke-direct {v1, v4, v3, v5}, Lcom/google/android/apps/books/model/SearchMatchTextRange;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;I)V

    .line 46
    .local v1, "match":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    iget-object v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 48
    .end local v1    # "match":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    .end local v3    # "textLocationRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mSearchResultCounter:I

    .line 177
    return-void
.end method

.method public findNextLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 3
    .param p1, "location"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v1

    .line 106
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 108
    .local v0, "resultEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    goto :goto_0
.end method

.method public findNextPassageWithSearchResults(I)I
    .locals 4
    .param p1, "passageIndex"    # I

    .prologue
    .line 83
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v2, :cond_0

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v2

    .line 98
    :goto_0
    return v2

    .line 87
    :cond_0
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 89
    .local v0, "firstLocationForNextPassage":Lcom/google/android/apps/books/annotations/TextLocation;
    if-nez v0, :cond_1

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v2

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->findNextLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    .line 94
    .local v1, "nextLocationWithResult":Lcom/google/android/apps/books/annotations/TextLocation;
    if-nez v1, :cond_2

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v2

    goto :goto_0

    .line 98
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v2

    goto :goto_0
.end method

.method public findPreviousLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 3
    .param p1, "location"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    if-nez v2, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-object v1

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 121
    .local v0, "resultEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    if-eqz v0, :cond_0

    .line 122
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    goto :goto_0
.end method

.method public findPreviousPassageWithSearchResults(I)I
    .locals 4
    .param p1, "passageIndex"    # I

    .prologue
    const/4 v2, -0x1

    .line 62
    iget-object v3, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v3, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v2

    .line 65
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 67
    .local v0, "firstLocationForThisPassage":Lcom/google/android/apps/books/annotations/TextLocation;
    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->findPreviousLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    .line 74
    .local v1, "previousLocationWithResult":Lcom/google/android/apps/books/annotations/TextLocation;
    if-eqz v1, :cond_0

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v2

    goto :goto_0
.end method

.method public getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 4
    .param p1, "passageIndex"    # I

    .prologue
    .line 52
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v1

    .line 53
    .local v1, "firstSegment":Lcom/google/android/apps/books/model/Segment;
    if-nez v1, :cond_0

    .line 54
    const/4 v2, 0x0

    .line 57
    :goto_0
    return-object v2

    .line 56
    :cond_0
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "firstPosition":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocation;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getNumMatches()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    if-nez v0, :cond_0

    .line 168
    const/4 v0, -0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getNumMatchesBeforeTextLocation(Lcom/google/android/apps/books/annotations/TextLocation;)I
    .locals 1
    .param p1, "location"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    if-nez v0, :cond_0

    .line 160
    const/4 v0, -0x1

    .line 162
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getTextLocationToSearchMatch(I)Ljava/util/SortedMap;
    .locals 7
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            "Lcom/google/android/apps/books/model/SearchMatchTextRange;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 131
    iget-object v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    if-nez v4, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-object v2

    .line 134
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 136
    .local v0, "firstLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    if-eqz v0, :cond_0

    .line 140
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/SearchResultMapImpl;->getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    .line 142
    .local v1, "firstLocationOfNext":Lcom/google/android/apps/books/annotations/TextLocation;
    iget-object v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getTextLocationComparator()Ljava/util/Comparator;

    move-result-object v3

    .line 144
    .local v3, "textLocationComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    if-eqz v1, :cond_3

    .line 145
    invoke-interface {v3, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_2

    .line 146
    new-instance v4, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Passage start positions out of order: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 150
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v4, v0, v1}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    .local v2, "result":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    goto :goto_0

    .line 152
    .end local v2    # "result":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    .restart local v2    # "result":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 181
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "LocationToSearchMatch"

    iget-object v3, p0, Lcom/google/android/apps/books/app/SearchResultMapImpl;->mLocationToSearchMatch:Ljava/util/TreeMap;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 183
    .local v0, "helper":Lcom/google/common/base/Objects$ToStringHelper;
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
