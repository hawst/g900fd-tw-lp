.class public abstract Lcom/google/android/apps/books/util/LeakSafeCallback;
.super Ljava/lang/Object;
.source "LeakSafeCallback.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;"
    }
.end annotation


# instance fields
.field private final mRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/google/android/apps/books/util/LeakSafeCallback;, "Lcom/google/android/apps/books/util/LeakSafeCallback<TT;>;"
    .local p1, "target":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/LeakSafeCallback;->mRef:Ljava/lang/ref/WeakReference;

    .line 20
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/util/LeakSafeCallback;, "Lcom/google/android/apps/books/util/LeakSafeCallback<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/LeakSafeCallback;->mRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 25
    .local v0, "target":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_0

    .line 26
    const/4 v1, 0x0

    .line 28
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/books/util/LeakSafeCallback;->handleMessage(Ljava/lang/Object;Landroid/os/Message;)Z

    move-result v1

    goto :goto_0
.end method

.method protected abstract handleMessage(Ljava/lang/Object;Landroid/os/Message;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/os/Message;",
            ")Z"
        }
    .end annotation
.end method
