.class final Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UpdateOfflineLicenseCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3168
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 3168
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public handleResultUi(III)V
    .locals 4
    .param p1, "outcome"    # I
    .param p2, "maxDevices"    # I
    .param p3, "devicesInUse"    # I

    .prologue
    const/4 v1, 0x1

    .line 3171
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/actionbar/UBLibActivity;

    .line 3172
    .local v0, "activity":Lcom/google/android/ublib/actionbar/UBLibActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3173
    :cond_0
    const-string v1, "ReaderFragment"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3174
    const-string v1, "ReaderFragment"

    const-string v2, "Could not update offline license state, activity gone/destroyed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3182
    :cond_1
    :goto_0
    return-void

    .line 3178
    :cond_2
    const/4 v2, -0x1

    if-eq p1, v2, :cond_1

    .line 3179
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    if-ne p1, v1, :cond_3

    :goto_1
    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/books/data/BooksDataController;->setHasOfflineLicense(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
