.class public Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
.super Ljava/lang/Object;
.source "ServerAnnotationReceipt.java"


# instance fields
.field public final characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

.field public final serverId:Ljava/lang/String;

.field public final serverTimestamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "serverId"    # Ljava/lang/String;
    .param p2, "serverTimestamp"    # J

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;-><init>(Ljava/lang/String;JLcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 0
    .param p1, "serverId"    # Ljava/lang/String;
    .param p2, "serverTimestamp"    # J
    .param p4, "characterQuota"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    .line 24
    iput-wide p2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    .line 25
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .line 26
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "serverId"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "serverTimestamp"

    iget-wide v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "characterQuota"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
