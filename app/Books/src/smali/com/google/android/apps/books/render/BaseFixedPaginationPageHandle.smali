.class abstract Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;
.super Lcom/google/android/apps/books/render/BasePageHandle;
.source "BaseFixedPaginationPageHandle.java"

# interfaces
.implements Lcom/google/android/apps/books/render/FixedPaginationPageHandle;


# instance fields
.field private final mBookPageIndex:I

.field private final mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;I)V
    .locals 0
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p3, "bookPageIndex"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/BasePageHandle;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    .line 23
    iput p3, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mBookPageIndex:I

    .line 24
    return-void
.end method


# virtual methods
.method protected createPosition()Lcom/google/android/apps/books/common/Position;
    .locals 3

    .prologue
    .line 48
    new-instance v1, Lcom/google/android/apps/books/common/Position;

    iget-object v0, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mBookPageIndex:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method protected createSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 6

    .prologue
    .line 54
    iget-object v2, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->getBookPageIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v0

    .line 55
    .local v0, "screenPageIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getPagesPerSpread()I

    move-result v2

    rem-int v1, v0, v2

    .line 56
    .local v1, "spreadPageIndex":I
    new-instance v2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    new-instance v3, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/books/render/SpreadPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V

    return-object v2
.end method

.method public getBookPageIndex()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mBookPageIndex:I

    return v0
.end method

.method public getFirstBookPageIndex()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mBookPageIndex:I

    return v0
.end method

.method public getFirstChapterIndex()I
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->getFirstBookPage()Lcom/google/android/apps/books/model/Page;

    move-result-object v0

    .line 29
    .local v0, "bookPage":Lcom/google/android/apps/books/model/Page;
    if-nez v0, :cond_0

    .line 30
    const/4 v1, -0x1

    .line 32
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPageId(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method
