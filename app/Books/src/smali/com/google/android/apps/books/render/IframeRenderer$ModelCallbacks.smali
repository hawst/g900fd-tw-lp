.class Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;
.super Ljava/lang/Object;
.source "IframeRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/IframeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModelCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/IframeRenderer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/IframeRenderer;)V
    .locals 0

    .prologue
    .line 766
    iput-object p1, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/render/IframeRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/IframeRenderer$1;

    .prologue
    .line 766
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;)V

    return-void
.end method


# virtual methods
.method public canDrawPages()Z
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/IframeRenderer;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/IframeRenderer;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 793
    return-void
.end method

.method public dispatchRenderError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/IframeRenderer;->dispatchRenderError(Ljava/lang/Exception;)V

    .line 787
    return-void
.end method

.method public drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "pageBounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    # invokes: Lcom/google/android/apps/books/render/IframeRenderer;->drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/IframeRenderer;->access$500(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V

    .line 777
    return-void
.end method

.method public getPageSize(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 769
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    # getter for: Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/apps/books/render/IframeRenderer;->access$300(Lcom/google/android/apps/books/render/IframeRenderer;)[Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v2

    aget-object v0, v1, v2

    .line 770
    .local v0, "targetRectF":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    invoke-virtual {p2, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 771
    return-object p2
.end method
