.class public Lcom/google/android/apps/books/render/StubRendererListener;
.super Ljava/lang/Object;
.source "StubRendererListener.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RendererListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 42
    return-void
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 31
    return-void
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 37
    return-void
.end method

.method public onNewSelectionBegins()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public onPagesNeedRedraw()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 24
    return-void
.end method

.method public onPassagesPurged(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    return-void
.end method

.method public onRenderError(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 18
    return-void
.end method

.method public onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V
    .locals 0
    .param p3, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "handleRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    .local p2, "textRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    return-void
.end method

.method public onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 0
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 64
    return-void
.end method
