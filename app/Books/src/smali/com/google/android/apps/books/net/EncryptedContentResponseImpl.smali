.class public Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;
.super Lcom/google/android/apps/books/net/BaseEncryptedContentResponse;
.source "EncryptedContentResponseImpl.java"


# instance fields
.field private final mInput:Ljava/io/InputStream;

.field private final mSessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/books/net/BaseEncryptedContentResponse;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mInput:Ljava/io/InputStream;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mSessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/net/BaseEncryptedContentResponse;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mInput:Ljava/io/InputStream;

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mSessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 22
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 45
    return-void
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mInput:Ljava/io/InputStream;

    return-object v0
.end method

.method public getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->mSessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    return-object v0
.end method
