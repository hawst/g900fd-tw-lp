.class public Lcom/google/android/apps/books/annotations/Annotation;
.super Ljava/lang/Object;
.source "Annotation.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationSortingKey;
.implements Lcom/google/android/apps/books/model/PaintableTextRange;


# static fields
.field public static final BOOKMARK_LAYER_ID:Ljava/lang/String;

.field public static final COPY_LAYER_ID:Ljava/lang/String;

.field public static final GEO_LAYER_ID:Ljava/lang/String;

.field public static final NOTES_LAYER_ID:Ljava/lang/String;


# instance fields
.field private final mAnnotationType:Ljava/lang/String;

.field private final mColor:I

.field private final mDataId:Ljava/lang/String;

.field private mLastUsedTimestamp:J

.field private final mLayerId:Ljava/lang/String;

.field private final mLocalId:Ljava/lang/String;

.field private final mNote:Ljava/lang/String;

.field private final mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

.field private final mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

.field private final mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->BOOKMARKS:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    .line 31
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/Annotation;->GEO_LAYER_ID:Ljava/lang/String;

    .line 32
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->NOTES:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->COPY:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)V
    .locals 2
    .param p1, "localId"    # Ljava/lang/String;
    .param p2, "layerId"    # Ljava/lang/String;
    .param p3, "annotationType"    # Ljava/lang/String;
    .param p4, "dataId"    # Ljava/lang/String;
    .param p5, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .param p7, "color"    # I
    .param p8, "marginNoteText"    # Ljava/lang/String;
    .param p9, "pageStructureLocationRange"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .param p10, "lastUsedTimestamp"    # J

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    .line 158
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    .line 159
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    .line 160
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    .line 161
    iput-object p5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 162
    iput p7, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    .line 163
    if-eqz p6, :cond_0

    .end local p6    # "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :goto_0
    iput-object p6, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .line 165
    iput-object p8, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    .line 166
    iput-object p9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .line 167
    iput-wide p10, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    .line 168
    return-void

    .line 163
    .restart local p6    # "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :cond_0
    new-instance p6, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .end local p6    # "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    invoke-direct {p6, v1, v1, v1}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static bookmarkWithFreshId(Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p0, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p1, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    const/4 v4, 0x0

    .line 109
    new-instance v0, Lcom/google/android/apps/books/annotations/Annotation;

    invoke-static {}, Lcom/google/android/apps/books/annotations/Annotation;->freshLocalId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    const-string v3, "bookmark"

    const/4 v7, 0x0

    const-string v8, ""

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object v5, p0

    move-object v6, p1

    move-object v9, v4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)V

    return-object v0
.end method

.method public static comparator(Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;)",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationSortingKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    .local p0, "locationComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    new-instance v0, Lcom/google/android/apps/books/annotations/Annotation$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/annotations/Annotation$1;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static freshLocalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAllVolumeLayerIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/annotations/Annotation;->GEO_LAYER_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 13
    .param p0, "localId"    # Ljava/lang/String;
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "dataId"    # Ljava/lang/String;
    .param p4, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p5, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .param p6, "color"    # I
    .param p7, "marginNoteText"    # Ljava/lang/String;
    .param p8, "pageStructureRange"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .param p9, "lastUsedTimestamp"    # J

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/apps/books/annotations/Annotation;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-wide/from16 v10, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)V

    return-object v0
.end method

.method public static withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p0, "layer"    # Ljava/lang/String;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "dataId"    # Ljava/lang/String;
    .param p3, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p4, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .param p5, "color"    # I
    .param p6, "marginNoteText"    # Ljava/lang/String;
    .param p7, "pageStructureLocationRange"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .param p8, "lastUsedTimestamp"    # J

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/apps/books/annotations/Annotation;

    invoke-static {}, Lcom/google/android/apps/books/annotations/Annotation;->freshLocalId()Ljava/lang/String;

    move-result-object v1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 386
    if-ne p0, p1, :cond_1

    .line 430
    :cond_0
    :goto_0
    return v1

    .line 389
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 390
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 393
    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 395
    .local v0, "that":Lcom/google/android/apps/books/annotations/Annotation;
    iget v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    iget v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 396
    goto :goto_0

    .line 398
    :cond_4
    iget-wide v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    iget-wide v6, v0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 399
    goto :goto_0

    .line 401
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    :cond_6
    move v1, v2

    .line 403
    goto :goto_0

    .line 401
    :cond_7
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 405
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    :cond_9
    move v1, v2

    .line 406
    goto :goto_0

    .line 405
    :cond_a
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 408
    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 409
    goto :goto_0

    .line 411
    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 412
    goto :goto_0

    .line 414
    :cond_d
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    :cond_e
    move v1, v2

    .line 415
    goto :goto_0

    .line 414
    :cond_f
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    if-nez v3, :cond_e

    .line 417
    :cond_10
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    :cond_11
    move v1, v2

    .line 419
    goto/16 :goto_0

    .line 417
    :cond_12
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    if-nez v3, :cond_11

    .line 421
    :cond_13
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/annotations/TextLocationRange;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    :cond_14
    move v1, v2

    .line 423
    goto/16 :goto_0

    .line 421
    :cond_15
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-nez v3, :cond_14

    .line 425
    :cond_16
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 427
    goto/16 :goto_0

    .line 425
    :cond_17
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getAfterSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    return-object v0
.end method

.method public getAnnotationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    return-object v0
.end method

.method public getBeforeSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    return-object v0
.end method

.method public getBestPositionForToc()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getStartLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 351
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getPageId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    return v0
.end method

.method public getDataId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    return-object v0
.end method

.method public getEndLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    return-object v0
.end method

.method public getLastUsedTimestamp()J
    .locals 2

    .prologue
    .line 231
    iget-wide v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    return-wide v0
.end method

.method public getLayerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    return-object v0
.end method

.method public getNormalizedSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->getNormalizedSelectedText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    return-object v0
.end method

.method public getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    return-object v0
.end method

.method public getPaintableRangeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    return-object v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getAfterSelectedText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getAfterSelectedText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortingLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 339
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->pageAsTextLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public getStartLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    return-object v0
.end method

.method public getTextualContext()Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    return-object v0
.end method

.method public hasNote()Z
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 436
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 437
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    .line 438
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 439
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 440
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v3, v1

    .line 441
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->hashCode()I

    move-result v1

    :goto_3
    add-int v0, v3, v1

    .line 442
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    add-int v0, v1, v3

    .line 443
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_4
    add-int v0, v3, v1

    .line 444
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 446
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 447
    return v0

    :cond_1
    move v1, v2

    .line 438
    goto :goto_0

    :cond_2
    move v1, v2

    .line 439
    goto :goto_1

    :cond_3
    move v1, v2

    .line 440
    goto :goto_2

    :cond_4
    move v1, v2

    .line 441
    goto :goto_3

    :cond_5
    move v1, v2

    .line 443
    goto :goto_4
.end method

.method public isUserChange(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 249
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "localId"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "color"

    iget v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateFromServer(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 16
    .param p1, "context"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .param p2, "positionRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "imageRange"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .param p4, "lastUsedTimestamp"    # J

    .prologue
    .line 303
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->isEmpty(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 305
    .local v2, "badServerContext":Z
    :goto_0
    if-eqz p2, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    const/4 v15, 0x1

    .line 307
    .local v15, "badServerPositionRange":Z
    :goto_1
    if-eqz p3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_2
    const/4 v14, 0x1

    .line 310
    .local v14, "badServerImageRange":Z
    :goto_2
    if-eqz v2, :cond_6

    if-eqz v14, :cond_6

    if-eqz v15, :cond_6

    .line 311
    const/4 v3, 0x0

    .line 320
    :goto_3
    return-object v3

    .line 303
    .end local v2    # "badServerContext":Z
    .end local v14    # "badServerImageRange":Z
    .end local v15    # "badServerPositionRange":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 305
    .restart local v2    # "badServerContext":Z
    :cond_4
    const/4 v15, 0x0

    goto :goto_1

    .line 307
    .restart local v15    # "badServerPositionRange":Z
    :cond_5
    const/4 v14, 0x0

    goto :goto_2

    .line 313
    .restart local v14    # "badServerImageRange":Z
    :cond_6
    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .line 315
    .local v8, "newContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :goto_4
    if-eqz v15, :cond_8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 317
    .local v7, "newPositionRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :goto_5
    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .line 319
    .local v11, "newImageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v4

    move-wide/from16 v0, p4

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    .line 320
    .local v12, "greaterTimestamp":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v3

    goto :goto_3

    .end local v7    # "newPositionRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    .end local v8    # "newContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .end local v11    # "newImageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .end local v12    # "greaterTimestamp":J
    :cond_7
    move-object/from16 v8, p1

    .line 313
    goto :goto_4

    .restart local v8    # "newContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :cond_8
    move-object/from16 v7, p2

    .line 315
    goto :goto_5

    .restart local v7    # "newPositionRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :cond_9
    move-object/from16 v11, p3

    .line 317
    goto :goto_6
.end method

.method public withFreshId()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1

    .prologue
    .line 328
    invoke-static {}, Lcom/google/android/apps/books/annotations/Annotation;->freshLocalId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/annotations/Annotation;->withUpdatedId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public withNewColor(I)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p1, "newColor"    # I

    .prologue
    .line 267
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-wide v10, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    move v7, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public withNewContext(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p1, "context"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 286
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget v7, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-wide v10, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    move-object v6, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public withNewNote(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p1, "newNote"    # Ljava/lang/String;

    .prologue
    .line 277
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget v7, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-wide v10, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    move-object v8, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public withUpdatedId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 12
    .param p1, "editedLocalId"    # Ljava/lang/String;

    .prologue
    .line 272
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget v7, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-wide v10, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLastUsedTimestamp:J

    move-object v1, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public withUpdatedLastUsedTimestamp(J)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 13
    .param p1, "timestamp"    # J

    .prologue
    .line 291
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLocalId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Annotation;->mLayerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Annotation;->mAnnotationType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Annotation;->mDataId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/Annotation;->mTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget v7, p0, Lcom/google/android/apps/books/annotations/Annotation;->mColor:I

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/Annotation;->mNote:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/Annotation;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-wide v10, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method
