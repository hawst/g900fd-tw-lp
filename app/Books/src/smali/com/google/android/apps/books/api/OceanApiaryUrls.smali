.class public Lcom/google/android/apps/books/api/OceanApiaryUrls;
.super Ljava/lang/Object;
.source "OceanApiaryUrls.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;,
        Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;
    }
.end annotation


# direct methods
.method private static addParametersToOffersUrl(Lcom/google/api/client/http/GenericUrl;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "url"    # Lcom/google/api/client/http/GenericUrl;
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .param p2, "offerId"    # Ljava/lang/String;
    .param p3, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 621
    const-string v0, "offerId"

    invoke-virtual {p0, v0, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    const-string v0, "volumeId"

    invoke-virtual {p0, v0, p3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    const-string v0, "androidId"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getAndroidId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    const-string v0, "serial"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    const-string v0, "model"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    const-string v0, "manufacturer"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    const-string v0, "device"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628
    const-string v0, "product"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/DeviceInfo;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    const-string v0, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    return-void
.end method

.method public static forAcceptOffer(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;
    .locals 6
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "offerId"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;

    .prologue
    .line 651
    const-string v2, "accept"

    const-string v5, "forAcceptOffer"

    move-object v0, p0

    move-object v1, p3

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOfferAction(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method public static forAddAnnotation(Lcom/google/android/apps/books/util/Config;Z)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "showOnlySummaryInResponse"    # Z

    .prologue
    .line 348
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 349
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 350
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    const-string v2, "annotations"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    if-eqz p1, :cond_0

    .line 353
    const-string v2, "showOnlySummaryInResponse"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_0
    const-string v2, "forAddAnnotation"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    return-object v1
.end method

.method public static forAddVolumeToMyEBooksShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)Lcom/google/api/client/http/GenericUrl;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;

    .prologue
    .line 135
    const/4 v0, 0x7

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forAddVolumeToShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method public static forAddVolumeToShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "shelf"    # I
    .param p3, "reason"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;

    .prologue
    .line 140
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 141
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 142
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v2, "bookshelves"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    const-string v2, "addVolume"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    const-string v2, "volumeId"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    if-eqz p3, :cond_0

    .line 148
    const-string v2, "reason"

    invoke-virtual {p3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :cond_0
    const-string v2, "forAddVolumeToShelf"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-object v1
.end method

.method public static forCloudloadingAdd(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 588
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 589
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 590
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "cloudloading"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591
    const-string v2, "addBook"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 592
    const-string v2, "upload_client_token"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    const-string v2, "forCloudloadingAddBook"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    return-object v1
.end method

.method public static forCloudloadingDelete(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 610
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 611
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 612
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "cloudloading"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 613
    const-string v2, "deleteBook"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 614
    const-string v2, "volumeId"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    const-string v2, "forCloudloadingDeleteBook"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    return-object v1
.end method

.method public static forCloudloadingGetFailed(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 599
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 600
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 601
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 602
    const-string v2, "useruploaded"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    const-string v2, "processingState"

    const-string v3, "COMPLETED_FAILED"

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    const-string v2, "forCloudloadingGetFailed"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    return-object v1
.end method

.method public static forDictionaryMetadata(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "keyVersion"    # Ljava/lang/String;

    .prologue
    .line 661
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 662
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 663
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "dictionary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 664
    const-string v2, "listOfflineMetadata"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 665
    const-string v2, "cpksver"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    const-string v2, "forDictionaryMetadata"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    return-object v1
.end method

.method public static forDismissOffer(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;
    .locals 6
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "offerId"    # Ljava/lang/String;
    .param p2, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;

    .prologue
    .line 656
    const-string v2, "dismiss"

    const/4 v4, 0x0

    const-string v5, "forDismissOffer"

    move-object v0, p0

    move-object v1, p2

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOfferAction(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method public static forDismissRecommendation(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "rating"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .prologue
    .line 452
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 453
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 454
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 455
    const-string v2, "recommended"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    const-string v2, "rate"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 457
    const-string v2, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    const-string v2, "rating"

    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    const-string v2, "volumeId"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    const-string v2, "forDismissRecommendation"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    return-object v1
.end method

.method public static forEditOrDeleteAnnotation(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "serverAnnotationId"    # Ljava/lang/String;

    .prologue
    .line 366
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 367
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 368
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    const-string v2, "annotations"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v2, "forEditOrDeleteAnnotation"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    return-object v1
.end method

.method public static forFetchAnnotationsSummary(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/Config;",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/client/http/GenericUrl;"
        }
    .end annotation

    .prologue
    .line 426
    .local p2, "layerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 427
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 428
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    const-string v2, "annotations"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    const-string v2, "summary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    const-string v2, "layerIds"

    const-string v3, ","

    invoke-static {v3}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    const-string v2, "volumeId"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    const-string v2, "forAnnotationsSummary"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    return-object v1
.end method

.method public static forFetchOffers(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;
    .locals 6
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;

    .prologue
    const/4 v3, 0x0

    .line 645
    const-string v2, "get"

    const-string v5, "forFetchOffers"

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOfferAction(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method public static forFetchVolume(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 119
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 120
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v2, "forFetchVolume"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-object v1
.end method

.method public static forFetchVolumeAnnotationData(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "key"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .prologue
    .line 564
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 565
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 566
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 567
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 568
    const-string v2, "layers"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    const-string v2, "dict"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    const-string v2, "data"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    iget-object v2, p2, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 574
    const-string v2, "contentVersion"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    const-string v2, "allowWebDefinitions"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    const-string v2, "forFetchVolumeAnnotationData"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    return-object v1
.end method

.method public static forFetchVolumeAnnotationDatas(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;II)Lcom/google/api/client/http/GenericUrl;
    .locals 8
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p3, "imageW"    # I
    .param p4, "imageH"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/Config;",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            ">;II)",
            "Lcom/google/api/client/http/GenericUrl;"
        }
    .end annotation

    .prologue
    .line 527
    .local p2, "keys":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/AnnotationData$Key;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v5

    .line 528
    .local v5, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v5}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v4

    .line 529
    .local v4, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, "volumes"

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 530
    iget-object v6, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    const-string v6, "layers"

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 532
    const-string v6, "geo"

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    const-string v6, "data"

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    const-string v6, "contentVersion"

    iget-object v7, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 536
    .local v1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 537
    .local v2, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    iget-object v6, v2, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 539
    .end local v2    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :cond_0
    const-string v6, "annotationDataId"

    invoke-virtual {v5, v6, v1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    const-string v6, "w"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    const-string v6, "h"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 545
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->getUserLocaleString()Ljava/lang/String;

    move-result-object v3

    .line 546
    .local v3, "localeString":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 547
    const-string v6, "locale"

    invoke-virtual {v5, v6, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    .end local v3    # "localeString":Ljava/lang/String;
    :cond_1
    const-string v6, "forFetchVolumeAnnotationDatas"

    invoke-virtual {v5}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    return-object v5
.end method

.method public static forFetchVolumeAnnotationSummary(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 483
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 484
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 485
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    const-string v2, "layersummary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    const-string v2, "contentVersion"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    const-string v2, "forFetchVolumeAnnotationSummary"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    return-object v1
.end method

.method public static forFetchVolumeAnnotations(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 5
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layerId"    # Ljava/lang/String;
    .param p3, "layerEtag"    # Ljava/lang/String;
    .param p4, "startPosition"    # Ljava/lang/String;
    .param p5, "endPosition"    # Ljava/lang/String;
    .param p6, "maxResults"    # I
    .param p7, "continuation"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 498
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 499
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 500
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    const-string v2, "layers"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 505
    const-string v2, "contentVersion"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    const-string v2, "startPosition"

    invoke-virtual {v1, v2, p4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    const-string v2, "startOffset"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    if-eqz p5, :cond_0

    .line 509
    const-string v2, "endPosition"

    invoke-virtual {v1, v2, p5}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    const-string v2, "endOffset"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    :cond_0
    if-eqz p7, :cond_1

    .line 513
    const-string v2, "pageToken"

    invoke-virtual {v1, v2, p7}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    :cond_1
    const-string v2, "maxResults"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    const-string v2, "forFetchVolumeAnnotations"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    return-object v1
.end method

.method public static forGetFrontPageRecommendations(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 439
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 440
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 441
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    const-string v2, "recommended"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443
    const-string v2, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    const-string v2, "forGetFrontPageRecommendations"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    return-object v1
.end method

.method public static forGetNewAnnotationOperations(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;JILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 7
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layerId"    # Ljava/lang/String;
    .param p3, "lastDate"    # J
    .param p5, "maxResults"    # I
    .param p6, "continuation"    # Ljava/lang/String;

    .prologue
    .line 396
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 397
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 398
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "mylibrary"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    const-string v3, "annotations"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    const-string v3, "volumeId"

    iget-object v4, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    const-string v3, "contentVersion"

    iget-object v4, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    const-string v3, "layerId"

    invoke-virtual {v1, v3, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    if-eqz p6, :cond_0

    .line 404
    const-string v3, "pageToken"

    invoke-virtual {v1, v3, p6}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v3, p3, v4

    if-eqz v3, :cond_1

    .line 407
    new-instance v2, Landroid/text/format/Time;

    const-string v3, "UTC"

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 408
    .local v2, "time":Landroid/text/format/Time;
    invoke-virtual {v2, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 409
    const-string v3, "updatedMin"

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    const-string v3, "showDeleted"

    const-string v4, "true"

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v3, "maxResults"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    const-string v3, "forGetNewAnnotationOperations"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    return-object v1
.end method

.method public static forGetRelatedBooks(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "association"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 467
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 468
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "volumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    const-string v2, "associated"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    const-string v2, "association"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    const-string v2, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    const-string v2, "forGetRelatedBooks"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    return-object v1
.end method

.method public static forGetUserSettings(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 720
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 721
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 722
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "myconfig"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 723
    const-string v2, "getUserSettings"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 724
    const-string v2, "forGetUserSettings"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    return-object v1
.end method

.method public static forMyEBooksAndSyncLicenses(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "cpksver"    # Ljava/lang/String;
    .param p2, "nonce"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 101
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 102
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "myconfig"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v2, "syncVolumeLicenses"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v2, "cpksver"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v2, "nonce"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v2, "features"

    const-string v3, "RENTALS"

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v2, "forMyEBooksAndSyncLicenses"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-object v1
.end method

.method private static forOfferAction(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "offerId"    # Ljava/lang/String;
    .param p4, "volumeId"    # Ljava/lang/String;
    .param p5, "logLabel"    # Ljava/lang/String;

    .prologue
    .line 634
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 635
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 636
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "promooffer"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638
    invoke-static {v1, p1, p3, p4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->addParametersToOffersUrl(Lcom/google/api/client/http/GenericUrl;Lcom/google/android/apps/books/app/DeviceInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p5, v2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    return-object v1
.end method

.method public static forOnboarding(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 674
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 675
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 676
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "onboarding"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    return-object v1
.end method

.method public static forOnboardingCategories(Lcom/google/android/apps/books/util/Config;Ljava/util/Locale;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 684
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOnboarding(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 685
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 686
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "listCategories"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 687
    const-string v2, "locale"

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    const-string v2, "forOnboardingCategories"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    return-object v1
.end method

.method public static forOnboardingVolumes(Lcom/google/android/apps/books/util/Config;Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p3, "pageSize"    # I
    .param p4, "pageToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/Config;",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/client/http/GenericUrl;"
        }
    .end annotation

    .prologue
    .line 700
    .local p2, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOnboarding(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 701
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 702
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "listCategoryVolumes"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 703
    const-string v2, "locale"

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    const-string v2, "categoryId"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    const-string v2, "pageSize"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    if-eqz p4, :cond_0

    .line 707
    const-string v2, "pageToken"

    invoke-virtual {v1, v2, p4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    :cond_0
    const-string v2, "forOnboardingVolumes"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    return-object v1
.end method

.method public static forReleaseDownloadAccess(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cpksver"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 226
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 227
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "myconfig"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v2, "releaseDownloadAccess"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    const-string v2, "cpksver"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    const-string v2, "volumeIds"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    const-string v2, "forReleaseDownloadAccess"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-object v1
.end method

.method public static forRemovingVolumeFromMyEBooksShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 161
    const/4 v0, 0x7

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRemovingVolumeFromShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;I)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method public static forRemovingVolumeFromShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;I)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "shelf"    # I

    .prologue
    .line 172
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 173
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 174
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v2, "bookshelves"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    const-string v2, "removeVolume"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v2, "volumeId"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const-string v2, "forRemovingVolumeFromShelf"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-object v1
.end method

.method public static forRequestAccess(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cpksver"    # Ljava/lang/String;
    .param p3, "nonce"    # Ljava/lang/String;
    .param p4, "licenseTypes"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 202
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 203
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "myconfig"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v2, "requestAccess"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v2, "cpksver"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string v2, "nonce"

    invoke-virtual {v1, v2, p3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    const-string v2, "volumeId"

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    if-eqz p4, :cond_0

    .line 209
    const-string v2, "licenseTypes"

    invoke-virtual {v1, v2, p4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :cond_0
    const-string v2, "forRequestAccess"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-object v1
.end method

.method public static forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 2
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 83
    new-instance v0, Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseApiaryUri()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static forSetReadingPosition(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "timestamp"    # Ljava/lang/String;
    .param p4, "action"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .prologue
    .line 327
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 328
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 329
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "mylibrary"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v2, "readingpositions"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    const-string v2, "setPosition"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    const-string v2, "timestamp"

    invoke-virtual {v1, v2, p3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    const-string v2, "position"

    invoke-virtual {v1, v2, p2}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    const-string v2, "action"

    invoke-static {p4}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->actionOrDefault(Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->getApiarySetPositionActionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    const-string v2, "deviceCookie"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getLoggingId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    const-string v2, "forSetReadingPosition"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    return-object v1
.end method

.method public static forUpdateUserSettings(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 729
    invoke-static {p0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRoot(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 730
    .local v1, "result":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v0

    .line 731
    .local v0, "pathParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "myconfig"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    const-string v2, "updateUserSettings"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 733
    const-string v2, "forUpdateUserSettings"

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    return-object v1
.end method

.method private static maybeLogResultUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "forWhat"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 581
    const-string v0, "ApiaryUrlsCreated"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    const-string v0, "ApiaryUrlsCreated"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    :cond_0
    return-void
.end method
