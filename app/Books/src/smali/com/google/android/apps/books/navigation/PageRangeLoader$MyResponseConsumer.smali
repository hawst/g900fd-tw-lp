.class Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;
.super Ljava/lang/Object;
.source "PageRangeLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/PageRangeLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyResponseConsumer"
.end annotation


# instance fields
.field final mPos:I

.field final mSeq:I

.field final synthetic this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;I)V
    .locals 1
    .param p2, "pos"    # I

    .prologue
    .line 569
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderSeq:I
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$600(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mSeq:I

    .line 570
    iput p2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    .line 571
    return-void
.end method

.method private isObsolete()Z
    .locals 2

    .prologue
    .line 574
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mSeq:I

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderSeq:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$600(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isPurgeable()Z
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$100(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$300(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$400(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->isObsolete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->delete(I)V

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    .line 641
    const/4 v0, 0x1

    .line 643
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 6
    .param p1, "info"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 580
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->isObsolete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    :goto_0
    return-void

    .line 584
    :cond_0
    const-string v0, "PageRangeLoader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    const-string v0, "PageRangeLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$100(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 589
    invoke-interface {p2}, Lcom/google/android/apps/books/render/PagePainter;->recycle()V

    goto :goto_0

    .line 593
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    const/4 v4, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->addPage(ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$700(Lcom/google/android/apps/books/navigation/PageRangeLoader;ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    goto :goto_0
.end method

.method public onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 4
    .param p1, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    const/4 v3, 0x1

    .line 598
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->isObsolete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 602
    :cond_1
    const-string v0, "PageRangeLoader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 603
    const-string v0, "PageRangeLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got special page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    sget-object v1, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    if-eq v0, v1, :cond_3

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mGapPages:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$800(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    goto :goto_0

    .line 612
    :cond_3
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    if-nez v0, :cond_4

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1000(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1000(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->reinit(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1100(Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    goto :goto_0

    .line 622
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->delete(I)V

    .line 623
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1300(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # setter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1402(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z

    .line 626
    :cond_5
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->mPos:I

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1500(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    if-le v0, v1, :cond_6

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # setter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1602(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z

    .line 629
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1700(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1700(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;->spreadRangeChanged()V

    goto/16 :goto_0
.end method
