.class Lcom/google/android/apps/books/app/OffersFragment$6;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OffersFragment;->acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;

.field final synthetic val$onResult:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 4
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 232
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->val$volumeId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 233
    .local v0, "acceptedVolumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # getter for: Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;
    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment;->access$700(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$6;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/apps/books/data/BooksDataController;->acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    .line 234
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 229
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$6;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
