.class Lcom/google/android/apps/books/app/ReaderFragment$26;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->setupFullGestureDetector(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field final synthetic val$gestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

.field final synthetic val$scaleGestureDetector:Landroid/view/ScaleGestureDetector;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/support/v4/view/GestureDetectorCompat;Landroid/view/ScaleGestureDetector;Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)V
    .locals 0

    .prologue
    .line 4777
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$scaleGestureDetector:Landroid/view/ScaleGestureDetector;

    iput-object p4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$gestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 4780
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 4781
    .local v0, "action":I
    if-nez v0, :cond_1

    .line 4782
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    int-to-float v2, v4

    .line 4783
    .local v2, "rightBound":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v4, v4, v2

    if-lez v4, :cond_1

    .line 4785
    const/4 v3, 0x0

    .line 4831
    .end local v2    # "rightBound":F
    :cond_0
    :goto_0
    return v3

    .line 4791
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$5500(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 4792
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$5500(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 4798
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v5

    invoke-virtual {v4, p2, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/books/app/SelectionState;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 4803
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v4, p2}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 4804
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mZoomEnabled:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4813
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$scaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v4, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4822
    :cond_4
    :goto_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 4824
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$gestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->onActionUp(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 4814
    :catch_0
    move-exception v1

    .line 4815
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "ReaderFragment"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4816
    const-string v4, "ReaderFragment"

    const-string v5, "Exception delegating to zoom detector: "

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 4827
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$26;->val$gestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->onActionCancel(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 4822
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
