.class Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;
.super Ljava/lang/Object;
.source "LocalSearchWithinVolumeLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->loadResults(Lcom/google/android/apps/books/model/SearchResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

.field final synthetic val$chapterSearchResults:Lcom/google/android/apps/books/model/ChapterSearchResults;

.field final synthetic val$doneParsing:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$handler:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;Lcom/google/android/apps/books/model/ChapterSearchResults;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    iput-object p2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$doneParsing:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-object p4, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$handler:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;

    iput-object p5, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$chapterSearchResults:Lcom/google/android/apps/books/model/ChapterSearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 175
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$doneParsing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    # getter for: Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mContentSource:Lcom/google/android/apps/books/render/ReaderDataSource;
    invoke-static {v2}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->access$300(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/render/ReaderDataSource;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/render/ReaderDataSource;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "content":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$handler:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->parse(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    .end local v0    # "content":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->val$chapterSearchResults:Lcom/google/android/apps/books/model/ChapterSearchResults;

    iget-object v2, v2, Lcom/google/android/apps/books/model/ChapterSearchResults;->chapterQueue:Ljava/util/concurrent/BlockingQueue;

    # getter for: Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->DONE:Lcom/google/android/apps/books/model/SearchResult;
    invoke-static {}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->access$400()Lcom/google/android/apps/books/model/SearchResult;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 185
    return-void

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->handleParseException(Ljava/lang/Exception;)V

    goto :goto_0
.end method
