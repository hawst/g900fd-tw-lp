.class public Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;
.super Lcom/google/android/apps/books/widget/ActionItem;
.source "TableOfContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TableOfContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TOCActionItem"
.end annotation


# instance fields
.field private final mPath:Landroid/graphics/Path;

.field private mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 110
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    move-result v1

    .line 112
    .local v1, "saveCount":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2d

    div-int/lit8 v0, v2, 0x64

    .line 116
    .local v0, "radius":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    int-to-float v5, v0

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 119
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/ActionItem;->draw(Landroid/graphics/Canvas;)V

    .line 120
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 124
    .end local v0    # "radius":I
    .end local v1    # "saveCount":I
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/ActionItem;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public needsTableOfContents()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected performClickAction()V
    .locals 5

    .prologue
    .line 86
    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 88
    .local v2, "parent":Landroid/view/ViewParent;
    :goto_0
    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    move-object v3, v2

    .line 89
    check-cast v3, Landroid/view/ViewGroup;

    const v4, 0x7f0e00c2

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "bookGridView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/BookNavView;

    .line 92
    .local v1, "navView":Lcom/google/android/apps/books/navigation/BookNavView;
    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->showTOC()V

    .line 99
    .end local v0    # "bookGridView":Landroid/view/View;
    .end local v1    # "navView":Lcom/google/android/apps/books/navigation/BookNavView;
    .end local v2    # "parent":Landroid/view/ViewParent;
    :goto_1
    return-void

    .line 95
    .restart local v0    # "bookGridView":Landroid/view/View;
    .restart local v2    # "parent":Landroid/view/ViewParent;
    :cond_0
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 96
    goto :goto_0

    .line 98
    .end local v0    # "bookGridView":Landroid/view/View;
    .end local v2    # "parent":Landroid/view/ViewParent;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/books/widget/TableOfContents;->show(Landroid/view/View;)V

    goto :goto_1
.end method

.method public setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V
    .locals 1
    .param p1, "toc"    # Lcom/google/android/apps/books/widget/TableOfContents;

    .prologue
    .line 77
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-nez v0, :cond_0

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    .line 82
    :cond_0
    return-void
.end method
