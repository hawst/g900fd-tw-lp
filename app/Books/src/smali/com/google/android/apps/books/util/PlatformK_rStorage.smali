.class public Lcom/google/android/apps/books/util/PlatformK_rStorage;
.super Ljava/lang/Object;
.source "PlatformK_rStorage.java"

# interfaces
.implements Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;


# static fields
.field private static final A:[B

.field private static final B:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 15
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/util/PlatformK_rStorage;->A:[B

    .line 16
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/books/util/PlatformK_rStorage;->B:[B

    return-void

    .line 15
    nop

    :array_0
    .array-data 1
        -0xat
        0x66t
        -0x8t
        0x24t
        0x66t
        0x6dt
        -0x71t
        0x31t
        0x7bt
        -0x26t
        0x13t
        0xft
        -0x76t
        -0x7et
        0x33t
        -0x1ct
    .end array-data

    .line 16
    :array_1
    .array-data 1
        0x6bt
        -0x62t
        -0x72t
        -0x7at
        0x32t
        0x2ct
        0x21t
        0x30t
        0x72t
        0x2ct
        -0x3bt
        -0x6ct
        -0x73t
        0x6et
        -0x5ft
        -0x80t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getK_r()Ljavax/crypto/SecretKey;
    .locals 7

    .prologue
    const/16 v6, 0x10

    .line 20
    new-array v0, v6, [B

    .line 21
    .local v0, "derivedKey":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 22
    mul-int/lit8 v3, v1, 0x2

    rem-int/lit8 v3, v3, 0x10

    mul-int/lit8 v4, v1, 0x2

    div-int/lit8 v4, v4, 0x10

    add-int v2, v3, v4

    .line 23
    .local v2, "k":I
    sget-object v3, Lcom/google/android/apps/books/util/PlatformK_rStorage;->A:[B

    aget-byte v3, v3, v2

    sget-object v4, Lcom/google/android/apps/books/util/PlatformK_rStorage;->B:[B

    rsub-int/lit8 v5, v1, 0xf

    aget-byte v4, v4, v5

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 21
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25
    .end local v2    # "k":I
    :cond_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v0, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v3
.end method

.method public getK_rVersion()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    return v0
.end method
