.class public Lcom/google/android/apps/books/render/HighlightPaintBox;
.super Ljava/lang/Object;
.source "HighlightPaintBox.java"


# instance fields
.field private final mFallbackColor:I

.field private final mHighlightPaints:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/google/android/apps/books/render/HighlightPaintBox;->findDefaultYellowHighlightColor()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/HighlightPaintBox;-><init>(I)V

    .line 27
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "fallbackColor"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightPaintBox;->mHighlightPaints:Ljava/util/Map;

    .line 31
    iput p1, p0, Lcom/google/android/apps/books/render/HighlightPaintBox;->mFallbackColor:I

    .line 32
    return-void
.end method

.method private createPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;
    .locals 8
    .param p1, "theme"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "isSelected"    # Z
    .param p4, "isIcon"    # Z
    .param p5, "coerceToAnnotationColor"    # Z

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 62
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 64
    .local v7, "paint":Landroid/graphics/Paint;
    if-nez p2, :cond_0

    .line 65
    iget v2, p0, Lcom/google/android/apps/books/render/HighlightPaintBox;->mFallbackColor:I

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/HighlightPaintBox;->createPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;

    move-result-object v7

    .line 96
    .end local v7    # "paint":Landroid/graphics/Paint;
    :goto_0
    return-object v7

    .line 68
    .restart local v7    # "paint":Landroid/graphics/Paint;
    :cond_0
    if-eqz p5, :cond_7

    .line 69
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->DRAWING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;->getClosestMatch(I)I

    move-result p2

    .line 71
    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 72
    if-eqz p4, :cond_2

    if-eqz p3, :cond_1

    .line 78
    .local v6, "fraction":F
    :goto_1
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 79
    invoke-static {p2, v6}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 72
    .end local v6    # "fraction":F
    :cond_1
    const v6, 0x3f333333    # 0.7f

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    const v6, 0x3f4ccccd    # 0.8f

    goto :goto_1

    :cond_3
    const v6, 0x3ecccccd    # 0.4f

    goto :goto_1

    .line 81
    :cond_4
    if-eqz p4, :cond_6

    if-eqz p3, :cond_5

    const v6, 0x3f59999a    # 0.85f

    .line 87
    .restart local v6    # "fraction":F
    :cond_5
    :goto_2
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 88
    invoke-static {p2, v6}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 81
    .end local v6    # "fraction":F
    :cond_6
    if-nez p3, :cond_5

    const/high16 v6, 0x3fc00000    # 1.5f

    goto :goto_2

    .line 93
    :cond_7
    invoke-virtual {v7, p2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private static findDefaultYellowHighlightColor()I
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getDefaultDrawingColor()I

    move-result v0

    return v0
.end method


# virtual methods
.method public getHighlightPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;
    .locals 6
    .param p1, "theme"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "isSelected"    # Z
    .param p4, "isIcon"    # Z
    .param p5, "coerceToAnnotationColor"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p3, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p4, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p5, :cond_3

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/render/HighlightPaintBox;->mHighlightPaints:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Paint;

    .line 48
    .local v1, "paint":Landroid/graphics/Paint;
    if-nez v1, :cond_0

    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/render/HighlightPaintBox;->createPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;

    move-result-object v1

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/books/render/HighlightPaintBox;->mHighlightPaints:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    :cond_0
    return-object v1

    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "paint":Landroid/graphics/Paint;
    :cond_1
    move v2, v4

    .line 45
    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2
.end method
