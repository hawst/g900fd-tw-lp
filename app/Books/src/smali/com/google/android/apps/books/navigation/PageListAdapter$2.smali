.class Lcom/google/android/apps/books/navigation/PageListAdapter$2;
.super Ljava/lang/Object;
.source "PageListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/PageListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/PageListAdapter;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 260
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFirstVisiblePosition()I

    move-result v1

    .line 261
    .local v1, "pos":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLastVisiblePosition()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 263
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v0

    .line 264
    .local v0, "child":Landroid/view/View;
    if-ne p1, v0, :cond_2

    .line 265
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mSideClicksScroll:Z
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$300(Lcom/google/android/apps/books/navigation/PageListAdapter;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 266
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingToCenter(I)V

    .line 273
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    return-void

    .line 268
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/navigation/PageListAdapter;->showSpreadInFullView(I)V

    goto :goto_1

    .line 262
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$2;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getNextVisiblePosition(I)I

    move-result v1

    goto :goto_0
.end method
