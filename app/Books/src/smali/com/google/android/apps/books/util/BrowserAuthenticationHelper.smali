.class public abstract Lcom/google/android/apps/books/util/BrowserAuthenticationHelper;
.super Ljava/lang/Object;
.source "BrowserAuthenticationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/google/android/apps/books/util/BrowserAuthenticationHelper;, "Lcom/google/android/apps/books/util/BrowserAuthenticationHelper<TT;>;"
    .local p1, "client":Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "missing client"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    iput-object v0, p0, Lcom/google/android/apps/books/util/BrowserAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    .line 49
    return-void
.end method
