.class public interface abstract Lcom/google/android/apps/books/model/DataControllerStore;
.super Ljava/lang/Object;
.source "DataControllerStore.java"


# virtual methods
.method public abstract clearLastDictionaryMetadataSyncTime()V
.end method

.method public abstract deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V
.end method

.method public abstract getDownloadedDictionaryFile(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Ljava/io/File;
.end method

.method public abstract getLocalDictionaryMetadataList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRequestedDictionaryMetadataList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getServerDictionaryMetadataList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
.end method

.method public abstract removeDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
.end method

.method public abstract removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
.end method

.method public abstract removeUserSessionKey()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract revokeDictionaryLanguage(Ljava/lang/String;)V
.end method

.method public abstract saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setRequestedDictionaryLanguages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateUserSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
