.class Lcom/google/android/apps/books/app/ReaderFragment$23;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SearchResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->startSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mChapterHeading:Ljava/lang/String;

.field private mCollectedResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private mOldestUnreportedResultTime:J

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$thisSearchSequenceNumber:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;I)V
    .locals 2

    .prologue
    .line 4476
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->val$thisSearchSequenceNumber:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4477
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mOldestUnreportedResultTime:J

    .line 4478
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mCollectedResults:Ljava/util/List;

    .line 4479
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mChapterHeading:Ljava/lang/String;

    return-void
.end method

.method private publishResults()V
    .locals 3

    .prologue
    .line 4518
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->val$thisSearchSequenceNumber:I

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mChapterHeading:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment$23;->publishSearchResultBatch(ZILjava/lang/String;)V

    .line 4519
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mChapterHeading:Ljava/lang/String;

    .line 4520
    return-void
.end method

.method private publishSearchResultBatch(ZILjava/lang/String;)V
    .locals 8
    .param p1, "isDone"    # Z
    .param p2, "thisSearchSequenceNumber"    # I
    .param p3, "chapterHeading"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 4524
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mCollectedResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 4555
    :goto_0
    return-void

    .line 4527
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mCollectedResults:Ljava/util/List;

    .line 4528
    .local v3, "newResults":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SearchResult;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mCollectedResults:Ljava/util/List;

    .line 4529
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mOldestUnreportedResultTime:J

    .line 4531
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 4532
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SearchResult;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/SearchResult;->setItemType(I)V

    .line 4541
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    move-object v1, p0

    move v2, p2

    move v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment$23$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$23;ILjava/util/List;ZLjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 4533
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_1

    .line 4534
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SearchResult;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/SearchResult;->setItemType(I)V

    .line 4535
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v6, v0, :cond_3

    .line 4536
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SearchResult;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/SearchResult;->setItemType(I)V

    .line 4535
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 4538
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SearchResult;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/SearchResult;->setItemType(I)V

    goto :goto_1
.end method


# virtual methods
.method public onBookDone()V
    .locals 3

    .prologue
    .line 4507
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->val$thisSearchSequenceNumber:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment$23;->publishSearchResultBatch(ZILjava/lang/String;)V

    .line 4508
    return-void
.end method

.method public onChapterDone()Z
    .locals 2

    .prologue
    .line 4498
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$23;->publishResults()V

    .line 4499
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11200(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->val$thisSearchSequenceNumber:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChapterStart(Ljava/lang/String;)Z
    .locals 2
    .param p1, "chapterHeading"    # Ljava/lang/String;

    .prologue
    .line 4492
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mChapterHeading:Ljava/lang/String;

    .line 4493
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11200(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->val$thisSearchSequenceNumber:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 4512
    const-string v0, "ReaderFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4513
    const-string v0, "ReaderFragment"

    const-string v1, "Exception during search"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4515
    :cond_0
    return-void
.end method

.method public onFound(Lcom/google/android/apps/books/model/SearchResult;)V
    .locals 6
    .param p1, "result"    # Lcom/google/android/apps/books/model/SearchResult;

    .prologue
    .line 4483
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mCollectedResults:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4484
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4485
    .local v0, "now":J
    iget-wide v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mOldestUnreportedResultTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 4486
    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23;->mOldestUnreportedResultTime:J

    .line 4488
    :cond_0
    return-void
.end method
