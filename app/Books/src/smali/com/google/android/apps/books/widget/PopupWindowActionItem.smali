.class public abstract Lcom/google/android/apps/books/widget/PopupWindowActionItem;
.super Lcom/google/android/apps/books/widget/ActionItem;
.source "PopupWindowActionItem.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field protected mPopup:Landroid/widget/PopupWindow;


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 103
    :cond_0
    return-void
.end method

.method protected isShowing()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->dismiss()V

    .line 96
    invoke-super {p0}, Lcom/google/android/apps/books/widget/ActionItem;->onDetachedFromWindow()V

    .line 97
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/ActionItem;->setEnabled(Z)V

    .line 32
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PopupWindowActionItem;->dismiss()V

    .line 33
    :cond_0
    return-void
.end method
