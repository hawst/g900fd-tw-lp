.class Lcom/google/android/apps/books/widget/StaggeredCoverView$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "StaggeredCoverView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 0

    .prologue
    .line 968
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x0

    .line 993
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v10

    .line 994
    .local v10, "scrollY":I
    neg-float v0, p4

    float-to-int v4, v0

    .line 995
    .local v4, "scrollVel":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # invokes: Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollExtent()I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$800(Lcom/google/android/apps/books/widget/StaggeredCoverView;)I

    move-result v9

    .line 996
    .local v9, "scrollExtent":I
    if-lez v10, :cond_0

    if-ltz v4, :cond_1

    :cond_0
    if-ge v10, v9, :cond_3

    if-lez v4, :cond_3

    .line 998
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$400(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 999
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$400(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrollStateChanged(Lcom/google/android/play/headerlist/PlayScrollableContentView;I)V

    .line 1002
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$900(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/widget/Scroller;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getWidth()I

    move-result v6

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    move v3, v1

    move v5, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1006
    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "dx"    # F
    .param p4, "dy"    # F

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 971
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->isInGesture:Z
    invoke-static {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$300(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 972
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$400(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 973
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$400(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-interface {v3, v4, v6}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrollStateChanged(Lcom/google/android/play/headerlist/PlayScrollableContentView;I)V

    .line 976
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollX()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, p4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollTo(II)V

    .line 977
    const/4 v1, 0x0

    .line 978
    .local v1, "pulled":Z
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I
    invoke-static {v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$500(Lcom/google/android/apps/books/widget/StaggeredCoverView;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 979
    .local v0, "overFraction":F
    :goto_0
    cmpl-float v3, v0, v2

    if-lez v3, :cond_4

    .line 980
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$600(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v1

    .line 984
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 985
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 988
    .end local v0    # "overFraction":F
    .end local v1    # "pulled":Z
    :cond_2
    return v6

    .restart local v1    # "pulled":Z
    :cond_3
    move v0, v2

    .line 978
    goto :goto_0

    .line 981
    .restart local v0    # "overFraction":F
    :cond_4
    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    .line 982
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # getter for: Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$700(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v2

    neg-float v3, v0

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v1

    goto :goto_1
.end method
