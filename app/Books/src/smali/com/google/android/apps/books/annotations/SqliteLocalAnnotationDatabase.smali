.class public Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;
.super Ljava/lang/Object;
.source "SqliteLocalAnnotationDatabase.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;,
        Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationTable;
    }
.end annotation


# static fields
.field private static final FULL_RETRIEVAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# instance fields
.field private final mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/16 v1, 0x13

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "local_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "layer_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "volume_id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "content_version"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "start_position"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "start_offset"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "end_position"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "end_offset"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "before_selected_text"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "selected_text"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "after_selected_text"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "data_id"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "color"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "notes"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "server_id"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "image_start_cfi"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "image_end_cfi"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "last_used_timestamp"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->FULL_RETRIEVAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 419
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getDatabaseFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 415
    new-instance v0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    .line 416
    return-void
.end method

.method public constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 0
    .param p1, "helper"    # Landroid/database/sqlite/SQLiteOpenHelper;

    .prologue
    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 425
    return-void
.end method

.method private addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;Z)V
    .locals 5
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layerVersion"    # Ljava/lang/String;
    .param p3, "serverAnnotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .param p4, "segmentIndex"    # Ljava/lang/Integer;
    .param p5, "throwOnConflict"    # Z

    .prologue
    .line 440
    invoke-direct {p0, p1, p3, p2, p4}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->newValuesForAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/ContentValues;

    move-result-object v1

    .line 443
    .local v1, "values":Landroid/content/ContentValues;
    if-eqz p5, :cond_0

    const/4 v0, 0x2

    .line 446
    .local v0, "onConflict":I
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "annotations"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 449
    return-void

    .line 443
    .end local v0    # "onConflict":I
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private annotationAdditionFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
    .locals 3
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 831
    new-instance v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->volumeVersionFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->localAnnotationFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;-><init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)V

    return-object v0
.end method

.method private annotationEditFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;
    .locals 4
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 931
    new-instance v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->volumeVersionFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->localAnnotationFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->serverIdFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;-><init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getDatabaseFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 402
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 403
    .local v1, "filesDir":Ljava/io/File;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 404
    .local v0, "accountDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v3, "books2.db"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2
.end method

.method private varargs getFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "queryParams"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v1

    .line 813
    .local v1, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 814
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 815
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->localAnnotationFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 819
    .end local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v2

    .restart local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v0
.end method

.method private getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private getSegmentVolumeAnnotationsUnlogged(Lcom/google/android/apps/books/annotations/Layer;I)Ljava/util/List;
    .locals 10
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p2, "segmentIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Layer;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 778
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    .line 779
    .local v1, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {p1, p2, v2}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->getAllAnnotationsPresent(Lcom/google/android/apps/books/annotations/Layer;ILandroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 781
    const-string v2, "%s=? AND %s=? AND %s=? AND %s=? AND %s=?"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "volume_id"

    aput-object v4, v3, v5

    const-string v4, "content_version"

    aput-object v4, v3, v6

    const-string v4, "layer_id"

    aput-object v4, v3, v7

    const-string v4, "layer_version"

    aput-object v4, v3, v8

    const-string v4, "segment_index"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 785
    .local v0, "query":Ljava/lang/String;
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    aput-object v3, v2, v5

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getLayerId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 788
    .end local v0    # "query":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private varargs getStringColumn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "queryParams"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 793
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p1, v2, v1

    .line 796
    .local v2, "projection":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "annotations"

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 800
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 801
    .local v9, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 802
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 806
    .end local v9    # "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .restart local v9    # "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method private getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private localAnnotationFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 25
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 836
    const-string v24, "start_position"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 837
    .local v23, "startPos":Ljava/lang/String;
    const-string v24, "start_offset"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 838
    .local v22, "startOffset":I
    const-string v24, "end_position"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 839
    .local v19, "endPos":Ljava/lang/String;
    const-string v24, "end_offset"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 841
    .local v18, "endOffset":I
    if-nez v23, :cond_0

    const/4 v9, 0x0

    .line 844
    .local v9, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :goto_0
    const-string v24, "before_selected_text"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 845
    .local v16, "beforeSelectedText":Ljava/lang/String;
    const-string v24, "selected_text"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 846
    .local v20, "selectedText":Ljava/lang/String;
    const-string v24, "after_selected_text"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 847
    .local v4, "afterSelectedText":Ljava/lang/String;
    new-instance v10, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v1, v4}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    .local v10, "context":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    const-string v24, "type"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 851
    .local v7, "type":Ljava/lang/String;
    const-string v24, "layer_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 852
    .local v6, "layerId":Ljava/lang/String;
    const-string v24, "local_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 854
    .local v5, "localId":Ljava/lang/String;
    const-string v24, "data_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 856
    .local v8, "dataId":Ljava/lang/String;
    const-string v24, "color"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 857
    .local v11, "color":I
    const-string v24, "notes"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 859
    .local v12, "notes":Ljava/lang/String;
    const-string v24, "image_start_cfi"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 860
    .local v21, "startCfi":Ljava/lang/String;
    const-string v24, "image_end_cfi"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 861
    .local v17, "endCfi":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->parseImageRange(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v13

    .line 863
    .local v13, "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    const-string v24, "last_used_timestamp"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 865
    .local v14, "lastUsedTimestamp":J
    invoke-static/range {v5 .. v15}, Lcom/google/android/apps/books/annotations/Annotation;->loaded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v24

    return-object v24

    .line 841
    .end local v4    # "afterSelectedText":Ljava/lang/String;
    .end local v5    # "localId":Ljava/lang/String;
    .end local v6    # "layerId":Ljava/lang/String;
    .end local v7    # "type":Ljava/lang/String;
    .end local v8    # "dataId":Ljava/lang/String;
    .end local v9    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    .end local v10    # "context":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .end local v11    # "color":I
    .end local v12    # "notes":Ljava/lang/String;
    .end local v13    # "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .end local v14    # "lastUsedTimestamp":J
    .end local v16    # "beforeSelectedText":Ljava/lang/String;
    .end local v17    # "endCfi":Ljava/lang/String;
    .end local v20    # "selectedText":Ljava/lang/String;
    .end local v21    # "startCfi":Ljava/lang/String;
    :cond_0
    new-instance v9, Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-object/from16 v0, v23

    move/from16 v1, v22

    move-object/from16 v2, v19

    move/from16 v3, v18

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private maybeDeleteUnsentAnnotation(Ljava/lang/String;)V
    .locals 5
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 540
    const-string v1, "local_id=? AND server_id IS NULL"

    .line 542
    .local v1, "whereClause":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 545
    .local v0, "whereArgs":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "annotations"

    const-string v4, "local_id=? AND server_id IS NULL"

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 547
    return-void
.end method

.method private maybeMarkSentAnnotationForDeletion(Ljava/lang/String;)V
    .locals 6
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 527
    const-string v2, "local_id=? AND server_id IS NOT NULL"

    .line 529
    .local v2, "whereClause":Ljava/lang/String;
    new-array v1, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 532
    .local v1, "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 533
    .local v0, "values":Landroid/content/ContentValues;
    const-string v3, "should_delete_on_server"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 534
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "local_id=? AND server_id IS NOT NULL"

    invoke-virtual {v3, v4, v0, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 537
    return-void
.end method

.method private newValuesForAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/ContentValues;
    .locals 16
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "serverAnnotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .param p3, "layerVersion"    # Ljava/lang/String;
    .param p4, "segmentIndex"    # Ljava/lang/Integer;

    .prologue
    .line 458
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 460
    .local v12, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 462
    .local v2, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    const-string v13, "local_id"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v13, "layer_id"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    if-eqz p3, :cond_0

    .line 465
    const-string v13, "layer_version"

    move-object/from16 v0, p3

    invoke-virtual {v12, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_0
    const-string v13, "type"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getAnnotationType()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v13, "volume_id"

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v13, "content_version"

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 473
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getStartLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v9

    .line 474
    .local v9, "startLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    const-string v13, "start_position"

    iget-object v14, v9, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v14}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v13, "start_offset"

    iget v14, v9, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 477
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getEndLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v4

    .line 478
    .local v4, "endLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    const-string v13, "end_position"

    iget-object v14, v4, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v14}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v13, "end_offset"

    iget v14, v4, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 482
    .end local v4    # "endLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    .end local v9    # "startLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_1
    const-string v13, "before_selected_text"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getBeforeSelectedText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const-string v13, "selected_text"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v13, "after_selected_text"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getAfterSelectedText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const-string v13, "color"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 487
    const-string v13, "notes"

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    const-string v13, "should_delete_on_server"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 491
    if-eqz p4, :cond_2

    .line 492
    const-string v13, "segment_index"

    move-object/from16 v0, p4

    invoke-virtual {v12, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 495
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getDataId()Ljava/lang/String;

    move-result-object v3

    .line 496
    .local v3, "dataId":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 497
    const-string v13, "data_id"

    invoke-virtual {v12, v13, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_3
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    .line 501
    .local v8, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    if-eqz v8, :cond_4

    .line 502
    const-string v13, "server_id"

    iget-object v14, v8, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v13, "server_timestamp"

    iget-wide v14, v8, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 506
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v5

    .line 507
    .local v5, "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    if-eqz v5, :cond_5

    .line 508
    const-string v14, "image_start_cfi"

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->toCfiString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v14, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v14, "image_end_cfi"

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->toCfiString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v14, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_5
    if-nez v8, :cond_6

    const-wide/16 v10, -0x1

    .line 513
    .local v10, "serverTimestamp":J
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v14

    invoke-static {v14, v15, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 514
    .local v6, "lastUsedTimestamp":J
    const-string v13, "last_used_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 516
    return-object v12

    .line 512
    .end local v6    # "lastUsedTimestamp":J
    .end local v10    # "serverTimestamp":J
    :cond_6
    iget-wide v10, v8, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    goto :goto_0
.end method

.method private parseImageRange(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 3
    .param p1, "startCfi"    # Ljava/lang/String;
    .param p2, "endCfi"    # Ljava/lang/String;

    .prologue
    .line 871
    :try_start_0
    invoke-static {p1, p2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->parseFromCfis(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;
    :try_end_0
    .catch Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 876
    :goto_0
    return-object v1

    .line 872
    :catch_0
    move-exception v0

    .line 873
    .local v0, "e":Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;
    const-string v1, "AnnotationsDB"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 874
    const-string v1, "AnnotationsDB"

    const-string v2, "exception parsing image cfis"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 876
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private varargs queryFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 8
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "queryParams"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 824
    sget-object v0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->FULL_RETRIEVAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "annotations"

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method private queryParams(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;

    .prologue
    .line 951
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    return-object v0
.end method

.method private serverIdFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 936
    const-string v0, "server_id"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateFullAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 906
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->UNKNOWN_SEGMENT_INDEX:Ljava/lang/Integer;

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->newValuesForAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/ContentValues;

    move-result-object v0

    .line 908
    .local v0, "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "annotations"

    const-string v3, "local_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 912
    return-void
.end method

.method private volumeVersionFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 3
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 881
    const-string v2, "volume_id"

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 882
    .local v1, "volumeId":Ljava/lang/String;
    const-string v2, "content_version"

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 883
    .local v0, "contentVersion":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private whereClause(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;

    .prologue
    .line 946
    const-string v0, "volume_id=? AND content_version=? AND layer_id=?"

    return-object v0
.end method


# virtual methods
.method public addUserAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;)V
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "serverAnnotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .param p3, "segmentIndex"    # Ljava/lang/Integer;

    .prologue
    .line 431
    const/4 v6, 0x1

    .line 432
    .local v6, "throwOnConflict":Z
    const/4 v2, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;Z)V

    .line 433
    return-void
.end method

.method public deleteAll()V
    .locals 2

    .prologue
    .line 978
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 979
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "DELETE FROM annotations"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 980
    const-string v1, "DELETE FROM volume_annotation_segment_summary"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 981
    const-string v1, "DELETE FROM layers"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 984
    const-string v1, "VACUUM"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 985
    return-void
.end method

.method public editAnnotationFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 0
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 453
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->updateFullAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    .line 454
    return-void
.end method

.method public expungeServerId(Ljava/lang/String;)V
    .locals 5
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 551
    const-string v1, "server_id=?"

    .line 552
    .local v1, "whereClause":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 555
    .local v0, "whereArgs":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "annotations"

    const-string v4, "server_id=?"

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 557
    return-void
.end method

.method public getAllServerIds(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/Set;
    .locals 3
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 941
    const-string v0, "server_id"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->whereClause(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "server_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NOT NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryParams(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getStringColumn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public getLatestServerTimestamp(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)J
    .locals 8
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, -0x1

    .line 575
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT MAX(server_timestamp) FROM annotations WHERE "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->whereClause(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 578
    .local v3, "sql":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryParams(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 580
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 581
    const/4 v6, 0x0

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 582
    .local v0, "answer":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-nez v6, :cond_0

    .line 592
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v4

    .end local v0    # "answer":J
    :goto_0
    return-wide v0

    .restart local v0    # "answer":J
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v0    # "answer":J
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v4

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;

    .prologue
    .line 973
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/annotations/LayersTable;->getLayer(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v0

    return-object v0
.end method

.method public getLocalIdForServerId(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 568
    const-string v1, "local_id"

    const-string v2, "server_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getStringColumn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 570
    .local v0, "localIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;I)Ljava/util/List;
    .locals 8
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p2, "segmentIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Layer;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 762
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getSegmentVolumeAnnotationsUnlogged(Lcom/google/android/apps/books/annotations/Layer;I)Ljava/util/List;

    move-result-object v0

    .line 764
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const-string v2, "AnnotationsDB"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 765
    const-string v2, "%s/%d"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 766
    .local v1, "querySpec":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 767
    const-string v2, "AnnotationsDB"

    const-string v3, "Volume annotation cache miss for %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    .end local v1    # "querySpec":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 770
    .restart local v1    # "querySpec":Ljava/lang/String;
    :cond_1
    const-string v2, "AnnotationsDB"

    const-string v3, "Loaded %d volume annotations for %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public load(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->whereClause(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "should_delete_on_server"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 563
    .local v0, "query":Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryParams(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public markEditedOnClient(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 8
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "newValue"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    const/4 v7, 0x0

    .line 896
    const-string v3, "server_id"

    const-string v4, "local_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getStringColumn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 899
    .local v2, "serverIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    .line 901
    .local v1, "serverId":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    const-wide/16 v4, -0x1

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;-><init>(Ljava/lang/String;J)V

    .line 902
    .local v0, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    new-instance v3, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    invoke-direct {v3, p2, v0}, Lcom/google/android/apps/books/annotations/ServerAnnotation;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->updateFullAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    .line 903
    return-void

    .line 899
    .end local v0    # "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    .end local v1    # "serverId":Ljava/lang/String;
    :cond_0
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v1, v3

    goto :goto_0
.end method

.method public markForDeletion(Ljava/lang/String;)V
    .locals 1
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 521
    move-object v0, p1

    .line 522
    .local v0, "idString":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->maybeMarkSentAnnotationForDeletion(Ljava/lang/String;)V

    .line 523
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->maybeDeleteUnsentAnnotation(Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public removeAnnotationsForLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    .locals 8
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;

    .prologue
    const/4 v5, 0x3

    .line 645
    const-string v4, "AnnotationsDB"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 646
    const-string v4, "AnnotationsDB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Removing annotations for layer "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 650
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 653
    :try_start_0
    const-string v4, "%s=? AND %s=? AND %s=? AND %s=?"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "volume_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "content_version"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "layer_id"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "layer_version"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 657
    .local v2, "query":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v3

    .line 658
    .local v3, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    const/4 v4, 0x4

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v3, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    iget-object v5, v3, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getLayerId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    const/4 v4, 0x3

    iget-object v5, p1, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    aput-object v5, v0, v4

    .line 661
    .local v0, "args":[Ljava/lang/String;
    const-string v4, "annotations"

    invoke-virtual {v1, v4, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 663
    invoke-static {p1, v1}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->removeVolumeAnnotationsForLayer(Lcom/google/android/apps/books/annotations/Layer;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 665
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 669
    return-void

    .line 667
    .end local v0    # "args":[Ljava/lang/String;
    .end local v2    # "query":Ljava/lang/String;
    .end local v3    # "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public removeLocalAnnotation(Ljava/lang/String;)V
    .locals 5
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 673
    const-string v1, "local_id=?"

    .line 674
    .local v1, "whereClause":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 677
    .local v0, "whereArgs":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "annotations"

    const-string v4, "local_id=?"

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 679
    return-void
.end method

.method public removeVolumeAnnotationsForVolume(Ljava/lang/String;)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 598
    invoke-static {}, Lcom/google/android/apps/books/annotations/Annotation;->getAllVolumeLayerIds()Ljava/util/List;

    move-result-object v6

    .line 599
    .local v6, "volumeLayerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 607
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_3

    .line 608
    if-lez v1, :cond_2

    .line 609
    const-string v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 611
    :cond_2
    const-string v7, "?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 616
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "volume_id=? AND layer_id IN ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 620
    .local v2, "query":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 621
    .local v3, "queryParams":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622
    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 624
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    new-array v4, v7, [Ljava/lang/String;

    .line 626
    .local v4, "queryParamsAsArray":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 629
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "annotations"

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    invoke-virtual {v0, v8, v2, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 633
    invoke-static {v0, p1}, Lcom/google/android/apps/books/annotations/LayersTable;->removeLayersForVolume(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 639
    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->removeVolumeAnnotationsForVolume(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public setLastUsedTimestamp(Ljava/lang/String;J)V
    .locals 6
    .param p1, "localId"    # Ljava/lang/String;
    .param p2, "timestamp"    # J

    .prologue
    .line 1008
    const-string v2, "local_id=?"

    .line 1009
    .local v2, "whereClause":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 1010
    .local v1, "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1011
    .local v0, "values":Landroid/content/ContentValues;
    const-string v3, "last_used_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1012
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "local_id=?"

    invoke-virtual {v3, v4, v0, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1014
    return-void
.end method

.method public setSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;ILjava/util/List;Z)V
    .locals 9
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p2, "segmentIndex"    # I
    .param p4, "allPresent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Layer;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/ServerAnnotation;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p3, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/ServerAnnotation;>;"
    const/4 v1, 0x3

    const/4 v5, 0x0

    .line 726
    const-string v0, "AnnotationsDB"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    const-string v0, "AnnotationsDB"

    const-string v1, "Saving %d volume annotations for %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 732
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 735
    :try_start_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .line 739
    .local v3, "annotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    const/4 v8, 0x0

    .line 740
    .local v8, "throwOnConflict":Z
    const-string v0, "AnnotationsDB"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 741
    const-string v0, "AnnotationsDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saving annotation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 756
    .end local v3    # "annotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "throwOnConflict":Z
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 751
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    invoke-static {p1, p2, v6, p4}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->setAllAnnotationsPresent(Lcom/google/android/apps/books/annotations/Layer;ILandroid/database/sqlite/SQLiteDatabase;Z)V

    .line 754
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 756
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 758
    return-void
.end method

.method public unsyncedAdditions()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    const-string v2, "server_id IS NULL"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v1

    .line 699
    .local v1, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 700
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;>;"
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 701
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->annotationAdditionFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 705
    .end local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;>;"
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v2

    .restart local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;>;"
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v0
.end method

.method public unsyncedDeletedServerIds()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 690
    const-string v0, "should_delete_on_server=1"

    .line 691
    .local v0, "query":Ljava/lang/String;
    const-string v1, "server_id"

    const-string v2, "should_delete_on_server=1"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getStringColumn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public unsyncedEdits()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 916
    const-string v2, "server_id IS NOT NULL AND server_timestamp <= 0"

    .line 918
    .local v2, "query":Ljava/lang/String;
    const-string v3, "server_id IS NOT NULL AND server_timestamp <= 0"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->queryFullAnnotations(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    .line 920
    .local v0, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 921
    .local v1, "edits":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 922
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->annotationEditFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 926
    .end local v1    # "edits":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;>;"
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v3

    .restart local v1    # "edits":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;>;"
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v1
.end method

.method public updateLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    .locals 4
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;

    .prologue
    .line 958
    const-string v1, "AnnotationsDB"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 959
    const-string v1, "AnnotationsDB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saving layer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 962
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 964
    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/apps/books/annotations/LayersTable;->updateLayer(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/books/annotations/Layer;)V

    .line 965
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 969
    return-void

    .line 967
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public updateServerReceipt(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V
    .locals 6
    .param p1, "localId"    # Ljava/lang/String;
    .param p2, "receipt"    # Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    .prologue
    .line 711
    const-string v2, "local_id=?"

    .line 712
    .local v2, "whereClause":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 715
    .local v1, "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 716
    .local v0, "values":Landroid/content/ContentValues;
    const-string v3, "server_id"

    iget-object v4, p2, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const-string v3, "server_timestamp"

    iget-wide v4, p2, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 718
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "local_id=?"

    invoke-virtual {v3, v4, v0, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 721
    return-void
.end method
