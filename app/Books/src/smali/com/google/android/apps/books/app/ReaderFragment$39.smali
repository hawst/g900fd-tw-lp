.class Lcom/google/android/apps/books/app/ReaderFragment$39;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->maybeHideTransitionCover()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$interpolator:Landroid/view/animation/AccelerateInterpolator;

.field final synthetic val$overlay:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/View;Landroid/view/animation/AccelerateInterpolator;)V
    .locals 0

    .prologue
    .line 8278
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->val$overlay:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->val$interpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v8, 0x0

    .line 8287
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTransitionCoverFrame()Landroid/view/View;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17000(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/View;

    move-result-object v1

    .line 8289
    .local v1, "fadeCoverFrame":Landroid/view/View;
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v6

    const v7, 0x7f0e0117

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 8291
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 8292
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 8293
    .local v2, "size":Landroid/graphics/Point;
    invoke-static {v0, v2}, Lcom/google/android/apps/books/util/ReaderUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 8295
    iget v5, v2, Landroid/graphics/Point;->x:I

    .line 8298
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    .line 8300
    .local v4, "toXDelta":I
    :goto_0
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    int-to-float v6, v4

    invoke-direct {v3, v8, v6, v8, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 8301
    .local v3, "slideOut":Landroid/view/animation/Animation;
    const-wide/16 v6, 0x190

    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 8302
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->val$interpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v3, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 8303
    new-instance v6, Lcom/google/android/apps/books/app/ReaderFragment$39$1;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$39$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$39;Landroid/view/View;)V

    invoke-virtual {v3, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 8326
    invoke-virtual {v1, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 8327
    return-void

    .line 8298
    .end local v3    # "slideOut":Landroid/view/animation/Animation;
    .end local v4    # "toXDelta":I
    :cond_0
    neg-int v4, v5

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 8330
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 8281
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39;->val$overlay:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8282
    return-void
.end method
