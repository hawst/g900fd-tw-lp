.class public Lcom/google/android/apps/books/view/LocationItemView;
.super Landroid/widget/LinearLayout;
.source "LocationItemView.java"


# instance fields
.field private mIndentPixels:I

.field private mOriginalLeftPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method private bind(ZLjava/lang/String;Ljava/lang/String;ZI)V
    .locals 6
    .param p1, "selected"    # Z
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "pageTitle"    # Ljava/lang/String;
    .param p4, "enabled"    # Z
    .param p5, "depth"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p4}, Lcom/google/android/apps/books/view/LocationItemView;->setEnabled(Z)V

    .line 67
    const v3, 0x7f0e003d

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/view/LocationItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 68
    .local v0, "descriptionView":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/books/view/LocationItemView;->styleDescriptionView(Landroid/widget/TextView;Z)V

    .line 72
    const v3, 0x7f0e003e

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/view/LocationItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 73
    .local v2, "pageTitleView":Landroid/widget/TextView;
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, p3, v4, v5}, Lcom/google/android/apps/books/util/ReaderUtils;->formatPageTitle(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/Long;Z)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {p0, v2, p1}, Lcom/google/android/apps/books/view/LocationItemView;->stylePageTitleView(Landroid/widget/TextView;Z)V

    .line 78
    iget v3, p0, Lcom/google/android/apps/books/view/LocationItemView;->mOriginalLeftPadding:I

    invoke-direct {p0, p5}, Lcom/google/android/apps/books/view/LocationItemView;->getDepthIndent(I)I

    move-result v4

    add-int v1, v3, v4

    .line 79
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getPaddingRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {p0, v1, v3, v4, v5}, Lcom/google/android/apps/books/view/LocationItemView;->setPadding(IIII)V

    .line 80
    return-void
.end method

.method private getDepthIndent(I)I
    .locals 3
    .param p1, "depth"    # I

    .prologue
    .line 107
    const/4 v1, 0x0

    add-int/lit8 v2, p1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 108
    .local v0, "effectiveDepth":I
    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/view/LocationItemView;->mIndentPixels:I

    mul-int/2addr v1, v2

    return v1
.end method

.method private getStartPageTitle(Lcom/google/android/apps/books/model/VolumeMetadata;I)Ljava/lang/String;
    .locals 4
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "chapterIndex"    # I

    .prologue
    .line 84
    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getStartPageTitle(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 89
    :goto_0
    return-object v1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "LocationItemView"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    const-string v1, "LocationItemView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error getting start page title: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;I)V
    .locals 7
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .param p3, "chapterIndex"    # I

    .prologue
    .line 42
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/Chapter;

    .line 43
    .local v6, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "description":Ljava/lang/String;
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/books/view/LocationItemView;->getStartPageTitle(Lcom/google/android/apps/books/model/VolumeMetadata;I)Ljava/lang/String;

    move-result-object v3

    .line 45
    .local v3, "pageTitle":Ljava/lang/String;
    invoke-interface {p1, p3, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isChapterViewable(ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v4

    .line 46
    .local v4, "enabled":Z
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Chapter;->getDepth()I

    move-result v5

    .line 47
    .local v5, "depth":I
    const/4 v1, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/LocationItemView;->bind(ZLjava/lang/String;Ljava/lang/String;ZI)V

    .line 48
    return-void
.end method

.method public bindCurrentPage(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;I)V
    .locals 7
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "currentPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p3, "depth"    # I

    .prologue
    const/4 v1, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0f01ff

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "description":Ljava/lang/String;
    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .local v3, "pageTitle":Ljava/lang/String;
    :goto_0
    move-object v0, p0

    move v4, v1

    move v5, p3

    .line 59
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/LocationItemView;->bind(ZLjava/lang/String;Ljava/lang/String;ZI)V

    .line 60
    return-void

    .line 56
    .end local v3    # "pageTitle":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 57
    .local v6, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, ""

    .restart local v3    # "pageTitle":Ljava/lang/String;
    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/view/LocationItemView;->mOriginalLeftPadding:I

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090195

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/view/LocationItemView;->mIndentPixels:I

    .line 39
    return-void
.end method

.method protected styleDescriptionView(Landroid/widget/TextView;Z)V
    .locals 2
    .param p1, "descriptionView"    # Landroid/widget/TextView;
    .param p2, "selected"    # Z

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f0a0173

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 96
    return-void

    .line 94
    :cond_1
    const v0, 0x7f0a0175

    goto :goto_0
.end method

.method protected stylePageTitleView(Landroid/widget/TextView;Z)V
    .locals 2
    .param p1, "startPageTitleView"    # Landroid/widget/TextView;
    .param p2, "selected"    # Z

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/LocationItemView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f0a0174

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 101
    return-void

    .line 99
    :cond_1
    const v0, 0x7f0a0175

    goto :goto_0
.end method
