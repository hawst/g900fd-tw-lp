.class public Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;
.super Lcom/google/android/apps/books/widget/CardDetailsLayout;
.source "TabletRecommendationCardDetailsLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/CardDetailsLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/CardDetailsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/CardDetailsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method

.method private static getLayoutBaseline(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 235
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 236
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/view/View;->getBaseline()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->onFinishInflate()V

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->setReasonMaxLines(I)V

    .line 33
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 38
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 121
    sub-int v35, p4, p2

    .line 123
    .local v35, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingLeft()I

    move-result v11

    .line 124
    .local v11, "contentLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingTop()I

    move-result v13

    .line 125
    .local v13, "contentTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingRight()I

    move-result v36

    sub-int v12, v35, v36

    .line 126
    .local v12, "contentRight":I
    const/4 v15, 0x0

    .line 127
    .local v15, "firstRowBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_0

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 129
    .local v19, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v33, v13, v36

    .line 130
    .local v33, "thisChildTop":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v31, v11, v36

    .line 131
    .local v31, "thisChildLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v36

    add-int v30, v33, v36

    .line 132
    .local v30, "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v37

    add-int v37, v37, v31

    move-object/from16 v0, v36

    move/from16 v1, v31

    move/from16 v2, v33

    move/from16 v3, v37

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 134
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    add-int v15, v30, v36

    .line 136
    .end local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v30    # "thisChildBottom":I
    .end local v31    # "thisChildLeft":I
    .end local v33    # "thisChildTop":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 138
    .restart local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v33, v13, v36

    .line 139
    .restart local v33    # "thisChildTop":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v36, v0

    sub-int v32, v12, v36

    .line 140
    .local v32, "thisChildRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v36

    add-int v30, v33, v36

    .line 141
    .restart local v30    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getMeasuredWidth()I

    move-result v37

    sub-int v37, v32, v37

    move-object/from16 v0, v36

    move/from16 v1, v37

    move/from16 v2, v33

    move/from16 v3, v32

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->layout(IIII)V

    .line 143
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    add-int v36, v36, v30

    move/from16 v0, v36

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->onOverflowIconPositioned()V

    .line 146
    .end local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v30    # "thisChildBottom":I
    .end local v32    # "thisChildRight":I
    .end local v33    # "thisChildTop":I
    :cond_1
    const/16 v23, 0x0

    .line 150
    .local v23, "secondRowBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getLayoutBaseline(Landroid/view/View;)I

    move-result v29

    .line 151
    .local v29, "subtitleBaseline":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getLayoutBaseline(Landroid/view/View;)I

    move-result v22

    .line 152
    .local v22, "priceBaseline":I
    move/from16 v0, v29

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 154
    .local v21, "maxBaseline":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 155
    const/16 v36, 0x0

    sub-int v37, v21, v29

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 156
    .local v10, "baselineOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 157
    .restart local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v36, v36, v15

    add-int v33, v36, v10

    .line 158
    .restart local v33    # "thisChildTop":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v31, v11, v36

    .line 159
    .restart local v31    # "thisChildLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v36

    add-int v30, v33, v36

    .line 160
    .restart local v30    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v37

    add-int v37, v37, v31

    move-object/from16 v0, v36

    move/from16 v1, v31

    move/from16 v2, v33

    move/from16 v3, v37

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 162
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    add-int v23, v30, v36

    .line 165
    .end local v10    # "baselineOffset":I
    .end local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v30    # "thisChildBottom":I
    .end local v31    # "thisChildLeft":I
    .end local v33    # "thisChildTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 166
    const/16 v36, 0x0

    sub-int v37, v21, v22

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 167
    .restart local v10    # "baselineOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 168
    .restart local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v36, v36, v15

    add-int v33, v36, v10

    .line 169
    .restart local v33    # "thisChildTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v36

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v37, v0

    add-int v36, v36, v37

    sub-int v31, v12, v36

    .line 170
    .restart local v31    # "thisChildLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v36

    add-int v30, v33, v36

    .line 171
    .restart local v30    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v37

    add-int v37, v37, v31

    move-object/from16 v0, v36

    move/from16 v1, v31

    move/from16 v2, v33

    move/from16 v3, v37

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 173
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    add-int v36, v36, v30

    move/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v23

    .line 179
    .end local v10    # "baselineOffset":I
    .end local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v30    # "thisChildBottom":I
    .end local v31    # "thisChildLeft":I
    .end local v33    # "thisChildTop":I
    :cond_3
    sub-int v16, p5, p3

    .line 180
    .local v16, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 182
    .local v20, "lpAction":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v6, v11, v36

    .line 183
    .local v6, "actionLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v36

    add-int v7, v6, v36

    .line 186
    .local v7, "actionRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingBottom()I

    move-result v36

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v37, v0

    add-int v36, v36, v37

    sub-int v30, v16, v36

    .line 187
    .restart local v30    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v36

    sub-int v33, v30, v36

    .line 189
    .restart local v33    # "thisChildTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v33

    move/from16 v2, v30

    invoke-virtual {v0, v6, v1, v7, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 191
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    sub-int v5, v33, v36

    .line 194
    .local v5, "actionButtonTopPlusMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 198
    .local v26, "separatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v25, v11, v36

    .line 199
    .local v25, "separatorLeft":I
    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    sub-int v24, v5, v36

    .line 200
    .local v24, "separatorBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getMeasuredHeight()I

    move-result v36

    sub-int v28, v24, v36

    .line 201
    .local v28, "separatorTop":I
    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v36, v0

    sub-int v27, v12, v36

    .line 202
    .local v27, "separatorRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v25

    move/from16 v2, v28

    move/from16 v3, v27

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 204
    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    sub-int v8, v28, v36

    .line 210
    .end local v25    # "separatorLeft":I
    .end local v26    # "separatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v27    # "separatorRight":I
    .end local v28    # "separatorTop":I
    .local v8, "actionSeparatorTop":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mActionTouchArea:Landroid/view/View;

    move-object/from16 v36, v0

    const/16 v37, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v37

    move/from16 v2, v24

    move/from16 v3, v35

    move/from16 v4, v16

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 215
    .end local v5    # "actionButtonTopPlusMargin":I
    .end local v6    # "actionLeft":I
    .end local v7    # "actionRight":I
    .end local v20    # "lpAction":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v24    # "separatorBottom":I
    .end local v30    # "thisChildBottom":I
    .end local v33    # "thisChildTop":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 218
    .restart local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v17, v23, v36

    .line 219
    .local v17, "highestChildTop":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v31, v11, v36

    .line 220
    .restart local v31    # "thisChildLeft":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v36, v0

    sub-int v18, v8, v36

    .line 221
    .local v18, "lowestChildBottom":I
    sub-int v9, v18, v17

    .line 222
    .local v9, "availableHeight":I
    const/16 v36, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v37

    sub-int v37, v9, v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 223
    .local v14, "extraHeight":I
    div-int/lit8 v34, v14, 0x2

    .line 224
    .local v34, "topExtra":I
    add-int v33, v17, v34

    .line 225
    .restart local v33    # "thisChildTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v36

    add-int v30, v33, v36

    .line 226
    .restart local v30    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getMeasuredWidth()I

    move-result v37

    add-int v37, v37, v31

    move-object/from16 v0, v36

    move/from16 v1, v31

    move/from16 v2, v33

    move/from16 v3, v37

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->layout(IIII)V

    .line 229
    .end local v9    # "availableHeight":I
    .end local v14    # "extraHeight":I
    .end local v17    # "highestChildTop":I
    .end local v18    # "lowestChildBottom":I
    .end local v19    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v30    # "thisChildBottom":I
    .end local v31    # "thisChildLeft":I
    .end local v33    # "thisChildTop":I
    .end local v34    # "topExtra":I
    :cond_4
    return-void

    .line 206
    .end local v8    # "actionSeparatorTop":I
    .restart local v5    # "actionButtonTopPlusMargin":I
    .restart local v6    # "actionLeft":I
    .restart local v7    # "actionRight":I
    .restart local v20    # "lpAction":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v30    # "thisChildBottom":I
    .restart local v33    # "thisChildTop":I
    :cond_5
    move v8, v5

    .line 207
    .restart local v8    # "actionSeparatorTop":I
    move/from16 v24, v5

    .restart local v24    # "separatorBottom":I
    goto/16 :goto_0

    .line 212
    .end local v5    # "actionButtonTopPlusMargin":I
    .end local v6    # "actionLeft":I
    .end local v7    # "actionRight":I
    .end local v8    # "actionSeparatorTop":I
    .end local v20    # "lpAction":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v24    # "separatorBottom":I
    .end local v30    # "thisChildBottom":I
    .end local v33    # "thisChildTop":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingBottom()I

    move-result v36

    sub-int v8, v16, v36

    .restart local v8    # "actionSeparatorTop":I
    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 19
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 37
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v16

    .line 38
    .local v16, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingLeft()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingRight()I

    move-result v18

    add-int v17, v17, v18

    sub-int v4, v16, v17

    .line 39
    .local v4, "allowedChildWidth":I
    const/high16 v17, -0x80000000

    move/from16 v0, v17

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 43
    .local v7, "fullChildWidthSpec":I
    const/4 v6, 0x0

    .line 44
    .local v6, "firstRowHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v7, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->measure(II)V

    .line 46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutWidth(Landroid/view/View;)I

    move-result v14

    .line 47
    .local v14, "subtractFromTitleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v6

    .line 53
    :goto_0
    const/4 v11, 0x0

    .line 54
    .local v11, "secondRowHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->measure(II)V

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutWidth(Landroid/view/View;)I

    move-result v13

    .line 57
    .local v13, "subtractFromSubtitleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mPrice:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v11

    .line 62
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getHorizontalMargins(Landroid/view/View;)I

    move-result v17

    add-int v17, v17, v14

    sub-int v5, v4, v17

    .line 65
    .local v5, "childWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 70
    .end local v5    # "childWidth":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getHorizontalMargins(Landroid/view/View;)I

    move-result v17

    add-int v17, v17, v13

    sub-int v5, v4, v17

    .line 73
    .restart local v5    # "childWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 79
    .end local v5    # "childWidth":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getHorizontalMargins(Landroid/view/View;)I

    move-result v17

    sub-int v5, v4, v17

    .line 82
    .restart local v5    # "childWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->measure(II)V

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getTextView()Landroid/widget/TextView;

    move-result-object v15

    .line 95
    .local v15, "tv":Landroid/widget/TextView;
    invoke-virtual {v15}, Landroid/widget/TextView;->getLineHeight()I

    move-result v17

    mul-int/lit8 v9, v17, 0x2

    .line 96
    .local v9, "linesHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getPaddingTop()I

    move-result v17

    add-int v17, v17, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getPaddingBottom()I

    move-result v18

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getVerticalMargins(Landroid/view/View;)I

    move-result v18

    add-int v10, v17, v18

    .line 102
    .end local v5    # "childWidth":I
    .end local v9    # "linesHeight":I
    .end local v15    # "tv":Landroid/widget/TextView;
    .local v10, "reasonHeight":I
    :goto_2
    const/4 v12, 0x0

    .line 103
    .local v12, "separatorHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v7, v1}, Landroid/view/View;->measure(II)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v12

    .line 108
    :cond_2
    const/4 v3, 0x0

    .line 109
    .local v3, "actionButtonHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->measure(II)V

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->mAction:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v3

    .line 114
    :cond_3
    add-int v17, v6, v11

    add-int v17, v17, v10

    add-int v17, v17, v12

    add-int v17, v17, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingTop()I

    move-result v18

    add-int v17, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->getPaddingBottom()I

    move-result v18

    add-int v8, v17, v18

    .line 116
    .local v8, "height":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/books/widget/TabletRecommendationCardDetailsLayout;->setMeasuredDimension(II)V

    .line 117
    return-void

    .line 49
    .end local v3    # "actionButtonHeight":I
    .end local v8    # "height":I
    .end local v10    # "reasonHeight":I
    .end local v11    # "secondRowHeight":I
    .end local v12    # "separatorHeight":I
    .end local v13    # "subtractFromSubtitleWidth":I
    .end local v14    # "subtractFromTitleWidth":I
    :cond_4
    const/4 v14, 0x0

    .restart local v14    # "subtractFromTitleWidth":I
    goto/16 :goto_0

    .line 59
    .restart local v11    # "secondRowHeight":I
    :cond_5
    const/4 v13, 0x0

    .restart local v13    # "subtractFromSubtitleWidth":I
    goto/16 :goto_1

    .line 99
    :cond_6
    const/4 v10, 0x0

    .restart local v10    # "reasonHeight":I
    goto :goto_2
.end method
