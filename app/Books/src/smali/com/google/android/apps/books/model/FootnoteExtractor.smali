.class public Lcom/google/android/apps/books/model/FootnoteExtractor;
.super Lcom/google/android/apps/books/model/ContentXmlHandler;
.source "FootnoteExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/FootnoteExtractor$1;,
        Lcom/google/android/apps/books/model/FootnoteExtractor$Element;,
        Lcom/google/android/apps/books/model/FootnoteExtractor$State;
    }
.end annotation


# static fields
.field static final CONTENT_TAGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContentBuilder:Ljava/lang/StringBuilder;

.field private final mFootnoteBuilder:Ljava/lang/StringBuilder;

.field private final mFootnoteId:Ljava/lang/String;

.field private mNumOpenTagsInAnchor:I

.field private mNumOpenTagsInFootnote:I

.field private final mParentTags:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/apps/books/model/FootnoteExtractor$Element;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegment:Ljava/lang/String;

.field private mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field private final mUrlScheme:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 203
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "aside"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "div"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "p"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "li"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "dd"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "dt"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/play/utils/collections/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor;->CONTENT_TAGS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "segment"    # Ljava/lang/String;
    .param p2, "footnoteId"    # Ljava/lang/String;
    .param p3, "urlScheme"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/books/model/ContentXmlHandler;-><init>()V

    .line 45
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mContentBuilder:Ljava/lang/StringBuilder;

    .line 56
    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    .line 57
    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    .line 59
    sget-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->START:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mSegment:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteId:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mUrlScheme:Ljava/lang/String;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/FootnoteExtractor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/FootnoteExtractor;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mUrlScheme:Ljava/lang/String;

    return-object v0
.end method

.method private foundFootnoteId(Ljava/lang/String;Lorg/xml/sax/Attributes;)Z
    .locals 7
    .param p1, "qName"    # Ljava/lang/String;
    .param p2, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 148
    const-string v6, "id"

    invoke-interface {p2, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "id":Ljava/lang/String;
    const-string v6, "epub:type"

    invoke-interface {p2, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "epubType":Ljava/lang/String;
    const-string v6, "aside"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "footnote"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "rearnote"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_0
    move v2, v4

    .line 153
    .local v2, "foundAside":Z
    :goto_0
    const-string v6, "a"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_1
    move v1, v4

    .line 155
    .local v1, "foundAnchor":Z
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteId:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-nez v2, :cond_2

    if-eqz v1, :cond_5

    :cond_2
    :goto_2
    return v4

    .end local v1    # "foundAnchor":Z
    .end local v2    # "foundAside":Z
    :cond_3
    move v2, v5

    .line 151
    goto :goto_0

    .restart local v2    # "foundAside":Z
    :cond_4
    move v1, v5

    .line 153
    goto :goto_1

    .restart local v1    # "foundAnchor":Z
    :cond_5
    move v4, v5

    .line 155
    goto :goto_2
.end method

.method private isContentTag(Ljava/lang/String;)Z
    .locals 1
    .param p1, "qName"    # Ljava/lang/String;

    .prologue
    .line 209
    sget-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor;->CONTENT_TAGS:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    .line 162
    .local v0, "element":Lcom/google/android/apps/books/model/FootnoteExtractor$Element;
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$1;->$SwitchMap$com$google$android$apps$books$model$FootnoteExtractor$State:[I

    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 198
    :cond_0
    :goto_0
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    iget-boolean v2, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    iget-boolean v3, v0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    or-int/2addr v2, v3

    iput-boolean v2, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    .line 201
    :cond_1
    return-void

    .line 164
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 169
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 170
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    if-lez v1, :cond_2

    .line 171
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    goto :goto_0

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    iget-boolean v1, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mIsTypeDisplayNoneDiv:Z

    if-nez v1, :cond_0

    .line 173
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->PAST_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_0

    .line 177
    :pswitch_2
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_0

    .line 180
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 184
    :pswitch_3
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    if-lez v1, :cond_4

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    const-string v2, "</"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    goto :goto_0

    .line 187
    :cond_4
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 188
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_0

    .line 190
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->getOpenTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->getCloseTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public getFootnote()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mSegment:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/FootnoteExtractor;->parse(Ljava/lang/String;)V

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mContentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "plainText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    :cond_0
    const/4 v2, 0x0

    .line 232
    :goto_0
    return-object v2

    .line 226
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    .line 228
    .local v0, "element":Lcom/google/android/apps/books/model/FootnoteExtractor$Element;
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->getOpenTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->getCloseTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 232
    .end local v0    # "element":Lcom/google/android/apps/books/model/FootnoteExtractor$Element;
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected onText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {p1}, Lcom/google/android/common/base/StringUtil;->isEmptyOrWhitespace(Ljava/lang/String;)Z

    move-result v0

    .line 82
    .local v0, "isEmptyOrWhitespace":Z
    if-nez v0, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    .line 86
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$1;->$SwitchMap$com$google$android$apps$books$model$FootnoteExtractor$State:[I

    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 98
    :goto_0
    return-void

    .line 89
    :pswitch_0
    if-nez v0, :cond_1

    .line 90
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 94
    :cond_1
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mContentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    invoke-direct {v0, p0, p3, p4}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;-><init>(Lcom/google/android/apps/books/model/FootnoteExtractor;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 104
    .local v0, "element":Lcom/google/android/apps/books/model/FootnoteExtractor$Element;
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    iget-boolean v1, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    iput-boolean v1, v0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    .line 108
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$1;->$SwitchMap$com$google$android$apps$books$model$FootnoteExtractor$State:[I

    iget-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 110
    :pswitch_0
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/books/model/FootnoteExtractor;->foundFootnoteId(Ljava/lang/String;Lorg/xml/sax/Attributes;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 122
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    iget-boolean v1, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    if-eqz v1, :cond_3

    .line 115
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_1

    .line 116
    :cond_3
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_1

    .line 119
    :cond_4
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_1

    .line 125
    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInAnchor:I

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 129
    :pswitch_2
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_0

    .line 133
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mParentTags:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;

    iget-object v1, v1, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mQName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/model/FootnoteExtractor;->isContentTag(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 134
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 141
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mFootnoteBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->getOpenTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    iget v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mNumOpenTagsInFootnote:I

    goto :goto_0

    .line 137
    :cond_6
    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    iput-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor;->mState:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
