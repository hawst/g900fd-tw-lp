.class Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;
.super Ljava/lang/Object;
.source "GridRowInvalidationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;

.field final synthetic val$view:Lcom/google/android/ublib/widget/AbsWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;->this$1:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;

    iput-object p2, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;->val$view:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;->this$1:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$100(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;->this$1:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->invalidateLists()V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;->val$view:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->invalidateViews()V

    goto :goto_0
.end method
