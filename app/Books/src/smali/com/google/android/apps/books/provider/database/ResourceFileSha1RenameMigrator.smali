.class Lcom/google/android/apps/books/provider/database/ResourceFileSha1RenameMigrator;
.super Ljava/lang/Object;
.source "ResourceFileSha1RenameMigrator.java"


# direct methods
.method public static execute(Ljava/io/File;)V
    .locals 33
    .param p0, "baseDir"    # Ljava/io/File;

    .prologue
    .line 28
    const-string v30, "ResourceFileMigrator"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 29
    const-string v30, "ResourceFileMigrator"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Migrating resource files in "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 36
    .local v24, "startMillis":J
    new-instance v7, Ljava/io/File;

    const-string v30, "accounts"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v7, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 37
    .local v7, "accountsDir":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 38
    .local v6, "accounts":[Ljava/lang/String;
    if-nez v6, :cond_2

    .line 68
    :cond_1
    :goto_0
    return-void

    .line 41
    :cond_2
    move-object v8, v6

    .local v8, "arr$":[Ljava/lang/String;
    array-length v0, v8

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    move v15, v11

    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v16    # "len$":I
    .local v15, "i$":I
    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_7

    aget-object v4, v8, v15

    .line 42
    .local v4, "account":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 43
    .local v5, "accountDir":Ljava/io/File;
    new-instance v29, Ljava/io/File;

    const-string v30, "volumes"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-direct {v0, v5, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 44
    .local v29, "volumesDir":Ljava/io/File;
    invoke-virtual/range {v29 .. v29}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v28

    .line 45
    .local v28, "volumes":[Ljava/lang/String;
    if-nez v28, :cond_4

    .line 41
    .end local v15    # "i$":I
    :cond_3
    add-int/lit8 v11, v15, 0x1

    .restart local v11    # "i$":I
    move v15, v11

    .end local v11    # "i$":I
    .restart local v15    # "i$":I
    goto :goto_1

    .line 48
    :cond_4
    move-object/from16 v9, v28

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v11, 0x0

    .end local v15    # "i$":I
    .restart local v11    # "i$":I
    move v14, v11

    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v17    # "len$":I
    .local v14, "i$":I
    :goto_2
    move/from16 v0, v17

    if-ge v14, v0, :cond_3

    aget-object v26, v9, v14

    .line 49
    .local v26, "volume":Ljava/lang/String;
    new-instance v27, Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 50
    .local v27, "volumeDir":Ljava/io/File;
    new-instance v20, Ljava/io/File;

    const-string v30, "res"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 51
    .local v20, "oldResDir":Ljava/io/File;
    new-instance v19, Ljava/io/File;

    const-string v30, "res2"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 52
    .local v19, "newResDir":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v23

    .line 53
    .local v23, "resources":[Ljava/lang/String;
    if-nez v23, :cond_6

    .line 48
    .end local v14    # "i$":I
    :cond_5
    add-int/lit8 v11, v14, 0x1

    .restart local v11    # "i$":I
    move v14, v11

    .end local v11    # "i$":I
    .restart local v14    # "i$":I
    goto :goto_2

    .line 56
    :cond_6
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdirs()Z

    .line 57
    move-object/from16 v10, v23

    .local v10, "arr$":[Ljava/lang/String;
    array-length v0, v10

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v11, 0x0

    .end local v14    # "i$":I
    .restart local v11    # "i$":I
    :goto_3
    move/from16 v0, v18

    if-ge v11, v0, :cond_5

    aget-object v21, v10, v11

    .line 58
    .local v21, "resource":Ljava/lang/String;
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 59
    .local v22, "resourceFile":Ljava/io/File;
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/apps/books/provider/database/ResourceFileSha1RenameMigrator;->renameResourceFile(Ljava/io/File;Ljava/io/File;)V

    .line 57
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 64
    .end local v4    # "account":Ljava/lang/String;
    .end local v5    # "accountDir":Ljava/io/File;
    .end local v10    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v18    # "len$":I
    .end local v19    # "newResDir":Ljava/io/File;
    .end local v20    # "oldResDir":Ljava/io/File;
    .end local v21    # "resource":Ljava/lang/String;
    .end local v22    # "resourceFile":Ljava/io/File;
    .end local v23    # "resources":[Ljava/lang/String;
    .end local v26    # "volume":Ljava/lang/String;
    .end local v27    # "volumeDir":Ljava/io/File;
    .end local v28    # "volumes":[Ljava/lang/String;
    .end local v29    # "volumesDir":Ljava/io/File;
    .restart local v15    # "i$":I
    :cond_7
    const-string v30, "ResourceFileMigrator"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 65
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v30

    sub-long v12, v30, v24

    .line 66
    .local v12, "elapsedMillis":J
    const-string v30, "ResourceFileMigrator"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Time to rename resource files: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "ms"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static renameResourceFile(Ljava/io/File;Ljava/io/File;)V
    .locals 11
    .param p0, "path"    # Ljava/io/File;
    .param p1, "newDir"    # Ljava/io/File;

    .prologue
    const/4 v10, 0x6

    .line 78
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "oldFilename":Ljava/lang/String;
    const/16 v7, 0xa

    invoke-static {v3, v7}, Lcom/google/android/apps/books/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v5

    .line 81
    .local v5, "resourceUrlBytes":[B
    new-instance v4, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v4, v5, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 82
    .local v4, "resourceUrl":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->resourceIdToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "newFilename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84
    .local v1, "newFile":Ljava/io/File;
    const-string v7, "ResourceFileMigrator"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    const-string v7, "ResourceFileMigrator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Renaming resource file for resource "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    invoke-virtual {p0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    .line 89
    .local v6, "success":Z
    if-nez v6, :cond_1

    const-string v7, "ResourceFileMigrator"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 90
    const-string v7, "ResourceFileMigrator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to move resource file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .end local v1    # "newFile":Ljava/io/File;
    .end local v2    # "newFilename":Ljava/lang/String;
    .end local v3    # "oldFilename":Ljava/lang/String;
    .end local v4    # "resourceUrl":Ljava/lang/String;
    .end local v5    # "resourceUrlBytes":[B
    .end local v6    # "success":Z
    :cond_1
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v7, "ResourceFileMigrator"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 94
    const-string v7, "ResourceFileMigrator"

    const-string v8, "Unsupported encoding UTF-8"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
