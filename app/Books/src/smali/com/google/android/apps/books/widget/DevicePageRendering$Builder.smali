.class public Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
.super Ljava/lang/Object;
.source "DevicePageRendering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/DevicePageRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mDebugText:Ljava/lang/String;

.field private mFallbackPosition:Lcom/google/android/apps/books/common/Position;

.field private mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

.field private mIndices:Lcom/google/android/apps/books/render/PageIndices;

.field private mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

.field private mPageOffset:I

.field private mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

.field private mPageText:Ljava/lang/String;

.field private mReadingPosition:Ljava/lang/String;

.field private mTouchables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mViewablePositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/apps/books/render/PageIndices;

    invoke-direct {v1, v3, v3}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    .line 399
    .local v1, "indices":Lcom/google/android/apps/books/render/PageIndices;
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mViewablePositions:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 402
    .local v2, "viewablePositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mTouchables:Ljava/util/List;

    if-nez v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 404
    .local v5, "touchables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    :goto_2
    new-instance v0, Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mReadingPosition:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageText:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mDebugText:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mFallbackPosition:Lcom/google/android/apps/books/common/Position;

    iget v10, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageOffset:I

    iget-object v11, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v12, 0x0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/books/widget/DevicePageRendering;-><init>(Lcom/google/android/apps/books/render/PageIndices;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;ILcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/android/apps/books/widget/DevicePageRendering$1;)V

    return-object v0

    .line 398
    .end local v1    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .end local v2    # "viewablePositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    .end local v5    # "touchables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    goto :goto_0

    .line 399
    .restart local v1    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mViewablePositions:Ljava/util/List;

    goto :goto_1

    .line 402
    .restart local v2    # "viewablePositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mTouchables:Ljava/util/List;

    goto :goto_2
.end method

.method public setDebugText(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "debugText"    # Ljava/lang/String;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mDebugText:Ljava/lang/String;

    .line 379
    return-object p0
.end method

.method public setFallbackPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "fallbackPosition"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mFallbackPosition:Lcom/google/android/apps/books/common/Position;

    .line 384
    return-object p0
.end method

.method public setFirstPositionOnNextPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "firstPositionOnNextPage"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    .line 359
    return-object p0
.end method

.method public setIndices(Lcom/google/android/apps/books/render/PageIndices;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "indices"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    .line 340
    return-object p0
.end method

.method public setPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "pageBounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    .line 369
    return-object p0
.end method

.method public setPageOffset(I)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "pageOffset"    # I

    .prologue
    .line 388
    iput p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageOffset:I

    .line 389
    return-object p0
.end method

.method public setPageStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "pageStructure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 394
    return-object p0
.end method

.method public setPageText(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "pageText"    # Ljava/lang/String;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mPageText:Ljava/lang/String;

    .line 374
    return-object p0
.end method

.method public setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mReadingPosition:Ljava/lang/String;

    .line 354
    return-object p0
.end method

.method public setTouchables(Ljava/util/List;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;)",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;"
        }
    .end annotation

    .prologue
    .line 363
    .local p1, "touchables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mTouchables:Ljava/util/List;

    .line 364
    return-object p0
.end method

.method public setViewablePositions(Ljava/util/List;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;)",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "viewablePositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->mViewablePositions:Ljava/util/List;

    .line 345
    return-object p0
.end method

.method public varargs setViewablePositions([Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 1
    .param p1, "viewablePositions"    # [Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 349
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setViewablePositions(Ljava/util/List;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v0

    return-object v0
.end method
