.class interface abstract Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
.super Ljava/lang/Object;
.source "PageTurnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PageTurnTask"
.end annotation


# virtual methods
.method public abstract canBeCanceled()Z
.end method

.method public abstract canRun()Z
.end method

.method public abstract getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
.end method

.method public abstract run()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation
.end method
