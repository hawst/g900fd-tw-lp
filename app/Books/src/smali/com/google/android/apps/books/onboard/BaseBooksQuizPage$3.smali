.class Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$3;
.super Ljava/lang/Object;
.source "BaseBooksQuizPage.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->serverOrderSortAndUpdateSnapshot(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 2
    .param p1, "lhs"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "rhs"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const v1, 0x7f0e0039

    .line 207
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 204
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/libraries/bind/data/Data;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$3;->compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    return v0
.end method
