.class public interface abstract Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;
.super Ljava/lang/Object;
.source "ScrubBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnScrubListener"
.end annotation


# virtual methods
.method public abstract onScrubBookmarkTap(I)V
.end method

.method public abstract onScrubFinished(I)V
.end method

.method public abstract onScrubStarted()V
.end method

.method public abstract onScrubUndo()V
.end method

.method public abstract onScrubUpdated(I)V
.end method
