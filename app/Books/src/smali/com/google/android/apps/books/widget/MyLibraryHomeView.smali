.class public Lcom/google/android/apps/books/widget/MyLibraryHomeView;
.super Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.source "MyLibraryHomeView.java"


# instance fields
.field private mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

.field private final mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mFilterToAdapter:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            "Lcom/google/android/apps/books/widget/BooksCardsAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mFilterToController:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            "Lcom/google/android/apps/books/widget/HeaderGridViewController;",
            ">;"
        }
    .end annotation
.end field

.field private mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

.field private mMyLibraryEmptyView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;Z)V
    .locals 2
    .param p1, "controller"    # Lcom/google/android/apps/books/widget/BooksHomeController;
    .param p2, "cardsContainer"    # Landroid/view/ViewGroup;
    .param p3, "isDeviceConnected"    # Z

    .prologue
    .line 168
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;-><init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;Z)V

    .line 57
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    .line 78
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 140
    new-instance v0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView$1;-><init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 170
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V

    .line 171
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V

    .line 172
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V

    .line 173
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->SAMPLES:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V

    .line 174
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V

    .line 175
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/MyLibraryHomeView;Ljava/lang/String;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->findUploadView(Ljava/lang/String;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    return-object v0
.end method

.method private findUploadView(Ljava/lang/String;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .locals 9
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 106
    sget-object v8, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {p0, v8}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getContentView(Lcom/google/android/apps/books/app/LibraryFilter;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    .line 108
    .local v4, "uploadGridView":Landroid/support/v7/widget/RecyclerView;
    if-nez v4, :cond_0

    move-object v0, v7

    .line 137
    :goto_0
    return-object v0

    .line 112
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v8

    if-ge v2, v8, :cond_3

    .line 113
    invoke-virtual {v4, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 114
    .local v5, "view":Landroid/view/View;
    instance-of v8, v5, Landroid/view/ViewGroup;

    if-nez v8, :cond_2

    .line 112
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move-object v6, v5

    .line 118
    check-cast v6, Landroid/view/ViewGroup;

    .line 120
    .local v6, "viewGroup":Landroid/view/ViewGroup;
    const v8, 0x7f0e00c9

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 123
    .local v0, "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 129
    .local v1, "doc":Ljava/lang/Object;
    instance-of v8, v1, Lcom/google/android/apps/books/playcards/UploadDocument;

    if-eqz v8, :cond_1

    move-object v3, v1

    .line 132
    check-cast v3, Lcom/google/android/apps/books/playcards/UploadDocument;

    .line 133
    .local v3, "uploadDoc":Lcom/google/android/apps/books/playcards/UploadDocument;
    invoke-virtual {v3}, Lcom/google/android/apps/books/playcards/UploadDocument;->getUploadId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    goto :goto_0

    .end local v0    # "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v1    # "doc":Ljava/lang/Object;
    .end local v3    # "uploadDoc":Lcom/google/android/apps/books/playcards/UploadDocument;
    .end local v5    # "view":Landroid/view/View;
    .end local v6    # "viewGroup":Landroid/view/ViewGroup;
    :cond_3
    move-object v0, v7

    .line 137
    goto :goto_0
.end method

.method public static getHeaderBottomMargin(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static getMinimumHeaderHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-static {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method private inflateFilter(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Lcom/google/android/apps/books/app/LibraryFilter;)V
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "filter"    # Lcom/google/android/apps/books/app/LibraryFilter;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 331
    const v4, 0x7f04006b

    invoke-virtual {p2, v4, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    .line 333
    .local v3, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 335
    new-instance v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/widget/HeaderGridViewController;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    .line 337
    .local v0, "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v4, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    const v4, 0x7f04006a

    invoke-virtual {p2, v4, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 341
    .local v1, "header":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 342
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v4, -0x1

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 343
    iget-object v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getMinimumHeaderHeight(Landroid/content/Context;)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 344
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 346
    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->addHeaderView(Landroid/view/View;)V

    .line 347
    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->setAccessibilitySkipCount(I)V

    .line 349
    sget-object v4, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    if-ne p3, v4, :cond_0

    .line 350
    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .line 351
    invoke-virtual {p0, p2, v3, v5}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getEmptyLibraryView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mMyLibraryEmptyView:Landroid/view/View;

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->maybeShowOrHideEmptyView()V

    .line 354
    :cond_0
    return-void
.end method

.method private setLibraryFilter()V
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getMyLibraryFilter()Lcom/google/android/apps/books/app/LibraryFilter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 367
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->resetCardsAdapterData()V

    .line 368
    return-void
.end method

.method private setupFilter(Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/apps/books/app/LibraryFilter;Landroid/view/ViewGroup;)V
    .locals 7
    .param p1, "controller"    # Lcom/google/android/apps/books/widget/BooksHomeController;
    .param p2, "filter"    # Lcom/google/android/apps/books/app/LibraryFilter;
    .param p3, "cardsContainer"    # Landroid/view/ViewGroup;

    .prologue
    .line 179
    iget-object v6, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-interface {p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getFilteredCardDataList(Lcom/google/android/apps/books/app/LibraryFilter;)Ljava/util/List;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;)V

    invoke-interface {v6, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, p3, v0, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->inflateFilter(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Lcom/google/android/apps/books/app/LibraryFilter;)V

    .line 184
    return-void
.end method

.method private updateRentalBadgesForController(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
    .locals 1
    .param p1, "controller"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 424
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 425
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 427
    :cond_0
    return-void
.end method


# virtual methods
.method public acknowledgeUploadFailure(Ljava/lang/String;)V
    .locals 1
    .param p1, "uploadId"    # Ljava/lang/String;

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/UploadsController;->cancelUpload(Ljava/lang/String;)V

    .line 436
    return-void
.end method

.method public getBooksDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method public getContentView(Lcom/google/android/apps/books/app/LibraryFilter;)Landroid/support/v7/widget/RecyclerView;
    .locals 2
    .param p1, "filter"    # Lcom/google/android/apps/books/app/LibraryFilter;

    .prologue
    .line 191
    iget-object v1, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .line 192
    .local v0, "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    if-nez v0, :cond_0

    .line 292
    new-instance v0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;-><init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method public getFilter(Landroid/view/View;)Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 3
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 197
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 198
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 199
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/LibraryFilter;

    .line 202
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;>;"
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected getFilteredCardDataList(Lcom/google/android/apps/books/app/LibraryFilter;)Ljava/util/List;
    .locals 1
    .param p1, "filter"    # Lcom/google/android/apps/books/app/LibraryFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mCardData:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/CollectionUtils;->filterToList(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->postProcessVolumeData(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHelpContext()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "mobile_library_all"

    .line 102
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const-string v0, "mobile_library_purchases"

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    const-string v0, "mobile_library_rentals"

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    const-string v0, "mobile_library_uploads"

    goto :goto_0

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->SAMPLES:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    const-string v0, "mobile_library_samples"

    goto :goto_0

    .line 102
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getLibraryFilter()Lcom/google/common/base/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method

.method public getSnapshottableView()Landroid/view/View;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getSortComparator()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getLibrarySortOrder()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v0

    return-object v0
.end method

.method protected isVolumeAboutToBeDeleted(Ljava/lang/String;)Z
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 315
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->volumeDownloadAnimationInProgress(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 316
    const/4 v2, 0x1

    .line 319
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected maybeShowOrHideEmptyView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    if-nez v2, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->isLibraryEmpty()Z

    move-result v1

    .line 407
    .local v1, "shouldShowEmptyView":Z
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderViewsCount()I

    move-result v0

    .line 409
    .local v0, "headerViewCount":I
    if-nez v1, :cond_2

    if-le v0, v3, :cond_2

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mMyLibraryEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->removeHeaderView(Landroid/view/View;)Z

    goto :goto_0

    .line 411
    :cond_2
    if-eqz v1, :cond_0

    if-ne v0, v3, :cond_0

    .line 412
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mMyLibraryEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->addHeaderView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public moveToHome()V
    .locals 4

    .prologue
    .line 212
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .line 213
    .local v0, "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_0

    .line 215
    .end local v0    # "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    :cond_0
    return-void
.end method

.method protected onCardsDataChanged()V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->onCardsDataChanged()V

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->alreadyFetchedMyEbooks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->maybeShowOrHideEmptyView()V

    .line 397
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 235
    return-void
.end method

.method public onDeviceConnectionChanged(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    .line 324
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->onDeviceConnectionChanged(Z)V

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 326
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 328
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :cond_0
    return-void
.end method

.method public onHomeFragmentLibraryFilterChanged()V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setLibraryFilter()V

    .line 363
    return-void
.end method

.method protected resetCardsAdapterData()V
    .locals 6

    .prologue
    .line 240
    iget-object v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 241
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .line 242
    .local v0, "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/LibraryFilter;

    .line 243
    .local v2, "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getFilteredCardDataList(Lcom/google/android/apps/books/app/LibraryFilter;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->resetCardData(Ljava/util/List;)V

    .line 244
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    if-nez v4, :cond_0

    .line 245
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setCardsAdapter(Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    goto :goto_0

    .line 248
    .end local v0    # "controller":Lcom/google/android/apps/books/widget/HeaderGridViewController;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;>;"
    .end local v2    # "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    :cond_1
    return-void
.end method

.method public setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fraction"    # F

    .prologue
    .line 219
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 220
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V

    goto :goto_0

    .line 222
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :cond_0
    return-void
.end method

.method protected setCardsAdapter(Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
    .locals 11
    .param p1, "filter"    # Lcom/google/android/apps/books/app/LibraryFilter;
    .param p2, "controller"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 256
    iget-object v9, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v9, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 257
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    iget-object v9, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 258
    .local v7, "res":Landroid/content/res/Resources;
    const v9, 0x7f0900fa

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 260
    .local v3, "minCellWidth":I
    const v9, 0x7f0900f7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 262
    .local v2, "extraRowPadding":I
    const v9, 0x7f0900f6

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 264
    .local v1, "extraCellPadding":I
    const v9, 0x7f0c000e

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 266
    .local v4, "minColumns":I
    invoke-static {v7, v3, v2, v1, v4}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getCardsGridColumnCount(Landroid/content/res/Resources;IIII)I

    move-result v5

    .line 269
    .local v5, "numColumns":I
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setOptimizedForScrollingEnabled(Z)V

    .line 271
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v6

    .line 272
    .local v6, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v9

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v6, v2, v9, v2, v10}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 274
    invoke-virtual {p2, v5}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->setColumnCount(I)V

    .line 276
    new-instance v8, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;

    invoke-direct {v8, p0, v0, v1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;-><init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;Landroid/widget/Adapter;I)V

    .line 286
    .local v8, "wrapAdapter":Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;
    invoke-virtual {p2, v8}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 287
    return-void
.end method

.method public setCardsData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->setCardsData(Ljava/util/List;)V

    .line 208
    return-void
.end method

.method public updateRentalBadges()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mAllBooksController:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->updateRentalBadgesForController(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->updateRentalBadgesForController(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToController:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->updateRentalBadgesForController(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    .line 421
    return-void
.end method

.method public volumeDownloadFractionAnimationDone(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 226
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 227
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->volumeDownloadFractionAnimationDone(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :cond_0
    return-void
.end method
