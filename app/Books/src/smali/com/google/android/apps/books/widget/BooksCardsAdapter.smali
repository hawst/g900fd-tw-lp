.class Lcom/google/android/apps/books/widget/BooksCardsAdapter;
.super Lcom/google/android/apps/books/widget/BaseCardsAdapter;
.source "BooksCardsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/DescribingListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BooksCardsAdapter$6;,
        Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;,
        Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;
    }
.end annotation


# static fields
.field public static final CARD_MEDIUM_16x9_COVER_ONLY:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MINI_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private final mAnimatingDownloadFractions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final mBookClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

.field private final mCoverAnimationEnabled:Z

.field private final mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private final mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

.field private mMaximumViewsToDisplay:I

.field private mOptimizedForScrollingEnabled:Z

.field private final mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

.field private final mPinOnClickListener:Landroid/view/View$OnClickListener;

.field private mPinsAreTouchable:Z

.field private mUseCoverOnlyCards:Z

.field private final mVolumeIdToPinView:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/books/widget/BooksDownloadStatusView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const v4, 0x3fb872b0    # 1.441f

    const/4 v2, 0x4

    .line 91
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040038

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->CARD_MEDIUM_16x9_COVER_ONLY:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 95
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04003a

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->CARD_SMALL_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 100
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040039

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->CARD_MINI_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;
    .param p4, "homeController"    # Lcom/google/android/apps/books/widget/BooksHomeController;
    .param p5, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;",
            "Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;",
            "Lcom/google/android/apps/books/widget/BooksHomeController;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCoverAnimationEnabled:Z

    .line 115
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mMaximumViewsToDisplay:I

    .line 117
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mAnimatingDownloadFractions:Ljava/util/Map;

    .line 118
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    .line 123
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$1;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mPinOnClickListener:Landroid/view/View$OnClickListener;

    .line 135
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$2;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mBookClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 625
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/widget/BooksCardsAdapter$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 147
    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    .line 148
    iput-object p4, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    .line 149
    iput-object p5, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    .line 150
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)Lcom/google/android/apps/books/widget/BooksHomeController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->onFetchedCoverImage()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinViewForVolume(Ljava/lang/String;Z)V

    return-void
.end method

.method private bindToVolume(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 18
    .param p1, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 336
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v17

    .line 337
    .local v17, "volumeId":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getBookDocument(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;

    move-result-object v3

    .line 338
    .local v3, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v12

    .line 339
    .local v12, "context":Landroid/content/Context;
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setVolumeId(Ljava/lang/String;)V

    .line 340
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/playcards/BookDocument;->setTitle(Ljava/lang/String;)V

    .line 341
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/playcards/BookDocument;->setSubTitle(Ljava/lang/String;)V

    .line 342
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/playcards/BookDocument;->setThumbnailURI(Landroid/net/Uri;)V

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->isAvailableForReading(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/playcards/BookDocument;->setIsAvailable(Z)V

    .line 345
    move-object/from16 v0, p2

    invoke-static {v12, v0}, Lcom/google/android/apps/books/util/RentalUtils;->getBannerText(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;

    move-result-object v10

    .line 346
    .local v10, "bannerText":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 347
    invoke-virtual {v3, v10}, Lcom/google/android/apps/books/playcards/BookDocument;->setPrice(Ljava/lang/String;)V

    .line 350
    :cond_0
    const v2, 0x7f0e00e4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 351
    .local v13, "errorOverlay":Landroid/widget/ImageView;
    if-eqz v13, :cond_1

    .line 352
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 355
    :cond_1
    const v2, 0x7f0e00e2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    .line 357
    .local v14, "pinView":Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    if-eqz v14, :cond_2

    .line 358
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->createOrGrabPinViewReference(Lcom/google/android/apps/books/widget/BooksDownloadStatusView;)Ljava/lang/ref/WeakReference;

    move-result-object v16

    .line 360
    .local v16, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setDocumentId(Ljava/lang/String;)V

    .line 366
    invoke-static {v12}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v14, v2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinView(Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksDownloadStatusView;Z)V

    .line 368
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mPinsAreTouchable:Z

    if-eqz v2, :cond_2

    .line 369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mPinOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v2}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    .end local v16    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    :cond_2
    const v2, 0x7f0e00e3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ProgressBar;

    .line 374
    .local v15, "progressBar":Landroid/widget/ProgressBar;
    if-eqz v15, :cond_3

    .line 375
    const/16 v2, 0x8

    invoke-virtual {v15, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 378
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->getVolumeContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/util/BookCoverImageProvider;

    new-instance v2, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/model/VolumeData;)V

    invoke-direct {v5, v2}, Lcom/google/android/apps/books/util/BookCoverImageProvider;-><init>(Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;)V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    const/4 v8, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;ZLcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 397
    new-instance v2, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mBookClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    invoke-direct {v2, v12, v3, v4}, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;-><init>(Landroid/content/Context;Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v12, v2, v4, v5}, Lcom/google/android/apps/books/util/AccessibilityUtils;->getVolumeContentDescription(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 402
    .local v11, "contentDescriptionBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p2

    invoke-static {v12, v0}, Lcom/google/android/apps/books/util/RentalUtils;->getBannerDescription(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;

    move-result-object v9

    .line 403
    .local v9, "bannerDescription":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 404
    const-string v2, ", "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 408
    return-void
.end method

.method private createOrGrabPinViewReference(Lcom/google/android/apps/books/widget/BooksDownloadStatusView;)Ljava/lang/ref/WeakReference;
    .locals 3
    .param p1, "view"    # Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/BooksDownloadStatusView;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/books/widget/BooksDownloadStatusView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 509
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getDocumentId()Ljava/lang/String;

    move-result-object v1

    .line 510
    .local v1, "volumeId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 511
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 513
    .local v0, "removed":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 517
    .end local v0    # "removed":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getBookDocument(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 521
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 522
    .local v1, "doc":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/google/android/apps/books/playcards/BookDocument;

    if-nez v2, :cond_1

    .line 523
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/playcards/BookDocument;

    .end local v1    # "doc":Ljava/lang/Object;
    invoke-direct {v1}, Lcom/google/android/apps/books/playcards/BookDocument;-><init>()V

    .line 524
    .local v1, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .end local v1    # "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    :cond_1
    move-object v0, v1

    .line 526
    check-cast v0, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 528
    .local v0, "bookDoc":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual {v0}, Lcom/google/android/apps/books/playcards/BookDocument;->reset()V

    .line 529
    return-object v0
.end method

.method public static getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 491
    sget-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->CARD_SMALL_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method private getCardMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p1, "cardPosition"    # I

    .prologue
    .line 482
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mUseCoverOnlyCards:Z

    if-eqz v0, :cond_0

    .line 483
    sget-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->CARD_MEDIUM_16x9_COVER_ONLY:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 485
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    goto :goto_0
.end method

.method private getUploadDocument(Landroid/view/View;)Lcom/google/android/apps/books/playcards/UploadDocument;
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 533
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/model/Document;

    .line 534
    .local v0, "doc":Lcom/google/android/ublib/cardlib/model/Document;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/apps/books/playcards/UploadDocument;

    if-nez v1, :cond_1

    .line 535
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/playcards/UploadDocument;

    .end local v0    # "doc":Lcom/google/android/ublib/cardlib/model/Document;
    invoke-direct {v0}, Lcom/google/android/apps/books/playcards/UploadDocument;-><init>()V

    .line 536
    .restart local v0    # "doc":Lcom/google/android/ublib/cardlib/model/Document;
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 538
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/model/Document;->reset()V

    .line 539
    check-cast v0, Lcom/google/android/apps/books/playcards/UploadDocument;

    .end local v0    # "doc":Lcom/google/android/ublib/cardlib/model/Document;
    return-object v0
.end method

.method private static getUploadStatusString(Landroid/content/res/Resources;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Ljava/lang/String;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "status"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 648
    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsAdapter$6;->$SwitchMap$com$google$android$apps$books$upload$proto$UploadProto$Upload$UploadStatus:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 674
    :pswitch_0
    const/4 v1, 0x0

    .line 676
    :goto_0
    return-object v1

    .line 650
    :pswitch_1
    const v0, 0x7f0f01db

    .line 676
    .local v0, "resourceId":I
    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 654
    .end local v0    # "resourceId":I
    :pswitch_2
    const v0, 0x7f0f01dc

    .line 655
    .restart local v0    # "resourceId":I
    goto :goto_1

    .line 658
    .end local v0    # "resourceId":I
    :pswitch_3
    const v0, 0x7f0f01de

    .line 659
    .restart local v0    # "resourceId":I
    goto :goto_1

    .line 664
    .end local v0    # "resourceId":I
    :pswitch_4
    const v0, 0x7f0f01df

    .line 665
    .restart local v0    # "resourceId":I
    goto :goto_1

    .line 668
    .end local v0    # "resourceId":I
    :pswitch_5
    const v0, 0x7f0f01dd

    .line 669
    .restart local v0    # "resourceId":I
    goto :goto_1

    .line 648
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private getVisiblePinView(Ljava/lang/String;)Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 502
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 503
    .local v0, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onFetchedCoverImage()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->onCoverImageLoaded()V

    .line 154
    return-void
.end method

.method private setErrorOverlay(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;Landroid/content/res/Resources;)V
    .locals 7
    .param p1, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "upload"    # Lcom/google/android/apps/books/upload/Upload;
    .param p3, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 284
    const v1, 0x7f0e00e4

    invoke-virtual {p1, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 285
    .local v6, "errorOverlay":Landroid/widget/ImageView;
    if-nez v6, :cond_0

    .line 333
    :goto_0
    return-void

    .line 288
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    if-eq v1, v2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    if-eq v1, v2, :cond_1

    .line 290
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 295
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    if-ne v1, v2, :cond_2

    .line 296
    const v3, 0x7f0f01e1

    .line 297
    .local v3, "dialogMessage":I
    const/4 v5, 0x0

    .line 298
    .local v5, "neutralButton":I
    const v4, 0x104000a

    .line 304
    .local v4, "positiveButton":I
    :goto_1
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/upload/Upload;III)V

    .line 331
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 300
    .end local v0    # "clickListener":Landroid/view/View$OnClickListener;
    .end local v3    # "dialogMessage":I
    .end local v4    # "positiveButton":I
    .end local v5    # "neutralButton":I
    :cond_2
    const v3, 0x7f0f01e2

    .line 301
    .restart local v3    # "dialogMessage":I
    const v5, 0x7f0f01e3

    .line 302
    .restart local v5    # "neutralButton":I
    const v4, 0x7f0f01e4

    .restart local v4    # "positiveButton":I
    goto :goto_1
.end method

.method private updatePinView(Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksDownloadStatusView;Z)V
    .locals 11
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinView"    # Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    .param p3, "deviceIsConnected"    # Z

    .prologue
    const/4 v8, 0x0

    .line 563
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    invoke-interface {v9, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->isPinned(Ljava/lang/String;)Z

    move-result v5

    .line 566
    .local v5, "pinned":Z
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mAnimatingDownloadFractions:Ljava/util/Map;

    invoke-interface {v9, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 567
    .local v0, "animatingFraction":Ljava/lang/Float;
    if-eqz v0, :cond_1

    .line 568
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 569
    .local v3, "fraction":F
    const/4 v4, 0x0

    .line 593
    .local v4, "onlineOnly":Z
    :goto_0
    invoke-virtual {p2, v3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setDownloadFraction(F)V

    .line 595
    invoke-virtual {p2, v5}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setDownloadRequested(Z)V

    .line 596
    invoke-virtual {p2, p3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setOnline(Z)V

    .line 601
    if-nez v4, :cond_6

    iget-boolean v9, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mPinsAreTouchable:Z

    if-nez v9, :cond_0

    if-nez v5, :cond_0

    const/4 v9, 0x0

    cmpl-float v9, v3, v9

    if-lez v9, :cond_6

    :cond_0
    :goto_1
    invoke-virtual {p2, v8}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setVisibility(I)V

    .line 603
    return-void

    .line 571
    .end local v3    # "fraction":F
    .end local v4    # "onlineOnly":Z
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v9}, Lcom/google/android/apps/books/widget/BooksHomeController;->getDataCache()Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    move-result-object v1

    .line 572
    .local v1, "cache":Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getLicenseAction(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-ne v9, v10, :cond_3

    const/4 v2, 0x1

    .line 573
    .local v2, "deleteContent":Z
    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v9, p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v7

    .line 576
    .local v7, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    if-nez v7, :cond_4

    .line 577
    const/4 v4, 0x0

    .line 578
    .restart local v4    # "onlineOnly":Z
    const/4 v6, 0x1

    .line 585
    .local v6, "usesEolm":Z
    :goto_3
    if-nez v2, :cond_2

    if-eqz v6, :cond_5

    if-nez v5, :cond_5

    .line 586
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "fraction":F
    goto :goto_0

    .end local v2    # "deleteContent":Z
    .end local v3    # "fraction":F
    .end local v4    # "onlineOnly":Z
    .end local v6    # "usesEolm":Z
    .end local v7    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_3
    move v2, v8

    .line 572
    goto :goto_2

    .line 580
    .restart local v2    # "deleteContent":Z
    .restart local v7    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_4
    invoke-static {v7}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isOnlineOnly(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v4

    .line 581
    .restart local v4    # "onlineOnly":Z
    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v6

    .restart local v6    # "usesEolm":Z
    goto :goto_3

    .line 588
    :cond_5
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getDownloadFraction(Ljava/lang/String;)F

    move-result v3

    .restart local v3    # "fraction":F
    goto :goto_0

    .line 601
    .end local v1    # "cache":Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    .end local v2    # "deleteContent":Z
    .end local v6    # "usesEolm":Z
    .end local v7    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_6
    const/16 v8, 0x8

    goto :goto_1
.end method

.method private updatePinViewForVolume(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "deviceIsConnected"    # Z

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getVisiblePinView(Ljava/lang/String;)Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-result-object v0

    .line 496
    .local v0, "pinView":Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    if-eqz v0, :cond_0

    .line 497
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinView(Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksDownloadStatusView;Z)V

    .line 499
    :cond_0
    return-void
.end method


# virtual methods
.method public bindToUpload(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;)V
    .locals 12
    .param p1, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 221
    invoke-virtual {p1, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getUploadDocument(Landroid/view/View;)Lcom/google/android/apps/books/playcards/UploadDocument;

    move-result-object v1

    .line 223
    .local v1, "doc":Lcom/google/android/apps/books/playcards/UploadDocument;
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/playcards/UploadDocument;->setUploadId(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/playcards/UploadDocument;->setTitle(Ljava/lang/String;)V

    .line 227
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/playcards/UploadDocument;->setThumbnailURI(Landroid/net/Uri;)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 230
    .local v10, "res":Landroid/content/res/Resources;
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getUploadStatusString(Landroid/content/res/Resources;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Ljava/lang/String;

    move-result-object v11

    .line 231
    .local v11, "uploadStatus":Ljava/lang/String;
    invoke-virtual {v1, v11}, Lcom/google/android/apps/books/playcards/UploadDocument;->setPrice(Ljava/lang/String;)V

    .line 233
    invoke-direct {p0, p1, p2, v10}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setErrorOverlay(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;Landroid/content/res/Resources;)V

    .line 235
    const v0, 0x7f0e00e2

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    .line 237
    .local v8, "pinView":Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    if-eqz v8, :cond_0

    .line 238
    invoke-virtual {v8, v6}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setVisibility(I)V

    .line 241
    :cond_0
    const v0, 0x7f0e00e3

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ProgressBar;

    .line 242
    .local v9, "progressBar":Landroid/widget/ProgressBar;
    sget-object v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$6;->$SwitchMap$com$google$android$apps$books$upload$proto$UploadProto$Upload$UploadStatus:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 254
    invoke-virtual {v9, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 255
    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getUploadPercentage()I

    move-result v0

    invoke-virtual {v9, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 256
    invoke-virtual {v9, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 259
    :goto_0
    new-instance v3, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;

    invoke-direct {v3, p0, v10}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Landroid/content/res/Resources;)V

    .line 276
    .local v3, "imageProvider":Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->getUploadContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    move-result-object v2

    move-object v0, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;ZLcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 278
    if-nez v11, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v7

    .line 280
    .local v7, "description":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1, v7}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 281
    return-void

    .line 246
    .end local v3    # "imageProvider":Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .end local v7    # "description":Ljava/lang/String;
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 247
    invoke-virtual {v9, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 251
    :pswitch_1
    invoke-virtual {v9, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 278
    .restart local v3    # "imageProvider":Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mMaximumViewsToDisplay:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/CardData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LongStringHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/CardData;

    .line 195
    .local v2, "data":Lcom/google/android/apps/books/widget/CardData;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getCardMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v3

    .line 197
    .local v3, "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    move-object v0, p2

    .line 198
    .local v0, "cardContainer":Landroid/view/View;
    if-nez v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 203
    :cond_0
    const v4, 0x7f0e00c9

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 205
    .local v1, "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 206
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getFillStyle()Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 207
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setCoverAnimationEnabled(Z)V

    .line 208
    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mOptimizedForScrollingEnabled:Z

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOptimizedForScrollingEnabled(Z)V

    .line 210
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 211
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v4

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->bindToVolume(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 213
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 214
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/CardData;->getUploadData()Lcom/google/android/apps/books/upload/Upload;

    move-result-object v4

    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->bindToUpload(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;)V

    .line 217
    :cond_2
    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public resetCardData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "cardData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setNotifyOnChange(Z)V

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->clear()V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 458
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->addAll(Ljava/util/Collection;)V

    .line 460
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->notifyDataSetChanged()V

    .line 461
    return-void
.end method

.method public setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fraction"    # F

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mAnimatingDownloadFractions:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinViewForVolume(Ljava/lang/String;Z)V

    .line 466
    return-void
.end method

.method public setMaximumViewsToDisplay(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 478
    iput p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mMaximumViewsToDisplay:I

    .line 479
    return-void
.end method

.method public setOptimizedForScrollingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 643
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mOptimizedForScrollingEnabled:Z

    .line 644
    return-void
.end method

.method public setPinsAreTouchable(Z)V
    .locals 0
    .param p1, "touchable"    # Z

    .prologue
    .line 639
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mPinsAreTouchable:Z

    .line 640
    return-void
.end method

.method public setUseCoverOnlyCards(Z)V
    .locals 0
    .param p1, "useCoverOnlyCards"    # Z

    .prologue
    .line 635
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mUseCoverOnlyCards:Z

    .line 636
    return-void
.end method

.method public setupExistingView(Landroid/view/View;I)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 421
    const v5, 0x7f0e00e2

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    .line 423
    .local v3, "pinView":Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
    if-eqz v3, :cond_0

    .line 424
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->createOrGrabPinViewReference(Lcom/google/android/apps/books/widget/BooksDownloadStatusView;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    .line 426
    .local v4, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mVolumeIdToPinView:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getDocumentId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    .end local v4    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/apps/books/widget/BooksDownloadStatusView;>;"
    :cond_0
    instance-of v5, p1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    if-eqz v5, :cond_1

    move-object v0, p1

    .line 432
    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 433
    .local v0, "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getDocument()Lcom/google/android/ublib/cardlib/model/Document;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 434
    .local v2, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    if-eqz v2, :cond_1

    .line 435
    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->isAvailableForReading(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->updateAvailability(Z)V

    .line 437
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/CardData;

    .line 438
    .local v1, "data":Lcom/google/android/apps/books/widget/CardData;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/apps/books/util/RentalUtils;->getBannerText(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/playcards/BookDocument;->setPrice(Ljava/lang/String;)V

    .line 440
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->updatePrice()V

    .line 444
    .end local v0    # "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v1    # "data":Lcom/google/android/apps/books/widget/CardData;
    .end local v2    # "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    :cond_1
    return-void
.end method

.method public volumeDownloadAnimationInProgress(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mAnimatingDownloadFractions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public volumeDownloadFractionAnimationDone(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mAnimatingDownloadFractions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinViewForVolume(Ljava/lang/String;Z)V

    .line 471
    return-void
.end method
