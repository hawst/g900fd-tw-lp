.class Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
.super Ljava/lang/Object;
.source "TextToSpeechController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/tts/TextToSpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PhraseSearchResult"
.end annotation


# instance fields
.field final phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

.field final reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 898
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 899
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 1
    .param p1, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 891
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 892
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V
    .locals 1
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 888
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 889
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 0
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .param p2, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 894
    iput-object p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 895
    iput-object p2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .line 896
    return-void
.end method
