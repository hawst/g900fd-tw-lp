.class public Lcom/google/android/apps/books/data/NetworkTaskQueue;
.super Ljava/lang/Object;
.source "NetworkTaskQueue.java"

# interfaces
.implements Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;,
        Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;
    }
.end annotation


# instance fields
.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMaxConcurrentBackgroundTasks:I

.field private final mMaxConcurrentTasks:I

.field private mNextTaskSequenceNumber:I

.field private mNextVolumeBoostSequenceNumber:I

.field private final mServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

.field private mTasks:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mThreadPool:Lcom/google/android/apps/books/util/LazyThreadPool;

.field private final mVolumeIdToOperationSequenceNumber:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/NetworkTaskServices;IILcom/google/android/apps/books/util/Logger;)V
    .locals 3
    .param p1, "controlExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "workerExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p4, "maxConcurrentBackgroundTasks"    # I
    .param p5, "maxConcurrentTasks"    # I
    .param p6, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mVolumeIdToOperationSequenceNumber:Lcom/google/common/collect/BiMap;

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->makeQueue(Ljava/util/Map;I)Ljava/util/Queue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    .line 50
    iput v2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextVolumeBoostSequenceNumber:I

    .line 55
    iput v2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextTaskSequenceNumber:I

    .line 74
    iput-object p3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    .line 75
    iput p4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentBackgroundTasks:I

    .line 76
    iput p5, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentTasks:I

    .line 77
    iput-object p6, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 78
    new-instance v0, Lcom/google/android/apps/books/util/LazyThreadPool;

    invoke-direct {v0, p0, p1, p2, p6}, Lcom/google/android/apps/books/util/LazyThreadPool;-><init>(Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/util/Logger;)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mThreadPool:Lcom/google/android/apps/books/util/LazyThreadPool;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/NetworkTaskQueue;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/NetworkTaskQueue;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/NetworkTaskQueue;)Lcom/google/android/apps/books/data/NetworkTaskServices;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/NetworkTaskQueue;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    return-object v0
.end method

.method private makeQueue(Ljava/util/Map;I)Ljava/util/Queue;
    .locals 2
    .param p2, "initialCapacity"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;I)",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "volumeIdToOperationSequenceNumber":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/PriorityQueue;

    new-instance v1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;-><init>(Lcom/google/android/apps/books/data/NetworkTaskQueue;Ljava/util/Map;)V

    invoke-direct {v0, p2, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public addTask(Lcom/google/android/apps/books/data/NetworkTask;)V
    .locals 4
    .param p1, "task"    # Lcom/google/android/apps/books/data/NetworkTask;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    new-instance v1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;

    iget v2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextTaskSequenceNumber:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextTaskSequenceNumber:I

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;-><init>(Lcom/google/android/apps/books/data/NetworkTask;I)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mThreadPool:Lcom/google/android/apps/books/util/LazyThreadPool;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->tickleOnControlExecutor()V

    .line 85
    return-void
.end method

.method public boostVolumePriority(Ljava/lang/String;)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v5, "NetworkTaskQueue"

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 93
    iget-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v5, "NetworkTaskQueue"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Boosting priority for volume "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextVolumeBoostSequenceNumber:I

    add-int/lit8 v4, v0, 0x1

    iput v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mNextVolumeBoostSequenceNumber:I

    .line 99
    .local v0, "boostSequenceNumber":I
    iget-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mVolumeIdToOperationSequenceNumber:Lcom/google/common/collect/BiMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mVolumeIdToOperationSequenceNumber:Lcom/google/common/collect/BiMap;

    invoke-static {v4}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v3

    .line 107
    .local v3, "volumeIdToOperationSequenceNumberCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    .line 108
    .local v2, "oldQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;>;"
    const/4 v4, 0x1

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 109
    .local v1, "initialCapacity":I
    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->makeQueue(Ljava/util/Map;I)Ljava/util/Queue;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    .line 110
    iget-object v4, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    invoke-interface {v4, v2}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 111
    return-void
.end method

.method public poll(I)Ljava/lang/Runnable;
    .locals 7
    .param p1, "numRunningTasks"    # I

    .prologue
    const/4 v2, 0x0

    .line 179
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "NetworkTaskQueue"

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v0

    .line 180
    .local v0, "shouldLog":Z
    iget v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentTasks:I

    if-lt p1, v3, :cond_1

    .line 182
    if-eqz v0, :cond_0

    .line 183
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "NetworkTaskQueue"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentTasks:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " already running: returning null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    :goto_0
    return-object v2

    .line 188
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;

    .line 189
    .local v1, "task":Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;
    if-eqz v1, :cond_3

    .line 190
    iget-object v3, v1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    invoke-virtual {v3}, Lcom/google/android/apps/books/data/NetworkTask;->getPriority()Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentBackgroundTasks:I

    if-lt p1, v3, :cond_2

    .line 193
    if-eqz v0, :cond_0

    .line 194
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "NetworkTaskQueue"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mMaxConcurrentBackgroundTasks:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " already running: returning null instead of background task"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mTasks:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 201
    new-instance v2, Lcom/google/android/apps/books/data/NetworkTaskQueue$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/data/NetworkTaskQueue$1;-><init>(Lcom/google/android/apps/books/data/NetworkTaskQueue;Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;)V

    goto :goto_0

    .line 208
    :cond_3
    if-eqz v0, :cond_0

    .line 209
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "NetworkTaskQueue"

    const-string v5, "No more tasks"

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
