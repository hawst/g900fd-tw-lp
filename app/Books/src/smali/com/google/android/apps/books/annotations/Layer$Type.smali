.class public final enum Lcom/google/android/apps/books/annotations/Layer$Type;
.super Ljava/lang/Enum;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/Layer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/annotations/Layer$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/annotations/Layer$Type;

.field public static final enum USER:Lcom/google/android/apps/books/annotations/Layer$Type;

.field public static final enum VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$Type;

    const-string v1, "USER"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/Layer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/Layer$Type;->USER:Lcom/google/android/apps/books/annotations/Layer$Type;

    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$Type;

    const-string v1, "VOLUME"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/annotations/Layer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/annotations/Layer$Type;

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$Type;->USER:Lcom/google/android/apps/books/annotations/Layer$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/annotations/Layer$Type;->$VALUES:[Lcom/google/android/apps/books/annotations/Layer$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/annotations/Layer$Type;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/apps/books/annotations/Layer$Type;->$VALUES:[Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/annotations/Layer$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/annotations/Layer$Type;

    return-object v0
.end method
