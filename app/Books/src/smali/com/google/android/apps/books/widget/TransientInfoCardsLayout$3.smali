.class Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCanceled:Z

.field final synthetic this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

.field final synthetic val$cardsView:Landroid/view/View;

.field final synthetic val$state:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->val$state:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object p3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->val$cardsView:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 793
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->mCanceled:Z

    .line 794
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 798
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->mCanceled:Z

    if-nez v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->val$state:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsStateNoTransition(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;->val$cardsView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationY(Landroid/view/View;F)V

    .line 802
    :cond_0
    return-void
.end method
