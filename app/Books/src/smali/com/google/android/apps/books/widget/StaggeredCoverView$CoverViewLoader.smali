.class Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;
.super Ljava/lang/Object;
.source "StaggeredCoverView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CoverViewLoader"
.end annotation


# instance fields
.field final mLoadCoversTask:Ljava/lang/Runnable;

.field final mViewsNeedingCovers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/widget/BookCoverContainerView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 1

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mViewsNeedingCovers:Ljava/util/Set;

    .line 130
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mLoadCoversTask:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;Lcom/google/android/apps/books/widget/StaggeredCoverView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/StaggeredCoverView$1;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    return-void
.end method


# virtual methods
.method public finishLoadingCovers()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mViewsNeedingCovers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mLoadCoversTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->post(Ljava/lang/Runnable;)Z

    .line 160
    :cond_0
    return-void
.end method

.method public loadCover(Lcom/google/android/apps/books/widget/BookCoverContainerView;Z)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .param p2, "inLayout"    # Z

    .prologue
    .line 141
    if-nez p2, :cond_0

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->loadCoverView(Z)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mViewsNeedingCovers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
