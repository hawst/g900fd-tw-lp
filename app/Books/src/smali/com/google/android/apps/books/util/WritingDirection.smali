.class public final enum Lcom/google/android/apps/books/util/WritingDirection;
.super Ljava/lang/Enum;
.source "WritingDirection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/util/WritingDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/util/WritingDirection;

.field public static final enum LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

.field public static final enum RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;


# instance fields
.field private final mSign:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lcom/google/android/apps/books/util/WritingDirection;

    const-string v1, "LEFT_TO_RIGHT"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/apps/books/util/WritingDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    .line 23
    new-instance v0, Lcom/google/android/apps/books/util/WritingDirection;

    const-string v1, "RIGHT_TO_LEFT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/books/util/WritingDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/util/WritingDirection;

    sget-object v1, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/util/WritingDirection;->$VALUES:[Lcom/google/android/apps/books/util/WritingDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sign"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/google/android/apps/books/util/WritingDirection;->mSign:I

    .line 29
    return-void
.end method

.method public static dotProduct(Lcom/google/android/apps/books/util/WritingDirection;I)I
    .locals 1
    .param p0, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;
    .param p1, "length"    # I

    .prologue
    .line 36
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/WritingDirection;->mSign:I

    mul-int/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/google/android/apps/books/util/WritingDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/WritingDirection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->$VALUES:[Lcom/google/android/apps/books/util/WritingDirection;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/util/WritingDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/util/WritingDirection;

    return-object v0
.end method
