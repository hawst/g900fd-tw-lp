.class Lcom/google/android/apps/books/app/HomeFragment$24;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 1969
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$24;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onMyEbooksVolumes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1989
    .local p1, "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    return-void
.end method

.method public onOffersData(Ljava/util/List;ZJ)V
    .locals 7
    .param p2, "fromServer"    # Z
    .param p3, "lastUpdateTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;ZJ)V"
        }
    .end annotation

    .prologue
    .line 1994
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$24$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/HomeFragment$24$1;-><init>(Lcom/google/android/apps/books/app/HomeFragment$24;Ljava/util/List;ZJ)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 2005
    return-void
.end method
