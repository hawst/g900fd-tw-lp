.class Lcom/google/android/apps/books/app/ReaderSettingsController$5;
.super Ljava/lang/Object;
.source "ReaderSettingsController.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderSettingsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$5;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$5;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$5;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onTypefaceChanged(Z)V

    .line 439
    :cond_0
    return-void
.end method
