.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->downloadAnnotationProcessor(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)Lcom/google/android/apps/books/annotations/AnnotationProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mIsReset:Z

.field private mServerIdsNotSeenDuringReset:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUnsyncedDeletedServerIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$lastSyncDate:J

.field final synthetic val$layer:Ljava/lang/String;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 873
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$layer:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$lastSyncDate:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mIsReset:Z

    .line 885
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mServerIdsNotSeenDuringReset:Ljava/util/Set;

    .line 942
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mUnsyncedDeletedServerIds:Ljava/util/List;

    return-void
.end method

.method private expungeFromCache(Ljava/lang/String;)V
    .locals 3
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 968
    if-eqz p1, :cond_0

    .line 969
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    new-instance v2, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;Ljava/lang/String;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 976
    :cond_0
    return-void
.end method

.method private getUnsyncedDeletedServerIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 944
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mUnsyncedDeletedServerIds:Ljava/util/List;

    if-nez v0, :cond_0

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->unsyncedDeletedServerIds()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mUnsyncedDeletedServerIds:Ljava/util/List;

    .line 947
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mUnsyncedDeletedServerIds:Ljava/util/List;

    return-object v0
.end method

.method private locallyApplyDeletionFromServerByServerId(Ljava/lang/String;)V
    .locals 1
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 963
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getLocalIdForServerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->expungeFromCache(Ljava/lang/String;)V

    .line 964
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->expungeServerId(Ljava/lang/String;)V

    .line 965
    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 8
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 918
    iget-boolean v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mIsReset:Z

    if-nez v3, :cond_1

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    iget-wide v4, v3, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    iget-wide v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$lastSyncDate:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1

    const/4 v1, 0x1

    .line 920
    .local v1, "isDefinitelyDuplicate":Z
    :goto_0
    if-nez v1, :cond_0

    .line 921
    iget-object v3, p1, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    iget-object v2, v3, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    .line 922
    .local v2, "serverId":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->getUnsyncedDeletedServerIds()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 939
    .end local v2    # "serverId":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 918
    .end local v1    # "isDefinitelyDuplicate":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 926
    .restart local v1    # "isDefinitelyDuplicate":Z
    .restart local v2    # "serverId":Ljava/lang/String;
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mIsReset:Z

    if-eqz v3, :cond_3

    .line 927
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mServerIdsNotSeenDuringReset:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 929
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getLocalIdForServerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 931
    .local v0, "editedLocalId":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 932
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$layer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/annotations/ServerAnnotation;->withOverriddenLocalId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/ServerAnnotation;

    move-result-object v6

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyEditFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1600(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    goto :goto_1

    .line 935
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$layer:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyAddFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    invoke-static {v3, v4, v5, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1700(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    goto :goto_1
.end method

.method public delete(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 895
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->locallyApplyDeletionFromServerByServerId(Ljava/lang/String;)V

    .line 896
    return-void
.end method

.method public done()V
    .locals 3

    .prologue
    .line 952
    iget-boolean v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mIsReset:Z

    if-eqz v2, :cond_0

    .line 953
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mServerIdsNotSeenDuringReset:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 954
    .local v0, "deletedServerId":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->locallyApplyDeletionFromServerByServerId(Ljava/lang/String;)V

    goto :goto_0

    .line 957
    .end local v0    # "deletedServerId":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 889
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mIsReset:Z

    .line 890
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$layer:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getAllServerIds(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->mServerIdsNotSeenDuringReset:Ljava/util/Set;

    .line 891
    return-void
.end method

.method public updateCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 5
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "quota"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 906
    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 907
    const-string v1, "AnnotationC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to update the character quota for the \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' layer (only the \'copy\' layer is currently updateable)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    :goto_0
    return-void

    .line 911
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/books/annotations/Layer$Type;->USER:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/books/annotations/Layer$Key;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V

    .line 912
    .local v0, "key":Lcom/google/android/apps/books/annotations/Layer$Key;
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    new-instance v2, Lcom/google/android/apps/books/annotations/Layer;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v3, v3, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v0, v3, v4, p2}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer;)V

    goto :goto_0
.end method
