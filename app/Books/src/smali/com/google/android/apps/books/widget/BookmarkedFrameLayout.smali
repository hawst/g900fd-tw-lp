.class public abstract Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;
.super Landroid/widget/FrameLayout;
.source "BookmarkedFrameLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final NO_BOOKMARK:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;


# instance fields
.field private mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

.field private mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field private mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

.field private mCanHaveBookmarksInBothCorners:Z

.field private mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

.field private final mTempMargins:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->NO_BOOKMARK:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canHaveBookmarksInBothCorners"    # Z
    .param p3, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    .line 69
    invoke-virtual {p0, p3, p2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->init(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Z)V

    .line 70
    return-void
.end method

.method private attachBookmarkAnimator(ILcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "bookmarkState"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookmarkView;->reset()V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    aput-object p2, v0, p1

    .line 150
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->onBookmarkStateChanged(I)V

    .line 151
    return-void
.end method

.method private getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mCanHaveBookmarksInBothCorners:Z

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 182
    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0
.end method

.method private maybeUpdateBookmarkViewLayout(IZ)Lcom/google/android/apps/books/widget/BookmarkView;
    .locals 4
    .param p1, "index"    # I
    .param p2, "createIfAbsent"    # Z

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v1

    .line 106
    .local v1, "sideOfSpine":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    aget-object v0, v2, p1

    .line 107
    .local v0, "bookmarkView":Lcom/google/android/apps/books/widget/BookmarkView;
    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->getBookmarkMargins(ILandroid/graphics/Point;)V

    .line 109
    if-nez v0, :cond_2

    .line 110
    new-instance v0, Lcom/google/android/apps/books/widget/BookmarkView;

    .end local v0    # "bookmarkView":Lcom/google/android/apps/books/widget/BookmarkView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/widget/BookmarkView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    .line 111
    .restart local v0    # "bookmarkView":Lcom/google/android/apps/books/widget/BookmarkView;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/BookmarkView;->createFrameLayoutParams(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    aput-object v0, v2, p1

    .line 118
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->onBookmarkLayoutChanged()V

    .line 120
    :cond_1
    return-object v0

    .line 115
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mTempMargins:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/BookmarkView;->createFrameLayoutParams(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 2
    .param p1, "animation"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 141
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->bookmarkViewIndex(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)I

    move-result v0

    .line 142
    .local v0, "index":I
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->attachBookmarkAnimator(ILcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 143
    return-void
.end method

.method public attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V
    .locals 1
    .param p1, "animation"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p2, "displayStyle"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    .line 132
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->bookmarkViewIndex(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)I

    move-result v0

    .line 133
    .local v0, "index":I
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->attachBookmarkAnimator(ILcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 134
    return-void
.end method

.method protected bookmarkViewIndex(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)I
    .locals 2
    .param p1, "displayStyle"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    const/4 v0, 0x0

    .line 205
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mCanHaveBookmarksInBothCorners:Z

    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-eq p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected getBookmarkMargins(ILandroid/graphics/Point;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "result"    # Landroid/graphics/Point;

    .prologue
    const/4 v0, 0x0

    .line 219
    iput v0, p2, Landroid/graphics/Point;->x:I

    .line 220
    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 221
    return-void
.end method

.method protected getBookmarkMeasurements()Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    return-object v0
.end method

.method protected getBookmarkView(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)Landroid/view/View;
    .locals 2
    .param p1, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->bookmarkViewIndex(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hideAllBookmarks()V
    .locals 3

    .prologue
    .line 186
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    array-length v1, v2

    .line 187
    .local v1, "numBookmarkAnimators":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 188
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 189
    sget-object v2, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->NO_BOOKMARK:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->attachBookmarkAnimator(ILcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 187
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_1
    return-void
.end method

.method public init(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Z)V
    .locals 2
    .param p1, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .param p2, "canHaveBookmarksInBothCorners"    # Z

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .line 76
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mCanHaveBookmarksInBothCorners:Z

    .line 77
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mCanHaveBookmarksInBothCorners:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    .line 78
    .local v0, "numBookmarksInView":I
    :goto_0
    new-array v1, v0, [Lcom/google/android/apps/books/widget/BookmarkView;

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkViews:[Lcom/google/android/apps/books/widget/BookmarkView;

    .line 79
    new-array v1, v0, [Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 80
    return-void

    .line 77
    .end local v0    # "numBookmarksInView":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onBookmarkLayoutChanged()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method protected onBookmarkStateChanged(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 159
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 160
    .local v4, "now":J
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    aget-object v0, v3, p1

    .line 161
    .local v0, "bookmarkState":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->isVisibleOnPage(J)Z

    move-result v2

    .line 165
    .local v2, "needBookmarkView":Z
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->maybeUpdateBookmarkViewLayout(IZ)Lcom/google/android/apps/books/widget/BookmarkView;

    move-result-object v1

    .line 166
    .local v1, "bookmarkView":Lcom/google/android/apps/books/widget/BookmarkView;
    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/apps/books/widget/BookmarkView;->onBookmarkChanged(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V

    goto :goto_0
.end method

.method public onBookmarksChanged()V
    .locals 3

    .prologue
    .line 198
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mBookmarkStates:[Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    array-length v1, v2

    .line 199
    .local v1, "numBookmarkAnimators":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 200
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->onBookmarkStateChanged(I)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method

.method public setSideOfSpine(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V
    .locals 2
    .param p1, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne p1, v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->mCanHaveBookmarksInBothCorners:Z

    if-nez v0, :cond_0

    .line 97
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->maybeUpdateBookmarkViewLayout(IZ)Lcom/google/android/apps/books/widget/BookmarkView;

    goto :goto_0
.end method
