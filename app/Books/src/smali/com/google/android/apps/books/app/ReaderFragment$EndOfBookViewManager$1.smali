.class Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->loadPrice()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/PurchaseInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V
    .locals 0

    .prologue
    .line 2609
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 3
    .param p1, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 2612
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$3702(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;Lcom/google/android/apps/books/app/PurchaseInfo;)Lcom/google/android/apps/books/app/PurchaseInfo;

    .line 2613
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdatePriceInScrubBar()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 2614
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$5500(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2615
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$5500(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$3700(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setPrice(Landroid/content/res/Resources;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 2617
    :cond_0
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2609
    check-cast p1, Lcom/google/android/apps/books/app/PurchaseInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;->take(Lcom/google/android/apps/books/app/PurchaseInfo;)V

    return-void
.end method
