.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->fetchCopyQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 501
    const/4 v4, 0x0

    .line 502
    .local v4, "quota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    const/4 v2, 0x0

    .line 504
    .local v2, "error":Ljava/lang/Exception;
    new-instance v3, Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v6, v6, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    sget-object v7, Lcom/google/android/apps/books/annotations/Layer$Type;->USER:Lcom/google/android/apps/books/annotations/Layer$Type;

    sget-object v8, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    invoke-direct {v3, v6, v7, v8}, Lcom/google/android/apps/books/annotations/Layer$Key;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V

    .line 508
    .local v3, "key":Lcom/google/android/apps/books/annotations/Layer$Key;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getCopyQuotaFromServer(Lcom/google/android/apps/books/annotations/Layer$Key;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    invoke-static {v6, v3, v7}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1000(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v4

    .line 510
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    new-instance v7, Lcom/google/android/apps/books/annotations/Layer;

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v8, v8, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    const-string v9, ""

    invoke-direct {v7, v3, v8, v9, v4}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    invoke-static {v6, v7}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 529
    :goto_0
    if-eqz v4, :cond_2

    .line 530
    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v5

    .line 535
    .local v5, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    new-instance v8, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12$1;

    invoke-direct {v8, p0, v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;Lcom/google/android/apps/books/util/ExceptionOr;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 541
    return-void

    .line 511
    .end local v5    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    :catch_0
    move-exception v1

    .line 514
    .local v1, "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLocalLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
    invoke-static {v6, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v0

    .line 515
    .local v0, "copyLayer":Lcom/google/android/apps/books/annotations/Layer;
    if-eqz v0, :cond_0

    .line 516
    iget-object v4, v0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    goto :goto_0

    .line 519
    :cond_0
    move-object v2, v1

    goto :goto_0

    .line 521
    .end local v0    # "copyLayer":Lcom/google/android/apps/books/annotations/Layer;
    .end local v1    # "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    :catch_1
    move-exception v1

    .line 522
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "AnnotationC"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 523
    const-string v6, "AnnotationC"

    const-string v7, "Error fetching character limit"

    invoke-static {v6, v7, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 525
    :cond_1
    move-object v2, v1

    goto :goto_0

    .line 532
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v5

    .restart local v5    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    goto :goto_1
.end method
