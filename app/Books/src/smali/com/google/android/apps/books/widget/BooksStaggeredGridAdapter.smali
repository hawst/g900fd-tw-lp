.class public Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "BooksStaggeredGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$CardFader;
    }
.end annotation


# instance fields
.field private final mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

.field private final mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyView:Landroid/view/View;

.field private mFooter:Landroid/view/View;

.field private mHasFetchedEbooksOnce:Z

.field private mHeader:Landroid/view/View;

.field private final mInfoCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

.field private mInfoView:Landroid/view/View;

.field private mInfoViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private final mOnboardCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

.field private mOnboardView:Landroid/view/View;

.field private mOnboardViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private mShowInfoView:Z

.field private mShowOnboardView:Z

.field private mSpacerView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BaseCardsAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$1;-><init>(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    .line 74
    new-instance v0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$2;-><init>(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    .line 86
    new-instance v0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$3;-><init>(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    new-instance v1, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter$4;-><init>(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)Lcom/google/android/ublib/view/FadeAnimationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getInfoViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)Lcom/google/android/ublib/view/FadeAnimationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getOnboardViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;

    move-result-object v0

    return-object v0
.end method

.method private canShowEmptyView()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHasFetchedEbooksOnce:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canShowInfoView()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowInfoView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canShowOnboardView()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowOnboardView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cardPositionFromAdapterPosition(I)I
    .locals 1
    .param p1, "adapterPosition"    # I

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getHeaderCount()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method private getCoverContainerView(Landroid/view/ViewGroup;Landroid/view/View;I)Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 375
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 376
    check-cast v1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    .line 377
    .local v1, "result":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->removeAllViews()V

    .line 381
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;-><init>(Z)V

    .line 382
    .local v0, "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    const/4 v2, -0x1

    iput v2, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->width:I

    .line 383
    iput v3, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->height:I

    .line 384
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 385
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->setCoverViewFactory(Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;)V

    .line 386
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->cardPositionFromAdapterPosition(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->setCoverViewIndex(I)V

    .line 387
    return-object v1

    .line 379
    .end local v0    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    .end local v1    # "result":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/widget/BookCoverContainerView;-><init>(Landroid/content/Context;)V

    .restart local v1    # "result":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    goto :goto_0
.end method

.method private getFooter()Landroid/view/View;
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHasFetchedEbooksOnce:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mFooter:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFooterCount()I
    .locals 1

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getFooter()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHeader()Landroid/view/View;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHeader:Landroid/view/View;

    return-object v0
.end method

.method private getHeaderCount()I
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 300
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 301
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    const/4 v0, 0x3

    .line 311
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowEmptyView()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHeader:Landroid/view/View;

    if-nez v1, :cond_0

    .line 311
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getInfoViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    return-object v0
.end method

.method private getOnboardViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-nez v0, :cond_0

    .line 216
    new-instance v0, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardViewFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    return-object v0
.end method

.method private isEmptyViewPosition(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowEmptyView()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 264
    if-ne p1, v0, :cond_0

    .line 266
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 264
    goto :goto_0

    :cond_1
    move v0, v1

    .line 266
    goto :goto_0
.end method

.method private isFooterPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getFooter()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHeaderPosition(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 253
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-ne p1, v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getHeader()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private isInfoViewPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v1

    if-eqz v1, :cond_0

    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnboardViewPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowOnboardView()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->canShowInfoView()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    if-ne p1, v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private isSpacerViewPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 271
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSpanAllColumns(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "span"    # Z

    .prologue
    .line 365
    if-eqz p1, :cond_0

    .line 366
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;-><init>(Z)V

    .line 367
    .local v0, "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    .end local v0    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    :cond_0
    return-void
.end method


# virtual methods
.method public createCoverView(Landroid/view/ViewGroup;I)Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "coverViewIndex"    # I

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, p1}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    return-object v0
.end method

.method public getCardCount()I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getHeaderCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getCardCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getFooterCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isSpacerViewPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isHeaderPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isFooterPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335
    :cond_0
    const/4 v0, 0x0

    .line 337
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->cardPositionFromAdapterPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 343
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isSpacerViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-wide/16 v0, -0x68

    .line 361
    :goto_0
    return-wide v0

    .line 346
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isInfoViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    const-wide/16 v0, -0x67

    goto :goto_0

    .line 349
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isOnboardViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    const-wide/16 v0, -0x69

    goto :goto_0

    .line 352
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isEmptyViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 353
    const-wide/16 v0, -0x66

    goto :goto_0

    .line 355
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isHeaderPosition(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 356
    const-wide/16 v0, -0x64

    goto :goto_0

    .line 358
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isFooterPosition(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 359
    const-wide/16 v0, -0x65

    goto :goto_0

    .line 361
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->cardPositionFromAdapterPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isInfoViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    const/4 v0, 0x4

    .line 453
    :goto_0
    return v0

    .line 442
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isOnboardViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    const/4 v0, 0x6

    goto :goto_0

    .line 444
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isEmptyViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 445
    const/4 v0, 0x3

    goto :goto_0

    .line 446
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isSpacerViewPosition(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 447
    const/4 v0, 0x5

    goto :goto_0

    .line 448
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isHeaderPosition(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 449
    const/4 v0, 0x1

    goto :goto_0

    .line 450
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->isFooterPosition(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 451
    const/4 v0, 0x2

    goto :goto_0

    .line 453
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 392
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 406
    invoke-direct {p0, p3, p2, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getCoverContainerView(Landroid/view/ViewGroup;Landroid/view/View;I)Lcom/google/android/apps/books/widget/BookCoverContainerView;

    move-result-object v0

    :goto_0
    return-object v0

    .line 394
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    goto :goto_0

    .line 396
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mSpacerView:Landroid/view/View;

    goto :goto_0

    .line 398
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    goto :goto_0

    .line 400
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    goto :goto_0

    .line 402
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getHeader()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 404
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getFooter()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x7

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    return v0
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 2
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 167
    :goto_0
    return-void

    .line 162
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mEmptyView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 166
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setFooter(Landroid/view/View;)V
    .locals 2
    .param p1, "footer"    # Landroid/view/View;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mFooter:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 118
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mFooter:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mFooter:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setHasFetchedEbooksOnce(Z)V
    .locals 1
    .param p1, "hasFetched"    # Z

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHasFetchedEbooksOnce:Z

    if-ne v0, p1, :cond_0

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHasFetchedEbooksOnce:Z

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setHeader(Landroid/view/View;)V
    .locals 2
    .param p1, "header"    # Landroid/view/View;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHeader:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 109
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHeader:Landroid/view/View;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mHeader:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setInfoView(Landroid/view/View;)V
    .locals 1
    .param p1, "infoView"    # Landroid/view/View;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    .line 132
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setOnboardView(Landroid/view/View;)V
    .locals 1
    .param p1, "onboardView"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    .line 141
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setSpacerView(Landroid/view/View;)V
    .locals 1
    .param p1, "spacer"    # Landroid/view/View;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mSpacerView:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mSpacerView:Landroid/view/View;

    .line 150
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpanAllColumns(Landroid/view/View;Z)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setupExistingView(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 415
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getItemViewType(I)I

    move-result v3

    if-nez v3, :cond_0

    .line 416
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    if-eqz v3, :cond_0

    .line 417
    instance-of v3, p1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    if-eqz v3, :cond_1

    check-cast p1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    .end local p1    # "view":Landroid/view/View;
    move-object v1, p1

    .line 421
    .local v1, "containerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    :goto_0
    if-eqz v1, :cond_0

    .line 422
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->cardPositionFromAdapterPosition(I)I

    move-result v0

    .line 423
    .local v0, "cardPosition":I
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->setCoverViewIndex(I)V

    .line 424
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->getCoverView()Landroid/view/View;

    move-result-object v2

    .line 425
    .local v2, "coverView":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 426
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mCardsAdapter:Lcom/google/android/apps/books/widget/BaseCardsAdapter;

    invoke-virtual {v3, v2, v0}, Lcom/google/android/apps/books/widget/BaseCardsAdapter;->setupExistingView(Landroid/view/View;I)V

    .line 431
    .end local v0    # "cardPosition":I
    .end local v1    # "containerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .end local v2    # "coverView":Landroid/view/View;
    :cond_0
    return-void

    .line 417
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public showInfoView(Z)V
    .locals 3
    .param p1, "showView"    # Z

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowInfoView:Z

    if-ne v0, p1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowInfoView:Z

    .line 182
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowInfoView:Z

    if-eqz v0, :cond_2

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getInfoViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mInfoCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    goto :goto_0
.end method

.method public showOnboardView(Z)V
    .locals 3
    .param p1, "showOnboardView"    # Z

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowOnboardView:Z

    if-ne v0, p1, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowOnboardView:Z

    .line 197
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mShowOnboardView:Z

    if-eqz v0, :cond_2

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 202
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getOnboardViewFadeAnimationController()Lcom/google/android/ublib/view/FadeAnimationController;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->mOnboardCardFadeListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    goto :goto_0
.end method
