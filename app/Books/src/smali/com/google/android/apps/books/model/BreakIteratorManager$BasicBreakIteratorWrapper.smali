.class abstract Lcom/google/android/apps/books/model/BreakIteratorManager$BasicBreakIteratorWrapper;
.super Ljava/lang/Object;
.source "BreakIteratorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BreakIteratorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "BasicBreakIteratorWrapper"
.end annotation


# instance fields
.field public breakIterator:Ljava/text/BreakIterator;

.field public final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    if-eqz p1, :cond_0

    .end local p1    # "locale":Ljava/util/Locale;
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/books/model/BreakIteratorManager$BasicBreakIteratorWrapper;->locale:Ljava/util/Locale;

    .line 46
    return-void

    .line 45
    .restart local p1    # "locale":Ljava/util/Locale;
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    goto :goto_0
.end method
