.class public Lcom/google/android/apps/books/eob/TextureEndOfBookView;
.super Landroid/widget/RelativeLayout;
.source "TextureEndOfBookView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParserError"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;
    }
.end annotation


# instance fields
.field private mBuyView:Landroid/widget/TextView;

.field private mCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

.field private mCoverFuture:Lcom/google/android/apps/books/common/ImageFuture;

.field private mCoverTouchDownTime:J

.field private mCoverView:Landroid/widget/ImageView;

.field private mCroppedProfileImage:Landroid/graphics/Bitmap;

.field private mDownX:F

.field private mIsPurchaseableSample:Z

.field private mListener:Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;

.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mPagingSlop:I

.field private mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

.field private mRateClickListener:Landroid/view/View$OnClickListener;

.field private mRatedView:Landroid/view/View;

.field private mShareClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPagingSlop:I

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPagingSlop:I

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPagingSlop:I

    .line 107
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/eob/TextureEndOfBookView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .param p1, "x1"    # J

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "x3"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getShareUrl(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/content/Context;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->updateRating(Landroid/content/Context;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Landroid/widget/ImageView;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setAuthorImage(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method private addPrice(Landroid/content/res/Resources;Landroid/widget/TextView;)V
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "buyButtonLabel"    # Landroid/widget/TextView;

    .prologue
    .line 456
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    if-eqz v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/RentalUtils;->getSaleOrRentalText(Lcom/google/android/apps/books/app/PurchaseInfo;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 458
    .local v0, "buttonText":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    .end local v0    # "buttonText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private getImageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/ImageManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 111
    .local v0, "booksApplication":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v1

    return-object v1
.end method

.method private getShareUrl(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "campaignId"    # Ljava/lang/String;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 442
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 443
    .local v0, "volumeId":Ljava/lang/String;
    invoke-static {p3}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/util/OceanUris;->getShareUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private notifyChange()V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mListener:Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mListener:Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;->onChange()V

    .line 453
    :cond_0
    return-void
.end method

.method private setAuthorImage(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "croppedImage"    # Landroid/graphics/Bitmap;
    .param p2, "authorImageView"    # Landroid/widget/ImageView;

    .prologue
    .line 391
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 392
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 393
    invoke-direct {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V

    .line 394
    return-void
.end method

.method private updateRating(Landroid/content/Context;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ratingInfo"    # Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    .prologue
    .line 310
    move-object/from16 v11, p1

    check-cast v11, Lcom/google/android/apps/books/app/ReadingActivity;

    .line 311
    .local v11, "readingActivity":Lcom/google/android/apps/books/app/ReadingActivity;
    invoke-virtual {v11}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    if-ltz v2, :cond_0

    .line 313
    const v2, 0x7f0e01e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 314
    .local v15, "unratedView":Landroid/view/View;
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    if-gtz v2, :cond_1

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRatedView:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 317
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/view/View;->setVisibility(I)V

    .line 318
    const v2, 0x7f0e01e9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRateClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    const v2, 0x7f0e01ea

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mShareClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V

    .line 388
    .end local v15    # "unratedView":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 324
    .restart local v15    # "unratedView":Landroid/view/View;
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v15, v2}, Landroid/view/View;->setVisibility(I)V

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRatedView:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 326
    const v2, 0x7f0e01f1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mShareClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    const v2, 0x7f0e01ef

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/play/layout/StarRatingBar;

    .line 328
    .local v10, "ratingBar":Lcom/google/android/play/layout/StarRatingBar;
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    int-to-float v2, v2

    invoke-virtual {v10, v2}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 329
    const v2, 0x7f0e01ee

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 330
    .local v14, "reviewerNameView":Landroid/widget/TextView;
    const v2, 0x7f0e01ec

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 331
    .local v8, "authorImageView":Landroid/widget/ImageView;
    const v2, 0x7f0a01be

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 332
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->authorTitle:Ljava/lang/String;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->authorProfileImageUrl:Ljava/lang/String;

    .line 334
    .local v7, "authorImageUrl":Ljava/lang/String;
    if-nez v7, :cond_2

    .line 335
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 336
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V

    goto :goto_0

    .line 339
    :cond_2
    const v2, 0x7f0e01ed

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRateClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setAuthorImage(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    goto/16 :goto_0

    .line 344
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getImageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v1

    .line 345
    .local v1, "imageManager":Lcom/google/android/apps/books/common/ImageManager;
    invoke-virtual {v11}, Lcom/google/android/apps/books/app/ReadingActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/app/BooksApplication;

    .line 347
    .local v9, "booksApplication":Lcom/google/android/apps/books/app/BooksApplication;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getReadNowCoverStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/RemoteFileCache;

    move-result-object v6

    .line 349
    .local v6, "imageStore":Lcom/google/android/apps/books/model/RemoteFileCache;
    new-instance v5, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v11, v8}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Lcom/google/android/apps/books/app/ReadingActivity;Landroid/widget/ImageView;)V

    .line 380
    .local v5, "imageCallback":Lcom/google/android/apps/books/common/ImageCallback;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 381
    .local v12, "res":Landroid/content/res/Resources;
    const v2, 0x7f09010f

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 383
    .local v13, "reviewerImageSize":I
    new-instance v3, Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 385
    .local v3, "authorImageConstraints":Lcom/google/android/ublib/util/ImageConstraints;
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    goto/16 :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 474
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 464
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 465
    const v0, 0x7f0e00e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;

    .line 466
    const v0, 0x7f0e01eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRatedView:Landroid/view/View;

    .line 467
    const v0, 0x7f0e01e7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mBuyView:Landroid/widget/TextView;

    .line 468
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v6, 0x0

    .line 398
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 429
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_1
    return v2

    .line 400
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mDownX:F

    goto :goto_0

    .line 404
    :pswitch_1
    iget v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mDownX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPagingSlop:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 405
    iput-wide v6, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    goto :goto_0

    .line 411
    :pswitch_2
    iget-wide v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 412
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 415
    .local v0, "hitRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/widget/ImageView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 416
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 417
    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    iget-boolean v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mIsPurchaseableSample:Z

    if-eqz v2, :cond_1

    const-string v2, "books_inapp_end_of_sample_cover"

    :goto_2
    invoke-static {v3, v2}, Lcom/google/android/apps/books/app/StoreLink;->forBook(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/app/StoreLink;

    move-result-object v1

    .line 421
    .local v1, "storeLink":Lcom/google/android/apps/books/app/StoreLink;
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_COVER_PRESSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/StoreLink;->viewStorePage(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Z)V

    .line 423
    const/4 v2, 0x1

    goto :goto_1

    .line 417
    .end local v1    # "storeLink":Lcom/google/android/apps/books/app/StoreLink;
    :cond_1
    const-string v2, "books_inapp_end_of_book_cover"

    goto :goto_2

    .line 425
    :cond_2
    iput-wide v6, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverTouchDownTime:J

    goto :goto_0

    .line 398
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public requestRatingInfoUpdate(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/books/app/ReadingActivity;

    new-instance v3, Lcom/google/android/apps/books/eob/TextureEndOfBookView$7;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$7;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/content/Context;)V

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/books/eob/RatingHelper;->requestRatingInfo(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)Z

    .line 303
    return-void
.end method

.method public setPrice(Landroid/content/res/Resources;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 433
    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mBuyView:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->addPrice(Landroid/content/res/Resources;Landroid/widget/TextView;)V

    .line 435
    invoke-direct {p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V

    .line 436
    return-void
.end method

.method public setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
    .locals 24
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "listener"    # Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .param p5, "account"    # Landroid/accounts/Account;
    .param p6, "systemBarManager"    # Lcom/google/android/apps/books/app/SystemBarManager;

    .prologue
    .line 117
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 118
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mListener:Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;

    .line 119
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    .line 121
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mIsPurchaseableSample:Z

    .line 122
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getBuyUrl()Ljava/lang/String;

    move-result-object v3

    const-string v5, "books_inapp_eob_buy_from_sample"

    invoke-static {v3, v5}, Lcom/google/android/apps/books/util/OceanUris;->appendCampaignId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 126
    .local v11, "buyUrl":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v13

    .line 127
    .local v13, "volumeId":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v4

    .line 129
    .local v4, "coverUri":Landroid/net/Uri;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v3

    if-nez v3, :cond_0

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;

    new-instance v5, Lcom/google/android/apps/books/eob/TextureEndOfBookView$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$1;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 143
    :cond_0
    const v3, 0x7f0e01e5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 144
    .local v23, "titleView":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 145
    .local v20, "res":Landroid/content/res/Resources;
    const v3, 0x7f0c0013

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v19

    .line 147
    .local v19, "minConstrainingDimensionDp":I
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v16

    .line 150
    .local v16, "config":Landroid/content/res/Configuration;
    move-object/from16 v0, v16

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_6

    .line 151
    move-object/from16 v0, v16

    iget v3, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    move/from16 v0, v19

    if-le v3, v0, :cond_5

    const/16 v22, 0x1

    .line 152
    .local v22, "showCover":Z
    :goto_0
    if-nez v22, :cond_1

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v3

    const v5, 0x7f090109

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v8

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v9

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v5, v8, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 168
    :cond_1
    :goto_1
    if-eqz v22, :cond_2

    .line 169
    new-instance v6, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Landroid/accounts/Account;)V

    .line 187
    .local v6, "ensurer":Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    new-instance v7, Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)V

    .line 201
    .local v7, "imageCallback":Lcom/google/android/apps/books/common/ImageCallback;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getImageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v3 .. v8}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 205
    .end local v6    # "ensurer":Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    .end local v7    # "imageCallback":Lcom/google/android/apps/books/common/ImageCallback;
    :cond_2
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    const v3, 0x7f0e01e6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 208
    .local v15, "authorView":Landroid/widget/TextView;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v14

    .line 209
    .local v14, "author":Ljava/lang/String;
    invoke-static {v14}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 210
    const/16 v3, 0x8

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    :goto_2
    const v3, 0x7f0e01e4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 216
    .local v21, "shortMessage":Landroid/widget/TextView;
    if-eqz v21, :cond_3

    .line 217
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mIsPurchaseableSample:Z

    if-eqz v3, :cond_9

    const v3, 0x7f0f00b2

    :goto_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 219
    .local v18, "instructions":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    .end local v18    # "instructions":Ljava/lang/String;
    :cond_3
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v3

    if-nez v3, :cond_4

    .line 223
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mIsPurchaseableSample:Z

    if-eqz v3, :cond_a

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mBuyView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mBuyView:Landroid/widget/TextView;

    new-instance v8, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    :cond_4
    :goto_4
    return-void

    .line 151
    .end local v14    # "author":Ljava/lang/String;
    .end local v15    # "authorView":Landroid/widget/TextView;
    .end local v21    # "shortMessage":Landroid/widget/TextView;
    .end local v22    # "showCover":Z
    :cond_5
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 161
    :cond_6
    move-object/from16 v0, v16

    iget v3, v0, Landroid/content/res/Configuration;->screenWidthDp:I

    move/from16 v0, v19

    if-le v3, v0, :cond_7

    const/16 v22, 0x1

    .line 162
    .restart local v22    # "showCover":Z
    :goto_5
    if-nez v22, :cond_1

    .line 163
    const v3, 0x7f0e01f2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    .line 164
    .local v17, "coverHolder":Landroid/view/ViewGroup;
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_1

    .line 161
    .end local v17    # "coverHolder":Landroid/view/ViewGroup;
    .end local v22    # "showCover":Z
    :cond_7
    const/16 v22, 0x0

    goto :goto_5

    .line 212
    .restart local v14    # "author":Ljava/lang/String;
    .restart local v15    # "authorView":Landroid/widget/TextView;
    .restart local v22    # "showCover":Z
    :cond_8
    invoke-virtual {v15, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 217
    .restart local v21    # "shortMessage":Landroid/widget/TextView;
    :cond_9
    const v3, 0x7f0f00b1

    goto :goto_3

    .line 256
    :cond_a
    new-instance v3, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRateClickListener:Landroid/view/View$OnClickListener;

    .line 267
    new-instance v3, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mShareClickListener:Landroid/view/View$OnClickListener;

    .line 280
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->requestRatingInfoUpdate(Landroid/content/Context;)V

    .line 287
    const v3, 0x7f0e01e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 288
    const v3, 0x7f0e01e9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mRateClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    const v3, 0x7f0e01ea

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mShareClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method
