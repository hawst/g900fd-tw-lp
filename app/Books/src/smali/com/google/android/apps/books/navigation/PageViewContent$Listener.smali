.class public interface abstract Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
.super Ljava/lang/Object;
.source "PageViewContent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/PageViewContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract getCookie()Ljava/lang/Object;
.end method

.method public abstract onBookmarksChanged()V
.end method

.method public abstract pageUpdated()V
.end method

.method public abstract setCookie(Ljava/lang/Object;)V
.end method
