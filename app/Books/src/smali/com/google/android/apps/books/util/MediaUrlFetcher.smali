.class public Lcom/google/android/apps/books/util/MediaUrlFetcher;
.super Ljava/lang/Object;
.source "MediaUrlFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;,
        Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mBackgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mContext:Landroid/content/Context;

.field private final mMediaFoundListener:Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;

.field private final mTargetWidth:I

.field private final mTouchableItem:Lcom/google/android/apps/books/render/TouchableItem;

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/render/TouchableItem;Ljava/lang/String;Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "touchableItem"    # Lcom/google/android/apps/books/render/TouchableItem;
    .param p4, "volumeId"    # Ljava/lang/String;
    .param p5, "mediaFoundListener"    # Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
    .param p6, "targetWidth"    # I

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mContext:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mAccount:Landroid/accounts/Account;

    .line 78
    iput-object p3, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTouchableItem:Lcom/google/android/apps/books/render/TouchableItem;

    .line 79
    iput-object p4, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mVolumeId:Ljava/lang/String;

    .line 80
    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mMediaFoundListener:Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;

    .line 81
    iput p6, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTargetWidth:I

    .line 82
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    .line 83
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mBackgroundThreadExecutor:Ljava/util/concurrent/Executor;

    .line 84
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTouchableItem:Lcom/google/android/apps/books/render/TouchableItem;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFailed(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchSucceeded(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mMediaFoundListener:Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/io/FileDescriptor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;
    .param p1, "x1"    # Ljava/io/FileDescriptor;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fileFound(Ljava/io/FileDescriptor;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFromNetwork()V

    return-void
.end method

.method private fetchFailed(I)V
    .locals 2
    .param p1, "messageId"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 202
    return-void
.end method

.method private fetchFromNetwork()V
    .locals 3

    .prologue
    .line 121
    const-string v1, "MediaUrlFetcher"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    const-string v1, "MediaUrlFetcher"

    const-string v2, "using network media"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTargetWidth:I

    .line 126
    .local v0, "targetWidth":I
    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mBackgroundThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 169
    return-void
.end method

.method private fetchSucceeded(Ljava/lang/String;)V
    .locals 2
    .param p1, "mediaUrl"    # Ljava/lang/String;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/util/MediaUrlFetcher$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher$3;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 191
    return-void
.end method

.method private fileFound(Ljava/io/FileDescriptor;)V
    .locals 2
    .param p1, "fileDescriptor"    # Ljava/io/FileDescriptor;

    .prologue
    .line 172
    const-string v0, "MediaUrlFetcher"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "MediaUrlFetcher"

    const-string v1, "using downloaded media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/util/MediaUrlFetcher$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher$2;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/io/FileDescriptor;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 182
    return-void
.end method


# virtual methods
.method public fetchMedia()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTouchableItem:Lcom/google/android/apps/books/render/TouchableItem;

    iget-object v1, v1, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->urlToResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "resourceId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v5}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 103
    .local v0, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    iget v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTargetWidth:I

    if-nez v1, :cond_1

    .line 107
    const-string v1, "MediaUrlFetcher"

    const/4 v5, 0x3

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const-string v1, "MediaUrlFetcher"

    const-string v5, "fetchMedia trying audio bypass"

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    new-instance v3, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;Lcom/google/android/apps/books/util/MediaUrlFetcher$1;)V

    .line 112
    .local v3, "consumer":Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;
    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher;->mVolumeId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v7, 0x1

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 118
    .end local v3    # "consumer":Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFromNetwork()V

    goto :goto_0
.end method
