.class public abstract Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "TypeSafeRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewHandle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<TT;>;"
    .local p1, "itemView":Landroid/view/View;, "TT;"
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getItemView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->itemView:Landroid/view/View;

    return-object v0
.end method

.method protected abstract onBind(I)V
.end method

.method protected onViewAttachedToWindow()V
    .locals 0

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<TT;>;"
    return-void
.end method

.method protected onViewDetachedFromWindow()V
    .locals 0

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<TT;>;"
    return-void
.end method

.method protected onViewRecycled()V
    .locals 0

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<TT;>;"
    return-void
.end method
