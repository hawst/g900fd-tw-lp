.class Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;
.super Lcom/google/android/apps/books/model/ContentXmlHandler;
.source "AnnotationContextSearchImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContextGatheringHandler"
.end annotation


# instance fields
.field private final mCharsToSave:I

.field private final mContextCanIncludeNearbyPages:Z

.field private mHasAfterText:Z

.field private mHasBeforeText:Z

.field private mNonWhiteCharsAfterEnd:I

.field private mNonWhiteCharsAfterStart:I

.field private mOnCorrectPage:Z

.field private final mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

.field private final mRoughAfterText:Ljava/lang/StringBuffer;

.field private final mRoughBeforeText:Lcom/google/android/apps/books/model/CircularBuffer;

.field private mSeenEndPos:Z

.field private mSeenStartPos:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/annotations/TextLocationRange;IZ)V
    .locals 2
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p2, "charsToSave"    # I
    .param p3, "contextCanIncludeNearbyPages"    # Z

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/books/model/ContentXmlHandler;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughAfterText:Ljava/lang/StringBuffer;

    .line 30
    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenStartPos:Z

    .line 31
    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenEndPos:Z

    .line 32
    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasBeforeText:Z

    .line 33
    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasAfterText:Z

    .line 34
    iput v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterStart:I

    .line 35
    iput v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    .line 40
    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mOnCorrectPage:Z

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 45
    iput p2, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mCharsToSave:I

    .line 46
    new-instance v0, Lcom/google/android/apps/books/model/CircularBuffer;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/model/CircularBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughBeforeText:Lcom/google/android/apps/books/model/CircularBuffer;

    .line 47
    iput-boolean p3, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mContextCanIncludeNearbyPages:Z

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/annotations/TextLocationRange;IZLcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$1;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;-><init>(Lcom/google/android/apps/books/annotations/TextLocationRange;IZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->hasBeforeText()Z

    move-result v0

    return v0
.end method

.method private hasBeforeText()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasBeforeText:Z

    return v0
.end method


# virtual methods
.method public canAddText()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mContextCanIncludeNearbyPages:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mOnCorrectPage:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructContext(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .locals 3
    .param p1, "selectedText"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughBeforeText:Lcom/google/android/apps/books/model/CircularBuffer;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/CircularBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughAfterText:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .local v0, "result":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    return-object v0
.end method

.method public hasGatheredEnough()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasBeforeText:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasAfterText:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onText(Ljava/lang/String;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 71
    iget-boolean v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenStartPos:Z

    if-nez v6, :cond_2

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->canAddText()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 73
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughBeforeText:Lcom/google/android/apps/books/model/CircularBuffer;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/books/model/CircularBuffer;->add(Ljava/lang/String;)V

    .line 90
    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenEndPos:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasAfterText:Z

    if-nez v6, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->canAddText()Z

    move-result v6

    if-nez v6, :cond_6

    .line 94
    iput-boolean v8, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasAfterText:Z

    .line 120
    :cond_1
    :goto_1
    return-void

    .line 75
    :cond_2
    iget-boolean v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasBeforeText:Z

    if-nez v6, :cond_0

    .line 76
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v6, v6, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    add-int/lit8 v0, v6, 0x1

    .line 77
    .local v0, "afterStartGoal":I
    const/4 v3, 0x0

    .line 78
    .local v3, "ii":I
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 79
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 80
    .local v1, "curChar":C
    iget v9, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterStart:I

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v6

    if-eqz v6, :cond_4

    move v6, v7

    :goto_3
    add-int/2addr v6, v9

    iput v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterStart:I

    .line 82
    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterStart:I

    if-ne v6, v0, :cond_5

    .line 83
    iput-boolean v8, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasBeforeText:Z

    .line 87
    .end local v1    # "curChar":C
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughBeforeText:Lcom/google/android/apps/books/model/CircularBuffer;

    invoke-virtual {v6, p1, v3}, Lcom/google/android/apps/books/model/CircularBuffer;->add(Ljava/lang/String;I)V

    goto :goto_0

    .restart local v1    # "curChar":C
    :cond_4
    move v6, v8

    .line 80
    goto :goto_3

    .line 78
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 95
    .end local v0    # "afterStartGoal":I
    .end local v1    # "curChar":C
    .end local v3    # "ii":I
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 96
    const/4 v3, 0x0

    .line 97
    .restart local v3    # "ii":I
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v2, v6, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    .line 98
    .local v2, "endOffset":I
    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    if-gt v6, v2, :cond_7

    .line 100
    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_7

    .line 101
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 102
    .restart local v1    # "curChar":C
    iget v9, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v6

    if-eqz v6, :cond_9

    move v6, v7

    :goto_5
    add-int/2addr v6, v9

    iput v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    .line 104
    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    if-le v6, v2, :cond_a

    .line 108
    .end local v1    # "curChar":C
    :cond_7
    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mNonWhiteCharsAfterEnd:I

    if-le v6, v2, :cond_8

    .line 111
    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mCharsToSave:I

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughAfterText:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    sub-int v5, v6, v9

    .line 112
    .local v5, "neededChars":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 113
    .local v4, "lengthToCopy":I
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughAfterText:Ljava/lang/StringBuffer;

    add-int v9, v3, v4

    invoke-virtual {p1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    .end local v4    # "lengthToCopy":I
    .end local v5    # "neededChars":I
    :cond_8
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRoughAfterText:Ljava/lang/StringBuffer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    iget v9, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mCharsToSave:I

    if-ne v6, v9, :cond_b

    :goto_6
    iput-boolean v8, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mHasAfterText:Z

    goto/16 :goto_1

    .restart local v1    # "curChar":C
    :cond_9
    move v6, v8

    .line 102
    goto :goto_5

    .line 100
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .end local v1    # "curChar":C
    :cond_b
    move v8, v7

    .line 117
    goto :goto_6
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v3, 0x1

    .line 53
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/model/ContentXmlHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 54
    invoke-static {p3, p4}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->getReadingPosition(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "position":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 56
    iget-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mContextCanIncludeNearbyPages:Z

    if-nez v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->extractPageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mOnCorrectPage:Z

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    iput-boolean v3, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenStartPos:Z

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 64
    iput-boolean v3, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->mSeenEndPos:Z

    .line 67
    :cond_2
    return-void
.end method
