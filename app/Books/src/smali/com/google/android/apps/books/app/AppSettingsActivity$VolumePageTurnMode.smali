.class Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumePageTurnMode"
.end annotation


# instance fields
.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 397
    const v0, 0x7f0f015c

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(I)V

    .line 398
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 399
    return-void
.end method


# virtual methods
.method protected getValue()Z
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getVolumeKeyPageTurn()Z

    move-result v0

    return v0
.end method

.method protected setValue(Z)V
    .locals 2
    .param p1, "useVolumeKeys"    # Z

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setVolumeKeyPageTurn(Z)V

    .line 409
    if-eqz p1, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->setSawVolumeKeyPageTurnDialog()V

    .line 412
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_VOLUME_KEY_BEHAVIOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$400(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V

    .line 414
    return-void
.end method
