.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->startedOpeningBook(Ljava/lang/Object;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$key:Ljava/lang/Object;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 942
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$key:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$key:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "BgDataController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    const-string v0, "BgDataController"

    const-string v1, "Opened another book before finishing opening previous book"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->flushQueuedActions()V
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V

    .line 958
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$volumeId:Ljava/lang/String;

    # setter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3802(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)Ljava/lang/String;

    .line 959
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$key:Ljava/lang/Object;

    # setter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3602(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/NetworkTaskQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->boostVolumePriority(Ljava/lang/String;)V

    .line 962
    return-void
.end method
