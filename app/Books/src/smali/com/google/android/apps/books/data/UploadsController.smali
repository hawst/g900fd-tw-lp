.class public interface abstract Lcom/google/android/apps/books/data/UploadsController;
.super Ljava/lang/Object;
.source "UploadsController.java"


# virtual methods
.method public abstract addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract cancelUpload(Ljava/lang/String;)V
.end method

.method public abstract pauseUpload(Ljava/lang/String;)V
.end method

.method public abstract resumeUpload(Ljava/lang/String;)V
.end method
