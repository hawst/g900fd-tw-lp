.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->saveVolumeDataOnMainThread(Lcom/google/android/apps/books/model/VolumeData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeData(Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    invoke-static {v0, v1, v2, v3, v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->setVolume(Lcom/google/android/apps/books/model/VolumeData;)V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 452
    return-void
.end method
