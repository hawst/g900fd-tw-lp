.class Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/BookView$Callbacks;
.implements Lcom/google/android/apps/books/widget/PagesView$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PagesViewCallbacks"
.end annotation


# instance fields
.field private final mNormTmp:Landroid/graphics/PointF;

.field private mTimeStartedShowingLoadingPage:J

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 2

    .prologue
    .line 3964
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4033
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mTimeStartedShowingLoadingPage:J

    .line 4174
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 3964
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    return-void
.end method

.method private dispatchSetUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 4235
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4236
    return-void
.end method

.method private normalizedDistanceToContent(Lcom/google/android/apps/books/widget/DevicePageRendering;F)I
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "distance"    # F

    .prologue
    const/4 v5, 0x0

    .line 4183
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    invoke-virtual {v2, v5, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 4184
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    invoke-interface {v2, p1, v3, v4}, Lcom/google/android/apps/books/render/Renderer;->normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    .line 4185
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    iget v0, v2, Landroid/graphics/PointF;->x:F

    .line 4187
    .local v0, "Ox":F
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    invoke-virtual {v2, p2, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 4188
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    invoke-interface {v2, p1, v3, v4}, Lcom/google/android/apps/books/render/Renderer;->normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    .line 4189
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mNormTmp:Landroid/graphics/PointF;

    iget v1, v2, Landroid/graphics/PointF;->x:F

    .line 4191
    .local v1, "Tx":F
    sub-float v2, v1, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    return v2
.end method


# virtual methods
.method public addRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 4071
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/DispatchRendererListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/DispatchRendererListener;->weaklyAddListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 4072
    return-void
.end method

.method public atLeastOnePositionHasBookmark(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 3980
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->atLeastOnePositionHasBookmark(I)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6900(Lcom/google/android/apps/books/widget/PagesViewController;I)Z

    move-result v0

    return v0
.end method

.method public flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 2
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 4019
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->pageFlipped()V

    .line 4020
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageFlipIncrementValue(Lcom/google/android/apps/books/util/ScreenDirection;)I

    move-result v1

    # += operator for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5812(Lcom/google/android/apps/books/widget/PagesViewController;I)I

    .line 4021
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->onOffsetChanged()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7200(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 4022
    return-void
.end method

.method public getBookmarkController()Lcom/google/android/apps/books/app/BookmarkController;
    .locals 1

    .prologue
    .line 4166
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8100(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/app/BookmarkController;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentOffsetFromBasePosition()I
    .locals 1

    .prologue
    .line 3985
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v0

    return v0
.end method

.method public getCurrentRelevantOffsets()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4014
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getCurrentRelevantOffsets()Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7100(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4240
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8800(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/ReaderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    return-object v0
.end method

.method public importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 4145
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getOffsetFromBasePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I
    invoke-static {v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7700(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v1

    .line 4147
    .local v1, "offset":I
    const v2, 0x7fffffff

    if-ne v1, v2, :cond_1

    .line 4162
    :cond_0
    :goto_0
    return-void

    .line 4151
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->isPendingPageRequest(I)Z
    invoke-static {v2, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7800(Lcom/google/android/apps/books/widget/PagesViewController;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4159
    new-instance v0, Lcom/google/android/apps/books/util/RenderRequestContext;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7900(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/apps/books/util/RenderRequestContext;-><init>(ILcom/google/android/apps/books/common/Position;I)V

    .line 4161
    .local v0, "cookie":Lcom/google/android/apps/books/util/RenderRequestContext;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->publishPageData(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V
    invoke-static {v2, p1, p2, p2, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8000(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V

    goto :goto_0
.end method

.method public isScrubbing()Z
    .locals 1

    .prologue
    .line 4114
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isScrubbing()Z

    move-result v0

    return v0
.end method

.method public loadPages([I)V
    .locals 10
    .param p1, "offsets"    # [I

    .prologue
    const/4 v9, 0x1

    .line 3990
    if-eqz p1, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getAbsoluteOffsets([I)[I
    invoke-static {v6, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7000(Lcom/google/android/apps/books/widget/PagesViewController;[I)[I

    move-result-object p1

    .line 3992
    :goto_0
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_4

    aget v4, v0, v2

    .line 3993
    .local v4, "offsetFromBasePosition":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/google/android/apps/books/widget/PagesView;->isCached(I)Z

    move-result v1

    .line 3994
    .local v1, "cached":Z
    if-nez v1, :cond_3

    .line 3995
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v5

    .line 3996
    .local v5, "position":Lcom/google/android/apps/books/render/RenderPosition;
    if-eqz v5, :cond_2

    .line 4000
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v6, v5, v4, v9, v9}, Lcom/google/android/apps/books/widget/PagesViewController;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;IZZ)V

    .line 3992
    .end local v5    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3990
    .end local v0    # "arr$":[I
    .end local v1    # "cached":Z
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "offsetFromBasePosition":I
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$200(Lcom/google/android/apps/books/widget/PagesViewController;)[I

    move-result-object p1

    goto :goto_0

    .line 4002
    .restart local v0    # "arr$":[I
    .restart local v1    # "cached":Z
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "offsetFromBasePosition":I
    .restart local v5    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_2
    const-string v6, "PagesViewHelper"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4003
    const-string v6, "PagesViewHelper"

    const-string v7, "Tried to load page before selecting reading position"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 4005
    .end local v5    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_3
    const-string v6, "PagesViewHelper"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4006
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v5

    .line 4007
    .restart local v5    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    const-string v6, "PagesViewHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Page cached: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (or pending), position: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 4010
    .end local v1    # "cached":Z
    .end local v4    # "offsetFromBasePosition":I
    .end local v5    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_4
    return-void
.end method

.method public onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 4081
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->dispatchSetUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4082
    return-void
.end method

.method public onFinishedTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 4096
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onFinishedTransitionToMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4097
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne p1, v0, :cond_0

    .line 4098
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderListener:Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->onPagesNeedRedraw()V

    .line 4100
    :cond_0
    return-void
.end method

.method public onLoadingPageVisibilityChanged(Z)V
    .locals 10
    .param p1, "loadingPageVisible"    # Z

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x3

    .line 4041
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderedFirstMainPage:Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7300(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 4060
    :cond_0
    :goto_0
    return-void

    .line 4045
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 4047
    .local v2, "now":J
    if-eqz p1, :cond_3

    .line 4048
    const-string v4, "PagesViewHelper"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4049
    const-string v4, "PagesViewHelper"

    const-string v5, "Loading page appeared"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4051
    :cond_2
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mTimeStartedShowingLoadingPage:J

    goto :goto_0

    .line 4052
    :cond_3
    iget-wide v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mTimeStartedShowingLoadingPage:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    .line 4053
    iget-wide v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mTimeStartedShowingLoadingPage:J

    sub-long v0, v2, v4

    .line 4054
    .local v0, "elapsed":J
    const-string v4, "PagesViewHelper"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4055
    const-string v4, "PagesViewHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Last loading page disappeared after "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4057
    :cond_4
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDisplayedLoadingPage(J)V

    .line 4058
    iput-wide v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->mTimeStartedShowingLoadingPage:J

    goto :goto_0
.end method

.method public onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V
    .locals 1
    .param p1, "handle"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    .line 4104
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V

    .line 4105
    return-void
.end method

.method public onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V
    .locals 1
    .param p1, "endHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p2, "progress"    # F

    .prologue
    .line 4109
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V

    .line 4110
    return-void
.end method

.method public onPageTouch(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/PointF;FZ)Z
    .locals 9
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "pagePositionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "location"    # Landroid/graphics/PointF;
    .param p4, "threshold"    # F
    .param p5, "confirmed"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4199
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempPointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/PointF;

    move-result-object v7

    invoke-interface {v6, p1, p3, v7}, Lcom/google/android/apps/books/render/Renderer;->normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    .line 4201
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempPointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempPointF:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/PointF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/PointF;->y:F

    float-to-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Point;->set(II)V

    .line 4202
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->normalizedDistanceToContent(Lcom/google/android/apps/books/widget/DevicePageRendering;F)I

    move-result v2

    .line 4203
    .local v2, "pageThreshold":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Holder;

    move-result-object v8

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getTappedAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v6, p1, v7, v2, v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8500(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 4205
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_0

    .line 4206
    if-eqz p5, :cond_2

    .line 4207
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    .line 4208
    .local v1, "handle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v3

    .line 4209
    .local v3, "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-nez v3, :cond_1

    .line 4210
    const-string v5, "PagesViewHelper"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4211
    const-string v5, "PagesViewHelper"

    const-string v6, "Couldn\'t find spread containing touched annotation"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4224
    .end local v1    # "handle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v3    # "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    :cond_0
    :goto_0
    return v4

    .line 4215
    .restart local v1    # "handle":Lcom/google/android/apps/books/render/PageHandle;
    .restart local v3    # "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    :cond_1
    iget-object v4, v3, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    sget-object v6, Lcom/google/android/apps/books/app/MoveType;->CHOSE_HIGHLIGHT:Lcom/google/android/apps/books/app/MoveType;

    invoke-virtual {p0, v4, v6, v5}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->showSpreadInFullView(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Z)V

    .line 4219
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Holder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->editAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V
    invoke-static {v6, p1, p2, v0, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8600(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V

    .end local v1    # "handle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v3    # "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    :cond_2
    move v4, v5

    .line 4222
    goto :goto_0
.end method

.method public onPageTurnAnimationFinished()V
    .locals 4

    .prologue
    .line 3968
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$700(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3976
    :goto_0
    return-void

    .line 3973
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->PROCESS_PAGE_DELAY_MILLIS:J
    invoke-static {}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6800()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->delay(J)V

    .line 3975
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PageTurnController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->onAnimationFinished()V

    goto :goto_0
.end method

.method public onScrollStateChanged(I)V
    .locals 1
    .param p1, "scrollState"    # I

    .prologue
    .line 4229
    if-nez p1, :cond_0

    .line 4230
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigation()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8700(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 4232
    :cond_0
    return-void
.end method

.method public removeRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 4076
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/DispatchRendererListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/DispatchRendererListener;->removeListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 4077
    return-void
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p3, "consumer"    # Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;

    .prologue
    .line 4065
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/RenderPosition;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-direct {v1, v2, p3, p2, v3}, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/common/Position;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/render/Renderer;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    .line 4067
    return-void
.end method

.method public setActionBarElevation(F)V
    .locals 1
    .param p1, "elevation"    # F

    .prologue
    .line 4091
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setActionBarElevation(F)V

    .line 4092
    return-void
.end method

.method public setSystemBarsVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 4086
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setSystemBarsVisible(Z)V

    .line 4087
    return-void
.end method

.method public showSpreadInFullView(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Z)V
    .locals 8
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "moveType"    # Lcom/google/android/apps/books/app/MoveType;
    .param p3, "animate"    # Z

    .prologue
    .line 4120
    const/4 v1, 0x0

    .line 4121
    .local v1, "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz p1, :cond_0

    .line 4122
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v7

    .line 4123
    .local v7, "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v7, :cond_3

    .line 4124
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    invoke-interface {v0, v7, p1}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, p1

    .line 4131
    .end local v7    # "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$7600(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-object v3, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 4134
    if-eqz v1, :cond_1

    .line 4135
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onUserSelectedNewPosition()V

    .line 4137
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->dispatchSetUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4140
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setActionBarElevation(F)V

    .line 4141
    return-void

    .line 4124
    .restart local v7    # "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 4127
    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public showTableOfContents(I)V
    .locals 1
    .param p1, "tabIndex"    # I

    .prologue
    .line 4171
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->showTableOfContents(I)V

    .line 4172
    return-void
.end method
