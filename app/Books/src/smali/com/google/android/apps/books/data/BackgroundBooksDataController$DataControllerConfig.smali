.class public Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataControllerConfig"
.end annotation


# instance fields
.field private final coverHeight:I

.field private final syncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

.field private final thumbnailHeight:I


# direct methods
.method public constructor <init>(IILcom/google/android/apps/books/sync/SyncAccountsState;)V
    .locals 0
    .param p1, "coverHeight"    # I
    .param p2, "thumbnailHeight"    # I
    .param p3, "syncState"    # Lcom/google/android/apps/books/sync/SyncAccountsState;

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->coverHeight:I

    .line 146
    iput p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->thumbnailHeight:I

    .line 147
    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->syncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 148
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)Lcom/google/android/apps/books/sync/SyncAccountsState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->syncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->coverHeight:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->thumbnailHeight:I

    return v0
.end method

.method public static fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 152
    .local v1, "resources":Landroid/content/res/Resources;
    const v4, 0x7f0900f1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 153
    .local v0, "coverHeight":I
    const v4, 0x7f0900f2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 155
    .local v3, "thumbnailHeight":I
    new-instance v2, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    .line 156
    .local v2, "syncState":Lcom/google/android/apps/books/sync/SyncAccountsState;
    new-instance v4, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;

    invoke-direct {v4, v0, v3, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;-><init>(IILcom/google/android/apps/books/sync/SyncAccountsState;)V

    return-object v4
.end method
