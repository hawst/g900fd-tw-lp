.class public Lcom/google/android/apps/books/render/SpreadPageIdentifier;
.super Ljava/lang/Object;
.source "SpreadPageIdentifier.java"


# instance fields
.field public final pageIndex:I

.field public final spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V
    .locals 0
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "pageIndex"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 30
    iput p2, p0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    .line 31
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "spreadIdentifier"

    iget-object v2, p0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "pageIndex"

    iget v2, p0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
