.class public final Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;
.super Ljava/lang/Object;
.source "PlayCardAnimationHelper.java"

# interfaces
.implements Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FadeListener"
.end annotation


# instance fields
.field private final mView:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Landroid/view/View;)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->mView:Landroid/view/View;

    .line 215
    return-void
.end method


# virtual methods
.method public onVisibilityChangeBegin()V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method public onVisibilityChangeEnd()V
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->mView:Landroid/view/View;

    const v2, 0x7f0e003b

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    .local v0, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    # getter for: Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCurrentlyAnimatedFadeOut:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->access$200(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    # getter for: Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->access$100(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;->mView:Landroid/view/View;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->removeView(Landroid/view/View;)V

    .line 228
    .end local v0    # "volumeId":Ljava/lang/String;
    :cond_0
    return-void
.end method
