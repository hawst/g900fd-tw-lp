.class public Lcom/google/android/apps/books/net/NetworkBooksServer;
.super Ljava/lang/Object;
.source "NetworkBooksServer.java"

# interfaces
.implements Lcom/google/android/apps/books/net/BooksServer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;,
        Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;,
        Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;
    }
.end annotation


# static fields
.field private static final SEGMENT_RESOURCE_TYPES_TO_EXCLUDE:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SEPARATOR:[B

.field private static final UPGRADE_SEPARATOR:[B


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

.field private final mAppInfo:[B

.field private final mConfig:Lcom/google/android/apps/books/util/Config;

.field private mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mRandom:Ljava/util/Random;

.field private final mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

.field private final mServerConfig:Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 524
    const-string v0, ":"

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEPARATOR:[B

    .line 525
    const-string v0, ","

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->UPGRADE_SEPARATOR:[B

    .line 717
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEGMENT_RESOURCE_TYPES_TO_EXCLUDE:Ljava/util/Set;

    .line 723
    sget-object v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEGMENT_RESOURCE_TYPES_TO_EXCLUDE:Ljava/util/Set;

    const-string v1, "text/css"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 724
    sget-object v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEGMENT_RESOURCE_TYPES_TO_EXCLUDE:Ljava/util/Set;

    const-string v1, "smil"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 725
    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/api/ApiaryClient;Lcom/google/android/apps/books/net/ResponseGetter;Lcom/google/android/apps/books/util/Logger;Ljava/util/Random;Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p3, "apiaryClient"    # Lcom/google/android/apps/books/api/ApiaryClient;
    .param p4, "responseGetter"    # Lcom/google/android/apps/books/net/ResponseGetter;
    .param p5, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p6, "random"    # Ljava/util/Random;
    .param p7, "serverConfig"    # Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    .line 228
    iput-object p1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    .line 229
    iput-object p2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 230
    iput-object p3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    .line 231
    iput-object p4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    .line 232
    iput-object p5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 233
    # invokes: Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getAppInfo()[B
    invoke-static {p7}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->access$000(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAppInfo:[B

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAppInfo:[B

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "valid appInfo required"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 235
    iput-object p6, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mRandom:Ljava/util/Random;

    .line 236
    iput-object p7, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mServerConfig:Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .line 237
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static addCoverSizeParameters(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "remoteUrl"    # Ljava/lang/String;
    .param p1, "height"    # I

    .prologue
    .line 1042
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "zoom"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "h"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "usc"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildPageUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->buildPageUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildSessionKeyUpgradeRequest(Ljava/util/List;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 620
    .local p1, "sessionKeys":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v2

    .line 621
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/SessionKey;

    .line 622
    .local v1, "key":Lcom/google/android/apps/books/model/SessionKey;
    sget-object v3, Lcom/google/android/apps/books/net/NetworkBooksServer;->UPGRADE_SEPARATOR:[B

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 623
    iget-object v3, v1, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 625
    .end local v1    # "key":Lcom/google/android/apps/books/model/SessionKey;
    :cond_0
    invoke-static {v2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers(Ljava/util/Collection;)[B

    move-result-object v3

    return-object v3
.end method

.method private extractAndVerifyMetadata([B[B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .locals 6
    .param p1, "encryptedK_sClause"    # [B
    .param p2, "expectedNonce"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 637
    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "non-null expectedNonce required"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 639
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/net/NetworkBooksServer;->extractMetadata([B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;

    move-result-object v0

    .line 641
    .local v0, "meta":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    iget-object v1, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->nonce:[B

    invoke-static {v1, p2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->buffersEqual([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 642
    const-string v1, "BooksServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extractAndVerifyMetadata() expected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readLongString([B)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " actual: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->nonce:[B

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readLongString([B)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_sVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    new-instance v1, Lorg/apache/http/client/ClientProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server returned unexpected nonce \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->nonce:[B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' (expecting \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\')."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 637
    .end local v0    # "meta":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 650
    .restart local v0    # "meta":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    :cond_1
    return-object v0
.end method

.method private extractMetadata([B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .locals 3
    .param p1, "encryptedK_sClause"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 659
    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/books/util/EncryptionUtils;->extractK_sMetadata([B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    :try_end_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 660
    :catch_0
    move-exception v0

    .line 662
    .local v0, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    new-instance v1, Lorg/apache/http/client/ClientProtocolException;

    const-string v2, "Problem extracting K_s metadata from server response"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getPageDimensionParams()Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;
    .locals 4

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    if-nez v0, :cond_0

    .line 374
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;-><init>(Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mServerConfig:Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    # invokes: Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getMemoryClass()I
    invoke-static {v1}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->access$500(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mServerConfig:Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    # invokes: Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getShorterScreenLengths()Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->access$600(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)Landroid/graphics/Point;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mServerConfig:Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    # invokes: Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getLongerScreenLengths()Landroid/graphics/Point;
    invoke-static {v3}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->access$700(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)Landroid/graphics/Point;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/ReaderUtils;->getMaxPageImageWidth(ILandroid/graphics/Point;Landroid/graphics/Point;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->widthString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->access$302(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;Ljava/lang/String;)Ljava/lang/String;

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mDimensionParams:Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    return-object v0
.end method

.method private nextNonce()[B
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mRandom:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeLongString(J)[B

    move-result-object v0

    return-object v0
.end method

.method private parseResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resId"    # Ljava/lang/String;
    .param p3, "resourceType"    # Ljava/lang/String;
    .param p4, "entity"    # Lorg/apache/http/HttpEntity;
    .param p6, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .local p5, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const/4 v7, 0x0

    .line 861
    const-string v4, "BooksServer"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    .line 863
    .local v1, "isDebugLoggable":Z
    if-eqz v1, :cond_0

    .line 864
    const-string v4, "BooksServer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parsing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " resource response"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    :cond_0
    const-string v4, "text/css"

    invoke-static {p3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 874
    invoke-interface {p4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v2

    .line 875
    .local v2, "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResourceResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v3

    .line 876
    .local v3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getStyle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v0, v4, v7}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>([BLcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    .line 877
    .local v0, "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    invoke-static {p4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    .line 907
    .end local v2    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    :goto_0
    new-instance v4, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;

    invoke-direct {v4, v0, v3}, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;-><init>(Lcom/google/android/apps/books/net/EncryptedContentResponse;Ljava/util/List;)V

    return-object v4

    .line 878
    .end local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .end local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_1
    const-string v4, "image/svg+xml"

    invoke-static {p3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 879
    invoke-interface {p4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v2

    .line 880
    .restart local v2    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResourceResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v3

    .line 881
    .restart local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/ByteString;->newInput()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v0, v4, p5, v7}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    .line 882
    .restart local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    invoke-static {p4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 883
    .end local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .end local v2    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .end local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_2
    const-string v4, "smil"

    invoke-static {p3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 884
    invoke-interface {p4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v2

    .line 885
    .restart local v2    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    if-eqz v1, :cond_3

    .line 886
    const-string v4, "BooksServer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " audio resources in SMIL resource"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResourceResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v3

    .line 890
    .restart local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/ByteString;->newInput()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v0, v4, p5, v7}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    .line 891
    .restart local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    invoke-static {p4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 893
    .end local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .end local v2    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .end local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_4
    invoke-static {p3}, Lcom/google/android/apps/books/model/ResourceUtils;->isFontResourceType(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "audio"

    invoke-static {p3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 896
    :cond_5
    const/4 p5, 0x0

    .line 903
    :cond_6
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    invoke-direct {v0, p4, p5, v7}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    .line 904
    .restart local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    const/4 v3, 0x0

    .restart local v3    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    goto/16 :goto_0
.end method

.method private parseResourceResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;
    .locals 1
    .param p1, "pages"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 911
    invoke-static {p1}, Lcom/google/android/apps/books/util/BlockedContentReason;->assertNotBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)V

    .line 912
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private parseResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/Set;)Ljava/util/List;
    .locals 4
    .param p1, "pages"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 772
    .local p2, "typesToExclude":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 776
    .local v2, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 777
    .local v1, "resourceProto":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 779
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/books/sync/ResourceProtos;->resourceProtoToResource(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 783
    .end local v1    # "resourceProto":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    :cond_2
    return-object v2
.end method

.method private parseSegmentContent(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "sectionId"    # Ljava/lang/String;
    .param p4, "entity"    # Lorg/apache/http/HttpEntity;
    .param p6, "ignoreResources"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;Z)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 735
    .local p5, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-interface {p4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 736
    .local v3, "rawInput":Ljava/io/InputStream;
    invoke-static {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v2

    .line 739
    .local v2, "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    invoke-static {v2}, Lcom/google/android/apps/books/util/BlockedContentReason;->assertNotBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)V

    .line 742
    if-eqz p6, :cond_1

    .line 743
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 748
    .local v4, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :goto_0
    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 749
    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getStyle()Ljava/lang/String;

    move-result-object v1

    .line 750
    .local v1, "css":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 751
    const-string v5, "BooksServer"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 752
    const-string v5, "BooksServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unhandled inline CSS: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bytes"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    .end local v1    # "css":Ljava/lang/String;
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/protobuf/ByteString;->newInput()Ljava/io/InputStream;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v0, p4, v5, p5, v6}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    .line 760
    .local v0, "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    new-instance v5, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;

    invoke-direct {v5, v0, v4}, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;-><init>(Lcom/google/android/apps/books/net/EncryptedContentResponse;Ljava/util/List;)V

    return-object v5

    .line 745
    .end local v0    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .end local v4    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_1
    sget-object v5, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEGMENT_RESOURCE_TYPES_TO_EXCLUDE:Ljava/util/Set;

    invoke-direct {p0, v2, v5}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResources(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/Set;)Ljava/util/List;

    move-result-object v4

    .restart local v4    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    goto :goto_0
.end method

.method private prepareMetadataBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;
    .locals 3
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 474
    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/books/util/Config;->encryptedOceanUriBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 477
    .local v0, "remoteBuilder":Landroid/net/Uri$Builder;
    const-string v1, "avail_pid"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 478
    return-object v0
.end method

.method private retrieveEntity(Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Ljava/util/EnumSet;)Lorg/apache/http/HttpEntity;
    .locals 4
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p3, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;",
            ">;)",
            "Lorg/apache/http/HttpEntity;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
        }
    .end annotation

    .prologue
    .line 488
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p4, "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-static {p4}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlags(Ljava/util/EnumSet;)V

    .line 490
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 491
    .local v0, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V

    .line 492
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 494
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v1

    .end local v0    # "resp":Lorg/apache/http/HttpResponse;
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v1
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    .locals 6
    .param p1, "offerId"    # Ljava/lang/String;
    .param p3, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/app/DeviceInfo;",
            ")",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "volumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1209
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v3, :cond_0

    :goto_0
    const-string v5, "exactly one volume id required"

    invoke-static {v3, v5}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 1210
    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v5, p1, v3, p3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forAcceptOffer(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 1212
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v5, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v3, v2, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 1214
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {v1, v4}, Lcom/google/api/client/http/HttpRequest;->setThrowExceptionOnExecuteError(Z)Lcom/google/api/client/http/HttpRequest;

    .line 1215
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    invoke-interface {v3, v1, v4}, Lcom/google/android/apps/books/api/ApiaryClient;->executeRaw(Lcom/google/api/client/http/HttpRequest;Landroid/accounts/Account;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    .line 1217
    .local v0, "rawResponse":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1219
    sget-object v3, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;->SUCCESS:Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    .line 1221
    :goto_1
    return-object v3

    .end local v0    # "rawResponse":Lcom/google/api/client/http/HttpResponse;
    .end local v1    # "request":Lcom/google/api/client/http/HttpRequest;
    .end local v2    # "url":Lcom/google/api/client/http/GenericUrl;
    :cond_0
    move v3, v4

    .line 1209
    goto :goto_0

    .line 1221
    .restart local v0    # "rawResponse":Lcom/google/api/client/http/HttpResponse;
    .restart local v1    # "request":Lcom/google/api/client/http/HttpRequest;
    .restart local v2    # "url":Lcom/google/api/client/http/GenericUrl;
    :cond_1
    const-class v3, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    invoke-virtual {v0, v3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    goto :goto_1
.end method

.method public addCloudloadingVolume(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .locals 6
    .param p1, "contentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 932
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forCloudloadingAdd(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 933
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v3, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 934
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;

    return-object v2
.end method

.method public addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1154
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forAddVolumeToMyEBooksShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1156
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    const-string v2, "BooksServer"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1157
    const-string v2, "BooksServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding volume using request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1160
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v2, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->ADD_VOLUME:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v2}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1162
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 1164
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1166
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 1168
    return-void

    .line 1166
    :catchall_0
    move-exception v2

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v2

    .line 1162
    nop

    :array_0
    .array-data 4
        0xc8
        0xcc
    .end array-data
.end method

.method public deleteCloudloadedVolume(Ljava/lang/String;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 940
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forCloudloadingDelete(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 941
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v3, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 942
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 943
    return-void
.end method

.method public dismissOffer(Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)V
    .locals 6
    .param p1, "offerId"    # Ljava/lang/String;
    .param p2, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1198
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forDismissOffer(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1199
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v3, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1200
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 1201
    return-void
.end method

.method public getAudioResourceContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 5
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 917
    sget-object v2, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->FLOWING_TEXT:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->RESOURCE:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 919
    .local v1, "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-static {v1}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlags(Ljava/util/EnumSet;)V

    .line 921
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-interface {v2, p1, v3, v4}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 922
    .local v0, "result":Ljava/io/InputStream;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 925
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v0

    .end local v0    # "result":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v2
.end method

.method public getCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/CcBox;
    .locals 18
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 386
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->buildPageUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 387
    .local v11, "remoteUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v11, v1}, Lcom/google/android/apps/books/net/NetworkBooksServer;->prepareMetadataBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v9

    .line 388
    .local v9, "remoteBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 390
    .local v10, "remoteTarget":Ljava/lang/String;
    const/4 v4, 0x0

    .line 391
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    sget-object v15, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->ORIGINAL_PAGES:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    sget-object v16, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->METADATA:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static/range {v15 .. v16}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v14

    .line 393
    .local v14, "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-static {v14}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlags(Ljava/util/EnumSet;)V

    .line 395
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v15, v10, v0, v1}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 396
    .local v12, "resp":Lorg/apache/http/HttpResponse;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 397
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    .line 398
    .local v13, "status":I
    const/16 v15, 0xc8

    if-eq v13, v15, :cond_0

    .line 400
    new-instance v15, Lorg/apache/http/client/HttpResponseException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Unexpected server response "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " for "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v15, v13, v0}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v15
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    .end local v12    # "resp":Lorg/apache/http/HttpResponse;
    .end local v13    # "status":I
    :catch_0
    move-exception v3

    .line 427
    .local v3, "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :try_start_1
    new-instance v15, Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Unsupported session key version: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v3}, Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    .end local v3    # "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :catchall_0
    move-exception v15

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 433
    invoke-static {v4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    throw v15

    .line 403
    .restart local v12    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v13    # "status":I
    :cond_0
    :try_start_2
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V

    .line 405
    invoke-static {v4}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v8

    .line 406
    .local v8, "payloadRaw":Ljava/lang/String;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 408
    .local v7, "payload":Lorg/json/JSONObject;
    invoke-static {v7}, Lcom/google/android/apps/books/util/BlockedContentReason;->assertNotBlocked(Lorg/json/JSONObject;)V

    .line 411
    const-string v15, "page"

    invoke-virtual {v7, v15}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 412
    .local v6, "pages":Lorg/json/JSONArray;
    if-nez v6, :cond_1

    .line 413
    const/4 v15, 0x0

    .line 432
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 433
    invoke-static {v4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    :goto_0
    return-object v15

    .line 415
    :cond_1
    :try_start_3
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 416
    new-instance v15, Lorg/apache/http/client/ClientProtocolException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Asked for 1 page, starting at "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", received "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_3
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 429
    .end local v6    # "pages":Lorg/json/JSONArray;
    .end local v7    # "payload":Lorg/json/JSONObject;
    .end local v8    # "payloadRaw":Ljava/lang/String;
    .end local v12    # "resp":Lorg/apache/http/HttpResponse;
    .end local v13    # "status":I
    :catch_1
    move-exception v3

    .line 430
    .local v3, "e":Lorg/json/JSONException;
    :try_start_4
    new-instance v15, Lorg/apache/http/client/ClientProtocolException;

    const-string v16, "problem reading json pages"

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v3}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v15
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 419
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v6    # "pages":Lorg/json/JSONArray;
    .restart local v7    # "payload":Lorg/json/JSONObject;
    .restart local v8    # "payloadRaw":Ljava/lang/String;
    .restart local v12    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v13    # "status":I
    :cond_2
    const/4 v15, 0x0

    :try_start_5
    invoke-virtual {v6, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 420
    .local v5, "jsonPage":Lorg/json/JSONObject;
    const-string v15, "cc_box"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 421
    .local v2, "ccbox":Lorg/json/JSONObject;
    if-eqz v2, :cond_3

    .line 422
    new-instance v15, Lcom/google/android/apps/books/sync/JsonCcBox;

    invoke-direct {v15, v2}, Lcom/google/android/apps/books/sync/JsonCcBox;-><init>(Lorg/json/JSONObject;)V
    :try_end_5
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 432
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 433
    invoke-static {v4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 424
    :cond_3
    const/4 v15, 0x0

    .line 432
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 433
    invoke-static {v4}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    goto :goto_0
.end method

.method public getCoverImage(Ljava/lang/String;I)Ljava/io/InputStream;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1052
    invoke-static {p1, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->addCoverSizeParameters(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 1053
    .local v3, "urlWithSizeParameters":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/util/Config;->prepareForOcean(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1054
    .local v1, "finalUrl":Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->VOLUME_COVER:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v4}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1056
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v4, v1, v5, v6}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 1058
    .local v0, "entity":Lorg/apache/http/HttpEntity;
    if-nez v0, :cond_0

    .line 1059
    new-instance v4, Lorg/apache/http/client/ClientProtocolException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "missing cover response for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1065
    .end local v0    # "entity":Lorg/apache/http/HttpEntity;
    :catchall_0
    move-exception v4

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v4

    .line 1061
    .restart local v0    # "entity":Lorg/apache/http/HttpEntity;
    :cond_0
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 1062
    .local v2, "result":Ljava/io/InputStream;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1065
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v2
.end method

.method public getDictionaryMetadata(Ljava/lang/String;)Ljava/util/List;
    .locals 18
    .param p1, "keyVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1227
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1229
    .local v16, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forDictionaryMetadata(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v17

    .line 1230
    .local v17, "url":Lcom/google/api/client/http/GenericUrl;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v14

    .line 1231
    .local v14, "request":Lcom/google/api/client/http/HttpRequest;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadataResponse;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v14, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadataResponse;

    .line 1234
    .local v15, "response":Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadataResponse;
    iget-object v2, v15, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadataResponse;->metadata:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;

    .line 1235
    .local v13, "metadata":Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;
    new-instance v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    iget-object v3, v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;->language:Ljava/lang/String;

    iget-wide v4, v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;->version:J

    iget-object v6, v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;->encrypted_key:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    iget-object v7, v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;->download_url:Ljava/lang/String;

    iget-wide v8, v13, Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;->size:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    iget-object v11, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v10, p1

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;-><init>(Ljava/lang/String;J[BLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1241
    .end local v13    # "metadata":Lcom/google/android/apps/books/api/data/ApiaryDictionaryMetadata;
    :cond_0
    return-object v16
.end method

.method public getFailedCloudeloadedVolumes()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 948
    iget-object v6, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v6}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forCloudloadingGetFailed(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v4

    .line 949
    .local v4, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v6, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v6, v4}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 950
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v6, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;

    iget-object v8, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v9, 0x0

    new-array v9, v9, [I

    invoke-interface {v6, v1, v7, v8, v9}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/ApiaryVolumes;

    .line 952
    .local v2, "response":Lcom/google/android/apps/books/api/data/ApiaryVolumes;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 953
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    iget v6, v2, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->totalItems:I

    if-lez v6, :cond_0

    .line 954
    iget-object v6, v2, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 955
    .local v5, "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 958
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v5    # "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    :cond_0
    return-object v3
.end method

.method public getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ">;)",
            "Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 530
    .local p1, "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    const-wide/16 v28, 0x3e8

    div-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v19, v0

    .line 531
    .local v19, "timestamp":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/net/NetworkBooksServer;->nextNonce()[B

    move-result-object v15

    .line 533
    .local v15, "nonce":[B
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeIntString(I)[B

    move-result-object v20

    .line 535
    .local v20, "timestampBytes":[B
    const/16 v25, 0x5

    move/from16 v0, v25

    new-array v0, v0, [[B

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v15, v25, v26

    const/16 v26, 0x1

    sget-object v27, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEPARATOR:[B

    aput-object v27, v25, v26

    const/16 v26, 0x2

    aput-object v20, v25, v26

    const/16 v26, 0x3

    sget-object v27, Lcom/google/android/apps/books/net/NetworkBooksServer;->SEPARATOR:[B

    aput-object v27, v25, v26

    const/16 v26, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAppInfo:[B

    move-object/from16 v27, v0

    aput-object v27, v25, v26

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers([[B)[B

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/books/util/EncryptionUtils;->E_r([B)[B

    move-result-object v5

    .line 539
    .local v5, "appInfoClause":[B
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/util/CollectionUtils;->size(Ljava/util/Collection;)I

    move-result v22

    .line 540
    .local v22, "upgradeCount":I
    if-lez v22, :cond_1

    .line 541
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/net/NetworkBooksServer;->buildSessionKeyUpgradeRequest(Ljava/util/List;)[B

    move-result-object v21

    .line 542
    .local v21, "upgradeBody":[B
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [[B

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v5, v25, v26

    const/16 v26, 0x1

    aput-object v21, v25, v26

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers([[B)[B

    move-result-object v4

    .line 547
    .end local v21    # "upgradeBody":[B
    .local v4, "activateClause":[B
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v25

    const-string v26, "books"

    invoke-virtual/range {v25 .. v26}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v25

    const-string v26, "cp_activate"

    invoke-static {v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v16

    .line 550
    .local v16, "requestUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    move-object/from16 v25, v0

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move/from16 v0, v28

    new-array v0, v0, [I

    move-object/from16 v28, v0

    invoke-interface/range {v25 .. v28}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v17

    .line 552
    .local v17, "resp":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 555
    .local v9, "entity":Lorg/apache/http/HttpEntity;
    :try_start_0
    invoke-static {v9}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v8

    .line 556
    .local v8, "encryptedK_sClauses":[B
    sget-object v25, Lcom/google/android/apps/books/net/NetworkBooksServer;->UPGRADE_SEPARATOR:[B

    move-object/from16 v0, v25

    invoke-static {v8, v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->splitBufferUsing([B[B)Ljava/util/ArrayList;

    move-result-object v7

    .line 562
    .local v7, "clauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 563
    .local v6, "clauseCount":I
    add-int/lit8 v25, v22, 0x1

    move/from16 v0, v25

    if-eq v6, v0, :cond_0

    .line 564
    const-string v25, "BooksServer"

    const/16 v26, 0x5

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 565
    const-string v25, "BooksServer"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unexpected number of clauses key upgrade: expected "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    add-int/lit8 v27, v22, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " but got "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_0
    new-instance v18, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;-><init>()V

    .line 573
    .local v18, "result":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [B

    .line 574
    .local v11, "newEncryptedKey":[B
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v15}, Lcom/google/android/apps/books/net/NetworkBooksServer;->extractAndVerifyMetadata([B[B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;

    move-result-object v14

    .line 575
    .local v14, "newKeyMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    new-instance v25, Lcom/google/android/apps/books/model/SessionKey;

    iget v0, v14, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_rVersion:I

    move/from16 v26, v0

    iget-object v0, v14, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_sVersion:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/apps/books/model/SessionKey;-><init>(ILjava/lang/String;[B)V

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;->newKey:Lcom/google/android/apps/books/model/SessionKey;

    .line 578
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;->oldKeyToNewKey:Ljava/util/Map;

    .line 581
    add-int/lit8 v25, v6, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 583
    .local v10, "matchedUpgradeCount":I
    if-lez v10, :cond_3

    .line 584
    const/16 v23, 0x1

    .local v23, "upgradeResponseIndex":I
    :goto_1
    move/from16 v0, v23

    if-gt v0, v10, :cond_3

    .line 586
    move/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [B

    .line 587
    .local v13, "newKeyBlob":[B
    array-length v0, v13

    move/from16 v25, v0

    if-nez v25, :cond_2

    .line 585
    :goto_2
    add-int/lit8 v23, v23, 0x1

    goto :goto_1

    .line 544
    .end local v4    # "activateClause":[B
    .end local v6    # "clauseCount":I
    .end local v7    # "clauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v8    # "encryptedK_sClauses":[B
    .end local v9    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "matchedUpgradeCount":I
    .end local v11    # "newEncryptedKey":[B
    .end local v13    # "newKeyBlob":[B
    .end local v14    # "newKeyMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .end local v16    # "requestUri":Landroid/net/Uri;
    .end local v17    # "resp":Lorg/apache/http/HttpResponse;
    .end local v18    # "result":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .end local v23    # "upgradeResponseIndex":I
    :cond_1
    move-object v4, v5

    .restart local v4    # "activateClause":[B
    goto/16 :goto_0

    .line 594
    .restart local v6    # "clauseCount":I
    .restart local v7    # "clauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .restart local v8    # "encryptedK_sClauses":[B
    .restart local v9    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "matchedUpgradeCount":I
    .restart local v11    # "newEncryptedKey":[B
    .restart local v13    # "newKeyBlob":[B
    .restart local v14    # "newKeyMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .restart local v16    # "requestUri":Landroid/net/Uri;
    .restart local v17    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v18    # "result":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .restart local v23    # "upgradeResponseIndex":I
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v15}, Lcom/google/android/apps/books/net/NetworkBooksServer;->extractAndVerifyMetadata([B[B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;

    move-result-object v24

    .line 596
    .local v24, "upgradeResponseMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    new-instance v12, Lcom/google/android/apps/books/model/SessionKey;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_rVersion:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_sVersion:Ljava/lang/String;

    move-object/from16 v26, v0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v12, v0, v1, v13}, Lcom/google/android/apps/books/model/SessionKey;-><init>(ILjava/lang/String;[B)V

    .line 598
    .local v12, "newKey":Lcom/google/android/apps/books/model/SessionKey;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;->oldKeyToNewKey:Ljava/util/Map;

    move-object/from16 v25, v0

    add-int/lit8 v26, v23, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 604
    .end local v6    # "clauseCount":I
    .end local v7    # "clauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v8    # "encryptedK_sClauses":[B
    .end local v10    # "matchedUpgradeCount":I
    .end local v11    # "newEncryptedKey":[B
    .end local v12    # "newKey":Lcom/google/android/apps/books/model/SessionKey;
    .end local v13    # "newKeyBlob":[B
    .end local v14    # "newKeyMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .end local v18    # "result":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .end local v23    # "upgradeResponseIndex":I
    .end local v24    # "upgradeResponseMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    :catchall_0
    move-exception v25

    invoke-static {v9}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    throw v25

    .restart local v6    # "clauseCount":I
    .restart local v7    # "clauses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .restart local v8    # "encryptedK_sClauses":[B
    .restart local v10    # "matchedUpgradeCount":I
    .restart local v11    # "newEncryptedKey":[B
    .restart local v14    # "newKeyMetadata":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .restart local v18    # "result":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    :cond_3
    invoke-static {v9}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    return-object v18
.end method

.method public getOffers(Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .locals 6
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1191
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchOffers(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1192
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1193
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    return-object v2
.end method

.method public getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 329
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const-string v0, "BooksServer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    const-string v0, "BooksServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fetching volume "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", page ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/net/NetworkBooksServer;->retrieveImageEntity(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 333
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;

    move-object v1, p0

    move-object v3, p3

    move-object v4, v2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/net/NetworkBooksServer$1;-><init>(Lcom/google/android/apps/books/net/NetworkBooksServer;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-object v0
.end method

.method public getPageStructure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 444
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const-string v1, "BooksServer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    const-string v1, "BooksServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fetching structure for volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", page ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/net/NetworkBooksServer;->retrieveStructureEntity(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 448
    .local v0, "entity":Lorg/apache/http/HttpEntity;
    new-instance v1, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p3, v2}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    return-object v1
.end method

.method public getResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 17
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 790
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->urlToResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 792
    .local v3, "resId":Ljava/lang/String;
    const-string v1, "missing volume ID"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    const-string v1, "missing resource ID"

    invoke-static {v3, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    const-string v1, "BooksServer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    invoke-static {v3}, Lcom/google/android/apps/books/util/StringUtils;->summarizeForLogging(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 797
    .local v15, "shortResId":Ljava/lang/String;
    const-string v1, "BooksServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fetching volume "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", resource ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    .end local v15    # "shortResId":Ljava/lang/String;
    :cond_0
    if-eqz p2, :cond_1

    .line 802
    const-string v1, "&hk=1"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 807
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/util/Config;->encryptedOceanUriBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v10

    .line 809
    .local v10, "remoteBuilder":Landroid/net/Uri$Builder;
    const-string v1, "text/css"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "smil"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "image/svg+xml"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 812
    :cond_2
    const-string v1, "alt"

    const-string v2, "proto"

    invoke-virtual {v10, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 815
    :cond_3
    const-string v1, "audio"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 816
    invoke-static {v10}, Lcom/google/android/apps/books/util/MediaUrls;->restrictFormatToMp3Mp4(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    .line 818
    :cond_4
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 820
    .local v11, "remoteTarget":Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->FLOWING_TEXT:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    sget-object v2, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->RESOURCE:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v16

    .line 822
    .local v16, "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlags(Ljava/util/EnumSet;)V

    .line 823
    const/4 v5, 0x0

    .line 825
    .local v5, "entity":Lorg/apache/http/HttpEntity;
    const/4 v14, 0x0

    .line 827
    .local v14, "returned":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-interface {v1, v11, v2, v4}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 828
    .local v12, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V

    .line 829
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 833
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v9

    .line 834
    .local v9, "entityContentType":Lorg/apache/http/Header;
    invoke-interface {v9}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 835
    .local v8, "contentType":Ljava/lang/String;
    const-string v1, "image/gif"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 836
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Server returned \'image not available\' gif for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and key "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 847
    .end local v8    # "contentType":Ljava/lang/String;
    .end local v9    # "entityContentType":Lorg/apache/http/Header;
    .end local v12    # "resp":Lorg/apache/http/HttpResponse;
    :catchall_0
    move-exception v1

    if-nez v14, :cond_5

    .line 848
    invoke-static {v5}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    .line 850
    :cond_5
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v1

    .line 841
    .restart local v8    # "contentType":Ljava/lang/String;
    .restart local v9    # "entityContentType":Lorg/apache/http/Header;
    .restart local v12    # "resp":Lorg/apache/http/HttpResponse;
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 844
    .local v13, "result":Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    const/4 v14, 0x1

    .line 847
    if-nez v14, :cond_7

    .line 848
    invoke-static {v5}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    .line 850
    :cond_7
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v13
.end method

.method public getSampleCategories(Ljava/util/Locale;)Lcom/google/android/apps/books/api/data/SampleCategories;
    .locals 7
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1246
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOnboardingCategories(Lcom/google/android/apps/books/util/Config;Ljava/util/Locale;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 1247
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1249
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/data/SampleCategories;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/api/data/SampleCategories;

    .line 1251
    .local v1, "sampleCategories":Lcom/google/android/apps/books/api/data/SampleCategories;
    return-object v1
.end method

.method public getSampleVolumes(Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/android/apps/books/api/data/SampleVolumes;
    .locals 7
    .param p1, "locale"    # Ljava/util/Locale;
    .param p3, "pageSize"    # I
    .param p4, "nextPageToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/api/data/SampleVolumes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1257
    .local p2, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3, p1, p2, p3, p4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forOnboardingVolumes(Lcom/google/android/apps/books/util/Config;Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 1259
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1261
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/data/SampleVolumes;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/api/data/SampleVolumes;

    .line 1263
    .local v1, "sampleVolumes":Lcom/google/android/apps/books/api/data/SampleVolumes;
    return-object v1
.end method

.method public getSegmentContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 13
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .param p3, "segmentUrl"    # Ljava/lang/String;
    .param p5, "widthString"    # Ljava/lang/String;
    .param p6, "ignoreResources"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 672
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const-string v1, "missing volume Id"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    const-string v1, "missing segment ID"

    invoke-static {p2, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674
    const-string v1, "missing segment URL"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    const-string v1, "BooksServer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    const-string v1, "BooksServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fetching volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", segment ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/util/Config;->encryptedOceanUriBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v9

    .line 685
    .local v9, "remoteBuilder":Landroid/net/Uri$Builder;
    const-string v1, "w"

    move-object/from16 v0, p5

    invoke-virtual {v9, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 687
    const-string v1, "alt"

    const-string v2, "proto"

    invoke-virtual {v9, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 688
    const-string v1, "inline"

    const-string v2, "0"

    invoke-virtual {v9, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 690
    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 692
    .local v10, "remoteTarget":Ljava/lang/String;
    const/4 v5, 0x0

    .line 693
    .local v5, "entity":Lorg/apache/http/HttpEntity;
    sget-object v12, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->FLOWING_TEXT:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    .line 694
    .local v12, "trafficFlag":Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;
    invoke-static {v12}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 696
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-interface {v1, v10, v2, v3}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 697
    .local v11, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V

    .line 698
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 700
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v6, p4

    move/from16 v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/net/NetworkBooksServer;->parseSegmentContent(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 706
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 713
    invoke-static {v5}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    return-object v1

    .line 702
    .end local v11    # "resp":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v8

    .line 703
    .local v8, "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :try_start_1
    new-instance v1, Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported session key version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v8}, Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 706
    .end local v8    # "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 713
    invoke-static {v5}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    throw v1
.end method

.method public getSharedFontContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1008
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/util/Config;->prepareForOcean(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1009
    .local v1, "oceanUrl":Ljava/lang/String;
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1012
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    const-string v3, "Accept-Charset"

    const-string v4, "utf-8"

    invoke-virtual {v0, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->SHARED_FONTS:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1015
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/apps/books/net/ResponseGetter;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 1017
    .local v2, "result":Ljava/io/InputStream;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1020
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v2

    .end local v2    # "result":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v3
.end method

.method public getUserSettings()Lcom/google/android/apps/books/api/data/UserSettings;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1268
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forGetUserSettings(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1269
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v3, v1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1270
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->USER_SETTINGS:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1273
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/data/UserSettings;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/UserSettings;

    .line 1274
    .local v2, "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1277
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v2

    .end local v2    # "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v3
.end method

.method public getVolumeManifest(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 10
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 963
    iget-object v7, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v7, p1}, Lcom/google/android/apps/books/util/OceanUris;->getManifestUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 964
    .local v2, "manifestUrl":Ljava/lang/String;
    const-string v7, "BooksServer"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 965
    const-string v7, "BooksServer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Fetch URL = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    :cond_0
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 971
    .local v3, "req":Lorg/apache/http/client/methods/HttpGet;
    const-string v7, "Accept"

    const-string v8, "application/xml,application/xhtml+xml,text/xml,image/svg+xml"

    invoke-virtual {v3, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const-string v7, "Accept-Charset"

    const-string v8, "utf-8"

    invoke-virtual {v3, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    sget-object v6, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->VOLUME_MANIFEST:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    .line 975
    .local v6, "trafficFlag":Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;
    invoke-static {v6}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 977
    iget-object v7, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v8, "download manifest"

    new-instance v9, Lcom/google/android/apps/books/net/NetworkBooksServer$2;

    invoke-direct {v9, p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$2;-><init>(Lcom/google/android/apps/books/net/NetworkBooksServer;)V

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v5

    .line 986
    .local v5, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    iget-object v8, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v9, 0x0

    new-array v9, v9, [I

    invoke-interface {v7, v3, v8, v9}, Lcom/google/android/apps/books/net/ResponseGetter;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 987
    .local v4, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V

    .line 988
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v0

    .line 991
    .local v0, "entity":Lorg/apache/http/HttpEntity;
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 993
    .local v1, "input":Ljava/io/InputStream;
    :try_start_2
    iget-object v7, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v7}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseManifest(Ljava/io/InputStream;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/VolumeManifest;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    .line 995
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 998
    :cond_1
    :try_start_4
    invoke-static {v0}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1001
    invoke-interface {v5}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1002
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v7

    .line 995
    :catchall_0
    move-exception v7

    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 998
    .end local v1    # "input":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    :try_start_6
    invoke-static {v0}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    throw v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1001
    .end local v0    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "resp":Lorg/apache/http/HttpResponse;
    :catchall_2
    move-exception v7

    invoke-interface {v5}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1002
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v7
.end method

.method public getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolume(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 242
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v3, v1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 243
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->VOLUME_OVERVIEW:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 245
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 247
    .local v2, "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v2

    .end local v2    # "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v3
.end method

.method public releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 500
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->RELEASE_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 502
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    iget-object v4, p2, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-static {v3, p1, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forReleaseDownloadAccess(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 504
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v4, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 507
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v1, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 508
    const-string v3, "BooksServer"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 509
    const-string v3, "BooksServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/releaseDownloadAccess license "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 521
    .end local v1    # "request":Lcom/google/api/client/http/HttpRequest;
    .end local v2    # "url":Lcom/google/api/client/http/GenericUrl;
    :goto_0
    return-void

    .line 512
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v3, "BooksServer"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 515
    const-string v3, "BooksServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/releaseDownloadAccess failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/LogUtil;->maybeScrubUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 519
    :cond_1
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v3
.end method

.method public removeVolumeFromMyEbooks(Ljava/lang/String;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 1173
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRemovingVolumeFromMyEBooksShelf(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1175
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    const-string v2, "BooksServer"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1176
    const-string v2, "BooksServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleting volume using request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1178
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1179
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v2, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->DELETE_VOLUME:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v2}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1181
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x3

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 1183
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1185
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 1187
    return-void

    .line 1185
    :catchall_0
    move-exception v2

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v2

    .line 1181
    nop

    :array_0
    .array-data 4
        0xc8
        0xcc
        0x194
    .end array-data
.end method

.method public requestVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    .locals 15
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1072
    new-instance v4, Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getRandom()Ljava/util/Random;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Random;->nextLong()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeLongString(J)[B

    move-result-object v11

    invoke-direct {v4, v11}, Ljava/lang/String;-><init>([B)V

    .line 1073
    .local v4, "nonce":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v11, v0, v12, v4, v1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forRequestAccess(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v10

    .line 1077
    .local v10, "url":Lcom/google/api/client/http/GenericUrl;
    sget-object v11, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->CHECK_ONLINE_ACCESS:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v11}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1079
    :try_start_0
    iget-object v11, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v12, Lcom/google/android/apps/books/api/ApiaryClientImpl;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v11, v10, v12}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v7

    .line 1082
    .local v7, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v11, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v12, Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    iget-object v13, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v14, 0x0

    new-array v14, v14, [I

    invoke-interface {v11, v7, v12, v13, v14}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    .line 1090
    .local v8, "resp":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1094
    :try_start_1
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-static {v11}, Lcom/google/android/apps/books/util/EncryptionUtils;->extractSessionKeyInHexFormat([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 1100
    .local v9, "sessionKeyHexFormat":Ljava/lang/String;
    if-eqz v8, :cond_0

    :try_start_2
    iget-object v11, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    if-eqz v11, :cond_0

    .line 1101
    iget-object v11, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v11}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    invoke-static {v4, v11, v9, v12}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->verifySignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/data/VerifiableResponse;)V

    .line 1105
    :cond_0
    const-string v11, "BooksServer"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1106
    const/4 v6, 0x0

    .line 1107
    .local v6, "reportFailure":Z
    const/4 v5, 0x0

    .line 1108
    .local v5, "outcome":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 1109
    iget-object v11, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    if-eqz v11, :cond_1

    .line 1110
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "d="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    iget-boolean v12, v12, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->deviceAllowed:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1111
    iget-object v11, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    iget-boolean v11, v11, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->deviceAllowed:Z

    if-nez v11, :cond_1

    .line 1112
    const/4 v6, 0x1

    .line 1115
    :cond_1
    iget-object v11, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->concurrentAccess:Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;

    if-eqz v11, :cond_2

    .line 1116
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v5, :cond_5

    const-string v11, ""

    :goto_0
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "c="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->concurrentAccess:Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;

    iget-boolean v12, v12, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->deviceAllowed:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1118
    iget-object v11, v8, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->concurrentAccess:Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;

    iget-boolean v11, v11, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->deviceAllowed:Z

    if-nez v11, :cond_2

    .line 1119
    const/4 v6, 0x1

    .line 1123
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Req license "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1125
    .local v3, "message":Ljava/lang/String;
    if-eqz v6, :cond_6

    .line 1126
    const-string v11, "BooksServer"

    invoke-static {v11, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1147
    .end local v3    # "message":Ljava/lang/String;
    .end local v5    # "outcome":Ljava/lang/String;
    .end local v6    # "reportFailure":Z
    :cond_3
    :goto_1
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v8

    .line 1096
    .end local v9    # "sessionKeyHexFormat":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1097
    .local v2, "e":Ljava/security/GeneralSecurityException;
    :try_start_3
    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11
    :try_end_3
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1133
    .end local v2    # "e":Ljava/security/GeneralSecurityException;
    .end local v7    # "request":Lcom/google/api/client/http/HttpRequest;
    .end local v8    # "resp":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    :catch_1
    move-exception v2

    .line 1135
    .local v2, "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :try_start_4
    const-string v11, "BooksServer"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1136
    const-string v11, "BooksServer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Expired session key in /requestAccess apiary call "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " for "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1139
    :cond_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1147
    .end local v2    # "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :catchall_0
    move-exception v11

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v11

    .line 1116
    .restart local v5    # "outcome":Ljava/lang/String;
    .restart local v6    # "reportFailure":Z
    .restart local v7    # "request":Lcom/google/api/client/http/HttpRequest;
    .restart local v8    # "resp":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    .restart local v9    # "sessionKeyHexFormat":Ljava/lang/String;
    :cond_5
    :try_start_5
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    .line 1127
    .restart local v3    # "message":Ljava/lang/String;
    :cond_6
    const-string v11, "BooksServer"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1128
    const-string v11, "BooksServer"

    invoke-static {v11, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1140
    .end local v3    # "message":Ljava/lang/String;
    .end local v5    # "outcome":Ljava/lang/String;
    .end local v6    # "reportFailure":Z
    .end local v7    # "request":Lcom/google/api/client/http/HttpRequest;
    .end local v8    # "resp":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    .end local v9    # "sessionKeyHexFormat":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 1141
    .local v2, "e":Ljava/io/IOException;
    :try_start_6
    const-string v11, "BooksServer"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1142
    const-string v11, "BooksServer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error making /requestAccess apiary call "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " for "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1145
    :cond_7
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public retrieveImageEntity(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lorg/apache/http/HttpEntity;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lorg/apache/http/HttpEntity;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
        }
    .end annotation

    .prologue
    .line 352
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getRemoteUrl()Ljava/lang/String;

    move-result-object v3

    .line 354
    .local v3, "remoteUrl":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {p3}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Lcom/google/android/apps/books/util/Config;->encryptedOceanUriBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 358
    .local v1, "remoteBuilder":Landroid/net/Uri$Builder;
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer;->getPageDimensionParams()Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    move-result-object v0

    .line 359
    .local v0, "dims":Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;
    # getter for: Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->heightString:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->access$200(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 360
    const-string v5, "h"

    # getter for: Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->heightString:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->access$200(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 366
    :goto_0
    sget-object v5, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->ORIGINAL_PAGES:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    .line 367
    .local v4, "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 369
    .local v2, "remoteTarget":Ljava/lang/String;
    invoke-direct {p0, v2, p3, p1, v4}, Lcom/google/android/apps/books/net/NetworkBooksServer;->retrieveEntity(Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Ljava/util/EnumSet;)Lorg/apache/http/HttpEntity;

    move-result-object v5

    return-object v5

    .line 362
    .end local v2    # "remoteTarget":Ljava/lang/String;
    .end local v4    # "trafficFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    :cond_0
    const-string v5, "w"

    # getter for: Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->widthString:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->access$300(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public retrieveStructureEntity(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lorg/apache/http/HttpEntity;
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lorg/apache/http/HttpEntity;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
        }
    .end annotation

    .prologue
    .line 454
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer;->buildPageUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 455
    .local v2, "remoteUrl":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/net/NetworkBooksServer;->prepareMetadataBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 459
    .local v0, "remoteBuilder":Landroid/net/Uri$Builder;
    const-string v3, "alt"

    const-string v4, "proto"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 461
    const-string v3, "ps"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 463
    const-string v3, "enc_all"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 465
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 467
    .local v1, "remoteTarget":Ljava/lang/String;
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->PAGE_STRUCTURE:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {p0, v1, p3, p1, v3}, Lcom/google/android/apps/books/net/NetworkBooksServer;->retrieveEntity(Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Ljava/util/EnumSet;)Lorg/apache/http/HttpEntity;

    move-result-object v3

    return-object v3
.end method

.method public syncVolumeLicenses(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/android/apps/books/model/SessionKey;)Ljava/util/List;
    .locals 18
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "volumeIdsToAcquire":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "volumeIdsWithLicenseAcquired":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getRandom()Ljava/util/Random;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/Random;->nextLong()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 259
    .local v4, "nonce":Ljava/lang/String;
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    .line 261
    .local v9, "sessionKeyVer":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v14, v9, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forMyEBooksAndSyncLicenses(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v10

    .line 264
    .local v10, "url":Lcom/google/api/client/http/GenericUrl;
    new-instance v6, Lcom/google/android/apps/books/api/data/SyncVolumeLicensesRequest;

    invoke-direct {v6}, Lcom/google/android/apps/books/api/data/SyncVolumeLicensesRequest;-><init>()V

    .line 265
    .local v6, "requestData":Lcom/google/android/apps/books/api/data/SyncVolumeLicensesRequest;
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 266
    .local v12, "volumeId":Ljava/lang/String;
    invoke-virtual {v6, v12}, Lcom/google/android/apps/books/api/data/SyncVolumeLicensesRequest;->addVolumeId(Ljava/lang/String;)V

    goto :goto_0

    .line 269
    .end local v12    # "volumeId":Ljava/lang/String;
    :cond_0
    const-string v14, "BooksServer"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 270
    const-string v14, "BooksServer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "syncVolumeLicenses volumeIdsToAcquire: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v14, v10, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v5

    .line 275
    .local v5, "request":Lcom/google/api/client/http/HttpRequest;
    const/4 v7, 0x0

    .line 276
    .local v7, "response":Lcom/google/android/apps/books/api/data/ApiaryVolumes;
    sget-object v14, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->MY_EBOOKS:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v14}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 278
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v15, Lcom/google/android/apps/books/api/data/ApiaryVolumes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v14, v5, v15, v0, v1}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryVolumes;

    move-object v7, v0

    .line 279
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 283
    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-static {v14}, Lcom/google/android/apps/books/util/EncryptionUtils;->extractSessionKeyInHexFormat([B)Ljava/lang/String;

    move-result-object v8

    .line 286
    .local v8, "sessionKeyHexFormat":Ljava/lang/String;
    const-string v14, "BooksServer"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 287
    iget-object v14, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    if-eqz v14, :cond_4

    .line 288
    const-string v14, "BooksServer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "apiary result contains "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " volumes"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_2
    :goto_1
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v13

    .line 295
    .local v13, "volumesInMyEbooks":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v14, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    if-eqz v14, :cond_5

    .line 296
    iget-object v14, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 297
    .local v11, "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    iget-object v14, v11, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    invoke-interface {v13, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 298
    iget-object v14, v11, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v2, v14, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    .line 299
    .local v2, "downloadAccess":Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    if-eqz v2, :cond_3

    .line 301
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v14}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v14

    invoke-static {v4, v14, v8, v2}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->verifySignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/data/VerifiableResponse;)V

    .line 303
    iget-boolean v14, v2, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->deviceAllowed:Z

    if-eqz v14, :cond_3

    .line 304
    iget-object v14, v11, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 281
    .end local v2    # "downloadAccess":Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    .end local v8    # "sessionKeyHexFormat":Ljava/lang/String;
    .end local v11    # "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .end local v13    # "volumesInMyEbooks":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v14

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v14

    .line 290
    .restart local v8    # "sessionKeyHexFormat":Ljava/lang/String;
    :cond_4
    const-string v14, "BooksServer"

    const-string v15, "apiary result has null volumes"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 309
    .restart local v13    # "volumesInMyEbooks":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_5
    iget v14, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->totalItems:I

    if-lez v14, :cond_7

    .line 310
    const-string v14, "BooksServer"

    const/4 v15, 0x6

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 311
    const-string v14, "BooksServer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "apiary response.volumes is null but totalItems is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->totalItems:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_6
    new-instance v14, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    const-string v15, "Null response.volumes in server response"

    invoke-direct {v14, v15}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 318
    :cond_7
    const-string v14, "BooksServer"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 319
    const-string v14, "BooksServer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "syncVolumeLicenses volumeIdsWithLicenseAcquired: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_8
    iget-object v14, v7, Lcom/google/android/apps/books/api/data/ApiaryVolumes;->volumes:Ljava/util/List;

    return-object v14
.end method

.method public updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;)Lcom/google/android/apps/books/api/data/UserSettings;
    .locals 7
    .param p1, "newUserSettings"    # Lcom/google/android/apps/books/api/data/UserSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1284
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forUpdateUserSettings(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 1285
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-string v4, "POST"

    invoke-interface {v3, v4, v1, p1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeJsonRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 1287
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v3, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->USER_SETTINGS:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v3}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 1290
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/api/data/UserSettings;

    iget-object v5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/UserSettings;

    .line 1291
    .local v2, "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1294
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    return-object v2

    .end local v2    # "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v3
.end method
