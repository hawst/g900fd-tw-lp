.class public Lcom/google/android/apps/books/app/AppSettingsActivity;
.super Lcom/google/android/ublib/actionbar/UBLibActivity;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;
.implements Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$NetworkTts;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;,
        Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;
    }
.end annotation


# instance fields
.field private mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

.field private mIsNetworkTtsSupported:Z

.field private mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

.field private mTts:Landroid/speech/tts/TextToSpeech;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    .line 557
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/AppSettingsActivity;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity;
    .param p1, "x1"    # Landroid/speech/tts/TextToSpeech;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/app/AppSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/AppSettingsActivity;)Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V

    return-void
.end method

.method private static logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V
    .locals 6
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 670
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 671
    return-void
.end method


# virtual methods
.method public notifySettingChanged()V
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->setRequestedOrientation(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;->notifyDataSetChanged()V

    .line 198
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    if-eqz p1, :cond_0

    .line 111
    const-string v3, "mIsNetworkTtsSupported"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z

    .line 114
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/AppSettingsActivity;->setRequestedOrientation(I)V

    .line 115
    const v3, 0x7f040025

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/AppSettingsActivity;->setContentView(I)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/apps/books/util/StyleUtils;->configureFlatBlueActionBar(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V

    .line 119
    const v3, 0x7f0e00ad

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/AppSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 120
    .local v1, "lv":Landroid/widget/ListView;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v0, "appSettingsItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/AppSettingsItem;>;"
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$VolumePageTurnMode;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->supportsOpenGLES2()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 125
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_1
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->shouldShowNotesExport(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    .line 132
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_2
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsMR1OrLater()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 138
    iget-boolean v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getUseNetworkTts()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 139
    :cond_3
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$NetworkTts;

    invoke-direct {v3, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$NetworkTts;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_4
    :goto_0
    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 148
    new-instance v2, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;

    invoke-direct {v2, p0, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    .line 150
    .local v2, "offlineDictionarySettingsItem":Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    .end local v2    # "offlineDictionarySettingsItem":Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;
    :cond_5
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    const v4, 0x7f040027

    invoke-direct {v3, p0, p0, v4, v0}, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    .line 155
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 156
    new-instance v3, Lcom/google/android/apps/books/app/AppSettingsActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$1;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 163
    return-void

    .line 143
    :cond_6
    new-instance v3, Landroid/speech/tts/TextToSpeech;

    new-instance v4, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity$1;)V

    invoke-direct {v3, p0, v4}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 167
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onDestroy()V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 173
    :cond_0
    return-void
.end method

.method public onFinishNotesExportDialog(ZLjava/lang/String;)V
    .locals 1
    .param p1, "isEnabled"    # Z
    .param p2, "folderName"    # Ljava/lang/String;

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mSaveNotesToDrive:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->onFinished(ZLjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$900(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;ZLjava/lang/String;)V

    .line 667
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 184
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 189
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 186
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->finish()V

    .line 187
    const/4 v0, 0x1

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 179
    const-string v0, "mIsNetworkTtsSupported"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 180
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 675
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStart()V

    .line 676
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStart(Landroid/app/Activity;)V

    .line 677
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 681
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStop()V

    .line 682
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStop(Landroid/app/Activity;)V

    .line 683
    return-void
.end method
