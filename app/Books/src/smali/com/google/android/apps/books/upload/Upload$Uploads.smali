.class public Lcom/google/android/apps/books/upload/Upload$Uploads;
.super Ljava/util/HashMap;
.source "Upload.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/Upload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Uploads"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/apps/books/upload/Upload;",
        ">;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/android/apps/books/upload/Upload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 4
    .param p1, "otherUploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 157
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 158
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    new-instance v3, Lcom/google/android/apps/books/upload/Upload;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v3, v2}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/upload/Upload$Uploads;->put(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;

    goto :goto_0

    .line 160
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public copyAll(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 4
    .param p1, "otherUploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 171
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    new-instance v3, Lcom/google/android/apps/books/upload/Upload;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v3, v2}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/upload/Upload$Uploads;->put(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;

    goto :goto_0

    .line 173
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    :cond_0
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/upload/Upload;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-super {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public put(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;
    .locals 1
    .param p1, "value"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    return-object v0
.end method
