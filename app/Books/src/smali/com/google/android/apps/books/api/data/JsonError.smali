.class public Lcom/google/android/apps/books/api/data/JsonError;
.super Ljava/lang/Object;
.source "JsonError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/data/JsonError$ErrorItem;
    }
.end annotation


# instance fields
.field public code:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "code"
    .end annotation
.end field

.field public errors:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "errors"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/JsonError$ErrorItem;",
            ">;"
        }
    .end annotation
.end field

.field public message:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "message"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public hasCodeAndReason(ILjava/lang/String;)Z
    .locals 4
    .param p1, "errorCode"    # I
    .param p2, "reasonString"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 16
    iget v3, p0, Lcom/google/android/apps/books/api/data/JsonError;->code:I

    if-eq v3, p1, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v2

    .line 19
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/api/data/JsonError;->errors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/JsonError$ErrorItem;

    .line 20
    .local v0, "errorItem":Lcom/google/android/apps/books/api/data/JsonError$ErrorItem;
    iget-object v3, v0, Lcom/google/android/apps/books/api/data/JsonError$ErrorItem;->reason:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 21
    const/4 v2, 0x1

    goto :goto_0
.end method
