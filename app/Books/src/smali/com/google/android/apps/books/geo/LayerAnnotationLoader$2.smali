.class Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;
.super Ljava/lang/Object;
.source "LayerAnnotationLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/geo/LayerAnnotationLoader;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/geo/AnnotationSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

.field final synthetic val$annotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

.field final synthetic val$inLayer:Lcom/google/common/base/Predicate;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/apps/books/geo/AnnotationSet;Lcom/google/common/base/Predicate;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    iput-object p2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;->val$annotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

    iput-object p3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;->val$inLayer:Lcom/google/common/base/Predicate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;
    .locals 2
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;->val$annotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->getAnnotationsInRangeIterator(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;

    move-result-object v0

    .line 80
    .local v0, "all":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;->val$inLayer:Lcom/google/common/base/Predicate;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterators;->filter(Ljava/util/Iterator;Lcom/google/common/base/Predicate;)Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v1

    return-object v1
.end method
