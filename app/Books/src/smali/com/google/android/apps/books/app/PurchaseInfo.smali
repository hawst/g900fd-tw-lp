.class public Lcom/google/android/apps/books/app/PurchaseInfo;
.super Ljava/lang/Object;
.source "PurchaseInfo.java"


# instance fields
.field public final hasMultipleOffers:Z

.field public final isRentable:Z

.field public final lowestPriceString:Ljava/lang/String;

.field public final offerType:I


# direct methods
.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->offerType:I

    .line 75
    return-void

    :cond_0
    move v0, v2

    .line 72
    goto :goto_0

    :cond_1
    move v1, v2

    .line 73
    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/books/app/data/JsonSaleInfo;)V
    .locals 12
    .param p1, "saleInfo"    # Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iget-object v5, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->offers:Ljava/util/List;

    .line 40
    .local v5, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;>;"
    iget-object v7, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->saleability:Ljava/lang/String;

    .line 41
    .local v7, "saleability":Ljava/lang/String;
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 42
    .local v2, "lowestPrice":D
    const/4 v0, 0x0

    .line 43
    .local v0, "currencyCode":Ljava/lang/String;
    iget-object v8, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->offers:Ljava/util/List;

    if-nez v8, :cond_2

    .line 45
    const/4 v8, 0x1

    iput v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->offerType:I

    .line 46
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    .line 47
    iget-object v8, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;

    if-eqz v8, :cond_0

    .line 48
    iget-object v8, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;

    iget-wide v2, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;->amount:D

    .line 50
    iget-object v8, p1, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;

    iget-object v0, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;->currencyCode:Ljava/lang/String;

    .line 64
    :cond_0
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v8, v2, v8

    if-nez v8, :cond_6

    const-string v8, ""

    :goto_0
    iput-object v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    .line 66
    const-string v8, "FOR_RENTAL_ONLY"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "FOR_SALE_AND_RENTAL"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    :cond_1
    const/4 v8, 0x1

    :goto_1
    iput-boolean v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    .line 68
    return-void

    .line 53
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 54
    .local v6, "offersSize":I
    const/4 v8, 0x1

    if-le v6, v8, :cond_4

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    .line 55
    if-lez v6, :cond_5

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;

    iget v8, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;->finskyOfferType:I

    :goto_3
    iput v8, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->offerType:I

    .line 57
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;

    .line 58
    .local v4, "offer":Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;
    iget-object v8, v4, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;->retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;

    iget-wide v8, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;->amountInMicros:D

    const-wide v10, 0x412e848000000000L    # 1000000.0

    div-double/2addr v8, v10

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 59
    if-nez v0, :cond_3

    .line 60
    iget-object v8, v4, Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;->retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;

    iget-object v0, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;->currencyCode:Ljava/lang/String;

    goto :goto_4

    .line 54
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "offer":Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;
    :cond_4
    const/4 v8, 0x0

    goto :goto_2

    .line 55
    :cond_5
    const/4 v8, 0x1

    goto :goto_3

    .line 64
    .end local v6    # "offersSize":I
    :cond_6
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/app/PurchaseInfo;->priceString(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 66
    :cond_7
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static priceString(DLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "amount"    # D
    .param p2, "currencyCode"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 79
    .local v0, "format":Ljava/text/NumberFormat;
    invoke-static {p2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 80
    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 1
    .param p0, "in"    # Landroid/os/Parcel;

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/apps/books/app/PurchaseInfo;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/PurchaseInfo;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 90
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 91
    iget v0, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->offerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    return-void

    :cond_0
    move v0, v2

    .line 89
    goto :goto_0

    :cond_1
    move v1, v2

    .line 90
    goto :goto_1
.end method
