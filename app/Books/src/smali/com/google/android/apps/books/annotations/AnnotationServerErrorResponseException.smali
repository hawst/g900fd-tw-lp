.class public Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
.super Lcom/google/android/apps/books/annotations/AnnotationServerException;
.source "AnnotationServerErrorResponseException.java"


# instance fields
.field public final error:Lcom/google/android/apps/books/api/data/JsonError;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/api/data/JsonError;)V
    .locals 1
    .param p1, "error"    # Lcom/google/android/apps/books/api/data/JsonError;

    .prologue
    .line 16
    iget-object v0, p1, Lcom/google/android/apps/books/api/data/JsonError;->message:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerException;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;->error:Lcom/google/android/apps/books/api/data/JsonError;

    .line 18
    return-void
.end method
