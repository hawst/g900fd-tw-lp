.class public abstract Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
.super Ljava/lang/Object;
.source "BaseVolumeCoverSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;
    }
.end annotation


# instance fields
.field private final mCoverHeight:I

.field private final mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "coverHeight"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 23
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 29
    iput p1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mCoverHeight:I

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    .prologue
    .line 19
    iget v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mCoverHeight:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p2, "x2"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->saveTempBlob(Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/DataControllerBlob;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->handleServerResultOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverFailure(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverSuccess(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method private deliverFailure(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 127
    return-void
.end method

.method private deliverFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$2;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 122
    return-void
.end method

.method private deliverSuccess(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p3, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 141
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 144
    new-instance v1, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;

    invoke-direct {v1, p0, p2, v0}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/DataControllerBlob;Ljava/lang/String;)V

    invoke-interface {p3, v1}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 155
    return-void
.end method

.method private getFallbackCover(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 72
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v2

    .line 74
    .local v2, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/BooksDataStore;->openFallbackCoverInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 76
    .local v1, "fallbackCover":Ljava/io/InputStream;
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->saveTempBlob(Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v3

    invoke-direct {p0, p2, v3, p1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverSuccess(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    .end local v1    # "fallbackCover":Ljava/io/InputStream;
    .end local v2    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/io/IOException;
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverFailure(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private getImage(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 63
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getFallbackCover(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getImageOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0
.end method

.method private getImageOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 8
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 84
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "volumeId":Ljava/lang/String;
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 92
    .local v5, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    new-instance v0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 106
    .end local v5    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v7

    .line 89
    .local v7, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, v7}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverFailure(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private handleServerResultOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p3, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$3;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 137
    return-void
.end method

.method private saveTempBlob(Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;
    .locals 2
    .param p1, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p2, "content"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    new-instance v1, Lcom/google/android/apps/books/model/EncryptedContentImpl;

    invoke-direct {v1, p2}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Ljava/io/InputStream;)V

    invoke-interface {p1, v1}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v0

    .line 111
    .local v0, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v1, Lcom/google/android/apps/books/data/LargeBlobFromServer;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/data/LargeBlobFromServer;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V

    return-object v1
.end method


# virtual methods
.method protected abstract getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public getImage(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 35
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    .line 36
    .local v5, "volumeId":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v4

    .line 38
    .local v4, "store":Lcom/google/android/apps/books/model/BooksDataStore;
    :try_start_0
    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v1

    .line 39
    .local v1, "coverFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    if-nez p3, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/books/data/VolumeContentFile;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 40
    invoke-static {p4}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V

    .line 60
    .end local v1    # "coverFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    :cond_0
    :goto_0
    return-void

    .line 45
    .restart local v1    # "coverFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    :cond_1
    invoke-interface {v1}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 46
    .local v0, "content":Ljava/io/InputStream;
    new-instance v6, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;

    invoke-direct {v6, p0, v4, v5, v0}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;-><init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/io/InputStream;)V

    invoke-static {p3, v6}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverSuccess(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Object;)V

    .line 48
    invoke-static {p4}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 49
    .end local v0    # "content":Ljava/io/InputStream;
    .end local v1    # "coverFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    :catch_0
    move-exception v2

    .line 51
    .local v2, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    .line 52
    .local v3, "hadExistingConsumers":Z
    iget-object v6, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v6, v5, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v6

    if-nez v6, :cond_2

    move v6, v7

    :goto_1
    or-int/2addr v3, v6

    .line 53
    iget-object v6, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v6, v5, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v6

    if-nez v6, :cond_3

    :goto_2
    or-int/2addr v3, v7

    .line 54
    if-nez v3, :cond_0

    .line 58
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getImage(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0

    :cond_2
    move v6, v8

    .line 52
    goto :goto_1

    :cond_3
    move v7, v8

    .line 53
    goto :goto_2
.end method
