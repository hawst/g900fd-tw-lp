.class public Lcom/google/android/apps/books/widget/RemoteImageView;
.super Landroid/widget/FrameLayout;
.source "RemoteImageView.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/RemoteImageView$ExceptionConsumer;,
        Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;
    }
.end annotation


# instance fields
.field private mAttachedToWindow:Z

.field private final mContentObserver:Landroid/database/ContentObserver;

.field private mContentObserverIsRegistered:Z

.field private final mDimmedCoverId:I

.field private mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

.field private final mErrorId:I

.field private mExceptionConsumer:Lcom/google/android/apps/books/widget/RemoteImageView$ExceptionConsumer;

.field private mFuture:Lcom/google/android/apps/books/common/ImageFuture;

.field private mImageManager:Lcom/google/android/apps/books/common/ImageManager;

.field private final mImageViewContainerId:I

.field private final mImageViewId:I

.field private mListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

.field private mMode:I

.field private final mSpinnerId:I

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/RemoteImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "set"    # Landroid/util/AttributeSet;
    .param p3, "defaultStyle"    # I

    .prologue
    const/4 v5, 0x0

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    new-instance v2, Lcom/google/android/apps/books/widget/RemoteImageView$1;

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/RemoteImageView$1;-><init>(Lcom/google/android/apps/books/widget/RemoteImageView;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserver:Landroid/database/ContentObserver;

    .line 106
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->RemoteImageView:[I

    .line 107
    .local v1, "attrs":[I
    invoke-virtual {p1, p2, v1, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mSpinnerId:I

    .line 109
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewId:I

    .line 110
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewId:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewContainerId:I

    .line 112
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mDimmedCoverId:I

    .line 113
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mErrorId:I

    .line 114
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    .line 115
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/RemoteImageView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/RemoteImageView;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/RemoteImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/RemoteImageView;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->loadImage()V

    return-void
.end method

.method private cancelActiveRemoteImageRequest()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 199
    :cond_0
    return-void
.end method

.method private findDimmedCoverView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mDimmedCoverId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private findErrorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mErrorId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private findImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private findImageViewContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewContainerId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private findProgressView()Landroid/view/View;
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mSpinnerId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getImageManager()Lcom/google/android/apps/books/common/ImageManager;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 132
    .local v0, "result":Lcom/google/android/apps/books/common/ImageManager;
    if-nez v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/common/BooksContext;

    invoke-interface {v1}, Lcom/google/android/apps/books/common/BooksContext;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v0

    .line 135
    :cond_0
    return-object v0
.end method

.method private static isContentUri(Landroid/net/Uri;)Z
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 269
    if-eqz p0, :cond_0

    .line 270
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "scheme":Ljava/lang/String;
    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 275
    .end local v1    # "scheme":Ljava/lang/String;
    .local v0, "result":Z
    :goto_0
    return v0

    .line 273
    .end local v0    # "result":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "result":Z
    goto :goto_0
.end method

.method private loadImage()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    move-object v4, p0

    move-object v5, v2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    iput-object v2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    goto :goto_0
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    .line 237
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserverIsRegistered:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mAttachedToWindow:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 240
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 241
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserverIsRegistered:Z

    .line 243
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    return-void
.end method

.method private setMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 346
    iput p1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mMode:I

    .line 347
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->updateViews()V

    .line 348
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;->onLoaded()V

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .line 352
    :cond_0
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 246
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserverIsRegistered:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 248
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 249
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mContentObserverIsRegistered:Z

    .line 251
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    return-void
.end method

.method private updateViews()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 357
    iget v6, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mMode:I

    packed-switch v6, :pswitch_data_0

    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findErrorView()Landroid/view/View;

    move-result-object v4

    .line 368
    .local v4, "viewToShow":Landroid/view/View;
    new-array v5, v9, [Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findProgressView()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findImageViewContainer()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v8

    .line 370
    .local v5, "viewsToHide":[Landroid/view/View;
    :goto_0
    if-eqz v4, :cond_0

    .line 371
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 373
    :cond_0
    move-object v0, v5

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 374
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 375
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 373
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 359
    .end local v0    # "arr$":[Landroid/view/View;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "viewToShow":Landroid/view/View;
    .end local v5    # "viewsToHide":[Landroid/view/View;
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findProgressView()Landroid/view/View;

    move-result-object v4

    .line 360
    .restart local v4    # "viewToShow":Landroid/view/View;
    new-array v5, v9, [Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findErrorView()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findImageViewContainer()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v8

    .line 361
    .restart local v5    # "viewsToHide":[Landroid/view/View;
    goto :goto_0

    .line 363
    .end local v4    # "viewToShow":Landroid/view/View;
    .end local v5    # "viewsToHide":[Landroid/view/View;
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findImageViewContainer()Landroid/view/View;

    move-result-object v4

    .line 364
    .restart local v4    # "viewToShow":Landroid/view/View;
    new-array v5, v9, [Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findErrorView()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findProgressView()Landroid/view/View;

    move-result-object v6

    aput-object v6, v5, v8

    .line 365
    .restart local v5    # "viewsToHide":[Landroid/view/View;
    goto :goto_0

    .line 378
    .restart local v0    # "arr$":[Landroid/view/View;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    :cond_2
    return-void

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected getImageViewId()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageViewId:I

    return v0
.end method

.method protected handleImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 148
    .local v0, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 5

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 180
    .local v3, "parent":Landroid/view/ViewParent;
    instance-of v4, v3, Landroid/widget/AdapterView;

    if-eqz v4, :cond_0

    move-object v1, v3

    .line 181
    check-cast v1, Landroid/widget/AdapterView;

    .line 182
    .local v1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {v1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 183
    .local v0, "adapter":Landroid/widget/Adapter;
    instance-of v4, v0, Landroid/widget/BaseAdapter;

    if-eqz v4, :cond_0

    move-object v2, v0

    .line 184
    check-cast v2, Landroid/widget/BaseAdapter;

    .line 185
    .local v2, "baseAdapter":Landroid/widget/BaseAdapter;
    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 188
    .end local v0    # "adapter":Landroid/widget/Adapter;
    .end local v1    # "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    .end local v2    # "baseAdapter":Landroid/widget/BaseAdapter;
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mAttachedToWindow:Z

    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->registerContentObserver()V

    .line 258
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 262
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mAttachedToWindow:Z

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->unregisterContentObserver()V

    .line 265
    return-void
.end method

.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    const/4 v0, 0x1

    .line 153
    if-eqz p1, :cond_2

    .line 154
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/RemoteImageView;->handleImage(Landroid/graphics/Bitmap;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v1, :cond_1

    .line 161
    .local v0, "isAsynchronousCallback":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->notifyDataSetChanged()V

    .line 172
    .end local v0    # "isAsynchronousCallback":Z
    :cond_0
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 173
    return-void

    .line 160
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_2
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mExceptionConsumer:Lcom/google/android/apps/books/widget/RemoteImageView$ExceptionConsumer;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mExceptionConsumer:Lcom/google/android/apps/books/widget/RemoteImageView$ExceptionConsumer;

    invoke-interface {v1, p2}, Lcom/google/android/apps/books/widget/RemoteImageView$ExceptionConsumer;->handleException(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->cancelActiveRemoteImageRequest()V

    .line 224
    return-void
.end method

.method public setImageManager(Lcom/google/android/apps/books/common/ImageManager;)V
    .locals 0
    .param p1, "im"    # Lcom/google/android/apps/books/common/ImageManager;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 128
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/widget/RemoteImageView;->setImageURI(Landroid/net/Uri;Lcom/google/android/apps/books/common/ImageManager$Ensurer;)V

    .line 308
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;Lcom/google/android/apps/books/common/ImageManager$Ensurer;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "ensurer"    # Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    .prologue
    .line 321
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    .line 322
    .local v0, "uriChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 324
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/RemoteImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/apps/books/widget/RemoteImageView;->isContentUri(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->unregisterContentObserver()V

    .line 330
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mUri:Landroid/net/Uri;

    .line 331
    iput-object p2, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    .line 333
    if-nez p1, :cond_3

    .line 334
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/RemoteImageView;->setMode(I)V

    .line 343
    :cond_1
    :goto_1
    return-void

    .line 321
    .end local v0    # "uriChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 336
    .restart local v0    # "uriChanged":Z
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->loadImage()V

    .line 338
    invoke-static {p1}, Lcom/google/android/apps/books/widget/RemoteImageView;->isContentUri(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->registerContentObserver()V

    goto :goto_1
.end method

.method public setListener(Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .prologue
    .line 388
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RemoteImageView;->mListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .line 389
    return-void
.end method

.method public setShowDimmedCover(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteImageView;->findDimmedCoverView()Landroid/widget/TextView;

    move-result-object v0

    .line 382
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 383
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    :cond_0
    return-void

    .line 383
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
