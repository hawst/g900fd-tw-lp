.class Lcom/google/android/apps/books/view/pages/PageTurnJava$6;
.super Ljava/lang/Object;
.source "PageTurnJava.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field final synthetic val$pageRenderDetails:Lcom/google/android/apps/books/view/pages/PageRenderDetails;

.field final synthetic val$slot:I

.field final synthetic val$smallEnoughBitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    iput p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$slot:I

    iput-object p3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$smallEnoughBitmap:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$pageRenderDetails:Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    iput-object p5, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # getter for: Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;
    invoke-static {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$000(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnScene;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$slot:I

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$smallEnoughBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$pageRenderDetails:Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setPage(ILandroid/graphics/Bitmap;ZLcom/google/android/apps/books/view/pages/PageRenderDetails;Z)V

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$smallEnoughBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$bitmap:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;->val$smallEnoughBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 375
    :cond_0
    return-void
.end method
