.class Lcom/google/android/apps/books/widget/PagesViewController$5;
.super Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 1

    .prologue
    .line 2295
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$5;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    return-void
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "clipRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2299
    const/4 v0, 0x3

    .line 2300
    .local v0, "typeMask":I
    const/4 v1, 0x3

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->hasTouchablesOfType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2301
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$5;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2700(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/PageCanvasPainter;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    invoke-interface {v1, p1, v2}, Lcom/google/android/apps/books/render/PageCanvasPainter;->paintMediaViews(Landroid/graphics/Canvas;Ljava/util/List;)V

    .line 2305
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$5;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->paintHighlightRectsAndMarginNotes(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    invoke-static {v1, p2, p3, p1, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2800(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 2308
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$5;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->shouldPaintTtsHighlight(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    invoke-static {v1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2309
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$5;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->paintTtsHighlight(Landroid/graphics/Canvas;)V
    invoke-static {v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3000(Lcom/google/android/apps/books/widget/PagesViewController;Landroid/graphics/Canvas;)V

    .line 2311
    :cond_1
    return-void
.end method
