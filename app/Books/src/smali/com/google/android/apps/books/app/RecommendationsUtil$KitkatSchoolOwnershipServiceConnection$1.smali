.class Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;
.super Landroid/os/AsyncTask;
.source "RecommendationsUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

.field final synthetic val$ownedService:Lcom/google/android/nfcprovision/ISchoolOwnedService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;Lcom/google/android/nfcprovision/ISchoolOwnedService;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    iput-object p2, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->val$ownedService:Lcom/google/android/nfcprovision/ISchoolOwnedService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 43
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->val$ownedService:Lcom/google/android/nfcprovision/ISchoolOwnedService;

    invoke-interface {v1}, Lcom/google/android/nfcprovision/ISchoolOwnedService;->isSchoolOwned()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    # getter for: Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->access$000(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_0
    return-object v1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v1, "RecommendationsUtil"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const-string v1, "RecommendationsUtil"

    const-string v2, "Error calling school-ownership service"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    # getter for: Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->access$000(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    # getter for: Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->access$000(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->this$0:Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    # getter for: Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v0}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->access$100(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
