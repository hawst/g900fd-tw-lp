.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

.field final synthetic val$dismissedRecs:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->val$dismissedRecs:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 353
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$100(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->val$dismissedRecs:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 354
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # invokes: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->maybeExpireDismissedRecommendations()V
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$400(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)V

    .line 355
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # invokes: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$200(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 356
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$100(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onDismissedRecommendations(Ljava/util/Set;)V

    goto :goto_0

    .line 358
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method
