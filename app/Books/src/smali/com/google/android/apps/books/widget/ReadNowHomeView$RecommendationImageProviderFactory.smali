.class Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecommendationImageProviderFactory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;Lcom/google/android/apps/books/widget/ReadNowHomeView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/ReadNowHomeView;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/ReadNowHomeView$1;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    return-void
.end method


# virtual methods
.method public createImageProvider()Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/apps/books/util/BookCoverImageProvider;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/BookCoverImageProvider;-><init>(Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;)V

    return-object v0
.end method

.method public getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 141
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-object v1, v1, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v6

    .line 142
    .local v6, "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v6}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v0

    .line 144
    .local v0, "sharedImageManager":Lcom/google/android/apps/books/common/ImageManager;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getReadNowCoverStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/RemoteFileCache;

    move-result-object v5

    .line 147
    .local v5, "imageStore":Lcom/google/android/apps/books/model/RemoteFileCache;
    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v7

    .line 149
    .local v7, "future":Lcom/google/android/apps/books/common/ImageFuture;
    return-object v7
.end method
