.class Lcom/google/android/apps/books/widget/PagesViewController$1;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 782
    .local p1, "oldBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p2, "newBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v9, 0x0

    .line 787
    .local v9, "somePageBookmarkChanged":Z
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$200(Lcom/google/android/apps/books/widget/PagesViewController;)[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget v10, v0, v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 788
    .local v7, "offsetFromBasePage":Ljava/lang/Integer;
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v8

    .line 789
    .local v8, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v8, :cond_1

    .line 787
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 792
    :cond_1
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v10

    invoke-virtual {v8}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 794
    .local v1, "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    if-eqz v1, :cond_0

    .line 797
    invoke-virtual {v8, p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/SortedMap;)Ljava/util/Collection;

    move-result-object v6

    .line 798
    .local v6, "newViewables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v3, 0x1

    .line 800
    .local v3, "hasBookmark":Z
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getBookmarkWillBePresent()Z

    move-result v10

    xor-int/2addr v10, v3

    if-eqz v10, :cond_0

    .line 801
    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->startAnimatingBookmark(Z)V

    .line 802
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v10

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-interface {v10, v11, v3}, Lcom/google/android/apps/books/widget/PagesView;->onBookmarkChanged(IZ)V

    .line 803
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v10

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v10, v11, v3}, Lcom/google/android/apps/books/widget/BookView;->onBookmarkChanged(IZ)V

    .line 804
    const/4 v9, 0x1

    goto :goto_1

    .line 798
    .end local v3    # "hasBookmark":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 810
    .end local v1    # "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .end local v6    # "newViewables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v7    # "offsetFromBasePage":Ljava/lang/Integer;
    .end local v8    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 812
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/render/PositionPageIdentifier;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;>;"
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageToRendering:Ljava/util/Map;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$600(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 813
    .restart local v8    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v8, :cond_4

    .line 816
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 817
    .restart local v1    # "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    invoke-virtual {v8, p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/SortedMap;)Ljava/util/Collection;

    move-result-object v6

    .line 818
    .restart local v6    # "newViewables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    const/4 v3, 0x1

    .line 820
    .restart local v3    # "hasBookmark":Z
    :goto_4
    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getBookmarkWillBePresent()Z

    move-result v10

    xor-int/2addr v10, v3

    if-eqz v10, :cond_4

    .line 821
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v10, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->startAnimatingBookmark(Z)V

    .line 822
    const/4 v9, 0x1

    goto :goto_3

    .line 818
    .end local v3    # "hasBookmark":Z
    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    .line 826
    .end local v1    # "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/render/PositionPageIdentifier;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;>;"
    .end local v6    # "newViewables":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v8    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_6
    if-eqz v9, :cond_7

    .line 827
    iget-object v10, p0, Lcom/google/android/apps/books/widget/PagesViewController$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v10}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/books/widget/BookView;->onBookmarksChanged()V

    .line 829
    :cond_7
    return-void
.end method
