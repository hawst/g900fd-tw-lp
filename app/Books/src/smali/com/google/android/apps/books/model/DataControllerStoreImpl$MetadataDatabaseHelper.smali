.class Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DataControllerStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/DataControllerStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MetadataDatabaseHelper"
.end annotation


# instance fields
.field private final DICTIONARY_METADATA_TABLE_CREATE:Ljava/lang/String;

.field private final DICTIONARY_PREFERENCE_TABLE_CREATE:Ljava/lang/String;

.field private final USER_SESSION_KEY_TABLE_CREATE:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/books/model/DataControllerStoreImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/DataControllerStoreImpl;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->this$0:Lcom/google/android/apps/books/model/DataControllerStoreImpl;

    .line 365
    const-string v0, "books3.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 339
    const-string v0, "CREATE TABLE dictionary_metadata ( language_code TEXT NOT NULL, version INTEGER NOT NULL, encrypted_dict_key BLOB NOT NULL,url TEXT NOT NULL,size LONG NOT NULL,session_key_version TEXT NOT NULL,account_name TEXT NOT NULL,UNIQUE( language_code, version ));"

    iput-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->DICTIONARY_METADATA_TABLE_CREATE:Ljava/lang/String;

    .line 351
    const-string v0, "CREATE TABLE dictionary_preference ( language_code TEXT NOT NULL PRIMARY KEY, download_requested INTEGER DEFAULT 0); "

    iput-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->DICTIONARY_PREFERENCE_TABLE_CREATE:Ljava/lang/String;

    .line 356
    const-string v0, "CREATE TABLE user_session_key ( session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB );"

    iput-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->USER_SESSION_KEY_TABLE_CREATE:Ljava/lang/String;

    .line 366
    iput-object p2, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->mContext:Landroid/content/Context;

    .line 367
    return-void
.end method

.method private recreateAllTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 433
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS dictionary_metadata"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 434
    const-string v1, "DROP TABLE IF EXISTS CREATE TABLE dictionary_preference ( language_code TEXT NOT NULL PRIMARY KEY, download_requested INTEGER DEFAULT 0); "

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 435
    const-string v1, "DROP TABLE IF EXISTS CREATE TABLE user_session_key ( session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 436
    const-string v1, "CREATE TABLE dictionary_metadata ( language_code TEXT NOT NULL, version INTEGER NOT NULL, encrypted_dict_key BLOB NOT NULL,url TEXT NOT NULL,size LONG NOT NULL,session_key_version TEXT NOT NULL,account_name TEXT NOT NULL,UNIQUE( language_code, version ));"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 437
    const-string v1, "CREATE TABLE dictionary_preference ( language_code TEXT NOT NULL PRIMARY KEY, download_requested INTEGER DEFAULT 0); "

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 438
    const-string v1, "CREATE TABLE user_session_key ( session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 439
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "DataCtlDB"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441
    const-string v1, "DataCtlDB"

    const-string v2, "onUpgrade: SQLiteException, could not recreate all data controller tables"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private recreateDictionaryMetadataTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 420
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS dictionary_metadata"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 421
    const-string v1, "CREATE TABLE dictionary_metadata ( language_code TEXT NOT NULL, version INTEGER NOT NULL, encrypted_dict_key BLOB NOT NULL,url TEXT NOT NULL,size LONG NOT NULL,session_key_version TEXT NOT NULL,account_name TEXT NOT NULL,UNIQUE( language_code, version ));"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "DataCtlDB"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 424
    const-string v1, "DataCtlDB"

    const-string v2, "onUpgrade: SQLiteException, could not recreate dictionary metadata table"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 371
    const-string v0, "CREATE TABLE dictionary_metadata ( language_code TEXT NOT NULL, version INTEGER NOT NULL, encrypted_dict_key BLOB NOT NULL,url TEXT NOT NULL,size LONG NOT NULL,session_key_version TEXT NOT NULL,account_name TEXT NOT NULL,UNIQUE( language_code, version ));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 372
    const-string v0, "CREATE TABLE dictionary_preference ( language_code TEXT NOT NULL PRIMARY KEY, download_requested INTEGER DEFAULT 0); "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 373
    const-string v0, "CREATE TABLE user_session_key ( session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 374
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/4 v10, 0x3

    .line 378
    if-ne p2, p3, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 382
    .local v6, "startTime":J
    const-string v5, "DataCtlDB"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 383
    const-string v5, "DataCtlDB"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Upgrading data controller DB from version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_2
    move v0, p2

    .line 390
    .local v0, "currentVersion":I
    const/4 v5, 0x1

    if-ne v0, v5, :cond_3

    .line 393
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->this$0:Lcom/google/android/apps/books/model/DataControllerStoreImpl;

    # getter for: Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;
    invoke-static {v5}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->access$000(Lcom/google/android/apps/books/model/DataControllerStoreImpl;)Lcom/google/android/apps/books/provider/VolumeContentStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDeleteContents(Ljava/io/File;)Z

    .line 395
    new-instance v4, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v5, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 396
    .local v4, "localPreferences":Lcom/google/android/apps/books/preference/LocalPreferences;
    const-wide/16 v8, 0x0

    invoke-virtual {v4, v8, v9}, Lcom/google/android/apps/books/preference/LocalPreferences;->setLastDictionaryMetadataSync(J)V

    .line 397
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->recreateDictionaryMetadataTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    const/4 v0, 0x2

    .line 404
    .end local v4    # "localPreferences":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_3
    :goto_1
    if-eq v0, p3, :cond_5

    .line 405
    const-string v5, "DataCtlDB"

    const/4 v8, 0x6

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 406
    const-string v5, "DataCtlDB"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Data controller DB upgrade from version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ended at version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", dropping existing data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;->recreateAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 412
    :cond_5
    const-string v5, "DataCtlDB"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 413
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 414
    .local v2, "endTime":J
    const-string v5, "DataCtlDB"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Data controller DB upgrade took "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v10, v2, v6

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " millis"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 400
    .end local v2    # "endTime":J
    :catch_0
    move-exception v1

    .line 402
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const/4 v0, -0x1

    goto :goto_1
.end method
