.class abstract Lcom/google/android/apps/books/render/BasePageHandle;
.super Ljava/lang/Object;
.source "BasePageHandle.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PageHandle;


# instance fields
.field private mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field private mPosition:Lcom/google/android/apps/books/common/Position;

.field private mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

.field private mSpreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

.field protected final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 0
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 19
    return-void
.end method


# virtual methods
.method protected createPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 99
    .local v0, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_0

    .line 100
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 102
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .locals 3

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 76
    .local v0, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_0

    .line 77
    new-instance v1, Lcom/google/android/apps/books/render/PositionPageIdentifier;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/render/PositionPageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 79
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFirstBookPage()Lcom/google/android/apps/books/model/Page;
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->getFirstBookPageIndex()I

    move-result v0

    .line 33
    .local v0, "bookPageIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 34
    const/4 v1, 0x0

    .line 36
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    goto :goto_0
.end method

.method public getFirstChapter()Lcom/google/android/apps/books/model/Chapter;
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->getFirstChapterIndex()I

    move-result v0

    .line 24
    .local v0, "chapterIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 25
    const/4 v1, 0x0

    .line 27
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Chapter;

    goto :goto_0
.end method

.method public getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    if-nez v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->createPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    return-object v0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->createPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->createPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    return-object v0
.end method

.method public getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mSpreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasePageHandle;->createSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mSpreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasePageHandle;->mSpreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    return-object v0
.end method
