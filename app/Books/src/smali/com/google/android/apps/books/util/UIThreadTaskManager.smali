.class public Lcom/google/android/apps/books/util/UIThreadTaskManager;
.super Ljava/lang/Object;
.source "UIThreadTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;,
        Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;,
        Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;,
        Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;
    }
.end annotation


# static fields
.field private static final UI_THREAD_MAX_PAUSE_NS:J

.field private static final sManager:Lcom/google/android/apps/books/util/UIThreadTaskManager;


# instance fields
.field private final mAnimatingAnimators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private final mAnimatorStateBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;

.field private final mBusyProviders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mNextTime:J

.field private final mQueueTask:Ljava/lang/Runnable;

.field private mQueueTaskPending:Z

.field private mRequestSeq:I

.field private final mTasks:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->UI_THREAD_MAX_PAUSE_NS:J

    .line 73
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->sManager:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mRequestSeq:I

    .line 75
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mBusyProviders:Ljava/util/Set;

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mHandler:Landroid/os/Handler;

    .line 77
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$1;-><init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    .line 92
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mNextTime:J

    .line 117
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;-><init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTask:Ljava/lang/Runnable;

    .line 308
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatingAnimators:Ljava/util/Set;

    .line 335
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;-><init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatorStateBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/util/UIThreadTaskManager;Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->queueIfBusy(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatingAnimators:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$208(Lcom/google/android/apps/books/util/UIThreadTaskManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mRequestSeq:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mRequestSeq:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/util/UIThreadTaskManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTaskPending:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/util/UIThreadTaskManager;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .param p1, "x1"    # J

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/util/UIThreadTaskManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getWaitMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->pollQueue()Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks()V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/util/UIThreadTaskManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .param p1, "x1"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mNextTime:J

    return-wide p1
.end method

.method static synthetic access$900()J
    .locals 2

    .prologue
    .line 26
    sget-wide v0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->UI_THREAD_MAX_PAUSE_NS:J

    return-wide v0
.end method

.method private checkQueuedTasks()V
    .locals 2

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getWaitMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V

    .line 151
    return-void
.end method

.method private checkQueuedTasks(J)V
    .locals 3
    .param p1, "millis"    # J

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTaskPending:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isQueueEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTaskPending:Z

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTask:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 p1, 0x3e8

    .end local p1    # "millis":J
    :cond_0
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 142
    :cond_1
    return-void
.end method

.method public static getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->sManager:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    return-object v0
.end method

.method private getWaitMillis()J
    .locals 8

    .prologue
    .line 163
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mNextTime:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 165
    .local v0, "millis":J
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    return-wide v2
.end method

.method private isQueueEmpty()Z
    .locals 2

    .prologue
    .line 98
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .local v0, "task":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x0

    .line 104
    :goto_1
    return v1

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    goto :goto_0

    .line 104
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private pollQueue()Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    .locals 2

    .prologue
    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .local v0, "task":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    .end local v0    # "task":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    :goto_0
    return-object v0

    .restart local v0    # "task":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private queueIfBusy(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z
    .locals 6
    .param p1, "task"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .prologue
    const/4 v2, 0x1

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isQueueEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 177
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    invoke-virtual {v3, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 178
    const-wide/16 v4, 0x3e8

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V

    .line 187
    :goto_0
    return v2

    .line 181
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getWaitMillis()J

    move-result-wide v0

    .line 182
    .local v0, "waitMillis":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 183
    iget-object v3, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mTasks:Ljava/util/PriorityQueue;

    invoke-virtual {v3, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V

    goto :goto_0

    .line 187
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isBusy()Z
    .locals 3

    .prologue
    .line 271
    iget-object v2, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mBusyProviders:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 272
    .local v1, "provider":Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;
    invoke-interface {v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;->isBusy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    const/4 v2, 0x1

    .line 276
    .end local v1    # "provider":Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onBusyEnded()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks()V

    .line 156
    return-void
.end method

.method public post(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V
    .locals 3
    .param p1, "task"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;
    .param p2, "priority"    # I

    .prologue
    .line 289
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;-><init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V

    .line 290
    .local v0, "queuedTask":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->queueIfBusy(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 293
    :cond_1
    return-void
.end method

.method public postDelayed(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;JI)V
    .locals 2
    .param p1, "task"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;
    .param p2, "millis"    # J
    .param p4, "priority"    # I

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    invoke-direct {v0, p0, p1, p4}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;-><init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V

    .line 301
    .local v0, "queuedTask":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 302
    return-void
.end method

.method public removeBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mBusyProviders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks()V

    .line 264
    return-void
.end method

.method public startAnimator(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatorStateBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 343
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 344
    return-void
.end method

.method public weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager;->mBusyProviders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks()V

    .line 256
    return-void
.end method
