.class Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;
.super Ljava/lang/Object;
.source "SnapshottingPageView.java"

# interfaces
.implements Lcom/google/android/apps/books/navigation/PageViewContent$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/SnapshottingPageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCookie:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCookie()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->mCookie:Ljava/lang/Object;

    return-object v0
.end method

.method public onBookmarksChanged()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->onBookmarksChanged()V

    .line 104
    return-void
.end method

.method public pageUpdated()V
    .locals 3

    .prologue
    .line 82
    const-string v0, "NavPageView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "NavPageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pageUpdated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->invalidate()V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # invokes: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->maybeUpdateBookmark()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$000(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V

    .line 89
    return-void
.end method

.method public setCookie(Ljava/lang/Object;)V
    .locals 0
    .param p1, "cookie"    # Ljava/lang/Object;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;->mCookie:Ljava/lang/Object;

    .line 94
    return-void
.end method
