.class Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ReaderMenuImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecentSearchesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 557
    .local p3, "objects":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .line 558
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 559
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 562
    move-object v4, p2

    .line 563
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_0

    .line 564
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 565
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f04005b

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 568
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 569
    .local v2, "query":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 571
    const v5, 0x1020014

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 572
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v3, :cond_1

    .line 573
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 574
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0f01ed

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 578
    :cond_1
    new-instance v5, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter$1;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586
    const v5, 0x7f0e0133

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 587
    .local v1, "putTextArrow":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0f01ee

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 589
    new-instance v5, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter$2;

    invoke-direct {v5, p0, v2}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter$2;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599
    .end local v1    # "putTextArrow":Landroid/widget/ImageView;
    .end local v3    # "textView":Landroid/widget/TextView;
    :cond_2
    return-object v4
.end method
