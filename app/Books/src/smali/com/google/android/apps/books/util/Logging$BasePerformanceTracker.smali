.class Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;
.super Ljava/lang/Object;
.source "Logging.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Logging$PerformanceTracker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/Logging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BasePerformanceTracker"
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/apps/books/util/Logging$CompletionCallback;

.field private final mStartTime:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/Logging$CompletionCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mStartTime:J

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mCallback:Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    .line 58
    return-void
.end method


# virtual methods
.method public checkpoint(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 60
    return-void
.end method

.method public done()J
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->getElapsedTime()J

    move-result-wide v0

    .line 70
    .local v0, "result":J
    iget-object v2, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mCallback:Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mCallback:Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/books/util/Logging$CompletionCallback;->completedWithElapsedTime(J)V

    .line 73
    :cond_0
    return-wide v0
.end method

.method protected getElapsedTime()J
    .locals 4

    .prologue
    .line 65
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mStartTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method protected getStartTime()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->mStartTime:J

    return-wide v0
.end method
