.class public Lcom/google/android/apps/books/data/PageStructureSubcontroller;
.super Ljava/lang/Object;
.source "PageStructureSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    }
.end annotation


# instance fields
.field private final mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentIdToResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V
    .locals 1
    .param p1, "encryptionScheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 40
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 52
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;

    .line 55
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p6, "x6"    # Lcom/google/android/apps/books/model/LocalSessionKey;

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p4, "x4"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "x5"    # Ljava/lang/Runnable;

    .prologue
    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .param p4, "x4"    # Z

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;

    return-object v0
.end method

.method private getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 8
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p5, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p6, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Runnable;",
            "Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 191
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;

    iget-object v3, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p7

    move-object v4, p2

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 221
    return-void
.end method

.method private getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V

    .line 161
    return-void
.end method

.method private getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p4, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 252
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;)V

    invoke-interface {p1, p3, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 270
    return-void
.end method

.method private getSpecificKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p6, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-interface {p1, p6, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 138
    return-void
.end method

.method private handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "e"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "continuation"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 227
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 238
    return-void
.end method

.method private onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Page;",
            "Ljava/lang/Runnable;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p6, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-interface {v0, v1, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->getPageStructureFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v6

    .local v6, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p4

    move-object v7, p5

    .line 177
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v6    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v8

    .line 180
    .local v8, "e":Ljava/io/IOException;
    invoke-direct {p0, p2, v8}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 349
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 338
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$9;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$9;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 344
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 301
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;>;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v6, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz p4, :cond_3

    .line 302
    :cond_0
    const/4 v2, 0x0

    .line 303
    .local v2, "encrypted":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 304
    .local v0, "cleartext":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 306
    .local v3, "inflated":Ljava/io/InputStream;
    :try_start_0
    iget-object v6, p3, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v6}, Lcom/google/android/apps/books/data/DataControllerBlob;->openInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 307
    iget-object v6, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iget-object v7, p3, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v7

    invoke-interface {v6, v2, v7}, Lcom/google/android/apps/books/data/EncryptionScheme;->decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;

    move-result-object v0

    .line 308
    new-instance v4, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v4, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    .end local v3    # "inflated":Ljava/io/InputStream;
    .local v4, "inflated":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v4}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v5

    .line 310
    .local v5, "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    invoke-virtual {v5}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPageCount()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_4

    .line 311
    new-instance v6, Ljava/lang/Exception;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected number of pages: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPageCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 321
    :goto_0
    if-eqz v2, :cond_1

    .line 322
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 324
    :cond_1
    if-eqz v0, :cond_2

    .line 325
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 327
    :cond_2
    if-eqz v4, :cond_3

    .line 328
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 334
    .end local v0    # "cleartext":Ljava/io/InputStream;
    .end local v2    # "encrypted":Ljava/io/InputStream;
    .end local v4    # "inflated":Ljava/io/InputStream;
    .end local v5    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    :cond_3
    :goto_1
    return-void

    .line 314
    .restart local v0    # "cleartext":Ljava/io/InputStream;
    .restart local v2    # "encrypted":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    .restart local v5    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    :cond_4
    :try_start_3
    iget-object v6, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPage(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v7

    invoke-virtual {v6, p2, v7, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 316
    .end local v5    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 317
    .end local v4    # "inflated":Ljava/io/InputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    :goto_2
    :try_start_4
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 321
    if-eqz v2, :cond_5

    .line 322
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 324
    :cond_5
    if-eqz v0, :cond_6

    .line 325
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 327
    :cond_6
    if-eqz v3, :cond_3

    .line 328
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 330
    :catch_1
    move-exception v6

    goto :goto_1

    .line 320
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 321
    :goto_3
    if-eqz v2, :cond_7

    .line 322
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 324
    :cond_7
    if-eqz v0, :cond_8

    .line 325
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 327
    :cond_8
    if-eqz v3, :cond_9

    .line 328
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 331
    :cond_9
    :goto_4
    throw v6

    .line 330
    :catch_2
    move-exception v7

    goto :goto_4

    .line 320
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "inflated":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    goto :goto_3

    .line 316
    :catch_3
    move-exception v1

    goto :goto_2

    .line 330
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    .restart local v5    # "pages":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    :catch_4
    move-exception v6

    goto :goto_1
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .param p4, "save"    # Z

    .prologue
    const/4 v2, 0x0

    .line 277
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;)V

    .line 279
    if-eqz p4, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;

    invoke-direct {v0, p0, p3, p2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$6;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 248
    return-void
.end method


# virtual methods
.method public getPageStructure(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 13
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    new-instance v9, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;

    const-string v2, "Page is forbidden"

    invoke-direct {v9, v2}, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;-><init>(Ljava/lang/String;)V

    .line 63
    .local v9, "e":Ljava/lang/Exception;
    move-object/from16 v0, p4

    invoke-static {v0, v9}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 64
    move-object/from16 v0, p5

    invoke-static {v0, v9}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 115
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    new-instance v4, Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p2, v2}, Lcom/google/android/apps/books/sync/VolumeContentId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .local v4, "contentId":Lcom/google/android/apps/books/sync/VolumeContentId;
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    .line 71
    .local v11, "pendingSaveResult":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    if-eqz v11, :cond_2

    .line 72
    move-object/from16 v0, p4

    invoke-direct {p0, p1, v4, v11, v0}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;)V

    .line 73
    const/16 p4, 0x0

    .line 74
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p5

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    goto :goto_0

    .line 78
    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalPageState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;

    move-result-object v12

    .line 80
    .local v12, "state":Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
    if-nez p4, :cond_3

    invoke-interface {v12}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->structureIsDownloaded()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 82
    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 86
    :cond_3
    const/4 v10, 0x0

    .line 87
    .local v10, "hadExistingConsumers":Z
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p4

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    or-int/2addr v10, v2

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p5

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v10, v2

    .line 89
    if-nez v10, :cond_0

    .line 93
    invoke-interface {v12}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->structureIsDownloaded()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 94
    invoke-interface {v12}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-direct {p0, p1, v4, v2, v0}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V

    goto :goto_0

    .line 87
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 88
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 98
    :cond_6
    new-instance v1, Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 107
    .local v1, "expiredKeyContinuation":Ljava/lang/Runnable;
    invoke-interface {v12}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v8

    .line 108
    .local v8, "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    if-eqz v8, :cond_7

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v5, p3

    move-object v6, v1

    move-object/from16 v7, p6

    .line 109
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getSpecificKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/SessionKeyId;)V

    goto/16 :goto_0

    :cond_7
    move-object v2, p0

    move-object v3, p1

    move-object/from16 v5, p3

    move-object v6, v1

    move-object/from16 v7, p6

    .line 114
    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto/16 :goto_0
.end method
