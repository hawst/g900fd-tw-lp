.class Lcom/google/android/apps/books/app/BookmarkController$1;
.super Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;
.source "BookmarkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BookmarkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BookmarkController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BookmarkController;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public layerAnnotationsLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 62
    iget-object v4, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    # getter for: Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BookmarkController;->access$000(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/SortedMap;

    move-result-object v3

    .line 63
    .local v3, "oldBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    new-instance v5, Ljava/util/TreeMap;

    iget-object v6, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    # getter for: Lcom/google/android/apps/books/app/BookmarkController;->mComparator:Ljava/util/Comparator;
    invoke-static {v6}, Lcom/google/android/apps/books/app/BookmarkController;->access$100(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/Comparator;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    # setter for: Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;
    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/BookmarkController;->access$002(Lcom/google/android/apps/books/app/BookmarkController;Ljava/util/SortedMap;)Ljava/util/SortedMap;

    .line 64
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 65
    .local v0, "bookmark":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v4, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    # getter for: Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BookmarkController;->access$000(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/SortedMap;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 67
    .end local v0    # "bookmark":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    # getter for: Lcom/google/android/apps/books/app/BookmarkController;->mBookmarkListeners:Ljava/util/Set;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BookmarkController;->access$200(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    .line 68
    .local v2, "listener":Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    iget-object v4, p0, Lcom/google/android/apps/books/app/BookmarkController$1;->this$0:Lcom/google/android/apps/books/app/BookmarkController;

    # getter for: Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BookmarkController;->access$000(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/SortedMap;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;->onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V

    goto :goto_1

    .line 71
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    .end local v3    # "oldBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_1
    return-void
.end method

.method public layerQuotaLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "quota":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    return-void
.end method
