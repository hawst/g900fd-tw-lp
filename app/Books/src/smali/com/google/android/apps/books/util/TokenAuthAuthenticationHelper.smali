.class public Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
.super Lcom/google/android/apps/books/util/BrowserAuthenticationHelper;
.source "TokenAuthAuthenticationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;,
        Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;,
        Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/BrowserAuthenticationHelper",
        "<",
        "Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;",
        ">;"
    }
.end annotation


# static fields
.field private static final ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

.field private static final TOKEN_AUTH_URL:Landroid/net/Uri;


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mContinuationUrl:Ljava/lang/String;

.field private mHasRetried:Z

.field private mInvalidatedTokens:Z

.field private final mServiceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "https://www.google.com/accounts/IssueAuthToken?service=gaia&Session=false"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

    .line 63
    const-string v0, "https://www.google.com/accounts/TokenAuth?"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->TOKEN_AUTH_URL:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "continuationUrl"    # Ljava/lang/String;
    .param p4, "serviceName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/BrowserAuthenticationHelper;-><init>(Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;)V

    .line 79
    iput-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z

    .line 81
    iput-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mHasRetried:Z

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mAccountName:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mContinuationUrl:Ljava/lang/String;

    .line 94
    iput-object p4, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mServiceName:Ljava/lang/String;

    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->getSidToken()V

    .line 96
    return-void
.end method

.method private abort()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 239
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->deliverResult(Landroid/net/Uri;Ljava/lang/Exception;)V

    .line 240
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->getUberToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->uriForToken(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->deliverResult(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method private deliverResult(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    if-eqz v0, :cond_0

    .line 229
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mHasRetried:Z

    if-nez v0, :cond_1

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mHasRetried:Z

    .line 231
    invoke-direct {p0}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->getSidToken()V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->finished(Landroid/net/Uri;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private getSidToken()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 99
    new-instance v1, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mAccountName:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v1, v2, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .local v1, "account":Landroid/accounts/Account;
    new-instance v5, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;-><init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Landroid/accounts/Account;)V

    .line 101
    .local v5, "callback":Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v2, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 102
    .local v0, "am":Landroid/accounts/AccountManager;
    const-string v2, "SID"

    iget-object v4, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v4, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v4}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 103
    return-void
.end method

.method private getUberToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "sid"    # Ljava/lang/String;
    .param p2, "lsid"    # Ljava/lang/String;

    .prologue
    .line 164
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    if-nez v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v2, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v0

    .line 169
    .local v0, "httpClient":Lcom/google/android/apps/books/net/ResponseGetter;
    if-eqz v0, :cond_0

    .line 170
    new-instance v1, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;-><init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/net/ResponseGetter;)V

    .line 213
    .local v1, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/String;>;"
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private uriForToken(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 218
    sget-object v1, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->TOKEN_AUTH_URL:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "auth"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "continue"

    iget-object v3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mContinuationUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "service"

    iget-object v3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mServiceName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source"

    iget-object v3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mServiceName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 223
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method
