.class public Lcom/google/android/apps/books/app/ContentsView$Arguments;
.super Ljava/lang/Object;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ContentsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Arguments"
.end annotation


# instance fields
.field public final annotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

.field public final metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field public final position:Lcom/google/android/apps/books/common/Position;

.field public final readerSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field public final readingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "annotations"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p4, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p5, "readerSettings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeMetadata;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 67
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->annotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->position:Lcom/google/android/apps/books/common/Position;

    .line 69
    iput-object p4, p0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->readingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 70
    iput-object p5, p0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->readerSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 71
    return-void
.end method
