.class public Lcom/google/android/apps/books/util/NativeHeapBitmapCache;
.super Ljava/lang/Object;
.source "NativeHeapBitmapCache.java"

# interfaces
.implements Lcom/google/android/apps/books/util/BitmapCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;,
        Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;,
        Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/BitmapCache",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static sMapFile:Ljava/io/RandomAccessFile;

.field private static sMapFileInitDone:Z

.field private static sTempFile:Ljava/io/File;


# instance fields
.field private final mBitmaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
            "<TT;>.BitmapSource;>;"
        }
    .end annotation
.end field

.field private final mBufferAllocSeq:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final mFallback:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private final mHeapAllocFraction:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRetainedKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;


# direct methods
.method public constructor <init>(ZILcom/google/android/apps/books/util/BitmapReusePool;Lcom/google/android/apps/books/util/BitmapCache;I)V
    .locals 6
    .param p1, "weakKeys"    # Z
    .param p2, "retainCount"    # I
    .param p3, "reusePool"    # Lcom/google/android/apps/books/util/BitmapReusePool;
    .param p5, "heapAllocFraction"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Lcom/google/android/apps/books/util/BitmapReusePool;",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<-TT;>;I)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p4, "fallback":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<-TT;>;"
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mPaint:Landroid/graphics/Paint;

    .line 76
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBufferAllocSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 279
    invoke-static {p5, v4}, Ljava/lang/Math;->max(II)I

    move-result p5

    .line 281
    if-eq p5, v4, :cond_0

    .line 282
    invoke-static {}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->initMapFile()V

    .line 285
    :cond_0
    if-eqz p1, :cond_2

    .line 286
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    .line 287
    if-nez p2, :cond_1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    .line 299
    :goto_1
    const-string v0, "NativeHeapBitmapCache"

    const/16 v1, 0x3c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v3, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v5, 0x3

    invoke-direct {v3, v5}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    invoke-static {v0, v4, v1, v2, v3}, Lcom/google/android/apps/books/util/ExecutorUtils;->createThreadPoolExecutor(Ljava/lang/String;IILjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor$CallerRunsPolicy;

    invoke-direct {v1}, Ljava/util/concurrent/ThreadPoolExecutor$CallerRunsPolicy;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 304
    iput-object p3, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;

    .line 305
    iput-object p4, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mFallback:Lcom/google/android/apps/books/util/BitmapCache;

    .line 306
    iput p5, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mHeapAllocFraction:I

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 310
    return-void

    .line 287
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;

    const/high16 v3, 0x3f400000    # 0.75f

    move-object v1, p0

    move v2, p2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;IFZI)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 296
    :cond_2
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    .line 297
    iput-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    goto :goto_1
.end method

.method public constructor <init>(ZLcom/google/android/apps/books/util/BitmapCache;)V
    .locals 6
    .param p1, "weakKeys"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 313
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p2, "fallback":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<-TT;>;"
    const/16 v2, 0x20

    if-nez p2, :cond_0

    new-instance v3, Lcom/google/android/apps/books/util/BitmapReusePool;

    invoke-direct {v3}, Lcom/google/android/apps/books/util/BitmapReusePool;-><init>()V

    :goto_0
    const/16 v5, 0xa

    move-object v0, p0

    move v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZILcom/google/android/apps/books/util/BitmapReusePool;Lcom/google/android/apps/books/util/BitmapCache;I)V

    .line 316
    return-void

    .line 313
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/books/util/BitmapCache;->getReusePool()Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v3

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mHeapAllocFraction:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBufferAllocSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$200()Ljava/io/RandomAccessFile;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFile:Ljava/io/RandomAccessFile;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Lcom/google/android/apps/books/util/BitmapReusePool;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->serializeBitmap(Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;)V

    return-void
.end method

.method private static declared-synchronized initMapFile()V
    .locals 2

    .prologue
    .line 123
    const-class v1, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFileInitDone:Z

    if-nez v0, :cond_0

    .line 124
    invoke-static {}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->openMapFile()Ljava/io/RandomAccessFile;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFile:Ljava/io/RandomAccessFile;

    .line 125
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFileInitDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit v1

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    const-class v1, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, ".NativeHeapBitmapCache"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sTempFile:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit v1

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static openMapFile()Ljava/io/RandomAccessFile;
    .locals 8

    .prologue
    .line 85
    :try_start_0
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v0, "/dev/zero"

    const-string v1, "rw"

    invoke-direct {v6, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 89
    .local v6, "devZero":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->PRIVATE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 114
    .end local v6    # "devZero":Ljava/io/RandomAccessFile;
    .local v7, "e":Ljava/io/IOException;
    :goto_0
    return-object v6

    .line 91
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v6    # "devZero":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v7

    .line 92
    .restart local v7    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 104
    const-string v0, "NativeHeapBitmapCache"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "NativeHeapBitmapCache"

    const-string v1, "Working around /dev/zero bug by mapping from a temp file"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    new-instance v6, Ljava/io/RandomAccessFile;

    .end local v6    # "devZero":Ljava/io/RandomAccessFile;
    sget-object v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sTempFile:Ljava/io/File;

    const-string v1, "rw"

    invoke-direct {v6, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 110
    :catch_1
    move-exception v7

    .line 111
    const-string v0, "NativeHeapBitmapCache"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    const-string v0, "NativeHeapBitmapCache"

    const-string v1, "Unable to open map file"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 114
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private replaceBitmap(Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
            "<TT;>.BitmapSource;",
            "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
            "<TT;>.BitmapSource;)Z"
        }
    .end annotation

    .prologue
    .line 328
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    .local p2, "oldVal":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    .local p3, "newVal":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    instance-of v1, v1, Ljava/util/concurrent/ConcurrentMap;

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    check-cast v1, Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, p2, p3}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 340
    :goto_0
    return v1

    .line 331
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    monitor-enter v2

    .line 332
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 333
    .local v0, "val":Ljava/lang/Object;
    if-ne v0, p2, :cond_1

    .line 336
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    const/4 v1, 0x1

    monitor-exit v2

    goto :goto_0

    .line 341
    .end local v0    # "val":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 340
    .restart local v0    # "val":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private serializeBitmap(Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
            "<TT;>.PendingBitmap;)V"
        }
    .end annotation

    .prologue
    .line 359
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    .local p2, "pending":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p2, :cond_0

    .line 360
    invoke-virtual {p2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->getScaledBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 361
    .local v0, "scaledBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Landroid/graphics/Bitmap;)V

    .line 362
    .local v1, "ser":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->replaceBitmap(Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;)Z

    .line 363
    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 365
    .end local v0    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v1    # "ser":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 436
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/BitmapReusePool;->clear()V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 441
    :cond_0
    return-void
.end method

.method public containsBitmap(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x1

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 417
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->referenceKey(Ljava/lang/Object;)V

    .line 420
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mFallback:Lcom/google/android/apps/books/util/BitmapCache;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mFallback:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/util/BitmapCache;->containsBitmap(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 404
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;

    .local v1, "src":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    if-eqz v1, :cond_1

    .line 405
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 406
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 407
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->referenceKey(Ljava/lang/Object;)V

    .line 411
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mFallback:Lcom/google/android/apps/books/util/BitmapCache;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    move-object v0, v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mFallback:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/util/BitmapCache;->getBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_1
.end method

.method public getBitmapSize(Ljava/lang/Object;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p2, "size"    # Landroid/graphics/Point;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/graphics/Point;",
            ")",
            "Landroid/graphics/Point;"
        }
    .end annotation

    .prologue
    .line 425
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;

    .line 426
    .local v0, "src":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v1

    goto :goto_0
.end method

.method public getReusePool()Lcom/google/android/apps/books/util/BitmapReusePool;
    .locals 1

    .prologue
    .line 320
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;

    return-object v0
.end method

.method public putBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)V
    .locals 6
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "cacheWidth"    # I
    .param p4, "cacheHeight"    # I
    .param p5, "cacheConfig"    # Landroid/graphics/Bitmap$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/graphics/Bitmap;",
            "II",
            "Landroid/graphics/Bitmap$Config;",
            ")V"
        }
    .end annotation

    .prologue
    .line 370
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)V

    .line 372
    .local v0, "pending":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v2, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$2;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Ljava/lang/Object;Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 379
    return-void
.end method

.method public referenceKey(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 396
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    :cond_0
    return-void
.end method

.method public removeBitmap(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 388
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBitmaps:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mRetainedKeys:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    :cond_0
    return-void
.end method
