.class public Lcom/google/android/apps/books/render/RendererUtils;
.super Ljava/lang/Object;
.source "RendererUtils.java"


# static fields
.field private static final sSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/render/RendererUtils;->sSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    return-void
.end method

.method public static normalizeSpreadIdentifier(Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 2
    .param p0, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 14
    iget v1, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v1, :cond_1

    .line 23
    .end local p1    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_0
    :goto_0
    return-object p1

    .line 17
    .restart local p1    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/RendererUtils;->sSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {p0, p1, v1}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 18
    sget-object v1, Lcom/google/android/apps/books/render/RendererUtils;->sSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/SpreadItems;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v0

    .line 20
    .local v0, "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v0, :cond_0

    .line 21
    iget-object p1, v0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    goto :goto_0
.end method
