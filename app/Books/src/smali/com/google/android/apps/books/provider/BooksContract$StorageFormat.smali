.class public final enum Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
.super Ljava/lang/Enum;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StorageFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

.field public static final enum ACCOUNT_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

.field public static final enum UNENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

.field public static final enum VOLUME_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;


# instance fields
.field private final mDatabaseValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 941
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    const-string v1, "UNENCRYPTED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->UNENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    .line 942
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    const-string v1, "VOLUME_KEY_ENCRYPTED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->VOLUME_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    .line 943
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    const-string v1, "ACCOUNT_KEY_ENCRYPTED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->ACCOUNT_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    .line 940
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->UNENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->VOLUME_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->ACCOUNT_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "databaseValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 948
    iput p3, p0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->mDatabaseValue:I

    .line 949
    return-void
.end method

.method public static fromDatabaseValues(Ljava/lang/Integer;Ljava/lang/Long;)Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .locals 6
    .param p0, "storageFormatDbValue"    # Ljava/lang/Integer;
    .param p1, "sessionKeyRowId"    # Ljava/lang/Long;

    .prologue
    .line 962
    if-eqz p0, :cond_1

    .line 963
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 964
    .local v3, "intValue":I
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->values()[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v0, v2

    .line 965
    .local v1, "format":Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    invoke-virtual {v1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->getDatabaseValue()I

    move-result v5

    if-ne v5, v3, :cond_0

    .line 973
    .end local v0    # "arr$":[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .end local v1    # "format":Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .end local v2    # "i$":I
    .end local v3    # "intValue":I
    .end local v4    # "len$":I
    :goto_1
    return-object v1

    .line 964
    .restart local v0    # "arr$":[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .restart local v1    # "format":Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .restart local v2    # "i$":I
    .restart local v3    # "intValue":I
    .restart local v4    # "len$":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 969
    .end local v0    # "arr$":[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .end local v1    # "format":Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .end local v2    # "i$":I
    .end local v3    # "intValue":I
    .end local v4    # "len$":I
    :cond_1
    if-eqz p1, :cond_2

    .line 971
    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->VOLUME_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    goto :goto_1

    .line 973
    :cond_2
    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->UNENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 940
    const-class v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .locals 1

    .prologue
    .line 940
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    return-object v0
.end method


# virtual methods
.method public getDatabaseValue()I
    .locals 1

    .prologue
    .line 952
    iget v0, p0, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->mDatabaseValue:I

    return v0
.end method
