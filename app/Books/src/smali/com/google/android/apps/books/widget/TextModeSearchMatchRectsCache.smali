.class public Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;
.super Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
.source "TextModeSearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;,
        Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
        "<",
        "Lcom/google/android/apps/books/widget/PassageSearchMatchData;",
        ">;"
    }
.end annotation


# static fields
.field private static final mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;


# instance fields
.field private final mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

.field private final mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389
    new-instance v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    invoke-direct {v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;ZZLcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;)V
    .locals 6
    .param p1, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p2, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .param p4, "displayTwoPages"    # Z
    .param p5, "isRightToLeft"    # Z
    .param p6, "renderCallbacks"    # Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    .prologue
    .line 55
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move v4, p5

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ZZLcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    .line 116
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mDisplayTwoPages:Z

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 56
    iput-object p6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    .line 57
    return-void
.end method

.method private getMatchIndexForPage(Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z
    .locals 5
    .param p1, "pageIndices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p2, "positionWithinSearchResults"    # Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    .prologue
    .line 468
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    iget v4, p1, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 470
    .local v2, "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v2, :cond_1

    .line 471
    iget-object v1, v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    .line 473
    .local v1, "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    iget v3, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/TreeMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 476
    .local v0, "closestPreviousMatch":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    .line 477
    iget v3, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 480
    iget-boolean v3, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    if-eqz v3, :cond_0

    .line 483
    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;

    iget v3, v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->min:I

    iput v3, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    .line 490
    :goto_0
    const/4 v3, 0x1

    .line 493
    .end local v0    # "closestPreviousMatch":Ljava/lang/Integer;
    .end local v1    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    :goto_1
    return v3

    .line 487
    .restart local v0    # "closestPreviousMatch":Ljava/lang/Integer;
    .restart local v1    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;

    iget v3, v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->max:I

    iput v3, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    goto :goto_0

    .line 493
    .end local v0    # "closestPreviousMatch":Ljava/lang/Integer;
    .end local v1    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private moveToIndices(Lcom/google/android/apps/books/util/PassagePages;II)Z
    .locals 7
    .param p1, "pages"    # Lcom/google/android/apps/books/util/PassagePages;
    .param p2, "passageIndex"    # I
    .param p3, "pageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 320
    invoke-virtual {p1, p3}, Lcom/google/android/apps/books/util/PassagePages;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v1

    .line 321
    .local v1, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v3

    .line 324
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v0

    .line 325
    .local v0, "handle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v0}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v2

    .line 326
    .local v2, "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v2, :cond_0

    .line 329
    iput-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    .line 330
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v4, v2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v3, v4, v5, v6}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 331
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected finishProcessRequestResult()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onCacheUpdated()V

    .line 337
    return-void
.end method

.method protected getHighlightRectsWalker(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/widget/Walker;
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 380
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 382
    .local v0, "searchRects":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->rects:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 383
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->getWalker()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v1

    .line 385
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMatchIndexForSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z
    .locals 8
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "positionWithinSearchResults"    # Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 395
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v4, p1, v7}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 396
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/TextPageHandle;

    invoke-interface {v4}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v3

    .line 400
    .local v3, "startIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-nez v3, :cond_0

    move v4, v5

    .line 455
    :goto_0
    return v4

    .line 405
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v4, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 408
    .local v2, "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-nez v2, :cond_1

    move v4, v5

    .line 409
    goto :goto_0

    .line 415
    :cond_1
    invoke-direct {p0, v3, p2}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getMatchIndexForPage(Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z

    move-result v1

    .local v1, "matchBeforeOrAtFirstPage":Z
    if-eqz v1, :cond_2

    .line 417
    iget-boolean v4, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    if-eqz v4, :cond_2

    move v4, v6

    .line 418
    goto :goto_0

    .line 422
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 423
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->getLast()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/TextPageHandle;

    invoke-interface {v4}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 424
    .local v0, "endIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v0, :cond_3

    .line 427
    sget-object v4, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getMatchIndexForPage(Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 428
    sget-object v4, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    iget-boolean v4, v4, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    if-eqz v4, :cond_3

    .line 429
    sget-object v4, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    iget-boolean v4, v4, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    iput-boolean v4, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 431
    sget-object v4, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPositionWithinSearchResults:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    iget v4, v4, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    iput v4, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    move v4, v6

    .line 433
    goto :goto_0

    .line 443
    .end local v0    # "endIndices":Lcom/google/android/apps/books/render/PageIndices;
    :cond_3
    if-eqz v1, :cond_4

    move v4, v6

    .line 444
    goto :goto_0

    .line 450
    :cond_4
    iput-boolean v5, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 451
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    iget v7, v3, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v5, v7}, Lcom/google/android/apps/books/app/SearchResultMap;->getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/app/SearchResultMap;->getNumMatchesBeforeTextLocation(Lcom/google/android/apps/books/annotations/TextLocation;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    move v4, v6

    .line 455
    goto :goto_0
.end method

.method protected getNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    .locals 12
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 128
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v7, p1, v9}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 129
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v7}, Lcom/google/android/apps/books/render/SpreadItems;->getLast()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/render/TextPageHandle;

    invoke-interface {v7}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v6

    .line 130
    .local v6, "startIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v6, :cond_4

    .line 131
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    iget v9, v6, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 133
    .local v4, "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v4, :cond_4

    .line 134
    iget-object v3, v4, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    .line 136
    .local v3, "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    iget v7, v6, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 138
    .local v0, "nextMatchEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    const/4 v1, 0x0

    .line 139
    .local v1, "nextMatchPage":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 140
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextMatchPage":Ljava/lang/Integer;
    check-cast v1, Ljava/lang/Integer;

    .line 142
    .restart local v1    # "nextMatchPage":Ljava/lang/Integer;
    :cond_0
    if-eqz v1, :cond_1

    .line 144
    new-instance v7, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v8, Lcom/google/android/apps/books/render/PageIndices;

    iget v9, v6, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v7, p0, v8, v11}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    .line 170
    .end local v0    # "nextMatchEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v1    # "nextMatchPage":Ljava/lang/Integer;
    .end local v3    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v4    # "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :goto_0
    return-object v7

    .line 147
    .restart local v0    # "nextMatchEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .restart local v1    # "nextMatchPage":Ljava/lang/Integer;
    .restart local v3    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .restart local v4    # "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    iget v9, v6, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v7, v9}, Lcom/google/android/apps/books/app/SearchResultMap;->findNextPassageWithSearchResults(I)I

    move-result v2

    .line 149
    .local v2, "nextPassageIndex":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7, v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isValidPassageIndex(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 150
    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 152
    .local v5, "searchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v5, :cond_2

    .line 153
    new-instance v8, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v9, Lcom/google/android/apps/books/render/PageIndices;

    iget-object v7, v5, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    invoke-virtual {v7}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {v9, v2, v7}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v8, p0, v9, v11}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    move-object v7, v8

    goto :goto_0

    .line 158
    :cond_2
    new-instance v7, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v8, Lcom/google/android/apps/books/render/PageIndices;

    invoke-direct {v8, v2, v10}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v7, p0, v8, v10}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    goto :goto_0

    .end local v5    # "searchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_3
    move-object v7, v8

    .line 161
    goto :goto_0

    .line 170
    .end local v0    # "nextMatchEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v1    # "nextMatchPage":Ljava/lang/Integer;
    .end local v2    # "nextPassageIndex":I
    .end local v3    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v4    # "passageSearchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_4
    new-instance v7, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    invoke-direct {v7, p0, v8, v10}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    goto :goto_0
.end method

.method protected getPaintables(I)Ljava/util/List;
    .locals 3
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 341
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 342
    .local v1, "resultSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/app/SearchResultMap;->getTextLocationToSearchMatch(I)Ljava/util/SortedMap;

    move-result-object v0

    .line 344
    .local v0, "map":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    if-eqz v0, :cond_0

    .line 345
    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 347
    :cond_0
    return-object v1
.end method

.method protected getPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    .locals 11
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 179
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v6, p1, v8}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 180
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v6, v9}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/render/TextPageHandle;

    invoke-interface {v6}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v5

    .line 181
    .local v5, "startIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v5, :cond_3

    .line 182
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    iget v8, v5, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 184
    .local v4, "searchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v4, :cond_3

    .line 185
    iget-object v0, v4, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    .line 187
    .local v0, "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    iget v6, v5, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/TreeMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 189
    .local v1, "prevMatchPage":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 191
    new-instance v6, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v7, Lcom/google/android/apps/books/render/PageIndices;

    iget v8, v5, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {v7, v8, v9}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v6, p0, v7, v10}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    .line 219
    .end local v0    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v1    # "prevMatchPage":Ljava/lang/Integer;
    .end local v4    # "searchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :goto_0
    return-object v6

    .line 195
    .restart local v0    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .restart local v1    # "prevMatchPage":Ljava/lang/Integer;
    .restart local v4    # "searchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    iget v8, v5, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v6, v8}, Lcom/google/android/apps/books/app/SearchResultMap;->findPreviousPassageWithSearchResults(I)I

    move-result v2

    .line 197
    .local v2, "prevPassageIndex":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v6, v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isValidPassageIndex(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 198
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 200
    .local v3, "searchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    if-eqz v3, :cond_1

    .line 201
    new-instance v7, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v8, Lcom/google/android/apps/books/render/PageIndices;

    iget-object v6, v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    invoke-virtual {v6}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v8, v2, v6}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v7, p0, v8, v10}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    move-object v6, v7

    goto :goto_0

    .line 207
    :cond_1
    new-instance v6, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    new-instance v7, Lcom/google/android/apps/books/render/PageIndices;

    const v8, 0x7fffffff

    invoke-direct {v7, v2, v8}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v6, p0, v7, v9}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    goto :goto_0

    .end local v3    # "searchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_2
    move-object v6, v7

    .line 211
    goto :goto_0

    .line 219
    .end local v0    # "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .end local v1    # "prevMatchPage":Ljava/lang/Integer;
    .end local v2    # "prevPassageIndex":I
    .end local v4    # "searchMatchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    :cond_3
    new-instance v6, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    invoke-direct {v6, p0, v7, v9}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;-><init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V

    goto :goto_0
.end method

.method public hasNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 4
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    move-result-object v0

    .line 353
    .local v0, "nextSearchMatch":Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    const-string v1, "TextModeCache"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    const-string v1, "TextModeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "has next search match: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :cond_0
    if-nez v0, :cond_1

    .line 357
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    .line 362
    :goto_0
    return-object v1

    .line 358
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->matchExists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 359
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0

    .line 362
    :cond_2
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_RESULT_YET:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0
.end method

.method public hasPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 2
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 367
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    move-result-object v0

    .line 368
    .local v0, "prevSearchMatch":Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    if-nez v0, :cond_0

    .line 369
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    .line 374
    :goto_0
    return-object v1

    .line 370
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->matchExists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0

    .line 374
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_RESULT_YET:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0
.end method

.method protected makeCachedData(ILjava/util/List;)Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    .locals 15
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)",
            "Lcom/google/android/apps/books/widget/PassageSearchMatchData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 62
    .local p2, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    const-string v12, "TextModeCache"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 63
    const-string v12, "TextModeCache"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Making PassageSearchMatchData for passage index: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getPaintables(I)Ljava/util/List;

    move-result-object v8

    .line 67
    .local v8, "paintables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 68
    .local v4, "localIdToRect":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/render/LabeledRect;>;"
    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6}, Ljava/util/TreeMap;-><init>()V

    .line 70
    .local v6, "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    iget-object v12, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    move/from16 v0, p1

    invoke-interface {v12, v0}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v7

    .line 72
    .local v7, "pages":Lcom/google/android/apps/books/util/PassagePages;
    const/4 v9, -0x1

    .line 73
    .local v9, "prevPageIndex":I
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/render/LabeledRect;

    .line 74
    .local v10, "rect":Lcom/google/android/apps/books/render/LabeledRect;
    iget-object v12, v10, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-interface {v4, v12, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-virtual {v10}, Lcom/google/android/apps/books/render/LabeledRect;->getRect()Landroid/graphics/Rect;

    move-result-object v12

    iget v11, v12, Landroid/graphics/Rect;->left:I

    .line 77
    .local v11, "xValueToSearchFor":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Lcom/google/android/apps/books/util/PassagePages;->getPageIndexForX(ILjava/lang/Integer;)I

    move-result v5

    .line 79
    .local v5, "pageIndex":I
    const/4 v12, -0x1

    if-ne v5, v12, :cond_2

    .line 80
    const-string v12, "TextModeCache"

    const/4 v13, 0x5

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 81
    const-string v12, "TextModeCache"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SearchMatchTextRange could not be associated with a page: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_2
    iget-object v12, v10, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 86
    .local v1, "currentMatchIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;

    .line 88
    .local v2, "existingMatchRange":Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;
    if-eqz v2, :cond_4

    .line 91
    iget v12, v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->min:I

    if-ge v1, v12, :cond_3

    .line 92
    iput v1, v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->min:I

    .line 94
    :cond_3
    iget v12, v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->max:I

    if-le v1, v12, :cond_1

    .line 95
    iput v1, v2, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;->max:I

    goto :goto_0

    .line 98
    :cond_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;

    invoke-direct {v13, v1, v1}, Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;-><init>(II)V

    invoke-virtual {v6, v12, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 104
    .end local v1    # "currentMatchIndex":I
    .end local v2    # "existingMatchRange":Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;
    .end local v5    # "pageIndex":I
    .end local v10    # "rect":Lcom/google/android/apps/books/render/LabeledRect;
    .end local v11    # "xValueToSearchFor":I
    :cond_5
    new-instance v12, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    move-object/from16 v0, p2

    invoke-direct {v12, v4, v6, v8, v0}, Lcom/google/android/apps/books/widget/PassageSearchMatchData;-><init>(Ljava/util/Map;Ljava/util/TreeMap;Ljava/util/List;Ljava/util/List;)V

    return-object v12
.end method

.method protected bridge synthetic makeCachedData(ILjava/util/List;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->makeCachedData(ILjava/util/List;)Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    move-result-object v0

    return-object v0
.end method

.method public maybeMoveToPendingMatch()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 266
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    if-eqz v8, :cond_0

    .line 267
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v8}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->getPassageIndex()I

    move-result v2

    .line 268
    .local v2, "passageIndex":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData;

    .line 269
    .local v3, "passageSearchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v8, v2}, Lcom/google/android/apps/books/app/SearchResultMap;->getTextLocationToSearchMatch(I)Ljava/util/SortedMap;

    move-result-object v5

    .line 271
    .local v5, "searchAnnotations":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    invoke-interface {v8, v2}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v1

    .line 273
    .local v1, "pages":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v3, :cond_1

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    .line 274
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v8}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->getLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/SearchMatchTextRange;

    .line 276
    .local v6, "searchMatchTextRange":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    if-eqz v6, :cond_1

    .line 277
    iget-object v8, v3, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->localIdToRect:Ljava/util/Map;

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/SearchMatchTextRange;->getPaintableRangeId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/LabeledRect;

    .line 279
    .local v4, "rect":Lcom/google/android/apps/books/render/LabeledRect;
    if-eqz v4, :cond_1

    .line 280
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/LabeledRect;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    iget v7, v8, Landroid/graphics/Rect;->left:I

    .line 281
    .local v7, "xValueToSearchFor":I
    invoke-virtual {v1, v7, v11}, Lcom/google/android/apps/books/util/PassagePages;->getPageIndexForX(ILjava/lang/Integer;)I

    move-result v0

    .line 283
    .local v0, "newPageIndex":I
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->moveToIndices(Lcom/google/android/apps/books/util/PassagePages;II)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 284
    iput-object v11, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    .line 313
    .end local v0    # "newPageIndex":I
    .end local v1    # "pages":Lcom/google/android/apps/books/util/PassagePages;
    .end local v2    # "passageIndex":I
    .end local v3    # "passageSearchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    .end local v4    # "rect":Lcom/google/android/apps/books/render/LabeledRect;
    .end local v5    # "searchAnnotations":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    .end local v6    # "searchMatchTextRange":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    .end local v7    # "xValueToSearchFor":I
    :cond_0
    :goto_0
    return-void

    .line 296
    .restart local v1    # "pages":Lcom/google/android/apps/books/util/PassagePages;
    .restart local v2    # "passageIndex":I
    .restart local v3    # "passageSearchData":Lcom/google/android/apps/books/widget/PassageSearchMatchData;
    .restart local v5    # "searchAnnotations":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v8}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->isLoadedOrLoading()Z

    move-result v8

    if-nez v8, :cond_0

    .line 301
    const/4 v8, 0x1

    invoke-virtual {p0, v2, v8}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->maybeLoadDataForPassage(IZ)V

    .line 302
    iget-object v8, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v9}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->getLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v9

    iget-object v9, v9, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    sget-object v10, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v8, v9, v10}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveToPosition(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    .line 310
    iput-object v11, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    goto :goto_0
.end method

.method protected moveToPrevOrNextMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .locals 9
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "searchingForward"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 226
    if-eqz p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    move-result-object v3

    .line 228
    .local v3, "nextSearchMatch":Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    :goto_0
    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->indices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v6, :cond_0

    .line 229
    iget-object v0, v3, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->indices:Lcom/google/android/apps/books/render/PageIndices;

    .line 230
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    iget-boolean v1, v3, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->exactMatch:Z

    .line 231
    .local v1, "isExactMatch":Z
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;->getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v4

    .line 241
    .local v4, "pages":Lcom/google/android/apps/books/util/PassagePages;
    iget v6, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {p0, v6, v8}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->maybeLoadDataForPassage(IZ)V

    .line 242
    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    .line 243
    iget v6, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-direct {p0, v4, v6, v7}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->moveToIndices(Lcom/google/android/apps/books/util/PassagePages;II)Z

    .line 262
    .end local v0    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .end local v1    # "isExactMatch":Z
    .end local v4    # "pages":Lcom/google/android/apps/books/util/PassagePages;
    :cond_0
    :goto_1
    return-void

    .line 226
    .end local v3    # "nextSearchMatch":Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->getPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;

    move-result-object v3

    goto :goto_0

    .line 245
    .restart local v0    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .restart local v1    # "isExactMatch":Z
    .restart local v3    # "nextSearchMatch":Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
    .restart local v4    # "pages":Lcom/google/android/apps/books/util/PassagePages;
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/app/SearchResultMap;->getTextLocationToSearchMatch(I)Ljava/util/SortedMap;

    move-result-object v5

    .line 247
    .local v5, "searchAnnotations":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    if-eqz v5, :cond_4

    invoke-interface {v5}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 248
    if-eqz p2, :cond_3

    invoke-interface {v5}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    move-object v2, v6

    .line 250
    .local v2, "location":Lcom/google/android/apps/books/annotations/TextLocation;
    :goto_2
    new-instance v6, Lcom/google/android/apps/books/render/PendingSearchMatchData;

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-direct {v6, v2, v7, v8}, Lcom/google/android/apps/books/render/PendingSearchMatchData;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;IZ)V

    iput-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    .line 252
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v7}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->getLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    sget-object v8, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v6, v7, v8}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveToPosition(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    goto :goto_1

    .line 248
    .end local v2    # "location":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_3
    invoke-interface {v5}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    move-object v2, v6

    goto :goto_2

    .line 255
    :cond_4
    const-string v6, "TextModeCache"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 256
    const-string v6, "TextModeCache"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "moveToPrevOrNextMatch reported match in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " but no match found."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onLoadedRangeDataBulkSuccess()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onSearchBarNavigationStateChanged()V

    .line 114
    return-void
.end method
