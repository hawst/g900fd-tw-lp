.class public Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final compoundResourceId:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "acct"    # Ljava/lang/String;
    .param p2, "vol"    # Ljava/lang/String;
    .param p3, "res"    # Ljava/lang/String;

    .prologue
    .line 2645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2646
    iput-object p1, p0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->accountName:Ljava/lang/String;

    .line 2647
    iput-object p2, p0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->volumeId:Ljava/lang/String;

    .line 2648
    iput-object p3, p0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->compoundResourceId:Ljava/lang/String;

    .line 2649
    return-void
.end method
