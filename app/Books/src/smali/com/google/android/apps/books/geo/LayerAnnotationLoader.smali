.class public Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
.super Ljava/lang/Object;
.source "LayerAnnotationLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;
    }
.end annotation


# instance fields
.field private final mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

.field private final mAnnotationListener:Lcom/google/android/apps/books/annotations/StubAnnotationListener;

.field private final mLayerId:Ljava/lang/String;

.field private final mPassageAnnotationLoadResult:Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

.field private final mPassageIndexToLoadResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mRequestedSegmentIndexes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegmentIndexToLoadResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/geo/AnnotationSet;)V
    .locals 6
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "layerId"    # Ljava/lang/String;
    .param p3, "annotationController"    # Lcom/google/android/apps/books/annotations/AnnotationController;
    .param p4, "annotationSet"    # Lcom/google/android/apps/books/geo/AnnotationSet;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mRequestedSegmentIndexes:Ljava/util/Set;

    .line 43
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mSegmentIndexToLoadResult:Ljava/util/Map;

    .line 48
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageIndexToLoadResult:Ljava/util/Map;

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mLayerId:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    .line 66
    new-instance v0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;-><init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)V

    .line 75
    .local v0, "inLayer":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Lcom/google/android/apps/books/annotations/Annotation;>;"
    new-instance v1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;

    invoke-direct {v1, p0, p4, v0}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$2;-><init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/apps/books/geo/AnnotationSet;Lcom/google/common/base/Predicate;)V

    iput-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageAnnotationLoadResult:Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

    .line 84
    new-instance v1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;-><init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)V

    iput-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mAnnotationListener:Lcom/google/android/apps/books/annotations/StubAnnotationListener;

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/books/annotations/AnnotationListener;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mAnnotationListener:Lcom/google/android/apps/books/annotations/StubAnnotationListener;

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationController;->weaklyAddListeners(Lcom/google/android/apps/books/annotations/VolumeVersion;[Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mLayerId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->onLoadedAnnotationsForSegmentIndex(ILcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageAnnotationLoadResult:Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

    return-object v0
.end method

.method private getAnnotationLoadResultForPassageIndex(I)Lcom/google/android/apps/books/util/Eventual;
    .locals 5
    .param p1, "passage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageIndexToLoadResult:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/Eventual;

    .line 119
    .local v1, "result":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-nez v1, :cond_0

    .line 120
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v1

    .line 121
    iget-object v3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageIndexToLoadResult:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 126
    .local v2, "segmentIndex":I
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->loadAnnotationsForSegmentIndex(I)V

    goto :goto_0

    .line 129
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "segmentIndex":I
    :cond_0
    return-object v1
.end method

.method private loadAnnotationsForSegmentIndex(I)V
    .locals 3
    .param p1, "segmentIndex"    # I

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->startAnnotationFetch(I)V

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mSegmentIndexToLoadResult:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    .line 142
    .local v0, "loadResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    if-eqz v0, :cond_0

    .line 143
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->onLoadedAnnotationsForSegmentIndex(ILcom/google/android/apps/books/util/ExceptionOr;)V

    .line 145
    :cond_0
    return-void
.end method

.method private onLoadedAnnotationsForSegmentIndex(ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 9
    .param p1, "segmentIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p2, "loadResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    const-string v6, "PlaceCardProvider"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 174
    const-string v6, "PlaceCardProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Annotations for segment "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " loaded with error "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mSegmentIndexToLoadResult:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v6, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForSegmentIndex(I)I

    move-result v3

    .line 182
    .local v3, "passageIndex":I
    const/4 v4, 0x0

    .line 183
    .local v4, "segmentAnnotationsNotLoaded":Z
    const/4 v0, 0x0

    .line 185
    .local v0, "error":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 186
    .local v2, "otherSegmentIndex":I
    iget-object v6, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mSegmentIndexToLoadResult:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/util/ExceptionOr;

    .line 188
    .local v5, "segmentLoadResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v6

    if-nez v6, :cond_1

    .line 189
    :cond_2
    const/4 v4, 0x1

    .line 190
    invoke-virtual {v5}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 197
    .end local v2    # "otherSegmentIndex":I
    .end local v5    # "segmentLoadResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    :cond_3
    if-eqz v4, :cond_4

    if-eqz v0, :cond_5

    .line 198
    :cond_4
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->getAnnotationLoadResultForPassageIndex(I)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 200
    :cond_5
    return-void
.end method

.method private startAnnotationFetch(I)V
    .locals 5
    .param p1, "segmentIndex"    # I

    .prologue
    .line 148
    iget-object v2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mRequestedSegmentIndexes:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 150
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegmentIdAtIndex(I)Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "segmentId":Ljava/lang/String;
    const-string v2, "LayerAnnotationLoader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    const-string v2, "LayerAnnotationLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading geo annotations for segment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v3, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v4, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mLayerId:Ljava/lang/String;

    invoke-interface {v3, v1, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeAnnotationRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationController;->fetchVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mRequestedSegmentIndexes:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    .end local v1    # "segmentId":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, "LayerAnnotationLoader"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    const-string v2, "LayerAnnotationLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error loading segment geo annotations: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public load(I)V
    .locals 0
    .param p1, "passageIndex"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->getAnnotationLoadResultForPassageIndex(I)Lcom/google/android/apps/books/util/Eventual;

    .line 115
    return-void
.end method

.method public load(ILcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->getAnnotationLoadResultForPassageIndex(I)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;-><init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 111
    return-void
.end method
