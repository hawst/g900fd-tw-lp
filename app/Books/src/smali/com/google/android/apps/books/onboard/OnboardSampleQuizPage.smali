.class public Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
.super Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;
.source "OnboardSampleQuizPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;,
        Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;,
        Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;
    }
.end annotation


# static fields
.field private static final MAX_SAMPLE_WAIT_TIME_MS:J

.field private static final SAMPLE_POLL_DELAY_MS:J


# instance fields
.field private final ROWS_FROM_END_TO_DISPLAY_MORE:I

.field private mAddableSampleData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mInitialSamplesToFetch:I

.field private mLastFetchGenres:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNextPageToken:Ljava/lang/String;

.field private mNumColumns:I

.field private final mObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private mOutstandingImageFetches:I

.field private mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

.field final mPeriodicExecutorCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

.field private mRequestId:I

.field private final mRowsFromEndToFetchMore:I

.field private final mSampleVolumeIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServerIndex:I

.field private mShowDone:Z

.field private final mSubsequentRowsToFetch:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->MAX_SAMPLE_WAIT_TIME_MS:J

    .line 50
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->SAMPLE_POLL_DELAY_MS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 196
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    iput-boolean v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z

    .line 153
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mServerIndex:I

    .line 155
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    .line 157
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    .line 163
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;

    .line 165
    iput-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 167
    iput-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    .line 182
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->ROWS_FROM_END_TO_DISPLAY_MORE:I

    .line 432
    new-instance v2, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V

    iput-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutorCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    .line 197
    invoke-static {p1}, Lcom/google/android/apps/books/util/ReaderUtils;->currentlyInLandscape(Landroid/content/Context;)Z

    move-result v1

    .line 198
    .local v1, "landscape":Z
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getNumColumns()I

    move-result v0

    .line 199
    .local v0, "columnsPerRow":I
    if-eqz v1, :cond_0

    mul-int v2, v0, v0

    :goto_0
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mInitialSamplesToFetch:I

    .line 202
    if-eqz v1, :cond_1

    const/4 v2, 0x3

    :goto_1
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRowsFromEndToFetchMore:I

    .line 203
    if-eqz v1, :cond_2

    :goto_2
    iput v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSubsequentRowsToFetch:I

    .line 205
    new-instance v2, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V

    iput-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 215
    return-void

    .line 199
    :cond_0
    mul-int/lit8 v2, v0, 0x2

    mul-int/2addr v2, v0

    goto :goto_0

    :cond_1
    move v2, v3

    .line 202
    goto :goto_1

    .line 203
    :cond_2
    const/4 v3, 0x7

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$108(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    return v0
.end method

.method static synthetic access$110(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/play/onboard/OnboardHostControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    return-object v0
.end method

.method static synthetic access$1200()J
    .locals 2

    .prologue
    .line 46
    sget-wide v0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->MAX_SAMPLE_WAIT_TIME_MS:J

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800()J
    .locals 2

    .prologue
    .line 46
    sget-wide v0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->SAMPLE_POLL_DELAY_MS:J

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/libraries/bind/card/CardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    return-object v0
.end method

.method static synthetic access$208(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mServerIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mServerIndex:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/libraries/bind/card/CardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->bulkDisplayNewSamples()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSubsequentRowsToFetch:I

    return v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->maybeLoadMoreSamples(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/graphics/Bitmap;
    .param p4, "x4"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/play/onboard/OnboardHostControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    return-object v0
.end method

.method private bulkDisplayNewSamples()V
    .locals 4

    .prologue
    .line 494
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    :goto_0
    return-void

    .line 498
    :cond_0
    const-string v1, "OnboardSampleQuizPage"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    const-string v1, "OnboardSampleQuizPage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding new samples to DataList "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    .line 503
    .local v0, "snapshotList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 504
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->serverOrderSortAndUpdateSnapshot(Ljava/util/List;)V

    .line 505
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 5
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p4, "serverIndex"    # I

    .prologue
    const v4, 0x7f0e0033

    .line 228
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 229
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    const v2, 0x7f0e002f

    invoke-virtual {v1, v2, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 230
    invoke-virtual {v1, v4, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 231
    const v2, 0x7f0e0031

    invoke-virtual {v1, v2, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 232
    const v2, 0x7f0e0035

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 234
    const v2, 0x7f0e0036

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 236
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    const v3, 0x7f040073

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 238
    if-nez p3, :cond_1

    .line 242
    const v2, 0x7f0e0034

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/StringUtils;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 244
    const/high16 v0, 0x3f800000    # 1.0f

    .line 245
    .local v0, "aspectRatio":F
    const-string v2, "OnboardSampleQuizPage"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    const-string v2, "OnboardSampleQuizPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Null cover for volume: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    :goto_0
    const v2, 0x7f0e0032

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 253
    const v2, 0x7f0e0030

    const v3, 0x7f02009a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 254
    const v2, 0x7f0e0039

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 255
    return-object v1

    .line 249
    .end local v0    # "aspectRatio":F
    :cond_1
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .restart local v0    # "aspectRatio":F
    goto :goto_0
.end method

.method private initialize(Z)V
    .locals 4
    .param p1, "clearSelections"    # Z

    .prologue
    .line 468
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->maybeFetchSamples(Z)V

    .line 469
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutorCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    const-wide/16 v2, 0x3e8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->schedule()V

    .line 472
    return-void
.end method

.method private maybeFetchSamples(Z)V
    .locals 6
    .param p1, "clearSelections"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 539
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    check-cast v1, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getSelectedGenres()Ljava/util/List;

    move-result-object v0

    .line 541
    .local v0, "genres":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 542
    :cond_0
    const-string v1, "OnboardSampleQuizPage"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 543
    const-string v3, "OnboardSampleQuizPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New genres need to be fetched. Sizes: old="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " new="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_1
    iput-object v5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;

    .line 548
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    .line 549
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 550
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData(Z)V

    .line 551
    if-eqz p1, :cond_2

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSelectedItemIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 554
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 555
    iget v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I

    .line 556
    iput v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mServerIndex:I

    .line 557
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    iget v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mInitialSamplesToFetch:I

    new-instance v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;

    iget v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;ILcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;)V

    invoke-virtual {v1, v0, v2, v5, v3}, Lcom/google/android/apps/books/app/OnboardingController;->getSampleVolumes(Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 559
    iput-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    .line 561
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 543
    goto :goto_0
.end method

.method private maybeLoadMoreSamples(I)V
    .locals 9
    .param p1, "numSamplesToRequest"    # I

    .prologue
    const/4 v8, 0x0

    .line 509
    iget v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I

    if-lez v4, :cond_1

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 517
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    const/16 v5, 0x64

    if-ge v4, v5, :cond_0

    .line 521
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v0

    .line 522
    .local v0, "lastVisibleRow":I
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/card/CardListView;->getCount()I

    move-result v3

    .line 523
    .local v3, "totalRowCount":I
    const-string v4, "OnboardSampleQuizPage"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 524
    const-string v4, "OnboardSampleQuizPage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Maybe requesting samples lastVisibleRow="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", totalRowCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getNumColumns()I

    move-result v5

    div-int v2, v4, v5

    .line 529
    .local v2, "numberOfRowsDownloaded":I
    iget v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRowsFromEndToFetchMore:I

    add-int/2addr v4, v0

    if-lt v4, v2, :cond_0

    .line 530
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;

    .line 532
    .local v1, "nextPageToken":Ljava/lang/String;
    iput-object v8, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;

    .line 533
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    iget-object v5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mLastFetchGenres:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;

    iget v7, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I

    invoke-direct {v6, p0, v7, v8}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;ILcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;)V

    invoke-virtual {v4, v5, p1, v1, v6}, Lcom/google/android/apps/books/app/OnboardingController;->getSampleVolumes(Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private maybeStopExecutor()V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->stop()V

    .line 477
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mPeriodicExecutor:Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    .line 479
    :cond_0
    return-void
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z

    return v0
.end method

.method protected getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 303
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0207

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 283
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setEnabled(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getShowFullSequence()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected getHeaderText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f020b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNumColumns()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNumColumns:I

    if-nez v0, :cond_0

    .line 261
    const v0, 0x7f0900fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->columnsPerRow(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNumColumns:I

    .line 263
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNumColumns:I

    return v0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f020d

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$2;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 0
    .param p1, "bdc"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 225
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->onAttachedToWindow()V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mQuizItemDataListFiltered:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 221
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 483
    invoke-super {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->onDetachedFromWindow()V

    .line 484
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->maybeStopExecutor()V

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mQuizItemDataListFiltered:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 486
    return-void
.end method

.method public onEnterPage(Z)V
    .locals 3
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 461
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->initialize(Z)V

    .line 462
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0211

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "accessibilityAnnouncement":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0, v0}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public onExitPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->maybeStopExecutor()V

    .line 491
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 566
    invoke-super {p0, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 567
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->initialize(Z)V

    .line 568
    return-void
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 572
    invoke-super {p0, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 574
    const-string v1, "onboard_samples_selected"

    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSelectedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 576
    return-void

    .line 574
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
