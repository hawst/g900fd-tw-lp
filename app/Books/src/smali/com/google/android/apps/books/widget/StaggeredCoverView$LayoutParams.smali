.class public Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "StaggeredCoverView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public final fillWidth:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "fillWidth"    # Z

    .prologue
    .line 93
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 94
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->fillWidth:Z

    .line 95
    return-void
.end method
