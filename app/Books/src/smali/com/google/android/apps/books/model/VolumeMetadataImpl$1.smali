.class Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;
.super Ljava/lang/Object;
.source "VolumeMetadataImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeMetadataImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/apps/books/model/Resource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/VolumeMetadataImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;->this$0:Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 2
    .param p1, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 162
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;->this$0:Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    # getter for: Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;
    invoke-static {v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->access$000(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/IdentifiableCollection;->getIdToValue()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Resource;

    .line 163
    .local v0, "fromManifest":Lcom/google/android/apps/books/model/Resource;
    if-eqz v0, :cond_0

    .line 166
    .end local v0    # "fromManifest":Lcom/google/android/apps/books/model/Resource;
    :goto_0
    return-object v0

    .restart local v0    # "fromManifest":Lcom/google/android/apps/books/model/Resource;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;->this$0:Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    # getter for: Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceCollector:Lcom/google/android/apps/books/render/ResourceCollector;
    invoke-static {v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->access$100(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)Lcom/google/android/apps/books/render/ResourceCollector;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/ResourceCollector;->apply(Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 159
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;->apply(Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v0

    return-object v0
.end method
