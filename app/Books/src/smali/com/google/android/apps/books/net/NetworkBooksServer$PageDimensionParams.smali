.class Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;
.super Ljava/lang/Object;
.source "NetworkBooksServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/NetworkBooksServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PageDimensionParams"
.end annotation


# instance fields
.field private final heightString:Ljava/lang/String;

.field private widthString:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->widthString:Ljava/lang/String;

    .line 157
    iput-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->heightString:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$1;

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->heightString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->widthString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$PageDimensionParams;->widthString:Ljava/lang/String;

    return-object p1
.end method
