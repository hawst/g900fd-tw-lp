.class Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;
.super Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;
.source "HoneycombResourceContentStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->configureWebView(Landroid/accounts/Account;Ljava/lang/String;Landroid/webkit/WebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Landroid/webkit/WebViewClient;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$clientDelegate:Landroid/webkit/WebViewClient;

.field final synthetic val$exceptionHandler:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

.field final synthetic val$resourceIdToResource:Lcom/google/common/base/Function;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/webkit/WebViewClient;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    iput-object p2, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$clientDelegate:Landroid/webkit/WebViewClient;

    iput-object p3, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$volumeId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    iput-object p6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$resourceIdToResource:Lcom/google/common/base/Function;

    iput-object p7, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$exceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;-><init>()V

    return-void
.end method

.method private isValidUriAuthority(Ljava/lang/String;)Z
    .locals 1
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    .line 208
    const-string v0, "com.google.android.apps.books"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isValidUriScheme(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 212
    const-string v0, "books-content"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "content"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 10
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 131
    const-string v6, "HCResourceStore"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 132
    const-string v6, "HCResourceStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "shouldInterceptRequest(url="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    # getter for: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mShutDown:Z
    invoke-static {v6}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$100(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 137
    const-string v6, "HCResourceStore"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 138
    const-string v6, "HCResourceStore"

    const-string v7, "shut down, returning 404 not found"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_1
    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildNotFoundResponse()Landroid/webkit/WebResourceResponse;
    invoke-static {}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$200()Landroid/webkit/WebResourceResponse;

    move-result-object v6

    .line 204
    :goto_0
    return-object v6

    .line 144
    :cond_2
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 145
    .local v4, "parsedUri":Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/apps/books/util/Config;->isBooksUri(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 146
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$account:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$volumeId:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->urlToResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 175
    :cond_3
    :try_start_0
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "content"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 177
    .local v1, "contentUri":Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    .line 178
    .local v3, "match":I
    sparse-switch v3, :sswitch_data_0

    .line 203
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "match":I
    :goto_1
    const-string v6, "HCResourceStore"

    const-string v7, "unable to build valid response, returning 404 not found"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildNotFoundResponse()Landroid/webkit/WebResourceResponse;
    invoke-static {}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$200()Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto :goto_0

    .line 149
    :cond_4
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "scheme":Ljava/lang/String;
    const-string v6, "data"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 151
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto :goto_0

    .line 152
    :cond_5
    const-string v6, "file"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 153
    sget-object v6, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "compileJs":Ljava/lang/String;
    sget-object v6, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_SIDELOADED_CONFIG_VALUE:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 156
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto :goto_0

    .line 161
    :cond_6
    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildNotFoundResponse()Landroid/webkit/WebResourceResponse;
    invoke-static {}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$200()Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto :goto_0

    .line 163
    .end local v0    # "compileJs":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->isValidUriScheme(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->isValidUriAuthority(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 166
    :cond_8
    const-string v6, "HCResourceStore"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 167
    const-string v6, "HCResourceStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Warning volume requested a non-Google Books URL: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_9
    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildNotFoundResponse()Landroid/webkit/WebResourceResponse;
    invoke-static {}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$200()Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto/16 :goto_0

    .line 180
    .end local v5    # "scheme":Ljava/lang/String;
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "match":I
    :sswitch_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    iget-object v7, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$account:Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$volumeId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildSectionContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/webkit/WebResourceResponse;
    invoke-static {v6, v7, v8, v1, v9}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$300(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto/16 :goto_0

    .line 184
    :sswitch_1
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    iget-object v7, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$account:Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$volumeId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$resourceIdToResource:Lcom/google/common/base/Function;

    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildResourceContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/base/Function;)Landroid/webkit/WebResourceResponse;
    invoke-static {v6, v7, v8, v1, v9}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$400(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/base/Function;)Landroid/webkit/WebResourceResponse;

    move-result-object v6

    goto/16 :goto_0

    .line 188
    :sswitch_2
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->this$0:Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    # invokes: Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildSharedResourceContentResponse(Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;
    invoke-static {v6, v1}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->access$500(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;
    :try_end_1
    .catch Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v6

    goto/16 :goto_0

    .line 191
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "match":I
    :catch_0
    move-exception v2

    .line 194
    .local v2, "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$exceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v6, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 195
    .end local v2    # "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    :catch_1
    move-exception v2

    .line 196
    .local v2, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$exceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v6, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 197
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 200
    .local v2, "e":Ljava/lang/Throwable;
    iget-object v6, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$exceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v6, v7}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 178
    :sswitch_data_0
    .sparse-switch
        0xca -> :sswitch_0
        0x12e -> :sswitch_1
        0x1a5 -> :sswitch_2
    .end sparse-switch
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$clientDelegate:Landroid/webkit/WebViewClient;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;->val$clientDelegate:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    .line 116
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
