.class public interface abstract Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;
.super Ljava/lang/Object;
.source "ScrubBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScrubberMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getAvailableContentFraction()F
.end method

.method public abstract getCalloutPageLabelLong(IZ)Ljava/lang/CharSequence;
.end method

.method public abstract getItemCount()I
.end method

.method public abstract getMaxViewableIndex()I
.end method

.method public abstract getScrubberDescription(I)Ljava/lang/CharSequence;
.end method

.method public abstract isRightToLeft()Z
.end method
