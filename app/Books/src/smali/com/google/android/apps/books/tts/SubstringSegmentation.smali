.class abstract Lcom/google/android/apps/books/tts/SubstringSegmentation;
.super Lcom/google/android/apps/books/tts/BaseTtsSegmentation;
.source "SubstringSegmentation.java"


# instance fields
.field private final mItemIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/Range;",
            ">;"
        }
    .end annotation
.end field

.field protected final mLocale:Ljava/util/Locale;

.field protected final mThreadLocalBreakIterator:Ljava/text/BreakIterator;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/tts/SubstringSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;Ljava/text/BreakIterator;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;Ljava/text/BreakIterator;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "breakIterator"    # Ljava/text/BreakIterator;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mLocale:Ljava/util/Locale;

    .line 40
    iput-object p4, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mThreadLocalBreakIterator:Ljava/text/BreakIterator;

    .line 41
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/tts/SubstringSegmentation;->buildSegmentation(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mItemIndices:Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method protected abstract buildSegmentation(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/Range;",
            ">;"
        }
    .end annotation
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mItemIndices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemRange(I)Lcom/google/android/apps/books/util/Range;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mItemIndices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/Range;

    return-object v0
.end method

.method public getItemText(I)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mItemIndices:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/Range;

    .line 59
    .local v0, "range":Lcom/google/android/apps/books/util/Range;
    iget-object v1, p0, Lcom/google/android/apps/books/tts/SubstringSegmentation;->mText:Ljava/lang/String;

    iget v2, v0, Lcom/google/android/apps/books/util/Range;->start:I

    iget v3, v0, Lcom/google/android/apps/books/util/Range;->end:I

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
