.class public Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
.super Lcom/google/android/apps/books/geo/AnnotationSet;
.source "ListeningAnnotationSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

.field private final mListeners:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoadedLayers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V
    .locals 1
    .param p1, "ordering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p2, "loadingTimer"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/geo/AnnotationSet;-><init>(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V

    .line 26
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListeners:Lcom/google/common/collect/Multimap;

    .line 28
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mLoadedLayers:Ljava/util/Set;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;-><init>(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mLoadedLayers:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)Lcom/google/common/collect/Multimap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListeners:Lcom/google/common/collect/Multimap;

    return-object v0
.end method


# virtual methods
.method public addAnnotationSetChangeListener(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "changeListener"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListeners:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    return-object v0
.end method

.method public hasLoaded(Ljava/lang/String;)Z
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mLoadedLayers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAnnotationSetChangeListener(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "changeListener"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListeners:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/Multimap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 75
    return-void
.end method
