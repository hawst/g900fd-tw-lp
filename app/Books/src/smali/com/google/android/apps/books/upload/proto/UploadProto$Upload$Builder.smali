.class public final Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "UploadProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;",
        "Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 3

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;-><init>()V

    .line 286
    .local v0, "builder":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    new-instance v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;-><init>(Lcom/google/android/apps/books/upload/proto/UploadProto$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 287
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .locals 3

    .prologue
    .line 331
    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    if-nez v1, :cond_0

    .line 332
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 336
    .local v0, "returnMe":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 337
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2

    .prologue
    .line 304
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->mergeFrom(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    return-object v0
.end method

.method public hasTransferHandle()Z
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .prologue
    .line 341
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getDefaultInstance()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-object p0

    .line 342
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 345
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getUploadPercentage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setUploadPercentage(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 348
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 349
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 351
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 352
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->mergeTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 354
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 355
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setOrigFileName(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 357
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 358
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getFileSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setFileSize(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    .line 360
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    .line 372
    .local v2, "tag":I
    sparse-switch v2, :sswitch_data_0

    .line 376
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 377
    :sswitch_0
    return-object p0

    .line 382
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 386
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setUploadPercentage(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 390
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 391
    .local v0, "rawValue":I
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->valueOf(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v3

    .line 392
    .local v3, "value":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    if-eqz v3, :cond_0

    .line 393
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 398
    .end local v0    # "rawValue":I
    .end local v3    # "value":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    :sswitch_4
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->newBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v1

    .line 399
    .local v1, "subBuilder":Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->hasTransferHandle()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    .line 402
    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 403
    invoke-virtual {v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->buildPartial()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 407
    .end local v1    # "subBuilder":Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setOrigFileName(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 411
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setFileSize(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 415
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    goto :goto_0

    .line 372
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1000(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1000(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->newBuilder(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->buildPartial()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1002(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 511
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$902(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 512
    return-object p0

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1002(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    goto :goto_0
.end method

.method public setFileSize(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1302(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->fileSize_:I
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1402(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;I)I

    .line 551
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 431
    if-nez p1, :cond_0

    .line 432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$302(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$402(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;

    .line 436
    return-object p0
.end method

.method public setOrigFileName(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 528
    if-nez p1, :cond_0

    .line 529
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1102(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->origFileName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1202(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;

    .line 533
    return-object p0
.end method

.method public setStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 470
    if-nez p1, :cond_0

    .line 471
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$702(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->status_:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$802(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 475
    return-object p0
.end method

.method public setTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 491
    if-nez p1, :cond_0

    .line 492
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$902(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1002(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 496
    return-object p0
.end method

.method public setUploadPercentage(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$502(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->uploadPercentage_:I
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$602(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;I)I

    .line 454
    return-object p0
.end method

.method public setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 567
    if-nez p1, :cond_0

    .line 568
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 570
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1502(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->volumeId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->access$1602(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;

    .line 572
    return-object p0
.end method
