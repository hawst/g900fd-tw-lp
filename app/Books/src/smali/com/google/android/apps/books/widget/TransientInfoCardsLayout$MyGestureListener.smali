.class Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;

    .prologue
    .line 452
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    return-void
.end method

.method private constrainCardsViewTranslation(F)F
    .locals 8
    .param p1, "newTranslation"    # F

    .prologue
    .line 512
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    .line 513
    .local v0, "cardsView":Landroid/view/View;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/ublib/view/TranslationHelper;->getTop(Landroid/view/View;)I

    move-result v1

    .line 518
    .local v1, "currentTop":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v7, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    invoke-static {v6, v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v6

    sub-int/2addr v6, v1

    int-to-float v4, v6

    .line 520
    .local v4, "translationToForegroundDefault":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v1

    int-to-float v5, v6

    .line 522
    .local v5, "translationToScrolledToBottom":F
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 525
    .local v3, "minTranslation":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v7, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    invoke-static {v6, v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v6

    sub-int v2, v6, v1

    .line 527
    .local v2, "maxTranslation":I
    int-to-float v6, v2

    invoke-static {p1, v3, v6}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v6

    return v6
.end method

.method private getMinimumFlingVelocity()I
    .locals 2

    .prologue
    .line 455
    const/high16 v0, 0x42c80000    # 100.0f

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private updateScroll(F)V
    .locals 6
    .param p1, "dy"    # F

    .prologue
    .line 531
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v1

    .line 532
    .local v1, "cardsView":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v2

    .line 533
    .local v2, "currentTranslation":F
    add-float v4, v2, p1

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->constrainCardsViewTranslation(F)F

    move-result v0

    .line 535
    .local v0, "boundedTranslation":F
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v4

    const/4 v5, 0x0

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getLowerState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v4, v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$900(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v3

    .line 536
    .local v3, "lowerState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsTranslation(FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    invoke-static {v4, v0, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1000(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 537
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 461
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 463
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/view/VelocityTracker;

    move-result-object v4

    const/16 v10, 0x3e8

    invoke-virtual {v4, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 464
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/view/VelocityTracker;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    .line 465
    .local v3, "velocity":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->getMinimumFlingVelocity()I

    move-result v10

    int-to-float v10, v10

    cmpg-float v4, v4, v10

    if-gez v4, :cond_0

    move v2, v0

    .line 504
    .end local v3    # "velocity":F
    :goto_0
    return v2

    .line 469
    .restart local v3    # "velocity":F
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v6

    .line 473
    .local v6, "cardsView":Landroid/view/View;
    const/4 v0, 0x0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_2

    .line 478
    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 479
    .local v5, "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    invoke-static {v0, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v7

    .line 480
    .local v7, "foregroundTop":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v0

    int-to-float v4, v7

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 481
    const/4 v1, 0x0

    .line 497
    .end local v7    # "foregroundTop":I
    .local v1, "newTranslationY":F
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v0

    if-eq v5, v0, :cond_5

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V
    invoke-static {v0, v5, v2, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$700(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V

    goto :goto_0

    .line 483
    .end local v1    # "newTranslationY":F
    .restart local v7    # "foregroundTop":I
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v0

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v8, v0, v4

    .line 484
    .local v8, "newTop":I
    sub-int v0, v8, v7

    int-to-float v1, v0

    .restart local v1    # "newTranslationY":F
    goto :goto_1

    .line 487
    .end local v1    # "newTranslationY":F
    .end local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .end local v7    # "foregroundTop":I
    .end local v8    # "newTop":I
    :cond_2
    const/4 v1, 0x0

    .line 488
    .restart local v1    # "newTranslationY":F
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v9

    .line 489
    .local v9, "translatedTop":F
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    invoke-static {v0, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v9, v0

    if-gez v0, :cond_3

    .line 490
    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 491
    .end local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    invoke-static {v0, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v9, v0

    if-gez v0, :cond_4

    .line 492
    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 494
    .end local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_4
    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 500
    .end local v9    # "translatedTop":F
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const/4 v4, 0x0

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateCardsViewTop(FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$800(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    goto :goto_0

    .end local v1    # "newTranslationY":F
    .end local v3    # "velocity":F
    .end local v5    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .end local v6    # "cardsView":Landroid/view/View;
    :cond_6
    move v2, v0

    .line 504
    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 541
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v2, v3

    .line 569
    :goto_0
    return v2

    .line 550
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/view/VelocityTracker;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 554
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mIsScrollingVertically:Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1100(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 556
    neg-float v3, p4

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->updateScroll(F)V

    goto :goto_0

    .line 559
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 560
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 561
    .local v1, "startY":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 562
    .local v0, "endY":F
    sub-float v4, v0, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 564
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # setter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mIsScrollingVertically:Z
    invoke-static {v3, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1102(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Z)Z

    .line 565
    neg-float v3, p4

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->updateScroll(F)V

    goto :goto_0

    .end local v0    # "endY":F
    .end local v1    # "startY":F
    :cond_3
    move v2, v3

    .line 569
    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 576
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v5

    const v6, 0x7f0e0100

    invoke-virtual {v5, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 577
    .local v0, "actionButton":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 578
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/MathUtils;->round(F)I

    move-result v1

    .line 579
    .local v1, "eventX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/MathUtils;->round(F)I

    move-result v2

    .line 581
    .local v2, "eventY":I
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 582
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 583
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v6

    aget v6, v6, v3

    iget-object v7, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v7

    aget v7, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 584
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getLocationOnScreen([I)V

    .line 585
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v6

    aget v6, v6, v3

    neg-int v6, v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I

    move-result-object v7

    aget v7, v7, v4

    neg-int v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 587
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 596
    .end local v1    # "eventX":I
    .end local v2    # "eventY":I
    :cond_0
    :goto_0
    return v3

    .line 592
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v5, v6, :cond_0

    .line 593
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z

    move v3, v4

    .line 594
    goto :goto_0
.end method
