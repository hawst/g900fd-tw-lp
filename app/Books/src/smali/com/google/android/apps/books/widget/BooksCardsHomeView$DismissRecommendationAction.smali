.class public final enum Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;
.super Ljava/lang/Enum;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DismissRecommendationAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

.field public static final enum HAVE_IT:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

.field public static final enum NOT_INTERESTED:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;


# instance fields
.field private final mUiStringResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 668
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    const-string v1, "HAVE_IT"

    const v2, 0x7f0f0083

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->HAVE_IT:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .line 669
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    const-string v1, "NOT_INTERESTED"

    const v2, 0x7f0f0082

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->NOT_INTERESTED:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .line 666
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->HAVE_IT:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->NOT_INTERESTED:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->$VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "uiStringResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674
    iput p3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->mUiStringResourceId:I

    .line 675
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 666
    const-class v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;
    .locals 1

    .prologue
    .line 666
    sget-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->$VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    return-object v0
.end method


# virtual methods
.method public getUiStringResourceId()I
    .locals 1

    .prologue
    .line 677
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->mUiStringResourceId:I

    return v0
.end method
