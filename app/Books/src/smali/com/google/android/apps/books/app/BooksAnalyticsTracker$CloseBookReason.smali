.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CloseBookReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum BAD_ACCESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum BLOCKED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum CONCURRENT_ACCESS_DENIED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum EOF_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum EXTERNAL_STORAGE_INCONSISTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum EXTERNAL_STORAGE_UNAVAILABLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum FILE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum NO_CONTENT_VERSION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum OFFLINE_LIMIT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum OTHER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum RENTAL_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum RESOURCE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum ROOT_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum SESSION_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum SOCKET_TIMEOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum UNSUPPORTED_UPLOADED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

.field public static final enum ZIP_DATA_FORMAT_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 587
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "CONCURRENT_ACCESS_DENIED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->CONCURRENT_ACCESS_DENIED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 588
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "NO_CONTENT_VERSION"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->NO_CONTENT_VERSION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 589
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "ROOT_KEY_EXPIRED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ROOT_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 590
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "OFFLINE_LIMIT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE_LIMIT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 591
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "RENTAL_EXPIRED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RENTAL_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 592
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "BLOCKED_CONTENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BLOCKED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 593
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "SESSION_KEY_EXPIRED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SESSION_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 594
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "OFFLINE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 595
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "EXTERNAL_STORAGE_UNAVAILABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_UNAVAILABLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 596
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "EXTERNAL_STORAGE_INCONSISTENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_INCONSISTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 597
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "EOF_EXCEPTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EOF_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 598
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "ZIP_DATA_FORMAT_EXCEPTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ZIP_DATA_FORMAT_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 599
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "RESOURCE_NOT_FOUND"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RESOURCE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 600
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "SOCKET_TIMEOUT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SOCKET_TIMEOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 601
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "FILE_NOT_FOUND"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->FILE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 602
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "BAD_ACCESS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BAD_ACCESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 603
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "UNSUPPORTED_UPLOADED_CONTENT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->UNSUPPORTED_UPLOADED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 604
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v1, "OTHER"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OTHER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 586
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->CONCURRENT_ACCESS_DENIED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->NO_CONTENT_VERSION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ROOT_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE_LIMIT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RENTAL_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BLOCKED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SESSION_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_UNAVAILABLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_INCONSISTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EOF_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ZIP_DATA_FORMAT_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RESOURCE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SOCKET_TIMEOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->FILE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BAD_ACCESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->UNSUPPORTED_UPLOADED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OTHER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 586
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 586
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .locals 1

    .prologue
    .line 586
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    return-object v0
.end method
