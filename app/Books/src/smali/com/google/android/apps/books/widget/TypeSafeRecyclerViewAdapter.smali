.class public abstract Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "TypeSafeRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;->onBindViewHolder(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;I)V
    .locals 0
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "viewHandle":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<*>;"
    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->onBind(I)V

    .line 57
    return-void
.end method

.method public bridge synthetic onViewAttachedToWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;->onViewAttachedToWindow(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V

    return-void
.end method

.method public onViewAttachedToWindow(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "holder":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<*>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->onViewAttachedToWindow()V

    .line 67
    return-void
.end method

.method public bridge synthetic onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;->onViewDetachedFromWindow(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V

    return-void
.end method

.method public onViewDetachedFromWindow(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "holder":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<*>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->onViewDetachedFromWindow()V

    .line 72
    return-void
.end method

.method public bridge synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;->onViewRecycled(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V

    return-void
.end method

.method public onViewRecycled(Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "holder":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<*>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->onViewRecycled()V

    .line 62
    return-void
.end method
