.class Lcom/google/android/apps/books/app/ContentsView$6;
.super Ljava/lang/Object;
.source "ContentsView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ContentsView;->createTocView(Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ContentsView;

.field final synthetic val$chapterIndex:I

.field final synthetic val$list:Landroid/widget/ExpandableListView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/widget/ExpandableListView;I)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/google/android/apps/books/app/ContentsView$6;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$list:Landroid/widget/ExpandableListView;

    iput p3, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$chapterIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$list:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$list:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$chapterIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/app/ContentsView$6;->val$list:Landroid/widget/ExpandableListView;

    invoke-virtual {v3}, Landroid/widget/ExpandableListView;->getChildCount()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 355
    const/4 v0, 0x1

    return v0
.end method
