.class Lcom/google/android/apps/books/app/ContentsView$7;
.super Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ContentsView;->createNotesView(Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ContentsView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p4, "x2"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    .param p5, "x3"    # Ljava/lang/String;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/apps/books/app/ContentsView$7;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getViewForAnnotation(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/books/annotations/Annotation;)Landroid/view/View;
    .locals 12
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    const/4 v11, 0x0

    .line 414
    if-nez p2, :cond_0

    .line 415
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    const v10, 0x7f04006e

    invoke-virtual {v9, v10, p1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 419
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/app/ContentsView$7;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # getter for: Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ContentsView;->access$500(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Arguments;

    move-result-object v9

    iget-object v9, v9, Lcom/google/android/apps/books/app/ContentsView$Arguments;->readerSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    iget-object v9, v9, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-static {v9}, Lcom/google/android/apps/books/util/ReaderUtils;->shouldUseDarkTheme(Ljava/lang/String;)Z

    move-result v3

    .line 421
    .local v3, "isNightTheme":Z
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v6

    .line 422
    .local v6, "servingColor":I
    sget-object v9, Lcom/google/android/apps/books/annotations/AnnotationUtils;->DRAWING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    invoke-interface {v9, v6}, Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;->getClosestMatch(I)I

    move-result v1

    .line 424
    .local v1, "drawingColor":I
    if-eqz v3, :cond_1

    const v9, 0x3f333333    # 0.7f

    :goto_0
    invoke-static {v1, v9}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v5

    .line 428
    .local v5, "quoteColor":I
    const v9, 0x7f0e0146

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 429
    .local v0, "colorBar":Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 431
    const v9, 0x7f0e0147

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 433
    .local v2, "highlighted":Landroid/widget/TextView;
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    const v9, 0x7f0e0148

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 437
    .local v7, "startPageTitle":Landroid/widget/TextView;
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/app/ContentsView$7;->getPageTitle(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    const v9, 0x7f0e0149

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 441
    .local v4, "noteText":Landroid/widget/TextView;
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 443
    if-eqz v3, :cond_2

    const v9, 0x3dcccccd    # 0.1f

    :goto_1
    invoke-static {v1, v9}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v8

    .line 446
    .local v8, "typedColor":I
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 447
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 454
    .end local v8    # "typedColor":I
    :goto_2
    return-object p2

    .line 424
    .end local v0    # "colorBar":Landroid/view/View;
    .end local v2    # "highlighted":Landroid/widget/TextView;
    .end local v4    # "noteText":Landroid/widget/TextView;
    .end local v5    # "quoteColor":I
    .end local v7    # "startPageTitle":Landroid/widget/TextView;
    :cond_1
    const/high16 v9, 0x3f800000    # 1.0f

    goto :goto_0

    .line 443
    .restart local v0    # "colorBar":Landroid/view/View;
    .restart local v2    # "highlighted":Landroid/widget/TextView;
    .restart local v4    # "noteText":Landroid/widget/TextView;
    .restart local v5    # "quoteColor":I
    .restart local v7    # "startPageTitle":Landroid/widget/TextView;
    :cond_2
    const v9, 0x3feccccd    # 1.85f

    goto :goto_1

    .line 450
    :cond_3
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
