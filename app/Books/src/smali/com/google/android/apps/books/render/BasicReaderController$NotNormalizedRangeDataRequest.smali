.class Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotNormalizedRangeDataRequest"
.end annotation


# instance fields
.field final mEndInPage:I

.field final mPageIndex:I

.field final mStartInPage:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IIIIIIZ)V
    .locals 7
    .param p2, "passageIndex"    # I
    .param p3, "pageIndex"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I
    .param p6, "externalRequestId"    # I
    .param p7, "taskRequestId"    # I
    .param p8, "reposted"    # Z

    .prologue
    .line 1201
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1203
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p6

    move v5, p7

    move v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IZIIZ)V

    .line 1204
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mPageIndex:I

    .line 1205
    iput p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mStartInPage:I

    .line 1206
    iput p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mEndInPage:I

    .line 1207
    return-void
.end method


# virtual methods
.method protected createRepostRequest()Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
    .locals 9

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$300(Lcom/google/android/apps/books/render/BasicReaderController;)I

    move-result v7

    .line 1223
    .local v7, "newRequestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mPageIndex:I

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mStartInPage:I

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mEndInPage:I

    iget v6, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mExternalRequestId:I

    const/4 v8, 0x1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIIIIIZ)V

    return-object v0
.end method

.method protected executeReaderCall()V
    .locals 6

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mTaskRequestId:I

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mPageIndex:I

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mStartInPage:I

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;->mEndInPage:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/TextReader;->loadNotNormalizedRangeData(IIIII)V

    .line 1213
    return-void
.end method
