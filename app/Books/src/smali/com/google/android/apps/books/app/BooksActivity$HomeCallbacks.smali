.class Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;
.super Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;
.source "BooksActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HomeFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomeCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/HomeFragment$Callbacks;",
        ">.FragmentCallbacks;",
        "Lcom/google/android/apps/books/app/HomeFragment$Callbacks;"
    }
.end annotation


# instance fields
.field private mTransitionView:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/apps/books/app/BooksActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/BooksActivity;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->mTransitionView:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/BooksActivity;Lcom/google/android/apps/books/app/BooksActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/BooksActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/BooksActivity$1;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;-><init>(Lcom/google/android/apps/books/app/BooksActivity;)V

    return-void
.end method

.method private maybeEnableTransitionAnimation(Landroid/view/View;)Landroid/support/v4/app/ActivityOptionsCompat;
    .locals 27
    .param p1, "coverView"    # Landroid/view/View;

    .prologue
    .line 103
    const/4 v5, 0x0

    .line 104
    .local v5, "animation":Landroid/support/v4/app/ActivityOptionsCompat;
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v25

    if-eqz v25, :cond_1

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move/from16 v25, v0

    if-eqz v25, :cond_1

    move-object/from16 v14, p1

    .line 106
    check-cast v14, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 107
    .local v14, "playCardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getImageView()Landroid/widget/ImageView;

    move-result-object v10

    .line 109
    .local v10, "imageView":Landroid/widget/ImageView;
    if-eqz v10, :cond_1

    .line 110
    invoke-virtual {v10}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v25

    check-cast v25, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v18

    .line 113
    .local v18, "thumbnailBitmap":Landroid/graphics/Bitmap;
    if-nez v18, :cond_0

    .line 114
    const/16 v25, 0x0

    .line 185
    .end local v10    # "imageView":Landroid/widget/ImageView;
    .end local v14    # "playCardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v18    # "thumbnailBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v25

    .line 117
    .restart local v10    # "imageView":Landroid/widget/ImageView;
    .restart local v14    # "playCardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .restart local v18    # "thumbnailBitmap":Landroid/graphics/Bitmap;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/books/app/BooksActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 118
    .local v6, "display":Landroid/view/Display;
    new-instance v16, Landroid/graphics/Point;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Point;-><init>()V

    .line 119
    .local v16, "size":Landroid/graphics/Point;
    move-object/from16 v0, v16

    invoke-static {v6, v0}, Lcom/google/android/apps/books/util/ReaderUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 121
    move-object/from16 v0, v16

    iget v4, v0, Landroid/graphics/Point;->x:I

    .line 122
    .local v4, "activityWidth":I
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/Point;->y:I

    .line 123
    .local v3, "activityHeight":I
    int-to-float v0, v4

    move/from16 v25, v0

    int-to-float v0, v3

    move/from16 v26, v0

    div-float v2, v25, v26

    .line 125
    .local v2, "activityAspectRatio":F
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    .line 126
    .local v19, "thumbnailHeight":I
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    .line 127
    .local v21, "thumbnailWidth":I
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v17, v25, v26

    .line 130
    .local v17, "thumbnailAspectRatio":F
    invoke-virtual {v10}, Landroid/widget/ImageView;->getHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v23, v0

    .line 131
    .local v23, "viewHeight":F
    invoke-virtual {v10}, Landroid/widget/ImageView;->getWidth()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v24, v0

    .line 133
    .local v24, "viewWidth":F
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v25, v23, v25

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v26, v24, v26

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->min(FF)F

    move-result v15

    .line 142
    .local v15, "scaleFactor":F
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v20, v25, v15

    .line 143
    .local v20, "thumbnailHeightScaled":F
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v22, v25, v15

    .line 147
    .local v22, "thumbnailWidthScaled":F
    cmpl-float v25, v17, v2

    if-lez v25, :cond_2

    .line 149
    move/from16 v0, v22

    float-to-int v13, v0

    .line 150
    .local v13, "newWidth":I
    div-float v25, v22, v2

    move/from16 v0, v25

    float-to-int v12, v0

    .line 157
    .local v12, "newHeight":I
    :goto_1
    int-to-float v0, v12

    move/from16 v25, v0

    sub-float v25, v25, v23

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    div-int/lit8 v9, v25, 0x2

    .line 158
    .local v9, "extraOnTop":I
    int-to-float v0, v13

    move/from16 v25, v0

    sub-float v25, v25, v24

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    div-int/lit8 v8, v25, 0x2

    .line 164
    .local v8, "extraOnSide":I
    :try_start_0
    sget-object v25, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v25

    invoke-static {v13, v12, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 173
    .local v11, "newBitmap":Landroid/graphics/Bitmap;
    neg-int v0, v8

    move/from16 v25, v0

    neg-int v0, v9

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v10, v11, v0, v1}, Landroid/support/v4/app/ActivityOptionsCompat;->makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v5

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->setTransitionBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "activityAspectRatio":F
    .end local v3    # "activityHeight":I
    .end local v4    # "activityWidth":I
    .end local v6    # "display":Landroid/view/Display;
    .end local v8    # "extraOnSide":I
    .end local v9    # "extraOnTop":I
    .end local v10    # "imageView":Landroid/widget/ImageView;
    .end local v11    # "newBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "newHeight":I
    .end local v13    # "newWidth":I
    .end local v14    # "playCardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v15    # "scaleFactor":F
    .end local v16    # "size":Landroid/graphics/Point;
    .end local v17    # "thumbnailAspectRatio":F
    .end local v18    # "thumbnailBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "thumbnailHeight":I
    .end local v20    # "thumbnailHeightScaled":F
    .end local v21    # "thumbnailWidth":I
    .end local v22    # "thumbnailWidthScaled":F
    .end local v23    # "viewHeight":F
    .end local v24    # "viewWidth":F
    :cond_1
    :goto_2
    move-object/from16 v25, v5

    .line 185
    goto/16 :goto_0

    .line 153
    .restart local v2    # "activityAspectRatio":F
    .restart local v3    # "activityHeight":I
    .restart local v4    # "activityWidth":I
    .restart local v6    # "display":Landroid/view/Display;
    .restart local v10    # "imageView":Landroid/widget/ImageView;
    .restart local v14    # "playCardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .restart local v15    # "scaleFactor":F
    .restart local v16    # "size":Landroid/graphics/Point;
    .restart local v17    # "thumbnailAspectRatio":F
    .restart local v18    # "thumbnailBitmap":Landroid/graphics/Bitmap;
    .restart local v19    # "thumbnailHeight":I
    .restart local v20    # "thumbnailHeightScaled":F
    .restart local v21    # "thumbnailWidth":I
    .restart local v22    # "thumbnailWidthScaled":F
    .restart local v23    # "viewHeight":F
    .restart local v24    # "viewWidth":F
    :cond_2
    move/from16 v0, v20

    float-to-int v12, v0

    .line 154
    .restart local v12    # "newHeight":I
    mul-float v25, v20, v2

    move/from16 v0, v25

    float-to-int v13, v0

    .restart local v13    # "newWidth":I
    goto :goto_1

    .line 178
    .restart local v8    # "extraOnSide":I
    .restart local v9    # "extraOnTop":I
    :catch_0
    move-exception v7

    .line 179
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    const-string v25, "BooksActivity"

    const/16 v26, 0x5

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 180
    const-string v25, "BooksActivity"

    const-string v26, "Couldn\'t allocate transition bitmap"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public consumeTransitionView()Landroid/view/View;
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->mTransitionView:Landroid/view/View;

    .line 241
    .local v0, "transitionView":Landroid/view/View;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->mTransitionView:Landroid/view/View;

    .line 242
    return-object v0
.end method

.method public getLibrarySortOrderFromPrefs()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 2

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getVolumeSortOrder()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v0

    return-object v0
.end method

.method public getSystemUi()Lcom/google/android/ublib/view/SystemUi;
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "coverView"    # Landroid/view/View;

    .prologue
    .line 76
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BooksActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    # invokes: Lcom/google/android/apps/books/app/BooksActivity;->creatingShortcut()Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksActivity;->access$000(Lcom/google/android/apps/books/app/BooksActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    # invokes: Lcom/google/android/apps/books/app/BooksActivity;->createShortcut(Ljava/lang/String;)V
    invoke-static {v2, p1}, Lcom/google/android/apps/books/app/BooksActivity;->access$100(Lcom/google/android/apps/books/app/BooksActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/BooksActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, p1, v3}, Lcom/google/android/apps/books/app/BooksApplication;->buildInternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 85
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/app/BookOpeningFlags;->setExtras(Landroid/content/Intent;)V

    .line 87
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->maybeEnableTransitionAnimation(Landroid/view/View;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v0

    .line 89
    .local v0, "animation":Landroid/support/v4/app/ActivityOptionsCompat;
    if-eqz v0, :cond_2

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v2, v1, v3}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    .line 93
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/app/BooksActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onSelectedLibrarySortOrder(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 5
    .param p1, "librarySortOrder"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 203
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    const-string v4, "HomeActivity.volumes"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/BooksActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HomeFragment;

    .line 204
    .local v0, "homeFragment":Lcom/google/android/apps/books/app/HomeFragment;
    if-nez v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 207
    .local v2, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getLibrarySortOrder()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v1

    .line 209
    .local v1, "oldLibrarySortOrder":Lcom/google/android/apps/books/app/LibraryComparator;
    if-eq p1, v1, :cond_1

    .line 210
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/LibraryComparator;->getAnalyticsAction()Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 211
    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setVolumeSortOrder(Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 212
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->onLibrarySortOrderChanged()V

    goto :goto_0

    .line 215
    :cond_1
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_NO_CHANGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    goto :goto_0
.end method

.method public setTransitionView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->mTransitionView:Landroid/view/View;

    .line 236
    return-void
.end method

.method public showLibrarySortMenu(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 6
    .param p1, "currentSort"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 222
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/BooksActivity;->isActivityDestroyed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 231
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/BooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 225
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 226
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-static {p1}, Lcom/google/android/apps/books/app/SortFragment$Arguments;->create(Lcom/google/android/apps/books/app/LibraryComparator;)Landroid/os/Bundle;

    move-result-object v0

    .line 228
    .local v0, "args":Landroid/os/Bundle;
    const-class v3, Lcom/google/android/apps/books/app/SortFragment;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v2, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 230
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "helpContext"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    .line 253
    invoke-static {p3}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/apps/books/app/HelpUtils;->startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;Lcom/google/android/apps/books/util/Config;)V

    .line 255
    return-void
.end method

.method public startUnpinProcess(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    const-string v2, "HomeActivity.volumes"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/BooksActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HomeFragment;

    .line 191
    .local v0, "homeFragment":Lcom/google/android/apps/books/app/HomeFragment;
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->startUnpinProcess(Ljava/lang/String;)V

    .line 194
    :cond_0
    return-void
.end method
