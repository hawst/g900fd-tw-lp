.class Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;
.super Ljava/lang/Object;
.source "TableOfContentsFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ContentsView$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/TableOfContentsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/TableOfContentsFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/TableOfContentsFragment;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;->this$0:Lcom/google/android/apps/books/app/TableOfContentsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/TableOfContentsFragment;Lcom/google/android/apps/books/app/TableOfContentsFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/TableOfContentsFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/TableOfContentsFragment$1;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;-><init>(Lcom/google/android/apps/books/app/TableOfContentsFragment;)V

    return-void
.end method

.method private setResult(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "action"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;->this$0:Lcom/google/android/apps/books/app/TableOfContentsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/TableOfContentsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/TableOfContentsActivity;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->setResult(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    .line 82
    return-void
.end method


# virtual methods
.method public navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "action"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;->setResult(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    .line 73
    return-void
.end method

.method public onAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;->setResult(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    .line 78
    return-void
.end method
