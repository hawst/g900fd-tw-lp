.class Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;
.super Lcom/google/android/apps/books/app/DismissOnBackgroundTouchDialog;
.source "ReaderSettingsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderSettingsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisplayOptionsDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderSettingsController;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "theme"    # I

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    .line 79
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/app/DismissOnBackgroundTouchDialog;-><init>(Landroid/content/Context;I)V

    .line 80
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0131

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const/4 v0, 0x1

    return v0
.end method
