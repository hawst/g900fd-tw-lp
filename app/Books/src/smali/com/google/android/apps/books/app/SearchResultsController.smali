.class public Lcom/google/android/apps/books/app/SearchResultsController;
.super Ljava/lang/Object;
.source "SearchResultsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SearchResultsController$4;,
        Lcom/google/android/apps/books/app/SearchResultsController$SearchState;,
        Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;,
        Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;,
        Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;
    }
.end annotation


# static fields
.field private static final NONE_FOUND_ROW:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;


# instance fields
.field private mCompletedResultsHeader:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private final mOnSearchItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mProgress:Landroid/view/View;

.field private mQuery:Ljava/lang/String;

.field private mQueryEmphasisColor:I

.field private mResultsHeader:Landroid/view/View;

.field private mSearchResultCallbacks:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;

.field private final mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

.field private mSearchState:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    const/4 v1, 0x1

    const v2, 0x7f04005f

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;-><init>(IILcom/google/android/apps/books/app/SearchResultsController$1;)V

    sput-object v0, Lcom/google/android/apps/books/app/SearchResultsController;->NONE_FOUND_ROW:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;
    .param p3, "queryEmphasisColor"    # I

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    new-instance v0, Lcom/google/android/apps/books/app/SearchResultsController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/SearchResultsController$1;-><init>(Lcom/google/android/apps/books/app/SearchResultsController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mOnSearchItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mQuery:Ljava/lang/String;

    .line 259
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchState;->NOT_YET_STARTED:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchState:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    .line 263
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mContext:Landroid/content/Context;

    .line 264
    iput-object p2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultCallbacks:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;

    .line 265
    iput p3, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mQueryEmphasisColor:I

    .line 266
    new-instance v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    .line 267
    return-void
.end method

.method static synthetic access$600()Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController;->NONE_FOUND_ROW:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/SearchResultsController;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultCallbacks:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;

    return-object v0
.end method

.method private populateHeaderView(ILjava/lang/String;Landroid/view/View;)V
    .locals 11
    .param p1, "numResults"    # I
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 346
    const v6, 0x1020014

    invoke-virtual {p3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 347
    .local v4, "text1":Landroid/widget/TextView;
    const v6, 0x1020015

    invoke-virtual {p3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 349
    .local v5, "text2":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 350
    .local v1, "res":Landroid/content/res/Resources;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</font></b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "htmlQuery":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 353
    const v6, 0x7f0f01f1

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v9

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, "resultString":Ljava/lang/String;
    :goto_0
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 359
    .local v3, "resultText":Landroid/text/Spanned;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    const v6, 0x7f0f01f0

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mQuery:Ljava/lang/String;

    aput-object v8, v7, v9

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 362
    new-instance v6, Lcom/google/android/apps/books/app/SearchResultsController$3;

    invoke-direct {v6, p0, p2}, Lcom/google/android/apps/books/app/SearchResultsController$3;-><init>(Lcom/google/android/apps/books/app/SearchResultsController;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    return-void

    .line 355
    .end local v2    # "resultString":Ljava/lang/String;
    .end local v3    # "resultText":Landroid/text/Spanned;
    :cond_0
    const v6, 0x7f11000e

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v0, v7, v10

    invoke-virtual {v1, v6, p1, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "resultString":Ljava/lang/String;
    goto :goto_0
.end method

.method private setSearchState(Lcom/google/android/apps/books/app/SearchResultsController$SearchState;)V
    .locals 0
    .param p1, "state"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchState:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    .line 342
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController;->showSearchState()V

    .line 343
    return-void
.end method

.method private showSearchState()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 302
    sget-object v1, Lcom/google/android/apps/books/app/SearchResultsController$4;->$SwitchMap$com$google$android$apps$books$app$SearchResultsController$SearchState:[I

    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchState:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 316
    :goto_0
    :pswitch_0
    return-void

    .line 306
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mProgress:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getSearchResultCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->access$800(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)I

    move-result v0

    .line 311
    .local v0, "count":I
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mProgress:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mQuery:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/SearchResultsController;->populateHeaderView(ILjava/lang/String;Landroid/view/View;)V

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addSearchResults(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 6
    .param p1, "chapterHeading"    # Ljava/lang/String;
    .param p3, "done"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p2, "results":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SearchResult;>;"
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->setChapterHeading(Ljava/lang/String;)V

    .line 326
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/SearchResult;

    .line 327
    .local v1, "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->found(Lcom/google/android/apps/books/model/SearchResult;)V

    goto :goto_0

    .line 331
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    :cond_0
    if-eqz p3, :cond_1

    .line 332
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SEARCH_COMPLETE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    iget-object v3, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getCount()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 335
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->onLoadComplete()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->access$900(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)V

    .line 336
    sget-object v2, Lcom/google/android/apps/books/app/SearchResultsController$SearchState;->COMPLETE:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/SearchResultsController;->setSearchState(Lcom/google/android/apps/books/app/SearchResultsController$SearchState;)V

    .line 338
    :cond_1
    return-void
.end method

.method public beginSearch(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mQuery:Ljava/lang/String;

    .line 375
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchState;->IN_PROGRESS:Lcom/google/android/apps/books/app/SearchResultsController$SearchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SearchResultsController;->setSearchState(Lcom/google/android/apps/books/app/SearchResultsController$SearchState;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->onLoadStart()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->access$1000(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)V

    .line 377
    return-void
.end method

.method public clearSearchResults()V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->clear()V

    .line 381
    return-void
.end method

.method public onAttachView(Landroid/view/View;)V
    .locals 4
    .param p1, "resultsLayout"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 273
    new-instance v1, Lcom/google/android/apps/books/app/SearchResultsController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/SearchResultsController$2;-><init>(Lcom/google/android/apps/books/app/SearchResultsController;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 279
    const v1, 0x7f0e01c3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 281
    .local v0, "resultsList":Landroid/widget/ListView;
    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04005d

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mResultsHeader:Landroid/view/View;

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mResultsHeader:Landroid/view/View;

    const v2, 0x7f0e0136

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mResultsHeader:Landroid/view/View;

    const v2, 0x7f0e0135

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mProgress:Landroid/view/View;

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mProgress:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mResultsHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 291
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mOnSearchItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 292
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController;->showSearchState()V

    .line 293
    return-void
.end method

.method public onDetachView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 296
    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mProgress:Landroid/view/View;

    .line 297
    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mCompletedResultsHeader:Landroid/view/View;

    .line 298
    iput-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mResultsHeader:Landroid/view/View;

    .line 299
    return-void
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 385
    return-void
.end method

.method public setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultsAdapter:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 389
    return-void
.end method
