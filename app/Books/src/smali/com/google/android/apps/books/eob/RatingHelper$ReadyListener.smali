.class public abstract Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;
.super Ljava/lang/Object;
.source "RatingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/eob/RatingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReadyListener"
.end annotation


# instance fields
.field public requestIsInProcess:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;->requestIsInProcess:Z

    return-void
.end method


# virtual methods
.method public abstract onRatingInfoAvailable(Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V
.end method
