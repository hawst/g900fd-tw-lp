.class Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;
.super Ljava/lang/Object;
.source "BaseVolumeCoverSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InputStreamSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreopenedInputStreamSource"
.end annotation


# instance fields
.field private mPreopened:Ljava/io/InputStream;

.field private final mStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 0
    .param p2, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p3, "volumeId"    # Ljava/lang/String;
    .param p4, "preopened"    # Ljava/io/InputStream;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p2, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 170
    iput-object p3, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mVolumeId:Ljava/lang/String;

    .line 171
    iput-object p4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mPreopened:Ljava/io/InputStream;

    .line 172
    return-void
.end method


# virtual methods
.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 2

    .prologue
    .line 187
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected request for ParcelFileDescriptor for volume cover"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mPreopened:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mPreopened:Ljava/io/InputStream;

    .line 178
    .local v0, "result":Ljava/io/InputStream;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mPreopened:Ljava/io/InputStream;

    .line 181
    .end local v0    # "result":Ljava/io/InputStream;
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mStore:Lcom/google/android/apps/books/model/BooksDataStore;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$PreopenedInputStreamSource;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method
