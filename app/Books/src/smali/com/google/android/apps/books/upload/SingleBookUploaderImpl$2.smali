.class Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;
.super Ljava/lang/Object;
.source "SingleBookUploaderImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->startUpload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(I)V
    .locals 4
    .param p1, "totalBytesRead"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getFileSize()I

    move-result v0

    if-lez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$700(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    mul-int/lit8 v2, p1, 0x64

    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v3}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getFileSize()I

    move-result v3

    div-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadProgress(Ljava/lang/String;I)V

    .line 142
    :cond_0
    return-void
.end method
