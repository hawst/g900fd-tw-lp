.class Lcom/google/android/apps/books/app/HomeMenuImpl$1;
.super Ljava/lang/Object;
.source "HomeMenuImpl.java"

# interfaces
.implements Landroid/widget/FilterQueryProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeMenuImpl;->configureSearchView(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeMenuImpl;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 18
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 124
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    const/4 v11, 0x0

    .line 153
    :cond_0
    :goto_0
    return-object v11

    .line 126
    :cond_1
    sget-object v15, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SUBMITTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;Ljava/lang/Long;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/HomeMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    invoke-static {v15}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    move-result-object v15

    invoke-interface {v15}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getVolumes()Ljava/util/List;

    move-result-object v14

    .line 130
    .local v14, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    if-nez v14, :cond_2

    const/4 v11, 0x0

    goto :goto_0

    .line 132
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    .line 136
    .local v10, "query":Ljava/lang/String;
    new-instance v11, Landroid/database/MatrixCursor;

    sget-object v15, Lcom/google/android/apps/books/app/HomeMenuImpl$SearchResultColumns;->PROJECTION:[Ljava/lang/String;

    const/16 v16, 0xa

    move/from16 v0, v16

    invoke-direct {v11, v15, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 137
    .local v11, "result":Landroid/database/MatrixCursor;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/HomeMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    invoke-static {v15}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    move-result-object v15

    invoke-interface {v15}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getAccount()Landroid/accounts/Account;

    move-result-object v15

    iget-object v2, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 139
    .local v2, "accountName":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 140
    .local v4, "fakeId":J
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/model/VolumeData;

    .line 141
    .local v13, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v8

    .line 142
    .local v8, "language":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_5

    const/4 v9, 0x0

    .line 145
    .local v9, "locale":Ljava/util/Locale;
    :goto_2
    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-static {v10, v15, v9}, Lcom/google/android/apps/books/util/BooksTextUtils;->matchesWordPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)Z

    move-result v15

    if-nez v15, :cond_4

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v15

    invoke-static {v10, v15, v9}, Lcom/google/android/apps/books/util/BooksTextUtils;->matchesWordPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 147
    :cond_4
    const/4 v15, 0x5

    new-array v12, v15, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-wide/16 v16, 0x1

    add-long v6, v4, v16

    .end local v4    # "fakeId":J
    .local v6, "fakeId":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v12, v15

    const/4 v15, 0x1

    aput-object v2, v12, v15

    const/4 v15, 0x2

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v12, v15

    const/4 v15, 0x3

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v12, v15

    const/4 v15, 0x4

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v12, v15

    .line 149
    .local v12, "row":[Ljava/lang/Object;
    invoke-virtual {v11, v12}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-wide v4, v6

    .end local v6    # "fakeId":J
    .restart local v4    # "fakeId":J
    goto :goto_1

    .line 142
    .end local v9    # "locale":Ljava/util/Locale;
    .end local v12    # "row":[Ljava/lang/Object;
    :cond_5
    invoke-static {v8}, Lcom/google/android/apps/books/util/LanguageUtil;->stringToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v9

    goto :goto_2
.end method
