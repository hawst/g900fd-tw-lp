.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;
.super Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;
.source "ReadAlongDataSourceFromStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaOverlayItemRetrieval"
.end annotation


# instance fields
.field private mFoundElements:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mIdSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequest:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

.field private final mRequestId:I

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;I)V
    .locals 2
    .param p2, "request"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    .param p3, "requestId"    # I

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .line 284
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)V

    .line 278
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mIdSet:Ljava/util/Set;

    .line 280
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mFoundElements:Ljava/util/Map;

    .line 285
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    .line 286
    iput p3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequestId:I

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mIdSet:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->getElementIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 288
    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mIdSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mFoundElements:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    .prologue
    .line 273
    iget v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequestId:I

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    return-object v0
.end method


# virtual methods
.method protected getXmlHandler()Lcom/google/android/apps/books/model/ContentXmlHandler;
    .locals 1

    .prologue
    .line 292
    new-instance v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)V

    return-object v0
.end method

.method protected onFailedParse()V
    .locals 3

    .prologue
    .line 327
    const-string v0, "MoSourceFromStorage"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    const-string v0, "MoSourceFromStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to retrieve MO items for passage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->getPassage()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :cond_0
    return-void
.end method

.method protected onSuccessfulParse()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mUiExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$900(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 322
    return-void
.end method
