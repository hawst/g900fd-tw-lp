.class Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;
.super Lcom/google/android/apps/books/app/FetchReadingPositionTask;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyFetchReadingPositionTask"
.end annotation


# instance fields
.field final mFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/books/app/ReaderFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 1
    .param p1, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "fragment"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 7302
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;
    invoke-static {p3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5600(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/app/FetchReadingPositionTask;-><init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/lang/String;Landroid/accounts/Account;)V

    .line 7303
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;->mFragment:Ljava/lang/ref/WeakReference;

    .line 7304
    return-void
.end method


# virtual methods
.method protected onFinished(Lcom/google/android/apps/books/model/VolumeData;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 7308
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;->mFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 7309
    .local v0, "fragment":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 7311
    new-instance v1, Lcom/google/android/apps/books/common/Position;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeHandleServerPosition(Lcom/google/android/apps/books/common/Position;J)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/common/Position;J)V

    .line 7313
    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingPositionFetchInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15202(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 7315
    :cond_0
    return-void
.end method
