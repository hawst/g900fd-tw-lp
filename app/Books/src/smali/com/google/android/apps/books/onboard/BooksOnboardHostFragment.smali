.class public Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;
.super Lcom/google/android/play/onboard/OnboardHostFragment;
.source "BooksOnboardHostFragment.java"


# instance fields
.field private mPageList:Lcom/google/android/libraries/bind/data/DataList;

.field private mSelectedGenres:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;-><init>()V

    return-void
.end method

.method private addTutorialPages(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getPageCount()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f040075

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setViewResId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getAppColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    const v1, 0x7f0f0202

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    const v1, 0x7f0f0205

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    const v1, 0x7f02011a

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->build()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    return-void
.end method

.method private static getColumnsPerRow(Landroid/content/res/Resources;II)I
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "widthBetweenOuterGutters"    # I
    .param p2, "minWidthResId"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 96
    .local v0, "minWidth":I
    div-int v1, p1, v0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public static getColumnsPerRow(Landroid/content/res/Resources;Landroid/view/WindowManager;I)I
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "windowManager"    # Landroid/view/WindowManager;
    .param p2, "minWidthResId"    # I

    .prologue
    .line 84
    invoke-static {p0, p1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getWidthBetweenOuterGutters(Landroid/content/res/Resources;Landroid/view/WindowManager;)I

    move-result v0

    invoke-static {p0, v0, p2}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getColumnsPerRow(Landroid/content/res/Resources;II)I

    move-result v0

    return v0
.end method

.method private static getScreenWidth(Landroid/view/WindowManager;)I
    .locals 3
    .param p0, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    .line 107
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 108
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 109
    .local v1, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 110
    iget v2, v1, Landroid/graphics/Point;->x:I

    return v2
.end method

.method private static getWidthBetweenOuterGutters(Landroid/content/res/Resources;Landroid/view/WindowManager;)I
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    .line 101
    const v1, 0x7f0900f3

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 103
    .local v0, "outerGutterPadding":I
    invoke-static {p1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getScreenWidth(Landroid/view/WindowManager;)I

    move-result v1

    mul-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    return v1
.end method

.method private selectedSamples()Z
    .locals 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    const-string v1, "onboard_samples_selected"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected exitOnboardFlow()V
    .locals 4

    .prologue
    .line 230
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v3}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 231
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v3, v2}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v1

    .line 232
    .local v1, "pageView":Landroid/view/View;
    instance-of v3, v1, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v3, :cond_0

    .line 233
    check-cast v1, Lcom/google/android/play/onboard/OnboardPage;

    .end local v1    # "pageView":Landroid/view/View;
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/google/android/play/onboard/OnboardPage;->onExitPage(Z)V

    .line 230
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getHostActivity()Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;

    move-result-object v0

    .line 238
    .local v0, "hostActivity":Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;
    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->onFinishedOnboardFlow()V

    .line 241
    :cond_2
    return-void
.end method

.method public finishOnboardFlow()V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->finishOnboardFlow()V

    .line 220
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->selectedSamples()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    .line 227
    :goto_0
    return-void

    .line 225
    :cond_0
    const v0, 0x7f0f020e

    sget-object v1, Lcom/google/android/play/onboard/InterstitialOverlay;->DEFAULT_ACCENT_COLORS_RES_IDS:[I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->showPulsatingDotOverlay(I[I)V

    goto :goto_0
.end method

.method protected getHostActivity()Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getShowFullSequence()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getPageList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 21

    .prologue
    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v3, :cond_0

    .line 116
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 160
    :goto_0
    return-object v3

    .line 118
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v16, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getSequenceType()I

    move-result v3

    if-nez v3, :cond_1

    .line 120
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->addTutorialPages(Ljava/util/List;)V

    .line 122
    :cond_1
    new-instance v3, Lcom/google/android/libraries/bind/data/DataList;

    sget v4, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    move-object/from16 v0, v16

    invoke-direct {v3, v4, v0}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    .line 125
    .local v10, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 126
    .local v17, "res":Landroid/content/res/Resources;
    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getWidthBetweenOuterGutters(Landroid/content/res/Resources;Landroid/view/WindowManager;)I

    move-result v20

    .line 128
    .local v20, "widthBetweenOuterGutters":I
    const v3, 0x7f0900fb

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getColumnsPerRow(Landroid/content/res/Resources;II)I

    move-result v13

    .line 130
    .local v13, "columns":I
    div-int v18, v20, v13

    .line 132
    .local v18, "roughColumnWidth":I
    const v3, 0x7f0901ba

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 138
    .local v14, "coverHorizontalPadding":I
    mul-int/lit8 v3, v14, 0x2

    sub-int v19, v18, v3

    .line 139
    .local v19, "roughCoverWidth":I
    move/from16 v0, v19

    int-to-float v3, v0

    const v4, 0x3f2b851f    # 0.67f

    mul-float/2addr v3, v4

    const/high16 v4, 0x437a0000    # 250.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    float-to-int v15, v3

    .line 143
    .local v15, "coverWidthToRequest":I
    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v11

    .line 146
    .local v11, "booksApplication":Lcom/google/android/apps/books/app/BooksApplication;
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v10}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    .line 147
    .local v9, "account":Landroid/accounts/Account;
    new-instance v2, Lcom/google/android/apps/books/app/OnboardingController;

    invoke-static {v10, v9}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v4

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/ublib/util/ImageConstraints;->fixedWidth(Ljava/lang/Integer;)Lcom/google/android/ublib/util/ImageConstraints;

    move-result-object v7

    invoke-virtual {v11, v9}, Lcom/google/android/apps/books/app/BooksApplication;->getReadNowCoverStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/RemoteFileCache;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/app/OnboardingController;-><init>(Lcom/google/android/apps/books/net/BooksServer;Lcom/google/android/apps/books/common/ImageManager;Ljava/util/concurrent/ExecutorService;Lcom/google/android/apps/books/util/HandlerExecutor;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/model/RemoteFileCache;)V

    .line 155
    .local v2, "onboardingController":Lcom/google/android/apps/books/app/OnboardingController;
    invoke-virtual {v11, v9}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v12

    .line 157
    .local v12, "booksDataController":Lcom/google/android/apps/books/data/BooksDataController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-static {v3, v2, v12}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/apps/books/data/BooksDataController;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    goto/16 :goto_0
.end method

.method public getSelectedGenres()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mSelectedGenres:Ljava/util/Set;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mSelectedGenres:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getSequenceType()I
    .locals 4

    .prologue
    .line 189
    const/4 v1, 0x0

    .line 190
    .local v1, "sequenceType":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 191
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 192
    const-string v2, "arg_sequenceType"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 194
    :cond_0
    return v1
.end method

.method public getShowFullSequence()Z
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getSequenceType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getCurrentPageIndex()I

    move-result v1

    if-nez v1, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getPageCount()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_TUTORIAL_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 262
    .local v0, "onboardingAction":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V

    .line 265
    .end local v0    # "onboardingAction":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
    :cond_0
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->onBackPressed()Z

    move-result v1

    return v1

    .line 258
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_GENRE_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 270
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->onDestroy()V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 272
    iput-object v1, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mPageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 273
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040074

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->setBackgroundView(Landroid/view/View;)V

    .line 178
    return-void
.end method

.method public setSelectedGenres(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "selectedGenres":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->mSelectedGenres:Ljava/util/Set;

    .line 245
    return-void
.end method

.method public setSequenceType(I)V
    .locals 2
    .param p1, "sequenceType"    # I

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getOrCreateArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 204
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "arg_sequenceType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->setArguments(Landroid/os/Bundle;)V

    .line 206
    return-void
.end method
