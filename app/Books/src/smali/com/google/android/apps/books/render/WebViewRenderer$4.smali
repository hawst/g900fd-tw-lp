.class Lcom/google/android/apps/books/render/WebViewRenderer$4;
.super Ljava/lang/Object;
.source "WebViewRenderer.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$4;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Exception;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$4;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->dispatchRenderError(Ljava/lang/Exception;)V

    .line 313
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 309
    check-cast p1, Ljava/lang/Exception;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer$4;->take(Ljava/lang/Exception;)V

    return-void
.end method
