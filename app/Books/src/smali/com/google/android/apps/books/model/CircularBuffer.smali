.class public Lcom/google/android/apps/books/model/CircularBuffer;
.super Ljava/lang/Object;
.source "CircularBuffer.java"


# instance fields
.field private final mBuffer:[C

.field private mPosition:I

.field private mWrapped:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "numChars"    # I

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean v0, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mWrapped:Z

    .line 13
    iput v0, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    .line 21
    new-array v0, p1, [C

    iput-object v0, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    .line 22
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;)V
    .locals 1
    .param p1, "added"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/model/CircularBuffer;->add(Ljava/lang/String;I)V

    .line 48
    return-void
.end method

.method public add(Ljava/lang/String;I)V
    .locals 5
    .param p1, "added"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 31
    iget v1, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    add-int/2addr v1, p2

    iget-object v2, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 32
    iget-object v1, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    array-length v1, v1

    iget v2, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    sub-int v0, v1, v2

    .line 33
    .local v0, "addableLength":I
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    iget v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    invoke-static {v1, v4, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 34
    iput v4, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    .line 35
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mWrapped:Z

    .line 36
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/CircularBuffer;->add(Ljava/lang/String;)V

    .line 41
    .end local v0    # "addableLength":I
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    iget v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    invoke-static {v1, v4, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    iget v1, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    add-int/2addr v1, p2

    iput v1, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    goto :goto_0
.end method

.method public getLast(I)Ljava/lang/String;
    .locals 6
    .param p1, "numChars"    # I

    .prologue
    .line 68
    iget-object v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    array-length v3, v3

    if-le p1, v3, :cond_0

    .line 69
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Requested more characters than are stored."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 71
    :cond_0
    iget v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    if-lt v3, p1, :cond_2

    .line 72
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    iget v4, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    sub-int/2addr v4, p1

    invoke-direct {v0, v3, v4, p1}, Ljava/lang/String;-><init>([CII)V

    .line 81
    :cond_1
    :goto_0
    return-object v0

    .line 74
    :cond_2
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    invoke-direct {v0, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    .line 75
    .local v0, "charsAtBeginning":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mWrapped:Z

    if-eqz v3, :cond_1

    .line 78
    iget v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mPosition:I

    sub-int v2, p1, v3

    .line 79
    .local v2, "numCharsFromEnd":I
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    iget-object v4, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    array-length v4, v4

    sub-int/2addr v4, v2

    invoke-direct {v1, v3, v4, v2}, Ljava/lang/String;-><init>([CII)V

    .line 81
    .local v1, "charsFromEnd":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/model/CircularBuffer;->mBuffer:[C

    array-length v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/CircularBuffer;->getLast(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
