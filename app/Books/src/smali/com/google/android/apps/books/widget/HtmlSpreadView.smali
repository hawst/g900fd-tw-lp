.class public Lcom/google/android/apps/books/widget/HtmlSpreadView;
.super Lcom/google/android/apps/books/widget/BaseSpreadView;
.source "HtmlSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;,
        Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;,
        Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    }
.end annotation


# static fields
.field private static final MIRROR_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;


# instance fields
.field private final mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

.field private final mDisplayTwoPages:Z

.field private final mPageCellSize:Landroid/graphics/Point;

.field private final mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

.field final mSpreadContentRect:Landroid/graphics/Rect;

.field private final mTopContainer:Landroid/widget/FrameLayout;

.field private mUserScale:F

.field private final mUserZoom:Landroid/graphics/Matrix;

.field private final mWebViewMirror:Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->MIRROR_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZI)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayTwoPages"    # Z
    .param p3, "backgroundColor"    # I

    .prologue
    const/4 v2, 0x1

    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BaseSpreadView;-><init>()V

    .line 49
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserZoom:Landroid/graphics/Matrix;

    .line 50
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserScale:F

    .line 57
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;

    .line 65
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    .line 228
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    .line 230
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;

    .line 232
    new-instance v1, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v3

    invoke-direct {v1, p0, p1, p2, v3}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;-><init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    .line 235
    new-instance v1, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;-><init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mWebViewMirror:Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mWebViewMirror:Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    if-eqz p2, :cond_1

    const/4 v1, 0x2

    :goto_0
    new-array v1, v1, [Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    iput-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    .line 241
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 243
    .local v0, "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    invoke-direct {v4, p0, p1, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;-><init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    aput-object v4, v1, v3

    .line 244
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    new-instance v3, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    sget-object v4, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct {v3, p0, p1, v4}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;-><init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    aput-object v3, v1, v2

    .line 247
    :cond_0
    return-void

    .end local v0    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_1
    move v1, v2

    .line 240
    goto :goto_0

    .line 241
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/widget/HtmlSpreadView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserScale:F

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserZoom:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/HtmlSpreadView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->updateSpreadContentRect()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/HtmlSpreadView;I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/HtmlSpreadView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPagesCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/HtmlSpreadView;)[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    return-object v0
.end method

.method static synthetic access$800()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->MIRROR_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public static getMainSpreadDrawingCacheBytes(Landroid/graphics/Point;)I
    .locals 2
    .param p0, "size"    # Landroid/graphics/Point;

    .prologue
    .line 45
    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p0, Landroid/graphics/Point;->y:I

    mul-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/books/widget/HtmlSpreadView;->MIRROR_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v1}, Lcom/google/android/apps/books/util/BitmapUtils;->getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method private getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    .locals 2
    .param p1, "pageOffset"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private getPageIndex(I)I
    .locals 1
    .param p1, "pageOffset"    # I

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    .line 271
    add-int/lit8 p1, p1, 0x1

    .line 273
    .end local p1    # "pageOffset":I
    :cond_0
    return p1
.end method

.method private getPagesCount()I
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateSpreadContentRect()V
    .locals 9

    .prologue
    .line 250
    iget-object v4, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    .local v0, "arr$":[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 252
    .local v3, "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget-object v5, v3, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 254
    .end local v3    # "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setContentRect(IIII)V

    .line 256
    return-void
.end method


# virtual methods
.method public clearContent()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 279
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    .line 280
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->invalidatePageContent()V

    .line 283
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public invalidatePageContent()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mWebViewMirror:Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->invalidateMirror()V

    .line 310
    return-void
.end method

.method public onBookmarkChanged(I)V
    .locals 1
    .param p1, "screenOffset"    # I

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->onBookmarkChanged(I)V

    .line 305
    return-void
.end method

.method public scaleAndScroll(FFF)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "scrollX"    # F
    .param p3, "scrollY"    # F

    .prologue
    const/4 v1, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setScaleX(F)V

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setScaleY(F)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setPivotX(F)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setPivotY(F)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    neg-float v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setTranslationX(F)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    neg-float v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->setTranslationY(F)V

    .line 327
    iput p1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserScale:F

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserZoom:Landroid/graphics/Matrix;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/util/MathUtils;->setScaleAndScrollMatrix(Landroid/graphics/Matrix;FFF)V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->invalidatePageContent()V

    .line 331
    return-void
.end method

.method public setPageContent(ILcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 1
    .param p1, "pageOffset"    # I
    .param p2, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 293
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 294
    return-void
.end method

.method public setPageLoading(ILandroid/graphics/Point;)V
    .locals 1
    .param p1, "screenOffset"    # I
    .param p2, "size"    # Landroid/graphics/Point;

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->setLoading(Landroid/graphics/Point;)V

    .line 288
    return-void
.end method

.method public setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V
    .locals 1
    .param p1, "screenOffset"    # I
    .param p2, "pageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p4, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V

    .line 300
    return-void
.end method
