.class Lcom/google/android/apps/books/data/PageContentSubcontroller$4;
.super Ljava/lang/Object;
.source "PageContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$page:Lcom/google/android/apps/books/model/Page;

    iput-object p5, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iput-object p6, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$100(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$page:Lcom/google/android/apps/books/model/Page;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iget-object v5, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/LocalSessionKey;

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$200(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 197
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
