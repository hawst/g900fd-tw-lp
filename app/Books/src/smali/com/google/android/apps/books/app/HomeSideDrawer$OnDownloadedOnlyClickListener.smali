.class public Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeSideDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "OnDownloadedOnlyClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 266
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    invoke-static {v3}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BaseBooksActivity;

    .line 267
    .local v0, "activity":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<*>;"
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 271
    .local v2, "resources":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;
    invoke-static {v3}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$200(Lcom/google/android/apps/books/app/HomeSideDrawer;)Landroid/widget/TextSwitcher;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0107

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 273
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;
    invoke-static {v3}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$200(Lcom/google/android/apps/books/app/HomeSideDrawer;)Landroid/widget/TextSwitcher;

    move-result-object v3

    const v4, 0x7f0f01a2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 276
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 277
    .local v1, "handler":Landroid/os/Handler;
    new-instance v3, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 292
    return-void
.end method
