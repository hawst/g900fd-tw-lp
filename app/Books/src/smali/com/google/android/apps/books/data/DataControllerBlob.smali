.class public interface abstract Lcom/google/android/apps/books/data/DataControllerBlob;
.super Ljava/lang/Object;
.source "DataControllerBlob.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InputStreamSource;


# virtual methods
.method public abstract openInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract save()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
