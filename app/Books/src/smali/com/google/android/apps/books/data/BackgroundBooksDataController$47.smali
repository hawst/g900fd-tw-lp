.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->lookup(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$languageId:Ljava/lang/String;

.field final synthetic val$term:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1336
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$term:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$languageId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1339
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/LocalDictionarySubController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$term:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$languageId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOnDictionaryKeyError:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->lookup(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/apps/books/annotations/DictionaryEntry;

    move-result-object v0

    .line 1341
    .local v0, "result":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1342
    return-void
.end method
