.class public Lcom/google/android/apps/books/util/BitmapSimpleDrawable;
.super Ljava/lang/Object;
.source "BitmapSimpleDrawable.java"

# interfaces
.implements Lcom/google/android/apps/books/util/SimpleDrawable;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;->mBitmap:Landroid/graphics/Bitmap;

    .line 17
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    return-void
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 22
    return-object p1
.end method

.method public getViewLayerType()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method
