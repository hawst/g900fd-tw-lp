.class public Lcom/google/android/apps/books/widget/StaggeredCoverViewPlayHeaderListLayout;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.source "StaggeredCoverViewPlayHeaderListLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I
    .locals 7
    .param p1, "contentView"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 31
    const/4 v0, -0x1

    .line 32
    .local v0, "absoluteY":I
    instance-of v4, p1, Lcom/google/android/apps/books/widget/StaggeredCoverView;

    if-eqz v4, :cond_1

    .line 34
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverViewPlayHeaderListLayout;->tryFindHeaderSpacerView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 35
    .local v2, "headerSpacer":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 36
    new-array v1, v6, [I

    .line 37
    .local v1, "contentViewLocationXy":[I
    new-array v3, v6, [I

    .line 42
    .local v3, "spacerViewLocationXy":[I
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 43
    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 44
    aget v4, v1, v5

    aget v5, v3, v5

    sub-int v0, v4, v5

    .end local v1    # "contentViewLocationXy":[I
    .end local v3    # "spacerViewLocationXy":[I
    :cond_0
    move v4, v0

    .line 48
    .end local v2    # "headerSpacer":Landroid/view/View;
    :goto_0
    return v4

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I

    move-result v4

    goto :goto_0
.end method
