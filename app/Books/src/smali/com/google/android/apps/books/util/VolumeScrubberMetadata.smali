.class public Lcom/google/android/apps/books/util/VolumeScrubberMetadata;
.super Ljava/lang/Object;
.source "VolumeScrubberMetadata.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mAvailableImageFraction:F

.field private mAvailableTextFraction:F

.field private mImageFirstInvalidPageIndex:I

.field private final mIsRightToLeft:Z

.field private final mMaxPageNumber:Ljava/lang/Long;

.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mMissingChapters:Z

.field private mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private final mResources:Landroid/content/res/Resources;

.field private mTextFirstInvalidPageIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/res/Resources;)V
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    const v3, 0x7fffffff

    const/high16 v2, 0x3f800000    # 1.0f

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 38
    iput v2, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableTextFraction:F

    .line 39
    iput v3, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mTextFirstInvalidPageIndex:I

    .line 40
    iput v2, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableImageFraction:F

    .line 41
    iput v3, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mImageFirstInvalidPageIndex:I

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mResources:Landroid/content/res/Resources;

    .line 51
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mIsRightToLeft:Z

    .line 52
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMissingChapters:Z

    .line 54
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v0

    .line 55
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->parsePageNumber(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMaxPageNumber:Ljava/lang/Long;

    .line 57
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->computeAvailableFractions(Ljava/util/List;Ljava/util/List;)V

    .line 58
    return-void
.end method

.method private computeAvailableFractions(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .local p2, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v6

    if-nez v6, :cond_0

    .line 107
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 90
    .local v2, "pageCount":I
    const/4 v0, 0x0

    .line 91
    .local v0, "availablePages":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/Segment;

    .line 92
    .local v5, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v5}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 93
    invoke-interface {v5}, Lcom/google/android/apps/books/model/Segment;->getPageCount()I

    move-result v6

    add-int/2addr v0, v6

    goto :goto_1

    .line 96
    .end local v5    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_2
    iput v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mTextFirstInvalidPageIndex:I

    .line 97
    int-to-float v6, v0

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableTextFraction:F

    .line 100
    const/4 v3, 0x0

    .line 101
    .local v3, "pageIndex":I
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    .line 102
    .local v4, "pagesSize":I
    :goto_2
    if-ge v3, v4, :cond_3

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 103
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 105
    :cond_3
    iput v3, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mImageFirstInvalidPageIndex:I

    .line 106
    int-to-float v6, v3

    int-to-float v7, v4

    div-float/2addr v6, v7

    iput v6, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableImageFraction:F

    goto :goto_0
.end method

.method private getPageTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAvailableContentFraction()F
    .locals 2

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMissingChapters:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-nez v0, :cond_1

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 204
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableTextFraction:F

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mAvailableImageFraction:F

    goto :goto_0
.end method

.method public getCalloutPageLabelLong(IZ)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "itemIndex"    # I
    .param p2, "scrubActive"    # Z

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->getPageTitle(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "pageTitle":Ljava/lang/String;
    const/4 v0, 0x0

    .line 133
    .local v0, "condensed":Z
    iget-object v2, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMaxPageNumber:Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Lcom/google/android/apps/books/util/ReaderUtils;->formatPageTitle(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/Long;Z)Ljava/lang/CharSequence;

    move-result-object v2

    return-object v2
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageCount()I

    move-result v0

    return v0
.end method

.method public getMaxViewableIndex()I
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 197
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mTextFirstInvalidPageIndex:I

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mImageFirstInvalidPageIndex:I

    goto :goto_1
.end method

.method public getScrubberDescription(I)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "itemIndex"    # I

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "pageTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0f0127

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mMaxPageNumber:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isRightToLeft()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mIsRightToLeft:Z

    return v0
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 0
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 62
    return-void
.end method
