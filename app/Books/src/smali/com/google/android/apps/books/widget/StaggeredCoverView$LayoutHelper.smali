.class Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;
.super Ljava/lang/Object;
.source "StaggeredCoverView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LayoutHelper"
.end annotation


# instance fields
.field private mBestCombination:[I

.field private mBottoms:[I

.field private mColumnCount:I

.field private mCombination:[I

.field private mHeights:[I

.field private mLayoutBottom:I

.field final synthetic this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 1

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->setColumnCount(I)V

    .line 194
    return-void
.end method

.method private computeLayoutBottom()I
    .locals 3

    .prologue
    .line 298
    const/4 v0, 0x0

    .line 299
    .local v0, "bottom":I
    const/4 v1, 0x0

    .local v1, "col":I
    :goto_0
    iget v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    if-ge v1, v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    aget v2, v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 299
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 302
    :cond_0
    return v0
.end method

.method private computeRowHeight(I[I[I[I)I
    .locals 4
    .param p1, "count"    # I
    .param p2, "columns"    # [I
    .param p3, "heights"    # [I
    .param p4, "bottoms"    # [I

    .prologue
    .line 257
    const/4 v1, 0x0

    .line 258
    .local v1, "maxBottom":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 259
    aget v2, p2, v0

    aget v2, p4, v2

    aget v3, p3, v0

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_0
    return v1
.end method

.method private isFullWidthChild(Landroid/view/View;)Z
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->fillWidth:Z

    return v0
.end method

.method private layoutRow(II[II[I)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "count"    # I
    .param p3, "columns"    # [I
    .param p4, "widthMinusPadding"    # I
    .param p5, "bottoms"    # [I

    .prologue
    .line 283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 284
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    add-int v6, p1, v1

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 285
    .local v3, "staggeredChild":Landroid/view/View;
    if-eqz p3, :cond_0

    aget v0, p3, v1

    .line 286
    .local v0, "col":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v5, v5, Lcom/google/android/apps/books/widget/StaggeredCoverView;->edgePadding:I

    mul-int v6, v0, p4

    iget v7, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    div-int/2addr v6, v7

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v6, v6, Lcom/google/android/apps/books/widget/StaggeredCoverView;->interiorPadding:I

    mul-int/2addr v6, v0

    add-int v2, v5, v6

    .line 289
    .local v2, "left":I
    aget v4, p5, v0

    .line 290
    .local v4, "top":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v4

    aput v5, p5, v0

    .line 291
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    aget v6, p5, v0

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 293
    aget v5, p5, v0

    iget-object v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v6, v6, Lcom/google/android/apps/books/widget/StaggeredCoverView;->interiorPadding:I

    add-int/2addr v5, v6

    aput v5, p5, v0

    .line 283
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "col":I
    .end local v2    # "left":I
    .end local v4    # "top":I
    :cond_0
    move v0, v1

    .line 285
    goto :goto_1

    .line 295
    .end local v3    # "staggeredChild":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private nextColumnCombination([III)Z
    .locals 7
    .param p1, "combination"    # [I
    .param p2, "k"    # I
    .param p3, "n"    # I

    .prologue
    .line 219
    add-int/lit8 v3, p3, -0x1

    .line 220
    .local v3, "maxValue":I
    add-int/lit8 v1, p2, -0x1

    .line 223
    .local v1, "lastSlot":I
    move v5, v1

    .line 224
    .local v5, "target":I
    :goto_0
    if-ltz v5, :cond_0

    .line 225
    sub-int v4, v1, v5

    .line 227
    .local v4, "numberSlotsAfterTarget":I
    sub-int v2, v3, v4

    .line 228
    .local v2, "maxValidValueAtTargetSlot":I
    aget v6, p1, v5

    if-ge v6, v2, :cond_1

    .line 234
    .end local v2    # "maxValidValueAtTargetSlot":I
    .end local v4    # "numberSlotsAfterTarget":I
    :cond_0
    if-ltz v5, :cond_3

    .line 237
    aget v6, p1, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v5

    .line 238
    add-int/lit8 v0, v5, 0x1

    .local v0, "j":I
    :goto_1
    if-ge v0, p2, :cond_2

    .line 239
    add-int/lit8 v6, v0, -0x1

    aget v6, p1, v6

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v0

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 231
    .end local v0    # "j":I
    .restart local v2    # "maxValidValueAtTargetSlot":I
    .restart local v4    # "numberSlotsAfterTarget":I
    :cond_1
    add-int/lit8 v5, v5, -0x1

    .line 232
    goto :goto_0

    .line 241
    .end local v2    # "maxValidValueAtTargetSlot":I
    .end local v4    # "numberSlotsAfterTarget":I
    .restart local v0    # "j":I
    :cond_2
    const/4 v6, 0x1

    .line 243
    .end local v0    # "j":I
    :goto_2
    return v6

    :cond_3
    const/4 v6, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    return v0
.end method

.method public getLayoutBottom()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mLayoutBottom:I

    return v0
.end method

.method public layoutViews(II)V
    .locals 30
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 324
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v4, v4, Lcom/google/android/apps/books/widget/StaggeredCoverView;->edgePadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, p1, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v7, v7, Lcom/google/android/apps/books/widget/StaggeredCoverView;->interiorPadding:I

    mul-int/2addr v6, v7

    sub-int v8, v4, v6

    .line 327
    .local v8, "widthMinusPadding":I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    move/from16 v0, v22

    if-ge v0, v4, :cond_0

    .line 328
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    const/4 v6, 0x0

    aput v6, v4, v22

    .line 327
    add-int/lit8 v22, v22, 0x1

    goto :goto_0

    .line 333
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v19

    .line 334
    .local v19, "childCount":I
    const/4 v5, 0x0

    .line 335
    .local v5, "index":I
    :goto_1
    move/from16 v0, v19

    if-ge v5, v0, :cond_a

    .line 336
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 337
    .local v18, "child":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->isFullWidthChild(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 338
    const/16 v29, 0x0

    .line 339
    .local v29, "top":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    .local v15, "arr$":[I
    array-length v0, v15

    move/from16 v25, v0

    .local v25, "len$":I
    const/16 v23, 0x0

    .local v23, "i$":I
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_1

    aget v17, v15, v23

    .line 340
    .local v17, "bottom":I
    move/from16 v0, v29

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v29

    .line 339
    add-int/lit8 v23, v23, 0x1

    goto :goto_2

    .line 342
    .end local v17    # "bottom":I
    :cond_1
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int v17, v29, v4

    .line 343
    .restart local v17    # "bottom":I
    const/4 v4, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v29

    move/from16 v2, p1

    move/from16 v3, v17

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 344
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget v6, v6, Lcom/google/android/apps/books/widget/StaggeredCoverView;->interiorPadding:I

    add-int v6, v6, v17

    invoke-static {v4, v6}, Ljava/util/Arrays;->fill([II)V

    .line 345
    add-int/lit8 v5, v5, 0x1

    .line 346
    goto :goto_1

    .line 348
    .end local v15    # "arr$":[I
    .end local v17    # "bottom":I
    .end local v23    # "i$":I
    .end local v25    # "len$":I
    .end local v29    # "top":I
    :cond_2
    move/from16 v21, v5

    .line 349
    .local v21, "firstStaggered":I
    move/from16 v24, v21

    .line 350
    .local v24, "lastStaggered":I
    :goto_3
    add-int/lit8 v4, v24, 0x1

    move/from16 v0, v19

    if-ge v4, v0, :cond_3

    .line 351
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    add-int/lit8 v6, v24, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 352
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->isFullWidthChild(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 358
    :cond_3
    sub-int v4, v24, v21

    add-int/lit8 v27, v4, 0x1

    .line 359
    .local v27, "staggeredCount":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    div-int v28, v27, v4

    .line 360
    .local v28, "staggeredRowCount":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    rem-int v11, v27, v4

    .line 362
    .local v11, "overflowCount":I
    const/16 v26, 0x0

    .local v26, "row":I
    :goto_4
    move/from16 v0, v26

    move/from16 v1, v28

    if-ge v0, v1, :cond_5

    .line 363
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->layoutRow(II[II[I)V

    .line 364
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    add-int/2addr v5, v4

    .line 362
    add-int/lit8 v26, v26, 0x1

    goto :goto_4

    .line 355
    .end local v11    # "overflowCount":I
    .end local v26    # "row":I
    .end local v27    # "staggeredCount":I
    .end local v28    # "staggeredRowCount":I
    :cond_4
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    .line 367
    .restart local v11    # "overflowCount":I
    .restart local v26    # "row":I
    .restart local v27    # "staggeredCount":I
    .restart local v28    # "staggeredRowCount":I
    :cond_5
    if-lez v11, :cond_9

    .line 378
    add-int/lit8 v4, v11, -0x1

    sub-int v10, v24, v4

    .line 379
    .local v10, "firstOverflow":I
    const/16 v22, 0x0

    :goto_5
    move/from16 v0, v22

    if-ge v0, v11, :cond_6

    .line 380
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mHeights:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    add-int v7, v10, v22

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    aput v6, v4, v22

    .line 381
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mCombination:[I

    aput v22, v4, v22

    .line 382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBestCombination:[I

    aput v22, v4, v22

    .line 379
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    .line 384
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBestCombination:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mHeights:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v6, v7}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->computeRowHeight(I[I[I[I)I

    move-result v16

    .line 386
    .local v16, "bestCombinationHeight":I
    :cond_7
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mCombination:[I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11, v6}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->nextColumnCombination([III)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mCombination:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mHeights:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v6, v7}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->computeRowHeight(I[I[I[I)I

    move-result v20

    .line 389
    .local v20, "combinationHeight":I
    move/from16 v0, v20

    move/from16 v1, v16

    if-ge v0, v1, :cond_7

    .line 390
    move/from16 v16, v20

    .line 391
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mCombination:[I

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBestCombination:[I

    const/4 v9, 0x0

    invoke-static {v4, v6, v7, v9, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_6

    .line 396
    .end local v20    # "combinationHeight":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBestCombination:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    move-object/from16 v9, p0

    move v13, v8

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->layoutRow(II[II[I)V

    .line 399
    .end local v10    # "firstOverflow":I
    .end local v16    # "bestCombinationHeight":I
    :cond_9
    add-int/2addr v5, v11

    goto/16 :goto_1

    .line 403
    .end local v11    # "overflowCount":I
    .end local v18    # "child":Landroid/view/View;
    .end local v21    # "firstStaggered":I
    .end local v24    # "lastStaggered":I
    .end local v26    # "row":I
    .end local v27    # "staggeredCount":I
    .end local v28    # "staggeredRowCount":I
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->computeLayoutBottom()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mLayoutBottom:I

    .line 404
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 306
    iput p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    .line 309
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBottoms:[I

    .line 310
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mHeights:[I

    .line 311
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mCombination:[I

    .line 312
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mColumnCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->mBestCombination:[I

    .line 313
    return-void
.end method
