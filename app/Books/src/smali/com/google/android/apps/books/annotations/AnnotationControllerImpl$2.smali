.class final Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->makeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isDeviceConnected()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public shouldAlwaysAskForFullAnnotationRefresh()Z
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_FORCE_ANNOTATION_REFRESH:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
