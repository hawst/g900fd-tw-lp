.class public Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
.super Ljava/lang/Object;
.source "ContentFormatDownloadProgress.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;",
        ">;"
    }
.end annotation


# static fields
.field public static final NO_PROGRESS:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;


# instance fields
.field private final mDownloadProgress:I

.field private final mIsFullyDownloaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;-><init>(D)V

    sput-object v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    return-void
.end method

.method public constructor <init>(D)V
    .locals 3
    .param p1, "downloadFraction"    # D

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p1, p2}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->downloadFractionToProgress(D)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    .line 72
    iget v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mIsFullyDownloaded:Z

    .line 73
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static downloadFractionToProgress(D)I
    .locals 2
    .param p0, "downloadFraction"    # D

    .prologue
    .line 117
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method

.method public static fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 4
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 39
    invoke-static {p0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->getBookPreferredFormat(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v0

    .line 40
    .local v0, "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    new-instance v1, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-static {p0, v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->getDownloadFraction(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)D

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;-><init>(D)V

    return-object v1
.end method

.method public static fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 4
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->getDownloadFraction(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;-><init>(D)V

    return-object v0
.end method

.method private static getBookPreferredFormat(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 2
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 49
    const-string v1, "preferred_mode"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromInteger(I)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .line 50
    .local v0, "preferredMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v1

    goto :goto_0
.end method

.method private static getDownloadFraction(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)D
    .locals 6
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 108
    const-string v4, "segment_fraction"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getDoubleObject(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    .line 109
    .local v2, "sectionFraction":Ljava/lang/Double;
    const-string v4, "resource_fraction"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getDoubleObject(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    .line 110
    .local v1, "resourceFraction":Ljava/lang/Double;
    const-string v4, "page_fraction"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getDoubleObject(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    .line 111
    .local v0, "pageFraction":Ljava/lang/Double;
    const-string v4, "structure_fraction"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getDoubleObject(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    .line 112
    .local v3, "structureFraction":Ljava/lang/Double;
    invoke-static {p1, v2, v1, v0, v3}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getDownloadedFraction(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D

    move-result-wide v4

    return-wide v4
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;)I
    .locals 2
    .param p1, "another"    # Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .prologue
    .line 150
    iget v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    iget v1, p1, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->compareTo(Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 166
    if-ne p0, p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    .line 169
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 170
    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 173
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 175
    check-cast v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .line 176
    .local v0, "other":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    iget v3, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    iget v4, v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 177
    goto :goto_0

    .line 179
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mIsFullyDownloaded:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mIsFullyDownloaded:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 180
    goto :goto_0
.end method

.method public getDownloadFraction()F
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 156
    const/16 v0, 0x1f

    .line 157
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 158
    .local v1, "result":I
    iget v2, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    add-int/lit8 v1, v2, 0x1f

    .line 159
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mIsFullyDownloaded:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int v1, v3, v2

    .line 160
    return v1

    .line 159
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0
.end method

.method public isFullyDownloaded()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mIsFullyDownloaded:Z

    return v0
.end method

.method public isPartiallyOrFullyDownloaded()Z
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->mDownloadProgress:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
