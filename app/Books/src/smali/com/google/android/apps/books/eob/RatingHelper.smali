.class public Lcom/google/android/apps/books/eob/RatingHelper;
.super Ljava/lang/Object;
.source "RatingHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;,
        Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    }
.end annotation


# direct methods
.method public static requestRatingInfo(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)Z
    .locals 7
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "readyListener"    # Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 63
    const-string v3, "RatingHelper"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    const-string v3, "RatingHelper"

    const-string v4, "requestRatingInfo"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    iput-boolean v5, p3, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;->requestIsInProcess:Z

    .line 69
    new-instance v1, Lcom/google/android/apps/books/eob/RatingHelper$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/books/eob/RatingHelper$1;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)V

    .line 145
    .local v1, "serviceConnection":Landroid/content/ServiceConnection;
    if-eqz p2, :cond_3

    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.vending.reviews.IReviewsService.BIND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "reviewServiceIntent":Landroid/content/Intent;
    const-string v3, "com.android.vending"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    invoke-virtual {p2, v0, v1, v5}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    .line 155
    .end local v0    # "reviewServiceIntent":Landroid/content/Intent;
    .local v2, "serviceIsBound":Z
    :goto_0
    const-string v3, "RatingHelper"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    const-string v3, "RatingHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bindService bound="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_1
    if-nez v2, :cond_2

    .line 160
    const/4 v3, 0x0

    invoke-virtual {p3, v3}, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;->onRatingInfoAvailable(Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V

    .line 163
    :cond_2
    return v2

    .line 152
    .end local v2    # "serviceIsBound":Z
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "serviceIsBound":Z
    goto :goto_0
.end method
