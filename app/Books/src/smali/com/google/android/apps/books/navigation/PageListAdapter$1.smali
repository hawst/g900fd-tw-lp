.class Lcom/google/android/apps/books/navigation/PageListAdapter$1;
.super Ljava/lang/Object;
.source "PageListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/PageListAdapter;-><init>(Lcom/google/android/apps/books/util/Destroyable;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/ublib/widget/AbsWarpListView;IIZZZLandroid/graphics/Point;IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

.field final synthetic val$thumbnailFraction:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/PageListAdapter;I)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    iput p2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->val$thumbnailFraction:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPageSizes(Lcom/google/android/apps/books/render/PageHandle;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 3
    .param p1, "pageHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p2, "outFullSize"    # Landroid/graphics/Point;
    .param p3, "outThumbnailSize"    # Landroid/graphics/Point;

    .prologue
    .line 148
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getWidth()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getHeight()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mMaxPageWidth:I
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$000(Lcom/google/android/apps/books/navigation/PageListAdapter;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->val$thumbnailFraction:I

    div-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/PageListAdapter;->mRowHeight:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageListAdapter;->access$100(Lcom/google/android/apps/books/navigation/PageListAdapter;)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->val$thumbnailFraction:I

    div-int/2addr v1, v2

    invoke-static {p3, v0, v1}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 151
    iget v0, p3, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->val$thumbnailFraction:I

    mul-int/2addr v0, v1

    iget v1, p3, Landroid/graphics/Point;->y:I

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->val$thumbnailFraction:I

    mul-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 153
    return-void
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->isDestroyed()Z

    move-result v0

    return v0
.end method

.method public spreadRangeChanged()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->notifyDataSetChanged()V

    .line 143
    return-void
.end method
