.class Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoCardsHelperCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 1280
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 1280
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2700(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    move-result-object v0

    return-object v0
.end method

.method public hideCardsLayout()Z
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1304
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z

    move-result v0

    .line 1306
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAllCardsDismissed()V
    .locals 2

    .prologue
    .line 1291
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z

    .line 1295
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2400(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1296
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 1298
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2402(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 1299
    return-void
.end method

.method public onCardsAdded(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 1
    .param p1, "animateTo"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->onCardsAdded(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 1287
    :cond_0
    return-void
.end method
