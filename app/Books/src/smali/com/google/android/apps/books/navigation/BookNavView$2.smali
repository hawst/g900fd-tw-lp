.class Lcom/google/android/apps/books/navigation/BookNavView$2;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/BookNavView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/google/android/ublib/widget/AbsWarpListView;III)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 190
    if-lez p3, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->adjustScroll()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$600(Lcom/google/android/apps/books/navigation/BookNavView;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->syncScrubberWithFling()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$700(Lcom/google/android/apps/books/navigation/BookNavView;)V

    .line 194
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 3
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "scrollState"    # I

    .prologue
    const/4 v2, 0x0

    .line 178
    if-nez p2, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->access$102(Lcom/google/android/apps/books/navigation/BookNavView;Lcom/google/android/apps/books/render/PageHandle;)Lcom/google/android/apps/books/render/PageHandle;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mOnlyFallbackHandle:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$202(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollStartedByScrubber:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$302(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$2;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$500(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onScrollStateChanged(I)V

    .line 185
    return-void
.end method
