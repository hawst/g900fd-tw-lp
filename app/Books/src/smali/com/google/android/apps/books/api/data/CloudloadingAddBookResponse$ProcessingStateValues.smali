.class public final enum Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;
.super Ljava/lang/Enum;
.source "CloudloadingAddBookResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProcessingStateValues"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

.field public static final enum COMPLETED_FAILED:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

.field public static final enum COMPLETED_SUCCESS:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

.field public static final enum RUNNING:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

.field public static final enum UNKNOWN:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->UNKNOWN:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    .line 25
    new-instance v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->RUNNING:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    .line 26
    new-instance v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    const-string v1, "COMPLETED_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->COMPLETED_SUCCESS:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    .line 27
    new-instance v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    const-string v1, "COMPLETED_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->COMPLETED_FAILED:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    sget-object v1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->UNKNOWN:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->RUNNING:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->COMPLETED_SUCCESS:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->COMPLETED_FAILED:Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->$VALUES:[Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->$VALUES:[Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    return-object v0
.end method
