.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$resource:Lcom/google/android/apps/books/model/Resource;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$resource:Lcom/google/android/apps/books/model/Resource;

    iput-object p5, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iput-object p6, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 250
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v0}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$300(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v6

    .line 240
    .local v6, "haveContentConsumers":Z
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getResourceContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 248
    .local v7, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v5, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iget-object v8, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$400(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0

    .line 242
    .end local v7    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :catch_0
    move-exception v9

    .line 243
    .local v9, "e":Ljava/io/IOException;
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v9}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 227
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
