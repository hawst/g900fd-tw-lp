.class public Lcom/google/android/apps/books/sync/VolumesSynchronizable;
.super Ljava/lang/Object;
.source "VolumesSynchronizable.java"

# interfaces
.implements Lcom/google/android/apps/books/sync/Synchronizable;


# static fields
.field private static final FULLY_DOWNLOADED_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field public static final FULL_INITIAL_VERSION:Ljava/lang/String; = "full-1.0.0"

.field static final PREVIEW_INITIAL_VERSION:Ljava/lang/String; = "preview-1.0.0"

.field private static final PROJECTION:[Ljava/lang/String;

.field static final UNKNOWN_VERSION_NUMBER:Ljava/lang/String; = "unknown-version"

.field private static final sColumnToClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

.field private final mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    invoke-static {}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getVolumeColumnToClass()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->sColumnToClass:Ljava/util/Map;

    .line 52
    invoke-static {}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->createProjection()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->PROJECTION:[Ljava/lang/String;

    .line 329
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "preferred_mode"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "segment_fraction"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "resource_fraction"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "page_fraction"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "structure_fraction"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->FULLY_DOWNLOADED_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/accounts/Account;Lcom/google/android/apps/books/common/VolumeUsageManager;Lcom/google/android/apps/books/provider/VolumeContentStore;)V
    .locals 2
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "volumeUsageManager"    # Lcom/google/android/apps/books/common/VolumeUsageManager;
    .param p4, "volumeContentStore"    # Lcom/google/android/apps/books/provider/VolumeContentStore;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const-string v0, "missing resolver"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    .line 85
    const-string v0, "missing account"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    .line 86
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v1, "empty account"

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkIsGraphic(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 87
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/VolumeUsageManager;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    .line 88
    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/VolumeContentStore;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    .line 89
    return-void
.end method

.method private blockContentWipe(Ljava/lang/String;Landroid/content/ContentValues;Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;)Z
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "oldValues"    # Landroid/content/ContentValues;
    .param p3, "oldAccess"    # Lcom/google/android/apps/books/model/VolumeData$Access;
    .param p4, "oldVersion"    # Ljava/lang/String;
    .param p5, "newAccess"    # Lcom/google/android/apps/books/model/VolumeData$Access;
    .param p6, "newVersion"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v2, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/common/VolumeUsageManager;->isVolumeLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    if-ne p3, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    if-eq p5, v2, :cond_2

    .line 311
    :cond_0
    const/4 v1, 0x1

    .line 326
    :cond_1
    :goto_0
    return v1

    .line 314
    :cond_2
    invoke-virtual {p3, p5}, Lcom/google/android/apps/books/model/VolumeData$Access;->shouldHonorTransition(Lcom/google/android/apps/books/model/VolumeData$Access;)Z

    move-result v0

    .line 315
    .local v0, "honor":Z
    if-nez v0, :cond_1

    .line 321
    invoke-virtual {p3, p5}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p4, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 326
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private contentWedged(Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 4
    .param p1, "access"    # Lcom/google/android/apps/books/model/VolumeData$Access;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "oldValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 418
    sget-object v3, Lcom/google/android/apps/books/model/VolumeData$Access;->FREE:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 420
    .local v0, "shouldHaveAllContent":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->hasForbiddenContent(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    return v1

    .end local v0    # "shouldHaveAllContent":Z
    :cond_1
    move v0, v2

    .line 418
    goto :goto_0

    .restart local v0    # "shouldHaveAllContent":Z
    :cond_2
    move v1, v2

    .line 420
    goto :goto_1
.end method

.method private static createProjection()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    sget-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->sColumnToClass:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->sColumnToClass:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static getNewValue(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "oldValue"    # Ljava/lang/String;
    .param p1, "newValues"    # Landroid/content/ContentValues;
    .param p2, "columnName"    # Ljava/lang/String;

    .prologue
    .line 363
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    .line 364
    .local v0, "hasValue":Z
    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .end local p0    # "oldValue":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private getRowUri(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 99
    const-string v1, "volume_id"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private hasForbiddenContent(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "oldValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x1

    .line 371
    const-string v6, "has_text_mode"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 372
    .local v1, "hasText":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eqz v6, :cond_2

    .line 373
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v6, p1}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 374
    .local v3, "resourcesUri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v6, p1}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 375
    .local v4, "sectionsUri":Landroid/net/Uri;
    const-string v6, "content_status"

    invoke-direct {p0, v4, v6}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->hasForbiddenRows(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "content_status"

    invoke-direct {p0, v3, v6}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->hasForbiddenRows(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 382
    .end local v3    # "resourcesUri":Landroid/net/Uri;
    .end local v4    # "sectionsUri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return v5

    .line 380
    :cond_2
    const-string v6, "has_image_mode"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 381
    .local v0, "hasImage":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v6, p1}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 382
    .local v2, "pagesUri":Landroid/net/Uri;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    const-string v6, "content_status"

    invoke-direct {p0, v2, v6}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->hasForbiddenRows(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private hasForbiddenRows(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "contentStatusColumn"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 390
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "COUNT(*)"

    aput-object v0, v2, v8

    .line 391
    .local v2, "PROJECTION":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 392
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 394
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 395
    .local v7, "hasForbidden":Z
    :goto_0
    if-eqz v7, :cond_0

    const-string v0, "VolumesSynchronizable"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "VolumesSynchronizable"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " has "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " forbidden rows"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7

    .end local v7    # "hasForbidden":Z
    :cond_1
    move v7, v8

    .line 394
    goto :goto_0

    .line 400
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private isFullyDownloaded(Ljava/lang/String;)Z
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 341
    .local v2, "uri":Landroid/net/Uri;
    sget-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->FULLY_DOWNLOADED_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v6

    .line 344
    .local v6, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 345
    const-string v0, "VolumesSynchronizable"

    const-string v1, "shouldHonorTransition() could not move to first, honoring transition"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    const/4 v0, 0x0

    .line 350
    if-eqz v6, :cond_0

    .line 351
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_0
    :goto_0
    return v0

    .line 348
    :cond_1
    :try_start_1
    invoke-static {v6}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 350
    if-eqz v6, :cond_0

    .line 351
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_0

    .line 350
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 351
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_2
    throw v0
.end method

.method private isInitialVersion(Ljava/lang/String;)Z
    .locals 1
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 409
    const-string v0, "full-1.0.0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preview-1.0.0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isManifestParsed(Ljava/lang/String;)Z
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 427
    iget-object v4, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, p1}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 428
    .local v3, "sectionDirUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, p1}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 430
    .local v1, "pageDirUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v4, v3}, Lcom/google/android/apps/books/util/ProviderUtils;->queryForDirCount(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v2

    .line 431
    .local v2, "sectionCount":I
    iget-object v4, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v4, v1}, Lcom/google/android/apps/books/util/ProviderUtils;->queryForDirCount(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v0

    .line 434
    .local v0, "pageCount":I
    if-gtz v2, :cond_0

    if-lez v0, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public delete(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "rows":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/content/ContentValues;>;"
    return-void
.end method

.method public extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 29
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "newValues"    # Landroid/content/ContentValues;

    .prologue
    .line 163
    const-string v2, "viewability"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v22

    .line 164
    .local v22, "hasViewability":Z
    const-string v2, "open_access"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v21

    .line 165
    .local v21, "hasOpenAccess":Z
    const-string v2, "buy_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v20

    .line 166
    .local v20, "hasBuyUrl":Z
    move/from16 v0, v22

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    move/from16 v0, v22

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 167
    :cond_0
    const-string v2, "VolumesSynchronizable"

    const-string v4, ""

    new-instance v26, Ljava/lang/IllegalArgumentException;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Incomplete access information in : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-static {v2, v4, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    :cond_1
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/books/sync/SyncUtil;->extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v23

    .line 176
    .local v23, "rawUpdates":Landroid/content/ContentValues;
    const-string v2, "version"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 178
    const-string v2, "volume_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "volumeId":Ljava/lang/String;
    const-string v2, "cover_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const-string v4, "cover_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 186
    :cond_2
    const-string v2, "viewability"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "open_access"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "buy_url"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "content_version"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 291
    .end local v23    # "rawUpdates":Landroid/content/ContentValues;
    :goto_0
    return-object v23

    .line 192
    .restart local v23    # "rawUpdates":Landroid/content/ContentValues;
    :cond_3
    new-instance v24, Landroid/content/ContentValues;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 194
    .local v24, "updates":Landroid/content/ContentValues;
    const-string v2, "viewability"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 195
    .local v9, "oldViewability":Ljava/lang/String;
    const-string v2, "open_access"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 196
    .local v11, "oldOpenAccess":Ljava/lang/String;
    const-string v2, "buy_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 197
    .local v10, "oldBuyUrl":Ljava/lang/String;
    const-string v2, "content_version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 198
    .local v6, "oldVersion":Ljava/lang/String;
    invoke-static {v9, v11, v10}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v5

    .line 200
    .local v5, "oldAccess":Lcom/google/android/apps/books/model/VolumeData$Access;
    const-string v2, "viewability"

    move-object/from16 v0, p2

    invoke-static {v9, v0, v2}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getNewValue(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 201
    .local v12, "newViewability":Ljava/lang/String;
    const-string v2, "open_access"

    move-object/from16 v0, p2

    invoke-static {v11, v0, v2}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getNewValue(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 202
    .local v14, "newOpenAccess":Ljava/lang/String;
    const-string v2, "buy_url"

    move-object/from16 v0, p2

    invoke-static {v10, v0, v2}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getNewValue(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 203
    .local v13, "newBuyUrl":Ljava/lang/String;
    const-string v2, "content_version"

    move-object/from16 v0, p2

    invoke-static {v6, v0, v2}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getNewValue(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 204
    .local v8, "newVersion":Ljava/lang/String;
    invoke-static {v12, v14, v13}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v7

    .local v7, "newAccess":Lcom/google/android/apps/books/model/VolumeData$Access;
    move-object/from16 v2, p0

    move-object/from16 v4, p1

    .line 206
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->blockContentWipe(Ljava/lang/String;Landroid/content/ContentValues;Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;)Z

    move-result v15

    .line 208
    .local v15, "blockWipe":Z
    if-eqz v15, :cond_9

    .line 210
    if-ne v5, v7, :cond_4

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v11, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v10, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 214
    :cond_4
    const-string v2, "viewability"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 215
    .local v19, "explicitViewability":Ljava/lang/String;
    const-string v2, "open_access"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 216
    .local v17, "explicitOpenAccess":Ljava/lang/String;
    const-string v2, "buy_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 217
    .local v16, "explicitBuyUrl":Ljava/lang/String;
    const-string v2, "content_version"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 218
    .local v18, "explicitVersion":Ljava/lang/String;
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Not honoring "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " for "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "\nviewability: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "\nopenAccess: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "\nbuyUrl: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "\nversion: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    .end local v16    # "explicitBuyUrl":Ljava/lang/String;
    .end local v17    # "explicitOpenAccess":Ljava/lang/String;
    .end local v18    # "explicitVersion":Ljava/lang/String;
    .end local v19    # "explicitViewability":Ljava/lang/String;
    :cond_5
    const-string v2, "viewability"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 229
    const-string v2, "open_access"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 238
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eq v2, v4, :cond_6

    .line 239
    const-string v2, "buy_url"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 242
    :cond_6
    if-eqz v6, :cond_8

    .line 244
    const-string v2, "content_version"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 245
    const-string v2, "VolumesSynchronizable"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 246
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Blocking "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " and version already known: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; rawUpdates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; updates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_1
    move-object/from16 v23, v24

    .line 291
    goto/16 :goto_0

    .line 255
    :cond_8
    const-string v2, "content_version"

    const-string v4, "unknown-version"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Setting version to unknown-version, but blocking "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; rawUpdates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, ";  updates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 260
    :cond_9
    if-nez v6, :cond_7

    .line 263
    invoke-static/range {v9 .. v14}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->isContentInvalid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v25

    .line 266
    .local v25, "willWipe":Z
    if-nez v25, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->isManifestParsed(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 269
    :cond_a
    const-string v2, "content_version"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 270
    const-string v2, "VolumesSynchronizable"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 271
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Allowing "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " (willWipe="

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "), blocking version: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " -> "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; parsed: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->isManifestParsed(Ljava/lang/String;)Z

    move-result v26

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; rawUpdates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; updates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 276
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->isInitialVersion(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v3, v1}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->contentWedged(Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 279
    :cond_c
    const-string v2, "content_version"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 280
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Requesting content clear on version: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "->"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " or content wedged: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v3, v1}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->contentWedged(Lcom/google/android/apps/books/model/VolumeData$Access;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v26

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, " for "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; rawUpdates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v26, "; updates: "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 288
    :cond_d
    const-string v2, "VolumesSynchronizable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Old version unknown, setting version normally for "

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public getRowKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    const-string v0, "volume_id"

    return-object v0
.end method

.method public getWritableColumnToClass()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->sColumnToClass:Ljava/util/Map;

    return-object v0
.end method

.method public insertOrThrow(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 132
    const-string v1, "VolumesSynchronizable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const-string v1, "VolumesSynchronizable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inserting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, p1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 136
    .local v0, "insertValues":Landroid/content/ContentValues;
    const-string v1, "content_version"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public query(Landroid/content/ContentValues;)Landroid/database/Cursor;
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getRowUri(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryAll()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 116
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 5
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "updateValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->getRowUri(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 143
    .local v0, "uri":Landroid/net/Uri;
    const-string v1, "VolumesSynchronizable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    const-string v1, "VolumesSynchronizable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updating "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/sync/VolumesSynchronizable;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0, p2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method
