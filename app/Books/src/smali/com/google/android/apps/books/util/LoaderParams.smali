.class public Lcom/google/android/apps/books/util/LoaderParams;
.super Ljava/lang/Object;
.source "LoaderParams.java"


# direct methods
.method public static buildFrom(Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 24
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 25
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v0, p0}, Lcom/google/android/apps/books/util/LoaderParams;->setAccount(Landroid/os/Bundle;Landroid/accounts/Account;)V

    .line 26
    return-object v0
.end method

.method public static buildFrom(Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 31
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v0, p0}, Lcom/google/android/apps/books/util/LoaderParams;->setAccount(Landroid/os/Bundle;Landroid/accounts/Account;)V

    .line 32
    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/LoaderParams;->setVolumeId(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 33
    return-object v0
.end method

.method public static getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 78
    const-string v0, "account"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method public static getAddToMyEBooks(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 98
    const-string v0, "addToMyEBooks"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getCreatingShortcut(Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 74
    const-string v0, "shortcutOptions"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getPromptBeforeAddingToMyEbooks(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 106
    const-string v0, "promptBeforeAdding"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getShowDisplaySettings(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 138
    const-string v0, "showDisplaySettings"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getUpdateVolumeOverview(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const-string v0, "updateVolumeOverview"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 82
    const-string v0, "volumeId"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWarnOnSample(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "args"    # Landroid/os/Bundle;

    .prologue
    .line 122
    const-string v0, "warnOnSample"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setAccount(Landroid/os/Bundle;Landroid/accounts/Account;)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 41
    const-string v0, "missing account"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v0, "account"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 43
    return-void
.end method

.method public static setAddToMyEBooks(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "addToMyEBooks"    # Z

    .prologue
    .line 63
    const-string v0, "addToMyEBooks"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 64
    return-void
.end method

.method public static setCreatingShortcut(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "val"    # Z

    .prologue
    .line 37
    const-string v0, "shortcutOptions"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method

.method public static setPromptBeforeAddingToMyEbooks(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "prompt"    # Z

    .prologue
    .line 70
    const-string v0, "promptBeforeAdding"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 71
    return-void
.end method

.method public static setShowDisplaySettings(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "openDisplaySettingsOnRecreate"    # Z

    .prologue
    .line 130
    const-string v0, "showDisplaySettings"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    return-void
.end method

.method public static setUpdateVolumeOverview(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "updateVolumeOverview"    # Z

    .prologue
    .line 55
    const-string v0, "updateVolumeOverview"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    return-void
.end method

.method public static setVolumeId(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 46
    const-string v0, "missing account"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v0, "volumeId"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public static setWarnOnSample(Landroid/os/Bundle;Z)V
    .locals 1
    .param p0, "args"    # Landroid/os/Bundle;
    .param p1, "warnOnSample"    # Z

    .prologue
    .line 114
    const-string v0, "warnOnSample"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 115
    return-void
.end method
