.class Lcom/google/android/apps/books/app/HomeFragment$5;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$5;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 1
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$5;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataListener:Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1800(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 549
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 545
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$5;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
