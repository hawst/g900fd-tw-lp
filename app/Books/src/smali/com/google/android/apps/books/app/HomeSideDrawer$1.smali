.class Lcom/google/android/apps/books/app/HomeSideDrawer$1;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeSideDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountListToggleButtonClicked(Z)V
    .locals 0
    .param p1, "isListExpanded"    # Z

    .prologue
    .line 131
    return-void
.end method

.method public onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
    .locals 1
    .param p1, "isLoaded"    # Z
    .param p2, "profileDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public onDownloadToggleClicked(Z)V
    .locals 2
    .param p1, "isDownloadedOnly"    # Z

    .prologue
    .line 135
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 136
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setDownloadedOnlyMode(Z)V

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # invokes: Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDownloadedOnlyViews()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$100(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    .line 138
    return-void
.end method

.method public onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
    .locals 1
    .param p1, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .prologue
    .line 116
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDrawerContent()V

    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryAccountClicked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->switchAccountTo(Ljava/lang/String;)V

    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
    .locals 1
    .param p1, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 124
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 125
    const/4 v0, 0x1

    return v0
.end method
