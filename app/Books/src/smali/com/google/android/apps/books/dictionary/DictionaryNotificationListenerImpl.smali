.class public Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;
.super Ljava/lang/Object;
.source "DictionaryNotificationListenerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;


# instance fields
.field private mAggregateNotifications:Z

.field private mCompletedTotalSize:J

.field private mContentTitle:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private final mHandler:Landroid/os/Handler;

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private mPendingDownloads:I

.field private mRequestedLanguages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShowStartNotification:Z

.field private mTotalSize:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mNotificationManager:Landroid/app/NotificationManager;

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mHandler:Landroid/os/Handler;

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mRequestedLanguages:Ljava/util/Set;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private postCompletionNotification()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 180
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 181
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0f017b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 182
    .local v2, "text":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mAggregateNotifications:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0f01c2

    :goto_0
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "mDownloadSuccessToastString":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v4, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContentTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v6, v6, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const v4, 0x7f02010e

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->updateNotification()V

    .line 195
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->showToast(Ljava/lang/String;)V

    .line 196
    return-void

    .line 182
    .end local v0    # "mDownloadSuccessToastString":Ljava/lang/String;
    :cond_0
    const v3, 0x7f0f01c1

    goto :goto_0
.end method

.method private shouldIgnoreNotification(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mRequestedLanguages:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl$1;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 205
    return-void
.end method

.method private updateNotification()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl$2;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 215
    return-void
.end method


# virtual methods
.method public downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;)V
    .locals 5
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v4, 0x0

    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->shouldIgnoreNotification(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    :goto_0
    return-void

    .line 152
    :cond_0
    iget v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    .line 154
    instance-of v1, p2, Lcom/google/android/apps/books/data/OutOfSpaceException;

    if-eqz v1, :cond_1

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f017d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "text":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContentTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4, v4, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f02010d

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->updateNotification()V

    goto :goto_0

    .line 157
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    instance-of v1, p2, Ljava/net/ConnectException;

    if-eqz v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "text":Ljava/lang/String;
    goto :goto_1

    .line 161
    .end local v0    # "text":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f017c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "text":Ljava/lang/String;
    goto :goto_1
.end method

.method public finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->shouldIgnoreNotification(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mCompletedTotalSize:J

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mCompletedTotalSize:J

    .line 142
    iget v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    if-nez v0, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->postCompletionNotification()V

    goto :goto_0
.end method

.method public startingBatch(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dictionaryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 68
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 104
    :cond_0
    return-void

    .line 71
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    .line 72
    iget v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mPendingDownloads:I

    if-le v6, v7, :cond_2

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mAggregateNotifications:Z

    .line 73
    iput-wide v10, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mCompletedTotalSize:J

    .line 74
    iput-wide v10, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mTotalSize:J

    .line 75
    iput-boolean v7, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mShowStartNotification:Z

    .line 77
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 78
    .local v4, "res":Landroid/content/res/Resources;
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-virtual {v6}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/dictionary/LocalDictionaryUtils;->getDisplayLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "language":Ljava/lang/String;
    iget-boolean v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mAggregateNotifications:Z

    if-eqz v6, :cond_3

    const v6, 0x7f0f0176

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 84
    .local v5, "tickerText":Ljava/lang/String;
    :goto_1
    iget-boolean v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mAggregateNotifications:Z

    if-eqz v6, :cond_4

    const v6, 0x7f0f0178

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_2
    iput-object v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContentTitle:Ljava/lang/String;

    .line 88
    const v6, 0x7f0b00be

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 90
    .local v0, "appColor":I
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContext:Landroid/content/Context;

    invoke-direct {v6, v8}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x1080081

    invoke-virtual {v6, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mContentTitle:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 99
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mRequestedLanguages:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 100
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 101
    .local v3, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    iget-wide v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mTotalSize:J

    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mTotalSize:J

    .line 102
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mRequestedLanguages:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .end local v0    # "appColor":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "language":Ljava/lang/String;
    .end local v3    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .end local v4    # "res":Landroid/content/res/Resources;
    .end local v5    # "tickerText":Ljava/lang/String;
    :cond_2
    move v6, v8

    .line 72
    goto/16 :goto_0

    .line 80
    .restart local v2    # "language":Ljava/lang/String;
    .restart local v4    # "res":Landroid/content/res/Resources;
    :cond_3
    const v6, 0x7f0f0175

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v2, v9, v8

    invoke-static {v6, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 84
    .restart local v5    # "tickerText":Ljava/lang/String;
    :cond_4
    const v6, 0x7f0f0177

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v2, v9, v8

    invoke-static {v6, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2
.end method

.method public startingDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->shouldIgnoreNotification(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mShowStartNotification:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->updateNotification()V

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mShowStartNotification:Z

    goto :goto_0
.end method

.method public updateDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;I)V
    .locals 8
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "progressPercent"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->shouldIgnoreNotification(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mAggregateNotifications:Z

    if-eqz v2, :cond_1

    .line 125
    const-wide/16 v2, 0x64

    iget-wide v4, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mCompletedTotalSize:J

    mul-long/2addr v2, v4

    int-to-long v4, p2

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mTotalSize:J

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 127
    .local v0, "aggregatePercent":I
    move p2, v0

    .line 129
    .end local v0    # "aggregatePercent":I
    :cond_1
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v2

    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, "percentDownloaded":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const/16 v3, 0x64

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 133
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;->updateNotification()V

    goto :goto_0
.end method
