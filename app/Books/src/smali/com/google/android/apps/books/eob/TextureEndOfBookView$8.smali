.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->updateRating(Landroid/content/Context;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field final synthetic val$authorImageView:Landroid/widget/ImageView;

.field final synthetic val$readingActivity:Lcom/google/android/apps/books/app/ReadingActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Lcom/google/android/apps/books/app/ReadingActivity;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->val$readingActivity:Lcom/google/android/apps/books/app/ReadingActivity;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->val$authorImageView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 352
    if-eqz p2, :cond_1

    const-string v0, "TextureEndOfBookView"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    const-string v0, "TextureEndOfBookView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onImage saw exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->val$readingActivity:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 360
    new-instance v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;-><init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
