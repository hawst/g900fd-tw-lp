.class public Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;
.super Ljava/lang/Object;
.source "BooksCardsHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UploadContextMenuDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContextMenuPressed(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 3
    .param p1, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 146
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->onCardOverflowMenuShown()V

    .line 148
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getDocument()Lcom/google/android/ublib/cardlib/model/Document;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/playcards/UploadDocument;

    .line 149
    .local v0, "doc":Lcom/google/android/apps/books/playcards/UploadDocument;
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;Landroid/content/Context;Lcom/google/android/apps/books/playcards/UploadDocument;)V

    .line 162
    .local v1, "docMenuHandler":Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;
    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;->showPopupMenu(Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    move-result-object v2

    return-object v2
.end method
