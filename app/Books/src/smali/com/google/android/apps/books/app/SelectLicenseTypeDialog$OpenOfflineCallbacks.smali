.class final Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;
.super Ljava/lang/Object;
.source "SelectLicenseTypeDialog.java"

# interfaces
.implements Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OpenOfflineCallbacks"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;Landroid/accounts/Account;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "volumeId"    # Ljava/lang/String;
    .param p4, "activity"    # Landroid/app/Activity;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->this$0:Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p3, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mVolumeId:Ljava/lang/String;

    .line 98
    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mAccount:Landroid/accounts/Account;

    .line 99
    check-cast p4, Lcom/google/android/ublib/actionbar/UBLibActivity;

    .end local p4    # "activity":Landroid/app/Activity;
    iput-object p4, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    .line 100
    return-void
.end method


# virtual methods
.method public handleResultUi(III)V
    .locals 12
    .param p1, "outcome"    # I
    .param p2, "maxDevices"    # I
    .param p3, "devicesInUse"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 104
    const-string v9, "SelectLicenseTypeDialog"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 105
    const-string v9, "SelectLicenseTypeDialog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "OpenOfflineLM handleResultUi(), outcome="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", maxDevices="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    invoke-virtual {v9}, Lcom/google/android/ublib/actionbar/UBLibActivity;->isActivityDestroyed()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 109
    :cond_1
    const-string v7, "SelectLicenseTypeDialog"

    const/4 v8, 0x4

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 110
    const-string v7, "SelectLicenseTypeDialog"

    const-string v8, "Could not update offline license state, activity gone/destroyed"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_2
    :goto_0
    return-void

    .line 114
    :cond_3
    if-ne p1, v7, :cond_4

    move v4, v7

    .line 116
    .local v4, "grantedLicense":Z
    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    iget-object v10, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mAccount:Landroid/accounts/Account;

    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v3

    .line 118
    .local v3, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    if-eqz v4, :cond_5

    .line 119
    if-eqz v3, :cond_2

    .line 120
    iget-object v8, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mVolumeId:Ljava/lang/String;

    invoke-interface {v3, v8, v7, v7}, Lcom/google/android/apps/books/data/BooksDataController;->setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V

    .line 122
    iget-object v7, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mVolumeId:Ljava/lang/String;

    sget-object v8, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-interface {v3, v7, v8}, Lcom/google/android/apps/books/data/BooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    goto :goto_0

    .end local v3    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v4    # "grantedLicense":Z
    :cond_4
    move v4, v8

    .line 114
    goto :goto_1

    .line 125
    .restart local v3    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .restart local v4    # "grantedLicense":Z
    :cond_5
    iget-object v9, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    invoke-static {v9}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    .line 126
    .local v2, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    iget-object v9, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mActivity:Lcom/google/android/ublib/actionbar/UBLibActivity;

    invoke-virtual {v9}, Lcom/google/android/ublib/actionbar/UBLibActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 127
    .local v5, "res":Landroid/content/res/Resources;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "args":Landroid/os/Bundle;
    const/4 v9, -0x1

    if-ne p1, v9, :cond_6

    .line 131
    const v6, 0x7f0f00e5

    .line 132
    .local v6, "titleResId":I
    const v7, 0x7f0f00e6

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "bodyString":Ljava/lang/String;
    :goto_2
    new-instance v7, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v7, v0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v7

    const v8, 0x104000a

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 144
    new-instance v7, Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    invoke-direct {v7, v0}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;-><init>(Landroid/os/Bundle;)V

    iget-object v8, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    .line 147
    const-class v7, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;

    const/4 v8, 0x0

    invoke-interface {v2, v7, v0, v8}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    .end local v1    # "bodyString":Ljava/lang/String;
    .end local v6    # "titleResId":I
    :cond_6
    const v6, 0x7f0f00e4

    .line 135
    .restart local v6    # "titleResId":I
    const v9, 0x7f11000d

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v8

    invoke-virtual {v5, v9, p3, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "bodyString":Ljava/lang/String;
    goto :goto_2
.end method
