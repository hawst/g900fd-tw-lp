.class public interface abstract Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;
.super Ljava/lang/Object;
.source "DictionaryDownloadListener.java"


# virtual methods
.method public abstract downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;)V
.end method

.method public abstract finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
.end method

.method public abstract startingBatch(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract startingDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
.end method

.method public abstract updateDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;I)V
.end method
