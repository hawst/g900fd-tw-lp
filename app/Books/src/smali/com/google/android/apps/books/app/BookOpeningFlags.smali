.class public Lcom/google/android/apps/books/app/BookOpeningFlags;
.super Ljava/lang/Object;
.source "BookOpeningFlags.java"


# static fields
.field public static final FROM_LIBRARY_VIEW:Lcom/google/android/apps/books/app/BookOpeningFlags;

.field public static final FROM_RECOMMENDED_SAMPLE:Lcom/google/android/apps/books/app/BookOpeningFlags;

.field public static final FROM_SEARCH_SUGGESTION:Lcom/google/android/apps/books/app/BookOpeningFlags;


# instance fields
.field private final mAddOption:I

.field private final mOpenDisplaySettingsOnRecreate:Z

.field private final mUpdateVolumeOverview:Z

.field private final mWarnOnSample:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/books/app/BookOpeningFlags;

    invoke-direct {v0, v2, v2, v2, v2}, Lcom/google/android/apps/books/app/BookOpeningFlags;-><init>(ZZIZ)V

    sput-object v0, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_LIBRARY_VIEW:Lcom/google/android/apps/books/app/BookOpeningFlags;

    .line 29
    new-instance v0, Lcom/google/android/apps/books/app/BookOpeningFlags;

    invoke-direct {v0, v2, v3, v2, v2}, Lcom/google/android/apps/books/app/BookOpeningFlags;-><init>(ZZIZ)V

    sput-object v0, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_SEARCH_SUGGESTION:Lcom/google/android/apps/books/app/BookOpeningFlags;

    .line 34
    new-instance v0, Lcom/google/android/apps/books/app/BookOpeningFlags;

    const/4 v1, 0x2

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/google/android/apps/books/app/BookOpeningFlags;-><init>(ZZIZ)V

    sput-object v0, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_RECOMMENDED_SAMPLE:Lcom/google/android/apps/books/app/BookOpeningFlags;

    return-void
.end method

.method private constructor <init>(ZZIZ)V
    .locals 0
    .param p1, "updateVolumeOverview"    # Z
    .param p2, "warnOnSample"    # Z
    .param p3, "addOption"    # I
    .param p4, "openDisplaySettingsOnRecreate"    # Z

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mUpdateVolumeOverview:Z

    .line 59
    iput-boolean p2, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mWarnOnSample:Z

    .line 60
    iput p3, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mAddOption:I

    .line 61
    iput-boolean p4, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mOpenDisplaySettingsOnRecreate:Z

    .line 62
    return-void
.end method

.method public static fromIntent(Landroid/content/Intent;)Lcom/google/android/apps/books/app/BookOpeningFlags;
    .locals 9
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 38
    const-string v8, "books:internalIntent"

    invoke-virtual {p0, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_0

    move v4, v6

    .line 40
    .local v4, "updateVolumeOverview":Z
    :goto_0
    const-string v8, "books:addToMyEBooks"

    invoke-virtual {p0, v8, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 42
    .local v1, "addToMyEBooks":Z
    const-string v8, "books:warnOnSample"

    invoke-virtual {p0, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 44
    .local v5, "warnOnSample":Z
    const-string v8, "books:showDisplaySettings"

    invoke-virtual {p0, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 47
    .local v2, "openDisplaySettings":Z
    const-string v8, "books:promptBeforeAdding"

    invoke-virtual {p0, v8, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 49
    .local v3, "promptBeforeAdding":Z
    if-nez v1, :cond_1

    move v0, v7

    .line 52
    .local v0, "addOption":I
    :goto_1
    new-instance v6, Lcom/google/android/apps/books/app/BookOpeningFlags;

    invoke-direct {v6, v4, v5, v0, v2}, Lcom/google/android/apps/books/app/BookOpeningFlags;-><init>(ZZIZ)V

    return-object v6

    .end local v0    # "addOption":I
    .end local v1    # "addToMyEBooks":Z
    .end local v2    # "openDisplaySettings":Z
    .end local v3    # "promptBeforeAdding":Z
    .end local v4    # "updateVolumeOverview":Z
    .end local v5    # "warnOnSample":Z
    :cond_0
    move v4, v7

    .line 38
    goto :goto_0

    .line 49
    .restart local v1    # "addToMyEBooks":Z
    .restart local v2    # "openDisplaySettings":Z
    .restart local v3    # "promptBeforeAdding":Z
    .restart local v4    # "updateVolumeOverview":Z
    .restart local v5    # "warnOnSample":Z
    :cond_1
    if-eqz v3, :cond_2

    move v0, v6

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private getAddToMyEbooks()Z
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mAddOption:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPromptBeforeAdding()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 83
    iget v1, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mAddOption:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setArgs(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mUpdateVolumeOverview:Z

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setUpdateVolumeOverview(Landroid/os/Bundle;Z)V

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->getAddToMyEbooks()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setAddToMyEBooks(Landroid/os/Bundle;Z)V

    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->getPromptBeforeAdding()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setPromptBeforeAddingToMyEbooks(Landroid/os/Bundle;Z)V

    .line 78
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mWarnOnSample:Z

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setWarnOnSample(Landroid/os/Bundle;Z)V

    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mOpenDisplaySettingsOnRecreate:Z

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setShowDisplaySettings(Landroid/os/Bundle;Z)V

    .line 80
    return-void
.end method

.method public setExtras(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 65
    const-string v0, "books:addToMyEBooks"

    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->getAddToMyEbooks()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 66
    const-string v1, "books:internalIntent"

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mUpdateVolumeOverview:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    const-string v0, "books:warnOnSample"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mWarnOnSample:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const-string v0, "books:promptBeforeAdding"

    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->getPromptBeforeAdding()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    const-string v0, "books:showDisplaySettings"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/BookOpeningFlags;->mOpenDisplaySettingsOnRecreate:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 72
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
