.class public Lcom/google/android/apps/books/widget/BooksClusterContentLayout;
.super Lcom/google/android/apps/books/widget/CardRowLayout;
.source "BooksClusterContentLayout.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
.implements Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/CardRowLayout;",
        "Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;",
        "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController",
        "<",
        "Lcom/google/android/apps/books/playcards/BookDocument;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnimateUponLayout:Z

.field protected final mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

.field private mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private final mCurrentDisplayedDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

.field private mLayoutRequested:Z

.field private mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

.field private mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/CardRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    .line 56
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimateUponLayout:Z

    .line 58
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mLayoutRequested:Z

    .line 60
    new-instance v0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/CardRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    .line 56
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimateUponLayout:Z

    .line 58
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mLayoutRequested:Z

    .line 60
    new-instance v0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/BooksClusterContentLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksClusterContentLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->removeViewLocal(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/widget/BooksClusterContentLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksClusterContentLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimateUponLayout:Z

    return p1
.end method

.method private removeViewLocal(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 219
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/CardRowLayout;->removeView(Landroid/view/View;)V

    .line 220
    return-void
.end method

.method private setDocuments(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 119
    if-eqz p1, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 122
    :cond_0
    return-void
.end method


# virtual methods
.method public createContent()V
    .locals 4

    .prologue
    .line 144
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 145
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 146
    .local v0, "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/playcards/BookDocument;

    move-object v1, v3

    .line 148
    .local v1, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V

    .line 144
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 150
    .end local v0    # "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    :cond_1
    return-void
.end method

.method public inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 132
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 133
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v3

    .line 134
    .local v3, "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {p1, v4, v1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->getCard(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move-result-object v0

    .line 136
    .local v0, "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 137
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getFillStyle()Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 138
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->addView(Landroid/view/View;)V

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    .end local v0    # "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v3    # "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_0
    return-void
.end method

.method public isAnimationEnabled()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationEnabled:Z

    return v0
.end method

.method public maybeRunAnimation()V
    .locals 12

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->isAnimationEnabled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 181
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 182
    .local v0, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getChildCount()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 183
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 184
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_1

    .line 182
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    :cond_1
    const v8, 0x7f0e003b

    invoke-virtual {v1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 190
    .local v7, "volumeId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v8, v8, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mPreviousLocations:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;

    .line 192
    .local v4, "previousChildLocation":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    if-eqz v4, :cond_0

    .line 194
    iget-object v8, v4, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->location:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v9

    sub-int v3, v8, v9

    .line 195
    .local v3, "leftDelta":I
    if-eqz v3, :cond_0

    .line 196
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v8, v8, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v8, v1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v5

    .line 198
    .local v5, "target":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    int-to-float v8, v3

    invoke-interface {v5, v8}, Lcom/google/android/ublib/view/TranslationHelper$Translatable;->setTranslationX(F)V

    .line 199
    const-string v8, "translationX"

    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v11, v11, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v11, v1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationX(Landroid/view/View;)F

    move-result v11

    aput v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v9, v10

    invoke-static {v5, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 202
    .local v6, "translateAnimatorX":Landroid/animation/ObjectAnimator;
    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 203
    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 204
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 209
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "leftDelta":I
    .end local v4    # "previousChildLocation":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    .end local v5    # "target":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    .end local v6    # "translateAnimatorX":Landroid/animation/ObjectAnimator;
    .end local v7    # "volumeId":Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v8, v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->playAnimators(Ljava/util/List;)V

    .line 211
    .end local v0    # "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    .end local v2    # "i":I
    :cond_3
    return-void
.end method

.method public onAnimationEnd()V
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mLayoutRequested:Z

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimateUponLayout:Z

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mLayoutRequested:Z

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->requestLayout()V

    .line 97
    :cond_0
    return-void
.end method

.method public onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V
    .locals 7
    .param p2, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p3, "cardMetaData"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "newDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCurrentDisplayedDocs:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->onDocumentsChanged(Ljava/util/List;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->setDocuments(Ljava/util/List;)V

    .line 158
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/books/widget/CardRowLayout;->onLayout(ZIIII)V

    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimateUponLayout:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->maybeRunAnimation()V

    .line 88
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->recordLocations()V

    goto :goto_0

    .line 86
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mLayoutRequested:Z

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout$1;-><init>(Lcom/google/android/apps/books/widget/BooksClusterContentLayout;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->post(Ljava/lang/Runnable;)Z

    .line 232
    return-void
.end method

.method public setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "imageProviderFactory"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;
    .param p6, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p7, "bindingProvider"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .local p4, "clickCallback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    .line 108
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->setDocuments(Ljava/util/List;)V

    .line 109
    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 110
    iput-object p4, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 111
    iput-object p5, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

    .line 112
    iput-object p6, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mAnimationEnabled:Z

    .line 115
    return-void
.end method

.method public setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V
    .locals 6
    .param p1, "playCard"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "book"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p3, "addView"    # Z

    .prologue
    .line 162
    if-nez p2, :cond_1

    .line 163
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bindNoDocument()V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;->createImageProvider()Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 167
    new-instance v0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    invoke-direct {v0, v1, p2, v2}, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;-><init>(Landroid/content/Context;Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;)V

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v0, 0x7f0e003b

    invoke-virtual {p2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setTag(ILjava/lang/Object;)V

    .line 170
    if-eqz p3, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksClusterContentLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
