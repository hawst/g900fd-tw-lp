.class public final enum Lcom/google/android/apps/books/data/BooksDataController$Priority;
.super Ljava/lang/Enum;
.source "BooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BooksDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Priority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field public static final enum BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field public static final enum HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 186
    new-instance v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/data/BooksDataController$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .line 188
    new-instance v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/data/BooksDataController$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .line 184
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/data/BooksDataController$Priority;

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;->$VALUES:[Lcom/google/android/apps/books/data/BooksDataController$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 184
    const-class v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/apps/books/data/BooksDataController$Priority;->$VALUES:[Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/data/BooksDataController$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/data/BooksDataController$Priority;

    return-object v0
.end method
