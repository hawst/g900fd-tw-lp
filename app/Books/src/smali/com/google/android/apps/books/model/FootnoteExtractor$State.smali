.class final enum Lcom/google/android/apps/books/model/FootnoteExtractor$State;
.super Ljava/lang/Enum;
.source "FootnoteExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/FootnoteExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/model/FootnoteExtractor$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field public static final enum DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field public static final enum IN_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field public static final enum IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field public static final enum PAST_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

.field public static final enum START:Lcom/google/android/apps/books/model/FootnoteExtractor$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->START:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 63
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    const-string v1, "IN_ANCHOR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 64
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    const-string v1, "PAST_ANCHOR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->PAST_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 65
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    const-string v1, "IN_FOOTNOTE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 66
    new-instance v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/model/FootnoteExtractor$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->START:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->PAST_ANCHOR:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->IN_FOOTNOTE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->DONE:Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->$VALUES:[Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/FootnoteExtractor$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/model/FootnoteExtractor$State;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/books/model/FootnoteExtractor$State;->$VALUES:[Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/model/FootnoteExtractor$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/model/FootnoteExtractor$State;

    return-object v0
.end method
