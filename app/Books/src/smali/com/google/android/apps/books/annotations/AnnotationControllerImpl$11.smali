.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->deliverAnnotationDataResults(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$results:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;->val$results:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 3
    .param p1, "cache"    # Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    .prologue
    .line 478
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;->val$results:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 479
    .local v1, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadingAnnotationDatas:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$900(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 481
    .end local v1    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;->val$results:Ljava/util/Map;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->addDatas(Ljava/util/Map;)V

    .line 482
    return-void
.end method
