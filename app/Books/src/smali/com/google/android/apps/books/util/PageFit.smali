.class public Lcom/google/android/apps/books/util/PageFit;
.super Ljava/lang/Object;
.source "PageFit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/PageFit$1;
    }
.end annotation


# instance fields
.field private final mContentToDisplay:Landroid/graphics/Matrix;

.field private final mDisplayToContent:Landroid/graphics/Matrix;

.field private final mTempPoint:[F

.field private final mTempRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mDisplayToContent:Landroid/graphics/Matrix;

    .line 24
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mContentToDisplay:Landroid/graphics/Matrix;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mTempRect:Landroid/graphics/RectF;

    return-void
.end method

.method public static constructUnitSquareDisplayToContentMatrix(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Landroid/graphics/Matrix;
    .locals 10
    .param p0, "displayDims"    # Landroid/graphics/Point;
    .param p1, "contentDims"    # Landroid/graphics/Point;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 62
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 64
    .local v1, "displayToContent":Landroid/graphics/Matrix;
    iget v5, p1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget v6, p1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    div-float v0, v5, v6

    .line 66
    .local v0, "contentVerticalSkinniness":F
    iget v5, p0, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget v6, p0, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 68
    .local v2, "displayVerticalSkinniness":F
    cmpl-float v5, v0, v2

    if-lez v5, :cond_0

    .line 70
    div-float v4, v0, v2

    .line 72
    .local v4, "widthScale":F
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 73
    sget-object v5, Lcom/google/android/apps/books/util/PageFit$1;->$SwitchMap$com$google$android$apps$books$render$Renderer$PagePositionOnScreen:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 92
    .end local v4    # "widthScale":F
    :goto_0
    return-object v1

    .line 76
    .restart local v4    # "widthScale":F
    :pswitch_0
    sub-float v5, v7, v4

    div-float/2addr v5, v9

    invoke-virtual {v1, v5, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 80
    :pswitch_1
    sub-float v5, v7, v4

    invoke-virtual {v1, v5, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 86
    .end local v4    # "widthScale":F
    :cond_0
    div-float v3, v2, v0

    .line 88
    .local v3, "heightScale":F
    invoke-virtual {v1, v7, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 90
    sub-float v5, v7, v3

    div-float/2addr v5, v9

    invoke-virtual {v1, v8, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setDisplayToContent(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "displayToContent"    # Landroid/graphics/Matrix;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mDisplayToContent:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mDisplayToContent:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/util/PageFit;->mContentToDisplay:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 32
    return-void
.end method


# virtual methods
.method public compute(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 4
    .param p1, "displayDims"    # Landroid/graphics/Point;
    .param p2, "contentDims"    # Landroid/graphics/Point;
    .param p3, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 39
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/books/util/PageFit;->constructUnitSquareDisplayToContentMatrix(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 41
    .local v0, "displayToContent":Landroid/graphics/Matrix;
    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float v1, v3, v1

    iget v2, p1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float v2, v3, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 42
    iget v1, p2, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 43
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/PageFit;->setDisplayToContent(Landroid/graphics/Matrix;)V

    .line 44
    return-void
.end method

.method public getContentToDisplayMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "m"    # Landroid/graphics/Matrix;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mContentToDisplay:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 119
    return-void
.end method

.method public mapDisplayToContent(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    aput v1, v0, v2

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    aput v1, v0, v3

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mDisplayToContent:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    aget v0, v0, v2

    iget-object v1, p0, Lcom/google/android/apps/books/util/PageFit;->mTempPoint:[F

    aget v1, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 103
    return-void
.end method
