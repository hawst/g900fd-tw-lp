.class Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
.super Ljava/lang/Object;
.source "SearchResultsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SearchResultsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchResultRow"
.end annotation


# static fields
.field private static typeToLayoutMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mItemViewType:I

.field private final mLayoutId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    .line 46
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f04005c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f040063

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f040061

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f040060

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f040062

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method

.method private constructor <init>(II)V
    .locals 0
    .param p1, "itemViewType"    # I
    .param p2, "layoutId"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput p1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->mItemViewType:I

    .line 60
    iput p2, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->mLayoutId:I

    .line 61
    return-void
.end method

.method synthetic constructor <init>(IILcom/google/android/apps/books/app/SearchResultsController$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/apps/books/app/SearchResultsController$1;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;-><init>(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->getItemViewType()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->createNewView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Ljava/util/Map;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;

    return-object v0
.end method

.method private createNewView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->mLayoutId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getItemViewType()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->mItemViewType:I

    return v0
.end method


# virtual methods
.method public getSearchResultMatchStart()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method protected populateView(Landroid/view/View;)V
    .locals 0
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    .line 69
    return-void
.end method
