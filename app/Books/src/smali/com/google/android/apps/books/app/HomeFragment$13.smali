.class Lcom/google/android/apps/books/app/HomeFragment$13;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->setupReadNowHeader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$13;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>()V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 944
    const v0, 0x7f0400ae

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 945
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 964
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$13;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getContentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 965
    const v1, 0x7f040058

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 966
    .local v0, "homeProgress":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$13;->getHeaderHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 967
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 968
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 973
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 977
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$13;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$13;->getHeaderBottomMargin()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderSpacerId()I
    .locals 1

    .prologue
    .line 959
    const v0, 0x7f0e01ac

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 954
    const v0, 0x7f0e01dc

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 989
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 994
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 983
    const/4 v0, 0x0

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 949
    const/4 v0, 0x1

    return v0
.end method
