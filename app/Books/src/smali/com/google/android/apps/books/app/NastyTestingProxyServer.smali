.class public Lcom/google/android/apps/books/app/NastyTestingProxyServer;
.super Ljava/lang/Object;
.source "NastyTestingProxyServer.java"

# interfaces
.implements Lcom/google/android/apps/books/net/BooksServer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;
    }
.end annotation


# instance fields
.field private final mDelegate:Lcom/google/android/apps/books/net/BooksServer;

.field private final mDenyDownloadLicenseSubstring:Ljava/lang/String;

.field private final mEnableStubDictionaryMetadata:Z

.field private final mPretendOffersAcceptAlwaysSucceeds:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/net/BooksServer;Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;)V
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p2, "config"    # Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    .line 70
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_DENY_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;->getString(Lcom/google/android/apps/books/util/ConfigValue;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDenyDownloadLicenseSubstring:Ljava/lang/String;

    .line 71
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->PRETEND_OFFERS_ALWAYS_SUCCEED:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;->getBoolean(Lcom/google/android/apps/books/util/ConfigValue;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mPretendOffersAcceptAlwaysSucceeds:Z

    .line 73
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_STUB_DICTIONARY_METADATA:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;->getBoolean(Lcom/google/android/apps/books/util/ConfigValue;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mEnableStubDictionaryMetadata:Z

    .line 75
    return-void
.end method

.method private shouldDenyLicense(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDenyDownloadLicenseSubstring:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDenyDownloadLicenseSubstring:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static wrap(Lcom/google/android/apps/books/net/BooksServer;Landroid/content/Context;)Lcom/google/android/apps/books/net/BooksServer;
    .locals 2
    .param p0, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;

    new-instance v1, Lcom/google/android/apps/books/app/NastyTestingProxyServer$1;

    invoke-direct {v1, p1}, Lcom/google/android/apps/books/app/NastyTestingProxyServer$1;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/NastyTestingProxyServer;-><init>(Lcom/google/android/apps/books/net/BooksServer;Lcom/google/android/apps/books/app/NastyTestingProxyServer$Config;)V

    return-object v0
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;
    .param p3, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/app/DeviceInfo;",
            ")",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    .local p2, "volumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mPretendOffersAcceptAlwaysSucceeds:Z

    if-eqz v0, :cond_0

    .line 219
    new-instance v0, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    invoke-direct {v0}, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;-><init>()V

    .line 221
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public addCloudloadingVolume(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->addCloudloadingVolume(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;

    move-result-object v0

    return-object v0
.end method

.method public addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "reason"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/net/BooksServer;->addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V

    .line 108
    return-void
.end method

.method public deleteCloudloadedVolume(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->deleteCloudloadedVolume(Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method public dismissOffer(Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)V
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;
    .param p2, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/net/BooksServer;->dismissOffer(Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)V

    .line 213
    return-void
.end method

.method public getAudioResourceContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getAudioResourceContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/CcBox;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "data"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->getCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/CcBox;

    move-result-object v0

    return-object v0
.end method

.method public getCoverImage(Ljava/lang/String;I)Ljava/io/InputStream;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/net/BooksServer;->getCoverImage(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDictionaryMetadata(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "keyVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mEnableStubDictionaryMetadata:Z

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x1

    new-array v10, v0, [Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    const/4 v11, 0x0

    new-instance v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    const-string v1, "en"

    const-wide/16 v2, 0x0

    const-string v4, "showmethemeaning"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const-string v5, "https://www.gstatic.com/booksbe/dict/en_0.db"

    const-wide/32 v6, 0x2faf080

    const-string v8, "Session Key"

    const-string v9, "User Name"

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;-><init>(Ljava/lang/String;J[BLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v10, v11

    invoke-static {v10}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 231
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getDictionaryMetadata(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getFailedCloudeloadedVolumes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0}, Lcom/google/android/apps/books/net/BooksServer;->getFailedCloudeloadedVolumes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ">;)",
            "Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;

    move-result-object v0

    return-object v0
.end method

.method public getOffers(Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .locals 1
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getOffers(Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOffers;

    move-result-object v0

    return-object v0
.end method

.method public getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v0

    return-object v0
.end method

.method public getPageStructure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->getPageStructure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v0

    return-object v0
.end method

.method public getResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 167
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/books/net/BooksServer;->getResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;

    move-result-object v0

    return-object v0
.end method

.method public getSampleCategories(Ljava/util/Locale;)Lcom/google/android/apps/books/api/data/SampleCategories;
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getSampleCategories(Ljava/util/Locale;)Lcom/google/android/apps/books/api/data/SampleCategories;

    move-result-object v0

    return-object v0
.end method

.method public getSampleVolumes(Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/android/apps/books/api/data/SampleVolumes;
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .param p3, "pageSize"    # I
    .param p4, "nextPageToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/api/data/SampleVolumes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    .local p2, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/books/net/BooksServer;->getSampleVolumes(Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/android/apps/books/api/data/SampleVolumes;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .param p3, "segmentUrl"    # Ljava/lang/String;
    .param p5, "widthString"    # Ljava/lang/String;
    .param p6, "ignoreResources"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 82
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->shouldDenyLicense(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Testing proxy: simulating offline device limit reached for volumeId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1ca3

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/net/BooksServer;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;

    move-result-object v0

    return-object v0
.end method

.method public getSharedFontContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getSharedFontContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getUserSettings()Lcom/google/android/apps/books/api/data/UserSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0}, Lcom/google/android/apps/books/net/BooksServer;->getUserSettings()Lcom/google/android/apps/books/api/data/UserSettings;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeManifest(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getVolumeManifest(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;

    move-result-object v0

    return-object v0
.end method

.method public releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/net/BooksServer;->releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V

    .line 155
    return-void
.end method

.method public removeVolumeFromMyEbooks(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->removeVolumeFromMyEbooks(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public requestVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v1, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->requestVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    move-result-object v0

    .line 98
    .local v0, "response":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->shouldDenyLicense(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, v0, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->deviceAllowed:Z

    .line 101
    :cond_0
    return-object v0
.end method

.method public syncVolumeLicenses(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/android/apps/books/model/SessionKey;)Ljava/util/List;
    .locals 1
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "volumeIdsToAcquire":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "volumeIdsWithLicenseAcquired":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksServer;->syncVolumeLicenses(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/android/apps/books/model/SessionKey;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;)Lcom/google/android/apps/books/api/data/UserSettings;
    .locals 1
    .param p1, "newUserSettings"    # Lcom/google/android/apps/books/api/data/UserSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->mDelegate:Lcom/google/android/apps/books/net/BooksServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksServer;->updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;)Lcom/google/android/apps/books/api/data/UserSettings;

    move-result-object v0

    return-object v0
.end method
