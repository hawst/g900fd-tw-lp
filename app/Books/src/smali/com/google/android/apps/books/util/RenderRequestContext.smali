.class public Lcom/google/android/apps/books/util/RenderRequestContext;
.super Ljava/lang/Object;
.source "RenderRequestContext.java"


# instance fields
.field public final basePosition:Lcom/google/android/apps/books/common/Position;

.field public final offsetFromBasePage:I

.field public final sequenceNumber:I


# direct methods
.method public constructor <init>(ILcom/google/android/apps/books/common/Position;I)V
    .locals 0
    .param p1, "sequenceNumber"    # I
    .param p2, "basePosition"    # Lcom/google/android/apps/books/common/Position;
    .param p3, "devicePage"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->basePosition:Lcom/google/android/apps/books/common/Position;

    .line 35
    iput p1, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->sequenceNumber:I

    .line 36
    iput p3, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    .line 37
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "sequenceNumber"

    iget v2, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->sequenceNumber:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "basePosition"

    iget-object v2, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->basePosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "offsetFromBasePage"

    iget v2, p0, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
