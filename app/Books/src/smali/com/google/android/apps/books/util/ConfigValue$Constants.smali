.class public Lcom/google/android/apps/books/util/ConfigValue$Constants;
.super Ljava/lang/Object;
.source "ConfigValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/ConfigValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Constants"
.end annotation


# static fields
.field public static JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

.field public static JS_DEBUG_CONFIG_VALUE:Ljava/lang/String;

.field public static JS_SIDELOADED_CONFIG_VALUE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    const-string v0, "compiled"

    sput-object v0, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

    .line 182
    const-string v0, "debug"

    sput-object v0, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_DEBUG_CONFIG_VALUE:Ljava/lang/String;

    .line 183
    const-string v0, "sideloaded"

    sput-object v0, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_SIDELOADED_CONFIG_VALUE:Ljava/lang/String;

    return-void
.end method
