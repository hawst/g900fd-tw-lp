.class Lcom/google/android/apps/books/data/UploadsControllerImpl$3;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$fd:Landroid/os/ParcelFileDescriptor;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$upload:Lcom/google/android/apps/books/upload/Upload;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$fd:Landroid/os/ParcelFileDescriptor;

    iput-object p3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$id:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 136
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$fd:Landroid/os/ParcelFileDescriptor;

    iget-object v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$fileName:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$id:Ljava/lang/String;

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/apps/books/upload/UploadDataStorage;->copyToTempDir(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 137
    .local v1, "fileSize":I
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v5}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadFilesize(Ljava/lang/String;I)V

    .line 140
    new-instance v2, Lcom/google/android/apps/books/upload/Upload;

    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v2, v4}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    .line 141
    .local v2, "uploadCopy":Lcom/google/android/apps/books/upload/Upload;
    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/upload/Upload;->setFileSize(I)V

    .line 142
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadManager:Lcom/google/android/apps/books/upload/UploadManager;
    invoke-static {v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$100(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadManager;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v6}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v6

    invoke-interface {v4, v5, v2, v6}, Lcom/google/android/apps/books/upload/UploadManager;->getUploader(Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;Lcom/google/android/apps/books/upload/Upload;Lcom/google/android/apps/books/upload/UploadDataStorage;)Lcom/google/android/apps/books/upload/SingleBookUploader;

    move-result-object v3

    .line 144
    .local v3, "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    invoke-interface {v3}, Lcom/google/android/apps/books/upload/SingleBookUploader;->startUpload()V

    .line 145
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$id:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 162
    .end local v1    # "fileSize":I
    .end local v2    # "uploadCopy":Lcom/google/android/apps/books/upload/Upload;
    .end local v3    # "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$400(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/concurrent/Executor;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/data/UploadsControllerImpl$3$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$3$1;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$3;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 158
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->deleteUpload(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    .end local v0    # "e":Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
    :catch_1
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;->val$id:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v0, v6}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->abortUpload(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method
