.class public Lcom/google/android/apps/books/widget/HighlightsSharingColor;
.super Ljava/lang/Object;
.source "HighlightsSharingColor.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Assignable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Assignable",
        "<",
        "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
        ">;"
    }
.end annotation


# instance fields
.field public color:I

.field public highlightRects:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field public isSelected:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public assign(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    .locals 3
    .param p1, "newHighlights"    # Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .prologue
    .line 16
    iget v0, p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->color:I

    iget-boolean v1, p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->isSelected:Z

    iget-object v2, p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->highlightRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic assign(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 9
    check-cast p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->assign(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move-result-object v0

    return-object v0
.end method

.method public set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    .locals 0
    .param p1, "color"    # I
    .param p2, "isSelected"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;"
        }
    .end annotation

    .prologue
    .line 21
    .local p3, "highlightRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/LabeledRect;>;"
    iput p1, p0, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->color:I

    .line 22
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->isSelected:Z

    .line 23
    iput-object p3, p0, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->highlightRects:Lcom/google/android/apps/books/widget/Walker;

    .line 24
    return-object p0
.end method
