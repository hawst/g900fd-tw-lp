.class public Lcom/google/android/apps/books/app/SortFragment$Arguments;
.super Ljava/lang/Object;
.source "SortFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SortFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Arguments"
.end annotation


# direct methods
.method static synthetic access$000(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1
    .param p0, "x0"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-static {p0}, Lcom/google/android/apps/books/app/SortFragment$Arguments;->getSortOrder(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lcom/google/android/apps/books/app/LibraryComparator;)Landroid/os/Bundle;
    .locals 3
    .param p0, "currentOrder"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 37
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "result":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 39
    const-string v1, "currentOrder"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LibraryComparator;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :cond_0
    return-object v0
.end method

.method private static getSortOrder(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 45
    const-string v0, "currentOrder"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/LibraryComparator;->fromString(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v0

    return-object v0
.end method
