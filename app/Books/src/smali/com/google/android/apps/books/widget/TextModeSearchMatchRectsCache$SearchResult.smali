.class public Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;
.super Ljava/lang/Object;
.source "TextModeSearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SearchResult"
.end annotation


# instance fields
.field final exactMatch:Z

.field final indices:Lcom/google/android/apps/books/render/PageIndices;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;Lcom/google/android/apps/books/render/PageIndices;Z)V
    .locals 0
    .param p2, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p3, "exactMatch"    # Z

    .prologue
    .line 522
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->this$0:Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523
    iput-object p2, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->indices:Lcom/google/android/apps/books/render/PageIndices;

    .line 524
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->exactMatch:Z

    .line 525
    return-void
.end method


# virtual methods
.method public matchExists()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->indices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 532
    invoke-static {p0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "indices"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->indices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "exact match"

    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$SearchResult;->exactMatch:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
