.class public Lcom/google/android/apps/books/util/MediaClipsLoader;
.super Ljava/lang/Object;
.source "MediaClipsLoader.java"


# direct methods
.method public static parseSmil(Ljava/io/InputStream;Lcom/google/android/apps/books/util/MediaClips;)V
    .locals 7
    .param p0, "resourceInputStream"    # Ljava/io/InputStream;
    .param p1, "mediaClips"    # Lcom/google/android/apps/books/util/MediaClips;

    .prologue
    const/4 v5, 0x6

    .line 57
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;->newPullParserLocked()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 58
    .local v1, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v4, 0x0

    invoke-interface {v1, p0, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 60
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .local v3, "type":I
    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 61
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 62
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "tag":Ljava/lang/String;
    const-string v4, "par"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 64
    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/MediaClipsLoader;->parseSmilPar(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 70
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "tag":Ljava/lang/String;
    .end local v3    # "type":I
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v4, "MediaClipLoader"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    const-string v4, "MediaClipLoader"

    const-string v5, "Unable to build smil parser"

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_1
    :goto_1
    return-void

    .line 65
    .restart local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v2    # "tag":Ljava/lang/String;
    .restart local v3    # "type":I
    :cond_2
    :try_start_1
    const-string v4, "seq"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/MediaClipsLoader;->parseSmilSeq(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 74
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "tag":Ljava/lang/String;
    .end local v3    # "type":I
    :catch_1
    move-exception v0

    .line 75
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "MediaClipLoader"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 76
    const-string v4, "MediaClipLoader"

    const-string v5, "Unable to parse smil"

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 78
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 79
    .local v0, "e":Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;
    const-string v4, "MediaClipLoader"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 80
    const-string v4, "MediaClipLoader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "skipping dupe smil for segment "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static parseSmilPar(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V
    .locals 21
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "mediaClips"    # Lcom/google/android/apps/books/util/MediaClips;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v4, 0x0

    .line 94
    .local v4, "segmentId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 95
    .local v5, "readingPosition":Ljava/lang/String;
    const/4 v6, 0x0

    .line 96
    .local v6, "elementId":Ljava/lang/String;
    const/4 v10, 0x0

    .line 98
    .local v10, "audioResourceId":Ljava/lang/String;
    const/4 v7, 0x0

    .line 99
    .local v7, "audioResource":Lcom/google/android/apps/books/model/Resource;
    const/4 v8, 0x0

    .line 100
    .local v8, "clipBeginMs":I
    const v9, 0x7fffffff

    .line 103
    .local v9, "clipEndMs":I
    :cond_0
    :goto_0
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .local v17, "type":I
    const/16 v19, 0x3

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    const-string v19, "par"

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_c

    .line 104
    :cond_1
    const/16 v19, 0x2

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 105
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 106
    .local v16, "tag":Ljava/lang/String;
    const-string v19, "text"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 107
    const/16 v19, 0x0

    const-string v20, "src"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 108
    .local v14, "src":Ljava/lang/String;
    const/16 v19, 0x0

    const-string v20, "gbs_pos"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 109
    .local v13, "pos":Ljava/lang/String;
    if-nez v14, :cond_3

    .line 110
    const-string v19, "MediaClipLoader"

    const/16 v20, 0x6

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 111
    const-string v19, "MediaClipLoader"

    const-string v20, "malformed smil par.text: no src"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    .end local v13    # "pos":Ljava/lang/String;
    .end local v14    # "src":Ljava/lang/String;
    .end local v16    # "tag":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 115
    .restart local v13    # "pos":Ljava/lang/String;
    .restart local v14    # "src":Ljava/lang/String;
    .restart local v16    # "tag":Ljava/lang/String;
    :cond_3
    const-string v19, "#"

    const/16 v20, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v15

    .line 116
    .local v15, "srcSplit":[Ljava/lang/String;
    if-eqz v15, :cond_6

    array-length v0, v15

    move/from16 v19, v0

    if-lez v19, :cond_6

    .line 117
    const/16 v19, 0x0

    aget-object v4, v15, v19

    .line 118
    array-length v0, v15

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    .line 119
    const/16 v19, 0x1

    aget-object v6, v15, v19

    .line 124
    :cond_4
    :goto_2
    if-nez v13, :cond_7

    .line 125
    const-string v19, "MediaClipLoader"

    const/16 v20, 0x5

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 126
    const-string v19, "MediaClipLoader"

    const-string v20, "malformed smil par.text: no reading position"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_5
    const-string v5, ""

    goto/16 :goto_0

    .line 122
    :cond_6
    move-object v4, v14

    goto :goto_2

    .line 130
    :cond_7
    move-object v5, v13

    goto/16 :goto_0

    .line 132
    .end local v13    # "pos":Ljava/lang/String;
    .end local v14    # "src":Ljava/lang/String;
    .end local v15    # "srcSplit":[Ljava/lang/String;
    :cond_8
    const-string v19, "audio"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 133
    const/16 v19, 0x0

    const-string v20, "src"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 134
    .restart local v14    # "src":Ljava/lang/String;
    const/16 v19, 0x0

    const-string v20, "clipBegin"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 136
    .local v11, "clipBegin":Ljava/lang/String;
    const/16 v19, 0x0

    const-string v20, "clipEnd"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 137
    .local v12, "clipEnd":Ljava/lang/String;
    if-nez v14, :cond_9

    .line 138
    const-string v19, "MediaClipLoader"

    const/16 v20, 0x6

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 139
    const-string v19, "MediaClipLoader"

    const-string v20, "malformed smil par.audio"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 144
    :cond_9
    const-string v19, "&amp;"

    const-string v20, "&"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 145
    .local v18, "url":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/provider/BooksContract$Files;->urlToResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 148
    if-nez v11, :cond_a

    .line 149
    :goto_3
    if-nez v12, :cond_b

    .line 150
    :goto_4
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableResource;->builder()Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v19

    const-string v20, "audio"

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v7

    goto/16 :goto_0

    .line 148
    :cond_a
    invoke-static {v11}, Lcom/google/android/apps/books/util/MediaClipsLoader;->smilTimeToMs(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 149
    :cond_b
    invoke-static {v12}, Lcom/google/android/apps/books/util/MediaClipsLoader;->smilTimeToMs(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 156
    .end local v11    # "clipBegin":Ljava/lang/String;
    .end local v12    # "clipEnd":Ljava/lang/String;
    .end local v14    # "src":Ljava/lang/String;
    .end local v16    # "tag":Ljava/lang/String;
    .end local v18    # "url":Ljava/lang/String;
    :cond_c
    new-instance v3, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;II)V

    .line 159
    .local v3, "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/util/MediaClips;->addClip(Lcom/google/android/apps/books/util/MediaClips$MediaClip;)V

    goto/16 :goto_1
.end method

.method private static parseSmilSeq(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "mediaClips"    # Lcom/google/android/apps/books/util/MediaClips;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;
        }
    .end annotation

    .prologue
    .line 223
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .local v1, "type":I
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    const-string v2, "seq"

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 224
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 225
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "tag":Ljava/lang/String;
    const-string v2, "par"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 227
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/MediaClipsLoader;->parseSmilPar(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V

    goto :goto_0

    .line 228
    :cond_2
    const-string v2, "seq"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/MediaClipsLoader;->parseSmilSeq(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/util/MediaClips;)V

    goto :goto_0

    .line 233
    .end local v0    # "tag":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public static smilTimeToMs(Ljava/lang/String;)I
    .locals 12
    .param p0, "smilTime"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/high16 v9, 0x447a0000    # 1000.0f

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 168
    if-nez p0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v4

    .line 171
    :cond_1
    const/4 v4, 0x0

    .line 172
    .local v4, "resultInMs":I
    const-string v6, "\\s+"

    const-string v7, ""

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 173
    .local v5, "smilTimeNoSpaces":Ljava/lang/String;
    const-string v6, ":"

    invoke-virtual {v5, v6, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "colonSplit":[Ljava/lang/String;
    :try_start_0
    array-length v6, v0

    if-ne v6, v8, :cond_6

    .line 178
    const/16 v3, 0x3e8

    .line 180
    .local v3, "multiplier":I
    move-object v2, v5

    .line 181
    .local v2, "fraction":Ljava/lang/String;
    const-string v6, "h"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 182
    const v3, 0x36ee80

    .line 183
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 193
    :cond_2
    :goto_1
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    int-to-float v7, v3

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 194
    goto :goto_0

    .line 184
    :cond_3
    const-string v6, "min"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 185
    const v3, 0xea60

    .line 186
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x3

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 187
    :cond_4
    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 188
    const/4 v3, 0x1

    .line 189
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 190
    :cond_5
    const-string v6, "s"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 191
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 194
    .end local v2    # "fraction":Ljava/lang/String;
    .end local v3    # "multiplier":I
    :cond_6
    array-length v6, v0

    if-ne v6, v10, :cond_7

    .line 197
    const/4 v6, 0x0

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const v7, 0xea60

    mul-int/2addr v6, v7

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    mul-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    add-int v4, v6, v7

    goto/16 :goto_0

    .line 202
    :cond_7
    const/4 v6, 0x0

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const v7, 0x36ee80

    mul-int/2addr v6, v7

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const v8, 0xea60

    mul-int/2addr v7, v8

    add-int/2addr v6, v7

    const/4 v7, 0x2

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    mul-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    add-int v4, v6, v7

    goto/16 :goto_0

    .line 206
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v6, "MediaClipLoader"

    invoke-static {v6, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 208
    const-string v6, "MediaClipLoader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad clip time value: \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
