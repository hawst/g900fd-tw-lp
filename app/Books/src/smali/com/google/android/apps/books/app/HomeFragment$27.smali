.class Lcom/google/android/apps/books/app/HomeFragment$27;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageManager$Ensurer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->getCoverLoadEnsurer(Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageManager$Ensurer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$dc:Lcom/google/android/apps/books/data/BooksDataController;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0

    .prologue
    .line 2392
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$27;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$27;->val$dc:Lcom/google/android/apps/books/data/BooksDataController;

    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeFragment$27;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ensure()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2396
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$27;->val$dc:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$27;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403
    return-void

    .line 2397
    :catch_0
    move-exception v0

    .line 2398
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "HomeFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2399
    const-string v1, "HomeFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error loading cover for home fragment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2401
    :cond_0
    invoke-static {v0}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v1

    throw v1
.end method
