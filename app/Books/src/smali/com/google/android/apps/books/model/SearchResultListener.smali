.class public interface abstract Lcom/google/android/apps/books/model/SearchResultListener;
.super Ljava/lang/Object;
.source "SearchResultListener.java"


# virtual methods
.method public abstract onBookDone()V
.end method

.method public abstract onChapterDone()Z
.end method

.method public abstract onChapterStart(Ljava/lang/String;)Z
.end method

.method public abstract onError(Ljava/lang/Throwable;)V
.end method

.method public abstract onFound(Lcom/google/android/apps/books/model/SearchResult;)V
.end method
