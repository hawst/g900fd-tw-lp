.class final Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;
.super Landroid/os/AsyncTask;
.source "BaseBooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SaveHasShownUnsupportedCountry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 990
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.SaveHasShownUnsupportedCountry;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/BaseBooksActivity$1;

    .prologue
    .line 990
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.SaveHasShownUnsupportedCountry;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 990
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.SaveHasShownUnsupportedCountry;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 993
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.SaveHasShownUnsupportedCountry;"
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 994
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHasShownUnsupportedCountryWarning(Z)V

    .line 995
    const/4 v1, 0x0

    return-object v1
.end method
