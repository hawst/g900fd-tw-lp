.class public Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "LocalDictionaryCheckableItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/AppSettingsCheckableItem;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsChecked:Z

.field private final mLanguage:Ljava/lang/String;

.field private final mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

.field private final mSizeInMB:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Z)V
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "isChecked"    # Z

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(I)V

    .line 37
    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/dictionary/LocalDictionaryUtils;->getDisplayLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mLanguage:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v2

    long-to-float v1, v2

    const/high16 v2, 0x49800000    # 1048576.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mSizeInMB:Ljava/lang/String;

    .line 39
    iput-boolean p2, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mIsChecked:Z

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 41
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;)I
    .locals 2
    .param p1, "o"    # Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mLanguage:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->compareTo(Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;)I

    move-result v0

    return v0
.end method

.method public getMetadata()Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    return-object v0
.end method

.method public getValue()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mIsChecked:Z

    return v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 56
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 57
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040027

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 58
    .local v5, "view":Landroid/view/View;
    const v7, 0x7f0e00af

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 59
    .local v3, "tv":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mLanguage:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const v7, 0x7f0e00b0

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 62
    .local v4, "tv_summary":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mSizeInMB:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const v7, 0x7f040026

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    .local v1, "checkboxView":Landroid/view/View;
    const v7, 0x7f0e00b1

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 66
    .local v6, "widgetSpot":Landroid/widget/LinearLayout;
    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 67
    const v7, 0x7f0e00ae

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 69
    .local v0, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->getValue()Z

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 70
    return-object v5
.end method

.method public setValue(Z)V
    .locals 0
    .param p1, "isChecked"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->mIsChecked:Z

    .line 53
    return-void
.end method
