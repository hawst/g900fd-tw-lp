.class public final enum Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
.super Ljava/lang/Enum;
.source "OceanApiaryUrls.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/OceanApiaryUrls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field static final DEFAULT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum NEXT_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum PREV_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum SEARCH_WITHIN_BOOK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum SELECT_CHAPTER:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field public static final enum TRANSIENT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;


# instance fields
.field private final mApiarySetPositionActionName:Ljava/lang/String;

.field private final mOceanName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 240
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "CHOSE_BOOKMARK"

    const-string v2, "ChoseBookmark"

    const-string v3, "bookmark"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 241
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "NEXT_PAGE"

    const-string v2, "NextPage"

    const-string v3, "next-page"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->NEXT_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 242
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "PREV_PAGE"

    const-string v2, "PrevPage"

    const-string v3, "prev-page"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->PREV_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 243
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "SCROLL_TO_PAGE"

    const-string v2, "ScrollToPage"

    const-string v3, "scroll"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 244
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "SEARCH_WITHIN_BOOK"

    const-string v2, "SearchWithinBook"

    const-string v3, "search"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SEARCH_WITHIN_BOOK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 245
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "SELECT_CHAPTER"

    const/4 v2, 0x5

    const-string v3, "SelectChapter"

    const-string v4, "chapter"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SELECT_CHAPTER:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 251
    new-instance v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    const-string v1, "TRANSIENT"

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->TRANSIENT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 238
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->NEXT_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->PREV_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SEARCH_WITHIN_BOOK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SELECT_CHAPTER:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->TRANSIENT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->$VALUES:[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 279
    sget-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->NEXT_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    sput-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->DEFAULT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "oceanName"    # Ljava/lang/String;
    .param p4, "apiaryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 257
    iput-object p3, p0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->mOceanName:Ljava/lang/String;

    .line 258
    iput-object p4, p0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->mApiarySetPositionActionName:Ljava/lang/String;

    .line 259
    return-void
.end method

.method public static actionOrDefault(Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .prologue
    .line 305
    if-eqz p0, :cond_0

    .line 311
    .end local p0    # "action":Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    :goto_0
    return-object p0

    .line 308
    .restart local p0    # "action":Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    :cond_0
    const-string v0, "ApiaryUrlsCreated"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    const-string v0, "ApiaryUrlsCreated"

    const-string v1, "Null action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_1
    sget-object p0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->DEFAULT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    goto :goto_0
.end method

.method public static actionWithOceanNameOrDefault(Ljava/lang/String;)Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .locals 7
    .param p0, "oceanName"    # Ljava/lang/String;

    .prologue
    .line 286
    if-nez p0, :cond_1

    .line 287
    sget-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->DEFAULT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 297
    :cond_0
    :goto_0
    return-object v0

    .line 289
    :cond_1
    invoke-static {}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->values()[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    move-result-object v1

    .local v1, "arr$":[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v0, v1, v2

    .line 290
    .local v0, "action":Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    invoke-virtual {v0}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->getOceanName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 289
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 294
    .end local v0    # "action":Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    :cond_2
    const-string v4, "ApiaryUrlsCreated"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 295
    const-string v4, "ApiaryUrlsCreated"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad action ocean name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_3
    sget-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->DEFAULT:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 238
    const-class v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->$VALUES:[Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    return-object v0
.end method


# virtual methods
.method public getApiarySetPositionActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->mApiarySetPositionActionName:Ljava/lang/String;

    return-object v0
.end method

.method public getOceanName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->mOceanName:Ljava/lang/String;

    return-object v0
.end method
