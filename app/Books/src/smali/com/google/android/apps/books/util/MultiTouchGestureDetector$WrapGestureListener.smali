.class Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;
.super Ljava/lang/Object;
.source "MultiTouchGestureDetector.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MultiTouchGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WrapGestureListener"
.end annotation


# instance fields
.field final mListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;->mListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .line 37
    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;->mListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    check-cast p1, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .end local p1    # "detector":Landroid/view/ScaleGestureDetector;
    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;->onGesture(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)Z

    move-result v0

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;->mListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    check-cast p1, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .end local p1    # "detector":Landroid/view/ScaleGestureDetector;
    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;->onGestureBegin(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)Z

    move-result v0

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;->mListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    check-cast p1, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .end local p1    # "detector":Landroid/view/ScaleGestureDetector;
    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;->onGestureEnd(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)V

    .line 52
    return-void
.end method
