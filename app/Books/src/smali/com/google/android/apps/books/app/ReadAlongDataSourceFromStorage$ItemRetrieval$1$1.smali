.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

.field final synthetic val$content:Lcom/google/android/apps/books/util/ExceptionOr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->val$content:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->access$100(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;)Landroid/util/SparseArray;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iget v1, v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->val$thisSegmentIndexInPassage:I

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->val$content:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->access$100(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iget v1, v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->val$numSegments:I

    if-ne v0, v1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;->this$2:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    # invokes: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->publishResult()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->access$200(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;)V

    .line 131
    :cond_0
    return-void
.end method
