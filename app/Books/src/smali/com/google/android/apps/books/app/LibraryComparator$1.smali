.class final enum Lcom/google/android/apps/books/app/LibraryComparator$1;
.super Lcom/google/android/apps/books/app/LibraryComparator;
.source "LibraryComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LibraryComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V
    .locals 6
    .param p3, "x0"    # I
    .param p4, "x1"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .prologue
    .line 14
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/LibraryComparator;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;Lcom/google/android/apps/books/app/LibraryComparator$1;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 10
    .param p1, "lhs"    # Lcom/google/android/apps/books/widget/CardData;
    .param p2, "rhs"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 22
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-nez v4, :cond_1

    .line 29
    :cond_0
    :goto_0
    return v2

    .line 25
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 26
    goto :goto_0

    .line 28
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/CardData;->getLastInteraction()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getLastInteraction()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 29
    .local v0, "result":J
    cmp-long v4, v0, v8

    if-ltz v4, :cond_0

    cmp-long v2, v0, v8

    if-lez v2, :cond_3

    move v2, v3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/apps/books/widget/CardData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/widget/CardData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/LibraryComparator$1;->compare(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    return v0
.end method
