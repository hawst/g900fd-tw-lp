.class Lcom/google/android/apps/books/app/ReaderMenuImpl$6;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;->showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

.field final synthetic val$hasFilteredResults:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Z)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->val$hasFilteredResults:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 544
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->val$hasFilteredResults:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->shouldShowRecentSearchesPopup()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$800(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$100(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$900(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/support/v7/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$100(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$900(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/support/v7/widget/SearchView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/ViewUtils;->dropDownFromAnchor(Landroid/widget/PopupWindow;Landroid/view/View;Z)V

    .line 550
    :goto_0
    return-void

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->dismissRecentSearchesPopup()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$400(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    goto :goto_0
.end method
