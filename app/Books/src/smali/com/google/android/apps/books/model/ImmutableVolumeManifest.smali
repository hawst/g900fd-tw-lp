.class public Lcom/google/android/apps/books/model/ImmutableVolumeManifest;
.super Ljava/lang/Object;
.source "ImmutableVolumeManifest.java"

# interfaces
.implements Lcom/google/android/apps/books/model/VolumeManifest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    }
.end annotation


# instance fields
.field private final mChapters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentVersion:Ljava/lang/String;

.field private final mFirstChapterStartSegmentIndex:I

.field private final mHasImageMode:Z

.field private final mHasMediaOverlays:Z

.field private final mHasTextMode:Z

.field private final mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

.field private final mIsRightToLeft:Z

.field private final mLanguage:Ljava/lang/String;

.field private final mMediaOverlayActiveClass:Ljava/lang/String;

.field private final mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private final mRenditionOrientation:Ljava/lang/String;

.field private final mRenditionSpread:Ljava/lang/String;

.field private final mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegmentResources:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZLcom/google/android/apps/books/model/VolumeManifest$Mode;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;Lcom/google/android/apps/books/util/IdentifiableCollection;Ljava/util/Collection;)V
    .locals 2
    .param p1, "contentVersion"    # Ljava/lang/String;
    .param p2, "hasTextMode"    # Z
    .param p3, "hasImageMode"    # Z
    .param p4, "preferredMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p5, "firstChapterStartSegmentIndex"    # I
    .param p6, "isRightToLeft"    # Z
    .param p7, "hasMediaOverlays"    # Z
    .param p8, "mediaOverlayActiveClass"    # Ljava/lang/String;
    .param p9, "language"    # Ljava/lang/String;
    .param p10, "orientation"    # Ljava/lang/String;
    .param p11, "spread"    # Ljava/lang/String;
    .param p14, "imageModePositions"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .param p15, "textModePositions"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/google/android/apps/books/model/VolumeManifest$Mode;",
            "IZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;",
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p12, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    .local p13, "segments":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<Lcom/google/android/apps/books/model/Segment;>;"
    .local p16, "pages":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<Lcom/google/android/apps/books/model/Page;>;"
    .local p17, "resources":Lcom/google/android/apps/books/util/IdentifiableCollection;, "Lcom/google/android/apps/books/util/IdentifiableCollection<Lcom/google/android/apps/books/model/Resource;>;"
    .local p18, "segmentResources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SegmentResource;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mContentVersion:Ljava/lang/String;

    .line 44
    iput-boolean p2, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasTextMode:Z

    .line 45
    iput-boolean p3, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasImageMode:Z

    .line 46
    iput-object p4, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 47
    iput p5, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mFirstChapterStartSegmentIndex:I

    .line 48
    iput-boolean p6, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mIsRightToLeft:Z

    .line 49
    iput-boolean p7, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasMediaOverlays:Z

    .line 50
    iput-object p8, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mMediaOverlayActiveClass:Ljava/lang/String;

    .line 51
    iput-object p9, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mLanguage:Ljava/lang/String;

    .line 52
    iput-object p10, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mRenditionOrientation:Ljava/lang/String;

    .line 53
    iput-object p11, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mRenditionSpread:Ljava/lang/String;

    .line 54
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .line 55
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .line 58
    invoke-static {p12}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mChapters:Ljava/util/List;

    .line 59
    iput-object p13, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 60
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 61
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;

    .line 62
    invoke-static/range {p18 .. p18}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mSegmentResources:Ljava/util/Collection;

    .line 63
    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getChapters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mChapters:Ljava/util/List;

    return-object v0
.end method

.method public getContentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mContentVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstChapterStartSegmentIndex()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mFirstChapterStartSegmentIndex:I

    return v0
.end method

.method public getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaOverlayActiveClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mMediaOverlayActiveClass:Ljava/lang/String;

    return-object v0
.end method

.method public getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    return-object v0
.end method

.method public getPreferredMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method

.method public getRenditionOrientation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mRenditionOrientation:Ljava/lang/String;

    return-object v0
.end method

.method public getRenditionSpread()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mRenditionSpread:Ljava/lang/String;

    return-object v0
.end method

.method public getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;

    return-object v0
.end method

.method public getSegmentResources()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mSegmentResources:Ljava/util/Collection;

    return-object v0
.end method

.method public getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    return-object v0
.end method

.method public getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    return-object v0
.end method

.method public hasImageMode()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasImageMode:Z

    return v0
.end method

.method public hasMediaOverlays()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasMediaOverlays:Z

    return v0
.end method

.method public hasTextMode()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mHasTextMode:Z

    return v0
.end method

.method public isRightToLeft()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->mIsRightToLeft:Z

    return v0
.end method
