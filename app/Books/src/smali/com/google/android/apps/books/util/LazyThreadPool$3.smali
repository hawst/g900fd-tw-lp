.class Lcom/google/android/apps/books/util/LazyThreadPool$3;
.super Ljava/lang/Object;
.source "LazyThreadPool.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/LazyThreadPool;->onTaskFinished()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/LazyThreadPool;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/LazyThreadPool;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    # operator-- for: Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I
    invoke-static {v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->access$110(Lcom/google/android/apps/books/util/LazyThreadPool;)I

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    # getter for: Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->access$200(Lcom/google/android/apps/books/util/LazyThreadPool;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v0

    const-string v1, "LazyThreadPool"

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    # getter for: Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->access$200(Lcom/google/android/apps/books/util/LazyThreadPool;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v0

    const-string v1, "LazyThreadPool"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Task completed; # workers = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    # getter for: Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I
    invoke-static {v3}, Lcom/google/android/apps/books/util/LazyThreadPool;->access$100(Lcom/google/android/apps/books/util/LazyThreadPool;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool$3;->this$0:Lcom/google/android/apps/books/util/LazyThreadPool;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->tickleOnControlExecutor()V

    .line 102
    return-void
.end method
