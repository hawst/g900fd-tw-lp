.class public Lcom/google/android/apps/books/util/IntOrFloat;
.super Ljava/lang/Object;
.source "IntOrFloat.java"


# direct methods
.method public static toFloat(I)F
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 40
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    .line 41
    shr-int/lit8 v0, p0, 0x1

    int-to-float v0, v0

    .line 42
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    goto :goto_0
.end method

.method public static toInt(I)I
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 34
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    .line 35
    shr-int/lit8 v0, p0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public static with(F)I
    .locals 1
    .param p0, "val"    # F

    .prologue
    .line 23
    invoke-static {p0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    and-int/lit8 v0, v0, -0x2

    return v0
.end method

.method public static with(I)I
    .locals 2
    .param p0, "val"    # I

    .prologue
    .line 27
    shl-int/lit8 v1, p0, 0x1

    or-int/lit8 v0, v1, 0x1

    .line 30
    .local v0, "result":I
    shr-int/lit8 v1, v0, 0x1

    if-ne v1, p0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    int-to-float v1, p0

    invoke-static {v1}, Lcom/google/android/apps/books/util/IntOrFloat;->with(F)I

    move-result v0

    goto :goto_0
.end method
