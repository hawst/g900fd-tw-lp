.class Lcom/google/android/apps/books/app/ChaptersAdapter$2;
.super Ljava/lang/Object;
.source "ChaptersAdapter.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ChaptersAdapter;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;ILcom/google/android/apps/books/common/Position;Landroid/widget/ExpandableListView;Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$2;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 1
    .param p1, "expandableListView"    # Landroid/widget/ExpandableListView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$2;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # invokes: Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    invoke-static {v0, p3}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$600(Lcom/google/android/apps/books/app/ChaptersAdapter;I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->onChildClick(I)V

    .line 173
    const/4 v0, 0x1

    return v0
.end method
