.class public Lcom/google/android/apps/books/annotations/StubAnnotationListener;
.super Ljava/lang/Object;
.source "StubAnnotationListener.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public annotationAdded(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p3, "newFullSet":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    return-void
.end method

.method public annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "annotations":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    return-void
.end method

.method public characterQuotaLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "quota":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    return-void
.end method

.method public volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "segment"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p3, "annotations":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    return-void
.end method
