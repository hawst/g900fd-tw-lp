.class Lcom/google/android/apps/books/data/OffersSubcontroller$1;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "OffersSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/OffersSubcontroller;->dismissOffer(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

.field final synthetic val$offerId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->val$offerId:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 166
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->val$offerId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->saveDismissedOfferOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V
    invoke-static {v1, p1, v2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$000(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V

    .line 167
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->val$offerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;
    invoke-static {v3}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$100(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->dismissOffer(Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "OffersSubcontroller"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    const-string v1, "OffersSubcontroller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error dismissing offer: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
