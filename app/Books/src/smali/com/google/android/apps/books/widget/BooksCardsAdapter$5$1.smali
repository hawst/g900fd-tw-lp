.class Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;
.super Ljava/lang/Object;
.source "BooksCardsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;

.field final synthetic val$callback:Lcom/google/android/apps/books/common/ImageCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;Lcom/google/android/apps/books/common/ImageCallback;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;->this$1:Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;->val$callback:Lcom/google/android/apps/books/common/ImageCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;->val$callback:Lcom/google/android/apps/books/common/ImageCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/common/ImageCallback;->onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;->this$1:Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    # invokes: Lcom/google/android/apps/books/widget/BooksCardsAdapter;->onFetchedCoverImage()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->access$100(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    .line 390
    return-void
.end method
