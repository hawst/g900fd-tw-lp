.class public Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sense"
.end annotation


# instance fields
.field public final conjugations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;",
            ">;"
        }
    .end annotation
.end field

.field public final definitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;",
            ">;"
        }
    .end annotation
.end field

.field public final examples:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field

.field public final partOfSpeech:Ljava/lang/String;

.field public final pronunciation:Ljava/lang/String;

.field public final pronunciationUrl:Ljava/lang/String;

.field public final source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

.field public final syllabification:Ljava/lang/String;

.field public final synonyms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
    .locals 0
    .param p1, "syllabification"    # Ljava/lang/String;
    .param p2, "pronunciation"    # Ljava/lang/String;
    .param p3, "partOfSpeech"    # Ljava/lang/String;
    .param p4, "pronunciationUrl"    # Ljava/lang/String;
    .param p9, "source"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;",
            ">;",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    .local p5, "definitions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;>;"
    .local p6, "examples":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    .local p7, "synonyms":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    .local p8, "conjugations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->syllabification:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->pronunciation:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->partOfSpeech:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->pronunciationUrl:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->definitions:Ljava/util/List;

    .line 75
    iput-object p6, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->examples:Ljava/util/List;

    .line 76
    iput-object p7, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->synonyms:Ljava/util/List;

    .line 77
    iput-object p8, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->conjugations:Ljava/util/List;

    .line 78
    iput-object p9, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .line 79
    return-void
.end method
