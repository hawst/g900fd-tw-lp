.class Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$2;
.super Ljava/lang/Object;
.source "OnboardGenreQuizPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$2;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 216
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_GENRES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$2;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    .line 220
    return-void
.end method
