.class public Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;
.super Lcom/google/android/apps/books/app/NonPersistentDialogFragment;
.source "ConcurrentAccessLimitExceededFragment.java"


# instance fields
.field private final mInfoUri:Landroid/net/Uri;

.field private final mMaxDevices:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NonPersistentDialogFragment;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mMaxDevices:I

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mInfoUri:Landroid/net/Uri;

    .line 33
    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;)V
    .locals 0
    .param p1, "maxDevices"    # I
    .param p2, "infoUri"    # Landroid/net/Uri;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NonPersistentDialogFragment;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mMaxDevices:I

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mInfoUri:Landroid/net/Uri;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->closeBookAndDismiss()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mInfoUri:Landroid/net/Uri;

    return-object v0
.end method

.method private closeBookAndDismiss()V
    .locals 1

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->closeBook()V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->dismiss()V

    .line 80
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->closeBookAndDismiss()V

    .line 75
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 44
    new-instance v2, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment$1;-><init>(Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;)V

    .line 51
    .local v2, "okListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment$2;-><init>(Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;)V

    .line 60
    .local v0, "helpListener":Landroid/content/DialogInterface$OnClickListener;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f110001

    iget v6, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mMaxDevices:I

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "template":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "%1$d"

    aput-object v5, v4, v7

    new-array v5, v8, [Ljava/lang/String;

    iget v6, p0, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->mMaxDevices:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 64
    .local v1, "message":Ljava/lang/CharSequence;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0f00e9

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0f00ea

    invoke-virtual {v4, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
