.class Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "SegmentContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$haveContentConsumers:Z

.field final synthetic val$ignoreResources:Z

.field final synthetic val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;ZZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p5, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-object p6, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-boolean p7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$ignoreResources:Z

    iput-boolean p8, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$haveContentConsumers:Z

    iput-object p9, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iput-object p10, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 13
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    const/4 v12, 0x5

    .line 230
    const/4 v10, 0x0

    .line 232
    .local v10, "response":Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v0

    .line 233
    .local v0, "server":Lcom/google/android/apps/books/net/BooksServer;
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v6, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mWidthString:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$400(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$ignoreResources:Z

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/net/BooksServer;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;

    move-result-object v10

    .line 237
    iget-boolean v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$haveContentConsumers:Z

    if-eqz v1, :cond_1

    .line 243
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getContent()Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v8

    .line 245
    .local v8, "bytes":[B
    new-instance v7, Lcom/google/android/apps/books/data/SmallBlobFromServer;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    invoke-direct {v7, v8, v1, v2}, Lcom/google/android/apps/books/data/SmallBlobFromServer;-><init>([BLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/SessionKeyId;)V

    .line 257
    .end local v8    # "bytes":[B
    .local v7, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    :goto_0
    new-instance v11, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getResources()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v11, v7, v1, v2, v3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/List;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;)V

    .line 258
    .local v11, "result":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
    invoke-static {v1, p1, v2, v11}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$600(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    if-eqz v10, :cond_0

    .line 269
    :try_start_1
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 277
    .end local v0    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v7    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v11    # "result":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    :cond_0
    :goto_1
    return-void

    .line 254
    .restart local v0    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getContent()Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v9

    .line 255
    .local v9, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v7, Lcom/google/android/apps/books/data/LargeBlobFromServer;

    invoke-direct {v7, v9}, Lcom/google/android/apps/books/data/LargeBlobFromServer;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V
    :try_end_2
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v7    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    goto :goto_0

    .line 270
    .end local v9    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .restart local v11    # "result":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    :catch_0
    move-exception v5

    .line 271
    .local v5, "e":Ljava/io/IOException;
    const-string v1, "SCSC"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "SCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing response object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 259
    .end local v0    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v11    # "result":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    :catch_1
    move-exception v5

    .line 260
    .local v5, "e":Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v6, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    move-object v2, p1

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 267
    if-eqz v10, :cond_0

    .line 269
    :try_start_4
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 270
    :catch_2
    move-exception v5

    .line 271
    .local v5, "e":Ljava/io/IOException;
    const-string v1, "SCSC"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "SCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing response object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 262
    .end local v5    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 263
    .restart local v5    # "e":Ljava/io/IOException;
    :try_start_5
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v1, p1, v2, v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$800(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 267
    if-eqz v10, :cond_0

    .line 269
    :try_start_6
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 270
    :catch_4
    move-exception v5

    .line 271
    const-string v1, "SCSC"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "SCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing response object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 264
    .end local v5    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v5

    .line 265
    .local v5, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :try_start_7
    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v1, p1, v2, v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$800(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 267
    if-eqz v10, :cond_0

    .line 269
    :try_start_8
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_1

    .line 270
    :catch_6
    move-exception v5

    .line 271
    .local v5, "e":Ljava/io/IOException;
    const-string v1, "SCSC"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "SCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing response object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 267
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_2

    .line 269
    :try_start_9
    invoke-interface {v10}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 274
    :cond_2
    :goto_2
    throw v1

    .line 270
    :catch_7
    move-exception v5

    .line 271
    .restart local v5    # "e":Ljava/io/IOException;
    const-string v2, "SCSC"

    invoke-static {v2, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 272
    const-string v2, "SCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
