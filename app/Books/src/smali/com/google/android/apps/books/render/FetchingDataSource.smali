.class public Lcom/google/android/apps/books/render/FetchingDataSource;
.super Ljava/lang/Object;
.source "FetchingDataSource.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ReaderDataSource;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private mHasShutDown:Z

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

.field private final mResourceIdToResource:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/common/base/Function;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "fsm"    # Lcom/google/android/apps/books/common/FileStorageManager;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            "Lcom/google/android/apps/books/common/FileStorageManager;",
            "Lcom/google/android/apps/books/util/Logger;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p6, "resourceIdToResource":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mHasShutDown:Z

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mAccount:Landroid/accounts/Account;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 67
    invoke-static {p5}, Lcom/google/android/apps/books/render/ResourceContentStoreUtils;->makeResourceStore(Lcom/google/android/apps/books/data/BooksDataController;)Lcom/google/android/apps/books/render/ResourceContentStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    .line 68
    iput-object p4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 69
    iput-object p5, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 70
    iput-object p6, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceIdToResource:Lcom/google/common/base/Function;

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 56
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getFileStorageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v3

    invoke-static {p1, p2}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v5

    invoke-interface {p3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getResourceIdToResource()Lcom/google/common/base/Function;

    move-result-object v6

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/FetchingDataSource;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/common/base/Function;)V

    .line 59
    return-void
.end method

.method private static resourceContentLogPrefix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getResourceContent("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static segmentContentLogPrefix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getSegmentContent("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public configureWebView(Lcom/google/android/apps/books/render/SnapshottingWebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 9
    .param p1, "webView"    # Lcom/google/android/apps/books/render/SnapshottingWebView;
    .param p3, "readerListener"    # Lcom/google/android/apps/books/render/ReaderListener;
    .param p4, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SnapshottingWebView;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;",
            "Lcom/google/android/apps/books/render/ReaderListener;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 242
    .local p2, "resourceExceptionHandler":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Exception;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceIdToResource:Lcom/google/common/base/Function;

    const/4 v8, 0x0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/books/render/ResourceContentStore;->configureWebView(Landroid/accounts/Account;Ljava/lang/String;Landroid/webkit/WebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Landroid/webkit/WebViewClient;)V

    .line 244
    return-void
.end method

.method public getCssContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cssIndex"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    .line 200
    iget-boolean v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mHasShutDown:Z

    if-eqz v4, :cond_0

    .line 201
    const-string v3, ""

    .line 225
    :goto_0
    return-object v3

    .line 203
    :cond_0
    invoke-static {p2}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 204
    const-string v4, "FetchingReaderBridge"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 205
    const-string v4, "FetchingReaderBridge"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCssContent given bad cssIndex: \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_1
    const-string v3, ""

    goto :goto_0

    .line 211
    :cond_2
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 212
    .local v0, "cssIndexInt":I
    iget-object v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getCssResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "cssResId":Ljava/lang/String;
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/books/render/FetchingDataSource;->getResourceContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 220
    .local v3, "result":Ljava/lang/String;
    goto :goto_0

    .line 221
    .end local v0    # "cssIndexInt":I
    .end local v1    # "cssResId":Ljava/lang/String;
    .end local v3    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 222
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v4, "FetchingReaderBridge"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 223
    const-string v4, "FetchingReaderBridge"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCssContent bad CSS index: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 225
    :cond_3
    const-string v3, ""

    goto :goto_0
.end method

.method public getResourceContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 146
    iget-boolean v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mHasShutDown:Z

    if-eqz v8, :cond_1

    .line 147
    const-string v8, "FetchingReaderBridge"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 148
    const-string v8, "FetchingReaderBridge"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/google/android/apps/books/render/FetchingDataSource;->resourceContentLogPrefix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "shut down -- returning empty string"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    const-string v8, ""

    .line 193
    :goto_0
    return-object v8

    .line 154
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v9, "DataSource#getResourceContent"

    invoke-static {v8, v9}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v7

    .line 157
    .local v7, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    iget-object v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceIdToResource:Lcom/google/common/base/Function;

    invoke-interface {v8, p2}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/Resource;

    .line 159
    .local v5, "resource":Lcom/google/android/apps/books/model/Resource;
    const/4 v3, 0x0

    .line 161
    .local v3, "input":Ljava/io/InputStream;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    sget-object v9, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v8, p1, v5, v9}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceResources(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/BooksDataController$Priority;)Ljava/util/List;

    move-result-object v6

    .line 163
    .local v6, "resourceResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    const-string v8, "#foundFonts"

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 165
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Resource;

    .line 166
    .local v4, "referencedResource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v4}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/books/model/ResourceUtils;->isFontResourceType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 176
    iget-object v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v8, p1, v4}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;

    move-result-object v0

    .line 178
    .local v0, "content":Ljava/io/InputStream;
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 185
    .end local v0    # "content":Ljava/io/InputStream;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "referencedResource":Lcom/google/android/apps/books/model/Resource;
    .end local v6    # "resourceResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :catch_0
    move-exception v1

    .line 186
    .local v1, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    .end local v1    # "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_3

    .line 191
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 193
    :cond_3
    invoke-interface {v7}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v8

    .line 181
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v6    # "resourceResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_4
    :try_start_2
    const-string v8, "#downloadedFonts"

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 183
    iget-object v8, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v8, p1, v5}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;

    move-result-object v3

    .line 184
    new-instance v8, Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 190
    if-eqz v3, :cond_5

    .line 191
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 193
    :cond_5
    invoke-interface {v7}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto :goto_0

    .line 187
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "resourceResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :catch_1
    move-exception v1

    .line 188
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-static {v1}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v8

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public getResourceContentUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    iget-object v2, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mAccount:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceIdToResource:Lcom/google/common/base/Function;

    invoke-interface {v0, p2}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v1, v2, p1, v0}, Lcom/google/android/apps/books/render/ResourceContentStore;->getResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 99
    iget-boolean v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mHasShutDown:Z

    if-eqz v4, :cond_2

    .line 100
    const-string v4, "FetchingReaderBridge"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 101
    const-string v4, "FetchingReaderBridge"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/google/android/apps/books/render/FetchingDataSource;->segmentContentLogPrefix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "shut down -- returning empty string"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    const-string v0, ""

    .line 132
    :cond_1
    :goto_0
    return-object v0

    .line 107
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v5, "DataSource#getSegmentContent"

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v3

    .line 110
    .local v3, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    iget-object v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getValueWithId(Ljava/lang/String;)Lcom/google/android/apps/books/util/Identifiable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Segment;

    .line 111
    .local v2, "segment":Lcom/google/android/apps/books/model/Segment;
    if-nez v2, :cond_4

    .line 112
    const-string v4, "FetchingReaderBridge"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 113
    const-string v4, "FetchingReaderBridge"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No segment for ID "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 120
    :cond_4
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v4, p1, v2}, Lcom/google/android/apps/books/data/DataControllerUtils;->getSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 127
    .local v0, "content":Ljava/lang/String;
    const-string v4, "#ensuredSegment"

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 128
    const-string v4, "FetchingReaderBridge"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129
    const-string v4, "FetchingReaderBridge"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/google/android/apps/books/render/FetchingDataSource;->segmentContentLogPrefix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ensured content"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    .end local v0    # "content":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    throw v1

    .line 123
    .end local v1    # "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :catch_1
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v4

    throw v4
.end method

.method public getSegments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mHasShutDown:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/render/FetchingDataSource;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ResourceContentStore;->shutDown()V

    .line 82
    return-void
.end method
