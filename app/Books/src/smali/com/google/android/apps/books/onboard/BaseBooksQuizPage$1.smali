.class final Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$1;
.super Ljava/lang/Object;
.source "BaseBooksQuizPage.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/apps/books/data/BooksDataController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$controller:Lcom/google/android/apps/books/app/OnboardingController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OnboardingController;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$1;->val$controller:Lcom/google/android/apps/books/app/OnboardingController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makePage(Landroid/content/Context;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logicalPosition"    # I
    .param p3, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;-><init>(Landroid/content/Context;)V

    .line 161
    .local v0, "page":Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$1;->val$controller:Lcom/google/android/apps/books/app/OnboardingController;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->setController(Lcom/google/android/apps/books/app/OnboardingController;)V

    .line 162
    return-object v0
.end method
