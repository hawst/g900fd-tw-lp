.class Lcom/google/android/apps/books/widget/ReadNowHomeView$4;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->startOnboarding(I)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    const v1, 0x7f0f01e8

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
