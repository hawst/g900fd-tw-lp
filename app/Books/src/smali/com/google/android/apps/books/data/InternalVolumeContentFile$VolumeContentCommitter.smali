.class Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;
.super Ljava/lang/Object;
.source "InternalVolumeContentFile.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$Committer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/InternalVolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeContentCommitter"
.end annotation


# instance fields
.field private mCommitted:Z

.field private final mContent:Lcom/google/android/apps/books/model/EncryptedContent;

.field private final mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

.field private final mTempFile:Ljava/io/File;

.field final synthetic this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/model/EncryptedContent;Ljava/io/File;)V
    .locals 0
    .param p2, "listener"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
    .param p3, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;
    .param p4, "tempFile"    # Ljava/io/File;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    .line 90
    iput-object p3, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mContent:Lcom/google/android/apps/books/model/EncryptedContent;

    .line 91
    iput-object p4, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mTempFile:Ljava/io/File;

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/model/EncryptedContent;Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p2, "x1"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
    .param p3, "x2"    # Lcom/google/android/apps/books/model/EncryptedContent;
    .param p4, "x3"    # Ljava/io/File;
    .param p5, "x4"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;-><init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/model/EncryptedContent;Ljava/io/File;)V

    return-void
.end method

.method private getFile()Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mCommitted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mTempFile:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->tempPath(Ljava/io/File;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    goto :goto_0
.end method

.method private tempPath(Ljava/io/File;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 3
    .param p1, "tempFile"    # Ljava/io/File;

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    # getter for: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;
    invoke-static {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$200(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    # getter for: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mTempFileSource:Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;
    invoke-static {v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$300(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;-><init>(Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;)V

    return-object v0
.end method


# virtual methods
.method public bestEffortAbort()V
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mCommitted:Z

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mTempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 129
    :cond_0
    return-void
.end method

.method public commit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mTempFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    # getter for: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$000(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    # invokes: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->ensureParent()V
    invoke-static {v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$100(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mTempFile:Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    # getter for: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;
    invoke-static {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$000(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mContent:Lcom/google/android/apps/books/model/EncryptedContent;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;->onSaved(Lcom/google/android/apps/books/model/EncryptedContent;)V

    .line 103
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->mCommitted:Z

    .line 104
    return-void
.end method

.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->getFile()Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;->getFile()Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
