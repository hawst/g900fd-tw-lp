.class public Lcom/google/android/apps/books/widget/CardDetailsLayout;
.super Landroid/widget/FrameLayout;
.source "CardDetailsLayout.java"


# instance fields
.field protected mAction:Landroid/widget/TextView;

.field protected mActionTouchArea:Landroid/view/View;

.field private final mLeftColumnViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mOldOverflowArea:Landroid/graphics/Rect;

.field protected mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

.field private final mOverflowArea:Landroid/graphics/Rect;

.field private mOverflowTouchExtend:I

.field protected mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

.field protected mPrice:Landroid/widget/TextView;

.field protected mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

.field protected mReasonSeparator:Landroid/view/View;

.field protected mSubtitle:Landroid/widget/TextView;

.field protected mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 58
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->init()V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 58
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->init()V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 58
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->init()V

    .line 63
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowTouchExtend:I

    .line 78
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 84
    const v0, 0x7f0e00cd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0e00ce

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0e00f0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    .line 87
    const v0, 0x7f0e00f1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReasonSeparator:Landroid/view/View;

    .line 88
    const v0, 0x7f0e00eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mAction:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mActionTouchArea:Landroid/view/View;

    .line 90
    const v0, 0x7f0e00e1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    .line 91
    const v0, 0x7f0e00e0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPrice:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e00e2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 20
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 166
    sub-int v16, p4, p2

    .line 167
    .local v16, "width":I
    sub-int v7, p5, p3

    .line 168
    .local v7, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingLeft()I

    move-result v4

    .line 169
    .local v4, "contentLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingTop()I

    move-result v6

    .line 170
    .local v6, "contentTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingRight()I

    move-result v17

    sub-int v5, v16, v17

    .line 171
    .local v5, "contentRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingBottom()I

    move-result v17

    sub-int v3, v7, v17

    .line 172
    .local v3, "contentBottom":I
    move v9, v6

    .line 174
    .local v9, "lastChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mLeftColumnViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    .line 175
    .local v10, "leftColumnView":Landroid/view/View;
    invoke-static {v10}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 176
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 177
    .local v11, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v17, v0

    add-int v15, v9, v17

    .line 178
    .local v15, "thisChildTop":I
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v17, v0

    add-int v13, v4, v17

    .line 179
    .local v13, "thisChildLeft":I
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v12, v15, v17

    .line 180
    .local v12, "thisChildBottom":I
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    add-int v17, v17, v13

    move/from16 v0, v17

    invoke-virtual {v10, v13, v15, v0, v12}, Landroid/view/View;->layout(IIII)V

    .line 182
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v17, v0

    add-int v9, v12, v17

    goto :goto_0

    .line 186
    .end local v10    # "leftColumnView":Landroid/view/View;
    .end local v11    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v12    # "thisChildBottom":I
    .end local v13    # "thisChildLeft":I
    .end local v15    # "thisChildTop":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 188
    .restart local v11    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v17, v0

    add-int v15, v6, v17

    .line 189
    .restart local v15    # "thisChildTop":I
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v17, v0

    sub-int v14, v5, v17

    .line 190
    .local v14, "thisChildRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v17

    add-int v12, v15, v17

    .line 191
    .restart local v12    # "thisChildBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getMeasuredWidth()I

    move-result v18

    sub-int v18, v14, v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v15, v14, v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->layout(IIII)V

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->onOverflowIconPositioned()V

    .line 195
    .end local v11    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v12    # "thisChildBottom":I
    .end local v14    # "thisChildRight":I
    .end local v15    # "thisChildTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 197
    .restart local v11    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getMeasuredHeight()I

    move-result v17

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v18, v0

    add-int v17, v17, v18

    sub-int v15, v3, v17

    .line 199
    .restart local v15    # "thisChildTop":I
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v17, v0

    sub-int v14, v5, v17

    .line 200
    .restart local v14    # "thisChildRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getMeasuredWidth()I

    move-result v18

    sub-int v18, v14, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getMeasuredHeight()I

    move-result v19

    add-int v19, v19, v15

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v15, v14, v2}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->layout(IIII)V

    .line 203
    .end local v11    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v14    # "thisChildRight":I
    .end local v15    # "thisChildTop":I
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v11, -0x80000000

    .line 107
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 108
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingLeft()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingRight()I

    move-result v10

    add-int/2addr v9, v10

    sub-int v0, v7, v9

    .line 109
    .local v0, "contentWidth":I
    const/4 v5, 0x0

    .line 110
    .local v5, "totalChildHeight":I
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 114
    .local v1, "fullWidthItemWidthSpec":I
    const/4 v6, 0x0

    .line 116
    .local v6, "widestRightItemWidth":I
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 117
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v9, v1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->measure(II)V

    .line 118
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutWidth(Landroid/view/View;)I

    move-result v6

    .line 121
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 122
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    invoke-virtual {v9, v1, p2}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->measure(II)V

    .line 123
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutWidth(Landroid/view/View;)I

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 127
    :cond_1
    sub-int v9, v0, v6

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 130
    .local v3, "leftItemWidthSpec":I
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 131
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v3, p2}, Landroid/widget/TextView;->measure(II)V

    .line 137
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getLineHeight()I

    move-result v9

    mul-int/lit8 v4, v9, 0x2

    .line 138
    .local v4, "linesHeight":I
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v10, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mTitle:Landroid/widget/TextView;

    invoke-static {v10}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getVerticalMargins(Landroid/view/View;)I

    move-result v10

    add-int/2addr v9, v10

    add-int/2addr v5, v9

    .line 142
    .end local v4    # "linesHeight":I
    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 146
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mPinView:Lcom/google/android/apps/books/widget/BooksDownloadStatusView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 147
    move v8, v3

    .line 151
    .local v8, "widthSpec":I
    :goto_0
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v9, v8, p2}, Landroid/widget/TextView;->measure(II)V

    .line 152
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mSubtitle:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v9

    add-int/2addr v5, v9

    .line 155
    .end local v8    # "widthSpec":I
    :cond_3
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 156
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-virtual {v9, v3, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->measure(II)V

    .line 157
    iget-object v9, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-static {v9}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getLayoutHeight(Landroid/view/View;)I

    move-result v9

    add-int/2addr v5, v9

    .line 160
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingTop()I

    move-result v9

    add-int/2addr v9, v5

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getPaddingBottom()I

    move-result v10

    add-int v2, v9, v10

    .line 161
    .local v2, "height":I
    invoke-virtual {p0, v7, v2}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->setMeasuredDimension(II)V

    .line 162
    return-void

    .line 149
    .end local v2    # "height":I
    :cond_5
    move v8, v1

    .restart local v8    # "widthSpec":I
    goto :goto_0
.end method

.method protected onOverflowIconPositioned()V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getHitRect(Landroid/graphics/Rect;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowTouchExtend:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowTouchExtend:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-ne v0, v1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    new-instance v0, Landroid/view/TouchDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-direct {v0, v1, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardDetailsLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOldOverflowArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardDetailsLayout;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
