.class Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;
.super Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;
.source "NativeHeapBitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/NativeHeapBitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SerializedBitmap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
        "<TT;>.BitmapSource;"
    }
.end annotation


# instance fields
.field final mData:Ljava/nio/ByteBuffer;

.field final synthetic this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 183
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .line 184
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;IILandroid/graphics/Bitmap$Config;)V

    .line 186
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int v0, v1, v2

    .line 187
    .local v0, "size":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->allocateBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mData:Ljava/nio/ByteBuffer;

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mData:Ljava/nio/ByteBuffer;

    invoke-virtual {p2, v1}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mData:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 190
    return-void
.end method

.method private allocateBuffer(I)Ljava/nio/ByteBuffer;
    .locals 7
    .param p1, "size"    # I

    .prologue
    .line 167
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFile:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->useJavaHeap()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    :try_start_0
    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->sMapFile:Ljava/io/RandomAccessFile;
    invoke-static {}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$200()Ljava/io/RandomAccessFile;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->PRIVATE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    int-to-long v4, p1

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    .line 170
    :catch_0
    move-exception v6

    .line 171
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "NativeHeapBitmapCache"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "NativeHeapBitmapCache"

    const-string v1, "Unable to map a buffer"

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    .end local v6    # "e":Ljava/io/IOException;
    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0
.end method

.method private useJavaHeap()Z
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mHeapAllocFraction:I
    invoke-static {v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$000(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mBufferAllocSeq:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$100(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mHeapAllocFraction:I
    invoke-static {v3}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$000(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)I

    move-result v3

    rem-int/2addr v2, v3

    if-nez v2, :cond_0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 160
    goto :goto_0

    :cond_0
    move v0, v1

    .line 162
    goto :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7fffffff -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method getBitmap()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 194
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.SerializedBitmap;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;
    invoke-static {v1}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$300(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mWidth:I

    iget v3, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mHeight:I

    iget-object v4, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mConfig:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/util/BitmapReusePool;->obtain(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 195
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$SerializedBitmap;->mData:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 197
    return-object v0
.end method
