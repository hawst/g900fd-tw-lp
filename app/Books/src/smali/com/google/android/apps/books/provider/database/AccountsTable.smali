.class public Lcom/google/android/apps/books/provider/database/AccountsTable;
.super Ljava/lang/Object;
.source "AccountsTable.java"


# direct methods
.method public static getCreationSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "CREATE TABLE accounts (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB, UNIQUE (account_name))"

    return-object v0
.end method

.method public static getCreationSqlVersion130()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "CREATE TABLE accounts (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, session_key_version TEXT, root_key_version INTEGER, session_key_blob BLOB, UNIQUE (account_name))"

    return-object v0
.end method
