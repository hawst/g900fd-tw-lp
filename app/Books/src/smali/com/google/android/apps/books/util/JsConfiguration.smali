.class public Lcom/google/android/apps/books/util/JsConfiguration;
.super Ljava/lang/Object;
.source "JsConfiguration.java"


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mCss:Lorg/json/JSONArray;

.field private final mDisplayDensity:F

.field private final mFirstContentPassageIndex:I

.field private final mFixedLayoutData:Lorg/json/JSONArray;

.field private final mFontTestString:Ljava/lang/String;

.field private final mLogPerformance:Z

.field private final mMarginPercentSides:I

.field private final mMarginPercentTopBottom:I

.field private final mPagesPerViewport:I

.field private final mPassageCss:Lorg/json/JSONArray;

.field private final mPassageSegments:Lorg/json/JSONArray;

.field private final mSendPageText:Z

.field private final mSharedFonts:Lorg/json/JSONArray;

.field private final mViewportSize:Landroid/graphics/Point;

.field public final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;ZZFIIILandroid/graphics/Point;Ljava/lang/String;I)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "passageSegments"    # Lorg/json/JSONArray;
    .param p4, "passageCss"    # Lorg/json/JSONArray;
    .param p5, "bakedCss"    # Lorg/json/JSONArray;
    .param p6, "sharedFonts"    # Lorg/json/JSONArray;
    .param p7, "fixedLayoutData"    # Lorg/json/JSONArray;
    .param p8, "logPerformance"    # Z
    .param p9, "sendPageText"    # Z
    .param p10, "displayDensity"    # F
    .param p11, "marginPercentTopBottom"    # I
    .param p12, "marginPercentSides"    # I
    .param p13, "pagesPerViewport"    # I
    .param p14, "viewportSize"    # Landroid/graphics/Point;
    .param p15, "fontTestString"    # Ljava/lang/String;
    .param p16, "firstContentPassageIndex"    # I

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mAccount:Landroid/accounts/Account;

    .line 83
    iput-object p2, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mVolumeId:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPassageSegments:Lorg/json/JSONArray;

    .line 85
    iput-object p4, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPassageCss:Lorg/json/JSONArray;

    .line 86
    iput-object p5, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mCss:Lorg/json/JSONArray;

    .line 87
    iput-object p6, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mSharedFonts:Lorg/json/JSONArray;

    .line 88
    iput-object p7, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFixedLayoutData:Lorg/json/JSONArray;

    .line 89
    iput-boolean p8, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mLogPerformance:Z

    .line 90
    iput-boolean p9, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mSendPageText:Z

    .line 91
    iput p10, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mDisplayDensity:F

    .line 92
    iput p11, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mMarginPercentTopBottom:I

    .line 93
    iput p12, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mMarginPercentSides:I

    .line 94
    iput p13, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPagesPerViewport:I

    .line 95
    iput-object p14, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mViewportSize:Landroid/graphics/Point;

    .line 96
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFontTestString:Ljava/lang/String;

    .line 97
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFirstContentPassageIndex:I

    .line 98
    return-void
.end method


# virtual methods
.method public toJson()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 101
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 103
    .local v0, "configData":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 104
    const-string v2, "volumeId"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 105
    const-string v2, "passageSegments"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPassageSegments:Lorg/json/JSONArray;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 106
    const-string v2, "passageCss"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPassageCss:Lorg/json/JSONArray;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 107
    const-string v2, "css"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mCss:Lorg/json/JSONArray;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 108
    const-string v2, "sharedFonts"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mSharedFonts:Lorg/json/JSONArray;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 109
    const-string v2, "fixedLayout"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFixedLayoutData:Lorg/json/JSONArray;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 110
    const-string v2, "logPerformance"

    iget-boolean v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mLogPerformance:Z

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 111
    const-string v2, "sendPageText"

    iget-boolean v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mSendPageText:Z

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 112
    const-string v2, "displayDensity"

    iget v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mDisplayDensity:F

    float-to-double v4, v3

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 113
    const-string v2, "marginPercentTopBottom"

    iget v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mMarginPercentTopBottom:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 114
    const-string v2, "marginPercentSides"

    iget v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mMarginPercentSides:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 115
    const-string v2, "pagesPerViewport"

    iget v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mPagesPerViewport:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 116
    const-string v2, "sdkVersion"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 117
    iget-object v2, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mViewportSize:Landroid/graphics/Point;

    if-eqz v2, :cond_0

    .line 118
    const-string v2, "viewportWidth"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mViewportSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 119
    const-string v2, "viewportHeight"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mViewportSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 122
    :cond_0
    const-string v2, "systemOverrides"

    invoke-static {}, Lcom/google/android/apps/books/util/Config;->getJavascriptSystemOverrides()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 123
    const-string v2, "fontTestString"

    iget-object v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFontTestString:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 124
    const-string v2, "firstContentPassageIndex"

    iget v3, p0, Lcom/google/android/apps/books/util/JsConfiguration;->mFirstContentPassageIndex:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    return-object v0

    .line 125
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "problem escaping volume metadata"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
