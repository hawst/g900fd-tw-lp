.class public final Lcom/google/android/apps/books/app/DeviceInfo$Builder;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/DeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mAndroidId:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mManufacturer:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mProductName:Ljava/lang/String;

.field private mSerialNumber:Ljava/lang/String;

.field private mUseFakeOffersData:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/app/DeviceInfo;
    .locals 9

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/apps/books/app/DeviceInfo;

    iget-object v1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mAndroidId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mModel:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mManufacturer:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mDeviceName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mProductName:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mUseFakeOffersData:Z

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/app/DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/DeviceInfo$1;)V

    return-object v0
.end method

.method public setAndroidId(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "androidId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mAndroidId:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public setDeviceName(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mDeviceName:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public setManufacturer(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "manufacturer"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mManufacturer:Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mModel:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public setProductName(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "productName"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mProductName:Ljava/lang/String;

    .line 60
    return-object p0
.end method

.method public setSerialNumber(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "serialNumber"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setUseFakeOffersData(Z)Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    .locals 0
    .param p1, "useFakeData"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->mUseFakeOffersData:Z

    .line 65
    return-object p0
.end method
