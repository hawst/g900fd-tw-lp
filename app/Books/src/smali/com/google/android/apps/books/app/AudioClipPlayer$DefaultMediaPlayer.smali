.class Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultMediaPlayer"
.end annotation


# instance fields
.field private mIMediaPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

.field private mPlayer:Landroid/media/MediaPlayer;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/AudioClipPlayer$1;

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method private getPlayer()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 333
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method


# virtual methods
.method public isPlaying()Z
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mIMediaPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;->onPrepared(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;)V

    .line 329
    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mIMediaPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;->onSeekComplete(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;)V

    .line 324
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 223
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 214
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 319
    return-void
.end method

.method public setDataSourceAndPreparePlayer(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;I)V
    .locals 8
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    .param p2, "requestId"    # I

    .prologue
    const/4 v4, 0x0

    .line 242
    new-instance v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;ILcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 312
    .local v3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$600(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mVolumeId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$500(Lcom/google/android/apps/books/app/AudioClipPlayer;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;
    invoke-static {p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->access$100(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v2

    sget-object v6, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v7, 0x0

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 314
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->mIMediaPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

    .line 209
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 238
    return-void
.end method
