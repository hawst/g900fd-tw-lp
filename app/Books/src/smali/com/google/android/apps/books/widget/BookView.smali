.class public Lcom/google/android/apps/books/widget/BookView;
.super Landroid/widget/FrameLayout;
.source "BookView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesDisplay;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BookView$PageDrawingData;,
        Lcom/google/android/apps/books/widget/BookView$Callbacks;
    }
.end annotation


# instance fields
.field private mActionBarElevation:F

.field private mBackground:Landroid/view/View;

.field private mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

.field private final mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

.field private mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private mDisplayTwoPages:Z

.field private mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

.field private final mMinSkimAnimDuration:I

.field private mNavViewBgColor:I

.field private final mPageOffsetToContent:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/widget/BookView$PageDrawingData;",
            ">;"
        }
    .end annotation
.end field

.field private mPinchingNavView:Z

.field private mReadingBgColor:I

.field private mScale:F

.field private mScrollX:F

.field private mScrollY:F

.field private final mSkimAnimDuration:I

.field private mZoomedUpNavViewScale:F

.field private mZoomedUpNavViewTx:F

.field private mZoomedUpNavViewTy:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 238
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/BookView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 239
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 242
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/BookView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 246
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 164
    iput v2, p0, Lcom/google/android/apps/books/widget/BookView;->mScale:F

    .line 165
    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mScrollX:F

    .line 166
    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mScrollY:F

    .line 170
    iput v2, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    .line 171
    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTx:F

    .line 172
    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTy:F

    .line 174
    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mActionBarElevation:F

    .line 225
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    .line 227
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BookView;->mPinchingNavView:Z

    .line 229
    new-instance v1, Lcom/google/android/apps/books/widget/BookView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/BookView$1;-><init>(Lcom/google/android/apps/books/widget/BookView;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 254
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mSkimAnimDuration:I

    .line 255
    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/apps/books/widget/BookView;->mMinSkimAnimDuration:I

    .line 256
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/BookView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BookView;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BookView;->mPinchingNavView:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/BookView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BookView;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->onNavViewAnimationEnded()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BookView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/navigation/BookNavView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BookView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    return-object v0
.end method

.method private animateNavViewHiding(FFFI)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "translationX"    # F
    .param p3, "translationY"    # F
    .param p4, "duration"    # I

    .prologue
    .line 524
    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewAnimator(FFFF)Landroid/animation/Animator;

    move-result-object v0

    .line 526
    .local v0, "animator":Landroid/animation/Animator;
    int-to-long v2, p4

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 527
    new-instance v1, Lcom/google/android/apps/books/widget/BookView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/BookView$3;-><init>(Lcom/google/android/apps/books/widget/BookView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 533
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->getSkimView()Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V

    .line 534
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v2, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 537
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->startAnimator(Landroid/animation/Animator;)V

    .line 538
    return-void
.end method

.method private animateNavViewShowing(FFFI)V
    .locals 8
    .param p1, "scale"    # F
    .param p2, "translationX"    # F
    .param p3, "translationY"    # F
    .param p4, "duration"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 696
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewVisible(Z)V

    .line 697
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/navigation/BookNavView;->setLabelsVisible(Z)V

    .line 699
    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewAnimator(FFFF)Landroid/animation/Animator;

    move-result-object v2

    .line 701
    .local v2, "navViewAnimator":Landroid/animation/Animator;
    int-to-long v4, p4

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 702
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 703
    new-instance v3, Lcom/google/android/apps/books/widget/BookView$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/widget/BookView$4;-><init>(Lcom/google/android/apps/books/widget/BookView;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 716
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/books/navigation/BookNavView;->getLabelAlphaAnimator(Z)Landroid/animation/AnimatorSet;

    move-result-object v1

    .line 717
    .local v1, "labelAnimator":Landroid/animation/Animator;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 719
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 721
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v2, v3, v7

    aput-object v1, v3, v6

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 723
    new-instance v3, Lcom/google/android/apps/books/widget/BookView$5;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/widget/BookView$5;-><init>(Lcom/google/android/apps/books/widget/BookView;)V

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 730
    new-array v3, v6, [Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/navigation/BookNavView;->getSkimView()Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v3}, Lcom/google/android/apps/books/util/ViewUtils;->useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V

    .line 731
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v3, :cond_0

    .line 732
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v4, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 734
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->startAnimator(Landroid/animation/Animator;)V

    .line 735
    return-void
.end method

.method private isVisibleOffset(I)Z
    .locals 1
    .param p1, "screenOffset"    # I

    .prologue
    .line 384
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BookView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeNavViewVisible(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 290
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/navigation/BookNavView;->setVisibility(I)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mBackground:Landroid/view/View;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/apps/books/widget/BookView;->mNavViewBgColor:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 294
    return-void

    :cond_0
    move v0, v2

    .line 290
    goto :goto_0

    :cond_1
    move v2, v1

    .line 291
    goto :goto_1

    .line 293
    :cond_2
    iget v0, p0, Lcom/google/android/apps/books/widget/BookView;->mReadingBgColor:I

    goto :goto_2
.end method

.method private offsetFromBasePositionToScreenOffset(I)I
    .locals 1
    .param p1, "offsetFromBasePosition"    # I

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method private onNavViewAnimationEnded()V
    .locals 2

    .prologue
    .line 434
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewVisible(Z)V

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->clear()V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onFinishedTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 439
    :cond_0
    return-void
.end method

.method private resetNavView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->setAlpha(F)V

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleX(F)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleY(F)V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationX(F)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationY(F)V

    .line 546
    return-void
.end method

.method private screenOffsetToOffsetFromBasePosition(I)I
    .locals 1
    .param p1, "screenOffset"    # I

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private setActionBarElevation(F)V
    .locals 2
    .param p1, "elevation"    # F

    .prologue
    .line 455
    iput p1, p0, Lcom/google/android/apps/books/widget/BookView;->mActionBarElevation:F

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    iget v1, p0, Lcom/google/android/apps/books/widget/BookView;->mActionBarElevation:F

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->setActionBarElevation(F)V

    .line 459
    :cond_0
    return-void
.end method

.method private setPageData(ILcom/google/android/apps/books/widget/BookView$PageDrawingData;)V
    .locals 2
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "pageData"    # Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    .prologue
    .line 376
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 377
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BookView;->offsetFromBasePositionToScreenOffset(I)I

    move-result v0

    .line 378
    .local v0, "screenOffset":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->isVisibleOffset(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 379
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->updateScreenOffset(I)V

    .line 381
    :cond_0
    return-void
.end method

.method private updateScreenOffset(I)V
    .locals 7
    .param p1, "screenOffset"    # I

    .prologue
    .line 336
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BookView;->screenOffsetToOffsetFromBasePosition(I)I

    move-result v0

    .line 337
    .local v0, "offsetFromBasePosition":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    .line 338
    .local v1, "pageData":Lcom/google/android/apps/books/widget/BookView$PageDrawingData;
    if-eqz v1, :cond_1

    .line 339
    iget-object v3, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-eqz v3, :cond_0

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    iget-object v5, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->content:Lcom/google/android/apps/books/util/SimpleDrawable;

    iget-object v6, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v3, p1, v4, v5, v6}, Lcom/google/android/apps/books/widget/BaseSpreadView;->setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V

    .line 353
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v3, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->content:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v3, :cond_1

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->content:Lcom/google/android/apps/books/util/SimpleDrawable;

    iget-object v5, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/apps/books/widget/BaseSpreadView;->setPageContent(ILcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    goto :goto_0

    .line 351
    :cond_1
    if-nez v1, :cond_2

    const/4 v2, 0x0

    .line 352
    .local v2, "size":Landroid/graphics/Point;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/google/android/apps/books/widget/BaseSpreadView;->setPageLoading(ILandroid/graphics/Point;)V

    goto :goto_0

    .line 351
    .end local v2    # "size":Landroid/graphics/Point;
    :cond_2
    iget-object v2, v1, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageDisplaySize:Landroid/graphics/Point;

    goto :goto_1
.end method

.method private zoomCurrentSpread()Z
    .locals 23

    .prologue
    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/navigation/BookNavView;->getCurrentSpread()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v3

    .line 557
    .local v3, "centerSpread":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    if-nez v3, :cond_0

    .line 558
    const/high16 v21, -0x40800000    # -1.0f

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    .line 559
    const/16 v21, 0x0

    .line 602
    :goto_0
    return v21

    .line 562
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageBorder()Landroid/view/View;

    move-result-object v4

    .line 564
    .local v4, "centerView":Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/apps/books/util/ViewUtils;->getEffectiveScale(Landroid/view/View;)J

    move-result-wide v16

    .line 565
    .local v16, "viewScale":J
    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/books/util/LongPointF;->getX(J)F

    move-result v18

    .line 566
    .local v18, "viewScaleX":F
    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/books/util/LongPointF;->getY(J)F

    move-result v19

    .line 568
    .local v19, "viewScaleY":F
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v21

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v22

    sub-int v21, v21, v22

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v22

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v20, v0

    .line 570
    .local v20, "width":F
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v21

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v22

    sub-int v21, v21, v22

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v22

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v7, v0

    .line 576
    .local v7, "height":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/widget/BookView;->mDisplayTwoPages:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getNonEmptyPagesCount()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v9, v21, v22

    .line 578
    .local v9, "scaleCompX":F
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mScale:F

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BookView;->getWidth()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v11, v21, v22

    .line 579
    .local v11, "scaledFullWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mScale:F

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BookView;->getHeight()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v10, v21, v22

    .line 580
    .local v10, "scaledFullHeight":F
    mul-float v21, v9, v11

    mul-float v22, v20, v18

    div-float v21, v21, v22

    mul-float v22, v7, v19

    div-float v22, v10, v22

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 582
    .local v8, "scale":F
    move-object/from16 v0, p0

    iput v8, v0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleX(F)V

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleY(F)V

    .line 587
    invoke-static {v4}, Lcom/google/android/apps/books/util/ViewUtils;->getLocationOnScreen(Landroid/view/View;)J

    move-result-wide v12

    .line 588
    .local v12, "viewLocation":J
    invoke-static {v12, v13}, Lcom/google/android/apps/books/util/LongPointF;->getX(J)F

    move-result v14

    .line 589
    .local v14, "viewLocationX":F
    invoke-static {v12, v13}, Lcom/google/android/apps/books/util/LongPointF;->getY(J)F

    move-result v15

    .line 591
    .local v15, "viewLocationY":F
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v18

    mul-float v21, v21, v8

    const/high16 v22, 0x40000000    # 2.0f

    div-float v21, v21, v22

    add-float v5, v14, v21

    .line 592
    .local v5, "centerX":F
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v19

    mul-float v21, v21, v8

    const/high16 v22, 0x40000000    # 2.0f

    div-float v21, v21, v22

    add-float v6, v15, v21

    .line 596
    .local v6, "centerY":F
    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getCenterAsFractionOfScreenWidth()F

    move-result v2

    .line 597
    .local v2, "centerCompX":F
    mul-float v21, v11, v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mScrollX:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    sub-float v21, v21, v5

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTx:F

    .line 598
    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v10, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mScrollY:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    sub-float v21, v21, v6

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTy:F

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTx:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationX(F)V

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTy:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationY(F)V

    .line 602
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 576
    .end local v2    # "centerCompX":F
    .end local v5    # "centerX":F
    .end local v6    # "centerY":F
    .end local v8    # "scale":F
    .end local v9    # "scaleCompX":F
    .end local v10    # "scaledFullHeight":F
    .end local v11    # "scaledFullWidth":F
    .end local v12    # "viewLocation":J
    .end local v14    # "viewLocationX":F
    .end local v15    # "viewLocationY":F
    :cond_1
    const/high16 v9, 0x3f800000    # 1.0f

    goto/16 :goto_1
.end method


# virtual methods
.method public animateSkimFromFull()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 687
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/apps/books/widget/BookView;->mSkimAnimDuration:I

    invoke-direct {p0, v0, v2, v2, v1}, Lcom/google/android/apps/books/widget/BookView;->animateNavViewShowing(FFFI)V

    .line 688
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 739
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->clearContent()V

    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->clear()V

    .line 741
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 298
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->removeBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 300
    return-void
.end method

.method public getNavView()Lcom/google/android/apps/books/navigation/BookNavView;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    return-object v0
.end method

.method public getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 418
    return-object p0
.end method

.method public getVisibleMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 663
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->inGridMode()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    goto :goto_0
.end method

.method public hideNavView(Z)V
    .locals 14
    .param p1, "animate"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 465
    if-nez p1, :cond_0

    .line 466
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->onNavViewAnimationEnded()V

    .line 517
    :goto_0
    return-void

    .line 472
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/high16 v9, 0x10e0000

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 475
    .local v2, "duration":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->zoomCurrentSpread()Z

    move-result v8

    if-nez v8, :cond_1

    .line 478
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const-string v9, "alpha"

    new-array v10, v13, [F

    aput v11, v10, v12

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 479
    .local v0, "animator":Landroid/animation/Animator;
    new-array v8, v13, [Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    aput-object v9, v8, v12

    invoke-static {v0, v8}, Lcom/google/android/apps/books/util/ViewUtils;->useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V

    .line 480
    int-to-long v8, v2

    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 481
    new-instance v8, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v8}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v8}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 509
    :goto_1
    new-instance v8, Lcom/google/android/apps/books/widget/BookView$2;

    invoke-direct {v8, p0}, Lcom/google/android/apps/books/widget/BookView$2;-><init>(Lcom/google/android/apps/books/widget/BookView;)V

    invoke-virtual {v0, v8}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 516
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->startAnimator(Landroid/animation/Animator;)V

    goto :goto_0

    .line 484
    .end local v0    # "animator":Landroid/animation/Animator;
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->getScaleX()F

    move-result v5

    .line 485
    .local v5, "scale":F
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->getTranslationX()F

    move-result v6

    .line 486
    .local v6, "translationX":F
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->getTranslationY()F

    move-result v7

    .line 487
    .local v7, "translationY":F
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->resetNavView()V

    .line 489
    invoke-virtual {p0, v5, v6, v7, v11}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewAnimator(FFFF)Landroid/animation/Animator;

    move-result-object v4

    .line 491
    .local v4, "navAnimator":Landroid/animation/Animator;
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v8, v12}, Lcom/google/android/apps/books/navigation/BookNavView;->getLabelAlphaAnimator(Z)Landroid/animation/AnimatorSet;

    move-result-object v3

    .line 493
    .local v3, "labelAnimator":Landroid/animation/Animator;
    iget-object v8, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/books/navigation/BookNavView;->configureZoomToFullAnimator(Landroid/animation/Animator;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 495
    invoke-virtual {v4}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 501
    :goto_2
    new-array v8, v13, [Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    aput-object v9, v8, v12

    invoke-static {v4, v8}, Lcom/google/android/apps/books/util/ViewUtils;->useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V

    .line 503
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 504
    .local v1, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v8, 0x2

    new-array v8, v8, [Landroid/animation/Animator;

    aput-object v3, v8, v12

    aput-object v4, v8, v13

    invoke-virtual {v1, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 506
    move-object v0, v1

    .restart local v0    # "animator":Landroid/animation/Animator;
    goto :goto_1

    .line 498
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v1    # "animatorSet":Landroid/animation/AnimatorSet;
    :cond_2
    invoke-virtual {v3}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v8

    const-wide/16 v10, 0x2

    div-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    goto :goto_2
.end method

.method public invalidatePageContent()V
    .locals 1

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->invalidatePageContent()V

    .line 390
    return-void
.end method

.method makeNavViewAnimator(FFFF)Landroid/animation/Animator;
    .locals 9
    .param p1, "scale"    # F
    .param p2, "translationX"    # F
    .param p3, "translationY"    # F
    .param p4, "abElevation"    # F

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 443
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 444
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v1, 0x5

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const-string v3, "scaleX"

    new-array v4, v7, [F

    aput p1, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const-string v3, "scaleY"

    new-array v4, v7, [F

    aput p1, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const-string v3, "translationX"

    new-array v4, v7, [F

    aput p2, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const-string v4, "translationY"

    new-array v5, v7, [F

    aput p3, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "actionBarElevation"

    new-array v4, v8, [F

    iget v5, p0, Lcom/google/android/apps/books/widget/BookView;->mActionBarElevation:F

    aput v5, v4, v6

    aput p4, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 451
    return-object v0
.end method

.method public onBasePositionChanged()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->clearContent()V

    .line 306
    return-void
.end method

.method public onBookmarkChanged(IZ)V
    .locals 2
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "isGainingBookmark"    # Z

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BookView;->offsetFromBasePositionToScreenOffset(I)I

    move-result v0

    .line 399
    .local v0, "screenOffset":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->isVisibleOffset(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->onBookmarkChanged(I)V

    .line 402
    :cond_0
    return-void
.end method

.method public onBookmarksChanged()V
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->onBookmarksChanged()V

    .line 406
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 261
    const v0, 0x7f0e00d6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBackground:Landroid/view/View;

    .line 262
    const v0, 0x7f0e00c1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/BookNavView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    .line 264
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 265
    return-void
.end method

.method public onPageOffsetChanged(I)V
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 312
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v2

    .line 313
    .local v2, "nearbyOffsets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 314
    .local v0, "contentCount":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "contentIndex":I
    :goto_0
    if-ltz v1, :cond_1

    .line 315
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 316
    .local v3, "pageOffset":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 317
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookView;->mPageOffsetToContent:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 314
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 321
    .end local v3    # "pageOffset":I
    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/BookView;->updateScreenOffset(I)V

    .line 322
    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/BookView;->mDisplayTwoPages:Z

    if-eqz v4, :cond_2

    .line 323
    const/4 v4, -0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/BookView;->updateScreenOffset(I)V

    .line 325
    :cond_2
    return-void
.end method

.method public pinchToSkim(F)V
    .locals 12
    .param p1, "detectorScale"    # F

    .prologue
    .line 610
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_0

    .line 611
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/apps/books/widget/BookView;->mPinchingNavView:Z

    .line 612
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/navigation/BookNavView;->setLabelsVisible(Z)V

    .line 614
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v9}, Lcom/google/android/apps/books/navigation/BookNavView;->getScaleX()F

    move-result v0

    .line 615
    .local v0, "curScale":F
    const/4 v9, 0x0

    cmpl-float v9, p1, v9

    if-ltz v9, :cond_1

    .line 616
    mul-float v6, p1, v0

    .line 617
    .local v6, "newScale":F
    const/high16 v9, 0x3f800000    # 1.0f

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    invoke-static {v6, v9, v10}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v6

    .line 618
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v9, v6}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleX(F)V

    .line 619
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v9, v6}, Lcom/google/android/apps/books/navigation/BookNavView;->setScaleY(F)V

    .line 621
    const/high16 v9, 0x3f800000    # 1.0f

    sub-float v9, v6, v9

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    div-float v4, v9, v10

    .line 622
    .local v4, "lambda":F
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTx:F

    mul-float v7, v4, v9

    .line 623
    .local v7, "tx":F
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTy:F

    mul-float v8, v4, v9

    .line 624
    .local v8, "ty":F
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v9, v7}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationX(F)V

    .line 625
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v9, v8}, Lcom/google/android/apps/books/navigation/BookNavView;->setTranslationY(F)V

    .line 627
    const/high16 v9, 0x41800000    # 16.0f

    mul-float/2addr v9, v4

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/widget/BookView;->setActionBarElevation(F)V

    .line 654
    .end local v0    # "curScale":F
    .end local v4    # "lambda":F
    .end local v6    # "newScale":F
    .end local v7    # "tx":F
    .end local v8    # "ty":F
    :cond_0
    :goto_0
    return-void

    .line 630
    .restart local v0    # "curScale":F
    :cond_1
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/google/android/apps/books/widget/BookView;->mPinchingNavView:Z

    .line 631
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->onBusyEnded()V

    .line 633
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    const/high16 v10, 0x3f800000    # 1.0f

    add-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float v5, v9, v10

    .line 634
    .local v5, "midScale":F
    cmpg-float v9, v0, v5

    if-gtz v9, :cond_2

    const/4 v3, 0x1

    .line 635
    .local v3, "goToSkim":Z
    :goto_1
    if-eqz v3, :cond_3

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float v9, v0, v9

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    div-float v2, v9, v10

    .line 638
    .local v2, "fractionLeft":F
    :goto_2
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mMinSkimAnimDuration:I

    int-to-float v9, v9

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mSkimAnimDuration:I

    iget v11, p0, Lcom/google/android/apps/books/widget/BookView;->mMinSkimAnimDuration:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v2

    add-float/2addr v9, v10

    float-to-int v1, v9

    .line 640
    .local v1, "duration":I
    if-eqz v3, :cond_4

    .line 641
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v9, v10, v11, v1}, Lcom/google/android/apps/books/widget/BookView;->animateNavViewShowing(FFFI)V

    goto :goto_0

    .line 634
    .end local v1    # "duration":I
    .end local v2    # "fractionLeft":F
    .end local v3    # "goToSkim":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 635
    .restart local v3    # "goToSkim":Z
    :cond_3
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    sub-float/2addr v9, v0

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    div-float v2, v9, v10

    goto :goto_2

    .line 645
    .restart local v1    # "duration":I
    .restart local v2    # "fractionLeft":F
    :cond_4
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v9, :cond_5

    .line 646
    iget-object v9, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->setSystemBarsVisible(Z)V

    .line 649
    :cond_5
    iget v9, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewScale:F

    iget v10, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTx:F

    iget v11, p0, Lcom/google/android/apps/books/widget/BookView;->mZoomedUpNavViewTy:F

    invoke-direct {p0, v9, v10, v11, v1}, Lcom/google/android/apps/books/widget/BookView;->animateNavViewHiding(FFFI)V

    goto :goto_0
.end method

.method public prepare(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Lcom/google/android/apps/books/widget/BookView$Callbacks;ZII)V
    .locals 3
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p3, "displayTwoPages"    # Z
    .param p4, "readingBgColor"    # I
    .param p5, "navViewBgColor"    # I

    .prologue
    .line 272
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 273
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/BookView;->mDisplayTwoPages:Z

    .line 274
    iput p4, p0, Lcom/google/android/apps/books/widget/BookView;->mReadingBgColor:I

    .line 275
    iput p5, p0, Lcom/google/android/apps/books/widget/BookView;->mNavViewBgColor:I

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mBackground:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/books/widget/BookView;->mReadingBgColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->removeView(Landroid/view/View;)V

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/widget/BookView;->mReadingBgColor:I

    invoke-static {p1, v0, p3, v1}, Lcom/google/android/apps/books/widget/BaseSpreadView;->create(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Landroid/content/Context;ZI)Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView;->mMainSpread:Lcom/google/android/apps/books/widget/BaseSpreadView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/widget/BookView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 287
    return-void
.end method

.method public scaleSkimToFull()V
    .locals 0

    .prologue
    .line 680
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->zoomCurrentSpread()Z

    .line 681
    return-void
.end method

.method public setPageLoading(ILandroid/graphics/Point;)V
    .locals 1
    .param p1, "offset"    # I
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 329
    invoke-static {p2}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->loading(Landroid/graphics/Point;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BookView;->setPageData(ILcom/google/android/apps/books/widget/BookView$PageDrawingData;)V

    .line 330
    return-void
.end method

.method public setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
    .locals 3
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p4, "transitionStartTime"    # J

    .prologue
    .line 358
    const-string v0, "BookView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const-string v0, "BookView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is normal page"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_0
    invoke-static {p2, p3}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->normalPage(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BookView;->setPageData(ILcom/google/android/apps/books/widget/BookView$PageDrawingData;)V

    .line 363
    return-void
.end method

.method public setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V
    .locals 3
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "type"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "displaySize"    # Landroid/graphics/Point;
    .param p5, "transitionStartTime"    # J

    .prologue
    .line 369
    const-string v0, "BookView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    const-string v0, "BookView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_0
    invoke-static {p2, p3, p4}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BookView;->setPageData(ILcom/google/android/apps/books/widget/BookView$PageDrawingData;)V

    .line 373
    return-void
.end method

.method public setZoom(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 423
    iput p1, p0, Lcom/google/android/apps/books/widget/BookView;->mScale:F

    .line 424
    iput p2, p0, Lcom/google/android/apps/books/widget/BookView;->mScrollX:F

    .line 425
    iput p3, p0, Lcom/google/android/apps/books/widget/BookView;->mScrollY:F

    .line 426
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BaseSpreadView;->scaleAndScroll(FFF)V

    .line 427
    return-void
.end method

.method public showSkimView()V
    .locals 1

    .prologue
    .line 672
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookView;->resetNavView()V

    .line 673
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/BookView;->makeNavViewVisible(Z)V

    .line 674
    return-void
.end method
