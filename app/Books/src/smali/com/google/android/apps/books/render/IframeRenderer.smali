.class public Lcom/google/android/apps/books/render/IframeRenderer;
.super Lcom/google/android/apps/books/render/TextModeReaderRenderer;
.source "IframeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/IframeRenderer$4;,
        Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;,
        Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mCanFitWidth:Z

.field private mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

.field private final mDisplayDensity:F

.field private final mDisplayTwoPages:Z

.field private final mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

.field private final mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMarginPercentage:I

.field private final mMaxPageHeight:I

.field private final mMaxPageWidth:I

.field private final mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

.field private final mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

.field private final mPageMatrix:Landroid/graphics/Matrix;

.field private final mPageMatrixInv:Landroid/graphics/Matrix;

.field private final mPagesRectF:[Landroid/graphics/RectF;

.field private mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

.field private final mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field private final mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final mShouldFitWidth:Z

.field private mSnapshotBitmap:Landroid/graphics/Bitmap;

.field private final mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

.field private final mTempPageBoundsRectF:Landroid/graphics/RectF;

.field private final mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

.field final mTempPageIndices1:[I

.field final mTempPageIndices2:[I

.field private mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/util/Logger;Landroid/graphics/Point;ZILandroid/view/ViewGroup;)V
    .locals 25
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p5, "screenSize"    # Landroid/graphics/Point;
    .param p6, "makePagesAccessible"    # Z
    .param p7, "marginNoteIconTapSize"    # I
    .param p8, "webViewParent"    # Landroid/view/ViewGroup;

    .prologue
    .line 144
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;I)V

    .line 104
    new-instance v18, Landroid/graphics/Matrix;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    .line 105
    new-instance v18, Landroid/graphics/Matrix;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrixInv:Landroid/graphics/Matrix;

    .line 359
    new-instance v18, Lcom/google/android/apps/books/render/IframeRenderer$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/IframeRenderer$3;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    .line 388
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    .line 389
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices2:[I

    .line 414
    new-instance v18, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    .line 509
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Lcom/google/android/apps/books/render/PageIdentifier;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 629
    new-instance v18, Landroid/graphics/RectF;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageBoundsRectF:Landroid/graphics/RectF;

    .line 146
    const-string v18, "must have Metadata"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v18, "must have WebView"

    move-object/from16 v0, p8

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const-string v18, "missing account"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/accounts/Account;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mAccount:Landroid/accounts/Account;

    .line 149
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 150
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 152
    invoke-virtual/range {p8 .. p8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 154
    .local v5, "context":Landroid/content/Context;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-static {v5, v0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->create(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-object/from16 v18, v0

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setBackgroundColor(I)V

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/render/JavaScriptExecutors;->newExecutor(Landroid/webkit/WebView;)Lcom/google/android/apps/books/render/JavaScriptExecutor;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v20

    move-object/from16 v0, p8

    move-object/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 160
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 163
    .local v10, "resources":Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/google/android/apps/books/util/ReaderUtils;->currentlyInLandscape(Landroid/content/Context;)Z

    move-result v8

    .line 164
    .local v8, "inLandscape":Z
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeManifest;->getRenditionSpread()Ljava/lang/String;

    move-result-object v16

    .line 165
    .local v16, "spread":Ljava/lang/String;
    if-eqz v16, :cond_0

    const-string v18, "auto"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 167
    :cond_0
    move-object/from16 v0, p0

    iput-boolean v8, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mCanFitWidth:Z

    .line 168
    if-eqz v8, :cond_7

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v18

    if-eqz v18, :cond_7

    const/16 v18, 0x1

    :goto_0
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mShouldFitWidth:Z

    .line 169
    if-eqz v8, :cond_8

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v18

    if-nez v18, :cond_8

    const/16 v18, 0x1

    :goto_1
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    .line 184
    .end local v8    # "inLandscape":Z
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    :goto_3
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    .line 185
    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    .line 186
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move/from16 v18, v0

    if-lez v18, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    move/from16 v18, v0

    if-gtz v18, :cond_3

    .line 187
    :cond_2
    const-string v18, "IframeRenderer"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "invalid dimensions: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "x"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_3
    sget-object v18, Lcom/google/android/apps/books/util/ConfigValue;->WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_f

    const/16 v17, 0x0

    .line 194
    .local v17, "viewLayerType":I
    :goto_4
    new-instance v18, Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    move/from16 v19, v0

    new-instance v20, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/IframeRenderer$ModelCallbacks;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/render/IframeRenderer$1;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    move/from16 v2, v17

    move/from16 v3, v19

    move-object/from16 v4, v20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;IZLcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    .line 196
    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayDensity:F

    .line 197
    const v18, 0x7f0c001b

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mMarginPercentage:I

    .line 199
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v15

    .line 202
    .local v15, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    const/4 v13, 0x0

    .line 203
    .local v13, "segmentIndex":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    .line 204
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_12

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/model/Segment;

    .line 205
    .local v11, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v11}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v14

    .line 206
    .local v14, "segmentW":I
    invoke-interface {v11}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v12

    .line 208
    .local v12, "segmentH":I
    if-gtz v14, :cond_4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    .line 209
    :cond_4
    if-gtz v12, :cond_5

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    .line 212
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move/from16 v18, v0

    mul-int v18, v18, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    move/from16 v19, v0

    mul-int v19, v19, v14

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_10

    const/4 v6, 0x1

    .line 214
    .local v6, "horizontalLetterBoxing":Z
    :goto_6
    if-eqz v6, :cond_11

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    move-object/from16 v18, v0

    new-instance v19, Landroid/graphics/RectF;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    move/from16 v22, v0

    mul-int v22, v22, v14

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    int-to-float v0, v12

    move/from16 v23, v0

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-direct/range {v19 .. v23}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v19, v18, v13

    .line 223
    :goto_7
    const-string v18, "IframeRenderer"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 224
    move v9, v13

    .line 225
    .local v9, "index":I
    const-string v18, "IframeRenderer"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "IFRAME["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    move-object/from16 v20, v0

    aget-object v20, v20, v9

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    .end local v9    # "index":I
    :cond_6
    add-int/lit8 v13, v13, 0x1

    .line 228
    goto/16 :goto_5

    .line 168
    .end local v6    # "horizontalLetterBoxing":Z
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v11    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v12    # "segmentH":I
    .end local v13    # "segmentIndex":I
    .end local v14    # "segmentW":I
    .end local v15    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .end local v17    # "viewLayerType":I
    .restart local v8    # "inLandscape":Z
    :cond_7
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 169
    :cond_8
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 172
    :cond_9
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mCanFitWidth:Z

    .line 173
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mShouldFitWidth:Z

    .line 174
    const-string v18, "none"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    const/4 v8, 0x0

    .end local v8    # "inLandscape":Z
    :cond_a
    :goto_8
    move-object/from16 v0, p0

    iput-boolean v8, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    .line 179
    const-string v18, "IframeRenderer"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 180
    const-string v18, "IframeRenderer"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Publication spread of "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 174
    .restart local v8    # "inLandscape":Z
    :cond_b
    const-string v18, "both"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    const/4 v8, 0x1

    goto :goto_8

    :cond_c
    const-string v18, "landscape"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_a

    const-string v18, "portrait"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    if-nez v8, :cond_d

    const/4 v8, 0x1

    goto :goto_8

    :cond_d
    const/4 v8, 0x0

    goto :goto_8

    .line 184
    .end local v8    # "inLandscape":Z
    :cond_e
    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v18, v0

    goto/16 :goto_3

    .line 191
    :cond_f
    const/16 v17, 0x1

    goto/16 :goto_4

    .line 212
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v11    # "segment":Lcom/google/android/apps/books/model/Segment;
    .restart local v12    # "segmentH":I
    .restart local v13    # "segmentIndex":I
    .restart local v14    # "segmentW":I
    .restart local v15    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .restart local v17    # "viewLayerType":I
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 219
    .restart local v6    # "horizontalLetterBoxing":Z
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    move-object/from16 v18, v0

    new-instance v19, Landroid/graphics/RectF;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move/from16 v23, v0

    mul-int v23, v23, v12

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v14

    move/from16 v24, v0

    div-float v23, v23, v24

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-direct/range {v19 .. v23}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v19, v18, v13

    goto/16 :goto_7

    .line 230
    .end local v6    # "horizontalLetterBoxing":Z
    .end local v11    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v12    # "segmentH":I
    .end local v14    # "segmentW":I
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/IframeRenderer;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Z)V

    .line 232
    new-instance v18, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    move/from16 v20, v0

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v21

    invoke-direct/range {v18 .. v21}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;-><init>(Ljava/util/List;ZZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    .line 235
    new-instance v18, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    invoke-direct/range {v18 .. v20}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;-><init>(Ljava/util/List;I)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    .line 238
    new-instance v18, Lcom/google/android/apps/books/render/SpreadItems;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    move/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/IframeRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 239
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/IframeRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/IframeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/IframeRenderer;)[Landroid/graphics/RectF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/IframeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/IframeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p2, "x2"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "x3"    # Landroid/graphics/Canvas;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/render/IframeRenderer;->drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V

    return-void
.end method

.method private addMarginsToViewMatrix(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;I)V
    .locals 6
    .param p1, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p2, "passageIndex"    # I

    .prologue
    .line 699
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    aget-object v2, v3, p2

    .line 701
    .local v2, "rect":Landroid/graphics/RectF;
    sget-object v3, Lcom/google/android/apps/books/render/IframeRenderer$4;->$SwitchMap$com$google$android$apps$books$render$Renderer$PagePositionOnScreen:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 709
    const/4 v0, 0x0

    .line 712
    .local v0, "deltaX":I
    :goto_0
    iget v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    sub-int/2addr v3, v4

    div-int/lit8 v1, v3, 0x2

    .line 713
    .local v1, "deltaY":I
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    int-to-float v4, v0

    int-to-float v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 714
    return-void

    .line 703
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    :pswitch_0
    iget v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    iget v4, v2, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    sub-int/2addr v3, v4

    div-int/lit8 v0, v3, 0x2

    .line 704
    .restart local v0    # "deltaX":I
    goto :goto_0

    .line 706
    .end local v0    # "deltaX":I
    :pswitch_1
    iget v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    iget v4, v2, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 707
    .restart local v0    # "deltaX":I
    goto :goto_0

    .line 701
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "pageBounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 797
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 798
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 799
    .local v0, "time1":J
    invoke-virtual {p3}, Landroid/graphics/Canvas;->save()I

    .line 800
    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p3, v4}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 801
    iget v4, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget v5, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget v6, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v7, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-virtual {p3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 804
    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v4, p3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->snapShot(Landroid/graphics/Canvas;)V

    .line 805
    invoke-virtual {p3}, Landroid/graphics/Canvas;->restore()V

    .line 807
    const-string v4, "IframeRenderer"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 808
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 809
    .local v2, "time2":J
    const-string v4, "IframeRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pass:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Page:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Draw:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    .end local v0    # "time1":J
    .end local v2    # "time2":J
    :cond_0
    return-void
.end method

.method private static fixupTextZoom(Landroid/webkit/WebSettings;)V
    .locals 1
    .param p0, "webSettings"    # Landroid/webkit/WebSettings;

    .prologue
    .line 350
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 351
    return-void
.end method

.method private getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private static getPreOffsetBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/PageIdentifier;[I)V
    .locals 3
    .param p0, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "result"    # [I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 492
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v0

    aput v0, p2, v1

    .line 494
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v0

    aput v0, p2, v2

    .line 499
    :goto_0
    return-void

    .line 496
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    aput v0, p2, v1

    .line 497
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v0

    aput v0, p2, v2

    goto :goto_0
.end method

.method private prepareTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 8
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 635
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v1

    .line 636
    .local v1, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageBoundsRectF:Landroid/graphics/RectF;

    iget v4, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    int-to-float v4, v4

    iget v5, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    int-to-float v5, v5

    iget v6, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    int-to-float v6, v6

    iget v7, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 638
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v2

    .line 640
    .local v2, "passageIndex":I
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    .line 641
    .local v0, "fit":Landroid/graphics/Matrix$ScaleToFit;
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageBoundsRectF:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    aget-object v5, v5, v2

    invoke-virtual {v3, v4, v5, v0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 642
    const-string v3, "IframeRenderer"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 643
    const-string v3, "IframeRenderer"

    const-string v4, "Unable to consruct matrix transform"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_0
    const/4 v3, 0x0

    .line 647
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private prepareTransformInverse(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 626
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrixInv:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setup(Lcom/google/android/apps/books/model/VolumeMetadata;Z)V
    .locals 25
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "makePagesAccessible"    # Z

    .prologue
    .line 246
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getContext()Landroid/content/Context;

    move-result-object v19

    .line 247
    .local v19, "context":Landroid/content/Context;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/IframeRenderer;->setupWebViews(Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 249
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 252
    .local v18, "compileJs":Ljava/lang/String;
    const-string v24, "file:///android_asset/"

    .line 253
    .local v24, "readerUrl":Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "compiled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 255
    new-instance v23, Lcom/google/android/apps/books/render/LruPassagePurger;

    const/16 v2, 0x11

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/LruPassagePurger;-><init>(I)V

    .line 264
    .local v23, "passagePurger":Lcom/google/android/apps/books/render/LruPassagePurger;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".html"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->loadUrl(Ljava/lang/String;)V

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setPassagePurger(Lcom/google/android/apps/books/render/LruPassagePurger;)V

    .line 268
    move/from16 v10, p2

    .line 269
    .local v10, "sendPageText":Z
    const/16 v21, 0x0

    .line 270
    .local v21, "marginPercentTopBottom":I
    const/16 v20, 0x0

    .line 271
    .local v20, "marginPercentSides":I
    const/16 v22, 0x1

    .line 276
    .local v22, "pagesPerViewport":I
    new-instance v15, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    invoke-direct {v15, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 277
    .local v15, "viewportSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getFirstContentPassageIndex()I

    move-result v17

    .line 278
    .local v17, "firstContentPassageIndex":I
    new-instance v1, Lcom/google/android/apps/books/util/JsConfiguration;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterSegmentJsonArray()Lorg/json/JSONArray;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterCssJsonArray()Lorg/json/JSONArray;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getBakedCssFilesJsonArray()Lorg/json/JSONArray;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSharedFontsUrisPlus()Lorg/json/JSONArray;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFixedLayoutJsonArray()Lorg/json/JSONArray;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v11, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v9, v11}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v9

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayDensity:F

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getFontTestString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/books/util/JsConfiguration;-><init>(Landroid/accounts/Account;Ljava/lang/String;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;ZZFIIILandroid/graphics/Point;Ljava/lang/String;I)V

    .line 285
    .local v1, "configData":Lcom/google/android/apps/books/util/JsConfiguration;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setJsConfiguration(Lcom/google/android/apps/books/util/JsConfiguration;)V

    .line 286
    return-void

    .line 256
    .end local v1    # "configData":Lcom/google/android/apps/books/util/JsConfiguration;
    .end local v10    # "sendPageText":Z
    .end local v15    # "viewportSize":Landroid/graphics/Point;
    .end local v17    # "firstContentPassageIndex":I
    .end local v20    # "marginPercentSides":I
    .end local v21    # "marginPercentTopBottom":I
    .end local v22    # "pagesPerViewport":I
    .end local v23    # "passagePurger":Lcom/google/android/apps/books/render/LruPassagePurger;
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_DEBUG_CONFIG_VALUE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 257
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "debug"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 258
    new-instance v23, Lcom/google/android/apps/books/render/LruPassagePurger;

    const/16 v2, 0x11

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/LruPassagePurger;-><init>(I)V

    .restart local v23    # "passagePurger":Lcom/google/android/apps/books/render/LruPassagePurger;
    goto/16 :goto_0

    .line 260
    .end local v23    # "passagePurger":Lcom/google/android/apps/books/render/LruPassagePurger;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/IframeRenderer;->createSideLoadFilePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v24

    .line 261
    new-instance v23, Lcom/google/android/apps/books/render/LruPassagePurger;

    const/16 v2, 0x11

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/LruPassagePurger;-><init>(I)V

    .restart local v23    # "passagePurger":Lcom/google/android/apps/books/render/LruPassagePurger;
    goto/16 :goto_0
.end method

.method private setupWebViews(Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 9
    .param p1, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 294
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v4, Lcom/google/android/apps/books/render/IframeRenderer$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/render/IframeRenderer$1;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setInvalidationListener(Ljava/lang/Runnable;)V

    .line 301
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setInitialScale(I)V

    .line 302
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 303
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setVerticalScrollBarEnabled(Z)V

    .line 305
    new-instance v3, Lcom/google/android/apps/books/render/BasicReaderController;

    new-instance v4, Lcom/google/android/apps/books/render/BasicTextReader;

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/render/BasicTextReader;-><init>(Lcom/google/android/apps/books/render/JavaScriptExecutor;)V

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderDataModel()Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/apps/books/render/BasicReaderController;-><init>(Lcom/google/android/apps/books/render/TextReader;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/util/Logger;)V

    iput-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 308
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setJavascriptReader(Lcom/google/android/apps/books/render/ReaderController;)V

    .line 309
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/render/IframeRenderer;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 311
    const-string v3, "JsBridge"

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/BasicReaderController;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/ReaderListener;

    .line 314
    .local v1, "loggingListener":Lcom/google/android/apps/books/render/ReaderListener;
    new-instance v3, Lcom/google/android/apps/books/render/FetchingDataSource;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/apps/books/render/FetchingDataSource;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V

    iput-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    .line 315
    new-instance v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;-><init>(Lcom/google/android/apps/books/render/ReaderListener;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/render/ReaderDataSource;)V

    .line 317
    .local v0, "bridge":Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const-string v4, "bridge"

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 320
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v3

    if-nez v3, :cond_0

    .line 321
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v4, Lcom/google/android/apps/books/render/IframeRenderer$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/render/IframeRenderer$2;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 330
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    iget-object v6, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, p1}, Lcom/google/android/apps/books/render/FetchingDataSource;->configureWebView(Lcom/google/android/apps/books/render/SnapshottingWebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 333
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    .line 334
    .local v2, "webSettings":Landroid/webkit/WebSettings;
    invoke-static {v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setupGenericWebSettings(Landroid/webkit/WebSettings;)V

    .line 337
    invoke-virtual {v2, v8}, Landroid/webkit/WebSettings;->setMinimumFontSize(I)V

    .line 338
    invoke-virtual {v2, v8}, Landroid/webkit/WebSettings;->setMinimumLogicalFontSize(I)V

    .line 340
    invoke-static {v2}, Lcom/google/android/apps/books/render/IframeRenderer;->fixupTextZoom(Landroid/webkit/WebSettings;)V

    .line 341
    return-void
.end method

.method private subtractMarginsFromScreenPoint(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;I)V
    .locals 4
    .param p1, "p"    # Landroid/graphics/Point;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "passageIndex"    # I

    .prologue
    .line 719
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;

    aget-object v0, v1, p3

    .line 720
    .local v0, "rect":Landroid/graphics/RectF;
    sget-object v1, Lcom/google/android/apps/books/render/IframeRenderer$4;->$SwitchMap$com$google$android$apps$books$render$Renderer$PagePositionOnScreen:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 730
    :goto_0
    iget v1, p1, Landroid/graphics/Point;->y:I

    iget v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageHeight:I

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Point;->y:I

    .line 731
    return-void

    .line 722
    :pswitch_0
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    iget v3, v0, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Point;->x:I

    goto :goto_0

    .line 725
    :pswitch_1
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMaxPageWidth:I

    iget v3, v0, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Point;->x:I

    goto :goto_0

    .line 720
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->activateMediaElement(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 763
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setReaderSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 764
    return-void
.end method

.method public beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p4, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 569
    new-instance v7, Landroid/graphics/Point;

    float-to-int v0, p1

    float-to-int v3, p2

    invoke-direct {v7, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 570
    .local v7, "screenPoint":Landroid/graphics/Point;
    iget-object v0, p3, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-direct {p0, v7, p4, v0}, Lcom/google/android/apps/books/render/IframeRenderer;->subtractMarginsFromScreenPoint(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;I)V

    .line 571
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransformInverse(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    .line 573
    .local v1, "passageIndex":I
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v2

    .line 574
    .local v2, "pageIndex":I
    const/4 v0, 0x2

    new-array v6, v0, [F

    iget v0, v7, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    aput v0, v6, v4

    iget v0, v7, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    aput v0, v6, v5

    .line 575
    .local v6, "point":[F
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrixInv:Landroid/graphics/Matrix;

    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    aget v3, v6, v4

    aget v4, v6, v5

    const/16 v5, 0x32

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController;->loadNearbyText(IIFFI)V

    .line 579
    .end local v1    # "passageIndex":I
    .end local v2    # "pageIndex":I
    .end local v6    # "point":[F
    :cond_0
    return-void
.end method

.method public canFitWidth()Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mCanFitWidth:Z

    return v0
.end method

.method public canProvideText()Z
    .locals 1

    .prologue
    .line 590
    const/4 v0, 0x1

    return v0
.end method

.method public cancelPendingRequests()V
    .locals 2

    .prologue
    .line 368
    const-string v0, "IframeRenderer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "IframeRenderer"

    const-string v1, "Cancelling all render requests"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->cancelPendingRequests()V

    .line 372
    return-void
.end method

.method public clearAnnotationCaches()V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->clearAnnotationCaches()V

    .line 898
    return-void
.end method

.method public convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z
    .locals 4
    .param p1, "p"    # Landroid/graphics/Point;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 736
    iget-object v3, p2, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-direct {p0, p1, p3, v3}, Lcom/google/android/apps/books/render/IframeRenderer;->subtractMarginsFromScreenPoint(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;I)V

    .line 737
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransformInverse(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 738
    const/4 v3, 0x2

    new-array v0, v3, [F

    iget v3, p1, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    aput v3, v0, v2

    iget v3, p1, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    aput v3, v0, v1

    .line 739
    .local v0, "xy":[F
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrixInv:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 740
    aget v2, v0, v2

    float-to-int v2, v2

    aget v3, v0, v1

    float-to-int v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 743
    .end local v0    # "xy":[F
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->destroy()V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/FetchingDataSource;->shutDown()V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    .line 382
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->cancelPendingRequests()V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->clearPaginationData()V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->destroy()V

    .line 385
    invoke-super {p0}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->destroy()V

    .line 386
    return-void
.end method

.method public displayTwoPages()Z
    .locals 1

    .prologue
    .line 913
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    return v0
.end method

.method public findTouchableItem(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;I)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 15
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "rendererTouchPoint"    # Landroid/graphics/Point;
    .param p3, "typeMask"    # I

    .prologue
    .line 653
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->hasTouchables()Z

    move-result v1

    if-nez v1, :cond_1

    .line 654
    :cond_0
    const/4 v1, 0x0

    .line 693
    :goto_0
    return-object v1

    .line 657
    :cond_1
    const/4 v1, -0x1

    move/from16 v0, p3

    if-eq v0, v1, :cond_4

    const/4 v12, 0x1

    .line 659
    .local v12, "filtered":Z
    :goto_1
    const/4 v9, 0x0

    .line 660
    .local v9, "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    const v10, 0x7fffffff

    .line 661
    .local v10, "candidateDistance":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 664
    .local v3, "candidateBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/books/render/TouchableItem;

    .line 665
    .local v14, "item":Lcom/google/android/apps/books/render/TouchableItem;
    if-eqz v12, :cond_3

    iget v1, v14, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    and-int v1, v1, p3

    if-eqz v1, :cond_2

    .line 672
    :cond_3
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Point;->y:I

    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/books/util/MathUtils;->manhattanDistanceFromPointToRect(IILandroid/graphics/Rect;)I

    move-result v11

    .line 674
    .local v11, "distance":I
    if-nez v11, :cond_5

    .line 676
    new-instance v1, Lcom/google/android/apps/books/render/TouchableItem;

    iget v2, v14, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "candidateBounds":Landroid/graphics/Rect;
    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    iget-object v5, v14, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    iget-object v6, v14, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    iget v7, v14, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    iget-object v8, v14, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/books/render/TouchableItem;-><init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 657
    .end local v9    # "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    .end local v10    # "candidateDistance":I
    .end local v11    # "distance":I
    .end local v12    # "filtered":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_4
    const/4 v12, 0x0

    goto :goto_1

    .line 680
    .restart local v3    # "candidateBounds":Landroid/graphics/Rect;
    .restart local v9    # "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    .restart local v10    # "candidateDistance":I
    .restart local v11    # "distance":I
    .restart local v12    # "filtered":Z
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_5
    if-ge v11, v10, :cond_2

    .line 681
    move-object v9, v14

    .line 682
    iget-object v1, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 683
    move v10, v11

    goto :goto_2

    .line 687
    .end local v11    # "distance":I
    .end local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_6
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->linkTapThresholdInPixels()I

    move-result v1

    if-ge v10, v1, :cond_7

    .line 689
    new-instance v1, Lcom/google/android/apps/books/render/TouchableItem;

    iget v2, v9, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    iget-object v4, v9, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    iget-object v5, v9, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    iget-object v6, v9, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    iget v7, v9, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    iget-object v8, v9, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/books/render/TouchableItem;-><init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 693
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 867
    .local p3, "visibleLayers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v0

    invoke-interface {p2, v0, p3}, Lcom/google/android/apps/books/widget/AnnotationIndex;->getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDecorationInsetFromPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering;)I
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 840
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 841
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v0, :cond_0

    .line 842
    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v3, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    sub-int v1, v2, v3

    .line 846
    .local v1, "pageWidth":I
    :goto_0
    iget v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMarginPercentage:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    return v2

    .line 844
    .end local v1    # "pageWidth":I
    :cond_0
    const/16 v1, 0x384

    .restart local v1    # "pageWidth":I
    goto :goto_0
.end method

.method public getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;
    .locals 2

    .prologue
    .line 442
    new-instance v0, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;

    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getFirstForbiddenPassageIndex()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;-><init>(Lcom/google/android/apps/books/render/TextModeReaderRenderer;I)V

    return-object v0
.end method

.method public getGridRowCount()I
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->getGridRowCount()I

    move-result v0

    return v0
.end method

.method public getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 4
    .param p1, "gridRowIndex"    # I

    .prologue
    .line 434
    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->gridRowIndexToStartBookPageIndex(I)I

    move-result v1

    .line 436
    .local v1, "startBookPageIndex":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Page;

    .line 437
    .local v0, "page":Lcom/google/android/apps/books/model/Page;
    new-instance v2, Lcom/google/android/apps/books/common/Position;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v2
.end method

.method public getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .param p3, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getHighlightRects(I)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    return-object v0
.end method

.method public getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "marginIconTapSize"    # I
    .param p4, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "IZ)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/MarginNote;",
            ">;"
        }
    .end annotation

    .prologue
    .line 892
    .local p1, "candidateAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getMarginNoteIcons(Ljava/util/Collection;Lcom/google/android/apps/books/widget/DevicePageRendering;Z)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    return-object v0
.end method

.method public getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/FixedLayoutPageHandle;
    .locals 5
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    .line 419
    .local v0, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPreOffsetBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/PageIdentifier;[I)V

    .line 420
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToContent(IILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget-object v1, v1, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-nez v1, :cond_0

    .line 422
    new-instance v1, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;

    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget v2, v2, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->bookPageIndex:I

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;-><init>(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;I)V

    .line 424
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    goto :goto_0
.end method

.method public bridge synthetic getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/FixedLayoutPageHandle;

    move-result-object v0

    return-object v0
.end method

.method public getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "result"    # Landroid/graphics/Matrix;

    .prologue
    .line 749
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/render/IframeRenderer;->addMarginsToViewMatrix(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;I)V

    .line 751
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p3, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 752
    const/4 v0, 0x1

    .line 757
    :goto_0
    return v0

    .line 754
    :cond_0
    const-string v0, "IframeRenderer"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 755
    const-string v0, "IframeRenderer"

    const-string v1, "getViewToPageMatrix prepareTransform failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 506
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 7
    .param p1, "p1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "p2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    .line 394
    .local v0, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v0, :cond_0

    .line 395
    const v1, 0x7fffffff

    .line 399
    :goto_0
    return v1

    .line 397
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPreOffsetBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/PageIdentifier;[I)V

    .line 398
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices2:[I

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPreOffsetBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/PageIdentifier;[I)V

    .line 399
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices1:[I

    aget v3, v3, v6

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices2:[I

    aget v4, v4, v5

    iget-object v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIndices2:[I

    aget v5, v5, v6

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenPageDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIII)I

    move-result v1

    goto :goto_0
.end method

.method public getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I
    .locals 7
    .param p1, "s1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "s2"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v6

    .line 408
    .local v6, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    iget-object v0, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v6, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    .line 409
    .local v1, "p1BookPageIndex":I
    iget-object v0, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v6, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v3

    .line 410
    .local v3, "p2BookPageIndex":I
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget v2, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v4, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget-boolean v5, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIIIZ)I

    move-result v0

    return v0
.end method

.method public getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 1
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    .local p2, "result":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/PageHandle;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/render/IframeRenderer;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/render/SpreadItems;->set(Lcom/google/android/apps/books/render/SpreadItems;)V

    .line 537
    return-object p2
.end method

.method public getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 5
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 516
    .local p2, "result":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/TextPageHandle;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget-boolean v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-static {v1, v2, p1, v3, v4}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->spreadIdentifierToPageIdentifiers(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadIdentifier;Z[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 518
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    if-eqz v1, :cond_0

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/FixedLayoutPageHandle;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/IframeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/FixedLayoutPageHandle;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 530
    :goto_0
    return-object p2

    .line 522
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/IframeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/FixedLayoutPageHandle;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "IframeRenderer"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 526
    const-string v1, "IframeRenderer"

    const-string v2, "Error in getTextSpreadPageHandles"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 528
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    iget-boolean v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->fill(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public isPassageForbidden(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isPassageForbidden(I)Z

    move-result v0

    return v0
.end method

.method public loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I
    .locals 9
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "needSelectionData"    # Z
    .param p4, "handleIndex"    # I
    .param p5, "deltaX"    # I
    .param p6, "deltaY"    # I
    .param p7, "prevRequest"    # I
    .param p8, "startSelection"    # Z

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/render/WebViewRendererModel;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I

    move-result v8

    .line 608
    .local v8, "result":I
    return v8
.end method

.method public loadRangeDataBulk(ILjava/util/Map;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 618
    .local p2, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->loadRangeDataBulk(ILjava/util/Map;)I

    move-result v0

    return v0
.end method

.method public onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 583
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    .line 584
    .local v1, "passageIndex":I
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v0

    .line 585
    .local v0, "pageIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v2, v1, v0, p1, p2}, Lcom/google/android/apps/books/render/BasicReaderController;->loadNotNormalizedRangeData(IIII)I

    .line 586
    return-void
.end method

.method public onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;)V
    .locals 3
    .param p1, "annotationRect"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget v1, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mMarginNoteIconTapSize:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isVertical()Z

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;IZ)V

    .line 904
    return-void
.end method

.method public pageBoundsInRendererCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    const/4 v3, 0x0

    .line 824
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 825
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v0, :cond_0

    .line 826
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget v3, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget v4, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v5, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 832
    :goto_0
    return-object v1

    .line 829
    :cond_0
    const-string v1, "IframeRenderer"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 830
    const-string v1, "IframeRenderer"

    const-string v2, "pageBoundsInRendererCoordinates: no bounds yet"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z
    .locals 1
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z

    move-result v0

    return v0
.end method

.method public rendererCoordinatesToPagePointsMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Matrix;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "canvasDims"    # Landroid/graphics/Point;

    .prologue
    .line 819
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/IframeRenderer;->prepareTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMatrix:Landroid/graphics/Matrix;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v0

    return v0
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 545
    iget-object v3, p1, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    if-eqz v3, :cond_1

    .line 547
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget-object v5, p1, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    iget-boolean v6, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mDisplayTwoPages:Z

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 550
    .local v1, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    new-instance v2, Lcom/google/android/apps/books/render/RenderPosition;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/books/render/RenderPosition;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    iget v5, p1, Lcom/google/android/apps/books/render/RenderPosition;->sampleSize:I

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    .end local v1    # "page":Lcom/google/android/apps/books/render/PageIdentifier;
    .local v2, "pageRequest":Lcom/google/android/apps/books/render/RenderPosition;
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v3, v2, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    .line 563
    .end local v2    # "pageRequest":Lcom/google/android/apps/books/render/RenderPosition;
    :goto_1
    return-void

    .line 552
    :catch_0
    move-exception v0

    .line 553
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, "IframeRenderer"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 554
    const-string v3, "IframeRenderer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error interpreting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 556
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/IframeRenderer;->dispatchRenderError(Ljava/lang/Exception;)V

    goto :goto_1

    .line 560
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_1
    move-object v2, p1

    .restart local v2    # "pageRequest":Lcom/google/android/apps/books/render/RenderPosition;
    goto :goto_0
.end method

.method public setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V
    .locals 1
    .param p1, "range"    # Lcom/google/android/apps/books/render/Renderer$PageRange;

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V

    .line 852
    return-void
.end method

.method public setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 873
    .local p1, "highlightsRectsCache":Lcom/google/android/apps/books/widget/PaintableRectsCache;, "Lcom/google/android/apps/books/widget/PaintableRectsCache<+Lcom/google/android/apps/books/widget/WalkableHighlightRects;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V

    .line 874
    return-void
.end method

.method public setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 357
    return-void
.end method

.method public shouldFitWidth()Z
    .locals 1

    .prologue
    .line 923
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mShouldFitWidth:Z

    return v0
.end method

.method public weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .prologue
    .line 908
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->weaklyAddOnLoadedRangeDataBulkListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V

    .line 909
    return-void
.end method
