.class public final Lcom/google/android/apps/books/provider/BooksContract$Pages;
.super Ljava/lang/Object;
.source "BooksContract.java"

# interfaces
.implements Lcom/google/android/apps/books/provider/BooksContract$PageColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pages"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    }
.end annotation


# direct methods
.method public static buildPageDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2812
    const-string v0, "Missing Account"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2813
    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildPageDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2766
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->builder([Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildPageUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 2820
    const-string v0, "Missing Account"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2821
    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildPageUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "baseUri"    # Landroid/net/Uri;
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 2782
    const-string v0, "Valid page required"

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2783
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildPageUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 2773
    const-string v0, "Valid page required"

    invoke-static {p2, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2774
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->builder([Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static checkPageUri(Landroid/net/Uri;)V
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2831
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 2832
    .local v0, "match":I
    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x192

    if-eq v0, v1, :cond_0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not a page Uri, match="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 2835
    return-void

    .line 2832
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static checkUri(Landroid/net/Uri;)V
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2825
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 2826
    .local v0, "match":I
    const/16 v1, 0x190

    if-eq v0, v1, :cond_0

    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x192

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not a pages Uri, match="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 2828
    return-void

    .line 2826
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "getPageId"    # Z

    .prologue
    .line 2874
    if-eqz p1, :cond_0

    .line 2875
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->checkPageUri(Landroid/net/Uri;)V

    .line 2879
    :goto_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 2880
    .local v0, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz p1, :cond_1

    const/4 v3, 0x5

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    :goto_1
    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 2877
    .end local v0    # "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->checkUri(Landroid/net/Uri;)V

    goto :goto_0

    .line 2880
    .restart local v0    # "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
