.class public interface abstract Lcom/google/android/apps/books/render/RenderResponseConsumer;
.super Ljava/lang/Object;
.source "RenderResponseConsumer.java"


# virtual methods
.method public abstract isPurgeable()Z
.end method

.method public abstract onMissingPosition()V
.end method

.method public abstract onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
.end method

.method public abstract onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
.end method
