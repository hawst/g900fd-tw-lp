.class Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HomeMenu$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/HomeFragment$1;

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getVolumes()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 405
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    .line 407
    .local v0, "bds":Lcom/google/android/apps/books/model/BooksDataStore;
    if-eqz v0, :cond_0

    .line 409
    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumes(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 417
    .end local v0    # "bds":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_0
    :goto_0
    return-object v3

    .line 410
    .restart local v0    # "bds":Lcom/google/android/apps/books/model/BooksDataStore;
    :catch_0
    move-exception v2

    .line 411
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "HomeFragment"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 412
    const-string v4, "HomeFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "problem loading volume list: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSearchSuggestionSelected(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_SEARCH_SUGGESTION:Lcom/google/android/apps/books/app/BookOpeningFlags;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 433
    return-void
.end method

.method public startRefresh()V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startForcedSync()V

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1300(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 446
    return-void
.end method

.method public startSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    const-string v1, "books_inapp_home_search_online"

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startSearch(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1300(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 439
    return-void
.end method
