.class public abstract Lcom/google/android/apps/books/app/BaseBooksActivity;
.super Lcom/google/android/apps/books/app/AnalyticsActivity;
.source "BaseBooksActivity.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Lcom/google/android/apps/books/app/AccountPicker$HostActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;,
        Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/app/BooksFragmentCallbacks;",
        ">",
        "Lcom/google/android/apps/books/app/AnalyticsActivity;",
        "Landroid/accounts/OnAccountsUpdateListener;",
        "Lcom/google/android/apps/books/app/AccountPicker$HostActivity;"
    }
.end annotation


# static fields
.field private static final sStubFragmentCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private final mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mAccountPicker:Lcom/google/android/apps/books/app/AccountPicker;

.field private final mAccountPickerCallbacks:Lcom/google/android/apps/books/app/AccountPicker$Callbacks;

.field private mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

.field private mBrowserAuthenticationRequestId:I

.field private mConfig:Lcom/google/android/apps/books/util/Config;

.field private mConsumedIntentAccount:Z

.field private mCurrentReaderTheme:Ljava/lang/String;

.field private mHasShownUnsupportedCountryDialog:Z

.field private mIsDestroyed:Z

.field private mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

.field private mPendingPurchase:Lcom/google/android/apps/books/util/PurchaseHelper;

.field private final mSyncEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/apps/books/app/StubFragmentCallbacks;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity;->sStubFragmentCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;-><init>()V

    .line 127
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mIsDestroyed:Z

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    .line 135
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConsumedIntentAccount:Z

    .line 138
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/BaseBooksActivity$1;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 223
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mHasShownUnsupportedCountryDialog:Z

    .line 462
    invoke-static {}, Lcom/google/android/apps/books/app/AccountPickers;->getPicker()Lcom/google/android/apps/books/app/AccountPicker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountPicker:Lcom/google/android/apps/books/app/AccountPicker;

    .line 474
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 1012
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/BaseBooksActivity$3;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    invoke-static {v0}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 1067
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/BaseBooksActivity$4;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountPickerCallbacks:Lcom/google/android/apps/books/app/AccountPicker$Callbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 70
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mHasShownUnsupportedCountryDialog:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/books/app/BaseBooksActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mHasShownUnsupportedCountryDialog:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/sync/SyncAccountsState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->handleAccountPickerResult(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/BaseBooksActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    return v0
.end method

.method static synthetic access$208(Lcom/google/android/apps/books/app/BaseBooksActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startManualSync()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;)Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/accounts/Account;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->setAccount(Landroid/accounts/Account;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->showAccountPicker()V

    return-void
.end method

.method protected static addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 3
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "ft"    # Landroid/support/v4/app/FragmentTransaction;

    .prologue
    .line 214
    const-string v0, "BooksActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "BooksActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    invoke-virtual {p2, p0, p1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 218
    return-void
.end method

.method private considerNaggingAboutSync()V
    .locals 8

    .prologue
    .line 744
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {p0, v5}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/sync/SyncController;->getSyncAutomatically()Z

    move-result v1

    .line 746
    .local v1, "autoSync":Z
    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    iget-object v6, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/sync/SyncAccountsState;->haveNagged(Ljava/lang/String;)Z

    move-result v4

    .line 748
    .local v4, "haveNagged":Z
    if-nez v1, :cond_0

    if-nez v4, :cond_0

    .line 751
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 752
    .local v2, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 754
    .local v3, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v5}, Lcom/google/android/apps/books/util/LoaderParams;->buildFrom(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v0

    .line 755
    .local v0, "args":Landroid/os/Bundle;
    const-class v5, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v0, v3, v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 757
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 759
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v2    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method protected static createAndAddFragment(ILjava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p0, "containerViewId"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "args"    # Landroid/os/Bundle;
    .param p4, "ft"    # Landroid/support/v4/app/FragmentTransaction;
    .param p5, "retainInstance"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Landroid/support/v4/app/Fragment;",
            ">(I",
            "Ljava/lang/Class",
            "<TU;>;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Landroid/support/v4/app/FragmentTransaction;",
            "Z)TU;"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TU;>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 187
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    const-string v2, "BooksActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    const-string v2, "BooksActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding fragment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    invoke-virtual {v1, p3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 191
    invoke-virtual {v1, p5}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 192
    invoke-virtual {p4, p0, v1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 193
    return-object v1

    .line 194
    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/ReflectiveOperationException;
    :goto_0
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 194
    .end local v0    # "e":Ljava/lang/ReflectiveOperationException;
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected static createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "arguments"    # Landroid/os/Bundle;
    .param p3, "ft"    # Landroid/support/v4/app/FragmentTransaction;
    .param p4, "retainInstance"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Landroid/support/v4/app/Fragment;",
            ">(",
            "Ljava/lang/Class",
            "<TU;>;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Landroid/support/v4/app/FragmentTransaction;",
            "Z)TU;"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TU;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 204
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1, p4}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 205
    invoke-virtual {v1, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 206
    invoke-static {v1, p1, p3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/support/v4/app/FragmentTransaction;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 207
    return-object v1

    .line 208
    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/ReflectiveOperationException;
    :goto_0
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 208
    .end local v0    # "e":Ljava/lang/ReflectiveOperationException;
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private findAccount()V
    .locals 9

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x3

    .line 557
    const-string v5, "BooksActivity"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 558
    const-string v5, "BooksActivity"

    const-string v7, "Starting account search"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->isInProgress()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 566
    const-string v5, "BooksActivity"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 567
    const-string v5, "BooksActivity"

    const-string v6, "Account search already in progress"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :cond_1
    :goto_0
    return-void

    .line 572
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/books/util/AccountUtils;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .line 573
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v5, v2

    if-nez v5, :cond_3

    .line 574
    new-instance v5, Lcom/google/android/apps/books/app/BaseBooksActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/app/BaseBooksActivity$2;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    const v6, 0x7f0f0119

    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/books/util/AccountUtils;->showAddAccount(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 586
    :cond_3
    const/4 v3, 0x0

    .line 587
    .local v3, "defaultToAny":Z
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConsumedIntentAccount:Z

    if-eqz v5, :cond_6

    const/4 v0, 0x0

    .line 588
    .local v0, "accountFromIntent":Landroid/accounts/Account;
    :goto_1
    invoke-static {p0, v0, v6}, Lcom/google/android/apps/books/util/AccountUtils;->findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    move-result-object v1

    .line 591
    .local v1, "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    iget-object v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    if-eqz v5, :cond_b

    .line 592
    iget-boolean v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-nez v5, :cond_4

    .line 593
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConsumedIntentAccount:Z

    .line 596
    :cond_4
    iget-boolean v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->needsAuthorization:Z

    if-eqz v5, :cond_7

    .line 597
    const-string v5, "BooksActivity"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 598
    const-string v5, "BooksActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found account "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " which needs authorization"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_5
    iget-object v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startAuthTokenFetch(Landroid/accounts/Account;)V

    goto :goto_0

    .line 587
    .end local v0    # "accountFromIntent":Landroid/accounts/Account;
    .end local v1    # "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->accountFromIntent()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_1

    .line 604
    .restart local v0    # "accountFromIntent":Landroid/accounts/Account;
    .restart local v1    # "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    :cond_7
    const-string v5, "BooksActivity"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 605
    const-string v7, "BooksActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found authorized account "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " from "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-eqz v5, :cond_9

    const-string v5, "preferences"

    :goto_2
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    :cond_8
    iget-boolean v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-nez v5, :cond_a

    .line 612
    .local v4, "saveIfChanged":Z
    :goto_3
    iget-object v5, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    invoke-direct {p0, v5, v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->setAccount(Landroid/accounts/Account;Z)V

    goto/16 :goto_0

    .line 605
    .end local v4    # "saveIfChanged":Z
    :cond_9
    const-string v5, "system accounts"

    goto :goto_2

    :cond_a
    move v4, v6

    .line 611
    goto :goto_3

    .line 616
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->showAccountPicker()V

    goto/16 :goto_0
.end method

.method public static getFragmentCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 3
    .param p0, "activity"    # Landroid/content/Context;

    .prologue
    .line 164
    instance-of v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;

    if-eqz v0, :cond_0

    .line 165
    check-cast p0, Lcom/google/android/apps/books/app/BaseBooksActivity;

    .end local p0    # "activity":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    .line 170
    .restart local p0    # "activity":Landroid/content/Context;
    :goto_0
    return-object v0

    .line 167
    :cond_0
    const-string v0, "BooksActivity"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    const-string v0, "BooksActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get fragment callbacks from invalid Activity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity;->sStubFragmentCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    goto :goto_0
.end method

.method public static getFragmentCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 160
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    return-object v0
.end method

.method public static getOrientation(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 452
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getRotationLockMode()Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "rotationLock":Ljava/lang/String;
    const-string v1, "portrait"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    const/4 v1, 0x7

    .line 459
    :goto_0
    return v1

    .line 456
    :cond_0
    const-string v1, "landscape"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 457
    const/4 v1, 0x6

    goto :goto_0

    .line 459
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getReaderTheme(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 536
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getReaderTheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleAccountPickerResult(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 842
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    if-nez p1, :cond_1

    .line 843
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 847
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->finish()V

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startAuthTokenFetch(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method public static markExternalIntent(Landroid/content/Intent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 862
    invoke-virtual {p0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 863
    return-void
.end method

.method public static maybeShowDogfoodDialog(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1094
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/Config;->getVersionString()Ljava/lang/String;

    move-result-object v0

    .line 1095
    .local v0, "currentAppVersion":Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1096
    .local v1, "localPreferences":Lcom/google/android/apps/books/preference/LocalPreferences;
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->USE_DOGFOOD_BEHAVIOR:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->appVersionHasChanged(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1098
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->setLastAppVersionOpened(Ljava/lang/String;)V

    .line 1099
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f01f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1103
    :cond_0
    return-void
.end method

.method private requiresMenuHandlingOverride(I)Z
    .locals 2
    .param p1, "keyCode"    # I

    .prologue
    .line 1116
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    const-string v0, "LGE"

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAccount(Landroid/accounts/Account;Z)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "saveSelectedAccountIfChanged"    # Z

    .prologue
    .line 719
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_2

    .line 725
    const-string v0, "BooksActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    const-string v0, "BooksActivity"

    const-string v1, "Restarting activity due to changed account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->restart(Landroid/app/Activity;)V

    .line 741
    :cond_1
    :goto_0
    return-void

    .line 730
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    .line 732
    if-eqz p2, :cond_3

    .line 733
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 736
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->considerNaggingAboutSync()V

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onSelectedAccount(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private showAccountPicker()V
    .locals 3

    .prologue
    .line 1005
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_PROMPT_ACCOUNT_CHOICE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeAccountAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;)V

    .line 1007
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->SHOWING_PICKER:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountPicker:Lcom/google/android/apps/books/app/AccountPicker;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    const/4 v2, 0x1

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/apps/books/app/AccountPicker;->pickAccount(Landroid/app/Activity;Landroid/accounts/Account;I)V

    .line 1010
    return-void
.end method

.method public static showAppSettingsActivity(Landroid/app/Activity;Z)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "forceLightTheme"    # Z

    .prologue
    .line 1000
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1001
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1002
    return-void
.end method

.method private startAuthTokenFetch(Landroid/accounts/Account;)V
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 626
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const-string v1, "BooksActivity"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    const-string v1, "BooksActivity"

    const-string v2, "startAuthTokenFetch"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->RETRIEVING_AUTH_TOKEN:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 630
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 631
    .local v0, "am":Landroid/accounts/AccountManager;
    const-string v2, "print"

    sget-object v3, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v5, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;

    const/4 v1, 0x0

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V

    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    move-object v1, p1

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 633
    return-void
.end method

.method private startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "installedFinskyPackage"    # Ljava/lang/String;
    .param p3, "requestDirectPurchase"    # Z
    .param p4, "volumeId"    # Ljava/lang/String;
    .param p5, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 877
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    const-string v4, "missing account"

    invoke-static {v3, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 882
    if-eqz p5, :cond_5

    iget-boolean v3, p5, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    if-nez v3, :cond_0

    if-eqz p3, :cond_5

    :cond_0
    const-string v3, "com.android.vending"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    .line 886
    .local v1, "usingDirectPurchase":Z
    :goto_0
    const/4 v0, 0x0

    .line 887
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 889
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, p4, v3, p5}, Lcom/google/android/apps/books/util/PurchaseHelper;->directPurchaseIntentV2(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)Landroid/content/Intent;

    move-result-object v0

    .line 892
    if-nez v0, :cond_2

    .line 894
    const-string v3, "BooksActivity"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 895
    if-nez p4, :cond_6

    const-string v2, ""

    .line 897
    .local v2, "volId":Ljava/lang/String;
    :goto_1
    const-string v3, "BooksActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot use V2 direct purchase"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    .end local v2    # "volId":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, p1, p2, v3}, Lcom/google/android/apps/books/util/PurchaseHelper;->directPurchaseIntentV1(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 904
    :cond_2
    if-nez v0, :cond_3

    .line 905
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, p1, p4, v3, p2}, Lcom/google/android/apps/books/util/PurchaseHelper;->fallbackPurchaseIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 907
    const/4 v1, 0x0

    .line 910
    :cond_3
    if-nez v0, :cond_7

    .line 911
    const-string v3, "BooksActivity"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 912
    const-string v3, "BooksActivity"

    const-string v4, "Cannot find a usable intent!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    :cond_4
    :goto_2
    return-void

    .line 882
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "usingDirectPurchase":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 895
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "usingDirectPurchase":Z
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 917
    :cond_7
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 918
    if-eqz v1, :cond_8

    .line 919
    new-instance v3, Lcom/google/android/apps/books/util/PurchaseHelper;

    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v3, p4, v4}, Lcom/google/android/apps/books/util/PurchaseHelper;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mPendingPurchase:Lcom/google/android/apps/books/util/PurchaseHelper;

    .line 920
    const/4 v3, 0x2

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2

    .line 931
    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method private startManualSync()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1045
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {p0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 1047
    .local v0, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface {v0, v1, v2, v5, v3}, Lcom/google/android/apps/books/data/BooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 1050
    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController;->JUST_FETCH:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v0, v4, v4, v5, v1}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1051
    return-void
.end method


# virtual methods
.method protected accountFromIntent()Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 641
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v2, 0x0

    .line 642
    .local v2, "result":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 643
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 645
    .local v3, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 646
    .local v0, "accountName":Ljava/lang/String;
    const-string v4, "authAccount"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 647
    const-string v4, "authAccount"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 654
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 655
    new-instance v2, Landroid/accounts/Account;

    .end local v2    # "result":Landroid/accounts/Account;
    const-string v4, "com.google"

    invoke-direct {v2, v0, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    .restart local v2    # "result":Landroid/accounts/Account;
    :cond_1
    return-object v2

    .line 648
    :cond_2
    const-string v4, "intent_extra_data_key"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 649
    const-string v4, "intent_extra_data_key"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 650
    :cond_3
    if-eqz v3, :cond_0

    .line 651
    const-string v4, "email"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Landroid/support/v4/app/Fragment;",
            ">(",
            "Ljava/lang/String;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 178
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 1054
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getAccountPickerCallbacks()Lcom/google/android/apps/books/app/AccountPicker$Callbacks;
    .locals 1

    .prologue
    .line 1086
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountPickerCallbacks:Lcom/google/android/apps/books/app/AccountPicker$Callbacks;

    return-object v0
.end method

.method protected getCurrentReaderTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mCurrentReaderTheme:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract getThemeId(Z)I
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 532
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mIsDestroyed:Z

    return v0
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 3
    .param p1, "accounts"    # [Landroid/accounts/Account;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v2, 0x0

    .line 543
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/AccountUtils;->findAccount([Landroid/accounts/Account;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    .line 544
    iput-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    .line 545
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 546
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 547
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->findAccount()V

    .line 549
    .end local v0    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v7, -0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 803
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 804
    packed-switch p1, :pswitch_data_0

    .line 834
    :cond_0
    :goto_0
    return-void

    .line 806
    :pswitch_0
    if-eqz p3, :cond_2

    const-string v3, "authAccount"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 808
    .local v1, "accountName":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_1

    new-instance v0, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v0, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    .local v0, "account":Landroid/accounts/Account;
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->handleAccountPickerResult(Landroid/accounts/Account;)V

    goto :goto_0

    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    :cond_2
    move-object v1, v0

    .line 806
    goto :goto_1

    .line 814
    :pswitch_1
    const-string v3, "BooksActivity"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 815
    const-string v5, "BooksActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post-purchase: success="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-ne p2, v7, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    :cond_3
    if-ne p2, v7, :cond_0

    .line 818
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mPendingPurchase:Lcom/google/android/apps/books/util/PurchaseHelper;

    invoke-static {p0, p3, v3}, Lcom/google/android/apps/books/util/PurchaseHelper;->buildReadIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/apps/books/util/PurchaseHelper;)Landroid/content/Intent;

    move-result-object v2

    .line 820
    .local v2, "readIntent":Landroid/content/Intent;
    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mPendingPurchase:Lcom/google/android/apps/books/util/PurchaseHelper;

    .line 821
    if-eqz v2, :cond_5

    .line 822
    const-string v3, "books:addToMyEBooks"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 824
    const-string v3, "books:internalIntent"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 825
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startActivity(Landroid/content/Intent;)V

    .line 826
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->finish()V

    goto :goto_0

    .end local v2    # "readIntent":Landroid/content/Intent;
    :cond_4
    move v3, v4

    .line 815
    goto :goto_2

    .line 827
    .restart local v2    # "readIntent":Landroid/content/Intent;
    :cond_5
    const-string v3, "BooksActivity"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 828
    const-string v3, "BooksActivity"

    const-string v4, "post-purchase: failed to create read intent"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 478
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 480
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 481
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->applyMissingDefaults()V

    .line 483
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 486
    if-eqz p1, :cond_0

    .line 487
    const-string v1, "BaseBooksActivity.browserAuthRequestId"

    iget v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    .line 490
    const-string v1, "BaseBooksActivity.pendingPurchase"

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/PurchaseHelper;->createInstance(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/apps/books/util/PurchaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mPendingPurchase:Lcom/google/android/apps/books/util/PurchaseHelper;

    .line 493
    const-string v1, "BaseBooksActivity.consumedIntentAccount"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConsumedIntentAccount:Z

    .line 496
    const-string v1, "BaseBooksActivity.account"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    .line 500
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getReaderTheme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mCurrentReaderTheme:Ljava/lang/String;

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mCurrentReaderTheme:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->shouldUseDarkTheme(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getThemeId(Z)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->setTheme(I)V

    .line 504
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->setRequestedOrientation(I)V

    .line 506
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 507
    new-instance v1, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 508
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 520
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 522
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mIsDestroyed:Z

    .line 524
    invoke-super {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onDestroy()V

    .line 525
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v0, 0x1

    .line 1121
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->requiresMenuHandlingOverride(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1128
    :cond_0
    :goto_0
    return v0

    .line 1125
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1128
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    const/4 v0, 0x1

    .line 1133
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->requiresMenuHandlingOverride(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1134
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->openOptionsMenu()V

    .line 1141
    :cond_0
    :goto_0
    return v0

    .line 1138
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1141
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 786
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 787
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 791
    iget v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I

    .line 792
    invoke-super {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onPause()V

    .line 793
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 772
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-super {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onResume()V

    .line 775
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 776
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 781
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->findAccount()V

    .line 782
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 706
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 707
    const-string v0, "BaseBooksActivity.consumedIntentAccount"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mConsumedIntentAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 708
    const-string v0, "BaseBooksActivity.account"

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 709
    return-void
.end method

.method protected abstract onSelectedAccount(Landroid/accounts/Account;)V
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 763
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-super {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onStart()V

    .line 765
    invoke-static {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->incrementNumResumedActivities(Landroid/app/Activity;)V

    .line 767
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 768
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 797
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->decrementNumResumedActivities(Landroid/app/Activity;)V

    .line 798
    invoke-super {p0}, Lcom/google/android/apps/books/app/AnalyticsActivity;->onStop()V

    .line 799
    return-void
.end method

.method public setOnKeyListener(Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    .prologue
    .line 1106
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity;->mOnKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    .line 1107
    return-void
.end method
