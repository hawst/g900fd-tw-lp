.class Lcom/google/android/apps/books/util/pool/FinitePool;
.super Ljava/lang/Object;
.source "FinitePool.java"

# interfaces
.implements Lcom/google/android/apps/books/util/pool/Pool;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/util/pool/Poolable",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/pool/Pool",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mInfinite:Z

.field private final mLimit:I

.field private final mManager:Lcom/google/android/apps/books/util/pool/PoolableManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/pool/PoolableManager",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mPoolCount:I

.field private mRoot:Lcom/google/android/apps/books/util/pool/Poolable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/pool/PoolableManager;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/pool/PoolableManager",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/apps/books/util/pool/FinitePool;, "Lcom/google/android/apps/books/util/pool/FinitePool<TT;>;"
    .local p1, "manager":Lcom/google/android/apps/books/util/pool/PoolableManager;, "Lcom/google/android/apps/books/util/pool/PoolableManager<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mManager:Lcom/google/android/apps/books/util/pool/PoolableManager;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mLimit:I

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mInfinite:Z

    .line 55
    return-void
.end method


# virtual methods
.method public acquire()Lcom/google/android/apps/books/util/pool/Poolable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lcom/google/android/apps/books/util/pool/FinitePool;, "Lcom/google/android/apps/books/util/pool/FinitePool<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mRoot:Lcom/google/android/apps/books/util/pool/Poolable;

    if-eqz v1, :cond_1

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mRoot:Lcom/google/android/apps/books/util/pool/Poolable;

    .line 71
    .local v0, "element":Lcom/google/android/apps/books/util/pool/Poolable;, "TT;"
    invoke-interface {v0}, Lcom/google/android/apps/books/util/pool/Poolable;->getNextPoolable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/pool/Poolable;

    iput-object v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mRoot:Lcom/google/android/apps/books/util/pool/Poolable;

    .line 72
    iget v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mPoolCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mPoolCount:I

    .line 77
    :goto_0
    if-eqz v0, :cond_0

    .line 78
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/pool/Poolable;->setNextPoolable(Ljava/lang/Object;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mManager:Lcom/google/android/apps/books/util/pool/PoolableManager;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/util/pool/PoolableManager;->onAcquired(Lcom/google/android/apps/books/util/pool/Poolable;)V

    .line 82
    :cond_0
    return-object v0

    .line 74
    .end local v0    # "element":Lcom/google/android/apps/books/util/pool/Poolable;, "TT;"
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mManager:Lcom/google/android/apps/books/util/pool/PoolableManager;

    invoke-interface {v1}, Lcom/google/android/apps/books/util/pool/PoolableManager;->newInstance()Lcom/google/android/apps/books/util/pool/Poolable;

    move-result-object v0

    .restart local v0    # "element":Lcom/google/android/apps/books/util/pool/Poolable;, "TT;"
    goto :goto_0
.end method

.method public release(Lcom/google/android/apps/books/util/pool/Poolable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/apps/books/util/pool/FinitePool;, "Lcom/google/android/apps/books/util/pool/FinitePool<TT;>;"
    .local p1, "element":Lcom/google/android/apps/books/util/pool/Poolable;, "TT;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mInfinite:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mPoolCount:I

    iget v1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mLimit:I

    if-ge v0, v1, :cond_1

    .line 88
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mPoolCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mPoolCount:I

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mRoot:Lcom/google/android/apps/books/util/pool/Poolable;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/util/pool/Poolable;->setNextPoolable(Ljava/lang/Object;)V

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mRoot:Lcom/google/android/apps/books/util/pool/Poolable;

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/util/pool/FinitePool;->mManager:Lcom/google/android/apps/books/util/pool/PoolableManager;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/pool/PoolableManager;->onReleased(Lcom/google/android/apps/books/util/pool/Poolable;)V

    .line 93
    return-void
.end method
