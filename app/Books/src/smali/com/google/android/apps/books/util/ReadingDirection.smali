.class public final enum Lcom/google/android/apps/books/util/ReadingDirection;
.super Ljava/lang/Enum;
.source "ReadingDirection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/util/ReadingDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/util/ReadingDirection;

.field public static final enum BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

.field public static final enum FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/android/apps/books/util/ReadingDirection;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/ReadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    .line 21
    new-instance v0, Lcom/google/android/apps/books/util/ReadingDirection;

    const-string v1, "BACKWARD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/util/ReadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/util/ReadingDirection;

    sget-object v1, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->$VALUES:[Lcom/google/android/apps/books/util/ReadingDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromScreenDirection(Lcom/google/android/apps/books/util/ScreenDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ReadingDirection;
    .locals 1
    .param p0, "screenDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p1, "writingDirection"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p0, v0, :cond_1

    .line 30
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    .line 38
    :goto_0
    return-object v0

    .line 30
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    goto :goto_0

    .line 33
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p0, v0, :cond_3

    .line 34
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    goto :goto_0

    .line 38
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/util/ReadingDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/android/apps/books/util/ReadingDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ReadingDirection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/util/ReadingDirection;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->$VALUES:[Lcom/google/android/apps/books/util/ReadingDirection;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/util/ReadingDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/util/ReadingDirection;

    return-object v0
.end method
