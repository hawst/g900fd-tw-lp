.class public abstract Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.super Ljava/lang/Object;
.source "AppSettingsCheckableItem.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AppSettingsItem;


# instance fields
.field private final mLabel:I

.field private final mSummary:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "label"    # I

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(II)V

    .line 28
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "label"    # I
    .param p2, "summary"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mLabel:I

    .line 33
    iput p2, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mSummary:I

    .line 34
    return-void
.end method


# virtual methods
.method protected abstract getValue()Z
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 38
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 39
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040027

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 40
    .local v5, "view":Landroid/view/View;
    const v7, 0x7f0e00af

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 41
    .local v3, "tv":Landroid/widget/TextView;
    iget v7, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mLabel:I

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 43
    const v7, 0x7f0e00b0

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 44
    .local v4, "tv_summary":Landroid/widget/TextView;
    iget v7, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mSummary:I

    if-eqz v7, :cond_0

    .line 45
    iget v7, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mSummary:I

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(I)V

    .line 50
    :goto_0
    const v7, 0x7f040026

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "checkboxView":Landroid/view/View;
    const v7, 0x7f0e00b1

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 52
    .local v6, "widgetSpot":Landroid/widget/LinearLayout;
    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 53
    const v7, 0x7f0e00ae

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 55
    .local v0, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->getValue()Z

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 57
    return-object v5

    .line 47
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    .end local v1    # "checkboxView":Landroid/view/View;
    .end local v6    # "widgetSpot":Landroid/widget/LinearLayout;
    :cond_0
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public maybeSendAccessibilityEvent(Landroid/view/View;Z)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "newChecked"    # Z

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 79
    .local v1, "res":Landroid/content/res/Resources;
    if-eqz p2, :cond_1

    const v4, 0x7f0f012b

    :goto_0
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "state":Ljava/lang/String;
    iget v4, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mLabel:I

    if-eqz v4, :cond_2

    .line 82
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->mLabel:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "textToAnnounce":Ljava/lang/String;
    :goto_1
    invoke-static {v0, p1, v3}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 88
    .end local v1    # "res":Landroid/content/res/Resources;
    .end local v2    # "state":Ljava/lang/String;
    .end local v3    # "textToAnnounce":Ljava/lang/String;
    :cond_0
    return-void

    .line 79
    .restart local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    const v4, 0x7f0f012c

    goto :goto_0

    .line 84
    .restart local v2    # "state":Ljava/lang/String;
    :cond_2
    move-object v3, v2

    .restart local v3    # "textToAnnounce":Ljava/lang/String;
    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 62
    const v3, 0x7f0e00ae

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 63
    .local v0, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 64
    .local v1, "isChecked":Z
    if-nez v1, :cond_0

    const/4 v2, 0x1

    .line 65
    .local v2, "newChecked":Z
    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->setValue(Z)V

    .line 67
    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;->maybeSendAccessibilityEvent(Landroid/view/View;Z)V

    .line 68
    return-void

    .line 64
    .end local v2    # "newChecked":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected abstract setValue(Z)V
.end method
