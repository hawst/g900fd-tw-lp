.class public interface abstract Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
.super Ljava/lang/Object;
.source "MediaOverlaysController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ControlledReader"
.end annotation


# virtual methods
.method public abstract activateMediaElement(IILjava/lang/String;)V
.end method

.method public abstract finishedPlayback(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
.end method

.method public abstract startedPlaying()V
.end method

.method public abstract stoppedPlaying()V
.end method
