.class public Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;
.super Ljava/lang/Object;
.source "SingleVolumeAnnotationCache.java"


# instance fields
.field private final mAnnotationCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mAnnotationDatas:Lcom/google/android/apps/books/annotations/EventualMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/annotations/EventualMap",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mKnownDeleted:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoadedLayers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeAnnotations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mListeners:Ljava/util/Set;

    .line 31
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationCache:Ljava/util/Map;

    .line 36
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mKnownDeleted:Ljava/util/Map;

    .line 38
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mVolumeAnnotations:Ljava/util/Map;

    .line 44
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mLoadedLayers:Ljava/util/Set;

    .line 51
    invoke-static {}, Lcom/google/android/apps/books/annotations/EventualMap;->create()Lcom/google/android/apps/books/annotations/EventualMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationDatas:Lcom/google/android/apps/books/annotations/EventualMap;

    return-void
.end method

.method private getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 208
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->makeNewCache(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .end local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    return-object v0
.end method

.method private getCachedAnnotations(Ljava/lang/String;)Lcom/google/common/collect/ImmutableList;
    .locals 1
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private getKnownDeletedForLayer(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mKnownDeleted:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 246
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-nez v0, :cond_0

    .line 247
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mKnownDeleted:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_0
    return-object v0
.end method

.method private listenerCopy()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mListeners:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private makeNewCache(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 213
    .local v0, "newCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    return-object v0
.end method

.method private sendLayerAnnotationUpdates(Ljava/lang/String;)V
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getCachedAnnotations(Ljava/lang/String;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 231
    .local v0, "annotationCopy":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->listenerCopy()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 232
    .local v2, "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/google/android/apps/books/annotations/AnnotationListener;->annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V

    goto :goto_0

    .line 234
    .end local v2    # "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    :cond_0
    return-void
.end method

.method private silentAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 218
    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "localId":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    return-void
.end method

.method private silentMarkDeleted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "localId"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 224
    .local v0, "removed":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getKnownDeletedForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->silentAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getCachedAnnotations(Ljava/lang/String;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 99
    .local v0, "annotationCopy":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->listenerCopy()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 100
    .local v2, "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    invoke-interface {v2, p1, p2, v0}, Lcom/google/android/apps/books/annotations/AnnotationListener;->annotationAdded(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/common/collect/ImmutableList;)V

    goto :goto_0

    .line 102
    .end local v2    # "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    :cond_0
    return-void
.end method

.method public addDatas(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "datas":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 146
    .local v1, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/util/ExceptionOr;

    .line 148
    .local v2, "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;"
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationDatas:Lcom/google/android/apps/books/annotations/EventualMap;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/books/annotations/EventualMap;->loadAndCache(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 151
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationDatas:Lcom/google/android/apps/books/annotations/EventualMap;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/books/annotations/EventualMap;->load(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    .end local v1    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .end local v2    # "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;"
    :cond_1
    return-void
.end method

.method public addVolumeAnnotations(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "segment"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p3, "values":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mVolumeAnnotations:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 163
    .local v1, "layerCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;>;"
    if-nez v1, :cond_0

    .line 164
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 165
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mVolumeAnnotations:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->listenerCopy()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 169
    .local v2, "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    invoke-interface {v2, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationListener;->volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V

    goto :goto_0

    .line 171
    .end local v2    # "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    :cond_1
    return-void
.end method

.method public expunge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "localId"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getKnownDeletedForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->sendLayerAnnotationUpdates(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public getAnnotationMarkedForDeletion(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "localId"    # Ljava/lang/String;

    .prologue
    .line 241
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getKnownDeletedForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    return-object v0
.end method

.method public getRecentAnnotationsForLayer(Ljava/lang/String;I)Ljava/util/List;
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v0, "annotationList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getCachedAnnotations(Ljava/lang/String;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 294
    .local v1, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-lez p2, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 318
    .end local v0    # "annotationList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    :goto_0
    return-object v0

    .line 298
    .restart local v0    # "annotationList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 303
    new-instance v2, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache$1;-><init>(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 318
    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public load(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "loaded":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mLoadedLayers:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 136
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 137
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->silentAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0

    .line 139
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mLoadedLayers:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->sendLayerAnnotationUpdates(Ljava/lang/String;)V

    .line 142
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public markDeleted(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->silentMarkDeleted(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->sendLayerAnnotationUpdates(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public markEdited(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 10
    .param p1, "newValue"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 186
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "layerId":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 188
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v4

    .line 189
    .local v4, "localId":Ljava/lang/String;
    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 190
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/annotations/Annotation;

    .line 192
    .local v5, "oldValue":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v5}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 194
    .local v2, "latestTimestamp":J
    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/books/annotations/Annotation;->withUpdatedLastUsedTimestamp(J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object p1

    .line 195
    invoke-virtual {p1, v5}, Lcom/google/android/apps/books/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 196
    invoke-interface {v0, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->sendLayerAnnotationUpdates(Ljava/lang/String;)V

    .line 200
    .end local v2    # "latestTimestamp":J
    .end local v5    # "oldValue":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    return-void
.end method

.method public publishCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p2, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->listenerCopy()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 88
    .local v1, "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationListener;->characterQuotaLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V

    goto :goto_0

    .line 90
    .end local v1    # "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    :cond_0
    return-void
.end method

.method public updateLastUsedTimestamp(Lcom/google/android/apps/books/annotations/Annotation;J)V
    .locals 6
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "timestamp"    # J

    .prologue
    .line 270
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, "layerId":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationCacheForLayer(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 272
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v3

    .line 273
    .local v3, "localId":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 274
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Annotation;

    .line 275
    .local v1, "cachedAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v1, p2, p3}, Lcom/google/android/apps/books/annotations/Annotation;->withUpdatedLastUsedTimestamp(J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v4

    .line 277
    .local v4, "newAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    .end local v1    # "cachedAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v4    # "newAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    return-void
.end method

.method public weaklyAddListener(Lcom/google/android/apps/books/annotations/AnnotationListener;)V
    .locals 6
    .param p1, "annotationListener"    # Lcom/google/android/apps/books/annotations/AnnotationListener;

    .prologue
    .line 64
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mListeners:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 65
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mListeners:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mLoadedLayers:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 68
    .local v2, "layer":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getCachedAnnotations(Ljava/lang/String;)Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v5

    invoke-interface {p1, v2, v5}, Lcom/google/android/apps/books/annotations/AnnotationListener;->annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V

    goto :goto_0

    .line 71
    .end local v2    # "layer":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mVolumeAnnotations:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 72
    .restart local v2    # "layer":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mVolumeAnnotations:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 74
    .local v4, "segmentToResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;>;"
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 75
    .local v3, "segmentIndex":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p1, v2, v3, v5}, Lcom/google/android/apps/books/annotations/AnnotationListener;->volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V

    goto :goto_1

    .line 80
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "layer":Ljava/lang/String;
    .end local v3    # "segmentIndex":I
    .end local v4    # "segmentToResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;>;"
    :cond_2
    return-void
.end method

.method public whenAnnotationDataLoaded(Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 261
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->mAnnotationDatas:Lcom/google/android/apps/books/annotations/EventualMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/annotations/EventualMap;->whenLoaded(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v0

    return v0
.end method
