.class public Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "BaseBooksQuizPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BooksReadWriteFilter"
.end annotation


# instance fields
.field private mSelectedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)V
    .locals 1

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    .line 43
    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 44
    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 6
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 53
    const v4, 0x7f0e002f

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "itemId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->mSelectedItemIds:Ljava/util/Set;

    if-eqz v4, :cond_0

    .line 56
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->mSelectedItemIds:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 60
    .local v1, "selected":Z
    :goto_0
    const v4, 0x7f0e0035

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 62
    const v4, 0x7f0e0033

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "title":Ljava/lang/String;
    const v4, 0x7f0e0038

    iget-object v5, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    invoke-virtual {v5}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v3, v1, v5}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getItemContentDescription(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 66
    const v4, 0x7f0e0039

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 67
    .local v2, "serverIndex":I
    const v5, 0x7f0e0037

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->colorResIdFromIndex(I)I

    move-result v4

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 71
    const/4 v4, 0x1

    return v4

    .line 58
    .end local v1    # "selected":Z
    .end local v2    # "serverIndex":I
    .end local v3    # "title":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "selected":Z
    goto :goto_0

    .line 67
    .restart local v2    # "serverIndex":I
    .restart local v3    # "title":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->colorMutedResIdFromIndex(I)I

    move-result v4

    goto :goto_1
.end method

.method public onPreFilter()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->this$0:Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    # invokes: Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->access$000(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;->mSelectedItemIds:Ljava/util/Set;

    .line 49
    return-void
.end method
