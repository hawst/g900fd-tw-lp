.class public abstract Lcom/google/android/apps/books/net/BaseEncryptedContentResponse;
.super Ljava/lang/Object;
.source "BaseEncryptedContentResponse.java"

# interfaces
.implements Lcom/google/android/apps/books/net/EncryptedContentResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BaseEncryptedContentResponse;->getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    .line 15
    .local v0, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v1

    goto :goto_0
.end method
