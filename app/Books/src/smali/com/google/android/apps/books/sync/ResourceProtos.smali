.class public Lcom/google/android/apps/books/sync/ResourceProtos;
.super Ljava/lang/Object;
.source "ResourceProtos.java"


# direct methods
.method public static resourceProtoToResource(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/android/apps/books/model/Resource;
    .locals 6
    .param p0, "resourceProto"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .prologue
    const/4 v5, 0x0

    .line 16
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;-><init>()V

    .line 18
    .local v0, "result":Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getUrl()Ljava/lang/String;

    move-result-object v2

    const-string v3, "&amp;"

    const-string v4, "&"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 19
    .local v1, "url":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v1, v5, v2, v5}, Lcom/google/android/apps/books/model/ResourceUtils;->makeId(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 20
    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 21
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 22
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->build()Lcom/google/android/apps/books/model/Resource;

    move-result-object v2

    return-object v2
.end method
