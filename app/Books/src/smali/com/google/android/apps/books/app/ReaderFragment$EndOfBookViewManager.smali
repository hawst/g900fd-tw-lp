.class Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;
.super Ljava/lang/Object;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EndOfBookViewManager"
.end annotation


# instance fields
.field private mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field private mEndOfBookViewHolder:Landroid/widget/FrameLayout;

.field private mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2594
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598
    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .line 2604
    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 2594
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method

.method static synthetic access$12100(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->requestLoadEndOfBookPage()V

    return-void
.end method

.method static synthetic access$17100(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->maybeNeedsReset()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3700(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;Lcom/google/android/apps/books/app/PurchaseInfo;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 2594
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    return-object p1
.end method

.method static synthetic access$5200(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->reset()V

    return-void
.end method

.method static synthetic access$5500(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/eob/TextureEndOfBookView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .prologue
    .line 2594
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->onViewContentChanged()V

    return-void
.end method

.method private maybeNeedsReset()Z
    .locals 1

    .prologue
    .line 2644
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onViewContentChanged()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 2707
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    if-nez v0, :cond_0

    .line 2721
    :goto_0
    return-void

    .line 2715
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getWidth()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getHeight()I

    move-result v2

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->measure(II)V

    .line 2719
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->layout(IIII)V

    .line 2720
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->createPagesViewOnHoverListener(Z)Landroid/view/View$OnHoverListener;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6000(Lcom/google/android/apps/books/app/ReaderFragment;Z)Landroid/view/View$OnHoverListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_0
.end method

.method private requestLoadEndOfBookPage()V
    .locals 10

    .prologue
    .line 2650
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2651
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2652
    const-string v0, "ReaderFragment"

    const-string v1, "mMetadata null while loading end of book page."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2695
    :cond_0
    :goto_0
    return-void

    .line 2657
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    if-nez v0, :cond_0

    .line 2661
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2662
    const-string v0, "ReaderFragment"

    const-string v1, "Loading metadata dependent end of book page"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2665
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$300(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/ViewGroup;

    move-result-object v9

    .line 2666
    .local v9, "parent":Landroid/view/ViewGroup;
    const v0, 0x7f0e0113

    invoke-virtual {v9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    .line 2668
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->hideEndOfBook()V

    .line 2669
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2671
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    .line 2672
    .local v8, "layoutInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0400cb

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 2673
    .local v7, "endOfBookLayout":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2674
    const v0, 0x7f0e01e2

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .line 2677
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$2;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V

    .line 2684
    .local v3, "listener":Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5600(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/accounts/Account;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SystemBarManager;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V

    .line 2692
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    if-eqz v0, :cond_0

    .line 2693
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setPrice(Landroid/content/res/Resources;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    goto/16 :goto_0
.end method

.method private reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2635
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    if-eqz v0, :cond_0

    .line 2636
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->clear()V

    .line 2637
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2638
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    .line 2639
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookViewHolder:Landroid/widget/FrameLayout;

    .line 2641
    :cond_0
    return-void
.end method


# virtual methods
.method public loadPrice()V
    .locals 5

    .prologue
    .line 2607
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2608
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2609
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V

    .line 2619
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/app/PurchaseInfo;>;"
    new-instance v2, Lcom/google/android/apps/books/app/LoadPriceTask;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5600(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/apps/books/app/LoadPriceTask;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 2621
    .local v2, "loadPriceTask":Lcom/google/android/apps/books/app/LoadPriceTask;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-static {v2, v3}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 2623
    .end local v0    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/app/PurchaseInfo;>;"
    .end local v2    # "loadPriceTask":Lcom/google/android/apps/books/app/LoadPriceTask;
    :cond_0
    return-void
.end method

.method public requestRatingInfoUpdate()V
    .locals 2

    .prologue
    .line 2701
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    if-eqz v0, :cond_0

    .line 2702
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mEndOfBookView:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->requestRatingInfoUpdate(Landroid/content/Context;)V

    .line 2704
    :cond_0
    return-void
.end method
