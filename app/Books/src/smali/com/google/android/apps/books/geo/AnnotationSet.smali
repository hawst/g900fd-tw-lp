.class public Lcom/google/android/apps/books/geo/AnnotationSet;
.super Ljava/lang/Object;
.source "AnnotationSet.java"


# instance fields
.field private final mLayerToPageIdToAnnotation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ListMultimap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private final mLocalIdIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

.field private final mPositionIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationSortingKey;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V
    .locals 2
    .param p1, "ordering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p2, "loadTimer"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLocalIdIndex:Ljava/util/Map;

    .line 49
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLayerToPageIdToAnnotation:Ljava/util/Map;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    .line 66
    new-instance v0, Ljava/util/TreeMap;

    invoke-static {p1}, Lcom/google/android/apps/books/annotations/TextLocation;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/Annotation;->comparator(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    .line 68
    return-void
.end method

.method private annotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Collection;
    .locals 3
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/geo/AnnotationSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    .line 192
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/geo/AnnotationSet;->asKey(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/geo/AnnotationSet;->asKey(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/geo/AnnotationSet;->asKey(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method private asKey(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/AnnotationSortingKey;
    .locals 1
    .param p1, "location"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/apps/books/geo/AnnotationSet$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet$1;-><init>(Lcom/google/android/apps/books/geo/AnnotationSet;Lcom/google/android/apps/books/annotations/TextLocation;)V

    return-object v0
.end method

.method private isEmpty()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method private removeItemsMatchingLayer(Ljava/util/Iterator;Ljava/lang/String;)V
    .locals 2
    .param p2, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 77
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 81
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_1
    return-void
.end method


# virtual methods
.method public addAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 4
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 105
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLocalIdIndex:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {v2, p1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v0

    .line 108
    .local v0, "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/geo/AnnotationSet;->getPageIdToAnnotationMap(Ljava/lang/String;)Lcom/google/common/collect/ListMultimap;

    move-result-object v1

    .line 111
    .local v1, "pageIdToAnnotation":Lcom/google/common/collect/ListMultimap;, "Lcom/google/common/collect/ListMultimap<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getPageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/google/common/collect/ListMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 113
    .end local v1    # "pageIdToAnnotation":Lcom/google/common/collect/ListMultimap;, "Lcom/google/common/collect/ListMultimap<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    return-void
.end method

.method public addAnnotations(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    const-string v3, "#addAnnotations start"

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 96
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 97
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/geo/AnnotationSet;->addAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0

    .line 99
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    if-eqz v2, :cond_2

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    invoke-interface {v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 102
    :cond_2
    return-void
.end method

.method public getAnnotationListCopy(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 225
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 226
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_1
    return-object v2
.end method

.method public getAnnotationsForImagePage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/geo/AnnotationSet;->getPageIdToAnnotationMap(Ljava/lang/String;)Lcom/google/common/collect/ListMultimap;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/common/collect/ListMultimap;->get(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;Ljava/util/Set;)Lcom/google/common/collect/ImmutableList;
    .locals 8
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/geo/AnnotationSet;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 144
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    .line 170
    :goto_0
    return-object v5

    .line 146
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 147
    .local v2, "end":Lcom/google/android/apps/books/annotations/TextLocation;
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v6, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-static {v5, v2, v6}, Lcom/google/android/apps/books/annotations/TextLocation;->compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v5

    if-lez v5, :cond_2

    .line 148
    const-string v5, "AnnotationSet"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 149
    const-string v5, "AnnotationSet"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Nonsensical annotation range: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " > "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_1
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    goto :goto_0

    .line 154
    :cond_2
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v4

    .line 156
    .local v4, "result":Lcom/google/common/collect/ImmutableList$Builder;, "Lcom/google/common/collect/ImmutableList$Builder<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->annotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 158
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz p2, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 162
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 165
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 166
    .local v1, "annotationEnd":Lcom/google/android/apps/books/annotations/TextLocation;
    if-eqz v2, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-static {v1, v2, v5}, Lcom/google/android/apps/books/annotations/TextLocation;->compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v5

    if-gtz v5, :cond_3

    .line 167
    :cond_5
    invoke-virtual {v4, v0}, Lcom/google/common/collect/ImmutableList$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList$Builder;

    goto :goto_1

    .line 170
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "annotationEnd":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_6
    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList$Builder;->build()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public getAnnotationsInRangeIterator(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;
    .locals 1
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->annotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getPageIdToAnnotationMap(Ljava/lang/String;)Lcom/google/common/collect/ListMultimap;
    .locals 2
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/collect/ListMultimap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLayerToPageIdToAnnotation:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/ListMultimap;

    .line 118
    .local v0, "pageIdToAnnotation":Lcom/google/common/collect/ListMultimap;, "Lcom/google/common/collect/ListMultimap<Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-nez v0, :cond_0

    .line 119
    invoke-static {}, Lcom/google/common/collect/ArrayListMultimap;->create()Lcom/google/common/collect/ArrayListMultimap;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLayerToPageIdToAnnotation:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :cond_0
    return-object v0
.end method

.method public getPageOrdering()Lcom/google/android/apps/books/common/Position$PageOrdering;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    return-object v0
.end method

.method public removeAnnotationsInLayer(Ljava/lang/String;)V
    .locals 1
    .param p1, "layer"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mPositionIndex:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->removeItemsMatchingLayer(Ljava/util/Iterator;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLocalIdIndex:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->removeItemsMatchingLayer(Ljava/util/Iterator;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/geo/AnnotationSet;->mLayerToPageIdToAnnotation:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-void
.end method

.method public setAnnotationsForLayer(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p2, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/geo/AnnotationSet;->removeAnnotationsInLayer(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/geo/AnnotationSet;->addAnnotations(Ljava/util/List;)V

    .line 131
    return-void
.end method
