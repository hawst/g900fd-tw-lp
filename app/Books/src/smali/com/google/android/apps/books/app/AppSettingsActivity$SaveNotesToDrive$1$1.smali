.class Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

.field final synthetic val$settingsOr:Lcom/google/android/apps/books/util/ExceptionOr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iput-object p2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->val$settingsOr:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x5

    const/4 v3, 0x3

    .line 576
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->val$settingsOr:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577
    const-string v1, "AppSettings"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    const-string v1, "AppSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UserSettings fails: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->val$settingsOr:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 582
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->val$settingsOr:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/UserSettings;

    .line 583
    .local v0, "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    if-nez v0, :cond_2

    .line 584
    const-string v1, "AppSettings"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 585
    const-string v1, "AppSettings"

    const-string v2, "UserSettings null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 589
    :cond_2
    const-string v1, "AppSettings"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 590
    const-string v1, "AppSettings"

    invoke-virtual {v0}, Lcom/google/android/apps/books/api/data/UserSettings;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/books/api/data/UserSettings;->notesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    if-nez v1, :cond_6

    .line 593
    const-string v1, "AppSettings"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 594
    const-string v1, "AppSettings"

    const-string v2, "UserSettings.notesExport null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    new-instance v2, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    invoke-direct {v2}, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;-><init>()V

    # setter for: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$702(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    .line 602
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$700(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->folderName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$700(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v2, v2, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->defaultFolderName()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$800(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->folderName:Ljava/lang/String;

    .line 606
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->val$context:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->notifySettingChanged()V

    goto :goto_0

    .line 598
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;->this$2:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    iget-object v2, v0, Lcom/google/android/apps/books/api/data/UserSettings;->notesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    # setter for: Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->access$702(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    goto :goto_1
.end method
