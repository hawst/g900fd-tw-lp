.class Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback$1;
.super Ljava/lang/Object;
.source "AccountPickerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;->run(Landroid/accounts/AccountManagerFuture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback$1;->this$1:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback$1;->this$1:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;

    iget-object v1, v0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;->this$0:Lcom/google/android/apps/books/app/AccountPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback$1;->this$1:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;

    iget-object v0, v0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;->this$0:Lcom/google/android/apps/books/app/AccountPickerFragment;

    # getter for: Lcom/google/android/apps/books/app/AccountPickerFragment;->mAdapter:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->access$300(Lcom/google/android/apps/books/app/AccountPickerFragment;)Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    # invokes: Lcom/google/android/apps/books/app/AccountPickerFragment;->onAccountClick(Landroid/accounts/Account;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->access$500(Lcom/google/android/apps/books/app/AccountPickerFragment;Landroid/accounts/Account;)V

    .line 213
    return-void
.end method
