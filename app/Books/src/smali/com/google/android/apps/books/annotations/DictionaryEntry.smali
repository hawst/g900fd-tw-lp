.class public Lcom/google/android/apps/books/annotations/DictionaryEntry;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;,
        Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;,
        Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;,
        Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;,
        Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;,
        Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    }
.end annotation


# instance fields
.field private fallBackTitle:Ljava/lang/String;

.field public final words:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "fallBackTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p2, "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/annotations/DictionaryEntry;-><init>(Ljava/util/List;)V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry;->fallBackTitle:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry;->words:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public getFallBackTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry;->fallBackTitle:Ljava/lang/String;

    return-object v0
.end method
