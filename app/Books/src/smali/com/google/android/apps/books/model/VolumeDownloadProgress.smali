.class public Lcom/google/android/apps/books/model/VolumeDownloadProgress;
.super Ljava/lang/Object;
.source "VolumeDownloadProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeDownloadProgress$1;
    }
.end annotation


# static fields
.field public static final NO_PROGRESS:Lcom/google/android/apps/books/model/VolumeDownloadProgress;


# instance fields
.field private final mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

.field private final mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

.field private final mPreferredFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    sget-object v1, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    sget-object v2, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    sget-object v3, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;-><init>(Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)V
    .locals 0
    .param p1, "epubProgress"    # Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .param p2, "imageProgress"    # Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .param p3, "preferredFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mPreferredFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 61
    return-void
.end method

.method public static fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .locals 4
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 123
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {p0, v2}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v1

    .line 125
    .local v1, "textProgress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {p0, v2}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    .line 127
    .local v0, "imageProgress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    new-instance v2, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    const-string v3, "preferred_mode"

    invoke-static {p0, v3}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->fromMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v3

    invoke-direct {v2, v1, v0, v3}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;-><init>(Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)V

    return-object v2
.end method

.method public static getDownloadedFraction(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D
    .locals 18
    .param p0, "format"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .param p1, "segmentFraction"    # Ljava/lang/Double;
    .param p2, "resourceFraction"    # Ljava/lang/Double;
    .param p3, "pageFraction"    # Ljava/lang/Double;
    .param p4, "structureFraction"    # Ljava/lang/Double;

    .prologue
    .line 144
    if-nez p0, :cond_1

    .line 145
    const-wide/16 v10, 0x0

    .line 170
    :cond_0
    :goto_0
    return-wide v10

    .line 147
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/model/VolumeDownloadProgress$1;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 160
    if-nez p1, :cond_4

    const-wide/16 v12, 0x0

    .line 162
    .local v12, "segFrac":D
    :goto_1
    const-wide v2, 0x3fefffffca501acbL    # 0.9999999

    cmpg-double v2, v12, v2

    if-gez v2, :cond_5

    .line 165
    const-wide/16 v16, 0x0

    .line 170
    .local v16, "resFrac":D
    :goto_2
    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v12 .. v17}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->weightedAverage(DDD)D

    move-result-wide v10

    goto :goto_0

    .line 149
    .end local v12    # "segFrac":D
    .end local v16    # "resFrac":D
    :pswitch_0
    if-nez p3, :cond_2

    const-wide/16 v0, 0x0

    .line 150
    .local v0, "pageFrac":D
    :goto_3
    if-nez p4, :cond_3

    const-wide/16 v4, 0x0

    .line 151
    .local v4, "structureFrac":D
    :goto_4
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->weightedAverage(DDD)D

    move-result-wide v10

    .line 152
    .local v10, "imageFrac":D
    if-eqz p1, :cond_0

    .line 155
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    invoke-static/range {v6 .. v11}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->weightedAverage(DDD)D

    move-result-wide v10

    goto :goto_0

    .line 149
    .end local v0    # "pageFrac":D
    .end local v4    # "structureFrac":D
    .end local v10    # "imageFrac":D
    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_3

    .line 150
    .restart local v0    # "pageFrac":D
    :cond_3
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    goto :goto_4

    .line 160
    .end local v0    # "pageFrac":D
    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    goto :goto_1

    .line 168
    .restart local v12    # "segFrac":D
    :cond_5
    if-nez p2, :cond_6

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    .restart local v16    # "resFrac":D
    :goto_5
    goto :goto_2

    .end local v16    # "resFrac":D
    :cond_6
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    goto :goto_5

    .line 147
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private getGreaterProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    .line 105
    :goto_0
    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/Comparables;->max(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    goto :goto_0
.end method

.method private static weightedAverage(DDD)D
    .locals 4
    .param p0, "a"    # D
    .param p2, "contributionOfA"    # D
    .param p4, "b"    # D

    .prologue
    .line 176
    mul-double v0, p0, p2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, p2

    mul-double/2addr v2, p4

    add-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    if-ne p0, p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v1

    .line 196
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 197
    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 199
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 200
    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 201
    .local v0, "other":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v3, :cond_4

    .line 202
    iget-object v3, v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-eqz v3, :cond_5

    move v1, v2

    .line 203
    goto :goto_0

    .line 204
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 205
    goto :goto_0

    .line 206
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v3, :cond_6

    .line 207
    iget-object v3, v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-eqz v3, :cond_0

    move v1, v2

    .line 208
    goto :goto_0

    .line 209
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 210
    goto :goto_0
.end method

.method public getEpubProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    return-object v0
.end method

.method public getImageProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    return-object v0
.end method

.method public getProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 3
    .param p1, "userSelectedFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 76
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 79
    .local v0, "formatToUse":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    :goto_0
    if-nez v0, :cond_1

    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getGreaterProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v1

    .line 89
    :goto_1
    return-object v1

    .line 76
    .end local v0    # "formatToUse":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mPreferredFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    goto :goto_0

    .line 84
    .restart local v0    # "formatToUse":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/model/VolumeDownloadProgress$1;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    goto :goto_1

    .line 86
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    goto :goto_1

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getProgress(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 1
    .param p1, "userSelectedMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 72
    invoke-static {p1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->fromMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 182
    const/16 v0, 0x1f

    .line 183
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 184
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 186
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 188
    return v1

    .line 184
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->hashCode()I

    move-result v2

    goto :goto_0

    .line 186
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public isPartiallyOrFullyDownloaded()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mEpubProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isPartiallyOrFullyDownloaded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->mImageProgress:Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isPartiallyOrFullyDownloaded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
