.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TocAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SELECT_BOOKMARK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SELECT_CHAPTER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SELECT_CURRENT_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SELECT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SHOULD_NEVER_HAPPEN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum SHOW_TOC_VIEW:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum VIEW_BOOKMARK_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum VIEW_CHAPTER_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

.field public static final enum VIEW_NOTES_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 161
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SHOW_TOC_VIEW"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOW_TOC_VIEW:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .line 166
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "VIEW_CHAPTER_LIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_CHAPTER_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "VIEW_BOOKMARK_LIST"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_BOOKMARK_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "VIEW_NOTES_LIST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_NOTES_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .line 170
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SELECT_CHAPTER"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CHAPTER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SELECT_CURRENT_PAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CURRENT_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SELECT_BOOKMARK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_BOOKMARK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SELECT_NOTE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .line 174
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const-string v1, "SHOULD_NEVER_HAPPEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOULD_NEVER_HAPPEN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .line 157
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOW_TOC_VIEW:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_CHAPTER_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_BOOKMARK_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_NOTES_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CHAPTER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CURRENT_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_BOOKMARK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOULD_NEVER_HAPPEN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    return-object v0
.end method
