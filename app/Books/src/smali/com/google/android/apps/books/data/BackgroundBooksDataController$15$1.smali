.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;)V
    .locals 0

    .prologue
    .line 690
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResource(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "innerVolumeId"    # Ljava/lang/String;
    .param p3, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 712
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/IdentifiableCollection;->getIdToValue()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Resource;

    .line 715
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 694
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v1, v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v3, v3, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->setVolumeManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 695
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v1, v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v1, v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 707
    :goto_0
    return-void

    .line 697
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v1, "BgDataController"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    const-string v1, "BgDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error saving manifest: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v1, v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 705
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v1, v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v2, v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    iget-object v3, v3, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v1
.end method
