.class public Lcom/google/android/apps/books/app/LoadPriceTask;
.super Lcom/google/android/apps/books/app/ApiaryLoadTask;
.source "LoadPriceTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/ApiaryLoadTask",
        "<",
        "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
        "Lcom/google/android/apps/books/app/PurchaseInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/app/PurchaseInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p4, "resultConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/app/PurchaseInfo;>;"
    const-class v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/android/apps/books/app/ApiaryLoadTask;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/Class;Lcom/google/android/ublib/utils/Consumer;)V

    .line 22
    iput-object p3, p0, Lcom/google/android/apps/books/app/LoadPriceTask;->mVolumeId:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method protected getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/app/LoadPriceTask;->mVolumeId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolume(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method protected process(Lcom/google/android/apps/books/api/data/ApiaryVolume;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 2
    .param p1, "volume"    # Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .prologue
    .line 32
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    if-nez v0, :cond_1

    .line 33
    :cond_0
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/books/app/PurchaseInfo;

    iget-object v1, p1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/PurchaseInfo;-><init>(Lcom/google/android/apps/books/app/data/JsonSaleInfo;)V

    goto :goto_0
.end method

.method protected bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LoadPriceTask;->process(Lcom/google/android/apps/books/api/data/ApiaryVolume;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v0

    return-object v0
.end method
