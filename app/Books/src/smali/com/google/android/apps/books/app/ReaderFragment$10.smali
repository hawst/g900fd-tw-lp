.class Lcom/google/android/apps/books/app/ReaderFragment$10;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreatePagesViewController()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/BookmarkController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$pagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 1752
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$10;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$10;->val$pagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 1
    .param p1, "bc"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 1755
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$10;->val$pagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->setBookmarkController(Lcom/google/android/apps/books/app/BookmarkController;)V

    .line 1756
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1752
    check-cast p1, Lcom/google/android/apps/books/app/BookmarkController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$10;->take(Lcom/google/android/apps/books/app/BookmarkController;)V

    return-void
.end method
