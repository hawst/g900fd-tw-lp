.class Lcom/google/android/apps/books/app/ContentsView$5;
.super Ljava/lang/Object;
.source "ContentsView.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ContentsView;->createTocView(Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ContentsView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ContentsView;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/apps/books/app/ContentsView$5;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChapterSelected(I)V
    .locals 6
    .param p1, "chapterIndex"    # I

    .prologue
    .line 324
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CHAPTER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    int-to-long v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V

    .line 327
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ContentsView$5;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # getter for: Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ContentsView;->access$500(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Arguments;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 329
    .local v1, "readingPos":Lcom/google/android/apps/books/common/Position;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ContentsView$5;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # getter for: Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ContentsView;->access$600(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/app/ContentsView$Callbacks;->navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    .end local v1    # "readingPos":Lcom/google/android/apps/books/common/Position;
    :cond_0
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, "ContentsView"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    const-string v2, "ContentsView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t find start position for chapter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCurrentPageSelected()V
    .locals 3

    .prologue
    .line 340
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_CURRENT_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$5;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # getter for: Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ContentsView;->access$600(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView$5;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # getter for: Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ContentsView;->access$500(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Arguments;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->position:Lcom/google/android/apps/books/common/Position;

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/ContentsView$Callbacks;->navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    .line 342
    return-void
.end method
