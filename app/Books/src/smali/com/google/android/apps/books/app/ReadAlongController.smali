.class public abstract Lcom/google/android/apps/books/app/ReadAlongController;
.super Ljava/lang/Object;
.source "ReadAlongController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReadAlongController$StopReason;,
        Lcom/google/android/apps/books/app/ReadAlongController$DataSource;,
        Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    }
.end annotation


# instance fields
.field protected mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

.field protected mSpeaking:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReadAlongController;->mSpeaking:Z

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    return-void
.end method


# virtual methods
.method public isSpeaking()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReadAlongController;->mSpeaking:Z

    return v0
.end method
