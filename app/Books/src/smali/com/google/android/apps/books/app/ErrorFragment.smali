.class public Lcom/google/android/apps/books/app/ErrorFragment;
.super Lcom/google/android/apps/books/app/NonPersistentDialogFragment;
.source "ErrorFragment.java"


# instance fields
.field private final mMessage:Ljava/lang/CharSequence;

.field private final mNegativeIntent:Landroid/content/Intent;

.field private final mNegativeText:Ljava/lang/CharSequence;

.field private final mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

.field private final mPositiveIntent:Landroid/content/Intent;

.field private final mPositiveText:Ljava/lang/CharSequence;

.field private final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NonPersistentDialogFragment;-><init>()V

    .line 117
    new-instance v0, Lcom/google/android/apps/books/app/ErrorFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ErrorFragment$2;-><init>(Lcom/google/android/apps/books/app/ErrorFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    .line 50
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mTitle:Ljava/lang/CharSequence;

    .line 51
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mMessage:Ljava/lang/CharSequence;

    .line 52
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveText:Ljava/lang/CharSequence;

    .line 53
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveIntent:Landroid/content/Intent;

    .line 54
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeText:Ljava/lang/CharSequence;

    .line 55
    iput-object v1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeIntent:Landroid/content/Intent;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 7
    .param p1, "message"    # Ljava/lang/CharSequence;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 79
    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ErrorFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "message"    # Ljava/lang/CharSequence;
    .param p3, "positiveText"    # Ljava/lang/CharSequence;
    .param p4, "positiveIntent"    # Landroid/content/Intent;
    .param p5, "negativeText"    # Ljava/lang/CharSequence;
    .param p6, "negativeIntent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NonPersistentDialogFragment;-><init>()V

    .line 117
    new-instance v0, Lcom/google/android/apps/books/app/ErrorFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ErrorFragment$2;-><init>(Lcom/google/android/apps/books/app/ErrorFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mTitle:Ljava/lang/CharSequence;

    .line 67
    iput-object p2, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mMessage:Ljava/lang/CharSequence;

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveText:Ljava/lang/CharSequence;

    .line 69
    iput-object p4, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveIntent:Landroid/content/Intent;

    .line 70
    iput-object p5, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeText:Ljava/lang/CharSequence;

    .line 71
    iput-object p6, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeIntent:Landroid/content/Intent;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ErrorFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ErrorFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ErrorFragment;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ErrorFragment;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ErrorFragment;->sendFeedback(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ErrorFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ErrorFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static getFeedbackIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getPositiveText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveText:Ljava/lang/CharSequence;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x104000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ErrorFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mTitle:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mTitle:Ljava/lang/CharSequence;

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0f00e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ErrorFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isFeedbackIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 183
    if-eqz p0, :cond_0

    const-string v0, "android.intent.action.BUG_REPORT"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendFeedback(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 131
    new-instance v2, Lcom/google/android/apps/books/app/ErrorFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ErrorFragment$3;-><init>(Lcom/google/android/apps/books/app/ErrorFragment;)V

    .line 153
    .local v2, "conn":Landroid/content/ServiceConnection;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 155
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 158
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 159
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    invoke-virtual {v3, p1, v6}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 161
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_0

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v6, :cond_1

    .line 162
    :cond_0
    const-string v6, "ErrorFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Feedback service can not be resolved for intent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :goto_0
    return-void

    .line 166
    .restart local v3    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 167
    .local v5, "serviceInfo":Landroid/content/pm/ServiceInfo;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v6, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v7, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .local v1, "componentName":Landroid/content/ComponentName;
    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 172
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v5    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_2
    const/4 v6, 0x1

    invoke-virtual {v0, p1, v2, v6}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ErrorFragment;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mMessage:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ErrorFragment;->getPositiveText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeIntent:Landroid/content/Intent;

    invoke-static {v2}, Lcom/google/android/apps/books/app/ErrorFragment;->isFeedbackIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    new-instance v1, Lcom/google/android/apps/books/app/ErrorFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ErrorFragment$1;-><init>(Lcom/google/android/apps/books/app/ErrorFragment;)V

    .line 113
    .local v1, "negativeClick":Landroid/content/DialogInterface$OnClickListener;
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ErrorFragment;->mNegativeText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 111
    .end local v1    # "negativeClick":Landroid/content/DialogInterface$OnClickListener;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "negativeClick":Landroid/content/DialogInterface$OnClickListener;
    goto :goto_0
.end method
