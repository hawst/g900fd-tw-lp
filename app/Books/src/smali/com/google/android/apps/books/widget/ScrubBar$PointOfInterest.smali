.class Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
.super Ljava/lang/Object;
.source "ScrubBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PointOfInterest"
.end annotation


# instance fields
.field public iconView:Landroid/view/View;

.field private mContentDescription:Ljava/lang/String;

.field public positionView:Landroid/view/View;

.field public scrubberIndex:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1, "icon"    # Landroid/view/View;
    .param p2, "position"    # Landroid/view/View;
    .param p3, "contentDescription"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    .line 190
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    .line 191
    iput-object p2, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    .line 192
    iput-object p3, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->mContentDescription:Ljava/lang/String;

    .line 193
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hasValidLocation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionCenter()F

    move-result v0

    return v0
.end method

.method private hasValidLocation()Z
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private positionCenter()F
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public adjustContentDescription(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "metadata":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;, "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata<*>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hasValidLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getCalloutPageLabelLong(IZ)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 228
    :cond_0
    return-void
.end method

.method public hideViews()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    return-void
.end method

.method public setScrubberIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;I)V
    .locals 0
    .param p2, "scrubberIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "metadata":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;, "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata<*>;"
    iput p2, p0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    .line 217
    if-eqz p1, :cond_0

    .line 218
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->adjustContentDescription(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V

    .line 220
    :cond_0
    return-void
.end method
