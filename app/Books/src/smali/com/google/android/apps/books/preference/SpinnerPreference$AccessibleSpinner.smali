.class public Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;
.super Landroid/widget/Spinner;
.source "SpinnerPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/preference/SpinnerPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccessibleSpinner"
.end annotation


# instance fields
.field private final mAccessibilityLabelId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    if-eqz p2, :cond_0

    .line 61
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->ListPreference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;->mAccessibilityLabelId:I

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :goto_0
    return-void

    .line 67
    :cond_0
    iput v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;->mAccessibilityLabelId:I

    goto :goto_0
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 3
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 80
    iget v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;->mAccessibilityLabelId:I

    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;->mAccessibilityLabelId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "accessibilityLabel":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 86
    .end local v0    # "accessibilityLabel":Ljava/lang/String;
    :cond_0
    return-void
.end method
