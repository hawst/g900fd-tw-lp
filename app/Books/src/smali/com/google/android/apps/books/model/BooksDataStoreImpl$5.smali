.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;->coverFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2406
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSaved(Lcom/google/android/apps/books/model/EncryptedContent;)V
    .locals 4
    .param p1, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;

    .prologue
    .line 2413
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    # getter for: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$400(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;->val$volumeId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2414
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    # getter for: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$500(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2415
    return-void
.end method
