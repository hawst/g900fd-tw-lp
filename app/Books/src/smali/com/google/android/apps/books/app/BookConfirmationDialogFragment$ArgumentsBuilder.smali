.class public Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
.super Ljava/lang/Object;
.source "BookConfirmationDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArgumentsBuilder"
.end annotation


# instance fields
.field protected final mBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "start"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    .line 44
    return-void
.end method


# virtual methods
.method public setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogBody"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-object p0
.end method

.method public setCancelLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "cancelLabel"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogCancelLabel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-object p0
.end method

.method public setCheckboxLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "checkboxLabel"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogCheckboxLabel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-object p0
.end method

.method public setIconResource(I)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "iconResId"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogIcon"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    return-object p0
.end method

.method public setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "okLabel"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogOkLabel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->mBundle:Landroid/os/Bundle;

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-object p0
.end method
