.class public Lcom/google/android/apps/books/model/ImmutableResource$Builder;
.super Ljava/lang/Object;
.source "ImmutableResource.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Resource;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mId:Ljava/lang/String;

.field private mIsDefault:Z

.field private mLanguage:Ljava/lang/String;

.field private mMd5Hash:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mOverlay:Ljava/lang/String;

.field private mShared:Z

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/Resource;
    .locals 10

    .prologue
    .line 197
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableResource;

    iget-object v1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMimeType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mLanguage:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMd5Hash:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mShared:Z

    iget-boolean v7, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mIsDefault:Z

    iget-object v8, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mOverlay:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/model/ImmutableResource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Lcom/google/android/apps/books/model/ImmutableResource$1;)V

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsDefault()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mIsDefault:Z

    return v0
.end method

.method public getIsShared()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mShared:Z

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5Hash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMd5Hash:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getOverlay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mOverlay:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 203
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mId:Ljava/lang/String;

    .line 204
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mUrl:Ljava/lang/String;

    .line 205
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMimeType:Ljava/lang/String;

    .line 206
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mLanguage:Ljava/lang/String;

    .line 207
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMd5Hash:Ljava/lang/String;

    .line 208
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mShared:Z

    .line 209
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mIsDefault:Z

    .line 210
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mOverlay:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mId:Ljava/lang/String;

    .line 123
    return-object p0
.end method

.method public setIsDefault(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "isDefault"    # Z

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mIsDefault:Z

    .line 183
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mLanguage:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public setMd5Hash(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "md5Hash"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMd5Hash:Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mMimeType:Ljava/lang/String;

    .line 143
    return-object p0
.end method

.method public setOverlay(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "overlay"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mOverlay:Ljava/lang/String;

    .line 193
    return-object p0
.end method

.method public setShared(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "shared"    # Z

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mShared:Z

    .line 173
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->mUrl:Ljava/lang/String;

    .line 133
    return-object p0
.end method
