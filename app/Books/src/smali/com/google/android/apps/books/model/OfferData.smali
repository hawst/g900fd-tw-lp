.class public Lcom/google/android/apps/books/model/OfferData;
.super Ljava/lang/Object;
.source "OfferData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/OfferData$1;,
        Lcom/google/android/apps/books/model/OfferData$Builder;
    }
.end annotation


# instance fields
.field private final mArtUri:Landroid/net/Uri;

.field private final mOfferId:Ljava/lang/String;

.field private final mOfferedBooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferedBookData;",
            ">;"
        }
    .end annotation
.end field

.field private final mQuota:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/OfferData$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/android/apps/books/model/OfferData$Builder;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    # getter for: Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferData$Builder;->access$100(Lcom/google/android/apps/books/model/OfferData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mOfferId:Ljava/lang/String;

    .line 123
    # getter for: Lcom/google/android/apps/books/model/OfferData$Builder;->mQuota:I
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferData$Builder;->access$200(Lcom/google/android/apps/books/model/OfferData$Builder;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/model/OfferData;->mQuota:I

    .line 124
    # getter for: Lcom/google/android/apps/books/model/OfferData$Builder;->mArtUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferData$Builder;->access$300(Lcom/google/android/apps/books/model/OfferData$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mArtUri:Landroid/net/Uri;

    .line 125
    # getter for: Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferedBooks:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferData$Builder;->access$400(Lcom/google/android/apps/books/model/OfferData$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mOfferedBooks:Ljava/util/List;

    .line 126
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/OfferData$Builder;Lcom/google/android/apps/books/model/OfferData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/OfferData$Builder;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/OfferData$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/OfferData;-><init>(Lcom/google/android/apps/books/model/OfferData$Builder;)V

    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/books/model/OfferData$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/OfferData$Builder;-><init>()V

    return-object v0
.end method

.method public static fromApiaryOffer(Lcom/google/android/apps/books/api/data/ApiaryOffer;)Lcom/google/android/apps/books/model/OfferData;
    .locals 5
    .param p0, "offer"    # Lcom/google/android/apps/books/api/data/ApiaryOffer;

    .prologue
    .line 45
    invoke-static {}, Lcom/google/android/apps/books/model/OfferData;->builder()Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setOfferId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer;->artUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setArtUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer;->quota:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setQuota(I)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v1

    .line 49
    .local v1, "builder":Lcom/google/android/apps/books/model/OfferData$Builder;
    iget-object v3, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;

    .line 50
    .local v0, "apiaryBook":Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;
    invoke-static {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->fromApiaryOfferedBook(Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;)Lcom/google/android/apps/books/model/OfferedBookData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/model/OfferData$Builder;->addOfferedBook(Lcom/google/android/apps/books/model/OfferedBookData;)Lcom/google/android/apps/books/model/OfferData$Builder;

    goto :goto_0

    .line 52
    .end local v0    # "apiaryBook":Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/OfferData$Builder;->build()Lcom/google/android/apps/books/model/OfferData;

    move-result-object v3

    return-object v3
.end method

.method public static fromApiaryOffers(Lcom/google/android/apps/books/api/data/ApiaryOffers;)Ljava/util/List;
    .locals 4
    .param p0, "apiaryOffers"    # Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/api/data/ApiaryOffers;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 57
    .local v2, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    if-eqz p0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/api/data/ApiaryOffers;->offers:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 58
    iget-object v3, p0, Lcom/google/android/apps/books/api/data/ApiaryOffers;->offers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryOffer;

    .line 59
    .local v0, "apiaryOffer":Lcom/google/android/apps/books/api/data/ApiaryOffer;
    invoke-static {v0}, Lcom/google/android/apps/books/model/OfferData;->fromApiaryOffer(Lcom/google/android/apps/books/api/data/ApiaryOffer;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    .end local v0    # "apiaryOffer":Lcom/google/android/apps/books/api/data/ApiaryOffer;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v2
.end method

.method public static fromOfferDataWithFiltering(Lcom/google/android/apps/books/model/OfferData;Ljava/util/Set;)Lcom/google/android/apps/books/model/OfferData;
    .locals 5
    .param p0, "offer"    # Lcom/google/android/apps/books/model/OfferData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/OfferData;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/model/OfferData;"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "volumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Lcom/google/android/apps/books/model/OfferData$Builder;

    invoke-direct {v3}, Lcom/google/android/apps/books/model/OfferData$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setOfferId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/OfferData;->getQuota()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setQuota(I)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/OfferData;->getArtUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferData$Builder;->setArtUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/OfferData$Builder;

    move-result-object v1

    .line 75
    .local v1, "builder":Lcom/google/android/apps/books/model/OfferData$Builder;
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/OfferData;->getOfferedBooks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/OfferedBookData;

    .line 76
    .local v0, "book":Lcom/google/android/apps/books/model/OfferedBookData;
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 79
    :cond_1
    new-instance v3, Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    invoke-direct {v3}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getAuthor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getCoverUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setCoverUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData;->getCanonicalVolumeLink()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setCanonicalVolumeLink(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->build()Lcom/google/android/apps/books/model/OfferedBookData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/model/OfferData$Builder;->addOfferedBook(Lcom/google/android/apps/books/model/OfferedBookData;)Lcom/google/android/apps/books/model/OfferData$Builder;

    goto :goto_0

    .line 87
    .end local v0    # "book":Lcom/google/android/apps/books/model/OfferedBookData;
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/OfferData$Builder;->build()Lcom/google/android/apps/books/model/OfferData;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public getArtUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mArtUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getOfferId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mOfferId:Ljava/lang/String;

    return-object v0
.end method

.method public getOfferedBooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferedBookData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData;->mOfferedBooks:Ljava/util/List;

    return-object v0
.end method

.method public getQuota()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/apps/books/model/OfferData;->mQuota:I

    return v0
.end method
