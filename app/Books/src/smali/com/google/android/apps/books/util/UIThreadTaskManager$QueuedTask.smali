.class Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
.super Ljava/lang/Object;
.source "UIThreadTaskManager.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/UIThreadTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueuedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final mPriority:I

.field private mRequestId:I

.field private final mTask:Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;

.field final synthetic this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V
    .locals 0
    .param p2, "task"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;
    .param p3, "priority"    # I

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p2, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mTask:Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;

    .line 199
    iput p3, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mPriority:I

    .line 200
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mPriority:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    .param p1, "x1"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mRequestId:I

    return p1
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)I
    .locals 3
    .param p1, "another"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .prologue
    .line 231
    iget v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mPriority:I

    iget v2, p1, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mPriority:I

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/MathUtils;->compare(II)I

    move-result v0

    .line 232
    .local v0, "r":I
    if-eqz v0, :cond_0

    .line 233
    neg-int v1, v0

    .line 235
    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mRequestId:I

    iget v2, p1, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mRequestId:I

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/MathUtils;->compare(II)I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 192
    check-cast p1, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->compareTo(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)I

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mTask:Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;->isValid()Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->queueIfBusy(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z
    invoke-static {v0, p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$1000(Lcom/google/android/apps/books/util/UIThreadTaskManager;Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->runTask()V

    .line 223
    :cond_0
    return-void
.end method

.method runTask()V
    .locals 12

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 216
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 207
    .local v4, "startNanos":J
    iget-object v6, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mTask:Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;

    invoke-interface {v6}, Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;->run()V

    .line 208
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 209
    .local v0, "endNanos":J
    const-string v6, "UIThreadTaskManager"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 210
    const-string v6, "UIThreadTaskManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mTask:Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " took "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v10, v0, v4

    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v10, v11, v9}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :cond_1
    sub-long v6, v0, v4

    const-wide/16 v8, 0x32

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x64

    div-long v2, v6, v8

    .line 215
    .local v2, "pauseTime":J
    iget-object v6, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # getter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->UI_THREAD_MAX_PAUSE_NS:J
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$900()J

    move-result-wide v8

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    add-long/2addr v8, v0

    # setter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mNextTime:J
    invoke-static {v6, v8, v9}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$802(Lcom/google/android/apps/books/util/UIThreadTaskManager;J)J

    goto :goto_0
.end method
