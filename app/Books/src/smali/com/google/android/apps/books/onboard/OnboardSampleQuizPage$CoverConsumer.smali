.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CoverConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mInnerRequestId:I

.field private final mVolume:Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;I)V
    .locals 0
    .param p2, "volume"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    .param p3, "innerRequestId"    # I

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mVolume:Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .line 59
    iput p3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mInnerRequestId:I

    .line 60
    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "bitmapExceptionOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mInnerRequestId:I

    if-eq v1, v2, :cond_0

    .line 83
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # operator-- for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$110(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    .line 69
    const-string v1, "OnboardSampleQuizPage"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    const-string v1, "OnboardSampleQuizPage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received cover reqId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mInnerRequestId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", volId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mVolume:Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    iget-object v3, v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 74
    const-string v1, "OnboardSampleQuizPage"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    const-string v1, "OnboardSampleQuizPage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cover fetch failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_2
    const/4 v0, 0x0

    .line 81
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mAddableSampleData:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mVolume:Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    iget-object v3, v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->mVolume:Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    iget-object v4, v4, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->volumeInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;

    iget-object v4, v4, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->title:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # operator++ for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mServerIndex:I
    invoke-static {v5}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$208(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v5

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 79
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 52
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
