.class Lcom/google/android/apps/books/widget/ReadNowHomeView$2;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/books/widget/CardData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$2;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/books/widget/CardData;)Z
    .locals 2
    .param p1, "data"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/LibraryFilter;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$2;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/apps/books/widget/CardData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$2;->apply(Lcom/google/android/apps/books/widget/CardData;)Z

    move-result v0

    return v0
.end method
