.class public interface abstract Lcom/google/android/apps/books/model/VolumeManifest;
.super Ljava/lang/Object;
.source "VolumeManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;,
        Lcom/google/android/apps/books/model/VolumeManifest$Mode;,
        Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;,
        Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    }
.end annotation


# virtual methods
.method public abstract getChapters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContentVersion()Ljava/lang/String;
.end method

.method public abstract getFirstChapterStartSegmentIndex()I
.end method

.method public abstract getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
.end method

.method public abstract getLanguage()Ljava/lang/String;
.end method

.method public abstract getMediaOverlayActiveClass()Ljava/lang/String;
.end method

.method public abstract getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPreferredMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.end method

.method public abstract getRenditionOrientation()Ljava/lang/String;
.end method

.method public abstract getRenditionSpread()Ljava/lang/String;
.end method

.method public abstract getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSegmentResources()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
.end method

.method public abstract hasImageMode()Z
.end method

.method public abstract hasMediaOverlays()Z
.end method

.method public abstract hasTextMode()Z
.end method

.method public abstract isRightToLeft()Z
.end method
