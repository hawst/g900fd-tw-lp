.class public Lcom/google/android/apps/books/app/SyncErrorFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "SyncErrorFragment.java"


# instance fields
.field private final mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

.field private final mPositiveClick:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/apps/books/app/SyncErrorFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/SyncErrorFragment$1;-><init>(Lcom/google/android/apps/books/app/SyncErrorFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SyncErrorFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    .line 33
    new-instance v0, Lcom/google/android/apps/books/app/SyncErrorFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/SyncErrorFragment$2;-><init>(Lcom/google/android/apps/books/app/SyncErrorFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SyncErrorFragment;->mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/SyncErrorFragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SyncErrorFragment;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SyncErrorFragment;->getCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    return-object v0
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SyncErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 18
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0f00ef

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 19
    const v1, 0x7f0f00f0

    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncErrorFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 20
    const v1, 0x7f0f00f1

    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncErrorFragment;->mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 22
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
