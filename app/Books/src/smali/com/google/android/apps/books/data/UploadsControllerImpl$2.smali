.class Lcom/google/android/apps/books/data/UploadsControllerImpl$2;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$upload:Lcom/google/android/apps/books/upload/Upload;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/BooksDataListener;->onNewUpload(Lcom/google/android/apps/books/upload/Upload;)V

    .line 128
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 124
    check-cast p1, Lcom/google/android/apps/books/model/BooksDataListener;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;->take(Lcom/google/android/apps/books/model/BooksDataListener;)V

    return-void
.end method
