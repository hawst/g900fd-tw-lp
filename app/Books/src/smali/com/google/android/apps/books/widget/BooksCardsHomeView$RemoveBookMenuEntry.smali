.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;
.super Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveBookMenuEntry"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 3
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 607
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 608
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f00c6

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZ)V

    .line 609
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->removeFromLibrary(Ljava/lang/String;)V

    .line 614
    return-void
.end method
