.class public Lcom/google/android/apps/books/app/mo/EnsureClipsTask;
.super Landroid/os/AsyncTask;
.source "EnsureClipsTask.java"

# interfaces
.implements Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;"
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;

.field private final mClips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;"
        }
    .end annotation
.end field

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private mException:Ljava/lang/Exception;

.field private final mRequestId:I

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;ILcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "requestId"    # I
    .param p4, "ensureClipsTaskCallback"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;I",
            "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "clips":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mException:Ljava/lang/Exception;

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mVolumeId:Ljava/lang/String;

    .line 44
    iput p3, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mRequestId:I

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mClips:Ljava/util/List;

    .line 46
    iput-object p4, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mCallback:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;

    .line 47
    iput-object p5, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 48
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    .line 52
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 53
    .local v5, "resIdToDescriptor":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/io/FileDescriptor;>;"
    const/4 v3, 0x0

    .line 55
    .local v3, "encounteredBadClip":Z
    iget-object v7, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mClips:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 56
    .local v0, "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getAudioResource()Lcom/google/android/apps/books/model/Resource;

    move-result-object v6

    .line 58
    .local v6, "resource":Lcom/google/android/apps/books/model/Resource;
    if-nez v6, :cond_2

    .line 59
    if-nez v3, :cond_1

    const-string v7, "EnsureClipsTask"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 60
    const-string v7, "EnsureClipsTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EnsureClips: no audio file for element "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getElementId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_1
    const/4 v3, 0x1

    .line 63
    goto :goto_0

    .line 66
    :cond_2
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 68
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v8, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mVolumeId:Ljava/lang/String;

    invoke-static {v7, v8, v6}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceParcelFileDescriptor(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 71
    .local v1, "descriptor":Ljava/io/FileDescriptor;
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    .end local v1    # "descriptor":Ljava/io/FileDescriptor;
    :catch_0
    move-exception v2

    .line 73
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "EnsureClipsTask"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 74
    const-string v7, "EnsureClipsTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error fetching content for clip: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getElementId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mException:Ljava/lang/Exception;

    .line 82
    .end local v0    # "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_4
    return-object v10
.end method

.method public execute()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 93
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mCallback:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;

    iget v1, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mRequestId:I

    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;->mException:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;->onClipsEnsured(ILjava/lang/Exception;)V

    .line 88
    return-void
.end method
