.class public abstract Lcom/google/android/apps/books/model/BaseVolumeData;
.super Ljava/lang/Object;
.source "BaseVolumeData.java"

# interfaces
.implements Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 27
    instance-of v0, p1, Lcom/google/android/apps/books/model/VolumeData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/books/model/VolumeData;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/VolumeDataUtils;->equals(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;
    .locals 3

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;->getViewability()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;->getOldStyleOpenAccessValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v0

    return-object v0
.end method

.method public getOldStyleOpenAccessValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;->isPublicDomain()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://schemas.google.com/books/2008#enabled"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http://schemas.google.com/books/2008#disabled"

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lcom/google/android/apps/books/model/VolumeDataUtils;->hashCode(Lcom/google/android/apps/books/model/VolumeData;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0}, Lcom/google/android/apps/books/model/VolumeDataUtils;->toString(Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
