.class public Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;
.super Lcom/google/android/ublib/utils/WrappedIoException;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientIoException"
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final mRetry:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .param p3, "shouldRetry"    # Z

    .prologue
    .line 314
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 315
    iput-boolean p3, p0, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;->mRetry:Z

    .line 316
    return-void
.end method


# virtual methods
.method public shouldRetry()Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;->mRetry:Z

    return v0
.end method
