.class Lcom/google/android/apps/books/widget/BookView$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "BookView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BookView;->animateNavViewShowing(FFFI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BookView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BookView;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BookView$4;->this$0:Lcom/google/android/apps/books/widget/BookView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView$4;->this$0:Lcom/google/android/apps/books/widget/BookView;

    # getter for: Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BookView;->access$200(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView$4;->this$0:Lcom/google/android/apps/books/widget/BookView;

    # getter for: Lcom/google/android/apps/books/widget/BookView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BookView;->access$200(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onFinishedTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 710
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView$4;->this$0:Lcom/google/android/apps/books/widget/BookView;

    # getter for: Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BookView;->access$300(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookView$4;->this$0:Lcom/google/android/apps/books/widget/BookView;

    # getter for: Lcom/google/android/apps/books/widget/BookView;->mBookNavView:Lcom/google/android/apps/books/navigation/BookNavView;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BookView;->access$300(Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->announceCurrentPosition()Z

    .line 713
    :cond_1
    return-void
.end method
