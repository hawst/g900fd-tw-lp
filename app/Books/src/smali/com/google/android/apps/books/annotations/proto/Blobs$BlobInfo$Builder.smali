.class public final Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Blobs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;",
        "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;-><init>()V

    .line 161
    .local v0, "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    new-instance v1, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;-><init>(Lcom/google/android/apps/books/annotations/proto/Blobs$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 162
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .locals 3

    .prologue
    .line 206
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    if-nez v1, :cond_0

    .line 207
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 211
    .local v0, "returnMe":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 212
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->mergeFrom(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .prologue
    .line 216
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getDefaultInstance()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object p0

    .line 217
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setName(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    .line 220
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setSize(I)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 232
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 236
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    :sswitch_0
    return-object p0

    .line 242
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setName(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    goto :goto_0

    .line 246
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setSize(I)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    goto :goto_0

    .line 232
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->access$302(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Z)Z

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->access$402(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 267
    return-object p0
.end method

.method public setSize(I)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->access$502(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Z)Z

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->size_:I
    invoke-static {v0, p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->access$602(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;I)I

    .line 285
    return-object p0
.end method
