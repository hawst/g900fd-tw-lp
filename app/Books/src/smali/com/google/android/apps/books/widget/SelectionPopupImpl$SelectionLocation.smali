.class Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;
.super Ljava/lang/Object;
.source "SelectionPopupImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SelectionPopupImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionLocation"
.end annotation


# instance fields
.field private final mAnchorPoint:Landroid/graphics/Point;

.field private final mHandleLongerAxis:I

.field private final mSelectionBottom:I

.field private final mSelectionLeft:I

.field private final mSelectionRight:I

.field private final mSelectionTop:I

.field final synthetic this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/SelectionPopupImpl;IIIIILandroid/graphics/Point;)V
    .locals 0
    .param p2, "selectionLeft"    # I
    .param p3, "selectionRight"    # I
    .param p4, "selectionTop"    # I
    .param p5, "selectionBottom"    # I
    .param p6, "handleLongerAxis"    # I
    .param p7, "anchorPoint"    # Landroid/graphics/Point;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput p2, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    .line 175
    iput p3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionRight:I

    .line 176
    iput p4, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    .line 177
    iput p5, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    .line 178
    iput p6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    .line 179
    iput-object p7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mAnchorPoint:Landroid/graphics/Point;

    .line 180
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->layout()V

    return-void
.end method

.method private layout()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 190
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mVertical:Z
    invoke-static {v6}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->access$200(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 191
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 192
    .local v4, "size":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6, v4, v4}, Landroid/view/View;->measure(II)V

    .line 193
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 194
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 197
    .local v2, "height":I
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    iget v8, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    div-int/lit8 v7, v2, 0x2

    sub-int v1, v6, v7

    .line 200
    .local v1, "drawLocationY":I
    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 201
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenHeight:I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->access$300(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I

    move-result v6

    sub-int/2addr v6, v2

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 204
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionRight:I

    add-int/2addr v6, v5

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenWidth:I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->access$400(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I

    move-result v7

    if-gt v6, v7, :cond_0

    .line 205
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionRight:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    add-int v0, v6, v7

    .line 253
    .local v0, "drawLocationX":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 255
    .local v3, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 256
    iput v1, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 257
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->requestLayout()V

    .line 258
    return-void

    .line 208
    .end local v0    # "drawLocationX":I
    .end local v3    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    sub-int/2addr v6, v5

    if-gez v6, :cond_2

    .line 209
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mAnchorPoint:Landroid/graphics/Point;

    if-eqz v6, :cond_1

    .line 211
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mAnchorPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    add-int v0, v6, v7

    .restart local v0    # "drawLocationX":I
    goto :goto_0

    .line 214
    .end local v0    # "drawLocationX":I
    :cond_1
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionRight:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    div-int/lit8 v7, v5, 0x2

    sub-int v0, v6, v7

    .restart local v0    # "drawLocationX":I
    goto :goto_0

    .line 217
    .end local v0    # "drawLocationX":I
    :cond_2
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    sub-int v0, v6, v5

    .restart local v0    # "drawLocationX":I
    goto :goto_0

    .line 222
    .end local v0    # "drawLocationX":I
    .end local v1    # "drawLocationY":I
    .end local v2    # "height":I
    .end local v4    # "size":I
    .end local v5    # "width":I
    :cond_3
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 223
    .restart local v4    # "size":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6, v4, v4}, Landroid/view/View;->measure(II)V

    .line 224
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 225
    .restart local v2    # "height":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 228
    .restart local v5    # "width":I
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionRight:I

    iget v8, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionLeft:I

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    div-int/lit8 v7, v5, 0x2

    sub-int v0, v6, v7

    .line 231
    .restart local v0    # "drawLocationX":I
    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 232
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenWidth:I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->access$400(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I

    move-result v6

    sub-int/2addr v6, v5

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 235
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    sub-int/2addr v6, v2

    if-ltz v6, :cond_4

    .line 236
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    sub-int v1, v6, v2

    .restart local v1    # "drawLocationY":I
    goto :goto_0

    .line 239
    .end local v1    # "drawLocationY":I
    :cond_4
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    add-int/2addr v6, v2

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->this$0:Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenHeight:I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->access$300(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I

    move-result v7

    if-lt v6, v7, :cond_6

    .line 240
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    if-lez v6, :cond_5

    .line 242
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    sub-int v1, v6, v2

    .restart local v1    # "drawLocationY":I
    goto/16 :goto_0

    .line 245
    .end local v1    # "drawLocationY":I
    :cond_5
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionTop:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    div-int/lit8 v7, v2, 0x2

    sub-int v1, v6, v7

    .restart local v1    # "drawLocationY":I
    goto/16 :goto_0

    .line 248
    .end local v1    # "drawLocationY":I
    :cond_6
    iget v6, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mSelectionBottom:I

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->mHandleLongerAxis:I

    add-int v1, v6, v7

    .restart local v1    # "drawLocationY":I
    goto/16 :goto_0
.end method
