.class public Lcom/google/android/apps/books/annotations/AnnotationData;
.super Ljava/lang/Object;
.source "AnnotationData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    }
.end annotation


# instance fields
.field public final key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

.field public final payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/annotations/AnnotationDataPayload;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .param p2, "payload"    # Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationData;->key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 88
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    .line 89
    return-void
.end method
