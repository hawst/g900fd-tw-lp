.class public Lcom/google/android/apps/books/util/BooksTextUtils;
.super Ljava/lang/Object;
.source "BooksTextUtils.java"


# direct methods
.method public static isNullOrWhitespace(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "sequence"    # Ljava/lang/CharSequence;

    .prologue
    .line 28
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static matchesWordPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)Z
    .locals 9
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 52
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v4

    .line 53
    :cond_1
    if-eqz p2, :cond_2

    invoke-static {p2}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v7

    .line 55
    .local v7, "iter":Ljava/text/BreakIterator;
    :goto_1
    invoke-virtual {v7, p1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 57
    const/4 v8, 0x1

    .line 58
    .local v8, "noCase":Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 59
    .local v5, "queryLen":I
    invoke-virtual {v7}, Ljava/text/BreakIterator;->first()I

    move-result v2

    .line 60
    .local v2, "start":I
    invoke-virtual {v7}, Ljava/text/BreakIterator;->next()I

    move-result v6

    .local v6, "end":I
    :goto_2
    const/4 v0, -0x1

    if-eq v6, v0, :cond_0

    move-object v0, p1

    move-object v3, p0

    .line 61
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v4, v1

    .line 62
    goto :goto_0

    .line 53
    .end local v2    # "start":I
    .end local v5    # "queryLen":I
    .end local v6    # "end":I
    .end local v7    # "iter":Ljava/text/BreakIterator;
    .end local v8    # "noCase":Z
    :cond_2
    invoke-static {}, Ljava/text/BreakIterator;->getWordInstance()Ljava/text/BreakIterator;

    move-result-object v7

    goto :goto_1

    .line 60
    .restart local v2    # "start":I
    .restart local v5    # "queryLen":I
    .restart local v6    # "end":I
    .restart local v7    # "iter":Ljava/text/BreakIterator;
    .restart local v8    # "noCase":Z
    :cond_3
    move v2, v6

    invoke-virtual {v7}, Ljava/text/BreakIterator;->next()I

    move-result v6

    goto :goto_2
.end method

.method public static transformCssForWebView(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "css"    # Ljava/lang/String;

    .prologue
    .line 43
    const-string v0, "-epub-"

    const-string v1, "-webkit-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
