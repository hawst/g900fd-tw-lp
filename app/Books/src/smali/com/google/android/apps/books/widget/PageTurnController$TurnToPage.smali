.class Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;
.super Ljava/lang/Object;
.source "PageTurnController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TurnToPage"
.end annotation


# instance fields
.field private final mPage:Lcom/google/android/apps/books/render/PageIdentifier;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PageTurnController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/render/PageIdentifier;)V
    .locals 0
    .param p2, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 611
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->mPage:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 613
    return-void
.end method


# virtual methods
.method public canBeCanceled()Z
    .locals 1

    .prologue
    .line 654
    const/4 v0, 0x0

    return v0
.end method

.method public canRun()Z
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 649
    const/4 v0, 0x0

    return-object v0
.end method

.method public run()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 623
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PageTurnController;->access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->mPage:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->directionTowardPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v1

    .line 625
    .local v1, "turnDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    if-eqz v1, :cond_1

    .line 627
    const-string v2, "PageTurnController"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 628
    const-string v2, "PageTurnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " starting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " turn"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # invokes: Lcom/google/android/apps/books/widget/PageTurnController;->startTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$400(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 634
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z
    invoke-static {v2, v5}, Lcom/google/android/apps/books/widget/PageTurnController;->access$802(Lcom/google/android/apps/books/widget/PageTurnController;Z)Z

    .line 635
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PageTurnController;->access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v2, v1, v3, v4, v5}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V

    .line 639
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 640
    .local v0, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    .end local v0    # "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
