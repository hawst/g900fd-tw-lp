.class public Lcom/google/android/apps/books/app/BooksAlarmBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BooksAlarmBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 31
    .local v3, "extras":Landroid/os/Bundle;
    const-string v4, "TYPE"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 32
    .local v2, "alarmType":I
    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 33
    const-string v4, "ACCOUNT"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "accountName":Ljava/lang/String;
    const-string v4, "BooksAlarm"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    const-string v4, "BooksAlarm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive Sync alarm for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    if-eqz v1, :cond_1

    .line 38
    new-instance v0, Landroid/accounts/Account;

    const-string v4, "com.google"

    invoke-direct {v0, v1, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/sync/SyncController;->requestManualSync()V

    .line 42
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    :cond_1
    return-void
.end method
