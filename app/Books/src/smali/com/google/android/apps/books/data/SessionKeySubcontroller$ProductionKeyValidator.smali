.class public Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProductionKeyValidator"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public validate(Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p1, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-static {v0}, Lcom/google/android/apps/books/util/EncryptionUtils;->extractK_sMetadata([B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;

    .line 326
    return-void
.end method
