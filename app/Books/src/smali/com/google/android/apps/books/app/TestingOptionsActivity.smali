.class public Lcom/google/android/apps/books/app/TestingOptionsActivity;
.super Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;
.source "TestingOptionsActivity.java"


# instance fields
.field private mDirty:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected createResources()Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
    .locals 9

    .prologue
    .line 12
    new-instance v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    const v1, 0x7f0400ca

    const v2, 0x7f0e01e1

    const v3, 0x7f0400c9

    const v4, 0x7f0e0132

    const v5, 0x7f0e00c0

    const v6, 0x7f040055

    const v7, 0x7f0e011f

    const v8, 0x7f0e0120

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;-><init>(IIIIIIII)V

    return-object v0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/TestingOptionsActivity;->mDirty:Z

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 32
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->finish()V

    .line 33
    return-void
.end method

.method protected onOptionChanged()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/TestingOptionsActivity;->mDirty:Z

    .line 25
    return-void
.end method

.method protected overridesViaGservices()Z
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    return v0
.end method
