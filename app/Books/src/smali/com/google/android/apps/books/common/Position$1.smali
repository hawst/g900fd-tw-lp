.class final Lcom/google/android/apps/books/common/Position$1;
.super Ljava/lang/Object;
.source "Position.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/common/Position;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;Z)Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/common/Position;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$pageMap:Lcom/google/android/apps/books/common/Position$PageOrdering;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/common/Position$PageOrdering;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/apps/books/common/Position$1;->val$pageMap:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;)I
    .locals 1
    .param p1, "lpos"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "rpos"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position$1;->val$pageMap:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/books/common/Position;->compare(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 232
    check-cast p1, Lcom/google/android/apps/books/common/Position;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/common/Position;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/common/Position$1;->compare(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method
