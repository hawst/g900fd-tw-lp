.class public interface abstract Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
.super Ljava/lang/Object;
.source "LocalContentSearchXmlHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SearchTextProcessor"
.end annotation


# virtual methods
.method public abstract handleEndDocument(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract handleNewReadingPosition(Ljava/lang/String;)V
.end method

.method public abstract handleNewText(Ljava/lang/String;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation
.end method
