.class Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RenderResponseConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NavViewRenderResponseConsumer"
.end annotation


# instance fields
.field final mDelegateRef:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field final mPosition:Lcom/google/android/apps/books/common/Position;

.field final mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p2, "delegate"    # Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
    .param p3, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 4793
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4794
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mDelegateRef:Ljava/lang/ref/Reference;

    .line 4795
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 4796
    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mUserToWrapperConsumer:Ljava/util/Map;
    invoke-static {p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4797
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 4798
    return-void
.end method


# virtual methods
.method public isPurgeable()Z
    .locals 2

    .prologue
    .line 4842
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mDelegateRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;

    .line 4843
    .local v0, "delegate":Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;->isPurgeable()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onMissingPosition()V
    .locals 3

    .prologue
    .line 4831
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mDelegateRef:Ljava/lang/ref/Reference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4838
    :goto_0
    return-void

    .line 4834
    :cond_0
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4835
    const-string v0, "PagesViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing position requested by navigation view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4837
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mPosition:Lcom/google/android/apps/books/common/Position;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->onMissingPosition(Lcom/google/android/apps/books/common/Position;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2300(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/common/Position;)V

    goto :goto_0
.end method

.method public onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 8
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 4802
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mDelegateRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;

    .line 4804
    .local v7, "delegate":Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
    if-nez v7, :cond_0

    .line 4818
    :goto_0
    return-void

    .line 4808
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mUserToWrapperConsumer:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4810
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5000(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v5

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/Logger;)V

    .line 4813
    .local v0, "decoratingPainter":Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9400(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-result-object v6

    .line 4815
    .local v6, "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    invoke-interface {v7, p1, v0, v6}, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;->onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 4817
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForAnyView(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    invoke-static {v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9500(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    goto :goto_0
.end method

.method public onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 2
    .param p1, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 4822
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->mDelegateRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;

    .line 4823
    .local v0, "delegate":Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
    if-eqz v0, :cond_0

    .line 4824
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mUserToWrapperConsumer:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4825
    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;->onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    .line 4827
    :cond_0
    return-void
.end method
