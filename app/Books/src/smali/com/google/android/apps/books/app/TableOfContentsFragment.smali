.class public Lcom/google/android/apps/books/app/TableOfContentsFragment;
.super Lcom/google/android/ublib/actionbar/UBLibFragment;
.source "TableOfContentsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/TableOfContentsFragment$1;,
        Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;
    }
.end annotation


# instance fields
.field private final mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

.field private mTitle:Ljava/lang/String;

.field private final mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;-><init>(Lcom/google/android/apps/books/app/TableOfContentsFragment;Lcom/google/android/apps/books/app/TableOfContentsFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .line 37
    iput-object v1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/app/ContentsView$Arguments;Ljava/lang/String;)V
    .locals 2
    .param p1, "arguments"    # Lcom/google/android/apps/books/app/ContentsView$Arguments;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/TableOfContentsFragment$ViewCallbacks;-><init>(Lcom/google/android/apps/books/app/TableOfContentsFragment;Lcom/google/android/apps/books/app/TableOfContentsFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mTitle:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    if-eqz v1, :cond_0

    .line 52
    const v1, 0x7f0400d2

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ContentsView;

    .line 53
    .local v0, "result":Lcom/google/android/apps/books/app/ContentsView;
    iget-object v1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/ContentsView;->setup(Lcom/google/android/apps/books/app/ContentsView$Arguments;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V

    .line 56
    .end local v0    # "result":Lcom/google/android/apps/books/app/ContentsView;
    :cond_0
    return-object v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->onResume()V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0e01fa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ContentsView;

    .line 65
    .local v0, "view":Lcom/google/android/apps/books/app/ContentsView;
    iget-object v1, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ContentsView;->setTitle(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->onStart()V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/app/TableOfContentsFragment;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 46
    :cond_0
    return-void
.end method
