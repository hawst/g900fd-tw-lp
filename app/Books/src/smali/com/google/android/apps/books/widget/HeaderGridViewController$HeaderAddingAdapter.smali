.class Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HeaderGridViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/HeaderGridViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeaderAddingAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->setHasStableIds(Z)V

    .line 60
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapterCount()I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$300(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 95
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    .line 96
    .local v0, "headerCount":I
    if-lt p1, v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$100(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    sub-int v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v2

    .line 99
    :goto_0
    return-wide v2

    :cond_0
    int-to-long v2, p1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    .line 87
    .local v0, "headerCount":I
    if-lt p1, v0, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$100(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    sub-int v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemViewType(I)I

    move-result v1

    .line 90
    :goto_0
    return v1

    :cond_0
    const/high16 v1, -0x80000000

    add-int/2addr v1, p1

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    .line 74
    .local v0, "headerCount":I
    if-lt p2, v0, :cond_0

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$100(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    sub-int v2, p2, v0

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 77
    :cond_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "type"    # I

    .prologue
    .line 64
    const v0, -0x7fffff80

    if-gt p2, v0, :cond_0

    .line 65
    new-instance v1, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderViewHolder;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$000(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Ljava/util/List;

    move-result-object v0

    const/high16 v3, -0x80000000

    sub-int v3, p2, v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderViewHolder;-><init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;Landroid/view/View;)V

    move-object v0, v1

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$100(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto :goto_0
.end method
