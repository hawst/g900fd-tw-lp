.class Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;
.super Ljava/lang/Object;
.source "MediaUrlFetcher.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MediaUrlFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaFileDescriptorOpeningConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/InputStreamSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;Lcom/google/android/apps/books/util/MediaUrlFetcher$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;
    .param p2, "x1"    # Lcom/google/android/apps/books/util/MediaUrlFetcher$1;

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;-><init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/InputStreamSource;

    .line 212
    .local v1, "input":Lcom/google/android/apps/books/data/InputStreamSource;
    if-eqz v1, :cond_0

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/InputStreamSource;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fileFound(Ljava/io/FileDescriptor;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$700(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    .end local v1    # "input":Lcom/google/android/apps/books/data/InputStreamSource;
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "MediaUrlFetcher"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    const-string v2, "MediaUrlFetcher"

    const-string v3, "IOException getting media fd"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFromNetwork()V
    invoke-static {v2}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$800(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 204
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFileDescriptorOpeningConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
