.class public Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
.super Ljava/lang/Object;
.source "JsonRecommendedBook.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;,
        Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;,
        Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;,
        Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "JsonRecommendedBook"


# instance fields
.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public recommendedInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "recommendedInfo"
    .end annotation
.end field

.field public saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "saleInfo"
    .end annotation
.end field

.field public volumeInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volumeInfo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method public static containsRequiredField(Ljava/lang/Object;Ljava/lang/String;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Z
    .locals 3
    .param p0, "fieldValue"    # Ljava/lang/Object;
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "book"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .prologue
    .line 122
    if-nez p0, :cond_1

    .line 123
    const-string v0, "JsonRecommendedBook"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "JsonRecommendedBook"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recommendation required field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_0
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static parseJson(Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .locals 11
    .param p0, "book"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .prologue
    .line 87
    iget-object v10, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->volumeInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;

    .line 90
    .local v10, "info":Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;
    const-string v0, "info"

    invoke-static {v10, v0, p0}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->containsRequiredField(Ljava/lang/Object;Ljava/lang/String;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v10, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;

    const-string v1, "info.imageLinks"

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->containsRequiredField(Ljava/lang/Object;Ljava/lang/String;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    const-string v1, "book.saleInfo"

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->containsRequiredField(Ljava/lang/Object;Ljava/lang/String;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    :cond_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    .line 96
    :cond_1
    iget-object v0, v10, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->authors:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->stitchAuthors(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "authorString":Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    iget-object v1, v10, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->title:Ljava/lang/String;

    iget v3, v10, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->averageRating:F

    iget-object v4, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    iget-object v5, v10, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;

    iget-object v5, v5, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;->thumbnail:Ljava/lang/String;

    new-instance v6, Lcom/google/android/apps/books/app/PurchaseInfo;

    iget-object v7, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    invoke-direct {v6, v7}, Lcom/google/android/apps/books/app/PurchaseInfo;-><init>(Lcom/google/android/apps/books/app/data/JsonSaleInfo;)V

    iget-object v7, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->recommendedInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;

    invoke-static {v7}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->reasonString(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    iget-object v8, v8, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->saleability:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->volumeInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;

    iget-object v9, v9, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->canonicalVolumeLink:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;-><init>(Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static parseRecommendationsJson(Ljava/util/List;Ljava/io/File;)V
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;,
            Landroid/util/MalformedJsonException;,
            Lorg/codehaus/jackson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 134
    .local v2, "is":Ljava/io/InputStream;
    new-instance v5, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v5}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-virtual {v5, v2}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonParser(Ljava/io/InputStream;)Lcom/google/api/client/json/JsonParser;

    move-result-object v5

    const-class v6, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;

    .line 137
    .local v3, "json":Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;
    if-eqz v3, :cond_0

    iget-object v5, v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;->items:Ljava/util/List;

    if-nez v5, :cond_1

    .line 138
    :cond_0
    new-instance v5, Landroid/util/MalformedJsonException;

    const-string v6, "Could not parse recommendation JSON."

    invoke-direct {v5, v6}, Landroid/util/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 141
    :cond_1
    iget-object v5, v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;->items:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .line 142
    .local v4, "jsonBook":Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    invoke-static {v4}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->parseJson(Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    move-result-object v0

    .line 143
    .local v0, "book":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    if-eqz v0, :cond_2

    .line 144
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    .end local v0    # "book":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .end local v4    # "jsonBook":Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    :cond_3
    return-void
.end method

.method private static reasonString(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "recommendedInfo"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;

    .prologue
    .line 104
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;->explanation:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    const-string v0, ""

    .line 107
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$RecommendedInfo;->explanation:Ljava/lang/String;

    goto :goto_0
.end method

.method private static stitchAuthors(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "authors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 113
    const-string v0, ""

    .line 116
    :goto_0
    return-object v0

    :cond_0
    const-string v0, " & "

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
