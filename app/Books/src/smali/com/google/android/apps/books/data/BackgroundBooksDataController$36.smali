.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$licenseType:Ljava/lang/String;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/SessionKey;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1127
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$volumeId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$licenseType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$sessionKey:Lcom/google/android/apps/books/model/SessionKey;

    iput-object p7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 1131
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$licenseType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;->val$sessionKey:Lcom/google/android/apps/books/model/SessionKey;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/net/BooksServer;->requestVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    move-result-object v1

    .line 1133
    .local v1, "result":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36$1;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;Lcom/google/android/apps/books/api/data/RequestAccessResponse;)V

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147
    .end local v1    # "result":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    :goto_0
    return-void

    .line 1139
    :catch_0
    move-exception v0

    .line 1140
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36$2;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;Ljava/io/IOException;)V

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    goto :goto_0
.end method
