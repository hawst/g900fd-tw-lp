.class Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->setupButtons(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

.field final synthetic val$dialogView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->val$dialogView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 441
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    .line 443
    .local v0, "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;->error:Lcom/google/android/apps/books/api/data/JsonError;

    if-nez v1, :cond_1

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->access$1100(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->onOfferAccepted()V

    .line 456
    .end local v0    # "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    :goto_0
    return-void

    .line 447
    .restart local v0    # "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;->error:Lcom/google/android/apps/books/api/data/JsonError;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->isUnrecoverableError(Lcom/google/android/apps/books/api/data/JsonError;)Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment;->access$1200(Lcom/google/android/apps/books/api/data/JsonError;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 448
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    const v2, 0x7f0f01b9

    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->val$dialogView:Landroid/view/View;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->showError(ILandroid/view/View;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->access$1300(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;ILandroid/view/View;)V

    goto :goto_0

    .line 455
    .end local v0    # "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    const v2, 0x7f0f01b8

    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->val$dialogView:Landroid/view/View;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->showError(ILandroid/view/View;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->access$1300(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;ILandroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 437
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
