.class Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageTurn3DMode"
.end annotation


# instance fields
.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 425
    const v0, 0x7f0f015f

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(I)V

    .line 426
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 427
    return-void
.end method


# virtual methods
.method protected getValue()Z
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getPageTurnMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "turn3d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected setValue(Z)V
    .locals 2
    .param p1, "pageTurn3D"    # Z

    .prologue
    .line 436
    if-eqz p1, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    const-string v1, "turn3d"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setPageTurnMode(Ljava/lang/String;)V

    .line 442
    :goto_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_TURN_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$400(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V

    .line 444
    return-void

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$PageTurn3DMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    const-string v1, "turn2d"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setPageTurnMode(Ljava/lang/String;)V

    goto :goto_0
.end method
