.class Lcom/google/android/apps/books/app/UploadsActivityAlias$1;
.super Ljava/lang/Object;
.source "UploadsActivityAlias.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/UploadsActivityAlias;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/UploadsActivityAlias;

.field final synthetic val$prefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field final synthetic val$uriToUpload:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/UploadsActivityAlias;Lcom/google/android/apps/books/preference/LocalPreferences;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->this$0:Lcom/google/android/apps/books/app/UploadsActivityAlias;

    iput-object p2, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->val$prefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    iput-object p3, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->val$uriToUpload:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->val$prefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHasSeenCloudLoadingIntro(Z)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->this$0:Lcom/google/android/apps/books/app/UploadsActivityAlias;

    iget-object v1, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;->val$uriToUpload:Landroid/net/Uri;

    # invokes: Lcom/google/android/apps/books/app/UploadsActivityAlias;->performChecksAndStartUpload(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->access$100(Lcom/google/android/apps/books/app/UploadsActivityAlias;Landroid/net/Uri;)V

    .line 119
    return-void
.end method
