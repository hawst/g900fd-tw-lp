.class public Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;,
        Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    }
.end annotation


# static fields
.field private static final FONT_RESOURCE_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LARGE_RESOURCE_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentIdToResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

.field private final mResourcePolicy:Lcom/google/android/apps/books/data/ResourcePolicy;

.field private final mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 62
    const-string v0, "image"

    const-string v1, "audio"

    const-string v2, "application/vnd.ms-opentype"

    const-string v3, "application/font-woff"

    invoke-static {v0, v1, v2, v3}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->LARGE_RESOURCE_TYPES:Ljava/util/Set;

    .line 65
    const-string v0, "application/vnd.ms-opentype"

    const-string v1, "application/font-woff"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->FONT_RESOURCE_TYPES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/ResourcePolicy;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "encryptionScheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;
    .param p2, "resourcePolicy"    # Lcom/google/android/apps/books/data/ResourcePolicy;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 74
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 76
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 95
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    .line 99
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    .line 100
    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcePolicy:Lcom/google/android/apps/books/data/ResourcePolicy;

    .line 101
    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mAccount:Landroid/accounts/Account;

    .line 102
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 54
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->LARGE_RESOURCE_TYPES:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p3, "x3"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "x4"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .param p5, "x5"    # Z

    .prologue
    .line 54
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localBlob(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localResourceRetriever(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "x4"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p5, "x5"    # Ljava/lang/Runnable;
    .param p6, "x6"    # Z
    .param p7, "x7"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p8, "x8"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 54
    invoke-direct/range {p0 .. p8}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "x4"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "x4"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p5, "x5"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p6, "x6"    # Ljava/lang/Runnable;

    .prologue
    .line 54
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/EncryptionScheme;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ResourcePolicy;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcePolicy:Lcom/google/android/apps/books/data/ResourcePolicy;

    return-object v0
.end method

.method private findFontResources(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554
    .local p1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 555
    .local v0, "fontResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Resource;

    .line 556
    .local v2, "resource":Lcom/google/android/apps/books/model/Resource;
    sget-object v3, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->FONT_RESOURCE_TYPES:Ljava/util/Set;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 557
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 560
    .end local v2    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_1
    return-object v0
.end method

.method private getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 11
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p5, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p6, "haveContentConsumers"    # Z
    .param p7, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p8, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Runnable;",
            "Z",
            "Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-interface {p3}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "RCSC"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v0, "Should not try to download shared resource from the subcontroller"

    invoke-direct {v10, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 273
    .local v10, "e":Ljava/lang/IllegalStateException;
    const-string v0, "RCSC"

    const-string v1, "bad logic"

    invoke-static {v0, v1, v10}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    invoke-direct {p0, p2, v10}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 423
    .end local v10    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    return-void

    .line 278
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;

    iget-object v3, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p8

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    goto :goto_0
.end method

.method private getKeyToDecryptLocalContent(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 1
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p3, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 466
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Resource;)V

    invoke-interface {p2, p4, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 479
    return-void
.end method

.method private getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 227
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V

    .line 252
    return-void
.end method

.method private handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p5, "e"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p6, "continuation"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 429
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$5;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p6

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 440
    return-void
.end method

.method private localBlob(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;

    .prologue
    .line 581
    new-instance v0, Lcom/google/android/apps/books/data/LocalBlob;

    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getResourceContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/data/LocalBlob;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    return-object v0
.end method

.method private localResourceRetriever(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 577
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 565
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$9;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$9;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 571
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 529
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p5, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    const/4 v1, 0x0

    .line 530
    .local v1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p5, :cond_1

    .line 531
    :cond_0
    iget-object v2, p3, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;->get(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v1

    .line 532
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2, v1, p5}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 535
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p4, :cond_3

    .line 536
    :cond_2
    iget-object v2, p3, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iget-object v4, p3, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/data/DataControllerUtils;->buildContentStreamSource(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/data/InputStreamSource;

    move-result-object v0

    .line 538
    .local v0, "contentStreamSource":Lcom/google/android/apps/books/data/InputStreamSource;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2, v0, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 540
    .end local v0    # "contentStreamSource":Lcom/google/android/apps/books/data/InputStreamSource;
    :cond_3
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V
    .locals 7
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p3, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "result"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .param p5, "save"    # Z

    .prologue
    const/4 v4, 0x0

    .line 488
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 489
    if-eqz p5, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v0, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    new-instance v6, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;

    invoke-direct {v6, p0, p4, p3, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;)V

    .line 520
    .local v6, "saveTask":Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;
    invoke-interface {p2, v6}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 524
    .end local v6    # "saveTask":Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;
    :goto_0
    return-void

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, p3, v1, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p4, "result"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    .prologue
    .line 444
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;

    invoke-direct {v0, p0, p4, p2, p3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 461
    return-void
.end method


# virtual methods
.method protected deliverEmptyResponses(Lcom/google/android/apps/books/sync/VolumeContentId;)V
    .locals 3
    .param p1, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;

    .prologue
    const/4 v2, 0x0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 216
    return-void
.end method

.method public getResourceContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 27
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p8, "onlyIfLocal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p5, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p6, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v23

    .line 118
    .local v23, "resourceId":Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v25

    .line 119
    .local v25, "resourceMimeType":Ljava/lang/String;
    new-instance v6, Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/books/sync/VolumeContentId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .local v6, "contentId":Lcom/google/android/apps/books/sync/VolumeContentId;
    const-string v4, "RCSC"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    const-string v4, "RCSC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Loading (ifLocal="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ") "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " resource "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalResourceState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 134
    .local v22, "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getStatus()Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v4, v5, :cond_2

    const/16 v24, 0x1

    .line 136
    .local v24, "resourceIsLocal":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    .line 137
    .local v7, "pendingSaveResult":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    if-eqz v7, :cond_3

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    .line 138
    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v4, v6, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    .line 200
    .end local v7    # "pendingSaveResult":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .end local v22    # "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .end local v24    # "resourceIsLocal":Z
    :cond_1
    :goto_1
    return-void

    .line 129
    :catch_0
    move-exception v20

    .line 130
    .local v20, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_1

    .line 134
    .end local v20    # "e":Ljava/io/IOException;
    .restart local v22    # "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :cond_2
    const/16 v24, 0x0

    goto :goto_0

    .line 144
    .restart local v7    # "pendingSaveResult":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .restart local v24    # "resourceIsLocal":Z
    :cond_3
    if-nez p4, :cond_5

    .line 146
    if-eqz v24, :cond_6

    .line 147
    if-eqz p5, :cond_4

    .line 148
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getResourceResources(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 151
    :cond_4
    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_1

    .line 154
    :cond_5
    if-eqz p8, :cond_6

    if-nez v24, :cond_6

    .line 156
    const/4 v4, 0x0

    check-cast v4, Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_1

    .line 160
    :cond_6
    const/16 v21, 0x0

    .line 161
    .local v21, "hadExistingConsumers":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p4

    invoke-virtual {v4, v6, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_2
    or-int v21, v21, v4

    .line 162
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p5

    invoke-virtual {v4, v6, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v4, 0x1

    :goto_3
    or-int v21, v21, v4

    .line 163
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v4, v6, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v4

    if-nez v4, :cond_9

    const/4 v4, 0x1

    :goto_4
    or-int v21, v21, v4

    .line 164
    if-nez v21, :cond_1

    .line 168
    if-eqz v24, :cond_b

    .line 169
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v26

    .line 170
    .local v26, "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    if-eqz v26, :cond_a

    .line 171
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V

    goto/16 :goto_1

    .line 161
    .end local v26    # "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    .line 162
    :cond_8
    const/4 v4, 0x0

    goto :goto_3

    .line 163
    :cond_9
    const/4 v4, 0x0

    goto :goto_4

    .line 173
    .restart local v26    # "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    :cond_a
    new-instance v12, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localBlob(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localResourceRetriever(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v12, v4, v5, v8, v9}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;)V

    .line 175
    .local v12, "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p3

    move-object/from16 v10, p1

    move-object v11, v6

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V

    goto/16 :goto_1

    .line 180
    .end local v12    # "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .end local v26    # "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    :cond_b
    const-string v4, "video"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 182
    :try_start_1
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getResourceContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->commit()V

    .line 184
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->deliverEmptyResponses(Lcom/google/android/apps/books/sync/VolumeContentId;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 185
    :catch_1
    move-exception v20

    .line 186
    .restart local v20    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 191
    .end local v20    # "e":Ljava/io/IOException;
    :cond_c
    new-instance v13, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, p3

    move-object/from16 v18, p7

    invoke-direct/range {v13 .. v18}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .local v13, "expiredKeyContinuation":Ljava/lang/Runnable;
    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v17, p3

    move-object/from16 v18, v13

    move-object/from16 v19, p7

    .line 199
    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto/16 :goto_1
.end method

.method protected saveFontAssociations(Lcom/google/android/apps/books/model/Resource;Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)V
    .locals 4
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p3, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p4, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/Resource;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 544
    .local p2, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    const-string v1, "text/css"

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 545
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->findFontResources(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 546
    .local v0, "fontResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 547
    invoke-interface {p3}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p4, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v3, p4, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->setResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 551
    .end local v0    # "fontResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_0
    return-void
.end method
