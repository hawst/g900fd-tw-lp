.class public final enum Lcom/google/android/apps/books/util/ScreenDirection;
.super Ljava/lang/Enum;
.source "ScreenDirection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/util/ScreenDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/util/ScreenDirection;

.field public static final enum LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

.field public static final enum RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;


# instance fields
.field private final mSign:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 18
    new-instance v0, Lcom/google/android/apps/books/util/ScreenDirection;

    const-string v1, "LEFT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/util/ScreenDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 23
    new-instance v0, Lcom/google/android/apps/books/util/ScreenDirection;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/books/util/ScreenDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 14
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v1, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->$VALUES:[Lcom/google/android/apps/books/util/ScreenDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sign"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/google/android/apps/books/util/ScreenDirection;->mSign:I

    .line 29
    return-void
.end method

.method public static dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;F)F
    .locals 1
    .param p0, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p1, "length"    # F

    .prologue
    .line 64
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/ScreenDirection;->mSign:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;I)I
    .locals 1
    .param p0, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p1, "length"    # I

    .prologue
    .line 56
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/ScreenDirection;->mSign:I

    mul-int/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1
    .param p0, "readingDirection"    # Lcom/google/android/apps/books/util/ReadingDirection;
    .param p1, "writingDirection"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne p0, v0, :cond_1

    .line 74
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 82
    :goto_0
    return-object v0

    .line 74
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0

    .line 77
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne p0, v0, :cond_3

    .line 78
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0

    .line 82
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static oppositeOf(Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1
    .param p0, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 35
    if-nez p0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    .line 37
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p0, v0, :cond_1

    .line 38
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0

    .line 40
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ScreenDirection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->$VALUES:[Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/util/ScreenDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/util/ScreenDirection;

    return-object v0
.end method
