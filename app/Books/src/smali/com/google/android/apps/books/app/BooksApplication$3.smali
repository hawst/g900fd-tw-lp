.class Lcom/google/android/apps/books/app/BooksApplication$3;
.super Ljava/lang/Object;
.source "BooksApplication.java"

# interfaces
.implements Lcom/google/android/apps/books/common/VolumeUsageManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mActiveVolumes:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/app/BooksApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BooksApplication;)V
    .locals 1

    .prologue
    .line 589
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksApplication$3;->this$0:Lcom/google/android/apps/books/app/BooksApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 591
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$3;->mActiveVolumes:Lcom/google/common/collect/Multimap;

    return-void
.end method


# virtual methods
.method public declared-synchronized acquireVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 595
    monitor-enter p0

    :try_start_0
    const-string v0, "BooksApp"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication$3;->isVolumeLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 596
    const-string v0, "BooksApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Locking volume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$3;->mActiveVolumes:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    monitor-exit p0

    return-void

    .line 595
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isVolumeLocked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 611
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$3;->mActiveVolumes:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized releaseVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 603
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication$3;->mActiveVolumes:Lcom/google/common/collect/Multimap;

    invoke-interface {v1, p1, p2}, Lcom/google/common/collect/Multimap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 604
    .local v0, "removed":Z
    const-string v1, "BooksApp"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication$3;->isVolumeLocked(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 605
    const-string v1, "BooksApp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unlocking volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    :cond_0
    monitor-exit p0

    return-void

    .line 603
    .end local v0    # "removed":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
