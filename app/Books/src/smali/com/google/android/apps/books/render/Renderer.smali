.class public interface abstract Lcom/google/android/apps/books/render/Renderer;
.super Ljava/lang/Object;
.source "Renderer.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;
.implements Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/Renderer$PageRange;,
        Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;,
        Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    }
.end annotation


# virtual methods
.method public abstract activateMediaElement(IILjava/lang/String;)I
.end method

.method public abstract applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
.end method

.method public abstract beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
.end method

.method public abstract canFitWidth()Z
.end method

.method public abstract canImmediatelyRedrawHighlights()Z
    .annotation runtime Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;
    .end annotation
.end method

.method public abstract canProvideText()Z
    .annotation runtime Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;
    .end annotation
.end method

.method public abstract cancelPendingRequests()V
.end method

.method public abstract clearAnnotationCaches()V
.end method

.method public abstract convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z
.end method

.method public abstract createHighlightsRectsCache(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/HighlightsRectsCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")",
            "Lcom/google/android/apps/books/widget/HighlightsRectsCache;"
        }
    .end annotation
.end method

.method public abstract createSearchMatchRectsCache(Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ILcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "I",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")",
            "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract displayTwoPages()Z
    .annotation runtime Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;
    .end annotation
.end method

.method public abstract findTouchableItem(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;I)Lcom/google/android/apps/books/render/TouchableItem;
.end method

.method public abstract getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDecorationInsetFromPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering;)I
.end method

.method public abstract getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;
.end method

.method public abstract getGridRowCount()I
.end method

.method public abstract getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "IZ)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/MarginNote;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;
.end method

.method public abstract getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z
.end method

.method public abstract getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;
.end method

.method public abstract getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
.end method

.method public abstract getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I
.end method

.method public abstract getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isPassageForbidden(I)Z
.end method

.method public abstract isPositionEnabled(Lcom/google/android/apps/books/common/Position;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I
.end method

.method public abstract needsBackgroundAnnotationLoad()Z
    .annotation runtime Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;
    .end annotation
.end method

.method public abstract normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
.end method

.method public abstract onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V
.end method

.method public abstract pageBoundsInRendererCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/graphics/Rect;
.end method

.method public abstract pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z
.end method

.method public abstract rendererCoordinatesToPagePointsMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Matrix;
.end method

.method public abstract requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
.end method

.method public abstract requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
.end method

.method public abstract setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V
.end method

.method public abstract setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V
.end method

.method public abstract shouldFitWidth()Z
.end method
