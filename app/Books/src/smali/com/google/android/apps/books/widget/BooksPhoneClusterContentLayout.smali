.class public Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;
.super Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;
.source "BooksPhoneClusterContentLayout.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
.implements Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent",
        "<",
        "Lcom/google/android/apps/books/playcards/BookDocument;",
        ">;",
        "Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;",
        "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController",
        "<",
        "Lcom/google/android/apps/books/playcards/BookDocument;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnimateUponLayout:Z

.field private final mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

.field private mLayoutRequested:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    .line 40
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimateUponLayout:Z

    .line 42
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mLayoutRequested:Z

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->removeViewLocal(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimateUponLayout:Z

    return p1
.end method

.method private removeViewLocal(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->removeView(Landroid/view/View;)V

    .line 183
    return-void
.end method


# virtual methods
.method public isAnimationEnabled()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public maybeRunAnimation()V
    .locals 18

    .prologue
    .line 105
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimateUponLayout:Z

    .line 106
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 108
    .local v2, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v10, v13, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 109
    .local v10, "translationHelper":Lcom/google/android/ublib/view/TranslationHelper;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v13

    float-to-int v13, v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getHeight()I

    move-result v14

    add-int v7, v13, v14

    .line 111
    .local v7, "nextOffscreenStartLocation":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getChildCount()I

    move-result v13

    if-ge v6, v13, :cond_3

    .line 112
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 113
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->isShown()Z

    move-result v13

    if-nez v13, :cond_1

    .line 111
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 117
    :cond_1
    const v13, 0x7f0e003b

    invoke-virtual {v3, v13}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 118
    .local v12, "volumeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v13, v13, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mPreviousLocations:Ljava/util/Map;

    invoke-interface {v13, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;

    .line 123
    .local v8, "previousChildTop":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 124
    .local v4, "childHeight":I
    invoke-virtual {v10, v3}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v13

    float-to-int v5, v13

    .line 125
    .local v5, "childTop":I
    if-eqz v8, :cond_2

    .line 127
    iget-object v13, v8, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->location:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    sub-int v11, v13, v5

    .line 128
    .local v11, "upDelta":I
    add-int v13, v5, v4

    invoke-static {v13, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 139
    :goto_2
    if-eqz v11, :cond_0

    .line 140
    invoke-virtual {v10, v3}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v13

    const-string v14, "translationY"

    const/4 v15, 0x2

    new-array v15, v15, [F

    const/16 v16, 0x0

    int-to-float v0, v11

    move/from16 v17, v0

    aput v17, v15, v16

    const/16 v16, 0x1

    const/16 v17, 0x0

    aput v17, v15, v16

    invoke-static {v13, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 144
    .local v9, "translateAnimatorY":Landroid/animation/ObjectAnimator;
    const-wide/16 v14, 0x12c

    invoke-virtual {v9, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 145
    const-wide/16 v14, 0x0

    invoke-virtual {v9, v14, v15}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 146
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 133
    .end local v9    # "translateAnimatorY":Landroid/animation/ObjectAnimator;
    .end local v11    # "upDelta":I
    :cond_2
    sub-int v11, v7, v5

    .line 136
    .restart local v11    # "upDelta":I
    add-int/2addr v7, v4

    goto :goto_2

    .line 150
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childHeight":I
    .end local v5    # "childTop":I
    .end local v8    # "previousChildTop":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    .end local v11    # "upDelta":I
    .end local v12    # "volumeId":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v13, v2}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->playAnimators(Ljava/util/List;)V

    .line 151
    return-void
.end method

.method public onAnimationEnd()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mLayoutRequested:Z

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimateUponLayout:Z

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mLayoutRequested:Z

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->requestLayout()V

    .line 99
    :cond_0
    return-void
.end method

.method public onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V
    .locals 7
    .param p2, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p3, "cardMetaData"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            ")V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "newDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getDocs()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->onDocumentsChanged(Ljava/util/List;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->setDocuments(Ljava/util/List;)V

    .line 159
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 20
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->isAnimating()Z

    move-result v2

    if-nez v2, :cond_3

    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getWidth()I

    move-result v8

    .line 56
    .local v8, "availableWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v14

    .line 57
    .local v14, "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    invoke-virtual {v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getWidth()I

    move-result v13

    .line 58
    .local v13, "columns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getPaddingLeft()I

    move-result v2

    sub-int v2, v8, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getPaddingRight()I

    move-result v6

    sub-int/2addr v2, v6

    div-int v12, v2, v13

    .line 59
    .local v12, "cellSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getPaddingTop()I

    move-result v16

    .line 60
    .local v16, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getPaddingLeft()I

    move-result v15

    .line 62
    .local v15, "paddingLeft":I
    invoke-virtual {v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v17

    .line 63
    .local v17, "tileCount":I
    const/16 v18, 0x0

    .local v18, "tileIndex":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    .line 64
    move/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v19

    .line 65
    .local v19, "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v10

    .line 66
    .local v10, "cardXStart":I
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getYStart()I

    move-result v11

    .line 67
    .local v11, "cardYStart":I
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 69
    .local v3, "card":Landroid/view/View;
    if-nez v3, :cond_0

    .line 70
    move/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->removeTileMetadata(I)V

    .line 63
    :goto_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 74
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 75
    .local v9, "cardHeight":I
    mul-int v2, v12, v10

    add-int v4, v15, v2

    .line 76
    .local v4, "cardLeft":I
    mul-int v2, v9, v11

    add-int v5, v16, v2

    .line 78
    .local v5, "cardTop":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    goto :goto_1

    .line 82
    .end local v3    # "card":Landroid/view/View;
    .end local v4    # "cardLeft":I
    .end local v5    # "cardTop":I
    .end local v9    # "cardHeight":I
    .end local v10    # "cardXStart":I
    .end local v11    # "cardYStart":I
    .end local v19    # "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimateUponLayout:Z

    if-eqz v2, :cond_2

    .line 83
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->maybeRunAnimation()V

    .line 90
    .end local v8    # "availableWidth":I
    .end local v12    # "cellSize":I
    .end local v13    # "columns":I
    .end local v14    # "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .end local v15    # "paddingLeft":I
    .end local v16    # "paddingTop":I
    .end local v17    # "tileCount":I
    .end local v18    # "tileIndex":I
    :goto_2
    return-void

    .line 85
    .restart local v8    # "availableWidth":I
    .restart local v12    # "cellSize":I
    .restart local v13    # "columns":I
    .restart local v14    # "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .restart local v15    # "paddingLeft":I
    .restart local v16    # "paddingTop":I
    .restart local v17    # "tileCount":I
    .restart local v18    # "tileIndex":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mAnimationHelper:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->recordLocations()V

    goto :goto_2

    .line 88
    .end local v8    # "availableWidth":I
    .end local v12    # "cellSize":I
    .end local v13    # "columns":I
    .end local v14    # "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .end local v15    # "paddingLeft":I
    .end local v16    # "paddingTop":I
    .end local v17    # "tileCount":I
    .end local v18    # "tileIndex":I
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->mLayoutRequested:Z

    goto :goto_2
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout$1;-><init>(Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->post(Ljava/lang/Runnable;)Z

    .line 195
    return-void
.end method

.method public setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V
    .locals 3
    .param p1, "playCard"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "book"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p3, "addView"    # Z

    .prologue
    .line 163
    if-nez p2, :cond_1

    .line 164
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bindNoDocument()V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->bindCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/model/Document;)V

    .line 167
    new-instance v0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->getCardClickCallback()Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    move-result-object v2

    invoke-direct {v0, v1, p2, v2}, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;-><init>(Landroid/content/Context;Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;)V

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v0, 0x7f0e003b

    invoke-virtual {p2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setTag(ILjava/lang/Object;)V

    .line 170
    if-eqz p3, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/model/Document;Z)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "x1"    # Lcom/google/android/ublib/cardlib/model/Document;
    .param p3, "x2"    # Z

    .prologue
    .line 32
    check-cast p2, Lcom/google/android/apps/books/playcards/BookDocument;

    .end local p2    # "x1":Lcom/google/android/ublib/cardlib/model/Document;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BooksPhoneClusterContentLayout;->setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V

    return-void
.end method
