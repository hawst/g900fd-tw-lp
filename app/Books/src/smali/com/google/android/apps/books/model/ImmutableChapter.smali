.class public Lcom/google/android/apps/books/model/ImmutableChapter;
.super Ljava/lang/Object;
.source "ImmutableChapter.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Chapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    }
.end annotation


# instance fields
.field private final mDepth:I

.field private final mId:Ljava/lang/String;

.field private final mReadingPosition:Ljava/lang/String;

.field private final mStartPageIndex:I

.field private final mStartSegmentIndex:I

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "startSegmentIndex"    # I
    .param p4, "startPageIndex"    # I
    .param p5, "depth"    # I
    .param p6, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mId:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mTitle:Ljava/lang/String;

    .line 23
    iput p3, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mStartSegmentIndex:I

    .line 24
    iput p4, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mStartPageIndex:I

    .line 25
    iput p5, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mDepth:I

    .line 26
    iput-object p6, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mReadingPosition:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getDepth()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mDepth:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getReadingPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mReadingPosition:Ljava/lang/String;

    return-object v0
.end method

.method public getStartPageIndex()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mStartPageIndex:I

    return v0
.end method

.method public getStartSegmentIndex()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mStartSegmentIndex:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter;->mTitle:Ljava/lang/String;

    return-object v0
.end method
