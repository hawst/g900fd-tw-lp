.class public Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;
.super Landroid/os/AsyncTask;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VolumeMetadataLoadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/VolumeMetadata;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActivity:Landroid/app/Activity;

.field private final mCheckShelfMembership:Z

.field private final mUpdateVolumeOverview:Z

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;ZZ)V
    .locals 1
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "volumeId"    # Ljava/lang/String;
    .param p5, "updateVolumeOverview"    # Z
    .param p6, "checkShelfMembership"    # Z

    .prologue
    .line 7469
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 7470
    const-string v0, "missing activity"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mActivity:Landroid/app/Activity;

    .line 7471
    const-string v0, "missing account"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mAccount:Landroid/accounts/Account;

    .line 7472
    const-string v0, "missing volumeId"

    invoke-static {p4, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mVolumeId:Ljava/lang/String;

    .line 7473
    iput-boolean p5, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mUpdateVolumeOverview:Z

    .line 7474
    iput-boolean p6, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mCheckShelfMembership:Z

    .line 7475
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7480
    :try_start_0
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mVolumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mAccount:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;-><init>(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/apps/books/util/Logger;)V

    .line 7483
    .local v0, "data":Lcom/google/android/apps/books/model/VolumeMetadataImpl;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    const-string v2, "#before populating metadata"

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 7484
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mUpdateVolumeOverview:Z

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mCheckShelfMembership:Z

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/android/apps/books/model/AssetDirectories;->makeAssetDirectory(Landroid/content/Context;)Lcom/google/android/apps/books/model/AssetDirectory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->productionMyEbooksChecker(Landroid/content/ContentResolver;)Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->populateFrom(ZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z

    move-result v1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDownloadedManifest:Z
    invoke-static {v9, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 7488
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    const-string v2, "#after populating metadata"

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 7490
    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$Access;->NO_VIEW:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7492
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/RentalUtils;->isExpiredNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7493
    new-instance v7, Lcom/google/android/apps/books/util/BlockedContentReason$NonSampleExpiredRentalException;

    const-string v1, "expired NO_VIEW rental"

    invoke-direct {v7, v1}, Lcom/google/android/apps/books/util/BlockedContentReason$NonSampleExpiredRentalException;-><init>(Ljava/lang/String;)V

    .line 7497
    .local v7, "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v7}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    .line 7507
    .end local v0    # "data":Lcom/google/android/apps/books/model/VolumeMetadataImpl;
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v1

    .line 7495
    .restart local v0    # "data":Lcom/google/android/apps/books/model/VolumeMetadataImpl;
    :cond_0
    new-instance v7, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAccessException;

    const-string v1, "tried opening a NO_VIEW volume"

    invoke-direct {v7, v1}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAccessException;-><init>(Ljava/lang/String;)V

    .restart local v7    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 7500
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getContentVersion()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 7501
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No content version stored for volume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 7502
    .local v8, "msg":Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    invoke-direct {v1, v8}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    goto :goto_1

    .line 7505
    .end local v8    # "msg":Ljava/lang/String;
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 7506
    .end local v0    # "data":Lcom/google/android/apps/books/model/VolumeMetadataImpl;
    :catch_0
    move-exception v7

    .line 7507
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-static {v7}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 7461
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7513
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeMetadata;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onLoadedVolumeMetadata(Lcom/google/android/apps/books/util/ExceptionOr;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 7514
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 7461
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
