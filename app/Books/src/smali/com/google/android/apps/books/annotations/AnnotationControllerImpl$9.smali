.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->fetchAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$imageH:I

.field final synthetic val$imageW:I

.field final synthetic val$key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

.field final synthetic val$processor:Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;II)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$processor:Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;

    iput p5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$imageW:I

    iput p6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$imageH:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 451
    const-string v0, "AnnotationC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    const-string v0, "AnnotationC"

    const-string v1, "Downloading annotation data from server."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$800(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/AnnotationServerController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$processor:Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;

    iget v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$imageW:I

    iget v5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;->val$imageH:I

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->downloadAnnotationDatas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Collection;Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;II)V

    .line 456
    return-void
.end method
