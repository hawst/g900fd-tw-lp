.class public Lcom/google/android/apps/books/model/RecentSearchesUtil;
.super Ljava/lang/Object;
.source "RecentSearchesUtil.java"


# direct methods
.method public static setLastVolumeSearch(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "lastSearch"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v0, 0xa

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/util/CollectionUtils;->moveEntryToFirst(Ljava/util/List;Ljava/lang/Object;I)V

    .line 13
    return-void
.end method
