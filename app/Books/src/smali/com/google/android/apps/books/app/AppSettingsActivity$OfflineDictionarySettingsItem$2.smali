.class Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->refresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 482
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 485
    .local p1, "dictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 486
    .local v2, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 487
    .local v1, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    .end local v1    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->updateSummary(Ljava/util/List;)V
    invoke-static {v3, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->access$500(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;Ljava/util/List;)V

    .line 490
    return-void
.end method
