.class public Lcom/google/android/apps/books/service/BooksSafeFetchController;
.super Ljava/lang/Object;
.source "BooksSafeFetchController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;,
        Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
    }
.end annotation


# instance fields
.field private final mDrainer:Lcom/google/android/apps/books/service/Drainer;

.field private final mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

.field private final mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

.field private mFirstException:Ljava/lang/Throwable;

.field private final mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

.field private final mSyncContext:Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;

.field private final mSyncResult:Landroid/content/SyncResult;


# direct methods
.method public constructor <init>(Landroid/content/SyncResult;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;Lcom/google/android/apps/books/service/Drainer;)V
    .locals 1
    .param p1, "syncResult"    # Landroid/content/SyncResult;
    .param p2, "fileStorageManager"    # Lcom/google/android/apps/books/common/FileStorageManager;
    .param p3, "fetchingExecutor"    # Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;
    .param p4, "planningExecutor"    # Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;
    .param p5, "drainer"    # Lcom/google/android/apps/books/service/Drainer;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFirstException:Ljava/lang/Throwable;

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncResult:Landroid/content/SyncResult;

    .line 81
    iput-object p2, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    .line 82
    iput-object p3, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    .line 83
    iput-object p4, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    .line 84
    iput-object p5, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    .line 85
    new-instance v0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;-><init>(Lcom/google/android/apps/books/service/BooksSafeFetchController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncContext:Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/service/BooksSafeFetchController;)Lcom/google/android/apps/books/common/FileStorageManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p1, "x1"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/service/SyncContextChangedException;,
            Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;,
            Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;,
            Lcom/google/android/apps/books/data/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->ensure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    return-void
.end method

.method private callable(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/apps/books/service/BooksSafeFetchController$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/service/BooksSafeFetchController$1;-><init>(Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    return-object v0
.end method

.method private ensure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V
    .locals 3
    .param p1, "delegate"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/service/SyncContextChangedException;,
            Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;,
            Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;,
            Lcom/google/android/apps/books/data/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v2, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    invoke-interface {v2}, Lcom/google/android/apps/books/common/FileStorageManager;->getLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    .line 103
    .local v1, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 105
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncContext:Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;

    invoke-virtual {v2}, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->check()V

    .line 106
    invoke-interface {p1}, Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;->ensure()V
    :try_end_0
    .catch Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apps/books/service/SyncContextChangedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/apps/books/data/OutOfSpaceException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 133
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->debugLog(Ljava/lang/Exception;)V

    .line 111
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    .end local v0    # "e":Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;
    :catchall_0
    move-exception v2

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 112
    :catch_1
    move-exception v0

    .line 113
    .local v0, "e":Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->debugLog(Ljava/lang/Exception;)V

    .line 114
    throw v0

    .line 115
    .end local v0    # "e":Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;
    :catch_2
    move-exception v0

    .line 116
    .local v0, "e":Lcom/google/android/apps/books/service/SyncContextChangedException;
    throw v0

    .line 117
    .end local v0    # "e":Lcom/google/android/apps/books/service/SyncContextChangedException;
    :catch_3
    move-exception v0

    .line 118
    .local v0, "e":Lcom/google/android/apps/books/data/OutOfSpaceException;
    throw v0

    .line 119
    .end local v0    # "e":Lcom/google/android/apps/books/data/OutOfSpaceException;
    :catch_4
    move-exception v0

    .line 120
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->errorLog(Ljava/lang/Exception;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncResult:Landroid/content/SyncResult;

    invoke-static {v2, v0}, Lcom/google/android/apps/books/service/SyncAdapter;->registerSoftError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 123
    .local v0, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :try_start_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->errorLog(Ljava/lang/Exception;)V

    .line 127
    iget-object v2, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncResult:Landroid/content/SyncResult;

    invoke-static {v2, v0}, Lcom/google/android/apps/books/service/SyncAdapter;->registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 131
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 128
    .end local v0    # "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :catch_6
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->errorLog(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 131
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method


# virtual methods
.method protected debugLog(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 136
    const-string v0, "BooksSyncEnsurer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "BooksSyncEnsurer"

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    return-void
.end method

.method public drain()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/ExecutionException;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public enqueueEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V
    .locals 2
    .param p1, "ensureOperation"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->callable(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 166
    return-void
.end method

.method public enqueuePlan(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 170
    return-void
.end method

.method protected errorLog(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFirstException:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    .line 143
    iput-object p1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFirstException:Ljava/lang/Throwable;

    .line 145
    :cond_0
    const-string v0, "BooksSyncEnsurer"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    const-string v0, "BooksSyncEnsurer"

    const-string v1, "errorLog: "

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    :cond_1
    return-void
.end method

.method public getFirstException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFirstException:Ljava/lang/Throwable;

    return-object v0
.end method

.method public numIoExceptions()J
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController;->mSyncResult:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    return-wide v0
.end method
