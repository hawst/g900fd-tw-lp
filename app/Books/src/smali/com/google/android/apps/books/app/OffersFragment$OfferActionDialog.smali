.class public Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OfferActionDialog"
.end annotation


# instance fields
.field private mImageFuture:Lcom/google/android/apps/books/common/ImageFuture;

.field private mThumbnailImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 398
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->showError(ILandroid/view/View;)V

    return-void
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1

    .prologue
    .line 506
    invoke-static {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getOffersCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private setupButtons(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialogView"    # Landroid/view/View;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 432
    invoke-static {p3}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 433
    .local v4, "volumeId":Ljava/lang/String;
    const v0, 0x7f0e01c0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 434
    .local v6, "addToLibraryButton":Landroid/view/View;
    const v0, 0x7f0e01bb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 435
    .local v2, "mainLayout":Landroid/view/View;
    const v0, 0x7f0e01ba

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 436
    .local v3, "spinner":Landroid/view/View;
    new-instance v5, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;

    invoke-direct {v5, p0, p2}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$1;-><init>(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;Landroid/view/View;)V

    .line 458
    .local v5, "offerResultConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;-><init>(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;Landroid/view/View;Landroid/view/View;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    return-void
.end method

.method private setupDescription(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialogView"    # Landroid/view/View;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 411
    const v2, 0x7f0e01be

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 413
    .local v0, "description":Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<i>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/apps/books/util/VolumeArguments;->getTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</i>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 416
    .local v1, "title":Ljava/lang/String;
    const v2, 0x7f0f01bb

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    return-void
.end method

.method private setupThumbnail(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialogView"    # Landroid/view/View;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 421
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090167

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 423
    .local v1, "thumbnailSize":I
    new-instance v0, Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 425
    .local v0, "imageConstraints":Lcom/google/android/ublib/util/ImageConstraints;
    const v2, 0x7f0e01bc

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mThumbnailImageView:Landroid/widget/ImageView;

    .line 426
    invoke-static {p3}, Lcom/google/android/apps/books/util/VolumeArguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/apps/books/util/VolumeArguments;->getCoverImageUri(Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getBookCoverImage(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    invoke-static {p1, v2, v3, v0, p0}, Lcom/google/android/apps/books/app/OffersFragment;->access$500(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mImageFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 429
    return-void
.end method

.method private showError(ILandroid/view/View;)V
    .locals 9
    .param p1, "errorId"    # I
    .param p2, "dialogView"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 516
    new-instance v2, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$3;-><init>(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;)V

    .line 522
    .local v2, "listener":Landroid/view/View$OnClickListener;
    const v6, 0x7f0e01c0

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 523
    .local v0, "addToLibraryButton":Landroid/view/View;
    const v6, 0x7f0e01bb

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 524
    .local v3, "mainLayout":Landroid/view/View;
    const v6, 0x7f0e01ba

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 526
    .local v5, "spinner":Landroid/view/View;
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 527
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 530
    const v6, 0x7f0e01be

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 532
    const v6, 0x7f0e01bf

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 533
    .local v1, "errorText":Landroid/widget/TextView;
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 534
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 537
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 538
    const v6, 0x7f0e01c1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 540
    .local v4, "okButton":Landroid/view/View;
    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 542
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 477
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 479
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 480
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0400b8

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 482
    .local v3, "view":Landroid/view/View;
    const-string v4, "dialogError"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 483
    const-string v4, "dialogError"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->showError(ILandroid/view/View;)V

    .line 494
    :goto_0
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 485
    :cond_0
    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->setupDescription(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V

    .line 486
    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->setupButtons(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V

    .line 487
    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->setupThumbnail(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mImageFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mImageFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 502
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 503
    return-void
.end method

.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 404
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mThumbnailImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mThumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 407
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->mImageFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 408
    return-void
.end method
