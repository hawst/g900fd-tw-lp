.class public abstract Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;
.super Ljava/lang/Object;
.source "BooksDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CachedRecommendations"
.end annotation


# instance fields
.field public lastModifiedMillis:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getRecommendedBooks()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end method
