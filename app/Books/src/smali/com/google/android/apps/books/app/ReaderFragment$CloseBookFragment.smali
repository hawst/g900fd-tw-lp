.class public Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;
.super Lcom/google/android/apps/books/app/ErrorFragment;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CloseBookFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5953
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ErrorFragment;-><init>()V

    .line 5954
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "message"    # Ljava/lang/CharSequence;
    .param p3, "positiveText"    # Ljava/lang/CharSequence;
    .param p4, "positiveIntent"    # Landroid/content/Intent;
    .param p5, "negativeText"    # Ljava/lang/CharSequence;
    .param p6, "negativeIntent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    .line 5960
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/app/ErrorFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .line 5961
    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 5965
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    .line 5966
    .local v0, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    if-eqz v0, :cond_0

    .line 5967
    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->closeBook()V

    .line 5969
    :cond_0
    return-void
.end method
