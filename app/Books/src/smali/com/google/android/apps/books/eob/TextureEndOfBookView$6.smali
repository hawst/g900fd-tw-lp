.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 270
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    sget-object v2, Lcom/google/android/apps/books/util/FinskyCampaignIds;->EOB_SHARE:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->val$activity:Landroid/app/Activity;

    # invokes: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getShareUrl(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/Context;)Landroid/net/Uri;
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$500(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 274
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$6;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startShare(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/net/Uri;)V

    .line 278
    :cond_0
    return-void
.end method
