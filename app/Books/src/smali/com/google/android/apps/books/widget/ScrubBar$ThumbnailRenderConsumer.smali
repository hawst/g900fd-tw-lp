.class Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;
.super Ljava/lang/Object;
.source "ScrubBar.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RenderResponseConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbnailRenderConsumer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ScrubBar;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar;)V
    .locals 0

    .prologue
    .line 1100
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar;Lcom/google/android/apps/books/widget/ScrubBar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/ScrubBar$1;

    .prologue
    .line 1100
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 1100
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->paint(Lcom/google/android/apps/books/render/PagePainter;)V

    return-void
.end method

.method private paint(Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 4
    .param p1, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/books/render/PagePainter;->canStillDraw()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1120
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1000(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 1121
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1000(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # invokes: Lcom/google/android/apps/books/widget/ScrubBar;->getThumbnailMaxSizeInPixels()Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1100(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/MathUtils;->fitIntoEvenHorizontal(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 1122
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1000(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1000(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    sget-object v3, Lcom/google/android/apps/books/widget/ScrubBar;->UNDO_THUMBNAIL_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1124
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/render/PagePainterUtils;->drawToScale(Lcom/google/android/apps/books/render/PagePainter;Landroid/graphics/Bitmap;)Z

    .line 1125
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mSosImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1200(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 1126
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mSosImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1200(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1127
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$1300(Lcom/google/android/apps/books/widget/ScrubBar;)Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1128
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->requestLayout()V

    .line 1130
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method


# virtual methods
.method public isActive()Z
    .locals 1

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailRenderConsumer:Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 1145
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMissingPosition()V
    .locals 0

    .prologue
    .line 1140
    return-void
.end method

.method public onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 1107
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->paint(Lcom/google/android/apps/books/render/PagePainter;)V

    .line 1108
    invoke-interface {p2}, Lcom/google/android/apps/books/render/PagePainter;->isUpdatable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    new-instance v1, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer$1;-><init>(Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;Lcom/google/android/apps/books/render/PagePainter;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1116
    :cond_0
    return-void
.end method

.method public onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 0
    .param p1, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 1135
    return-void
.end method
