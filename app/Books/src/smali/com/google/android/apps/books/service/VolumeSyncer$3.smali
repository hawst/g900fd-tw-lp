.class Lcom/google/android/apps/books/service/VolumeSyncer$3;
.super Ljava/lang/Object;
.source "VolumeSyncer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/service/VolumeSyncer;->downloadVolumeContent(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

.field final synthetic val$noIoExceptions:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/VolumeSyncer;Z)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$3;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    iput-boolean p2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$3;->val$noIoExceptions:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer$3;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # getter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mShowedStartNotification:Z
    invoke-static {v0}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$200(Lcom/google/android/apps/books/service/VolumeSyncer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer$3;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # getter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    invoke-static {v0}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$300(Lcom/google/android/apps/books/service/VolumeSyncer;)Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$3;->val$noIoExceptions:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->finishedVolumeDownload(Z)V

    .line 158
    :cond_0
    return-void
.end method
