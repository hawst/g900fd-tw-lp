.class Lcom/google/android/apps/books/app/HomeFragment$29;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->removeFromRecommendations(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2564
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$29;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$29;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 1
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 2567
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$29;->val$volumeId:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->addDismissedRecommendation(Ljava/lang/String;)V

    .line 2568
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2564
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$29;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
