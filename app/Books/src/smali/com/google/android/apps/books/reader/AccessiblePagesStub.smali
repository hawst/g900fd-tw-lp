.class public Lcom/google/android/apps/books/reader/AccessiblePagesStub;
.super Ljava/lang/Object;
.source "AccessiblePagesStub.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setImportantForAccessibility(Z)V
    .locals 0
    .param p1, "isImportant"    # Z

    .prologue
    .line 27
    return-void
.end method

.method public setPageRendering(ILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 0
    .param p1, "pageIndex"    # I
    .param p2, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 22
    return-void
.end method

.method public setSelectionChangedListener(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    .prologue
    .line 13
    return-void
.end method

.method public wantsPageRenderings()Z
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return v0
.end method
