.class Lcom/google/android/apps/books/widget/GeoAnnotationView$2;
.super Ljava/lang/Object;
.source "GeoAnnotationView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/GeoAnnotationView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$100(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_WIKIPEDIA_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logGeoAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$100(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 159
    :cond_0
    return-void
.end method
