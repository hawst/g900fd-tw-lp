.class Lcom/google/android/apps/books/api/TranslationServerController$2$1;
.super Ljava/lang/Object;
.source "TranslationServerController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/api/TranslationServerController$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/api/TranslationServerController$2;

.field final synthetic val$result:Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/api/TranslationServerController$2;Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController$2$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$2;

    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2$1;->val$result:Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController$2$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$2;

    iget-object v0, v0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;

    iget-object v1, p0, Lcom/google/android/apps/books/api/TranslationServerController$2$1;->val$result:Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;

    iget-object v1, v1, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;->detectedLanguage:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2$1;->val$result:Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;

    iget-object v2, v2, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;->translation:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;->onCompleted(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method
