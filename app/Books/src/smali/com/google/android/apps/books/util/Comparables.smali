.class public Lcom/google/android/apps/books/util/Comparables;
.super Ljava/lang/Object;
.source "Comparables.java"


# direct methods
.method public static max(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "a":Ljava/lang/Comparable;, "TT;"
    .local p1, "b":Ljava/lang/Comparable;, "TT;"
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .end local p1    # "b":Ljava/lang/Comparable;, "TT;"
    :goto_0
    return-object p1

    .restart local p1    # "b":Ljava/lang/Comparable;, "TT;"
    :cond_0
    move-object p1, p0

    goto :goto_0
.end method

.method public static min(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "a":Ljava/lang/Comparable;, "TT;"
    .local p1, "b":Ljava/lang/Comparable;, "TT;"
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .end local p0    # "a":Ljava/lang/Comparable;, "TT;"
    :goto_0
    return-object p0

    .restart local p0    # "a":Ljava/lang/Comparable;, "TT;"
    :cond_0
    move-object p0, p1

    goto :goto_0
.end method
