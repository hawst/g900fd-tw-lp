.class Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadPageRequest"
.end annotation


# instance fields
.field final offset:Ljava/lang/Integer;

.field final page:I

.field final passage:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IIILjava/lang/Integer;)V
    .locals 6
    .param p2, "requestId"    # I
    .param p3, "passage"    # I
    .param p4, "page"    # I
    .param p5, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 411
    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 412
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->passage:I

    .line 413
    iput-object p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->offset:Ljava/lang/Integer;

    .line 414
    iput p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->page:I

    .line 415
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 419
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issuing LoadPageRequest "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->passage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->offset:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->passage:I

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->page:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->offset:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    .line 424
    return-void
.end method
