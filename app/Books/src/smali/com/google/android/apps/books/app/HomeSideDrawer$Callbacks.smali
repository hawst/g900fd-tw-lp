.class public interface abstract Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeSideDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getAccount()Landroid/accounts/Account;
.end method

.method public abstract getActivity()Landroid/app/Activity;
.end method

.method public abstract getLibraryMode()Ljava/lang/String;
.end method

.method public abstract onSideDrawerClosed()V
.end method

.method public abstract onSideDrawerOpened()V
.end method

.method public abstract refreshCurrentHomeView()V
.end method

.method public abstract showHelpAndFeedback()V
.end method

.method public abstract showMyLibrary()V
.end method

.method public abstract showReadNow()V
.end method

.method public abstract showSettings()V
.end method

.method public abstract showShop()V
.end method

.method public abstract switchAccountTo(Ljava/lang/String;)V
.end method
