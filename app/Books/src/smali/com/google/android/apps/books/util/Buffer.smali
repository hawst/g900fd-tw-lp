.class public Lcom/google/android/apps/books/util/Buffer;
.super Ljava/lang/Object;
.source "Buffer.java"


# instance fields
.field public final bytes:I

.field public final data:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v2, 0x0

    .line 30
    .local v2, "bytesRead":I
    const/16 v4, 0x4000

    new-array v0, v4, [B

    .line 33
    .local v0, "buffer":[B
    :cond_0
    array-length v4, v0

    sub-int/2addr v4, v2

    const/16 v5, 0x1000

    if-ge v4, v5, :cond_1

    .line 34
    array-length v4, v0

    mul-int/lit8 v4, v4, 0x2

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 35
    .local v3, "newBuffer":[B
    move-object v0, v3

    .line 37
    .end local v3    # "newBuffer":[B
    :cond_1
    array-length v4, v0

    sub-int/2addr v4, v2

    invoke-virtual {p1, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 38
    .local v1, "bytesIn":I
    if-lez v1, :cond_2

    .line 39
    add-int/2addr v2, v1

    .line 41
    :cond_2
    if-gez v1, :cond_0

    .line 43
    iput-object v0, p0, Lcom/google/android/apps/books/util/Buffer;->data:[B

    .line 44
    iput v2, p0, Lcom/google/android/apps/books/util/Buffer;->bytes:I

    .line 45
    return-void
.end method
