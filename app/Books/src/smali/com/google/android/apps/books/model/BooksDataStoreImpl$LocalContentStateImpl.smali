.class public Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalContentStateImpl"
.end annotation


# instance fields
.field private final mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

.field private final mStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 0
    .param p1, "status"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .param p2, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 2758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2759
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;->mStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 2760
    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    .line 2761
    return-void
.end method


# virtual methods
.method public getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 1

    .prologue
    .line 2768
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .locals 1

    .prologue
    .line 2764
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;->mStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    return-object v0
.end method
