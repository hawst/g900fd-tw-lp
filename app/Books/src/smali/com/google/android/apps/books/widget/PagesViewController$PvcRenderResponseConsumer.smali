.class public interface abstract Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PvcRenderResponseConsumer"
.end annotation


# virtual methods
.method public abstract isPurgeable()Z
.end method

.method public abstract onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
.end method

.method public abstract onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
.end method
