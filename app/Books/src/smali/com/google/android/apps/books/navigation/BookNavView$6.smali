.class Lcom/google/android/apps/books/navigation/BookNavView$6;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/BookNavView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 611
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2800(Lcom/google/android/apps/books/navigation/BookNavView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->isScrolling()Z
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2900(Lcom/google/android/apps/books/navigation/BookNavView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 624
    :cond_0
    :goto_0
    return v0

    .line 615
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->announceCurrentPosition()Z

    move-result v1

    if-nez v1, :cond_0

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$6;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->delay(J)V

    .line 624
    const/4 v0, 0x1

    goto :goto_0
.end method
