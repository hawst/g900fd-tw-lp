.class Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinderHoneycomb;
.super Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;
.source "BooksUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BooksUserContentBinderHoneycomb"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;-><init>(Landroid/content/Context;)V

    .line 320
    return-void
.end method


# virtual methods
.method protected getCoverUri(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)Landroid/net/Uri;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 326
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getCoverUri(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/NetworkOnMainThreadException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 334
    :goto_0
    return-object v1

    .line 327
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Landroid/os/NetworkOnMainThreadException;
    const-string v1, "BooksUserContent"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    const-string v1, "BooksUserContent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Incorrect NetworkOnMainThreadException (thread id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", main thread id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getMainThreadId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
