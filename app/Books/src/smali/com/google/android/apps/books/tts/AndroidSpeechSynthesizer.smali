.class public Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;
.super Ljava/lang/Object;
.source "AndroidSpeechSynthesizer.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;,
        Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;
    }
.end annotation


# instance fields
.field private final mClientCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

.field private final mContext:Landroid/content/Context;

.field private mHandledInitialization:Z

.field private mInitStatus:Ljava/lang/Integer;

.field private final mLocale:Ljava/util/Locale;

.field private final mQueueUntilInitialized:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mToaster:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;

.field private final mTts:Landroid/speech/tts/TextToSpeech;

.field private final mTtsLanguage:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;

.field private mUseNetworkTts:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;ZLjava/util/Locale;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;
    .param p3, "useNetworkTts"    # Z
    .param p4, "locale"    # Ljava/util/Locale;

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mUseNetworkTts:Z

    .line 194
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    .line 196
    iput-object p1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mContext:Landroid/content/Context;

    .line 197
    iput-object p2, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mClientCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    .line 198
    iput-boolean p3, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mUseNetworkTts:Z

    .line 199
    iput-object p4, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mLocale:Ljava/util/Locale;

    .line 201
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 202
    new-instance v0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTtsLanguage:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;

    .line 215
    new-instance v0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mToaster:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;

    .line 225
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    monitor-enter v1

    .line 226
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->maybeHandleInitialization()V

    .line 227
    monitor-exit v1

    .line 228
    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mClientCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->onUtteranceCompleted(Ljava/lang/String;)V

    return-void
.end method

.method private attachUtteranceListener()V
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsMR1OrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->attachUtteranceListenerIcsMR1()V

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->attachUtteranceListenerPreIcsMR1()V

    goto :goto_0
.end method

.method private attachUtteranceListenerIcsMR1()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    new-instance v1, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 305
    return-void
.end method

.method private attachUtteranceListenerPreIcsMR1()V
    .locals 2

    .prologue
    .line 309
    new-instance v0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$4;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V

    .line 316
    .local v0, "listener":Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, v0}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 317
    return-void
.end method

.method private static logTtsLocaleIssue(Ljava/util/Locale;Ljava/lang/String;)V
    .locals 3
    .param p0, "locale"    # Ljava/util/Locale;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 62
    const-string v0, "AndroidTTS"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "AndroidTTS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TTS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void
.end method

.method private maybeHandleInitialization()V
    .locals 3

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mHandledInitialization:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mInitStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mLocale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTtsLanguage:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;

    iget-object v2, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mToaster:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->maybeSetLanguage(Ljava/util/Locale;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;)V

    .line 260
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->attachUtteranceListener()V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mInitStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mClientCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;->onInitializationError()V

    .line 264
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mHandledInitialization:Z

    .line 266
    :cond_1
    return-void
.end method

.method private maybeSetLanguage(Ljava/util/Locale;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;)V
    .locals 2
    .param p1, "bookLocale"    # Ljava/util/Locale;
    .param p2, "ttsLanguage"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;
    .param p3, "toaster"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;

    .prologue
    .line 185
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->shouldSetLanguage(Ljava/util/Locale;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;)Z

    move-result v0

    .line 187
    .local v0, "setLang":Z
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, p1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 190
    :cond_0
    return-void
.end method

.method private onUtteranceCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 381
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$5;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 391
    return-void
.end method

.method private playSilentUtterance(JILjava/lang/String;)V
    .locals 3
    .param p1, "durationInMs"    # J
    .param p3, "queueMode"    # I
    .param p4, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 346
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->playSilentUtteranceLollipop(JILjava/lang/String;)V

    .line 354
    :goto_0
    return-void

    .line 348
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 349
    .local v0, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "utteranceId"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, p1, p2, p3, v0}, Landroid/speech/tts/TextToSpeech;->playSilence(JILjava/util/HashMap;)I

    goto :goto_0
.end method

.method private playSilentUtteranceLollipop(JILjava/lang/String;)V
    .locals 1
    .param p1, "durationInMs"    # J
    .param p3, "queueMode"    # I
    .param p4, "utteranceId"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/speech/tts/TextToSpeech;->playSilentUtterance(JILjava/lang/String;)I

    .line 341
    return-void
.end method

.method public static shouldSetLanguage(Ljava/util/Locale;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;)Z
    .locals 6
    .param p0, "bookLocale"    # Ljava/util/Locale;
    .param p1, "ttsLanguage"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;
    .param p2, "toaster"    # Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;

    .prologue
    const v5, 0x7f0f0138

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    if-nez p0, :cond_0

    .line 174
    :goto_0
    return v2

    .line 110
    :cond_0
    invoke-interface {p1, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    .line 111
    .local v1, "languageAvailability":I
    invoke-interface {p1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;->getLanguage()Ljava/util/Locale;

    move-result-object v0

    .line 113
    .local v0, "defaultTtsLocale":Ljava/util/Locale;
    packed-switch v1, :pswitch_data_0

    .line 171
    const/4 v2, 0x0

    .local v2, "setLang":Z
    goto :goto_0

    .line 118
    .end local v2    # "setLang":Z
    :pswitch_0
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 120
    :cond_1
    invoke-interface {p2, v5, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;->toast(ILjava/util/Locale;)V

    .line 121
    const-string v4, "matches lang but not country/variant"

    invoke-static {p0, v4}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->logTtsLocaleIssue(Ljava/util/Locale;Ljava/lang/String;)V

    .line 124
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    move v2, v3

    .line 126
    .restart local v2    # "setLang":Z
    :cond_4
    goto :goto_0

    .line 132
    .end local v2    # "setLang":Z
    :pswitch_1
    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 133
    invoke-interface {p2, v5, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;->toast(ILjava/util/Locale;)V

    .line 134
    const-string v4, "matches lang/country but not variant"

    invoke-static {p0, v4}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->logTtsLocaleIssue(Ljava/util/Locale;Ljava/lang/String;)V

    .line 137
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    move v2, v3

    .line 140
    .restart local v2    # "setLang":Z
    :cond_7
    goto :goto_0

    .line 147
    .end local v2    # "setLang":Z
    :pswitch_2
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    :cond_8
    move v2, v3

    .line 151
    .restart local v2    # "setLang":Z
    :cond_9
    goto/16 :goto_0

    .line 156
    .end local v2    # "setLang":Z
    :pswitch_3
    const v3, 0x7f0f0139

    invoke-interface {p2, v3, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;->toast(ILjava/util/Locale;)V

    .line 157
    const-string v3, "data need to be installed"

    invoke-static {p0, v3}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->logTtsLocaleIssue(Ljava/util/Locale;Ljava/lang/String;)V

    .line 158
    const/4 v2, 0x0

    .line 159
    .restart local v2    # "setLang":Z
    goto/16 :goto_0

    .line 164
    .end local v2    # "setLang":Z
    :pswitch_4
    const v3, 0x7f0f013a

    invoke-interface {p2, v3, p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;->toast(ILjava/util/Locale;)V

    .line 165
    const-string v3, "engine needs to be installed"

    invoke-static {p0, v3}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->logTtsLocaleIssue(Ljava/util/Locale;Ljava/lang/String;)V

    .line 166
    const/4 v2, 0x0

    .line 167
    .restart local v2    # "setLang":Z
    goto/16 :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private speakPhrase(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "phraseId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 357
    if-nez p1, :cond_0

    .line 363
    const-wide/16 v2, 0x1

    invoke-direct {p0, v2, v3, v4, p2}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->playSilentUtterance(JILjava/lang/String;)V

    .line 373
    :goto_0
    return-void

    .line 367
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 368
    .local v0, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "utteranceId"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-boolean v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mUseNetworkTts:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsMR1OrLater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->setNetworkVoiceIcsMr1(Ljava/util/HashMap;)V

    .line 372
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, p1, v4, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0
.end method


# virtual methods
.method public clearQueue()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 250
    return-void
.end method

.method public enqueuePhrase(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "phraseId"    # Ljava/lang/String;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    monitor-enter v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mInitStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mInitStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 235
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->speakPhrase(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    :goto_0
    monitor-exit v1

    .line 245
    return-void

    .line 236
    :cond_1
    const-string v0, "AndroidTTS"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "AndroidTTS"

    const-string v2, "Dropping phrase since engine failed to initialize"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 242
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onInit(I)V
    .locals 6
    .param p1, "status"    # I

    .prologue
    .line 321
    iget-object v5, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    monitor-enter v5

    .line 322
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mInitStatus:Ljava/lang/Integer;

    .line 326
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->maybeHandleInitialization()V

    .line 329
    iget-object v4, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 330
    .local v0, "entry":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 331
    .local v2, "phrase":Ljava/lang/String;
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 332
    .local v3, "phraseId":Ljava/lang/String;
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->speakPhrase(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 335
    .end local v0    # "entry":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "phrase":Ljava/lang/String;
    .end local v3    # "phraseId":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 334
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mQueueUntilInitialized:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 335
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336
    return-void
.end method

.method public setNetworkVoiceIcsMr1(Ljava/util/HashMap;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "networkTts"

    const-string v1, "true"

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 396
    return-void
.end method
