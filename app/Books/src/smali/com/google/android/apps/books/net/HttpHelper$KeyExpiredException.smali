.class public Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
.super Lcom/google/android/ublib/utils/WrappedIoException;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KeyExpiredException"
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 338
    return-void
.end method
