.class Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;)V
    .locals 0

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(II)V
    .locals 7
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    .line 1313
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$300(Lcom/google/android/apps/books/render/BasicReaderController;)I

    move-result v5

    .line 1319
    .local v5, "newRequestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget v2, v2, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mPassageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v3, v3, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mRanges:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget v4, v4, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mExternalRequestId:I

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/util/Map;IIZ)V

    .line 1321
    .local v0, "newRequest":Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$500(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/PriorityQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 1322
    return-void
.end method
