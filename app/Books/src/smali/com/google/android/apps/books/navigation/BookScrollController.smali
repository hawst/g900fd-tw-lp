.class public Lcom/google/android/apps/books/navigation/BookScrollController;
.super Ljava/lang/Object;
.source "BookScrollController.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;


# instance fields
.field private mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

.field private final mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

.field private final mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private mDecorationChangePending:Z

.field private mDestroyed:Z

.field private final mFullScreenSpreadSize:Landroid/graphics/Point;

.field private mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/GlueWarpListAdapter",
            "<",
            "Lcom/google/android/apps/books/navigation/PageListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private final mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

.field private final mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

.field private final mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/ublib/widget/AbsWarpListView;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/render/SpreadIdentifier;Landroid/graphics/Point;)V
    .locals 3
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p3, "bookSkimView"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p6, "centerSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p7, "fullScreenSpreadSize"    # Landroid/graphics/Point;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            "Lcom/google/android/apps/books/render/Renderer;",
            "Lcom/google/android/ublib/widget/AbsWarpListView;",
            "Lcom/google/android/apps/books/widget/BookView$Callbacks;",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .local p5, "bitmapCache":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<Lcom/google/android/apps/books/navigation/NavPageKey;>;"
    const/4 v2, 0x0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-direct {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .line 47
    new-instance v0, Lcom/google/android/apps/books/navigation/BookScrollController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookScrollController$1;-><init>(Lcom/google/android/apps/books/navigation/BookScrollController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    .line 60
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDecorationChangePending:Z

    .line 62
    new-instance v0, Lcom/google/android/apps/books/navigation/BookScrollController$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookScrollController$2;-><init>(Lcom/google/android/apps/books/navigation/BookScrollController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 288
    new-instance v0, Lcom/google/android/apps/books/navigation/BookScrollController$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookScrollController$3;-><init>(Lcom/google/android/apps/books/navigation/BookScrollController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 240
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 241
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 242
    iput-object p3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 243
    iput-object p4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 244
    iput-object p5, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 245
    iput-object p7, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mFullScreenSpreadSize:Landroid/graphics/Point;

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {p4, v0}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->addRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 249
    invoke-direct {p0, p6}, Lcom/google/android/apps/books/navigation/BookScrollController;->makeAdapter(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setViewAlignment(F)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->setCenterLockPosition(I)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->weaklyAddListView(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 258
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/BookScrollController;)Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookScrollController;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/navigation/BookScrollController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookScrollController;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->unglueAdapter()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/BookScrollController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookScrollController;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->onHeightChanged()V

    return-void
.end method

.method private jumpToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .locals 8
    .param p1, "spreadId"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "isForward"    # Z

    .prologue
    const/4 v7, 0x0

    .line 152
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling()V

    .line 153
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v4, v7}, Lcom/google/android/ublib/widget/AbsWarpListView;->setCenterLockPosition(I)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v2

    .line 156
    .local v2, "oldAdapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/BookScrollController;->makeAdapter(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    .line 157
    .local v0, "newAdapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    invoke-virtual {v0, v7}, Lcom/google/android/apps/books/navigation/PageListAdapter;->setHighlightCurrentSpread(Z)V

    .line 159
    if-eqz p2, :cond_0

    .line 160
    new-instance v4, Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-direct {v4, v2, v0}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    iput-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    .line 161
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 162
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getCount()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getNegativeCount()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingToCenter(I)V

    .line 174
    :goto_0
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 175
    return-void

    .line 164
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v3

    .line 165
    .local v3, "position":I
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v1

    .line 167
    .local v1, "offset":I
    new-instance v4, Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-direct {v4, v0, v2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    iput-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    .line 168
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 169
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getCount()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getNegativeCount()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v4, v5, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->position(II)V

    .line 171
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v4, v7}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingToCenter(I)V

    goto :goto_0
.end method

.method private makeAdapter(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/navigation/PageListAdapter;
    .locals 22
    .param p1, "centerSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/model/Page;

    .line 262
    .local v21, "firstPage":Lcom/google/android/apps/books/model/Page;
    invoke-interface/range {v21 .. v21}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v6

    .line 264
    .local v6, "position":Lcom/google/android/apps/books/common/Position;
    new-instance v1, Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    const/4 v7, 0x0

    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v10, 0x2

    move-object/from16 v8, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/books/navigation/PageRangeLoader;-><init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/SpreadIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 268
    .local v1, "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    div-int/lit8 v13, v2, 0x5

    .line 270
    .local v13, "maxPageWidth":I
    new-instance v7, Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/4 v12, 0x2

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v15

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/BookScrollController;->mFullScreenSpreadSize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    const v18, 0x7f0201ff

    const v19, 0x7f020200

    const/16 v20, 0x1

    move-object/from16 v8, p0

    move-object v10, v1

    invoke-direct/range {v7 .. v20}, Lcom/google/android/apps/books/navigation/PageListAdapter;-><init>(Lcom/google/android/apps/books/util/Destroyable;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/ublib/widget/AbsWarpListView;IIZZZLandroid/graphics/Point;IIZ)V

    .line 277
    .local v7, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    return-object v7
.end method

.method private onHeightChanged()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling()V

    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->makeAdapter(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 307
    return-void
.end method

.method private pageLoaderKnowsPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    .line 184
    .local v0, "pageRangeLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v1

    if-lt p1, v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v1

    if-gt p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private unglueAdapter()V
    .locals 5

    .prologue
    .line 120
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    if-nez v3, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v0

    .line 125
    .local v0, "centerPosition":I
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/navigation/PageListAdapter;

    iput-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v2

    .line 128
    .local v2, "childPosition":I
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v1

    .line 130
    .local v1, "childOffset":I
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v3, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 131
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->position(II)V

    .line 133
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getLeftAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    if-ne v3, v4, :cond_2

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getRightAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    .line 139
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    .line 141
    iget-boolean v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDecorationChangePending:Z

    if-eqz v3, :cond_0

    .line 142
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDecorationChangePending:Z

    .line 143
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->onDecorationsChanged()V

    goto :goto_0

    .line 136
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getLeftAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 311
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDestroyed:Z

    if-nez v0, :cond_1

    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDestroyed:Z

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->removeRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->destroy()V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    .line 320
    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getLeftAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getRightAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    .line 326
    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    .line 329
    :cond_1
    return-void
.end method

.method public getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 83
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    goto :goto_0
.end method

.method public getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    return-object v0
.end method

.method public getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 3
    .param p1, "spreadPosition"    # I

    .prologue
    .line 103
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    if-eqz v2, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 105
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    move v1, p1

    .line 111
    .local v1, "position":I
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    return-object v2

    .line 107
    .end local v0    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    .end local v1    # "position":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 108
    .restart local v0    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    .restart local v1    # "position":I
    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDestroyed:Z

    return v0
.end method

.method public onDecorationsChanged()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->onDecorationsChanged()V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mDecorationChangePending:Z

    goto :goto_0
.end method

.method public redraw()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->invalidateLists()V

    .line 149
    return-void
.end method

.method public scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .locals 10
    .param p1, "newSpreadId"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "toCommit"    # Z

    .prologue
    const v6, 0x7fffffff

    .line 195
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v7}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFinalCenterPosition()I

    move-result v0

    .line 196
    .local v0, "currentPosition":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v3

    .line 198
    .local v3, "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    .line 205
    .local v1, "currentSpreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionComparator()Ljava/util/Comparator;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v9, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v7, v8, v9}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-lez v7, :cond_1

    const/4 v2, 0x1

    .line 208
    .local v2, "isForward":Z
    :goto_0
    if-nez v1, :cond_2

    move v5, v6

    .line 213
    .local v5, "spreadDifference":I
    :goto_1
    if-eq v5, v6, :cond_3

    add-int v6, v0, v5

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/navigation/BookScrollController;->pageLoaderKnowsPosition(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 215
    add-int v4, v0, v5

    .line 229
    .local v4, "position":I
    :goto_2
    if-eq v4, v0, :cond_0

    .line 230
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mBookSkimView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v6, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingToCenter(I)V

    .line 232
    .end local v4    # "position":I
    :cond_0
    :goto_3
    return-void

    .line 205
    .end local v2    # "isForward":Z
    .end local v5    # "spreadDifference":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 208
    .restart local v2    # "isForward":Z
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v7, p1, v1}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v5

    goto :goto_1

    .line 216
    .restart local v5    # "spreadDifference":I
    :cond_3
    if-eqz p2, :cond_4

    .line 217
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/navigation/BookScrollController;->jumpToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    goto :goto_3

    .line 219
    :cond_4
    if-eqz v1, :cond_0

    .line 222
    if-eqz v2, :cond_5

    .line 223
    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v4

    .restart local v4    # "position":I
    goto :goto_2

    .line 225
    .end local v4    # "position":I
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v4

    .restart local v4    # "position":I
    goto :goto_2
.end method

.method public showSpreadInFull(I)V
    .locals 2
    .param p1, "spreadPosition"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mGlueAdapter:Lcom/google/android/apps/books/util/GlueWarpListAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/PageListAdapter;->showSpreadInFullView(I)V

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookScrollController;->mAdapter:Lcom/google/android/apps/books/navigation/PageListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/navigation/PageListAdapter;->showSpreadInFullView(I)V

    goto :goto_0
.end method
