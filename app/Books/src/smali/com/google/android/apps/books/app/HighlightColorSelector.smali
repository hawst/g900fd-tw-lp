.class public Lcom/google/android/apps/books/app/HighlightColorSelector;
.super Landroid/widget/LinearLayout;
.source "HighlightColorSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    }
.end annotation


# instance fields
.field private mCurrentPrefColorIndex:I

.field private mListener:Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;

.field private mShowSelection:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/app/HighlightColorSelector;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HighlightColorSelector;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mCurrentPrefColorIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/HighlightColorSelector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HighlightColorSelector;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mShowSelection:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/HighlightColorSelector;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HighlightColorSelector;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatches(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/HighlightColorSelector;)Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HighlightColorSelector;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mListener:Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;

    return-object v0
.end method

.method private colorizeDrawable(Landroid/graphics/drawable/Drawable;I)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "color"    # I

    .prologue
    .line 54
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 55
    return-void
.end method

.method public static createColorSelector(Landroid/content/Context;Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)Landroid/view/View;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listener"    # Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    .param p2, "showSelection"    # Z
    .param p3, "currentPrefColorIndex"    # I

    .prologue
    .line 109
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040057

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HighlightColorSelector;

    .line 112
    .local v0, "selectorView":Lcom/google/android/apps/books/app/HighlightColorSelector;
    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/books/app/HighlightColorSelector;->init(Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)V

    .line 113
    return-object v0
.end method

.method private init(Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    .param p2, "showSelection"    # Z
    .param p3, "currentPrefColorIndex"    # I

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mListener:Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;

    .line 47
    iput-boolean p2, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mShowSelection:Z

    .line 48
    iput p3, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mCurrentPrefColorIndex:I

    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatches(Z)V

    .line 51
    return-void
.end method

.method public static setupColorSelector(Landroid/view/View;Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)V
    .locals 2
    .param p0, "parentView"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    .param p2, "showSelection"    # Z
    .param p3, "currentPrefColorIndex"    # I

    .prologue
    .line 127
    const v1, 0x7f0e01d1

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HighlightColorSelector;

    .line 129
    .local v0, "selector":Lcom/google/android/apps/books/app/HighlightColorSelector;
    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/books/app/HighlightColorSelector;->init(Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)V

    .line 130
    return-void
.end method

.method private setupSwatch(IIZ)V
    .locals 5
    .param p1, "swatchId"    # I
    .param p2, "prefColorIndex"    # I
    .param p3, "needsListener"    # Z

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 59
    .local v3, "swatch":Landroid/widget/ImageButton;
    invoke-static {p2}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getDrawingColorFromPrefColor(I)I

    move-result v0

    .line 62
    .local v0, "color":I
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mShowSelection:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/android/apps/books/app/HighlightColorSelector;->mCurrentPrefColorIndex:I

    if-ne p2, v4, :cond_0

    const v2, 0x7f0201fa

    .line 65
    .local v2, "id":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HighlightColorSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 66
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/app/HighlightColorSelector;->colorizeDrawable(Landroid/graphics/drawable/Drawable;I)V

    .line 67
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    if-nez p3, :cond_1

    .line 83
    :goto_1
    return-void

    .line 62
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "id":I
    :cond_0
    const v2, 0x7f020085

    goto :goto_0

    .line 73
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "id":I
    :cond_1
    new-instance v4, Lcom/google/android/apps/books/app/HighlightColorSelector$1;

    invoke-direct {v4, p0, p2}, Lcom/google/android/apps/books/app/HighlightColorSelector$1;-><init>(Lcom/google/android/apps/books/app/HighlightColorSelector;I)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method private setupSwatches(Z)V
    .locals 2
    .param p1, "needsListener"    # Z

    .prologue
    .line 88
    const v0, 0x7f0e012c

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatch(IIZ)V

    .line 90
    const v0, 0x7f0e012d

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatch(IIZ)V

    .line 92
    const v0, 0x7f0e012b

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatch(IIZ)V

    .line 94
    const v0, 0x7f0e012e

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatch(IIZ)V

    .line 96
    return-void
.end method
