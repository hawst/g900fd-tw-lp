.class Lcom/google/android/apps/books/app/InfoCardsHelper$3;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/view/View;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

.field final synthetic val$cardProviderOrder:I

.field final synthetic val$selectionSequenceNumber:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/InfoCardsHelper;II)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    iput p2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->val$selectionSequenceNumber:I

    iput p3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->val$cardProviderOrder:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$400(Lcom/google/android/apps/books/app/InfoCardsHelper;)Ljava/util/PriorityQueue;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;

    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    iget v3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->val$selectionSequenceNumber:I

    iget v4, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->val$cardProviderOrder:I

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;-><init>(Lcom/google/android/apps/books/app/InfoCardsHelper;IILcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # invokes: Lcom/google/android/apps/books/app/InfoCardsHelper;->maybeLoadCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$500(Lcom/google/android/apps/books/app/InfoCardsHelper;)V

    .line 206
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 200
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper$3;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
