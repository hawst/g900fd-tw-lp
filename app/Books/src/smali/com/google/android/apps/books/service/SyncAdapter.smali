.class Lcom/google/android/apps/books/service/SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/SyncAdapter$BackgroundThreadFactory;
    }
.end annotation


# instance fields
.field private final mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

.field private final mConfig:Lcom/google/android/apps/books/util/Config;

.field private final mDrainer:Lcom/google/android/apps/books/service/Drainer;

.field private final mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

.field private final mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

.field private final mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

.field private final mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

.field private final mSyncAccountsState:Lcom/google/android/apps/books/sync/SyncAccountsState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 106
    new-instance v1, Lcom/google/android/apps/books/service/Drainer;

    invoke-direct {v1}, Lcom/google/android/apps/books/service/Drainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    .line 151
    new-instance v1, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v1, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mSyncAccountsState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getFileStorageManager()Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/BooksContext;

    .line 156
    .local v0, "appContext":Lcom/google/android/apps/books/common/BooksContext;
    invoke-interface {v0}, Lcom/google/android/apps/books/common/BooksContext;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    .line 157
    invoke-interface {v0}, Lcom/google/android/apps/books/common/BooksContext;->getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    .line 159
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-static {}, Lcom/google/android/apps/books/service/SyncAdapter;->createFetchingExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/service/Drainer;->managed(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-static {}, Lcom/google/android/apps/books/service/SyncAdapter;->createPlanningExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/service/Drainer;->managed(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    .line 162
    return-void
.end method

.method private buildSyncPlan(Landroid/accounts/Account;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)Lcom/google/android/apps/books/service/SyncPlan;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    .prologue
    .line 776
    iget-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mSyncAccountsState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface {v1, v4, v6, v7}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastSyncTime(Ljava/lang/String;J)J

    move-result-wide v2

    .line 778
    .local v2, "lastSync":J
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v0

    .line 780
    .local v0, "downloadContentSilently":Z
    invoke-static {p2, v2, v3, v0}, Lcom/google/android/apps/books/service/SyncPlan;->buildSyncPlan(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;JZ)Lcom/google/android/apps/books/service/SyncPlan;

    move-result-object v1

    return-object v1
.end method

.method private static createFetchingExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 140
    const/4 v0, 0x3

    .line 141
    .local v0, "poolSize":I
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v2, Lcom/google/android/apps/books/service/SyncAdapter$BackgroundThreadFactory;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/service/SyncAdapter$BackgroundThreadFactory;-><init>(Lcom/google/android/apps/books/service/SyncAdapter$1;)V

    invoke-static {v4, v4, v1, v2}, Lcom/google/android/apps/books/util/ExecutorUtils;->createThreadPoolExecutor(IILjava/util/concurrent/TimeUnit;Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    return-object v1
.end method

.method private static createPlanningExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method private downloadVolumeContent(Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;Lcom/google/android/apps/books/service/SingleVolumeSyncUi;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    .locals 6
    .param p1, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p2, "volume"    # Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    .param p3, "volumeSyncUi"    # Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .param p4, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p5, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 764
    new-instance v0, Lcom/google/android/apps/books/service/VolumeSyncer;

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/service/VolumeSyncer;-><init>(Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SingleVolumeSyncUi;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;)V

    .line 766
    .local v0, "syncer":Lcom/google/android/apps/books/service/VolumeSyncer;
    invoke-virtual {v0}, Lcom/google/android/apps/books/service/VolumeSyncer;->downloadVolumeContent()Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    move-result-object v1

    return-object v1
.end method

.method private downloadVolumes(Landroid/accounts/Account;[Ljava/lang/String;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/data/BooksDataController;)Ljava/util/List;
    .locals 20
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeIds"    # [Ljava/lang/String;
    .param p3, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p4, "syncUi"    # Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/service/BooksSafeFetchController;",
            "Lcom/google/android/apps/books/service/SyncService$SyncUi;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 791
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v19

    .line 792
    .local v19, "vvs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    const/4 v3, 0x0

    .line 794
    .local v3, "volumePosition":I
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v15

    .line 795
    .local v15, "store":Lcom/google/android/apps/books/model/BooksDataStore;
    move-object/from16 v10, p2

    .local v10, "arr$":[Ljava/lang/String;
    array-length v13, v10

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v13, :cond_5

    aget-object v18, v10, v12

    .line 797
    .local v18, "volumeId":Ljava/lang/String;
    const-string v2, "BooksSync"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 798
    const-string v2, "BooksSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download volume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    :cond_0
    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v14

    .line 803
    .local v14, "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/books/service/SyncAdapter;->shouldSync(Lcom/google/android/apps/books/model/LocalVolumeData;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 804
    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolume(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v17

    .line 805
    .local v17, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    if-eqz v17, :cond_4

    .line 806
    new-instance v16, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolume(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->getDownloadProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v14, v4}, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;-><init>(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;)V

    .line 808
    .local v16, "syncData":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    new-instance v1, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    move-object/from16 v0, p2

    array-length v4, v0

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->volumeData:Lcom/google/android/apps/books/model/VolumeData;

    move-object/from16 v2, p4

    move-object/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;-><init>(Lcom/google/android/apps/books/service/SyncService$SyncUi;IILandroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 812
    .local v1, "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v9

    move-object/from16 v4, p0

    move-object/from16 v5, p3

    move-object/from16 v6, v16

    move-object v7, v1

    move-object/from16 v8, p5

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/service/SyncAdapter;->downloadVolumeContent(Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;Lcom/google/android/apps/books/service/SingleVolumeSyncUi;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 830
    .end local v1    # "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .end local v16    # "syncData":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    .end local v17    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 795
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 814
    .restart local v1    # "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .restart local v16    # "syncData":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    .restart local v17    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :catch_0
    move-exception v11

    .line 815
    .local v11, "e":Ljava/lang/Exception;
    const-string v2, "BooksSync"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 816
    const-string v2, "BooksSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to download content for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/books/service/SyncAdapter;->shouldAbortSync(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 819
    const-string v2, "BooksSync"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 820
    const-string v2, "BooksSync"

    const-string v4, "Skipping remaining volumes"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    :cond_3
    throw v11

    .line 825
    .end local v1    # "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v16    # "syncData":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    :cond_4
    const-string v2, "BooksSync"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 826
    const-string v2, "BooksSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "downloadVolumes: no volume data for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 833
    .end local v14    # "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v17    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    .end local v18    # "volumeId":Ljava/lang/String;
    :cond_5
    invoke-interface/range {p4 .. p4}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->finishedAllVolumeDownloads()V

    .line 834
    return-object v19
.end method

.method private enqueueVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/service/BooksSafeFetchController;)V
    .locals 3
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p3, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;

    .prologue
    .line 655
    const-string v0, "BooksSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    const-string v0, "BooksSync"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enqueueVolumeCover vid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/service/SyncAdapter$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/service/SyncAdapter$2;-><init>(Lcom/google/android/apps/books/service/SyncAdapter;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V

    invoke-virtual {p3, v0}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueueEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    .line 671
    return-void
.end method

.method private findFirstExternalStorageException(Ljava/lang/Iterable;)Ljava/io/FileNotFoundException;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/util/concurrent/ExecutionException;",
            ">;)",
            "Ljava/io/FileNotFoundException;"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "exceptions":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/util/concurrent/ExecutionException;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutionException;

    .line 514
    .local v1, "exception":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 515
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v3, v0, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    if-eqz v3, :cond_1

    .line 516
    check-cast v0, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    .line 521
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "exception":Ljava/util/concurrent/ExecutionException;
    :goto_0
    return-object v0

    .line 517
    .restart local v0    # "cause":Ljava/lang/Throwable;
    .restart local v1    # "exception":Ljava/util/concurrent/ExecutionException;
    :cond_1
    instance-of v3, v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    if-eqz v3, :cond_0

    .line 518
    check-cast v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    goto :goto_0

    .line 521
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "exception":Ljava/util/concurrent/ExecutionException;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationController;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 426
    .local v0, "app":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v1

    return-object v1
.end method

.method private getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method private getOnlyVolumesWithVersions(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 416
    .local v2, "vvs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeVersion;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    .line 417
    .local v1, "vv":Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    iget-object v3, v1, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 418
    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->asVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 421
    .end local v1    # "vv":Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    :cond_1
    return-object v2
.end method

.method private getVolumesForAccount(Landroid/accounts/Account;)Ljava/util/List;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 632
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    .line 633
    .local v0, "store":Lcom/google/android/apps/books/model/BooksDataStore;
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumeIdsWithMaybeVersions(I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method private maybeSyncDictionaryMetadata(Lcom/google/android/apps/books/data/BooksDataController;Landroid/accounts/Account;)V
    .locals 14
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 332
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 333
    .local v3, "localPreferences":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getLastDictionaryMetadataSync()J

    move-result-wide v4

    .line 334
    .local v4, "lastSyncTime":J
    sget-object v7, Lcom/google/android/apps/books/util/ConfigValue;->DICTIONARY_METADATA_SYNC_THROTTLE_SECONDS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getLong(Landroid/content/Context;)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long v8, v10, v12

    .line 336
    .local v8, "throttleMillis":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    add-long v12, v4, v8

    cmp-long v7, v10, v12

    if-gez v7, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v0

    .line 344
    .local v0, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->syncDictionaryMetadata(Lcom/google/android/ublib/utils/Consumer;)V

    .line 345
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/util/ExceptionOr;

    .line 346
    .local v6, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    invoke-virtual {v6}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 347
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, Lcom/google/android/apps/books/preference/LocalPreferences;->setLastDictionaryMetadataSync(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349
    .end local v0    # "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    .end local v6    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    :catch_0
    move-exception v2

    .line 350
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v7, "BooksSync"

    const/4 v10, 0x6

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 351
    const-string v7, "BooksSync"

    const-string v10, "Error while blocking on sync dictionary metadata"

    invoke-static {v7, v10, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    .locals 6
    .param p0, "syncResult"    # Landroid/content/SyncResult;
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 436
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    .line 442
    .local v0, "stats":Landroid/content/SyncStats;
    monitor-enter v0

    .line 444
    :try_start_0
    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    .line 445
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    const-string v1, "BooksSync"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    const-string v1, "BooksSync"

    const-string v2, "Hard error"

    invoke-static {v1, v2, p1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 449
    :cond_0
    return-void

    .line 445
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method static registerSoftError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    .locals 6
    .param p0, "syncResult"    # Landroid/content/SyncResult;
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 459
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    .line 465
    .local v0, "stats":Landroid/content/SyncStats;
    monitor-enter v0

    .line 467
    :try_start_0
    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 468
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    const-string v1, "BooksSync"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 470
    const-string v1, "BooksSync"

    const-string v2, "Soft error"

    invoke-static {v1, v2, p1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 472
    :cond_0
    return-void

    .line 468
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private shouldAbortSync(Ljava/lang/Exception;)Z
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 501
    instance-of v0, p1, Lcom/google/android/apps/books/service/SyncContextChangedException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldSync(Lcom/google/android/apps/books/model/LocalVolumeData;)Z
    .locals 2
    .param p1, "localVolumeData"    # Lcom/google/android/apps/books/model/LocalVolumeData;

    .prologue
    .line 842
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-eq v0, v1, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private syncAnnotations(Landroid/accounts/Account;Ljava/util/List;)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .local p2, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 367
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationController;

    move-result-object v0

    .line 369
    .local v0, "controller":Lcom/google/android/apps/books/annotations/AnnotationController;
    const-string v2, "BooksSync"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    const-string v2, "BooksSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "started syncAnnotations() # vols="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 373
    .local v1, "latch":Ljava/util/concurrent/CountDownLatch;
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/service/SyncAdapter;->getOnlyVolumesWithVersions(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    aput-object v5, v3, v4

    sget-object v4, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/books/service/SyncAdapter$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/books/service/SyncAdapter$1;-><init>(Lcom/google/android/apps/books/service/SyncAdapter;Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/apps/books/annotations/AnnotationController;->startServerSync(Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    .line 386
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 388
    const-string v2, "BooksSync"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 389
    const-string v2, "BooksSync"

    const-string v3, "finished syncAnnotations()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_1
    return-void
.end method

.method private syncContent(Landroid/accounts/Account;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 15
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p3, "syncUi"    # Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p6, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 704
    const-string v3, "BooksSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 705
    const-string v3, "BooksSync"

    const-string v4, "started syncContent()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->drain()Ljava/util/List;

    move-result-object v10

    .line 710
    .local v10, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    invoke-static {v10}, Lcom/google/android/apps/books/service/SyncAdapter;->throwAnySystemicException(Ljava/lang/Iterable;)V

    .line 712
    move-object/from16 v0, p1

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->buildSyncPlan(Landroid/accounts/Account;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)Lcom/google/android/apps/books/service/SyncPlan;

    move-result-object v12

    .line 714
    .local v12, "plan":Lcom/google/android/apps/books/service/SyncPlan;
    const-string v3, "BooksSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 715
    const-string v3, "BooksSync"

    const-string v4, "plan: %d downloads, %d added"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v12, Lcom/google/android/apps/books/service/SyncPlan;->volumesToDownload:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v12, Lcom/google/android/apps/books/service/SyncPlan;->volumesAddedToLibrary:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    :cond_1
    iget-object v3, v12, Lcom/google/android/apps/books/service/SyncPlan;->volumesAddedToLibrary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 721
    iget-object v3, v12, Lcom/google/android/apps/books/service/SyncPlan;->volumesAddedToLibrary:Ljava/util/List;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->addedVolumes(Landroid/accounts/Account;Ljava/util/List;)V

    .line 725
    :cond_2
    iget-object v3, v12, Lcom/google/android/apps/books/service/SyncPlan;->volumesToDownload:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    .line 726
    .local v14, "volume":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    iget-object v3, v14, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->localData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/service/SyncAdapter;->shouldSync(Lcom/google/android/apps/books/model/LocalVolumeData;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 727
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloading content for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v14}, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v13

    .line 730
    .local v13, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    new-instance v2, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v7, v14, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->volumeData:Lcom/google/android/apps/books/model/VolumeData;

    move-object/from16 v3, p3

    move-object/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;-><init>(Lcom/google/android/apps/books/service/SyncService$SyncUi;IILandroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 732
    .local v2, "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v8

    move-object v3, p0

    move-object/from16 v4, p2

    move-object v5, v14

    move-object v6, v2

    move-object/from16 v7, p5

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/service/SyncAdapter;->downloadVolumeContent(Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;Lcom/google/android/apps/books/service/SingleVolumeSyncUi;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 743
    invoke-interface {v13}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto :goto_0

    .line 734
    .end local v2    # "volumeSyncUi":Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    :catch_0
    move-exception v9

    .line 735
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "BooksSync"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 736
    const-string v3, "BooksSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Problem downloading content for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v9}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 739
    :cond_4
    invoke-direct {p0, v9}, Lcom/google/android/apps/books/service/SyncAdapter;->shouldAbortSync(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 740
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 743
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-interface {v13}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v3

    .restart local v9    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-interface {v13}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto/16 :goto_0

    .line 753
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v13    # "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .end local v14    # "volume":Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->drain()Ljava/util/List;

    .line 754
    iget-object v3, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mSyncAccountsState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, Lcom/google/android/apps/books/sync/SyncAccountsState;->setLastSyncTime(Ljava/lang/String;J)V

    .line 756
    const-string v3, "BooksSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 757
    const-string v3, "BooksSync"

    const-string v4, "finished syncContent()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_7
    return-void
.end method

.method private syncCoversAndContent(Landroid/accounts/Account;ZLcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "download"    # Z
    .param p3, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p4, "syncUi"    # Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .param p5, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p6, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p7, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 530
    .local v10, "contentResolver":Landroid/content/ContentResolver;
    const-string v2, "BooksLazySync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    .line 532
    .local v12, "lazySync":Z
    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    move-object/from16 v0, p6

    move-object/from16 v1, p3

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->syncVolumeCovers(Lcom/google/android/apps/books/data/BooksDataController;Ljava/util/List;Lcom/google/android/apps/books/service/BooksSafeFetchController;)V

    .line 536
    iget-object v2, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    invoke-interface {v2}, Lcom/google/android/apps/books/common/FileStorageManager;->getLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v13

    .line 537
    .local v13, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 539
    :try_start_0
    invoke-static {v10}, Lcom/google/android/apps/books/provider/BooksProvider;->getFileStorageDirectory(Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v9

    .line 540
    .local v9, "baseDir":Ljava/io/File;
    invoke-static {v9}, Lcom/google/android/apps/books/util/FileUtils;->freeBytesOnFilesystem(Ljava/io/File;)J

    move-result-wide v2

    const-wide/32 v4, 0x3938700

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 541
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    const-wide v4, 0x9a7ec800L

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/books/provider/StaleContentDeleter;->deleteStaleContentLocked(Lcom/google/android/apps/books/model/BooksDataStore;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    :cond_0
    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 548
    if-eqz p2, :cond_1

    .line 549
    if-nez v12, :cond_2

    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v11

    .line 552
    .local v11, "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 554
    :try_start_1
    move-object/from16 v0, p7

    invoke-static {v11, v0}, Lcom/google/android/apps/books/provider/StaleContentDeleter;->deleteUnwantedContent(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 556
    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 559
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/service/SyncAdapter;->syncContent(Landroid/accounts/Account;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    .line 572
    .end local v11    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->myEBooksDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v10, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 573
    return-void

    .line 545
    .end local v9    # "baseDir":Ljava/io/File;
    :catchall_0
    move-exception v2

    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 556
    .restart local v9    # "baseDir":Ljava/io/File;
    .restart local v11    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :catchall_1
    move-exception v2

    invoke-interface {v13}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 562
    .end local v11    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_2
    const-string v2, "BooksSync"

    const-string v3, "======== Skipping syncContent() due to lazy flag"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private syncMyEbooks(Lcom/google/android/apps/books/data/BooksDataController;Landroid/content/SyncResult;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .locals 5
    .param p1, "booksDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 395
    const-string v2, "BooksSync"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 396
    const-string v2, "BooksSync"

    const-string v3, "started syncMyEbooks"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {p1, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureMyEbooksVolumes(Lcom/google/android/apps/books/data/BooksDataController;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 408
    .local v1, "results":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    const-string v2, "BooksSync"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 409
    const-string v2, "BooksSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "finished syncMyEbooks, got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_1
    return-object v1

    .line 402
    .end local v1    # "results":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p2, v0}, Lcom/google/android/apps/books/service/SyncAdapter;->registerSoftError(Landroid/content/SyncResult;Ljava/lang/Exception;)V

    .line 406
    throw v0
.end method

.method private syncMyEbooksAnnotations(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getVolumesForAccount(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/service/SyncAdapter;->syncAnnotations(Landroid/accounts/Account;Ljava/util/List;)V

    .line 363
    return-void
.end method

.method private syncVolumeCovers(Lcom/google/android/apps/books/data/BooksDataController;Ljava/util/List;Lcom/google/android/apps/books/service/BooksSafeFetchController;)V
    .locals 6
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p3, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;",
            "Lcom/google/android/apps/books/service/BooksSafeFetchController;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "volumeDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    const/4 v5, 0x3

    .line 642
    const-string v2, "BooksSync"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 643
    const-string v2, "BooksSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "syncVolumeCovers() touching covers for count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/VolumeData;

    .line 646
    .local v1, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/apps/books/service/SyncAdapter;->enqueueVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/service/BooksSafeFetchController;)V

    goto :goto_0

    .line 648
    .end local v1    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    :cond_1
    const-string v2, "BooksSync"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 649
    const-string v2, "BooksSync"

    const-string v3, "finished syncVolumeCovers()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    :cond_2
    return-void
.end method

.method public static throwAnySystemicException(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/util/concurrent/ExecutionException;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/service/SyncContextChangedException;,
            Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;,
            Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;,
            Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
        }
    .end annotation

    .prologue
    .line 483
    .local p0, "exceptions":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/util/concurrent/ExecutionException;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutionException;

    .line 484
    .local v1, "exception":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 485
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v3, v0, Lcom/google/android/apps/books/service/SyncContextChangedException;

    if-eqz v3, :cond_1

    .line 486
    check-cast v0, Lcom/google/android/apps/books/service/SyncContextChangedException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 487
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_1
    instance-of v3, v0, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    if-eqz v3, :cond_2

    .line 488
    check-cast v0, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 489
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_2
    instance-of v3, v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    if-eqz v3, :cond_3

    .line 490
    check-cast v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 491
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_3
    instance-of v3, v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    if-eqz v3, :cond_0

    .line 492
    check-cast v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 495
    .end local v1    # "exception":Ljava/util/concurrent/ExecutionException;
    :cond_4
    return-void
.end method

.method private upSyncAnnotations(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/service/SyncAdapter;->syncAnnotations(Landroid/accounts/Account;Ljava/util/List;)V

    .line 358
    return-void
.end method

.method private upSyncCollectionVolumes(Lcom/google/android/apps/books/data/BooksDataController;Landroid/accounts/Account;J)V
    .locals 5
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "collectionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 581
    const-string v1, "BooksSync"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    const-string v1, "BooksSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "started upSyncCollectionVolumes() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    :cond_0
    :try_start_0
    invoke-static {p1, p3, p4}, Lcom/google/android/apps/books/data/DataControllerUtils;->uploadCollectionMembershipChanges(Lcom/google/android/apps/books/data/BooksDataController;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 596
    :cond_1
    :goto_0
    const-string v1, "BooksSync"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 597
    const-string v1, "BooksSync"

    const-string v2, "finished upSyncCollectionVolumes()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_2
    return-void

    .line 587
    :catch_0
    move-exception v0

    .line 588
    .local v0, "e":Ljava/lang/Exception;
    instance-of v1, v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    if-eqz v1, :cond_3

    .line 589
    check-cast v0, Ljava/io/IOException;

    .end local v0    # "e":Ljava/lang/Exception;
    throw v0

    .line 590
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v1, "BooksSync"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 591
    const-string v1, "BooksSync"

    const-string v2, "Collection volumes upsync error. Device is still online, continuing."

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private upSyncReadingPositions(Landroid/accounts/Account;)V
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    .line 605
    const-string v5, "BooksSync"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 606
    const-string v5, "BooksSync"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "started syncReadingPositions() for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 611
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 612
    .local v2, "resolver":Landroid/content/ContentResolver;
    new-instance v3, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v3, v2, v5}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 616
    .local v3, "synchronizable":Lcom/google/android/apps/books/sync/StatesServerSynchronizable;
    new-instance v4, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;

    iget-object v5, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    iget-object v6, p0, Lcom/google/android/apps/books/service/SyncAdapter;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-direct {v4, v3, v5, p1, v6}, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;-><init>(Lcom/google/android/apps/books/sync/StatesServerSynchronizable;Lcom/google/android/apps/books/api/ApiaryClient;Landroid/accounts/Account;Lcom/google/android/apps/books/util/Config;)V

    .line 618
    .local v4, "upSyncher":Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;
    invoke-virtual {v4}, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->upsync()V
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 626
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "synchronizable":Lcom/google/android/apps/books/sync/StatesServerSynchronizable;
    .end local v4    # "upSyncher":Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;
    :goto_0
    const-string v5, "BooksSync"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 627
    const-string v5, "BooksSync"

    const-string v6, "finished upSyncReadingPositions()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_1
    return-void

    .line 619
    :catch_0
    move-exception v1

    .line 620
    .local v1, "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    throw v1

    .line 621
    .end local v1    # "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    :catch_1
    move-exception v1

    .line 622
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "BooksSync"

    const-string v6, "Reading positions upsync error. Device is still online, continuing."

    invoke-static {v5, v6, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 37
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v21

    .line 169
    .local v21, "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger(Landroid/content/Context;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v16

    .line 170
    .local v16, "logger":Lcom/google/android/apps/books/util/Logger;
    const-string v5, "onPerformSync"

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v31

    .line 172
    .local v31, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    const-string v5, "SyncService.VOLUME_IDS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 174
    .local v34, "volumeIdsString":Ljava/lang/String;
    if-eqz v34, :cond_b

    .line 175
    invoke-static/range {v34 .. v34}, Lcom/google/android/apps/books/service/SyncService;->getVolumeIds(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v33

    .line 179
    .local v33, "volumeIds":[Ljava/lang/String;
    :goto_0
    const-string v5, "upload"

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v32

    .line 180
    .local v32, "uploadOnly":Z
    const-string v5, "SyncService.DOWNLOAD"

    const/4 v6, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 181
    .local v13, "download":Z
    const-string v5, "SyncService.DISPLAY_PROGRESS"

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v23

    .line 183
    .local v23, "displayProgress":Z
    const-string v5, "BooksSync"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 184
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v5, "Starting books sync"

    move-object/from16 v0, v30

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    .local v30, "sb":Ljava/lang/StringBuilder;
    if-eqz v33, :cond_0

    .line 186
    const-string v5, ", v="

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    if-eqz v32, :cond_1

    .line 189
    const-string v5, ", u"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_1
    if-eqz v13, :cond_2

    .line 192
    const-string v5, ", d"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_2
    if-eqz v23, :cond_3

    .line 195
    const-string v5, ", p"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_3
    const-string v5, "BooksSync"

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    .end local v30    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const-string v5, "BooksSync"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 201
    const-string v5, "BooksSync"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onPerformSync() extras: volumeIds="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v34

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_5
    const/16 v29, 0x0

    .line 205
    .local v29, "okToCancel":Z
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/common/BooksContext;

    invoke-interface {v5}, Lcom/google/android/apps/books/common/BooksContext;->getSyncUi()Lcom/google/android/apps/books/service/SyncService$SyncUi;

    move-result-object v15

    .line 206
    .local v15, "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    if-nez v23, :cond_6

    invoke-interface {v15}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->shouldNotifyByDefault()Z

    move-result v5

    if-nez v5, :cond_6

    .line 207
    new-instance v15, Lcom/google/android/apps/books/service/SyncService$StubSyncUi;

    .end local v15    # "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    invoke-direct {v15}, Lcom/google/android/apps/books/service/SyncService$StubSyncUi;-><init>()V

    .line 210
    .restart local v15    # "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    :cond_6
    const-string v5, "BooksLazySync"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v27

    .line 212
    .local v27, "lazySync":Z
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v19

    .line 213
    .local v19, "broadcaster":Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    const-string v28, "sync"

    .line 214
    .local v28, "notificationBlock":Ljava/lang/String;
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->addNotificationBlock(Ljava/lang/Object;)V

    .line 216
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/service/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v10

    .line 220
    .local v10, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v22

    .line 221
    .local v22, "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    new-instance v4, Lcom/google/android/apps/books/service/BooksSafeFetchController;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mFetchingExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mPlanningExecutor:Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    move-object/from16 v5, p5

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/service/BooksSafeFetchController;-><init>(Landroid/content/SyncResult;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;Lcom/google/android/apps/books/service/Drainer;)V

    .line 225
    .local v4, "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    sget-object v36, Lcom/google/android/apps/books/service/SyncService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v36
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/android/apps/books/data/OutOfSpaceException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 227
    :try_start_1
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/model/BooksDataStore;->ensureMyEbooksCollection()V

    .line 229
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->upSyncReadingPositions(Landroid/accounts/Account;)V

    .line 230
    const-wide/16 v6, 0x7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v10, v1, v6, v7}, Lcom/google/android/apps/books/service/SyncAdapter;->upSyncCollectionVolumes(Lcom/google/android/apps/books/data/BooksDataController;Landroid/accounts/Account;J)V

    .line 231
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->upSyncAnnotations(Landroid/accounts/Account;)V

    .line 233
    if-eqz v33, :cond_e

    .line 234
    if-nez v27, :cond_c

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, v33

    move-object v8, v4

    move-object v9, v15

    .line 235
    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/books/service/SyncAdapter;->downloadVolumes(Landroid/accounts/Account;[Ljava/lang/String;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/data/BooksDataController;)Ljava/util/List;

    move-result-object v35

    .line 237
    .local v35, "vvs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/service/SyncAdapter;->syncAnnotations(Landroid/accounts/Account;Ljava/util/List;)V

    .line 241
    .end local v35    # "vvs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    :goto_1
    const/16 v29, 0x1

    .line 257
    :goto_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/books/data/BooksDataController;->JUST_FETCH:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v10, v5, v6, v7, v8}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 258
    sget-object v5, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 259
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->maybeSyncDictionaryMetadata(Lcom/google/android/apps/books/data/BooksDataController;Landroid/accounts/Account;)V

    .line 260
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 262
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v20

    .line 264
    .local v20, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    move-object/from16 v0, v20

    invoke-interface {v10, v0}, Lcom/google/android/apps/books/data/BooksDataController;->syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V

    .line 265
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    .line 269
    .end local v20    # "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :cond_7
    const-string v5, "BooksSync"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 270
    const-string v5, "BooksSync"

    const-string v6, "marking executor for shutdown when finished..."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;

    .line 274
    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/sync/SyncController;->updateSyncSettings(Lcom/google/android/apps/books/util/Config;)V

    .line 275
    monitor-exit v36
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 320
    .end local v4    # "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .end local v22    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_9
    :goto_3
    invoke-interface/range {v31 .. v31}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 322
    const-string v5, "BooksSync"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 323
    const-string v5, "BooksSync"

    const-string v6, "Finished books sync"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_a
    return-void

    .line 177
    .end local v10    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v13    # "download":Z
    .end local v15    # "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .end local v19    # "broadcaster":Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .end local v23    # "displayProgress":Z
    .end local v27    # "lazySync":Z
    .end local v28    # "notificationBlock":Ljava/lang/String;
    .end local v29    # "okToCancel":Z
    .end local v32    # "uploadOnly":Z
    .end local v33    # "volumeIds":[Ljava/lang/String;
    :cond_b
    const/16 v33, 0x0

    .restart local v33    # "volumeIds":[Ljava/lang/String;
    goto/16 :goto_0

    .line 239
    .restart local v4    # "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .restart local v10    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .restart local v13    # "download":Z
    .restart local v15    # "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .restart local v19    # "broadcaster":Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .restart local v22    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    .restart local v23    # "displayProgress":Z
    .restart local v27    # "lazySync":Z
    .restart local v28    # "notificationBlock":Ljava/lang/String;
    .restart local v29    # "okToCancel":Z
    .restart local v32    # "uploadOnly":Z
    :cond_c
    :try_start_3
    const-string v5, "BooksSync"

    const-string v6, "======== Skipping downloadVolumes() due to lazy flag"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 275
    :catchall_0
    move-exception v5

    monitor-exit v36
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/google/android/apps/books/data/OutOfSpaceException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 277
    .end local v4    # "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .end local v22    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :catch_0
    move-exception v24

    .line 279
    .local v24, "e":Ljava/lang/InterruptedException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->abortAndDrain()Ljava/util/List;

    move-result-object v25

    .line 280
    .local v25, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->findFirstExternalStorageException(Ljava/lang/Iterable;)Ljava/io/FileNotFoundException;

    move-result-object v26

    .line 282
    .local v26, "hardException":Ljava/io/FileNotFoundException;
    if-eqz v26, :cond_d

    .line 283
    move-object/from16 v0, p5

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V

    .line 286
    :cond_d
    if-eqz v29, :cond_10

    .line 290
    const-string v5, "BooksSync"

    const-string v6, "SyncManager interrupted sync, will pick up later"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 308
    :goto_4
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 313
    :catch_1
    move-exception v24

    .line 314
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 242
    .end local v24    # "e":Ljava/lang/InterruptedException;
    .end local v25    # "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    .end local v26    # "hardException":Ljava/io/FileNotFoundException;
    .restart local v4    # "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .restart local v22    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_e
    if-eqz v32, :cond_f

    .line 243
    const/16 v29, 0x1

    goto/16 :goto_2

    .line 247
    :cond_f
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->syncMyEbooks(Lcom/google/android/apps/books/data/BooksDataController;Landroid/content/SyncResult;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    move-result-object v18

    .line 249
    .local v18, "myEbooksVolumesResults":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    const/16 v29, 0x1

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object v14, v4

    move-object/from16 v17, v10

    .line 250
    invoke-direct/range {v11 .. v18}, Lcom/google/android/apps/books/service/SyncAdapter;->syncCoversAndContent(Landroid/accounts/Account;ZLcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SyncService$SyncUi;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    .line 252
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/service/SyncAdapter;->syncMyEbooksAnnotations(Landroid/accounts/Account;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 313
    .end local v18    # "myEbooksVolumesResults":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    :catch_2
    move-exception v24

    .line 314
    .restart local v24    # "e":Ljava/lang/InterruptedException;
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 294
    .end local v4    # "ensurer":Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .end local v22    # "dataStore":Lcom/google/android/apps/books/model/BooksDataStore;
    .restart local v25    # "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    .restart local v26    # "hardException":Ljava/io/FileNotFoundException;
    :cond_10
    :try_start_8
    const-string v5, "BooksSync"

    const-string v6, "SyncManager interrupted sync prematurely"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_4

    .line 308
    .end local v24    # "e":Ljava/lang/InterruptedException;
    .end local v25    # "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    .end local v26    # "hardException":Ljava/io/FileNotFoundException;
    :catchall_1
    move-exception v5

    const-string v6, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v6}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_b

    .line 317
    :cond_11
    :goto_5
    throw v5

    .line 296
    :catch_3
    move-exception v24

    .line 297
    .local v24, "e":Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;
    :try_start_a
    move-object/from16 v0, p5

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 308
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_3

    .line 313
    :catch_4
    move-exception v24

    .line 314
    .local v24, "e":Ljava/lang/InterruptedException;
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 298
    .end local v24    # "e":Ljava/lang/InterruptedException;
    :catch_5
    move-exception v24

    .line 299
    .local v24, "e":Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;
    :try_start_c
    move-object/from16 v0, p5

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 308
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_3

    .line 313
    :catch_6
    move-exception v24

    .line 314
    .local v24, "e":Ljava/lang/InterruptedException;
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 300
    .end local v24    # "e":Ljava/lang/InterruptedException;
    :catch_7
    move-exception v24

    .line 302
    .local v24, "e":Lcom/google/android/apps/books/data/OutOfSpaceException;
    :try_start_e
    move-object/from16 v0, p5

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/apps/books/service/SyncAdapter;->registerHardError(Landroid/content/SyncResult;Ljava/lang/Exception;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 308
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_3

    .line 313
    :catch_8
    move-exception v24

    .line 314
    .local v24, "e":Ljava/lang/InterruptedException;
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 303
    .end local v24    # "e":Ljava/lang/InterruptedException;
    :catch_9
    move-exception v24

    .line 304
    .local v24, "e":Ljava/lang/Exception;
    :try_start_10
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 305
    const-string v5, "BooksSync"

    const-string v6, "Sync error"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 308
    :cond_12
    const-string v5, "sync"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 312
    :try_start_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/service/SyncAdapter;->mDrainer:Lcom/google/android/apps/books/service/Drainer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/service/Drainer;->drain()Ljava/util/List;
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_a

    goto/16 :goto_3

    .line 313
    :catch_a
    move-exception v24

    .line 314
    .local v24, "e":Ljava/lang/InterruptedException;
    const-string v5, "BooksSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 315
    const-string v5, "BooksSync"

    const-string v6, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 313
    .end local v24    # "e":Ljava/lang/InterruptedException;
    :catch_b
    move-exception v24

    .line 314
    .restart local v24    # "e":Ljava/lang/InterruptedException;
    const-string v6, "BooksSync"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 315
    const-string v6, "BooksSync"

    const-string v7, "Interrupted drain during sync recovery"

    move-object/from16 v0, v24

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5
.end method
