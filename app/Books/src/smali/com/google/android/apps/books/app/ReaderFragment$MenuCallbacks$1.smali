.class Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->onItemSelected(Lcom/google/android/apps/books/app/ReaderMenu$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/BookmarkController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;)V
    .locals 0

    .prologue
    .line 4144
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 3
    .param p1, "bc"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 4147
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4165
    :goto_0
    return-void

    .line 4151
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v1, v2, :cond_2

    .line 4152
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4155
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/app/BookmarkController;->toggleBookmarks(Ljava/util/List;Z)V

    goto :goto_0

    .line 4152
    .end local v0    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterPageRenderings()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 4157
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;Lcom/google/android/apps/books/app/BookmarkController;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/UnloadableEventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 4144
    check-cast p1, Lcom/google/android/apps/books/app/BookmarkController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->take(Lcom/google/android/apps/books/app/BookmarkController;)V

    return-void
.end method
