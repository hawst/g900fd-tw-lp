.class Lcom/google/android/apps/books/app/HomeFragment$19;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->setVolumes(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$myEbooksVolumesResults:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 0

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$19;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$19;->val$myEbooksVolumesResults:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 2
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 1483
    const-string v0, "HomeFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1484
    const-string v0, "HomeFragment"

    const-string v1, "takeLocalVolumeData and VolumeDownloadProgress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1486
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$19;->val$myEbooksVolumesResults:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    iget-object v0, v0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->localVolumeData:Ljava/util/Map;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->loadedLocalVolumeData(Ljava/util/Map;)V

    .line 1487
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$19;->val$myEbooksVolumesResults:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    iget-object v0, v0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->downloadProgress:Ljava/util/Map;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->loadedVolumeDownloadProgress(Ljava/util/Map;)V

    .line 1488
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1480
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$19;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
