.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnnotationAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_HIGHLIGHT_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_HIGHLIGHT_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_HIGHLIGHT_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_HIGHLIGHT_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum ADD_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum CHANGE_HIGHLIGHT_TO_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum CHANGE_HIGHLIGHT_TO_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum CHANGE_HIGHLIGHT_TO_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum CHANGE_HIGHLIGHT_TO_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum DELETE_HIGHLIGHT_NO_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum DELETE_HIGHLIGHT_WITH_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum EDIT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum REMOVE_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

.field public static final enum REMOVE_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 233
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_BOOKMARK_FROM_MENU"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 234
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "REMOVE_BOOKMARK_FROM_MENU"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 235
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_BOOKMARK_FROM_PAGE_TAP"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 236
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "REMOVE_BOOKMARK_FROM_PAGE_TAP"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 241
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_HIGHLIGHT_COLOR_1"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 242
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_HIGHLIGHT_COLOR_2"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 243
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_HIGHLIGHT_COLOR_3"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 244
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_HIGHLIGHT_COLOR_4"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 249
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "CHANGE_HIGHLIGHT_TO_COLOR_1"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 250
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "CHANGE_HIGHLIGHT_TO_COLOR_2"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 251
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "CHANGE_HIGHLIGHT_TO_COLOR_3"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 252
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "CHANGE_HIGHLIGHT_TO_COLOR_4"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 257
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "DELETE_HIGHLIGHT_NO_NOTE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_NO_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 258
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "DELETE_HIGHLIGHT_WITH_NOTE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_WITH_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 263
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "ADD_NOTE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 264
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    const-string v1, "EDIT_NOTE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->EDIT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 229
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_NO_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_WITH_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->EDIT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 229
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 229
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    return-object v0
.end method
