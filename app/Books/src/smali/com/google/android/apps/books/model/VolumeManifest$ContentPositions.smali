.class public Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
.super Ljava/lang/Object;
.source "VolumeManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContentPositions"
.end annotation


# instance fields
.field private final mContentEnd:Ljava/lang/String;

.field private final mContentStart:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "contentStart"    # Ljava/lang/String;
    .param p2, "contentEnd"    # Ljava/lang/String;

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    iput-object p1, p0, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->mContentStart:Ljava/lang/String;

    .line 243
    iput-object p2, p0, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->mContentEnd:Ljava/lang/String;

    .line 244
    return-void
.end method


# virtual methods
.method public getContentEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->mContentEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getContentStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->mContentStart:Ljava/lang/String;

    return-object v0
.end method
