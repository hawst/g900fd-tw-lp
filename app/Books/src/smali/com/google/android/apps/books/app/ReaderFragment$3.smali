.class Lcom/google/android/apps/books/app/ReaderFragment$3;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 692
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getClipboardCopyToastString(Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 739
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 740
    .local v3, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/ClipboardCopyController;->getRemainingCharacterCount()I

    move-result v0

    .line 742
    .local v0, "newRemainingCharacterCount":I
    if-gtz v0, :cond_0

    .line 743
    const v5, 0x7f0f01c0

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 751
    :goto_0
    return-object v5

    .line 745
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/ClipboardCopyController;->getAllowedCharacterCount()I

    move-result v5

    sub-int v4, v5, v0

    .line 748
    .local v4, "usedCharacterCount":I
    mul-int/lit8 v5, v4, 0x64

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/ClipboardCopyController;->getAllowedCharacterCount()I

    move-result v6

    div-int/2addr v5, v6

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 750
    .local v2, "percentUsed":I
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v1

    .line 751
    .local v1, "percentFormatter":Ljava/text/NumberFormat;
    const v5, 0x7f0f01bf

    new-array v6, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    int-to-double v8, v2

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v1, v8, v9}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private getQuoteShareUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const-string v1, "books_inapp_quotesharing"

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getShareUrl(Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public attachSelectionPopup(Landroid/view/View;)V
    .locals 2
    .param p1, "popup"    # Landroid/view/View;

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0e0118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 810
    return-void
.end method

.method public dismissActiveSelection()V
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->dismissActiveSelection()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 707
    return-void
.end method

.method public dismissInfoCards()V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 769
    return-void
.end method

.method public getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotationController()Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 692
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$3;->getContext()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getSelectionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$300(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public getShareTextForQuote(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 8
    .param p1, "quote"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 779
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$3;->getQuoteShareUrl()Ljava/lang/String;

    move-result-object v0

    .line 780
    .local v0, "url":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0109

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAuthor()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1500(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 784
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f010a

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p1, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAuthor()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1500(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public isSample()Z
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    return v0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isVertical()Z

    move-result v0

    return v0
.end method

.method public lockSelection()V
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->lockSelection()V

    .line 714
    :cond_0
    return-void
.end method

.method public onSearchInBookRequested(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "searchQuery"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x1

    .line 791
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$3;->dismissActiveSelection()V

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onSearchRequested()Z

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->onSearchFieldFocusChanged(Z)V

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/app/ReaderMenu;->setSearchQuery(Ljava/lang/CharSequence;Z)V

    .line 795
    return-void
.end method

.method public onSelectedTextCopiedToClipboard()V
    .locals 3

    .prologue
    .line 759
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ClipboardCopyController;->isCopyingLimited()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 760
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$3;->getContext()Landroid/app/Activity;

    move-result-object v0

    .line 761
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$3;->getClipboardCopyToastString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 764
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    return-void
.end method

.method public setSystemUiVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 736
    return-void
.end method

.method public translateText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTranslateViewController()Lcom/google/android/apps/books/widget/TranslateViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TranslateViewController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->translateText(Ljava/lang/CharSequence;)V

    .line 719
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$3;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 720
    return-void
.end method
