.class Lcom/google/android/apps/books/app/OnboardingController$4;
.super Ljava/lang/Object;
.source "OnboardingController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OnboardingController;->addBookToMyLibrary(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OnboardingController;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OnboardingController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/apps/books/app/OnboardingController$4;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OnboardingController$4;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/OnboardingController$4;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    # getter for: Lcom/google/android/apps/books/app/OnboardingController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {v1}, Lcom/google/android/apps/books/app/OnboardingController;->access$000(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/OnboardingController$4;->val$volumeId:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;->ONBOARDING:Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "OnboardingController"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    const-string v1, "OnboardingController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBookToMyLibrary("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/OnboardingController$4;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
