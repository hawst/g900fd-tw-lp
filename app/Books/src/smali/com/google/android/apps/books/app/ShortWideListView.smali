.class public Lcom/google/android/apps/books/app/ShortWideListView;
.super Landroid/widget/LinearLayout;
.source "ShortWideListView.java"


# instance fields
.field private mAdapter:Landroid/widget/Adapter;

.field private mPreviousCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ShortWideListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ShortWideListView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ShortWideListView;->update()V

    return-void
.end method

.method private update()V
    .locals 4

    .prologue
    .line 68
    iget-object v2, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    .line 69
    .local v1, "newCount":I
    iget v2, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    if-ge v1, v2, :cond_0

    .line 70
    const-string v2, "ShortWideListView"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    const-string v2, "ShortWideListView"

    const-string v3, "Tried to shrink ShortWideListView"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mAdapter:Landroid/widget/Adapter;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ShortWideListView;->addView(Landroid/view/View;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_1
    iput v1, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mPreviousCount:I

    .line 80
    return-void
.end method


# virtual methods
.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/books/app/ShortWideListView;->mAdapter:Landroid/widget/Adapter;

    .line 56
    new-instance v0, Lcom/google/android/apps/books/app/ShortWideListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ShortWideListView$1;-><init>(Lcom/google/android/apps/books/app/ShortWideListView;)V

    invoke-interface {p1, v0}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ShortWideListView;->update()V

    .line 63
    return-void
.end method
