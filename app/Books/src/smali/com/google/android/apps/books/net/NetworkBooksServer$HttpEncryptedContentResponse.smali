.class Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;
.super Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;
.source "NetworkBooksServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/NetworkBooksServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HttpEncryptedContentResponse"
.end annotation


# instance fields
.field private final mEntity:Lorg/apache/http/HttpEntity;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .param p1, "content"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 133
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 134
    return-void
.end method

.method synthetic constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/io/InputStream;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Lcom/google/android/apps/books/net/NetworkBooksServer$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method private constructor <init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 129
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Lcom/google/android/apps/books/net/NetworkBooksServer$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method private constructor <init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .param p2, "content"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/io/InputStream;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 122
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 123
    iput-object p1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;->mEntity:Lorg/apache/http/HttpEntity;

    .line 124
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .param p2, "x1"    # Ljava/io/InputStream;
    .param p3, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p4, "x3"    # Lcom/google/android/apps/books/net/NetworkBooksServer$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method private constructor <init>([B)V
    .locals 2
    .param p1, "content"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 143
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 144
    return-void
.end method

.method synthetic constructor <init>([BLcom/google/android/apps/books/net/NetworkBooksServer$1;)V
    .locals 0
    .param p1, "x0"    # [B
    .param p2, "x1"    # Lcom/google/android/apps/books/net/NetworkBooksServer$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>([B)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;->close()V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;->mEntity:Lorg/apache/http/HttpEntity;

    invoke-static {v0}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    .line 150
    return-void
.end method
