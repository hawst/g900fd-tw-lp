.class Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;
.super Ljava/lang/Object;
.source "KeyFramePageAnimator.java"


# static fields
.field private static final mBezierPages:[Lcom/google/android/apps/books/view/pages/BezierPage;

.field private static mInterval:F

.field private static mIntervals:I

.field private static sShadowTexBuffer:Ljava/nio/FloatBuffer;

.field private static final sShadowTexCoord:[F

.field private static final sTmpP0:Landroid/graphics/PointF;

.field private static final sTmpP1:Landroid/graphics/PointF;

.field private static final sTmpPoint:[Lcom/google/android/opengl/common/Float3;

.field private static final sTmpVertexData:[F


# instance fields
.field private final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field private mShadowTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mInterval:F

    .line 24
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/android/apps/books/view/pages/BezierPage;

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mBezierPages:[Lcom/google/android/apps/books/view/pages/BezierPage;

    .line 29
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sShadowTexCoord:[F

    .line 39
    const/16 v0, 0xc

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpVertexData:[F

    .line 40
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/opengl/common/Float3;->getArray(I)[Lcom/google/android/opengl/common/Float3;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    .line 41
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP0:Landroid/graphics/PointF;

    .line 42
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    return-void

    .line 29
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V
    .locals 2
    .param p1, "scene"    # Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 46
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sShadowTexBuffer:Ljava/nio/FloatBuffer;

    .line 48
    sget-object v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sShadowTexBuffer:Ljava/nio/FloatBuffer;

    sget-object v1, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sShadowTexCoord:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mShadowTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 51
    return-void
.end method

.method private getShadowTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mShadowTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mShadow:I

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    const-string v3, "Shadow"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILandroid/graphics/Matrix;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mShadowTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mShadowTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    return-object v0
.end method

.method private shadowPos(FFFLcom/google/android/opengl/common/Float3;ZZ)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "t"    # F
    .param p4, "result"    # Lcom/google/android/opengl/common/Float3;
    .param p5, "reverseShadow"    # Z
    .param p6, "spineOnRight"    # Z

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    .line 149
    if-eqz p6, :cond_2

    move v0, v1

    .line 157
    .local v0, "spineX":F
    :goto_0
    const v3, 0x3f7d70a4    # 0.99f

    if-eqz p5, :cond_0

    sub-float p3, v1, p3

    .end local p3    # "t":F
    :cond_0
    invoke-virtual {p0, v3, p2, p3, p4}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 158
    if-eqz p5, :cond_1

    .line 159
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v1, v2

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    .line 162
    :cond_1
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    const v3, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, p1

    add-float/2addr v1, v3

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    .line 163
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->y:F

    const/4 v3, 0x0

    add-float/2addr v1, v3

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->y:F

    .line 164
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->z:F

    const v3, -0x457ced91    # -0.001f

    add-float/2addr v1, v3

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->z:F

    .line 167
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v1, v4

    add-float/2addr v1, v0

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->x:F

    .line 168
    iget v1, p4, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v1, v4

    add-float/2addr v1, v2

    iput v1, p4, Lcom/google/android/opengl/common/Float3;->y:F

    .line 169
    return-void

    .end local v0    # "spineX":F
    .restart local p3    # "t":F
    :cond_2
    move v0, v2

    .line 149
    goto :goto_0
.end method


# virtual methods
.method public loadCurves([I)V
    .locals 10
    .param p1, "bezierCurveData"    # [I

    .prologue
    .line 116
    const/4 v2, 0x0

    .line 117
    .local v2, "currentPointIndex":I
    const/4 v6, 0x0

    .line 119
    .local v6, "numPages":I
    :goto_0
    array-length v8, p1

    if-ge v2, v8, :cond_2

    .line 121
    new-instance v1, Lcom/google/android/apps/books/view/pages/BezierPage;

    invoke-direct {v1}, Lcom/google/android/apps/books/view/pages/BezierPage;-><init>()V

    .line 122
    .local v1, "bezierPage":Lcom/google/android/apps/books/view/pages/BezierPage;
    sget-object v8, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mBezierPages:[Lcom/google/android/apps/books/view/pages/BezierPage;

    aput-object v1, v8, v6

    .line 123
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/4 v8, 0x3

    if-ge v4, v8, :cond_1

    .line 124
    new-instance v0, Lcom/google/android/apps/books/view/pages/Bezier;

    invoke-direct {v0}, Lcom/google/android/apps/books/view/pages/Bezier;-><init>()V

    .line 125
    .local v0, "bezier":Lcom/google/android/apps/books/view/pages/Bezier;
    const/4 v5, 0x0

    .local v5, "j":I
    move v3, v2

    .end local v2    # "currentPointIndex":I
    .local v3, "currentPointIndex":I
    :goto_2
    const/16 v8, 0x8

    if-ge v5, v8, :cond_0

    .line 126
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "currentPointIndex":I
    .restart local v2    # "currentPointIndex":I
    aget v7, p1, v3

    .line 127
    .local v7, "point":I
    invoke-virtual {v0, v7}, Lcom/google/android/apps/books/view/pages/Bezier;->addValue(I)V

    .line 125
    add-int/lit8 v5, v5, 0x1

    move v3, v2

    .end local v2    # "currentPointIndex":I
    .restart local v3    # "currentPointIndex":I
    goto :goto_2

    .line 129
    .end local v7    # "point":I
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/view/pages/BezierPage;->addBezier(Lcom/google/android/apps/books/view/pages/Bezier;)V

    .line 123
    add-int/lit8 v4, v4, 0x1

    move v2, v3

    .end local v3    # "currentPointIndex":I
    .restart local v2    # "currentPointIndex":I
    goto :goto_1

    .line 132
    .end local v0    # "bezier":Lcom/google/android/apps/books/view/pages/Bezier;
    .end local v5    # "j":I
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/BezierPage;->sampleDistances()V

    .line 136
    add-int/lit8 v6, v6, 0x1

    .line 137
    goto :goto_0

    .line 138
    .end local v1    # "bezierPage":Lcom/google/android/apps/books/view/pages/BezierPage;
    .end local v4    # "i":I
    :cond_2
    add-int/lit8 v8, v6, -0x1

    sput v8, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    .line 139
    const/high16 v8, 0x3f800000    # 1.0f

    sget v9, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    sput v8, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mInterval:F

    .line 140
    return-void
.end method

.method public needsCurves()Z
    .locals 1

    .prologue
    .line 104
    sget v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public normal(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "t"    # F
    .param p4, "result"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x3ca3d70a    # 0.02f

    .line 58
    sget-object v1, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v1, v1, v4

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 59
    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    add-float v1, p1, v3

    :goto_0
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v2, v2, v5

    invoke-virtual {p0, v1, p2, p3, v2}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 60
    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    add-float v1, p2, v3

    :goto_1
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v2, v2, v6

    invoke-virtual {p0, p1, v1, p3, v2}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 61
    sget-object v1, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v1, v1, v5

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 62
    sget-object v1, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v1, v1, v6

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 63
    sget-object v1, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v1, v1, v5

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2, p4}, Lcom/google/android/opengl/common/Float3;->cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 64
    invoke-virtual {p4}, Lcom/google/android/opengl/common/Float3;->getLength()F

    move-result v0

    .line 65
    .local v0, "norm":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 66
    invoke-virtual {p4}, Lcom/google/android/opengl/common/Float3;->normalize()V

    .line 70
    :goto_2
    return-object p4

    .line 59
    .end local v0    # "norm":F
    :cond_0
    sub-float v1, p1, v3

    goto :goto_0

    .line 60
    :cond_1
    sub-float v1, p2, v3

    goto :goto_1

    .line 68
    .restart local v0    # "norm":F
    :cond_2
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p4, v7, v7, v1}, Lcom/google/android/opengl/common/Float3;->set(FFF)V

    goto :goto_2
.end method

.method public pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "t"    # F
    .param p4, "result"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 79
    sget v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x0

    sget v4, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    int-to-float v4, v4

    mul-float/2addr v4, p3

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 80
    .local v1, "i":I
    const/4 v2, 0x0

    int-to-float v3, v1

    sget v4, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mInterval:F

    mul-float/2addr v3, v4

    sub-float v3, p3, v3

    sget v4, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mIntervals:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 84
    .local v0, "deltaT":F
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mBezierPages:[Lcom/google/android/apps/books/view/pages/BezierPage;

    add-int/lit8 v3, v1, 0x0

    aget-object v2, v2, v3

    sget-object v3, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP0:Landroid/graphics/PointF;

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/books/view/pages/BezierPage;->pointAt(FLandroid/graphics/PointF;)V

    .line 85
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mBezierPages:[Lcom/google/android/apps/books/view/pages/BezierPage;

    add-int/lit8 v3, v1, 0x1

    aget-object v2, v2, v3

    sget-object v3, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/books/view/pages/BezierPage;->pointAt(FLandroid/graphics/PointF;)V

    .line 88
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    sget-object v3, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP0:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float v4, v5, v0

    mul-float/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 89
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    sget-object v3, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP0:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v4, v5, v0

    mul-float/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 91
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iput v2, p4, Lcom/google/android/opengl/common/Float3;->x:F

    .line 92
    iput p2, p4, Lcom/google/android/opengl/common/Float3;->y:F

    .line 94
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpP1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    neg-float v2, v2

    iput v2, p4, Lcom/google/android/opengl/common/Float3;->z:F

    .line 96
    return-object p4
.end method

.method public shadowDraw(Lcom/google/android/apps/books/view/pages/Page;Landroid/graphics/Point;F)V
    .locals 26
    .param p1, "topPage"    # Lcom/google/android/apps/books/view/pages/Page;
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "t"    # F

    .prologue
    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v0, v2, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    move-object/from16 v24, v0

    .line 177
    .local v24, "renderer":Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingPageFromLeft()Z

    move-result v7

    .line 178
    .local v7, "shadowIsOnLeft":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v8

    .line 179
    .local v8, "spineOnRight":Z
    if-eqz v7, :cond_1

    const/high16 v25, -0x40800000    # -1.0f

    .line 180
    .local v25, "xCoordAwayFromPage":F
    :goto_0
    if-eqz v7, :cond_0

    .line 181
    const/16 v2, 0x404

    invoke-static {v2}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 184
    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v5, 0x0

    aget-object v6, v2, v5

    move-object/from16 v2, p0

    move/from16 v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->shadowPos(FFFLcom/google/android/opengl/common/Float3;ZZ)V

    .line 185
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v5, 0x1

    aget-object v6, v2, v5

    move-object/from16 v2, p0

    move/from16 v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->shadowPos(FFFLcom/google/android/opengl/common/Float3;ZZ)V

    .line 186
    const/4 v4, 0x0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x2

    aget-object v6, v2, v3

    move-object/from16 v2, p0

    move/from16 v3, v25

    move/from16 v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->shadowPos(FFFLcom/google/android/opengl/common/Float3;ZZ)V

    .line 187
    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x3

    aget-object v6, v2, v3

    move-object/from16 v2, p0

    move/from16 v3, v25

    move/from16 v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->shadowPos(FFFLcom/google/android/opengl/common/Float3;ZZ)V

    .line 189
    sget-object v9, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpVertexData:[F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v10, v2, Lcom/google/android/opengl/common/Float3;->x:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v11, v2, Lcom/google/android/opengl/common/Float3;->y:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v12, v2, Lcom/google/android/opengl/common/Float3;->z:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v13, v2, Lcom/google/android/opengl/common/Float3;->x:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v14, v2, Lcom/google/android/opengl/common/Float3;->y:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v15, v2, Lcom/google/android/opengl/common/Float3;->z:F

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->x:F

    move/from16 v16, v0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->y:F

    move/from16 v17, v0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->z:F

    move/from16 v18, v0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->x:F

    move/from16 v19, v0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->y:F

    move/from16 v20, v0

    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpPoint:[Lcom/google/android/opengl/common/Float3;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget v0, v2, Lcom/google/android/opengl/common/Float3;->z:F

    move/from16 v21, v0

    invoke-static/range {v9 .. v21}, Lcom/google/android/opengl/carousel/GL2Helper;->setVector12f([FFFFFFFFFFFFF)V

    .line 196
    const v22, 0x3e4ccccd    # 0.2f

    .line 197
    .local v22, "easeInMargin":F
    const/high16 v2, 0x3f400000    # 0.75f

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v3, p3

    move/from16 v0, p3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const v4, 0x3e4ccccd    # 0.2f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd    # 0.2f

    div-float v23, v2, v3

    .line 198
    .local v23, "opacity":F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->getShadowTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpOneTexture(Lcom/google/android/apps/books/view/pages/TexturePlacement;FZ[F)V

    .line 201
    invoke-virtual/range {p1 .. p2}, Lcom/google/android/apps/books/view/pages/Page;->setupPageStretch(Landroid/graphics/Point;)V

    .line 203
    const/16 v2, 0xbe2

    invoke-static {v2}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 205
    const-string v2, "aTextureCoord"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getAttribLocation(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x2

    const/16 v11, 0x1406

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sShadowTexBuffer:Ljava/nio/FloatBuffer;

    invoke-static/range {v9 .. v14}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 207
    sget-object v2, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->sTmpVertexData:[F

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuad([FZ)Z

    .line 209
    const/16 v2, 0xbe2

    invoke-static {v2}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 210
    const/16 v2, 0x405

    invoke-static {v2}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 211
    return-void

    .line 179
    .end local v22    # "easeInMargin":F
    .end local v23    # "opacity":F
    .end local v25    # "xCoordAwayFromPage":F
    :cond_1
    const/high16 v25, 0x3f800000    # 1.0f

    goto/16 :goto_0
.end method
