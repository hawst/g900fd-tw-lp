.class Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
.super Ljava/lang/Object;
.source "PagesView2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesView2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PageDrawingData"
.end annotation


# instance fields
.field public final bitmap:Landroid/graphics/Bitmap;

.field public final bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field public final fitSize:Landroid/graphics/Point;

.field public final pageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Point;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "fitSize"    # Landroid/graphics/Point;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p4, "pageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bitmap:Landroid/graphics/Bitmap;

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->fitSize:Landroid/graphics/Point;

    .line 88
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->pageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 90
    return-void
.end method
