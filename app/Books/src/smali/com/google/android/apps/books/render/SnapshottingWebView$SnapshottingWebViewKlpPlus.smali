.class Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;
.super Lcom/google/android/apps/books/render/SnapshottingWebView;
.source "SnapshottingWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/SnapshottingWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnapshottingWebViewKlpPlus"
.end annotation


# instance fields
.field private mAcceptSizeChange:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "drawDelegate"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/render/SnapshottingWebView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;->mAcceptSizeChange:Z

    .line 189
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/SnapshottingWebView$1;

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V

    return-void
.end method


# virtual methods
.method public onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;->mAcceptSizeChange:Z

    if-eqz v0, :cond_0

    .line 194
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->onSizeChanged(IIII)V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;->mAcceptSizeChange:Z

    .line 197
    :cond_0
    return-void
.end method
