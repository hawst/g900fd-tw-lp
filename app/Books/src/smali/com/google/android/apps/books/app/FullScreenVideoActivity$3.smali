.class Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;
.super Landroid/widget/MediaController;
.source "FullScreenVideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/FullScreenVideoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/FullScreenVideoActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/FullScreenVideoActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;->this$0:Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    invoke-direct {p0, p2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 131
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 132
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 133
    .local v0, "action":I
    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 144
    .end local v0    # "action":I
    :goto_0
    return v1

    .line 136
    .restart local v0    # "action":I
    :cond_0
    if-ne v0, v1, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 138
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;->this$0:Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->onBackPressed()V

    goto :goto_0

    .line 144
    .end local v0    # "action":I
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/MediaController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/widget/MediaController;->onWindowVisibilityChanged(I)V

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;->this$0:Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->access$200(Lcom/google/android/apps/books/app/FullScreenVideoActivity;Z)V

    .line 125
    return-void

    .line 124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
