.class public Lcom/google/android/apps/books/app/BookmarkController;
.super Ljava/lang/Object;
.source "BookmarkController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    }
.end annotation


# instance fields
.field private final mAnnotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

.field final mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

.field private final mBookmarkListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;",
            ">;"
        }
    .end annotation
.end field

.field private mBookmarks:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field

.field private final mEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/UserChangesEditor;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;Ljava/util/Comparator;)V
    .locals 2
    .param p1, "editor"    # Lcom/google/android/apps/books/annotations/UserChangesEditor;
    .param p2, "search"    # Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/UserChangesEditor;",
            "Lcom/google/android/apps/books/annotations/AnnotationContextSearch;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarkListeners:Ljava/util/Set;

    .line 56
    new-instance v0, Lcom/google/android/apps/books/app/BookmarkController$1;

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/BookmarkController$1;-><init>(Lcom/google/android/apps/books/app/BookmarkController;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/books/app/BookmarkController;->mEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/books/app/BookmarkController;->mAnnotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/books/app/BookmarkController;->mComparator:Ljava/util/Comparator;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/SortedMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/app/BookmarkController;Ljava/util/SortedMap;)Ljava/util/SortedMap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BookmarkController;
    .param p1, "x1"    # Ljava/util/SortedMap;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/Comparator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/BookmarkController;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarkListeners:Ljava/util/Set;

    return-object v0
.end method

.method private addBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 5
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->suggestedLocationForBookmark()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    .line 143
    .local v1, "location":Lcom/google/android/apps/books/annotations/TextLocation;
    if-nez v1, :cond_1

    .line 144
    const-string v3, "BkmkController"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    const-string v3, "BkmkController"

    const-string v4, "No bookmarkable locations found on current page."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 152
    .local v0, "annotationRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/google/android/apps/books/annotations/Annotation;->bookmarkWithFreshId(Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    .line 154
    .local v2, "placeholder":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/BookmarkController;->addPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0
.end method

.method private addPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 3
    .param p1, "placeholder"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/BookmarkController;->mAnnotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    invoke-static {p1, v2}, Lcom/google/android/apps/books/annotations/Updateables;->forPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V

    .line 160
    return-void
.end method

.method private removeBookmarks(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 164
    .local v0, "bookmark":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BookmarkController;->mEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiRemove(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0

    .line 166
    .end local v0    # "bookmark":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    return-void
.end method


# virtual methods
.method public addBookmarkListener(Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarkListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;->onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V

    .line 94
    :cond_0
    return-void
.end method

.method public getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    return-object v0
.end method

.method public getViewableBookmarks(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/util/Collection;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    if-nez p1, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/SortedMap;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public pagesContainBookmark(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    invoke-static {p1, v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/List;Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v0

    .line 115
    .local v0, "viewable":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeListener(Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarkListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public toggleBookmarks(Ljava/util/List;Z)V
    .locals 3
    .param p2, "fromMenu"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    invoke-static {p1, v2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/List;Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v1

    .line 131
    .local v1, "viewable":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 132
    .local v0, "shouldBeAdding":Z
    if-eqz v0, :cond_1

    .line 134
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/BookmarkController;->addBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 138
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/BookmarkController;->mBookmarks:Ljava/util/SortedMap;

    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v2

    invoke-static {p2, v0, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookmarkAction(ZZI)V

    goto :goto_0

    .line 136
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/BookmarkController;->removeBookmarks(Ljava/util/List;)V

    goto :goto_1
.end method
