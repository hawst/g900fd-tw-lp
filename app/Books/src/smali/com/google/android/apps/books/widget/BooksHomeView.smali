.class public interface abstract Lcom/google/android/apps/books/widget/BooksHomeView;
.super Ljava/lang/Object;
.source "BooksHomeView.java"


# virtual methods
.method public abstract getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
.end method

.method public abstract getHelpContext()Ljava/lang/String;
.end method

.method public abstract getSnapshottableView()Landroid/view/View;
.end method

.method public abstract hide(Z)V
.end method

.method public abstract moveToHome()V
.end method

.method public abstract setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V
.end method

.method public abstract setCardsData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract show(Z)V
.end method

.method public abstract volumeDownloadFractionAnimationDone(Ljava/lang/String;)V
.end method
