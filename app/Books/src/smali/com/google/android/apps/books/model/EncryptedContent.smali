.class public interface abstract Lcom/google/android/apps/books/model/EncryptedContent;
.super Ljava/lang/Object;
.source "EncryptedContent.java"


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getContent()Ljava/io/InputStream;
.end method

.method public abstract getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
.end method
