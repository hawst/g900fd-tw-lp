.class public Lcom/google/android/apps/books/app/data/JsonSaleInfo;
.super Ljava/lang/Object;
.source "JsonSaleInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;,
        Lcom/google/android/apps/books/app/data/JsonSaleInfo$Duration;,
        Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;,
        Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;
    }
.end annotation


# static fields
.field public static final SALEABILITY_FOR_RENTAL_ONLY:Ljava/lang/String; = "FOR_RENTAL_ONLY"

.field public static final SALEABILITY_FOR_SALE_AND_RENTAL:Ljava/lang/String; = "FOR_SALE_AND_RENTAL"


# instance fields
.field public buyLink:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "buyLink"
    .end annotation
.end field

.field public offers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "offers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;",
            ">;"
        }
    .end annotation
.end field

.field public retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$Price;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "retailPrice"
    .end annotation
.end field

.field public saleability:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "saleability"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method
