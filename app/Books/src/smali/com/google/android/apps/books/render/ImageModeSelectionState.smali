.class Lcom/google/android/apps/books/render/ImageModeSelectionState;
.super Ljava/lang/Object;
.source "ImageModeSelectionState.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SelectionState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

.field private final mNumContextCharacters:I

.field private final mPageId:Ljava/lang/String;

.field private final mPageIndex:I

.field private mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

.field private final mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/PageStructureSelection;ILjava/lang/String;ILcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;)V
    .locals 1
    .param p1, "selection"    # Lcom/google/android/apps/books/render/PageStructureSelection;
    .param p2, "pageIndex"    # I
    .param p3, "pageId"    # Ljava/lang/String;
    .param p4, "numContextCharacters"    # I
    .param p5, "callbacks"    # Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    .line 33
    iput p2, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageIndex:I

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageId:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    .line 36
    iput p4, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mNumContextCharacters:I

    .line 37
    return-void
.end method

.method private getAfterText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    iget v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mNumContextCharacters:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->getAfterText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBeforeText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    iget v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mNumContextCharacters:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->getBeforeText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    iget v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageIndex:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureSelection;->getRange(ILjava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    return-object v0
.end method


# virtual methods
.method public createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 94
    new-instance v4, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getBeforeText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getSelectedText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getAfterText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v0, v1, v3}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .local v4, "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    const-string v1, "copy"

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    iget v6, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageIndex:I

    iget-object v7, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageId:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/books/render/PageStructureSelection;->getRange(ILjava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v3, v2

    move-object v6, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 10
    .param p1, "androidColor"    # I
    .param p2, "marginNoteText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 65
    new-instance v4, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getBeforeText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getSelectedText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getAfterText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v0, v1, v3}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .local v4, "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    const-string v1, "notes"

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v3, v2

    move v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public getContainingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "index"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageId:Ljava/lang/String;

    invoke-static {p1, v1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 105
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/annotations/PageStructureLocation;->NATURAL_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->firstContainingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    return-object v1
.end method

.method public getNormalizedSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->collapseWhitespace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOverlappingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "index"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 111
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageId:Ljava/lang/String;

    invoke-static {p1, v1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 113
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/annotations/PageStructureLocation;->NATURAL_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->firstOverlappingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    return-object v1
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, -0x1

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureSelection;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "loader"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public moveSelectionHandle(IZIIIZ)I
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "firstHandle"    # Z
    .param p3, "deltaX"    # I
    .param p4, "deltaY"    # I
    .param p5, "prevRequest"    # I
    .param p6, "isDoneMoving"    # Z

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-virtual {v0, p2, p3, p4, p6}, Lcom/google/android/apps/books/render/PageStructureSelection;->update(ZIIZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;->onSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mPageStructureLocationRange:Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .line 47
    :cond_0
    if-eqz p6, :cond_1

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;->onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeSelectionState;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureSelection;->getRequestId()I

    move-result v0

    return v0
.end method
