.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$CollectionVolumesQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CollectionVolumesQuery"
.end annotation


# static fields
.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1117
    sget-object v0, Lcom/google/android/apps/books/model/VolumesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "last_interaction"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CollectionVolumesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
