.class public Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;
.super Ljava/lang/Object;
.source "NetworkTaskQueue.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/NetworkTaskQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TaskComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;",
        ">;"
    }
.end annotation


# instance fields
.field private final mVolumeIdComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/data/NetworkTaskQueue;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/NetworkTaskQueue;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p2, "volumeIdToOperationSequenceNumber":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->this$0:Lcom/google/android/apps/books/data/NetworkTaskQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-instance v0, Lcom/google/android/apps/books/util/MapBasedComparator;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/books/util/MapBasedComparator;-><init>(Ljava/util/Map;Ljava/lang/Comparable;)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->mVolumeIdComparator:Ljava/util/Comparator;

    .line 125
    return-void
.end method

.method private compareVolumeIds(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "leftVolumeId"    # Ljava/lang/String;
    .param p2, "rightVolumeId"    # Ljava/lang/String;

    .prologue
    .line 157
    invoke-static {p1, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 158
    const/4 v1, 0x0

    .line 173
    :cond_0
    :goto_0
    return v1

    .line 161
    :cond_1
    if-nez p1, :cond_2

    .line 162
    const/4 v1, 0x1

    goto :goto_0

    .line 164
    :cond_2
    if-nez p2, :cond_3

    .line 165
    const/4 v1, -0x1

    goto :goto_0

    .line 167
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->mVolumeIdComparator:Ljava/util/Comparator;

    invoke-interface {v3, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    neg-int v1, v3

    .line 168
    .local v1, "result":I
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->this$0:Lcom/google/android/apps/books/data/NetworkTaskQueue;

    # getter for: Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v3}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->access$000(Lcom/google/android/apps/books/data/NetworkTaskQueue;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v3

    const-string v4, "NetworkTaskQueue"

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 169
    if-gez v1, :cond_4

    move-object v2, p1

    .line 170
    .local v2, "winningVolumeId":Ljava/lang/String;
    :goto_1
    if-gez v1, :cond_5

    move-object v0, p2

    .line 171
    .local v0, "losingVolumeId":Ljava/lang/String;
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->this$0:Lcom/google/android/apps/books/data/NetworkTaskQueue;

    # getter for: Lcom/google/android/apps/books/data/NetworkTaskQueue;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v3}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->access$000(Lcom/google/android/apps/books/data/NetworkTaskQueue;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v3

    const-string v4, "NetworkTaskQueue"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Preferring "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " over "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "losingVolumeId":Ljava/lang/String;
    .end local v2    # "winningVolumeId":Ljava/lang/String;
    :cond_4
    move-object v2, p2

    .line 169
    goto :goto_1

    .restart local v2    # "winningVolumeId":Ljava/lang/String;
    :cond_5
    move-object v0, p1

    .line 170
    goto :goto_2
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;)I
    .locals 6
    .param p1, "lhs"    # Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;
    .param p2, "rhs"    # Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;

    .prologue
    .line 135
    iget-object v4, p1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    invoke-virtual {v4}, Lcom/google/android/apps/books/data/NetworkTask;->getPriority()Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-result-object v4

    iget-object v5, p2, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    invoke-virtual {v5}, Lcom/google/android/apps/books/data/NetworkTask;->getPriority()Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/data/BooksDataController$Priority;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    .line 137
    .local v1, "priorityComparison":I
    if-eqz v1, :cond_1

    .line 138
    neg-int v3, v1

    .line 148
    :cond_0
    :goto_0
    return v3

    .line 141
    :cond_1
    iget-object v4, p1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    invoke-virtual {v4}, Lcom/google/android/apps/books/data/NetworkTask;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "leftVolumeId":Ljava/lang/String;
    iget-object v4, p2, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    invoke-virtual {v4}, Lcom/google/android/apps/books/data/NetworkTask;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "rightVolumeId":Ljava/lang/String;
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->compareVolumeIds(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 144
    .local v3, "volumeComparison":I
    if-nez v3, :cond_0

    .line 148
    iget v4, p1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->sequenceNumber:I

    iget v5, p2, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->sequenceNumber:I

    sub-int v3, v4, v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 119
    check-cast p1, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskComparator;->compare(Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;)I

    move-result v0

    return v0
.end method
