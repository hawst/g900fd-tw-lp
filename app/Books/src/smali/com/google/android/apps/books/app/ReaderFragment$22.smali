.class Lcom/google/android/apps/books/app/ReaderFragment$22;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 4418
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 4421
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11000(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4422
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/SearchScrubBar;->getPreviousButton()Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 4423
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_PREVIOUS_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 4426
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPreviousMatch()V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4439
    :cond_0
    :goto_0
    return-void

    .line 4427
    :catch_0
    move-exception v0

    .line 4428
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11100(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Exception;)V

    goto :goto_0

    .line 4430
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/SearchScrubBar;->getNextButton()Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 4431
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_NEXT_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 4434
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToNextMatch()V
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 4435
    :catch_1
    move-exception v0

    .line 4436
    .restart local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$22;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11100(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Exception;)V

    goto :goto_0
.end method
