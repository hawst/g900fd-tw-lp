.class public Lcom/google/android/apps/books/widget/BooksTitleRowCreator;
.super Ljava/lang/Object;
.source "BooksTitleRowCreator.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setTitle(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "containerView"    # Landroid/view/View;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 22
    const v1, 0x7f0e0203

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 24
    .local v0, "textView":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getTitleView(Ljava/lang/String;Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 16
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0400d7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 17
    .local v0, "containerView":Landroid/view/View;
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/widget/BooksTitleRowCreator;->setTitle(Landroid/view/View;Ljava/lang/String;)V

    .line 18
    return-object v0
.end method
