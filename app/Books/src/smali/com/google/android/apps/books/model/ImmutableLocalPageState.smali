.class public Lcom/google/android/apps/books/model/ImmutableLocalPageState;
.super Ljava/lang/Object;
.source "ImmutableLocalPageState.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;


# instance fields
.field private final mContentStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

.field private final mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

.field private final mStructureStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 0
    .param p1, "contentStatus"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .param p2, "structureStatus"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .param p3, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mContentStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 16
    iput-object p2, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mStructureStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 17
    iput-object p3, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    .line 18
    return-void
.end method


# virtual methods
.method public getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    return-object v0
.end method

.method public imageIsLocal()Z
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mContentStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public structureIsDownloaded()Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalPageState;->mStructureStatus:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
