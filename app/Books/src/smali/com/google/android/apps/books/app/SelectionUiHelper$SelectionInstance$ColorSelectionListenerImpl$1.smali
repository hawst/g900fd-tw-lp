.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->colorSelected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

.field final synthetic val$removeButtonParent:Landroid/view/ViewGroup;

.field final synthetic val$reqId:I

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;Landroid/view/ViewGroup;IJ)V
    .locals 0

    .prologue
    .line 1253
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->this$2:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$removeButtonParent:Landroid/view/ViewGroup;

    iput p3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$reqId:I

    iput-wide p4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$startTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 8
    .param p1, "transition"    # Landroid/animation/LayoutTransition;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "transitionType"    # I

    .prologue
    .line 1263
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$removeButtonParent:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1265
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->this$2:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    iget-object v4, v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v4, v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;
    invoke-static {v4}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->this$2:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    iget-object v4, v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v4, v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;
    invoke-static {v4}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/SelectionPopup;->getShowRequestId()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$reqId:I

    if-eq v4, v5, :cond_1

    .line 1281
    :cond_0
    :goto_0
    return-void

    .line 1269
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->val$startTime:J

    sub-long v2, v4, v6

    .line 1270
    .local v2, "durationMs":J
    const-wide/16 v4, 0x5dc

    const-wide/16 v6, 0x0

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 1272
    .local v0, "delay":J
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;->this$2:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->access$2800(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1$1;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;)V

    invoke-virtual {v4, v5, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0
    .param p1, "transition"    # Landroid/animation/LayoutTransition;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "transitionType"    # I

    .prologue
    .line 1257
    return-void
.end method
