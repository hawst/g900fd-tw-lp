.class public Lcom/google/android/apps/books/util/StoppableInputStream;
.super Ljava/io/InputStream;
.source "StoppableInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

.field private final mDebugString:Ljava/lang/String;

.field protected final mIn:Ljava/io/InputStream;

.field private mStopped:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;Ljava/lang/String;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;
    .param p3, "debugString"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    .line 52
    iput-object p3, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mDebugString:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;->onClose(Lcom/google/android/apps/books/util/StoppableInputStream;)V

    .line 79
    :cond_0
    return-void
.end method

.method public declared-synchronized mark(I)V
    .locals 1
    .param p1, "readlimit"    # I

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_0
    monitor-exit p0

    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 128
    iget-boolean v1, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v1, :cond_0

    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 148
    :goto_0
    return v1

    .line 131
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/security/GeneralSecurityException;

    if-eqz v1, :cond_1

    .line 136
    const-string v1, "Stoppable"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const-string v1, "Stoppable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore Corrupt Webview Resource Read (see b/10287113): Cause="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mDebugString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    .line 141
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_1
    const-string v1, "Stoppable"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    const-string v1, "Stoppable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad Webview Read: Cause="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mDebugString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_2
    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    :cond_0
    monitor-exit p0

    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 3
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mIn:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/StoppableInputStream;->mStopped:Z

    .line 60
    return-void
.end method
