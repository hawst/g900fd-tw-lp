.class public interface abstract Lcom/google/android/apps/books/model/BooksDataStore$Committer;
.super Ljava/lang/Object;
.source "BooksDataStore.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InputStreamSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Committer"
.end annotation


# virtual methods
.method public abstract bestEffortAbort()V
.end method

.method public abstract commit()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
