.class public Lcom/google/android/apps/books/widget/SearchScrubBar;
.super Landroid/widget/FrameLayout;
.source "SearchScrubBar.java"


# instance fields
.field private final mDelegateOnClickListener:Landroid/view/View$OnClickListener;

.field private mExitSearch:Landroid/view/View;

.field private mMatches:Landroid/widget/TextView;

.field private mNext:Landroid/view/View;

.field private mPrevious:Landroid/view/View;

.field private mRealOnClickListener:Landroid/view/View$OnClickListener;

.field private final mSearchScrubBarTextActiveColor:I

.field private final mSearchScrubBarTextInactiveColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/SearchScrubBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const v1, -0x777778

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    new-instance v0, Lcom/google/android/apps/books/widget/SearchScrubBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/SearchScrubBar$1;-><init>(Lcom/google/android/apps/books/widget/SearchScrubBar;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mDelegateOnClickListener:Landroid/view/View$OnClickListener;

    .line 57
    const v0, 0x7f01017b

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getColorFromAttr(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mSearchScrubBarTextActiveColor:I

    .line 59
    const v0, 0x7f01017c

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getColorFromAttr(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mSearchScrubBarTextInactiveColor:I

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/SearchScrubBar;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SearchScrubBar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mRealOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public getNextButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    return-object v0
.end method

.method public getPreviousButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    return-object v0
.end method

.method public hideSearchBarMatchText()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 83
    const v0, 0x7f0e01c7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    .line 84
    return-void
.end method

.method public setExitSearchListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "exitSearchListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 163
    const v0, 0x7f0e01c6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mExitSearch:Landroid/view/View;

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mExitSearch:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    return-void
.end method

.method public setMatchDescriptionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "matchDescriptionOnClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    return-void
.end method

.method public setNextButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 127
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mRealOnClickListener:Landroid/view/View$OnClickListener;

    .line 111
    return-void
.end method

.method public setPreviousButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 119
    return-void
.end method

.method public setSearchBarMatchText(IIZ)V
    .locals 8
    .param p1, "numMatchesBeforeSpread"    # I
    .param p2, "numMatches"    # I
    .param p3, "currentSpreadContainsMatch"    # Z

    .prologue
    const/4 v7, 0x0

    .line 132
    if-lez p2, :cond_1

    .line 134
    add-int/lit8 v0, p1, 0x1

    .line 136
    .local v0, "numMatchesToReport":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11000f

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, p2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "searchBarMatchText":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    if-eqz p3, :cond_0

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mSearchScrubBarTextActiveColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    .end local v0    # "numMatchesToReport":I
    .end local v1    # "searchBarMatchText":Ljava/lang/CharSequence;
    :goto_1
    return-void

    .line 143
    .restart local v0    # "numMatchesToReport":I
    .restart local v1    # "searchBarMatchText":Ljava/lang/CharSequence;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mMatches:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mSearchScrubBarTextInactiveColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 149
    .end local v0    # "numMatchesToReport":I
    .end local v1    # "searchBarMatchText":Ljava/lang/CharSequence;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->hideSearchBarMatchText()V

    goto :goto_1
.end method

.method public setupPagingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
    .locals 4
    .param p1, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    const v3, 0x7f0e01c9

    const v2, 0x7f0e01c8

    .line 92
    sget-object v1, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne p1, v1, :cond_0

    .line 93
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    .line 94
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 101
    .local v0, "res":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    const v2, 0x7f0f0117

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    const v2, 0x7f0f0118

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mDelegateOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mDelegateOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void

    .line 96
    .end local v0    # "res":Landroid/content/res/Resources;
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mPrevious:Landroid/view/View;

    .line 97
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/SearchScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/SearchScrubBar;->mNext:Landroid/view/View;

    goto :goto_0
.end method
