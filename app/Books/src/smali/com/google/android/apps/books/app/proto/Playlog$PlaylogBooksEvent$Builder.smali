.class public final Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Playlog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;",
        "Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->create()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 3

    .prologue
    .line 202
    new-instance v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;-><init>()V

    .line 203
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    new-instance v1, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;-><init>(Lcom/google/android/apps/books/app/proto/Playlog$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .line 204
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->buildPartial()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    if-nez v1, :cond_0

    .line 249
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .line 253
    .local v0, "returnMe":Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .line 254
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 2

    .prologue
    .line 221
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->create()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->mergeFrom(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->clone()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->clone()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->clone()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .prologue
    .line 258
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getDefaultInstance()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-object p0

    .line 259
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getClientId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setClientId(J)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    .line 262
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getType()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setType(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 274
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 278
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 279
    :sswitch_0
    return-object p0

    .line 284
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setClientId(J)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    goto :goto_0

    .line 288
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 289
    .local v0, "rawValue":I
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->valueOf(I)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    move-result-object v2

    .line 290
    .local v2, "value":Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setType(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    goto :goto_0

    .line 274
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setClientId(J)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->access$302(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Z)Z

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    # setter for: Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->clientId_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->access$402(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;J)J

    .line 310
    return-object p0
.end method

.method public setType(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    .prologue
    .line 326
    if-nez p1, :cond_0

    .line 327
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->access$502(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Z)Z

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->result:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    # setter for: Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->type_:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->access$602(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    .line 331
    return-object p0
.end method
