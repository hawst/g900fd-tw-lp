.class public Lcom/google/android/apps/books/model/ImmutablePage;
.super Ljava/lang/Object;
.source "ImmutablePage.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Page;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutablePage$1;,
        Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    }
.end annotation


# instance fields
.field private final mIsViewable:Z

.field private final mPageId:Ljava/lang/String;

.field private final mPageOrder:I

.field private final mRemoteUrl:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "pageOrder"    # I
    .param p3, "isViewable"    # Z
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "remoteUrl"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mPageId:Ljava/lang/String;

    .line 69
    iput p2, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mPageOrder:I

    .line 70
    iput-boolean p3, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mIsViewable:Z

    .line 71
    iput-object p4, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mTitle:Ljava/lang/String;

    .line 72
    iput-object p5, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mRemoteUrl:Ljava/lang/String;

    .line 73
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutablePage$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Lcom/google/android/apps/books/model/ImmutablePage$1;

    .prologue
    .line 5
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/model/ImmutablePage;-><init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 102
    instance-of v0, p1, Lcom/google/android/apps/books/model/Page;

    if-eqz v0, :cond_0

    .line 103
    check-cast p1, Lcom/google/android/apps/books/model/Page;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/PageUtils;->equals(Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/Page;)Z

    move-result v0

    .line 105
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mPageId:Ljava/lang/String;

    return-object v0
.end method

.method public getPageOrder()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mPageOrder:I

    return v0
.end method

.method public getRemoteUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mRemoteUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/android/apps/books/model/PageUtils;->hashCode(Lcom/google/android/apps/books/model/Page;)I

    move-result v0

    return v0
.end method

.method public isViewable()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutablePage;->mIsViewable:Z

    return v0
.end method
