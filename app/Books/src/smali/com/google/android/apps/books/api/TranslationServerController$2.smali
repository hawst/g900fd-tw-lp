.class Lcom/google/android/apps/books/api/TranslationServerController$2;
.super Ljava/lang/Object;
.source "TranslationServerController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/api/TranslationServerController;->translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/api/TranslationServerController;

.field final synthetic val$callback:Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;

.field final synthetic val$sourceLanguage:Ljava/lang/String;

.field final synthetic val$targetLanguage:Ljava/lang/String;

.field final synthetic val$text:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/api/TranslationServerController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$sourceLanguage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$targetLanguage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$text:Ljava/lang/CharSequence;

    iput-object p5, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 116
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mServer:Lcom/google/android/apps/books/api/TranslationServer;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$000(Lcom/google/android/apps/books/api/TranslationServerController;)Lcom/google/android/apps/books/api/TranslationServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$sourceLanguage:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$targetLanguage:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$text:Ljava/lang/CharSequence;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/api/TranslationServer;->translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;

    move-result-object v1

    .line 118
    .local v1, "result":Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$200(Lcom/google/android/apps/books/api/TranslationServerController;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/api/TranslationServerController$2$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/books/api/TranslationServerController$2$1;-><init>(Lcom/google/android/apps/books/api/TranslationServerController$2;Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .end local v1    # "result":Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
    :cond_0
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$2;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$200(Lcom/google/android/apps/books/api/TranslationServerController;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/api/TranslationServerController$2$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/api/TranslationServerController$2$2;-><init>(Lcom/google/android/apps/books/api/TranslationServerController$2;Ljava/lang/Exception;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
