.class Lcom/google/android/apps/books/render/BasicReaderController$MyListener;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ReaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;Lcom/google/android/apps/books/render/BasicReaderController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/BasicReaderController$1;

    .prologue
    .line 808
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;)V

    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderListener;->d(Ljava/lang/String;)V

    .line 1048
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderListener;->e(Ljava/lang/String;)V

    .line 1053
    return-void
.end method

.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onActivatedMoElement"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1058
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderListener;->onActivatedMoElement(IILjava/lang/String;)V

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1060
    return-void
.end method

.method public onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "performanceMetrics"    # Lcom/google/android/apps/books/util/JsPerformanceMetrics;

    .prologue
    const/4 v5, 0x3

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onChapterReady"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 851
    const-string v0, "ReaderController"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    const-string v0, "ReaderController"

    const-string v1, "onChapterLoaded(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    invoke-virtual {p2}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JS metrics: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1600(Lcom/google/android/apps/books/render/BasicReaderController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V

    .line 871
    :cond_1
    :goto_0
    return-void

    .line 862
    :cond_2
    const-string v0, "ReaderController"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863
    const-string v0, "ReaderController"

    const-string v1, "Suppressing onChapterReady"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onChapterUnloaded(I)V
    .locals 3
    .param p1, "passageIndex"    # I

    .prologue
    .line 875
    const-string v0, "ReaderController"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clearing Passage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onChapterUnloaded"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 879
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderListener;->onChapterUnloaded(I)V

    .line 882
    return-void
.end method

.method public onDocumentChanged()V
    .locals 2

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onDocumentChanged"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderListener;->onDocumentChanged()V

    .line 1029
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onError"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderListener;->onError(Ljava/lang/Throwable;)V

    .line 1043
    return-void
.end method

.method public onInvalidPosition(II)V
    .locals 3
    .param p1, "margin"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 961
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onInvalidPosition"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 963
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;

    .line 965
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;
    if-eqz v0, :cond_0

    .line 966
    invoke-interface {v0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;->run()V

    .line 973
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 974
    return-void

    .line 968
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onInvalidPosition(II)V

    goto :goto_0
.end method

.method public onJsApiReady()V
    .locals 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onJsApiReady"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderListener;->onJsApiReady()V

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderAlive:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1202(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 816
    return-void
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 1007
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onLoadedRangeData"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1008
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;

    .line 1009
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;
    if-eqz v0, :cond_0

    move v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1010
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;->run(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    .line 1012
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1013
    return-void
.end method

.method public onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V
    .locals 3
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1017
    .local p2, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataBulkHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;

    .line 1019
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;
    if-eqz v0, :cond_0

    .line 1020
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;->run(ILcom/google/common/collect/Multimap;)V

    .line 1022
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1023
    return-void
.end method

.method public onMissingPosition(I)V
    .locals 3
    .param p1, "requestId"    # I

    .prologue
    .line 978
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onMissingPosition"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 980
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;

    .line 982
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;
    if-eqz v0, :cond_0

    .line 983
    invoke-interface {v0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;->run()V

    .line 988
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 989
    return-void

    .line 985
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/ReaderListener;->onMissingPosition(I)V

    goto :goto_0
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 6
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onNearbyTextLoaded"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1035
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V

    .line 1037
    return-void
.end method

.method public onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "readingPosition"    # Ljava/lang/String;
    .param p6, "bounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p7, "firstPositionOnNextPageInPassage"    # Lcom/google/android/apps/books/common/Position;
    .param p8, "pageText"    # Ljava/lang/String;
    .param p9, "debugText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 940
    .local p4, "allPositionsOnThisPage":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    .local p5, "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onPageData"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 942
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    const-string v0, "ReaderController"

    const-string v1, "onPageData(%d, %d, %s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1600(Lcom/google/android/apps/books/render/BasicReaderController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 949
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/books/render/ReaderListener;->onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    :cond_1
    :goto_0
    return-void

    .line 953
    :cond_2
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 954
    const-string v0, "ReaderController"

    const-string v1, "Suppressing onPageData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPageLoaded(III)V
    .locals 7
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "requestId"    # I

    .prologue
    const/4 v6, 0x3

    .line 905
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onPageLoaded"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 907
    const-string v1, "ReaderController"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 908
    const-string v1, "ReaderController"

    const-string v2, "onPageLoaded(%d, %d, %d)"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;

    .line 916
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 919
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1600(Lcom/google/android/apps/books/render/BasicReaderController;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 920
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderListener;->onPageLoaded(III)V

    .line 922
    if-eqz v0, :cond_1

    .line 923
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;->run(II)V

    .line 931
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 932
    return-void

    .line 926
    :cond_2
    const-string v1, "ReaderController"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 927
    const-string v1, "ReaderController"

    const-string v2, "Suppressing onPageLoaded"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPageRangeReady(II)V
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "lastReadyPageIndex"    # I

    .prologue
    const/4 v5, 0x3

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onPageRangeReady"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 888
    const-string v0, "ReaderController"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 889
    const-string v0, "ReaderController"

    const-string v1, "onPageRangeReady(%d, %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1600(Lcom/google/android/apps/books/render/BasicReaderController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 895
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onPageRangeReady(II)V

    .line 901
    :cond_1
    :goto_0
    return-void

    .line 897
    :cond_2
    const-string v0, "ReaderController"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 898
    const-string v0, "ReaderController"

    const-string v1, "Suppressing onPageRangeReady"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1065
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onPassageMoListReady"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1066
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnReadableItemsReadyHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1000(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;

    .line 1068
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;
    if-eqz v0, :cond_0

    .line 1069
    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;->run(ILjava/util/Map;)V

    .line 1071
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1072
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 994
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v2, "BasicReaderController#onPassageTextReady"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 995
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnReadableItemsReadyHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1000(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;

    .line 997
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;
    if-eqz v0, :cond_0

    .line 998
    invoke-interface {v0, p2, p3, p4, p5}, Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;->run(ILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 1000
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1001
    return-void
.end method

.method public onPassagesPurged(ILjava/util/Collection;)V
    .locals 2
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1076
    .local p2, "passages":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onPassagesPurged"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 1077
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onPassagesPurged(ILjava/util/Collection;)V

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 1079
    return-void
.end method

.method public onPreferencesApplied()V
    .locals 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onPreferencesApplied"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mIssuedCurrentSettings:Z
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1500(Lcom/google/android/apps/books/render/BasicReaderController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderListener;->onPreferencesApplied()V

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1602(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 845
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 846
    return-void

    .line 842
    :cond_1
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    const-string v0, "ReaderController"

    const-string v1, "Ignoring application of stale settings"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onReaderInitialized()V
    .locals 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const-string v1, "BasicReaderController#onReaderInitialized"

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V

    .line 821
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderListener;->onReaderInitialized()V

    .line 822
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderInitialized:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1402(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V

    .line 824
    return-void
.end method

.method public onReaderReady()V
    .locals 2

    .prologue
    .line 828
    const-string v0, "ReaderController"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829
    const-string v0, "ReaderController"

    const-string v1, "onReaderReady is only utilized through the outbound ReaderListener."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
