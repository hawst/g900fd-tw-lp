.class public Lcom/google/android/apps/books/widget/PageTurnController;
.super Ljava/lang/Object;
.source "PageTurnController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;,
        Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;,
        Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;,
        Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;,
        Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;,
        Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;,
        Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

.field private final mCommandQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation
.end field

.field private mEndedTurnSequenceNumber:I

.field private mEnqueuedTurnSequenceNumber:I

.field private mGestureFraction:F

.field private mIsTurningLastValue:Z

.field private mStartedTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field private mStartedTurnSequenceNumber:I

.field private mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field private mWaitingForFinishAnimation:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;)V
    .locals 1
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    .line 159
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    .line 160
    return-void
.end method

.method static synthetic access$004(Lcom/google/android/apps/books/widget/PageTurnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/widget/PageTurnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PageTurnController;->startTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/PageTurnController;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/widget/PageTurnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # F

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/PageTurnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->publishTurnState()V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/widget/PageTurnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/widget/PageTurnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PageTurnController;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z

    return p1
.end method

.method private advance()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    .line 325
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 326
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 327
    .local v3, "tasks":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v1, "newTasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 329
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;

    .line 330
    .local v2, "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;->canRun()Z

    move-result v4

    if-nez v4, :cond_2

    .line 331
    const-string v4, "PageTurnController"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 332
    const-string v4, "PageTurnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Delaying "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 335
    :cond_2
    const-string v4, "PageTurnController"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 336
    const-string v4, "PageTurnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Servicing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 339
    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;->run()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 343
    .end local v2    # "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 348
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;

    .line 349
    .restart local v2    # "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_1

    .line 355
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "newTasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    .end local v2    # "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    .end local v3    # "tasks":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->maybeUpdateIsTurning()V

    .line 356
    return-void
.end method

.method private clearTurnState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 295
    iput-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 296
    iput-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 297
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    .line 298
    return-void
.end method

.method private enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V
    .locals 1
    .param p1, "command"    # Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->advance()V

    .line 312
    return-void
.end method

.method private maybeUpdateIsTurning()V
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->isTurning()Z

    move-result v0

    .line 367
    .local v0, "isTurning":Z
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mIsTurningLastValue:Z

    if-eq v1, v0, :cond_0

    .line 368
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mIsTurningLastValue:Z

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->onIsTurningChanged(Z)V

    .line 371
    :cond_0
    return-void
.end method

.method private publishTurnState()V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    iget v2, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;F)V

    .line 377
    return-void
.end method

.method private startTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 360
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 362
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->publishTurnState()V

    .line 363
    return-void
.end method


# virtual methods
.method public clearPendingOperations()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z

    .line 175
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->clearTurnState()V

    .line 178
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->publishTurnState()V

    .line 179
    return-void
.end method

.method public enqueueEndTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZF)V
    .locals 6
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F

    .prologue
    .line 238
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    if-ne v0, v1, :cond_0

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;

    iget v5, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;ZFI)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V

    goto :goto_0
.end method

.method public enqueueEndTurn(Z)Z
    .locals 6
    .param p1, "cancel"    # Z

    .prologue
    const/4 v0, 0x0

    .line 252
    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    iget v2, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    if-ne v1, v2, :cond_0

    .line 268
    :goto_0
    return v0

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->hasPendingOperations()Z

    move-result v1

    if-nez v1, :cond_2

    .line 258
    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 267
    .local v0, "result":Z
    :cond_1
    :goto_1
    new-instance v1, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;

    iget v2, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;ZI)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V

    goto :goto_0

    .line 265
    .end local v0    # "result":Z
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "result":Z
    goto :goto_1
.end method

.method public enqueueStartTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 7
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "finishAutomatically"    # Z

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 195
    .local v1, "removedTasks":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 196
    .local v3, "tasks":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 197
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;

    .line 198
    .local v2, "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;->canBeCanceled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 199
    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;->getDirection()Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    .line 201
    .local v0, "existingTaskDir":Lcom/google/android/apps/books/util/ScreenDirection;
    if-eqz v0, :cond_0

    if-eq p1, v0, :cond_0

    .line 202
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 207
    .end local v0    # "existingTaskDir":Lcom/google/android/apps/books/util/ScreenDirection;
    .end local v2    # "task":Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;
    :cond_1
    if-lez v1, :cond_2

    const-string v4, "PageTurnController"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 208
    const-string v4, "PageTurnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Turn dir changed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Discarded "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tasks"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_2
    new-instance v4, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V

    .line 213
    return-void
.end method

.method public enqueueTurnToPosition(Lcom/google/android/apps/books/render/PageIdentifier;)V
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 275
    new-instance v0, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/PageTurnController$TurnToPage;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/render/PageIdentifier;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V

    .line 276
    return-void
.end method

.method public enqueueUpdateTurn(F)V
    .locals 2
    .param p1, "gestureFraction"    # F

    .prologue
    .line 221
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    if-ne v0, v1, :cond_0

    .line 231
    :goto_0
    return-void

    .line 230
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;F)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTask(Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;)V

    goto :goto_0
.end method

.method public hasPendingOperations()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCommandQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTurning()Z
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->hasPendingOperations()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationFinished()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 284
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z

    if-eqz v1, :cond_1

    .line 285
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v3, v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 288
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->clearTurnState()V

    .line 289
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->publishTurnState()V

    .line 291
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PageTurnController;->advance()V

    .line 292
    return-void
.end method
