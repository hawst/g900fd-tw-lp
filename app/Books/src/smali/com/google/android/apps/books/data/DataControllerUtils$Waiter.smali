.class public Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;
.super Ljava/lang/Object;
.source "DataControllerUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Waiter"
.end annotation


# instance fields
.field private final mSemaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;->mSemaphore:Ljava/util/concurrent/Semaphore;

    .line 361
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;->block()V

    .line 362
    return-void
.end method


# virtual methods
.method public block()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 369
    return-void
.end method

.method protected unblock()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 377
    return-void
.end method
