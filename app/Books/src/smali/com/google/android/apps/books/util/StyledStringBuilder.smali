.class public Lcom/google/android/apps/books/util/StyledStringBuilder;
.super Ljava/lang/Object;
.source "StyledStringBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/StyledStringBuilder$Item;
    }
.end annotation


# instance fields
.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/StyledStringBuilder$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/StyledStringBuilder;->mItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addStyledText(Ljava/lang/CharSequence;Ljava/lang/Object;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "style"    # Ljava/lang/Object;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/util/StyledStringBuilder;->mItems:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;-><init>(Lcom/google/android/apps/books/util/StyledStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public addText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/util/StyledStringBuilder;->mItems:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;-><init>(Lcom/google/android/apps/books/util/StyledStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public build()Landroid/text/SpannableString;
    .locals 8

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/google/android/apps/books/util/StyledStringBuilder;->mItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;

    .line 42
    .local v2, "item":Lcom/google/android/apps/books/util/StyledStringBuilder$Item;
    iget-object v5, v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 44
    .end local v2    # "item":Lcom/google/android/apps/books/util/StyledStringBuilder$Item;
    :cond_0
    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 46
    .local v4, "result":Landroid/text/SpannableString;
    const/4 v3, 0x0

    .line 47
    .local v3, "itemStartOffset":I
    iget-object v5, p0, Lcom/google/android/apps/books/util/StyledStringBuilder;->mItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;

    .line 48
    .restart local v2    # "item":Lcom/google/android/apps/books/util/StyledStringBuilder$Item;
    iget-object v5, v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;->style:Ljava/lang/Object;

    if-eqz v5, :cond_1

    .line 49
    iget-object v5, v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;->style:Ljava/lang/Object;

    iget-object v6, v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;->text:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/2addr v6, v3

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 52
    :cond_1
    iget-object v5, v2, Lcom/google/android/apps/books/util/StyledStringBuilder$Item;->text:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v3, v5

    .line 53
    goto :goto_1

    .line 55
    .end local v2    # "item":Lcom/google/android/apps/books/util/StyledStringBuilder$Item;
    :cond_2
    return-object v4
.end method
