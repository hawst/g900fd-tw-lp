.class public abstract Lcom/google/android/apps/books/widget/CoverContainerView;
.super Landroid/widget/FrameLayout;
.source "CoverContainerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Landroid/widget/FrameLayout;"
    }
.end annotation


# instance fields
.field private mCoverView:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mCoverViewMeasuredHeight:I

.field private mIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getCoverView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    return-object v0
.end method

.method public abstract isCoverViewMeasurable()Z
.end method

.method public loadCoverView(Z)V
    .locals 5
    .param p1, "inLayout"    # Z

    .prologue
    .line 57
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;

    iget v1, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mIndex:I

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;->createCoverView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    .line 59
    if-eqz p1, :cond_1

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    const/4 v1, 0x0

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/widget/CoverContainerView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CoverContainerView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    iget v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewMeasuredHeight:I

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CoverContainerView;->isCoverViewMeasurable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewMeasuredHeight:I

    .line 93
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewMeasuredHeight:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 95
    return-void
.end method

.method public setCoverViewFactory(Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    .local p1, "factory":Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;, "Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory<TT;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverViewFactory:Lcom/google/android/apps/books/widget/CoverContainerView$CoverViewFactory;

    .line 50
    return-void
.end method

.method public setCoverViewIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    iput p1, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mIndex:I

    .line 54
    return-void
.end method

.method public unloadCoverView(Z)V
    .locals 1
    .param p1, "inLayout"    # Z

    .prologue
    .line 69
    .local p0, "this":Lcom/google/android/apps/books/widget/CoverContainerView;, "Lcom/google/android/apps/books/widget/CoverContainerView<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 70
    if-eqz p1, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CoverContainerView;->removeViewInLayout(Landroid/view/View;)V

    .line 75
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    .line 77
    :cond_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CoverContainerView;->mCoverView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CoverContainerView;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
