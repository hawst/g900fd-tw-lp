.class interface abstract Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "SentencePolicy"
.end annotation


# virtual methods
.method public abstract needsMoreSentencesToFinishCurrentMatch()Z
.end method

.method public abstract onSentence(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;Ljava/util/Collection;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;Z)V"
        }
    .end annotation
.end method
