.class public abstract enum Lcom/google/android/apps/books/model/VolumeData$Access;
.super Ljava/lang/Enum;
.source "VolumeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "Access"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/model/VolumeData$Access;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/model/VolumeData$Access;

.field public static final enum FREE:Lcom/google/android/apps/books/model/VolumeData$Access;

.field public static final enum NO_VIEW:Lcom/google/android/apps/books/model/VolumeData$Access;

.field public static final enum PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

.field public static final enum SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 181
    new-instance v0, Lcom/google/android/apps/books/model/VolumeData$Access$1;

    const-string v1, "NO_VIEW"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeData$Access$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->NO_VIEW:Lcom/google/android/apps/books/model/VolumeData$Access;

    .line 186
    new-instance v0, Lcom/google/android/apps/books/model/VolumeData$Access$2;

    const-string v1, "SAMPLE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/model/VolumeData$Access$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    .line 191
    new-instance v0, Lcom/google/android/apps/books/model/VolumeData$Access$3;

    const-string v1, "FREE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/model/VolumeData$Access$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->FREE:Lcom/google/android/apps/books/model/VolumeData$Access;

    .line 204
    new-instance v0, Lcom/google/android/apps/books/model/VolumeData$Access$4;

    const-string v1, "PURCHASED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/model/VolumeData$Access$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    .line 179
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/model/VolumeData$Access;

    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$Access;->NO_VIEW:Lcom/google/android/apps/books/model/VolumeData$Access;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$Access;->FREE:Lcom/google/android/apps/books/model/VolumeData$Access;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->$VALUES:[Lcom/google/android/apps/books/model/VolumeData$Access;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 179
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/apps/books/model/VolumeData$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/apps/books/model/VolumeData$1;

    .prologue
    .line 179
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/VolumeData$Access;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;
    .locals 1
    .param p0, "viewability"    # Ljava/lang/String;
    .param p1, "openAccess"    # Ljava/lang/String;
    .param p2, "buyUrl"    # Ljava/lang/String;

    .prologue
    .line 216
    const-string v0, "empty viewability"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkIsGraphic(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 217
    const-string v0, "empty openAccess"

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkIsGraphic(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 218
    const-string v0, "http://schemas.google.com/books/2008#view_all_pages"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    const-string v0, "http://schemas.google.com/books/2008#enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    sget-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->FREE:Lcom/google/android/apps/books/model/VolumeData$Access;

    .line 229
    :goto_0
    return-object v0

    .line 221
    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    sget-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    goto :goto_0

    .line 225
    :cond_1
    const-string v0, "http://schemas.google.com/books/2008#view_unknown"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "http://schemas.google.com/books/2008#view_no_pages"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->NO_VIEW:Lcom/google/android/apps/books/model/VolumeData$Access;

    goto :goto_0

    .line 229
    :cond_3
    sget-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 179
    const-class v0, Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeData$Access;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/model/VolumeData$Access;
    .locals 1

    .prologue
    .line 179
    sget-object v0, Lcom/google/android/apps/books/model/VolumeData$Access;->$VALUES:[Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/model/VolumeData$Access;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/model/VolumeData$Access;

    return-object v0
.end method


# virtual methods
.method public abstract shouldHonorTransition(Lcom/google/android/apps/books/model/VolumeData$Access;)Z
.end method
