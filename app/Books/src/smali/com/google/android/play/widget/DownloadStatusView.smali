.class public Lcom/google/android/play/widget/DownloadStatusView;
.super Landroid/view/View;
.source "DownloadStatusView.java"


# instance fields
.field protected mArcPaintOffline:Landroid/graphics/Paint;

.field protected mArcPaintOnline:Landroid/graphics/Paint;

.field protected final mArcRect:Landroid/graphics/RectF;

.field protected mBackground:Landroid/graphics/drawable/Drawable;

.field protected mCompleteOverlay:Landroid/graphics/drawable/Drawable;

.field protected mDefaultOverlay:Landroid/graphics/drawable/Drawable;

.field protected mDownloadFraction:F

.field protected mDownloadRequested:Z

.field protected mIsOnline:Z

.field protected mMinInProgressDisplayFraction:F

.field protected mProgressOverlay:Landroid/graphics/drawable/Drawable;


# direct methods
.method private getArcPaintSwitched()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mIsOnline:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOffline:Landroid/graphics/Paint;

    goto :goto_0
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 257
    .local v2, "res":Landroid/content/res/Resources;
    iget v3, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 258
    .local v1, "percentDownloaded":I
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget v4, Lcom/google/android/play/R$string;->play_percent_downloaded:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "descriptionStringBuilder":Ljava/lang/StringBuilder;
    const/16 v3, 0x64

    if-ge v1, v3, :cond_0

    .line 262
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v3, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    if-eqz v3, :cond_1

    sget v3, Lcom/google/android/play/R$string;->play_download_is_requested:I

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 262
    :cond_1
    sget v3, Lcom/google/android/play/R$string;->play_download_not_requested:I

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x43b40000    # 360.0f

    const/high16 v2, 0x43870000    # 270.0f

    .line 212
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 215
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    float-to-double v0, v0

    const-wide v8, 0x3e7ad7f29abcaf48L    # 1.0E-7

    cmpg-double v0, v0, v8

    if-gez v0, :cond_1

    .line 216
    iget-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    if-eqz v0, :cond_0

    .line 217
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    .line 218
    .local v7, "overlay":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mMinInProgressDisplayFraction:F

    mul-float/2addr v3, v0

    invoke-direct {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getArcPaintSwitched()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 235
    :goto_0
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 236
    return-void

    .line 221
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDefaultOverlay:Landroid/graphics/drawable/Drawable;

    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 223
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    float-to-double v0, v0

    const-wide v8, 0x3fefffffca501acbL    # 0.9999999

    cmpg-double v0, v0, v8

    if-gez v0, :cond_2

    .line 224
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    iget v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mMinInProgressDisplayFraction:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 226
    .local v6, "downloadFraction":F
    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    mul-float/2addr v3, v6

    invoke-direct {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getArcPaintSwitched()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 227
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    .line 228
    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 230
    .end local v6    # "downloadFraction":F
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 231
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mCompleteOverlay:Landroid/graphics/drawable/Drawable;

    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 8
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingLeft()I

    move-result v2

    .line 241
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingTop()I

    move-result v3

    .line 242
    .local v3, "paddingTop":I
    iget-object v6, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 243
    .local v1, "backgroundWidth":I
    iget-object v6, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 245
    .local v0, "backgroundHeight":I
    add-int v6, v1, v2

    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingRight()I

    move-result v7

    add-int v5, v6, v7

    .line 246
    .local v5, "viewWidth":I
    add-int v6, v0, v3

    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingBottom()I

    move-result v7

    add-int v4, v6, v7

    .line 248
    .local v4, "viewHeight":I
    invoke-virtual {p0, v5, v4}, Lcom/google/android/play/widget/DownloadStatusView;->setMeasuredDimension(II)V

    .line 249
    return-void
.end method
