.class public final Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidClientInfo"
.end annotation


# instance fields
.field public androidId:J

.field public applicationBuild:Ljava/lang/String;

.field public board:Ljava/lang/String;

.field public brand:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public device:Ljava/lang/String;

.field public deviceId:J

.field public fingerprint:Ljava/lang/String;

.field public gmsCoreVersionCode:I

.field public hardware:Ljava/lang/String;

.field public locale:Ljava/lang/String;

.field public loggingId:Ljava/lang/String;

.field public manufacturer:Ljava/lang/String;

.field public mccMnc:Ljava/lang/String;

.field public model:Ljava/lang/String;

.field public osBuild:Ljava/lang/String;

.field public product:Ljava/lang/String;

.field public radioVersion:Ljava/lang/String;

.field public sdkVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 691
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 692
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->clear()Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    .line 693
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 696
    iput-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    .line 697
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    .line 698
    iput-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    .line 699
    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    .line 700
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    .line 701
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    .line 702
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    .line 703
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    .line 704
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    .line 705
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    .line 706
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    .line 707
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    .line 708
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    .line 709
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    .line 710
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    .line 711
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    .line 712
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    .line 713
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    .line 714
    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    .line 715
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->cachedSize:I

    .line 716
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 784
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 785
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 786
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 789
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 790
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 793
    :cond_1
    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    if-eqz v1, :cond_2

    .line 794
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 797
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 798
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 801
    :cond_3
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 802
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 805
    :cond_4
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 806
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 809
    :cond_5
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 810
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 813
    :cond_6
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 814
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 817
    :cond_7
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 818
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 821
    :cond_8
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 822
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 825
    :cond_9
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 826
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 829
    :cond_a
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 830
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 833
    :cond_b
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 834
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 837
    :cond_c
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 838
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 841
    :cond_d
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 842
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 845
    :cond_e
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 846
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 849
    :cond_f
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 850
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 853
    :cond_10
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_11

    .line 854
    const/16 v1, 0x12

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 857
    :cond_11
    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    if-eqz v1, :cond_12

    .line 858
    const/16 v1, 0x13

    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 861
    :cond_12
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 869
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 870
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 874
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 875
    :sswitch_0
    return-object p0

    .line 880
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    goto :goto_0

    .line 884
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    goto :goto_0

    .line 888
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    goto :goto_0

    .line 892
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    goto :goto_0

    .line 896
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    goto :goto_0

    .line 900
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    goto :goto_0

    .line 904
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    goto :goto_0

    .line 908
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    goto :goto_0

    .line 912
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    goto :goto_0

    .line 916
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    goto :goto_0

    .line 920
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    goto :goto_0

    .line 924
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    goto :goto_0

    .line 928
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    goto :goto_0

    .line 932
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    goto :goto_0

    .line 936
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    goto :goto_0

    .line 940
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    goto :goto_0

    .line 944
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    goto/16 :goto_0

    .line 948
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    goto/16 :goto_0

    .line 952
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    goto/16 :goto_0

    .line 870
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 617
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 722
    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 723
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 726
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 728
    :cond_1
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    if-eqz v0, :cond_2

    .line 729
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 731
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 732
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 734
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 735
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 737
    :cond_4
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 738
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 740
    :cond_5
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 741
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 743
    :cond_6
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 744
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 746
    :cond_7
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 747
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 749
    :cond_8
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 750
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 752
    :cond_9
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 753
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 755
    :cond_a
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 756
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 758
    :cond_b
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 759
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 761
    :cond_c
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 762
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 764
    :cond_d
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 765
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 767
    :cond_e
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 768
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 770
    :cond_f
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 771
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 773
    :cond_10
    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 774
    const/16 v0, 0x12

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 776
    :cond_11
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    if-eqz v0, :cond_12

    .line 777
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->gmsCoreVersionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 779
    :cond_12
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 780
    return-void
.end method
