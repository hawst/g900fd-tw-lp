.class public Lcom/google/android/play/search/PlaySearchPlateTextContainer;
.super Landroid/widget/FrameLayout;
.source "PlaySearchPlateTextContainer.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/play/search/PlaySearchListener;


# instance fields
.field private mController:Lcom/google/android/play/search/PlaySearchController;

.field private final mInputManager:Landroid/view/inputmethod/InputMethodManager;

.field private mSearchBoxIdleText:Landroid/widget/ImageView;

.field private mSearchBoxTextInput:Landroid/widget/EditText;

.field private mSuspendTextChangeCallback:Z

.field private mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)Lcom/google/android/play/search/PlaySearchController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    return-object v0
.end method

.method private focusInputBoxAndShowKeyboard()V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 136
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 137
    return-void
.end method

.method private requestVoice()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchVoiceController;->requestVoiceSearch(Landroid/content/Context;)V

    .line 143
    :cond_0
    return-void
.end method

.method private switchToMode(I)V
    .locals 3
    .param p1, "searchMode"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 79
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->focusInputBoxAndShowKeyboard()V

    .line 98
    :goto_0
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 94
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->unfocusInputBoxAndHideKeyboard()V

    .line 96
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 92
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->requestVoice()V

    goto :goto_1
.end method

.method private unfocusInputBoxAndHideKeyboard()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 148
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 132
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 127
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 103
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchVoiceController;->cancelPendingVoiceSearch(Landroid/content/Context;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 47
    sget v0, Lcom/google/android/play/R$id;->search_box_idle_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    .line 48
    sget v0, Lcom/google/android/play/R$id;->search_box_text_input:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->switchToMode(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/play/search/PlaySearchPlateTextContainer$1;

    invoke-direct {v1, p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer$1;-><init>(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchController;->setMode(I)V

    .line 166
    :cond_0
    return-void
.end method

.method public onModeChanged(I)V
    .locals 0
    .param p1, "searchMode"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->switchToMode(I)V

    .line 76
    return-void
.end method

.method public onQueryChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSuspendTextChangeCallback:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSuspendTextChangeCallback:Z

    .line 115
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSuspendTextChangeCallback:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchController;->setQuery(Ljava/lang/String;)V

    .line 122
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 153
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0}, Lcom/google/android/play/search/PlaySearchController;->getMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchController;->setMode(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchVoiceController;->cancelPendingVoiceSearch(Landroid/content/Context;)V

    .line 159
    :cond_0
    return-void
.end method

.method public setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 2
    .param p1, "playSearchController"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->removePlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 68
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 69
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 70
    new-instance v0, Lcom/google/android/play/search/PlaySearchVoiceController;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-direct {v0, v1}, Lcom/google/android/play/search/PlaySearchVoiceController;-><init>(Lcom/google/android/play/search/PlaySearchController;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mVoiceController:Lcom/google/android/play/search/PlaySearchVoiceController;

    .line 71
    return-void
.end method
