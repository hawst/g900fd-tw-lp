.class public Lcom/google/android/play/search/PlaySearchActionButton;
.super Landroid/widget/ImageView;
.source "PlaySearchActionButton.java"

# interfaces
.implements Lcom/google/android/play/search/PlaySearchListener;


# instance fields
.field private final mCanPerformVoiceSearch:Z

.field private mClearDrawable:Landroid/graphics/drawable/Drawable;

.field private mController:Lcom/google/android/play/search/PlaySearchController;

.field private mCurrentMode:I

.field private mMicDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$drawable;->play_ic_clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mClearDrawable:Landroid/graphics/drawable/Drawable;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$drawable;->ic_mic_dark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mMicDrawable:Landroid/graphics/drawable/Drawable;

    .line 54
    invoke-static {p1}, Lcom/google/android/play/search/PlaySearchVoiceController;->canPerformVoiceSearch(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCanPerformVoiceSearch:Z

    .line 55
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/PlaySearchController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchActionButton;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/search/PlaySearchActionButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchActionButton;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    return v0
.end method

.method private handleChange()V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 98
    iget-object v4, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-nez v4, :cond_0

    .line 111
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v4}, Lcom/google/android/play/search/PlaySearchController;->getMode()I

    move-result v1

    .line 103
    .local v1, "searchMode":I
    iget-object v4, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v4}, Lcom/google/android/play/search/PlaySearchController;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "query":Ljava/lang/String;
    if-eq v1, v5, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 105
    :cond_1
    iget-boolean v4, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCanPerformVoiceSearch:Z

    if-eqz v4, :cond_2

    :goto_1
    invoke-direct {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 106
    :cond_3
    if-eq v1, v2, :cond_4

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 107
    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    goto :goto_0

    .line 109
    :cond_5
    invoke-direct {p0, v3}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    goto :goto_0
.end method

.method private setMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    .line 115
    if-nez p1, :cond_1

    .line 116
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const/4 v1, 0x0

    .line 121
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v0, -0x1

    .line 122
    .local v0, "accessibilityString":I
    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    .line 123
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mClearDrawable:Landroid/graphics/drawable/Drawable;

    .line 124
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_clear:I

    .line 130
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_3
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 126
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mMicDrawable:Landroid/graphics/drawable/Drawable;

    .line 127
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_voice_search_button:I

    goto :goto_1
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishInflate()V

    .line 61
    new-instance v0, Lcom/google/android/play/search/PlaySearchActionButton$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/PlaySearchActionButton$1;-><init>(Lcom/google/android/play/search/PlaySearchActionButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method

.method public onModeChanged(I)V
    .locals 0
    .param p1, "searchMode"    # I

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->handleChange()V

    .line 90
    return-void
.end method

.method public onQueryChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->handleChange()V

    .line 95
    return-void
.end method

.method public setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 1
    .param p1, "playSearchController"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->removePlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 84
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 85
    return-void
.end method
