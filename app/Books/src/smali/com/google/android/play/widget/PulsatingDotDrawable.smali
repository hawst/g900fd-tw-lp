.class public Lcom/google/android/play/widget/PulsatingDotDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "PulsatingDotDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# instance fields
.field protected mIsAnimating:Z

.field protected final mMinRadius:F

.field protected final mOffsetMs:J

.field protected final mPaint:Landroid/graphics/Paint;

.field protected final mPeriodMs:J

.field protected final mRadiusSpan:F

.field protected mStartMs:J


# direct methods
.method public constructor <init>(IJJFF)V
    .locals 2
    .param p1, "color"    # I
    .param p2, "periodMs"    # J
    .param p4, "offsetMs"    # J
    .param p6, "minRadius"    # F
    .param p7, "maxRadius"    # F

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 40
    iput-wide p2, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPeriodMs:J

    .line 41
    iput-wide p4, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mOffsetMs:J

    .line 43
    const/4 v0, 0x0

    invoke-static {v0, p6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mMinRadius:F

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p7, v0}, Ljava/lang/Math;->min(FF)F

    move-result p7

    .line 45
    sub-float v0, p7, p6

    iput v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mRadiusSpan:F

    .line 47
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    .line 48
    iget-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    iget-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    iget-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected calculateRadiusFraction()F
    .locals 13

    .prologue
    const/high16 v12, 0x3f000000    # 0.5f

    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 105
    .local v0, "currentTimeMs":J
    iget-wide v8, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mOffsetMs:J

    add-long/2addr v8, v0

    iget-wide v10, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPeriodMs:J

    rem-long v6, v8, v10

    .line 106
    .local v6, "relativeTimeMs":J
    long-to-float v5, v6

    iget-wide v8, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPeriodMs:J

    long-to-float v8, v8

    div-float v2, v5, v8

    .line 108
    .local v2, "cycleFraction":F
    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v10, v2

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v5, v8

    mul-float/2addr v5, v12

    add-float v3, v12, v5

    .line 109
    .local v3, "interpolated":F
    iget v5, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mMinRadius:F

    iget v8, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mRadiusSpan:F

    mul-float/2addr v8, v3

    add-float v4, v5, v8

    .line 110
    .local v4, "output":F
    return v4
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 55
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    .line 56
    .local v9, "width":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    .line 57
    .local v7, "height":I
    int-to-float v3, v9

    int-to-float v4, v7

    const/16 v5, 0xff

    const/16 v6, 0x1f

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayerAlpha(FFFFII)I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->calculateRadiusFraction()F

    move-result v0

    invoke-static {v9, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    div-float v8, v0, v10

    .line 59
    .local v8, "radius":F
    int-to-float v0, v9

    div-float/2addr v0, v10

    int-to-float v1, v7

    div-float/2addr v1, v10

    iget-object v2, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v8, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->invalidateSelf()V

    .line 65
    :cond_0
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mIsAnimating:Z

    return v0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 75
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 80
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mStartMs:J

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mIsAnimating:Z

    .line 92
    invoke-virtual {p0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->invalidateSelf()V

    .line 94
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/PulsatingDotDrawable;->mIsAnimating:Z

    .line 101
    return-void
.end method
