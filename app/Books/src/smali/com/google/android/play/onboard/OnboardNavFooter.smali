.class public Lcom/google/android/play/onboard/OnboardNavFooter;
.super Landroid/widget/FrameLayout;
.source "OnboardNavFooter.java"


# instance fields
.field protected final mEndButton:Landroid/widget/TextView;

.field protected final mPageIndicator:Lcom/google/android/play/widget/PageIndicator;

.field protected final mStartButton:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$layout;->play_onboard_nav_footer:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$color;->play_onboard_app_color_dark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->setBackgroundColor(I)V

    .line 48
    sget v0, Lcom/google/android/play/R$id;->start_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mStartButton:Landroid/widget/TextView;

    .line 49
    sget v0, Lcom/google/android/play/R$id;->end_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mEndButton:Landroid/widget/TextView;

    .line 50
    sget v0, Lcom/google/android/play/R$id;->page_indicator:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/PageIndicator;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mPageIndicator:Lcom/google/android/play/widget/PageIndicator;

    .line 51
    return-void
.end method


# virtual methods
.method protected updateButton(Landroid/widget/TextView;Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;)V
    .locals 6
    .param p1, "button"    # Landroid/widget/TextView;
    .param p2, "info"    # Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->isEnabled()Z

    move-result v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 70
    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->getIconResId()I

    move-result v3

    if-nez v3, :cond_1

    move-object v2, v4

    .line 73
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mStartButton:Landroid/widget/TextView;

    if-ne p1, v3, :cond_2

    .line 74
    invoke-static {p1, v2}, Lcom/google/android/play/utils/TextViewUtils;->setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V

    .line 78
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->getClickRunnable()Ljava/lang/Runnable;

    move-result-object v1

    .line 79
    .local v1, "clickRunnable":Ljava/lang/Runnable;
    if-nez v1, :cond_3

    move-object v0, v4

    .line 86
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    :goto_3
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void

    .line 68
    .end local v0    # "clickListener":Landroid/view/View$OnClickListener;
    .end local v1    # "clickRunnable":Ljava/lang/Runnable;
    .end local v2    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v3, 0x4

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardNavFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->getIconResId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 76
    .restart local v2    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-static {p1, v2}, Lcom/google/android/play/utils/TextViewUtils;->setDrawableEnd(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 79
    .restart local v1    # "clickRunnable":Ljava/lang/Runnable;
    :cond_3
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooter$1;

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/onboard/OnboardNavFooter$1;-><init>(Lcom/google/android/play/onboard/OnboardNavFooter;Ljava/lang/Runnable;)V

    goto :goto_3
.end method

.method public updatePageInfo(Lcom/google/android/play/onboard/OnboardHostControl;Lcom/google/android/play/onboard/OnboardPageInfo;)V
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;
    .param p2, "pageInfo"    # Lcom/google/android/play/onboard/OnboardPageInfo;

    .prologue
    .line 58
    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/OnboardPageInfo;->isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mPageIndicator:Lcom/google/android/play/widget/PageIndicator;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/OnboardPageInfo;->getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/PageIndicator;->setPageCount(I)V

    .line 61
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mPageIndicator:Lcom/google/android/play/widget/PageIndicator;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/OnboardPageInfo;->getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/PageIndicator;->setSelectedPage(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mStartButton:Landroid/widget/TextView;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/OnboardPageInfo;->getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooter;->updateButton(Landroid/widget/TextView;Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->mEndButton:Landroid/widget/TextView;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/OnboardPageInfo;->getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooter;->updateButton(Landroid/widget/TextView;Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;)V

    .line 65
    return-void

    .line 58
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
