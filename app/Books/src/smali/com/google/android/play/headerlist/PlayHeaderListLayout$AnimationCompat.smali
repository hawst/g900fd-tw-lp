.class Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
.super Ljava/lang/Object;
.source "PlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationCompat"
.end annotation


# instance fields
.field private mAlpha:F

.field private mScale:F

.field private mTranslationX:F

.field private mTranslationY:F

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 3436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3431
    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    .line 3432
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    .line 3433
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationX:F

    .line 3434
    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    .line 3437
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    .line 3438
    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    .param p1, "x1"    # F

    .prologue
    .line 3429
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    .param p1, "x1"    # F

    .prologue
    .line 3429
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    return p1
.end method


# virtual methods
.method public animateAlpha(FI)V
    .locals 5
    .param p1, "toAlpha"    # F
    .param p2, "duration"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/16 v4, 0xc

    .line 3472
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 3500
    :goto_0
    return-void

    .line 3475
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_1

    .line 3476
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 3478
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    invoke-direct {v0, v1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3479
    .local v0, "animation":Landroid/view/animation/AlphaAnimation;
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 3480
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 3482
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_2

    .line 3483
    new-instance v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3498
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public animateScale(FI)V
    .locals 5
    .param p1, "toScale"    # F
    .param p2, "duration"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/16 v4, 0xc

    .line 3596
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 3597
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    .line 3628
    :goto_0
    return-void

    .line 3600
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_1

    .line 3601
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 3606
    :cond_1
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    invoke-direct {v0, v1, p1, v2, p1}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 3607
    .local v0, "animation":Landroid/view/animation/ScaleAnimation;
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 3608
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 3610
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_2

    .line 3611
    new-instance v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat$2;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3626
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public getAlpha()F
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3460
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 3461
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    .line 3466
    :goto_0
    return v0

    .line 3463
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 3464
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    goto :goto_0

    .line 3466
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    goto :goto_0
.end method

.method public getTranslationY()F
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3511
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 3512
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    .line 3517
    :goto_0
    return v0

    .line 3514
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 3515
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0

    .line 3517
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    goto :goto_0
.end method

.method public setAlpha(F)V
    .locals 4
    .param p1, "alpha"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3442
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 3456
    :cond_0
    :goto_0
    return-void

    .line 3445
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 3446
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 3448
    :cond_2
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 3449
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mAlpha:F

    .line 3450
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3451
    .local v0, "animation":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 3452
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 3453
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public setScale(F)V
    .locals 5
    .param p1, "scale"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3575
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 3576
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    .line 3592
    :cond_0
    :goto_0
    return-void

    .line 3579
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 3580
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setScaleX(F)V

    .line 3581
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 3583
    :cond_2
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 3584
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    .line 3585
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mScale:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 3587
    .local v0, "animation":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 3588
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 3589
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public setTranslationX(F)V
    .locals 4
    .param p1, "translationX"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3555
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 3556
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationX:F

    .line 3571
    :cond_0
    :goto_0
    return-void

    .line 3559
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 3560
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 3562
    :cond_2
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationX:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 3563
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationX:F

    .line 3564
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, p1, p1, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 3566
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 3567
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 3568
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public setTranslationY(F)V
    .locals 4
    .param p1, "translationY"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3523
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 3524
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    .line 3539
    :cond_0
    :goto_0
    return-void

    .line 3527
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 3528
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 3530
    :cond_2
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 3531
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mTranslationY:F

    .line 3532
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v3, v3, p1, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 3534
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 3535
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 3536
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public setZ(F)V
    .locals 1
    .param p1, "z"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 3504
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z
    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$1300()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3505
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setZ(F)V

    .line 3507
    :cond_0
    return-void
.end method
