.class public Lcom/google/android/play/widget/TouchRedirectingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "TouchRedirectingFrameLayout.java"


# instance fields
.field private mCapturingChildView:Landroid/view/View;

.field private mVisibilities:[I


# virtual methods
.method protected captureTouchEventJBMR1(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 72
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 73
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 74
    .local v0, "result":Z
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 75
    return v0
.end method

.method protected captureTouchEventPreJBMR1(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->getChildCount()I

    move-result v2

    .line 89
    .local v2, "childCount":I
    iget-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mCapturingChildView:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 91
    .local v0, "capturingIndex":I
    sub-int v7, v2, v0

    add-int/lit8 v3, v7, -0x1

    .line 92
    .local v3, "countAbove":I
    iget-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mVisibilities:[I

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mVisibilities:[I

    array-length v7, v7

    if-eq v7, v3, :cond_1

    .line 93
    :cond_0
    new-array v7, v3, [I

    iput-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mVisibilities:[I

    .line 96
    :cond_1
    add-int/lit8 v4, v0, 0x1

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_3

    .line 97
    invoke-virtual {p0, v4}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 98
    .local v1, "child":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mVisibilities:[I

    sub-int v8, v4, v0

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v6

    aput v6, v7, v8

    .line 99
    .local v6, "visibility":I
    if-nez v6, :cond_2

    .line 100
    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 96
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 103
    .end local v1    # "child":Landroid/view/View;
    .end local v6    # "visibility":I
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 105
    .local v5, "preliminaryResult":Z
    add-int/lit8 v4, v0, 0x1

    :goto_1
    if-ge v4, v2, :cond_4

    .line 106
    invoke-virtual {p0, v4}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 107
    .restart local v1    # "child":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mVisibilities:[I

    sub-int v8, v4, v0

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 105
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 109
    .end local v1    # "child":Landroid/view/View;
    :cond_4
    if-nez v5, :cond_5

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    const/4 v7, 0x1

    :goto_2
    return v7

    :cond_6
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mCapturingChildView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->captureTouchEventJBMR1(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 56
    :goto_0
    return v0

    .line 52
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->captureTouchEventPreJBMR1(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 56
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1
    .param p1, "childCount"    # I
    .param p2, "i"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mCapturingChildView:Landroid/view/View;

    if-eqz v0, :cond_0

    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->mCapturingChildView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/TouchRedirectingFrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->getChildDrawingOrder(II)I

    move-result v0

    goto :goto_0
.end method
