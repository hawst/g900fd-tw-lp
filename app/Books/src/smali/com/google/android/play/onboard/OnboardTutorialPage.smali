.class public Lcom/google/android/play/onboard/OnboardTutorialPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "OnboardTutorialPage.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;,
        Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;
    }
.end annotation


# static fields
.field public static final DK_BACKGROUND_COLOR:I

.field public static final DK_BODY_TEXT:I

.field public static final DK_ICON_DRAWABLE_ID:I

.field public static final DK_TITLE_TEXT:I


# instance fields
.field protected mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_backgroundColor:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BACKGROUND_COLOR:I

    .line 27
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_titleText:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_TITLE_TEXT:I

    .line 31
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_bodyText:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BODY_TEXT:I

    .line 35
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_iconDrawableId:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_ICON_DRAWABLE_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardTutorialPage;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 60
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/google/android/play/onboard/OnboardPageInfo;

    check-cast v1, Lcom/google/android/play/onboard/OnboardPageInfo;

    return-object v1

    :cond_0
    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_INFO:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public onEnterPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 72
    return-void
.end method

.method public onExitPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 77
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 64
    return-void
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 67
    return-void
.end method

.method public setOnboardHostControl(Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0
    .param p1, "control"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardTutorialPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    .line 55
    return-void
.end method
