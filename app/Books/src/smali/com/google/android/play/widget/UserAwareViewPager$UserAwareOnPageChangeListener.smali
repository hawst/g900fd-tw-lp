.class public Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "UserAwareViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/widget/UserAwareViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserAwareOnPageChangeListener"
.end annotation


# instance fields
.field private final viewPager:Lcom/google/android/play/widget/UserAwareViewPager;


# direct methods
.method public constructor <init>(Lcom/google/android/play/widget/UserAwareViewPager;)V
    .locals 0
    .param p1, "viewPager"    # Lcom/google/android/play/widget/UserAwareViewPager;

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;->viewPager:Lcom/google/android/play/widget/UserAwareViewPager;

    .line 82
    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 1
    .param p1, "visualPosition"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;->viewPager:Lcom/google/android/play/widget/UserAwareViewPager;

    invoke-virtual {v0}, Lcom/google/android/play/widget/UserAwareViewPager;->isSettingCurrentItem()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;->onPageSelected(IZ)V

    .line 87
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPageSelected(IZ)V
    .locals 0
    .param p1, "visualPosition"    # I
    .param p2, "byUser"    # Z

    .prologue
    .line 94
    return-void
.end method
