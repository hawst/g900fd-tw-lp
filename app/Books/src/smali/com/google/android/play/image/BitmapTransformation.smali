.class public interface abstract Lcom/google/android/play/image/BitmapTransformation;
.super Ljava/lang/Object;
.source "BitmapTransformation.java"


# virtual methods
.method public abstract drawFocusedOverlay(Landroid/graphics/Canvas;II)V
.end method

.method public abstract drawPressedOverlay(Landroid/graphics/Canvas;II)V
.end method

.method public abstract getTransformationInset(II)I
.end method

.method public abstract transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
.end method
