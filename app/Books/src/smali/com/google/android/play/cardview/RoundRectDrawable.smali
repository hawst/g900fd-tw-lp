.class Lcom/google/android/play/cardview/RoundRectDrawable;
.super Lcom/google/android/play/cardview/CardViewBackgroundDrawable;
.source "RoundRectDrawable.java"


# instance fields
.field private final mBoundsF:Landroid/graphics/RectF;

.field private final mBoundsI:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/content/res/ColorStateList;FF)V
    .locals 1
    .param p1, "colorStateList"    # Landroid/content/res/ColorStateList;
    .param p2, "radius"    # F
    .param p3, "inset"    # F

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;-><init>(Landroid/content/res/ColorStateList;FF)V

    .line 25
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsF:Landroid/graphics/RectF;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsI:Landroid/graphics/Rect;

    .line 27
    return-void
.end method

.method private updateBounds(Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsI:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsI:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mInset:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mInset:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 37
    iget-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsF:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsI:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 38
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsF:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mCornerRadius:F

    iget v2, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mCornerRadius:F

    iget-object v3, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 32
    return-void
.end method

.method public getOutline(Landroid/graphics/Outline;)V
    .locals 2
    .param p1, "outline"    # Landroid/graphics/Outline;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mBoundsI:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/play/cardview/RoundRectDrawable;->mCornerRadius:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Outline;->setRoundRect(Landroid/graphics/Rect;F)V

    .line 50
    return-void
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/play/cardview/RoundRectDrawable;->updateBounds(Landroid/graphics/Rect;)V

    .line 44
    return-void
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 55
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 60
    return-void
.end method
