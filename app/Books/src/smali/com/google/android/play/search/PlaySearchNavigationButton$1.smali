.class Lcom/google/android/play/search/PlaySearchNavigationButton$1;
.super Ljava/lang/Object;
.source "PlaySearchNavigationButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/search/PlaySearchNavigationButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;


# direct methods
.method constructor <init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$000(Lcom/google/android/play/search/PlaySearchNavigationButton;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchController;->setMode(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchController;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchController;->setQuery(Ljava/lang/String;)V

    .line 70
    :cond_0
    return-void
.end method
