.class final Lcom/google/android/play/cardview/CardViewGroupDelegates$1;
.super Ljava/lang/Object;
.source "CardViewGroupDelegates.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroupDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/cardview/CardViewGroupDelegates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 52
    return-void
.end method

.method public setBackgroundColor(Landroid/view/View;I)V
    .locals 0
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 56
    return-void
.end method

.method public setBackgroundResource(Landroid/view/View;I)V
    .locals 0
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "resId"    # I

    .prologue
    .line 60
    return-void
.end method
