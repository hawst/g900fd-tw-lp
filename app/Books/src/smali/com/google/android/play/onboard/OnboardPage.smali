.class public interface abstract Lcom/google/android/play/onboard/OnboardPage;
.super Ljava/lang/Object;
.source "OnboardPage.java"


# static fields
.field public static final DK_PAGE_ID:I

.field public static final DK_PAGE_INFO:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardPage_pageId:I

    sput v0, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    .line 18
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardPage_pageInfo:I

    sput v0, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_INFO:I

    return-void
.end method


# virtual methods
.method public abstract getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;
.end method

.method public abstract onEnterPage(Z)V
.end method

.method public abstract onExitPage(Z)V
.end method

.method public abstract restoreOnboardState(Landroid/os/Bundle;)V
.end method

.method public abstract saveOnboardState(Landroid/os/Bundle;)V
.end method

.method public abstract setOnboardHostControl(Lcom/google/android/play/onboard/OnboardHostControl;)V
.end method
