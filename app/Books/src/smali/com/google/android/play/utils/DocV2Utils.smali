.class public Lcom/google/android/play/utils/DocV2Utils;
.super Ljava/lang/Object;
.source "DocV2Utils.java"


# direct methods
.method public static getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;
    .locals 4
    .param p0, "doc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p1, "imageType"    # I

    .prologue
    const/4 v2, 0x0

    .line 13
    if-eqz p0, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v3, :cond_1

    .line 23
    :cond_0
    :goto_0
    return-object v2

    .line 17
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 18
    .local v1, "images":[Lcom/google/android/finsky/protos/Common$Image;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 19
    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-ne v3, p1, :cond_2

    .line 20
    aget-object v2, v1, v0

    goto :goto_0

    .line 18
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
