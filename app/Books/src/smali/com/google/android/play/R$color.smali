.class public final Lcom/google/android/play/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final play_avatar_focused_outline:I = 0x7f0b008d

.field public static final play_avatar_outline:I = 0x7f0b008a

.field public static final play_avatar_pressed_fill:I = 0x7f0b008b

.field public static final play_avatar_pressed_outline:I = 0x7f0b008c

.field public static final play_card_shadow_end_color:I = 0x7f0b0097

.field public static final play_card_shadow_start_color:I = 0x7f0b0096

.field public static final play_disabled_grey:I = 0x7f0b005c

.field public static final play_dismissed_overlay:I = 0x7f0b0084

.field public static final play_fg_primary:I = 0x7f0b0066

.field public static final play_fg_secondary:I = 0x7f0b0067

.field public static final play_header_list_tab_text_color:I = 0x7f0b0126

.field public static final play_header_list_tab_underline_color:I = 0x7f0b008f

.field public static final play_main_background:I = 0x7f0b0060

.field public static final play_onboard__page_indicator_dot_active:I = 0x7f0b009d

.field public static final play_onboard__page_indicator_dot_inactive:I = 0x7f0b009e

.field public static final play_onboard_accent_color_a:I = 0x7f0b00a3

.field public static final play_onboard_accent_color_b:I = 0x7f0b00a4

.field public static final play_onboard_accent_color_c:I = 0x7f0b00a5

.field public static final play_onboard_accent_color_d:I = 0x7f0b00a6

.field public static final play_onboard_app_color:I = 0x7f0b009b

.field public static final play_onboard_app_color_dark:I = 0x7f0b009c

.field public static final play_onboard_simple_quiz_background:I = 0x7f0b00a2

.field public static final play_reason_separator:I = 0x7f0b0061

.field public static final play_search_plate_navigation_button_color:I = 0x7f0b0099

.field public static final play_tab_strip_text_selected:I = 0x7f0b0065

.field public static final play_transparent:I = 0x7f0b005d

.field public static final play_white:I = 0x7f0b0058
