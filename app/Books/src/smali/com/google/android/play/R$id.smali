.class public final Lcom/google/android/play/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_info_container:I = 0x7f0e0169

.field public static final account_name:I = 0x7f0e0162

.field public static final action_bar:I = 0x7f0e0090

.field public static final action_bar_container:I = 0x7f0e008f

.field public static final action_button:I = 0x7f0e018f

.field public static final action_text:I = 0x7f0e0164

.field public static final alt_play_background:I = 0x7f0e016d

.field public static final avatar:I = 0x7f0e00a0

.field public static final background_container:I = 0x7f0e016c

.field public static final caption:I = 0x7f0e017e

.field public static final content_container:I = 0x7f0e0105

.field public static final controls_container:I = 0x7f0e016e

.field public static final cover_photo:I = 0x7f0e0166

.field public static final display_name:I = 0x7f0e016a

.field public static final end_button:I = 0x7f0e0180

.field public static final flm_paddingEnd:I = 0x7f0e001d

.field public static final flm_paddingStart:I = 0x7f0e001c

.field public static final flm_width:I = 0x7f0e001e

.field public static final header_shadow:I = 0x7f0e0175

.field public static final hero_container:I = 0x7f0e016f

.field public static final icon:I = 0x7f0e005c

.field public static final li_badge:I = 0x7f0e015f

.field public static final li_description:I = 0x7f0e00cf

.field public static final li_label:I = 0x7f0e015b

.field public static final li_overflow:I = 0x7f0e00e1

.field public static final li_rating:I = 0x7f0e00ef

.field public static final li_snippet_1:I = 0x7f0e015d

.field public static final li_snippet_2:I = 0x7f0e015c

.field public static final li_snippet_avatar:I = 0x7f0e0160

.field public static final li_snippet_text:I = 0x7f0e0161

.field public static final li_subtitle:I = 0x7f0e00ce

.field public static final li_thumbnail:I = 0x7f0e00cb

.field public static final li_thumbnail_frame:I = 0x7f0e00ca

.field public static final li_title:I = 0x7f0e00cd

.field public static final loading_progress_bar:I = 0x7f0e00f2

.field public static final navigation_button:I = 0x7f0e018b

.field public static final page_indicator:I = 0x7f0e017f

.field public static final pager_tab_strip:I = 0x7f0e0171

.field public static final play_drawer_list:I = 0x7f0e0163

.field public static final play_header_banner:I = 0x7f0e0177

.field public static final play_header_list_tab_container:I = 0x7f0e0173

.field public static final play_header_list_tab_scroll:I = 0x7f0e0172

.field public static final play_header_listview:I = 0x7f0e0022

.field public static final play_header_spacer:I = 0x7f0e0024

.field public static final play_header_toolbar:I = 0x7f0e0176

.field public static final play_header_viewpager:I = 0x7f0e0023

.field public static final play_onboard__OnboardPage_pageId:I = 0x7f0e0026

.field public static final play_onboard__OnboardPage_pageInfo:I = 0x7f0e0027

.field public static final play_onboard__OnboardPagerAdapter_pageGenerator:I = 0x7f0e0025

.field public static final play_onboard__OnboardSimpleQuizPage_title:I = 0x7f0e002c

.field public static final play_onboard__OnboardTutorialPage_backgroundColor:I = 0x7f0e0028

.field public static final play_onboard__OnboardTutorialPage_bodyText:I = 0x7f0e002a

.field public static final play_onboard__OnboardTutorialPage_iconDrawableId:I = 0x7f0e002b

.field public static final play_onboard__OnboardTutorialPage_titleText:I = 0x7f0e0029

.field public static final play_onboard_background:I = 0x7f0e002d

.field public static final play_onboard_drops:I = 0x7f0e017d

.field public static final play_onboard_footer:I = 0x7f0e017c

.field public static final play_onboard_overlay:I = 0x7f0e002e

.field public static final play_onboard_pager:I = 0x7f0e017b

.field public static final play_search_plate:I = 0x7f0e0186

.field public static final play_search_suggestions_list:I = 0x7f0e0187

.field public static final rating_badge_container:I = 0x7f0e015e

.field public static final scroll_proxy:I = 0x7f0e017a

.field public static final search_box_idle_text:I = 0x7f0e018d

.field public static final search_box_text_input:I = 0x7f0e018e

.field public static final secondary_avatar_left:I = 0x7f0e0167

.field public static final secondary_avatar_right:I = 0x7f0e0168

.field public static final splash:I = 0x7f0e0182

.field public static final start_button:I = 0x7f0e00bb

.field public static final suggest_text:I = 0x7f0e0189

.field public static final suggestion_divider:I = 0x7f0e018a

.field public static final suggestion_list_recycler_view:I = 0x7f0e0192

.field public static final swipe_refresh_layout:I = 0x7f0e0179

.field public static final switch_button:I = 0x7f0e0165

.field public static final tab_bar:I = 0x7f0e0170

.field public static final tab_bar_title:I = 0x7f0e0174

.field public static final text_container:I = 0x7f0e018c

.field public static final toggle_account_list_button:I = 0x7f0e016b
