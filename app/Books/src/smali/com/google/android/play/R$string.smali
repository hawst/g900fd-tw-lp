.class public final Lcom/google/android/play/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final play_accessibility_search_plate_back_button:I = 0x7f0f002d

.field public static final play_accessibility_search_plate_clear:I = 0x7f0f002a

.field public static final play_accessibility_search_plate_menu_button:I = 0x7f0f002e

.field public static final play_accessibility_search_plate_voice_search_button:I = 0x7f0f002c

.field public static final play_content_description_hide_account_list_button:I = 0x7f0f0023

.field public static final play_content_description_page_indicator:I = 0x7f0f0027

.field public static final play_content_description_show_account_list_button:I = 0x7f0f0022

.field public static final play_download_is_requested:I = 0x7f0f0025

.field public static final play_download_not_requested:I = 0x7f0f0026

.field public static final play_drawer_close:I = 0x7f0f0021

.field public static final play_drawer_open:I = 0x7f0f0020

.field public static final play_drawer_title:I = 0x7f0f001f

.field public static final play_onboard_button_next:I = 0x7f0f0028

.field public static final play_onboard_interstitial_grid_cell:I = 0x7f0f001e

.field public static final play_percent_downloaded:I = 0x7f0f0024
