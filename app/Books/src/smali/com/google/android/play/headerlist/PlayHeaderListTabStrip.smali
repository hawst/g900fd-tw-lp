.class public Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
.super Landroid/widget/FrameLayout;
.source "PlayHeaderListTabStrip.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;
    }
.end annotation


# instance fields
.field private mAnimateContainerPadding:Z

.field private mEnablePagerScrollSync:Z

.field private mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mLastScrollto:I

.field private mMaxTabWidthPx:I

.field private mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

.field private final mPageListener:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mScrollView:Landroid/widget/HorizontalScrollView;

.field private mSmoothScrollThresholdPx:F

.field private mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

.field private mUseHighContrast:Z

.field private mWatchingAdapter:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/PagerAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPageListener:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mSmoothScrollThresholdPx:F

    .line 72
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;IIZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->scrollToVisualPosition(IIZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateTabs()V

    return-void
.end method

.method private getSelectedTabPosition()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 368
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez v3, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v2

    .line 371
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 372
    .local v1, "visualPosition":I
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v3, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 373
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0
.end method

.method private scrollToVisualPosition(IIZ)V
    .locals 10
    .param p1, "visualPosition"    # I
    .param p2, "extraOffset"    # I
    .param p3, "animate"    # Z

    .prologue
    const/4 v7, 0x0

    .line 292
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    if-nez v8, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v5

    .line 296
    .local v5, "tabStripChildCount":I
    if-eqz v5, :cond_0

    if-ltz p1, :cond_0

    if-ge p1, v5, :cond_0

    .line 301
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v8, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 303
    .local v2, "selectedChild":Landroid/view/View;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    if-eqz v8, :cond_0

    .line 307
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 308
    .local v3, "selectedLeft":I
    add-int v8, v3, p2

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int v6, v8, v9

    .line 310
    .local v6, "targetScrollX":I
    iget v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mLastScrollto:I

    if-eq v6, v8, :cond_0

    .line 313
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v8}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v0

    .line 314
    .local v0, "currentScrollX":I
    sub-int v8, v6, v0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 315
    .local v1, "distance":I
    int-to-float v8, v1

    iget v9, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mSmoothScrollThresholdPx:F

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    const/4 v4, 0x1

    .line 318
    .local v4, "smoothScroll":Z
    :goto_1
    if-eqz v4, :cond_3

    if-eqz p3, :cond_3

    .line 319
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v8, v6, v7}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    .line 323
    :goto_2
    iput v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mLastScrollto:I

    goto :goto_0

    .end local v4    # "smoothScroll":Z
    :cond_2
    move v4, v7

    .line 315
    goto :goto_1

    .line 321
    .restart local v4    # "smoothScroll":Z
    :cond_3
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v8, v6, v7}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_2
.end method

.method private updateAdapter(Landroid/support/v4/view/PagerAdapter;Landroid/support/v4/view/PagerAdapter;)V
    .locals 1
    .param p1, "oldAdapter"    # Landroid/support/v4/view/PagerAdapter;
    .param p2, "newAdapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPageListener:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/PagerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mWatchingAdapter:Ljava/lang/ref/WeakReference;

    .line 139
    :cond_0
    if-eqz p2, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPageListener:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/PagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 141
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mWatchingAdapter:Ljava/lang/ref/WeakReference;

    .line 143
    :cond_1
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateTabs()V

    .line 144
    return-void
.end method

.method private updateTabs()V
    .locals 8

    .prologue
    .line 162
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v6}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->removeAllViews()V

    .line 163
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez v6, :cond_0

    const/4 v0, 0x0

    .line 164
    .local v0, "adapter":Landroid/support/v4/view/PagerAdapter;
    :goto_0
    if-nez v0, :cond_1

    .line 193
    :goto_1
    return-void

    .line 163
    .end local v0    # "adapter":Landroid/support/v4/view/PagerAdapter;
    :cond_0
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    goto :goto_0

    .line 168
    .restart local v0    # "adapter":Landroid/support/v4/view/PagerAdapter;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 169
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v3

    .line 170
    .local v3, "tabCount":I
    const/4 v5, 0x0

    .local v5, "visualPosition":I
    :goto_2
    if-ge v5, v3, :cond_2

    .line 171
    invoke-static {v0, v5}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v2

    .line 172
    .local v2, "logicalPosition":I
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {p0, v1, v6, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->makeTabView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v4

    .line 173
    .local v4, "tabView":Landroid/view/View;
    invoke-virtual {p0, v4, v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->bindTabViewData(Landroid/view/View;Landroid/support/v4/view/PagerAdapter;I)V

    .line 174
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v6, v4}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->addView(Landroid/view/View;)V

    .line 170
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 179
    .end local v2    # "logicalPosition":I
    .end local v4    # "tabView":Landroid/view/View;
    :cond_2
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v6}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    new-instance v7, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;

    invoke-direct {v7, p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    invoke-virtual {v6, v7}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 191
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(Z)V

    .line 192
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v6}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidateTabPadding()V

    goto :goto_1
.end method


# virtual methods
.method protected applyTabContrastMode(Landroid/view/View;Z)V
    .locals 3
    .param p1, "tabView"    # Landroid/view/View;
    .param p2, "useHighContrast"    # Z

    .prologue
    .line 231
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    .line 232
    .local v0, "title":Landroid/widget/TextView;
    if-eqz p2, :cond_0

    .line 233
    sget v1, Lcom/google/android/play/R$drawable;->play_header_list_tab_high_contrast_bg:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 234
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 240
    :goto_0
    return-void

    .line 236
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$color;->play_header_list_tab_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method protected bindTabViewData(Landroid/view/View;Landroid/support/v4/view/PagerAdapter;I)V
    .locals 3
    .param p1, "tabView"    # Landroid/view/View;
    .param p2, "adapter"    # Landroid/support/v4/view/PagerAdapter;
    .param p3, "logicalPosition"    # I

    .prologue
    .line 221
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    .line 222
    .local v0, "title":Landroid/widget/TextView;
    invoke-virtual {p2, p3}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    invoke-static {p2, p3}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 224
    .local v1, "visualPosition":I
    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->makeOnTabClickListener(I)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    return-void
.end method

.method protected getMaxTabWidth()I
    .locals 2

    .prologue
    .line 246
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mMaxTabWidthPx:I

    if-nez v1, :cond_0

    .line 248
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 249
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 250
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mMaxTabWidthPx:I

    .line 252
    .end local v0    # "metrics":Landroid/util/DisplayMetrics;
    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mMaxTabWidthPx:I

    return v1
.end method

.method getSubViewReferences()V
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/google/android/play/R$id;->play_header_list_tab_scroll:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 119
    sget v0, Lcom/google/android/play/R$id;->play_header_list_tab_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    .line 121
    return-void
.end method

.method protected final makeOnTabClickListener(I)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "pagerPosition"    # I

    .prologue
    .line 262
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;I)V

    return-object v0
.end method

.method protected makeTabView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "logicalPosition"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 206
    sget v1, Lcom/google/android/play/R$layout;->play_header_list_tab_text:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 208
    .local v0, "tabView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getMaxTabWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 209
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mUseHighContrast:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->applyTabContrastMode(Landroid/view/View;Z)V

    .line 210
    return-object v0
.end method

.method public notifyPagerAdapterChanged()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 147
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez v2, :cond_1

    move-object v0, v1

    .line 148
    .local v0, "adapter":Landroid/support/v4/view/PagerAdapter;
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mWatchingAdapter:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mWatchingAdapter:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/PagerAdapter;

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateAdapter(Landroid/support/v4/view/PagerAdapter;Landroid/support/v4/view/PagerAdapter;)V

    .line 149
    return-void

    .line 147
    .end local v0    # "adapter":Landroid/support/v4/view/PagerAdapter;
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getSubViewReferences()V

    .line 113
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$color;->play_header_list_tab_underline_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setSelectedIndicatorColor(I)V

    .line 115
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 346
    iget-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mAnimateContainerPadding:Z

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getSelectedTabPosition()I

    move-result v2

    .line 347
    .local v2, "oldPosition":I
    :goto_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 348
    iget-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mAnimateContainerPadding:Z

    if-eqz v4, :cond_0

    .line 349
    invoke-virtual {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(Z)V

    .line 350
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xc

    if-lt v3, v4, :cond_0

    .line 351
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getSelectedTabPosition()I

    move-result v0

    .line 352
    .local v0, "newPosition":I
    if-eq v0, v2, :cond_0

    .line 353
    sub-int v1, v0, v2

    .line 354
    .local v1, "offset":I
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    neg-int v4, v1

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setTranslationX(F)V

    .line 355
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 361
    .end local v0    # "newPosition":I
    .end local v1    # "offset":I
    :cond_0
    return-void

    .end local v2    # "oldPosition":I
    :cond_1
    move v2, v3

    .line 346
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 153
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 154
    .local v0, "width":I
    if-lez v0, :cond_0

    .line 155
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setStripWidth(I)V

    .line 157
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 158
    return-void
.end method

.method setExternalOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "onPageChangeListener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 76
    return-void
.end method

.method setUseFloatingTabPadding(ZZ)V
    .locals 1
    .param p1, "useFloatingTabPadding"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setUseFloatingTabPadding(Z)V

    .line 102
    iput-boolean p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mAnimateContainerPadding:Z

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(Z)V

    .line 104
    return-void
.end method

.method setUseHighContrast(Z)V
    .locals 4
    .param p1, "useHighContrast"    # Z

    .prologue
    .line 84
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mUseHighContrast:Z

    if-eq v2, p1, :cond_0

    .line 85
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mUseHighContrast:Z

    .line 87
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    .line 88
    .local v1, "tabStripChildCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 89
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mUseHighContrast:Z

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->applyTabContrastMode(Landroid/view/View;Z)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    .end local v0    # "i":I
    .end local v1    # "tabStripChildCount":I
    :cond_0
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 127
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    .line 128
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPageListener:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->notifyPagerAdapterChanged()V

    .line 132
    return-void
.end method

.method protected updateSelectedTab(IZ)V
    .locals 4
    .param p1, "visualPosition"    # I
    .param p2, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    .line 337
    invoke-direct {p0, p1, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->scrollToVisualPosition(IIZ)V

    .line 338
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 339
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setSelected(Z)V

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 339
    goto :goto_1

    .line 341
    :cond_1
    return-void
.end method

.method protected updateSelectedTab(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 328
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_0

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 332
    .local v0, "visualPosition":I
    invoke-virtual {p0, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(IZ)V

    goto :goto_0
.end method
