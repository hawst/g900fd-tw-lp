.class public Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
.super Ljava/lang/Object;
.source "PlayDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/drawer/PlayDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlayDrawerDownloadSwitchConfig"
.end annotation


# instance fields
.field public final actionText:Ljava/lang/String;

.field public final checkedTextColor:I

.field public final isChecked:Z

.field public final thumbDrawableId:I

.field public final trackDrawableId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIZ)V
    .locals 0
    .param p1, "actionText"    # Ljava/lang/String;
    .param p2, "checkedTextColor"    # I
    .param p3, "trackDrawableId"    # I
    .param p4, "thumbDrawableId"    # I
    .param p5, "isChecked"    # Z

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->actionText:Ljava/lang/String;

    .line 248
    iput p2, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->checkedTextColor:I

    .line 249
    iput p3, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->trackDrawableId:I

    .line 250
    iput p4, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->thumbDrawableId:I

    .line 251
    iput-boolean p5, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->isChecked:Z

    .line 252
    return-void
.end method
