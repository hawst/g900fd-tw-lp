.class public Lcom/google/android/play/layout/PlayCardViewSmall;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewSmall.java"


# instance fields
.field private mMode:I

.field protected mRatingBadgeContainer:Landroid/view/View;

.field private final mTextContentHeight:I

.field private mVerticalBump:I

.field private final mVerticalBumpLimit:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardViewSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->mMode:I

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 54
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_small_card_content_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTextContentHeight:I

    .line 55
    sget v1, Lcom/google/android/play/R$dimen;->play_card_extra_vspace:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBumpLimit:I

    .line 56
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 62
    sget v0, Lcom/google/android/play/R$id;->rating_badge_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewSmall;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    .line 63
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 42
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v19

    .line 196
    .local v19, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v20

    .line 197
    .local v20, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v21

    .line 198
    .local v21, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v18

    .line 200
    .local v18, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getWidth()I

    move-result v36

    .line 201
    .local v36, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getHeight()I

    move-result v5

    .line 203
    .local v5, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v31

    check-cast v31, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 205
    .local v31, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v30

    .line 206
    .local v30, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v37, v0

    move-object/from16 v0, v31

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v38, v0

    add-int v38, v38, v19

    move-object/from16 v0, v31

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v39, v0

    add-int v39, v39, v21

    move-object/from16 v0, v31

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v40, v0

    add-int v40, v40, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v41

    add-int v40, v40, v41

    move-object/from16 v0, v31

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v41, v0

    add-int v41, v41, v21

    add-int v41, v41, v30

    invoke-virtual/range {v37 .. v41}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v34

    check-cast v34, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 213
    .local v34, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v28

    check-cast v28, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 215
    .local v28, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 217
    .local v23, "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 219
    .local v8, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 221
    .local v15, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 224
    .local v27, "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    add-int v37, v21, v30

    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v37, v37, v38

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    move/from16 v38, v0

    add-int v35, v37, v38

    .line 225
    .local v35, "titleTop":I
    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v37, v0

    add-int v33, v19, v37

    .line 226
    .local v33, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v32

    .line 227
    .local v32, "titleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v38

    add-int v38, v38, v33

    add-int v39, v35, v32

    move-object/from16 v0, v37

    move/from16 v1, v33

    move/from16 v2, v35

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 230
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v37, v0

    add-int v17, v35, v37

    .line 231
    .local v17, "overflowTop":I
    sub-int v37, v36, v20

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v38, v0

    sub-int v16, v37, v38

    .line 232
    .local v16, "overflowRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v38

    sub-int v38, v16, v38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v39

    add-int v39, v39, v17

    move-object/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v17

    move/from16 v3, v16

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 237
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    move/from16 v37, v0

    const/16 v38, 0x0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 240
    .local v6, "innerVerticalBump":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v7

    .line 241
    .local v7, "labelHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mMode:I

    move/from16 v37, v0

    if-nez v37, :cond_2

    sub-int v37, v5, v18

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v38, v0

    sub-int v37, v37, v38

    sub-int v37, v37, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    move/from16 v38, v0

    sub-int v10, v37, v38

    .line 244
    .local v10, "labelTop":I
    :goto_0
    sub-int v37, v36, v20

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v38, v0

    sub-int v9, v37, v38

    .line 245
    .local v9, "labelRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v38

    sub-int v38, v9, v38

    add-int v39, v10, v7

    move-object/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v39

    invoke-virtual {v0, v1, v10, v9, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v37

    const/16 v38, 0x8

    move/from16 v0, v37

    move/from16 v1, v38

    if-eq v0, v1, :cond_0

    .line 250
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mMode:I

    move/from16 v37, v0

    if-nez v37, :cond_3

    add-int v37, v35, v32

    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v38, v0

    add-int v37, v37, v38

    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v37, v37, v38

    add-int v29, v37, v6

    .line 254
    .local v29, "subtitleTop":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v38

    add-int v38, v38, v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v39

    add-int v39, v39, v29

    move-object/from16 v0, v37

    move/from16 v1, v33

    move/from16 v2, v29

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 259
    .end local v29    # "subtitleTop":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v37

    add-int v37, v37, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getBaseline()I

    move-result v38

    sub-int v24, v37, v38

    .line 260
    .local v24, "ratingBadgeTop":I
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v37, v0

    add-int v22, v19, v37

    .line 261
    .local v22, "ratingBadgeLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getMeasuredWidth()I

    move-result v38

    add-int v38, v38, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/view/View;->getMeasuredHeight()I

    move-result v39

    add-int v39, v39, v24

    move-object/from16 v0, v37

    move/from16 v1, v22

    move/from16 v2, v24

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v37

    const/16 v38, 0x8

    move/from16 v0, v37

    move/from16 v1, v38

    if-eq v0, v1, :cond_1

    .line 266
    sub-int v37, v5, v18

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v38, v0

    sub-int v37, v37, v38

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    move/from16 v38, v0

    sub-int v25, v37, v38

    .line 267
    .local v25, "snippetBottom":I
    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v37, v0

    add-int v26, v19, v37

    .line 268
    .local v26, "snippetLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredHeight()I

    move-result v38

    sub-int v38, v25, v38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredWidth()I

    move-result v39

    add-int v39, v39, v26

    move-object/from16 v0, v37

    move/from16 v1, v26

    move/from16 v2, v38

    move/from16 v3, v39

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardSnippet;->layout(IIII)V

    .line 272
    .end local v25    # "snippetBottom":I
    .end local v26    # "snippetLeft":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    .line 273
    .local v14, "loadingWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 274
    .local v11, "loadingHeight":I
    sub-int v37, v36, v19

    sub-int v37, v37, v20

    sub-int v37, v37, v14

    div-int/lit8 v37, v37, 0x2

    add-int v12, v19, v37

    .line 275
    .local v12, "loadingLeft":I
    sub-int v37, v5, v21

    sub-int v37, v37, v18

    sub-int v37, v37, v11

    div-int/lit8 v37, v37, 0x2

    add-int v13, v21, v37

    .line 276
    .local v13, "loadingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getMeasuredWidth()I

    move-result v38

    add-int v38, v38, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/view/View;->getMeasuredHeight()I

    move-result v39

    add-int v39, v39, v13

    move-object/from16 v0, v37

    move/from16 v1, v38

    move/from16 v2, v39

    invoke-virtual {v0, v12, v13, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 280
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->recomputeOverflowAreaIfNeeded()V

    .line 281
    return-void

    .line 241
    .end local v9    # "labelRight":I
    .end local v10    # "labelTop":I
    .end local v11    # "loadingHeight":I
    .end local v12    # "loadingLeft":I
    .end local v13    # "loadingTop":I
    .end local v14    # "loadingWidth":I
    .end local v22    # "ratingBadgeLeft":I
    .end local v24    # "ratingBadgeTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/widget/TextView;->getBottom()I

    move-result v37

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v37, v37, v38

    add-int v10, v37, v6

    goto/16 :goto_0

    .line 250
    .restart local v9    # "labelRight":I
    .restart local v10    # "labelTop":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v37

    add-int v37, v37, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/play/layout/PlayTextView;->getBaseline()I

    move-result v38

    sub-int v29, v37, v38

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 31
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 83
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/PlayCardViewSmall;->measureThumbnailSpanningWidth(I)V

    .line 85
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v26

    .line 86
    .local v26, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v10

    .line 87
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v11

    .line 88
    .local v11, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v12

    .line 89
    .local v12, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v9

    .line 90
    .local v9, "paddingBottom":I
    sub-int v27, v26, v10

    sub-int v2, v27, v11

    .line 92
    .local v2, "contentWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 94
    .local v21, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 95
    .local v4, "heightSpecMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 100
    .local v5, "heightSpecSize":I
    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v27, v0

    add-int v27, v27, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTextContentHeight:I

    move/from16 v28, v0

    div-int/lit8 v29, v2, 0x2

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->max(II)I

    move-result v28

    add-int v27, v27, v28

    add-int v6, v27, v9

    .line 102
    .local v6, "idealHeight":I
    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v27

    if-ne v4, v0, :cond_3

    if-lez v5, :cond_3

    move v3, v5

    .line 105
    .local v3, "height":I
    :goto_0
    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v27, v2, v27

    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v22, v27, v28

    .line 106
    .local v22, "thumbnailWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v22

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v29, v0

    const/high16 v30, 0x40000000    # 2.0f

    invoke-static/range {v29 .. v30}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    invoke-virtual/range {v27 .. v29}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 111
    .local v23, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 113
    .local v18, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 115
    .local v7, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 117
    .local v13, "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 121
    .local v15, "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v27, v0

    const/high16 v28, -0x80000000

    move/from16 v0, v28

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v27

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    add-int v27, v27, v28

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    add-int v8, v27, v28

    .line 126
    .local v8, "labelWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Landroid/widget/ImageView;->measure(II)V

    .line 129
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v27, v2, v27

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v24, v27, v28

    .line 130
    .local v24, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Landroid/widget/TextView;->measure(II)V

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getVisibility()I

    move-result v27

    if-eqz v27, :cond_7

    .line 135
    sub-int v27, v2, v8

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v14, v27, v28

    .line 137
    .local v14, "ratingBadgeWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mRatingBadgeContainer:Landroid/view/View;

    move-object/from16 v27, v0

    const/high16 v28, -0x80000000

    move/from16 v0, v28

    invoke-static {v14, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Landroid/view/View;->measure(II)V

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v27

    const/16 v28, 0x8

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_0

    .line 144
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mMode:I

    move/from16 v27, v0

    if-nez v27, :cond_4

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v27, v2, v27

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v17, v27, v28

    .line 150
    .local v17, "subtitleAvailableWidth":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v19

    .line 152
    .local v19, "subtitleMeasuredWidth":I
    move/from16 v0, v19

    move/from16 v1, v17

    if-le v0, v1, :cond_0

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 158
    .end local v17    # "subtitleAvailableWidth":I
    .end local v19    # "subtitleMeasuredWidth":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v27

    const/16 v28, 0x8

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 159
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v27, v2, v27

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v16, v27, v28

    .line 160
    .local v16, "snippetWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v16

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/high16 v29, 0x40000000    # 2.0f

    invoke-virtual/range {v27 .. v29}, Lcom/google/android/play/layout/PlayCardSnippet;->measure(II)V

    .line 164
    .end local v16    # "snippetWidth":I
    :cond_1
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v28

    add-int v27, v27, v28

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v28, v0

    add-int v27, v27, v28

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v28, v0

    add-int v27, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v28

    add-int v27, v27, v28

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v28, v0

    add-int v20, v27, v28

    .line 167
    .local v20, "textContentHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mMode:I

    move/from16 v27, v0

    if-nez v27, :cond_5

    .line 168
    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v28

    add-int v27, v27, v28

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v20, v20, v27

    .line 175
    :cond_2
    :goto_2
    sub-int v27, v3, v12

    sub-int v27, v27, v9

    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    sub-int v25, v27, v20

    .line 181
    .local v25, "verticalGap":I
    if-gtz v25, :cond_6

    div-int/lit8 v27, v25, 0x2

    :goto_3
    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    .line 188
    .end local v14    # "ratingBadgeWidth":I
    .end local v20    # "textContentHeight":I
    .end local v25    # "verticalGap":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Landroid/view/View;->measure(II)V

    .line 190
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Lcom/google/android/play/layout/PlayCardViewSmall;->setMeasuredDimension(II)V

    .line 191
    return-void

    .end local v3    # "height":I
    .end local v7    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v8    # "labelWidth":I
    .end local v13    # "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v15    # "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v22    # "thumbnailWidth":I
    .end local v23    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v24    # "titleWidth":I
    :cond_3
    move v3, v6

    .line 102
    goto/16 :goto_0

    .line 144
    .restart local v3    # "height":I
    .restart local v7    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v8    # "labelWidth":I
    .restart local v13    # "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v14    # "ratingBadgeWidth":I
    .restart local v15    # "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v18    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v22    # "thumbnailWidth":I
    .restart local v23    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v24    # "titleWidth":I
    :cond_4
    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v27, v2, v27

    sub-int v17, v27, v8

    goto/16 :goto_1

    .line 170
    .restart local v20    # "textContentHeight":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v27

    const/16 v28, 0x8

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    .line 171
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredHeight()I

    move-result v28

    add-int v27, v27, v28

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v20, v20, v27

    goto :goto_2

    .line 181
    .restart local v25    # "verticalGap":I
    :cond_6
    div-int/lit8 v27, v25, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBumpLimit:I

    move/from16 v28, v0

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->min(II)I

    move-result v27

    goto :goto_3

    .line 185
    .end local v14    # "ratingBadgeWidth":I
    .end local v20    # "textContentHeight":I
    .end local v25    # "verticalGap":I
    :cond_7
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/play/layout/PlayCardViewSmall;->mVerticalBump:I

    goto :goto_4
.end method
