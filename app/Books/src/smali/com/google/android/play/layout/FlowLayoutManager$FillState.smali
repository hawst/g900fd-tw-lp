.class final Lcom/google/android/play/layout/FlowLayoutManager$FillState;
.super Ljava/lang/Object;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FillState"
.end annotation


# instance fields
.field mHeightFilled:I

.field mNextAnchorPosition:I

.field mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

.field mNextItemChildIndex:I

.field mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

.field mNextItemPosition:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/layout/FlowLayoutManager$1;

    .prologue
    .line 2222
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;-><init>()V

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2263
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    .line 2264
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    .line 2266
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemPosition:I

    .line 2267
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemChildIndex:I

    .line 2268
    iput-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2269
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-eqz v0, :cond_0

    .line 2270
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->recycle()V

    .line 2271
    iput-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 2273
    :cond_0
    return-void
.end method

.method public takeNextItem()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .locals 2

    .prologue
    .line 2257
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 2258
    .local v0, "nextItem":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 2259
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FillState{mHeightFilled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mNextAnchorPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mNextItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mNextItemPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemPosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mNextItemChildIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemChildIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "notnull"

    goto :goto_0
.end method
