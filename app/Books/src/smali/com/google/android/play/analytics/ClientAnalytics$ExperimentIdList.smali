.class public final Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExperimentIdList"
.end annotation


# instance fields
.field public id:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1612
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1613
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->clear()Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    .line 1614
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;
    .locals 1

    .prologue
    .line 1617
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    .line 1618
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->cachedSize:I

    .line 1619
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1638
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1639
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 1640
    const/4 v0, 0x0

    .line 1641
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1642
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 1643
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1644
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1645
    add-int/lit8 v0, v0, 0x1

    .line 1646
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1642
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1650
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 1651
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1653
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1662
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1666
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1667
    :sswitch_0
    return-object p0

    .line 1672
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1674
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 1675
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1676
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1677
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1679
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1680
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1681
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1679
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1674
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 1684
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1685
    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    goto :goto_0

    .line 1662
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1592
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1625
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1626
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1627
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;->id:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1628
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1629
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1626
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1633
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1634
    return-void
.end method
