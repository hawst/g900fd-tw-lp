.class public Lcom/google/android/play/layout/PlayActionButton;
.super Landroid/widget/Button;
.source "PlayActionButton.java"


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 100
    const-string v0, "Don\'t call PlayActionButton.setOnClickListener() directly"

    .line 101
    .local v0, "error":Ljava/lang/String;
    const-string v1, "PlayCommon"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v1, "PlayCommon"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Call PlayActionButton.configure()"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
