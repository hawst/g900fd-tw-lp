.class public Lcom/google/android/play/utils/PlayCommonNetworkStack;
.super Ljava/lang/Object;
.source "PlayCommonNetworkStack.java"


# instance fields
.field private final mBitmapCache:Lcom/android/volley/Cache;

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

.field private final mContext:Landroid/content/Context;

.field private final mDfeCache:Lcom/android/volley/Cache;

.field private final mDfeRequestQueue:Lcom/android/volley/RequestQueue;

.field private mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

.field private final mPlayDfeApis:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/dfe/api/PlayDfeApi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkInfoCache"    # Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/utils/PlayCommonNetworkStack;-><init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;Z)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;Z)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkInfoCache"    # Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;
    .param p3, "clearCache"    # Z

    .prologue
    const/4 v7, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/play/utils/config/GservicesValue;->init(Landroid/content/Context;[Ljava/lang/String;)V

    .line 75
    invoke-static {p2}, Lcom/google/android/play/image/FifeUrlUtils;->setNetworkInfoCacheInstance(Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    .line 77
    iput-object p1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mContext:Landroid/content/Context;

    .line 80
    new-instance v2, Lcom/android/volley/toolbox/DiskBasedCache;

    const-string v1, "play_common_main"

    invoke-direct {p0, p1, v1}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    mul-int/lit16 v1, v1, 0x400

    invoke-direct {v2, v3, v1}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    iput-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeCache:Lcom/android/volley/Cache;

    .line 83
    if-eqz p3, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeCache:Lcom/android/volley/Cache;

    invoke-interface {v1}, Lcom/android/volley/Cache;->clear()V

    .line 87
    :cond_0
    new-instance v2, Lcom/android/volley/RequestQueue;

    iget-object v3, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeCache:Lcom/android/volley/Cache;

    new-instance v4, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v5, Lcom/google/android/volley/GoogleHttpClientStack;

    invoke-direct {v5, p1, v7}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    new-instance v6, Lcom/android/volley/toolbox/ByteArrayPool;

    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    invoke-direct {v6, v1}, Lcom/android/volley/toolbox/ByteArrayPool;-><init>(I)V

    invoke-direct {v4, v5, v6}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;Lcom/android/volley/toolbox/ByteArrayPool;)V

    const/4 v1, 0x2

    invoke-direct {v2, v3, v4, v1}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;I)V

    iput-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeRequestQueue:Lcom/android/volley/RequestQueue;

    .line 90
    iget-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1}, Lcom/android/volley/RequestQueue;->start()V

    .line 91
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApis:Ljava/util/Map;

    .line 94
    new-instance v2, Lcom/android/volley/toolbox/DiskBasedCache;

    const-string v1, "play_common_images"

    invoke-direct {p0, p1, v1}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    mul-int/lit16 v1, v1, 0x400

    invoke-direct {v2, v3, v1}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    iput-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapCache:Lcom/android/volley/Cache;

    .line 97
    if-eqz p3, :cond_1

    .line 98
    iget-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapCache:Lcom/android/volley/Cache;

    invoke-interface {v1}, Lcom/android/volley/Cache;->clear()V

    .line 101
    :cond_1
    new-instance v2, Lcom/android/volley/RequestQueue;

    iget-object v3, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapCache:Lcom/android/volley/Cache;

    new-instance v4, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v5, Lcom/google/android/volley/GoogleHttpClientStack;

    invoke-direct {v5, p1, v7}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    new-instance v6, Lcom/android/volley/toolbox/ByteArrayPool;

    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    invoke-direct {v6, v1}, Lcom/android/volley/toolbox/ByteArrayPool;-><init>(I)V

    invoke-direct {v4, v5, v6}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;Lcom/android/volley/toolbox/ByteArrayPool;)V

    invoke-direct {v2, v3, v4}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;)V

    iput-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    .line 104
    iget-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1}, Lcom/android/volley/RequestQueue;->start()V

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 106
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    new-instance v1, Lcom/google/android/play/image/BitmapLoader;

    iget-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v5, Lcom/google/android/play/image/TentativeGcRunner;

    invoke-direct {v5}, Lcom/google/android/play/image/TentativeGcRunner;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/play/image/BitmapLoader;-><init>(Lcom/android/volley/RequestQueue;IILcom/google/android/play/image/TentativeGcRunner;)V

    iput-object v1, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 108
    return-void
.end method

.method private getCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "suffix"    # Ljava/lang/String;

    .prologue
    .line 150
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 151
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 152
    return-object v0
.end method


# virtual methods
.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    return-object v0
.end method

.method public declared-synchronized getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApis:Ljava/util/Map;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/dfe/api/PlayDfeApi;

    .line 116
    .local v1, "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    if-nez v1, :cond_1

    .line 117
    iget-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeCache:Lcom/android/volley/Cache;

    invoke-static {v2, v3, p1}, Lcom/google/android/play/dfe/api/PlayDfeApiContext;->create(Landroid/content/Context;Lcom/android/volley/Cache;Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    move-result-object v0

    .line 120
    .local v0, "playDfeApiContext":Lcom/google/android/play/dfe/api/PlayDfeApiContext;
    sget-boolean v2, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 121
    const-string v2, "Created new PlayDfeApiContext: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/play/utils/PlayCommonLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    :cond_0
    new-instance v1, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;

    .end local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    iget-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mDfeRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-direct {v1, v2, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;-><init>(Lcom/android/volley/RequestQueue;Lcom/google/android/play/dfe/api/PlayDfeApiContext;)V

    .line 124
    .restart local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    iget-object v2, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApis:Ljava/util/Map;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    .end local v0    # "playDfeApiContext":Lcom/google/android/play/dfe/api/PlayDfeApiContext;
    :cond_1
    monitor-exit p0

    return-object v1

    .line 115
    .end local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Lcom/google/android/play/utils/PlayCommonNetworkStack$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/utils/PlayCommonNetworkStack$1;-><init>(Lcom/google/android/play/utils/PlayCommonNetworkStack;)V

    iput-object v0, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/utils/PlayCommonNetworkStack;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
