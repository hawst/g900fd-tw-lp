.class public interface abstract Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
.super Ljava/lang/Object;
.source "PlayDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/drawer/PlayDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlayDrawerContentClickListener"
.end annotation


# virtual methods
.method public abstract onAccountListToggleButtonClicked(Z)V
.end method

.method public abstract onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
.end method

.method public abstract onDownloadToggleClicked(Z)V
.end method

.method public abstract onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
.end method

.method public abstract onSecondaryAccountClicked(Ljava/lang/String;)Z
.end method

.method public abstract onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
.end method
