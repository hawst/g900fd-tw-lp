.class public Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;
.super Ljava/lang/Object;
.source "PlayHeaderScrollableContentListener.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;


# instance fields
.field private mAbsoluteY:I

.field private mAdapter:Landroid/widget/Adapter;

.field private final mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final mObserver:Landroid/database/DataSetObserver;

.field protected mScrollState:I


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1
    .param p1, "layout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    .line 30
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 31
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private reset(Z)V
    .locals 1
    .param p1, "resetAdapter"    # Z

    .prologue
    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 67
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mScrollState:I

    .line 68
    return-void
.end method

.method private updateAdapter(Landroid/widget/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-ne v0, p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 78
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    .line 79
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 82
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onScrollStateChanged(Lcom/google/android/play/headerlist/PlayScrollableContentView;I)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/play/headerlist/PlayScrollableContentView;
    .param p2, "newState"    # I

    .prologue
    .line 87
    invoke-interface {p1}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 88
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mScrollState:I

    .line 89
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScrollStateChanged(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrollStateChanged(Lcom/google/android/play/headerlist/PlayScrollableContentView;I)V

    .line 93
    :cond_0
    return-void
.end method

.method public onScrolled(Lcom/google/android/play/headerlist/PlayScrollableContentView;II)V
    .locals 3
    .param p1, "view"    # Lcom/google/android/play/headerlist/PlayScrollableContentView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 97
    invoke-interface {p1}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 98
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    .line 105
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mScrollState:I

    invoke-interface {p1}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->getItemCount()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v2, p3, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScroll(III)V

    .line 106
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrolled(Lcom/google/android/play/headerlist/PlayScrollableContentView;II)V

    .line 109
    :cond_0
    return-void

    .line 102
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    goto :goto_0

    .line 105
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    goto :goto_1
.end method

.method reset()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    .line 56
    return-void
.end method
