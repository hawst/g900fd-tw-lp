.class Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
.super Landroid/widget/FrameLayout;
.source "PlayDrawerProfileInfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;
    }
.end annotation


# instance fields
.field private mAccountInfoContainer:Landroid/view/View;

.field private mAccountListEnabled:Z

.field private mAccountListExpanded:Z

.field private mAccountName:Landroid/widget/TextView;

.field private mDisplayName:Landroid/widget/TextView;

.field private mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

.field private mProfileAccount:Landroid/accounts/Account;

.field private mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mSecondaryAccountLeft:Landroid/accounts/Account;

.field private mSecondaryAccountRight:Landroid/accounts/Account;

.field private mSecondaryAvatarImageLeft:Lcom/google/android/play/image/FifeImageView;

.field private mSecondaryAvatarImageRight:Lcom/google/android/play/image/FifeImageView;

.field private mToggleAccountListButton:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    return-void
.end method

.method private bindAccountToggler()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 212
    .local v0, "res":Landroid/content/res/Resources;
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/play/R$drawable;->ic_up_white_16:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 215
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/play/R$string;->play_content_description_hide_account_list_button:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 220
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/play/R$drawable;->ic_down_white_16:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 221
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/play/R$string;->play_content_description_show_account_list_button:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private configureAvatar(Lcom/google/android/play/image/FifeImageView;ZLcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 3
    .param p1, "avatarImage"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "showAvatar"    # Z
    .param p3, "accountDocV2"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 162
    if-eqz p2, :cond_1

    .line 163
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 164
    if-eqz p3, :cond_0

    .line 165
    const/4 v1, 0x4

    invoke-static {p3, v1}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 170
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    invoke-virtual {p1}, Lcom/google/android/play/image/FifeImageView;->clearCachedState()V

    .line 171
    iget-object v1, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {p1, v1, v2, p4}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 178
    .end local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getDefaultAvatar()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 176
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private getDefaultAvatar()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$drawable;->ic_profile_none:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 187
    .local v0, "avatarBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public configure(Landroid/accounts/Account;[Landroid/accounts/Account;Ljava/util/Map;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 10
    .param p1, "currentAccount"    # Landroid/accounts/Account;
    .param p2, "nonCurrentAccounts"    # [Landroid/accounts/Account;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "[",
            "Landroid/accounts/Account;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/protos/DocumentV2$DocV2;",
            ">;",
            "Lcom/google/android/play/image/BitmapLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "accountDocV2s":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$DocV2;>;"
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 117
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAccount:Landroid/accounts/Account;

    .line 118
    array-length v5, p2

    if-lez v5, :cond_0

    aget-object v5, p2, v8

    :goto_0
    iput-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountRight:Landroid/accounts/Account;

    .line 119
    array-length v5, p2

    if-le v5, v7, :cond_1

    aget-object v5, p2, v7

    :goto_1
    iput-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountLeft:Landroid/accounts/Account;

    .line 121
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 122
    .local v0, "profileDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountRight:Landroid/accounts/Account;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountRight:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object v2, v5

    .line 124
    .local v2, "secondaryAccountRightDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :goto_2
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountLeft:Landroid/accounts/Account;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountLeft:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object v1, v5

    .line 127
    .local v1, "secondaryAccountLeftDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :goto_3
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    sget v6, Lcom/google/android/play/R$color;->play_main_background:I

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    if-nez v0, :cond_4

    .line 130
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    sget v6, Lcom/google/android/play/R$drawable;->bg_default_profile_art:I

    invoke-virtual {v5, v6}, Lcom/google/android/play/image/FifeImageView;->setImageResource(I)V

    .line 131
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    :goto_4
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-direct {p0, v5, v7, v0, p4}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configureAvatar(Lcom/google/android/play/image/FifeImageView;ZLcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/play/image/BitmapLoader;)V

    .line 154
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageLeft:Lcom/google/android/play/image/FifeImageView;

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountLeft:Landroid/accounts/Account;

    if-eqz v5, :cond_6

    move v5, v7

    :goto_5
    invoke-direct {p0, v6, v5, v1, p4}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configureAvatar(Lcom/google/android/play/image/FifeImageView;ZLcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/play/image/BitmapLoader;)V

    .line 156
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageRight:Lcom/google/android/play/image/FifeImageView;

    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountRight:Landroid/accounts/Account;

    if-eqz v6, :cond_7

    :goto_6
    invoke-direct {p0, v5, v7, v2, p4}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configureAvatar(Lcom/google/android/play/image/FifeImageView;ZLcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/play/image/BitmapLoader;)V

    .line 158
    return-void

    .end local v0    # "profileDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "secondaryAccountLeftDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v2    # "secondaryAccountRightDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_0
    move-object v5, v6

    .line 118
    goto :goto_0

    :cond_1
    move-object v5, v6

    .line 119
    goto :goto_1

    .restart local v0    # "profileDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_2
    move-object v2, v6

    .line 122
    goto :goto_2

    .restart local v2    # "secondaryAccountRightDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_3
    move-object v1, v6

    .line 124
    goto :goto_3

    .line 137
    .restart local v1    # "secondaryAccountLeftDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->clearCachedState()V

    .line 139
    const/16 v5, 0xf

    invoke-static {v0, v5}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v3

    .line 141
    .local v3, "selectedProfileCoverImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 143
    .local v4, "selectedProfileDisplayName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5, p0}, Lcom/google/android/play/image/FifeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 144
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v6, v3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v9, v3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v5, v6, v9, p4}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 146
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 147
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_5
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .end local v3    # "selectedProfileCoverImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v4    # "selectedProfileDisplayName":Ljava/lang/String;
    :cond_6
    move v5, v8

    .line 154
    goto :goto_5

    :cond_7
    move v7, v8

    .line 156
    goto :goto_6
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->invalidate()V

    .line 256
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    if-nez v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;->onAvatarClicked(Landroid/accounts/Account;)V

    goto :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageLeft:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_3

    .line 204
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountLeft:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;->onAvatarClicked(Landroid/accounts/Account;)V

    goto :goto_0

    .line 205
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageRight:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAccountRight:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;->onAvatarClicked(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 76
    sget v0, Lcom/google/android/play/R$id;->cover_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 77
    sget v0, Lcom/google/android/play/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    .line 78
    sget v0, Lcom/google/android/play/R$id;->secondary_avatar_left:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageLeft:Lcom/google/android/play/image/FifeImageView;

    .line 79
    sget v0, Lcom/google/android/play/R$id;->secondary_avatar_right:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageRight:Lcom/google/android/play/image/FifeImageView;

    .line 80
    sget v0, Lcom/google/android/play/R$id;->display_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    .line 81
    sget v0, Lcom/google/android/play/R$id;->account_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    .line 82
    sget v0, Lcom/google/android/play/R$id;->toggle_account_list_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageView;

    .line 83
    sget v0, Lcom/google/android/play/R$id;->account_info_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageLeft:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mSecondaryAvatarImageRight:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "coverImageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 260
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 1
    .param p1, "coverImageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 267
    return-void
.end method

.method public setAccountListEnabled(Z)V
    .locals 1
    .param p1, "accountListEnabled"    # Z

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    if-eq v0, p1, :cond_0

    .line 92
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    .line 93
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->bindAccountToggler()V

    .line 94
    if-nez p1, :cond_0

    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 100
    :cond_0
    return-void
.end method

.method public setAccountListExpanded(Z)V
    .locals 1
    .param p1, "accountListExpanded"    # Z

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    if-eq v0, p1, :cond_0

    .line 104
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    .line 105
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->bindAccountToggler()V

    .line 107
    :cond_0
    return-void
.end method

.method public setAccountTogglerListener(Landroid/view/View$OnClickListener;)V
    .locals 6
    .param p1, "accountTogglerListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 231
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 232
    .local v1, "paddingLeft":I
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 233
    .local v2, "paddingRight":I
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 234
    .local v3, "paddingTop":I
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 236
    .local v0, "paddingBottom":I
    if-eqz p1, :cond_0

    .line 237
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    sget v5, Lcom/google/android/play/R$drawable;->play_highlight_overlay_dark:I

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 243
    :goto_0
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 245
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    invoke-virtual {v4, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    return-void

    .line 239
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountInfoContainer:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setOnAvatarClickListener(Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;)V
    .locals 0
    .param p1, "onAvatarClickedListener"    # Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mOnAvatarClickedListener:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;

    .line 193
    return-void
.end method
