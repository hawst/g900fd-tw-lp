.class Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
.super Ljava/lang/Object;
.source "PlayImageProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/imageview/PlayImageProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ManagedBitmapsKey"
.end annotation


# instance fields
.field public final imageToken:Ljava/lang/Object;

.field public final requestedHeight:I

.field public final requestedWidth:I


# direct methods
.method private constructor <init>(Lcom/google/android/play/imageview/PlayManagedBitmap;)V
    .locals 1
    .param p1, "bitmap"    # Lcom/google/android/play/imageview/PlayManagedBitmap;

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    iget-object v0, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    .line 381
    iget v0, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->requestedHeight:I

    iput v0, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedHeight:I

    .line 382
    iget v0, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->requestedWidth:I

    iput v0, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedWidth:I

    .line 383
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/imageview/PlayManagedBitmap;Lcom/google/android/play/imageview/PlayImageProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/imageview/PlayManagedBitmap;
    .param p2, "x1"    # Lcom/google/android/play/imageview/PlayImageProvider$1;

    .prologue
    .line 368
    invoke-direct {p0, p1}, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;-><init>(Lcom/google/android/play/imageview/PlayManagedBitmap;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;II)V
    .locals 0
    .param p1, "imageToken"    # Ljava/lang/Object;
    .param p2, "requestedWidth"    # I
    .param p3, "requestedHeight"    # I

    .prologue
    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    iput-object p1, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    .line 375
    iput p2, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedWidth:I

    .line 376
    iput p3, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedHeight:I

    .line 377
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;IILcom/google/android/play/imageview/PlayImageProvider$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/android/play/imageview/PlayImageProvider$1;

    .prologue
    .line 368
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;-><init>(Ljava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 387
    if-ne p0, p1, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v1

    .line 388
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 390
    check-cast v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;

    .line 392
    .local v0, "that":Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    iget v3, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedHeight:I

    iget v4, v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedHeight:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 393
    :cond_4
    iget v3, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedWidth:I

    iget v4, v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedWidth:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 394
    :cond_5
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 396
    goto :goto_0

    .line 394
    :cond_6
    iget-object v3, v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedWidth:I

    .line 405
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->requestedHeight:I

    add-int v0, v1, v2

    .line 406
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 407
    return v0

    .line 406
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
