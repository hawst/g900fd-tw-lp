.class public Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/image/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BitmapCacheEntry"
.end annotation


# instance fields
.field public bitmap:Landroid/graphics/Bitmap;

.field public requestedHeight:I

.field public requestedWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "requestedWidth"    # I
    .param p3, "requestedHeight"    # I

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    .line 140
    iput p2, p0, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedWidth:I

    .line 141
    iput p3, p0, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedHeight:I

    .line 142
    return-void
.end method
