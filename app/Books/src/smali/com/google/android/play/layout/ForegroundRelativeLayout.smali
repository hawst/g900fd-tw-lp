.class public Lcom/google/android/play/layout/ForegroundRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "ForegroundRelativeLayout.java"


# static fields
.field private static IS_HC_OR_ABOVE:Z

.field private static IS_JBMR1_OR_ABOVE:Z


# instance fields
.field private mForegroundBoundsChanged:Z

.field private mForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mForegroundPaddingBottom:I

.field private mForegroundPaddingLeft:I

.field private mForegroundPaddingRight:I

.field private mForegroundPaddingTop:I

.field private final mOverlayBounds:Landroid/graphics/Rect;

.field private final mSelfBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->IS_HC_OR_ABOVE:Z

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/layout/ForegroundRelativeLayout;->IS_JBMR1_OR_ABOVE:Z

    return-void

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v1, v2

    .line 25
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingLeft:I

    .line 30
    iput v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingTop:I

    .line 31
    iput v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingRight:I

    .line 32
    iput v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingBottom:I

    .line 34
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 35
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 36
    iput-boolean v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundBoundsChanged:Z

    .line 49
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010109

    aput v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 52
    .local v1, "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 55
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 164
    iget-object v6, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 166
    .local v6, "foreground":Landroid/graphics/drawable/Drawable;
    iget-boolean v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundBoundsChanged:Z

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundBoundsChanged:Z

    .line 168
    iget-object v3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 169
    .local v3, "selfBounds":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 171
    .local v4, "overlayBounds":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->getWidth()I

    move-result v8

    .line 172
    .local v8, "w":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->getHeight()I

    move-result v7

    .line 174
    .local v7, "h":I
    iget v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingLeft:I

    iget v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingTop:I

    iget v2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingRight:I

    sub-int v2, v8, v2

    iget v9, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingBottom:I

    sub-int v9, v7, v9

    invoke-virtual {v3, v0, v1, v2, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 178
    sget-boolean v0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->IS_JBMR1_OR_ABOVE:Z

    if-eqz v0, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->getLayoutDirection()I

    move-result v5

    .line 180
    .local v5, "layoutDirection":I
    const/16 v0, 0x77

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/GravityCompat;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 186
    .end local v5    # "layoutDirection":I
    :goto_0
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 189
    .end local v3    # "selfBounds":Landroid/graphics/Rect;
    .end local v4    # "overlayBounds":Landroid/graphics/Rect;
    .end local v7    # "h":I
    .end local v8    # "w":I
    :cond_0
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 191
    .end local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void

    .line 184
    .restart local v3    # "selfBounds":Landroid/graphics/Rect;
    .restart local v4    # "overlayBounds":Landroid/graphics/Rect;
    .restart local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "h":I
    .restart local v8    # "w":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 129
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->drawableHotspotChanged(FF)V

    .line 131
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 134
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/RelativeLayout;->drawableStateChanged()V

    .line 121
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 124
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 112
    invoke-super {p0}, Landroid/widget/RelativeLayout;->jumpDrawablesToCurrentState()V

    .line 113
    sget-boolean v0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 116
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 148
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundBoundsChanged:Z

    .line 151
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 155
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundBoundsChanged:Z

    .line 158
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, 0x0

    .line 59
    iget-object v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_3

    .line 60
    iget-object v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 62
    iget-object v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 66
    iput v3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingLeft:I

    .line 67
    iput v3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingTop:I

    .line 68
    iput v3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingRight:I

    .line 69
    iput v3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingBottom:I

    .line 71
    if-eqz p1, :cond_4

    .line 72
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setWillNotDraw(Z)V

    .line 73
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 74
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 77
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 78
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingLeft:I

    .line 80
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingTop:I

    .line 81
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingRight:I

    .line 82
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingBottom:I

    .line 87
    .end local v0    # "padding":Landroid/graphics/Rect;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->requestLayout()V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->invalidate()V

    .line 90
    :cond_3
    return-void

    .line 85
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public setForegroundPadding(IIII)V
    .locals 0
    .param p1, "foregroundPaddingLeft"    # I
    .param p2, "foregroundPaddingTop"    # I
    .param p3, "foregroundPaddingRight"    # I
    .param p4, "foregroundPaddingBottom"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingLeft:I

    .line 96
    iput p2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingTop:I

    .line 97
    iput p3, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingRight:I

    .line 98
    iput p4, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundPaddingBottom:I

    .line 100
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->requestLayout()V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->invalidate()V

    .line 102
    return-void
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 140
    iget-object v2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 141
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 142
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 144
    .end local v0    # "isVisible":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 141
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/ForegroundRelativeLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
