.class Lcom/google/android/play/drawer/PlayDrawerAdapter$3;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileInfoView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvatarClicked(Landroid/accounts/Account;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 402
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 403
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 405
    .local v0, "isAccountDocLoaded":Z
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-interface {v2, v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 415
    .end local v0    # "isAccountDocLoaded":Z
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onSecondaryAccountClicked(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    goto :goto_0
.end method
