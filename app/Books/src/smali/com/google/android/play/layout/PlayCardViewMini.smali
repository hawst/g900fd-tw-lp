.class public Lcom/google/android/play/layout/PlayCardViewMini;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewMini.java"


# instance fields
.field private mCurrTitleMaxLines:I

.field private final mLabelThreshold:I

.field private final mTextContentHeight:I

.field private mVerticalOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardViewMini;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 50
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_mini_card_content_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->mTextContentHeight:I

    .line 52
    sget v1, Lcom/google/android/play/R$dimen;->play_mini_card_label_threshold:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabelThreshold:I

    .line 53
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->mCurrTitleMaxLines:I

    .line 54
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 33
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingLeft()I

    move-result v17

    .line 173
    .local v17, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingRight()I

    move-result v18

    .line 174
    .local v18, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingTop()I

    move-result v19

    .line 175
    .local v19, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingBottom()I

    move-result v16

    .line 177
    .local v16, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getWidth()I

    move-result v29

    .line 178
    .local v29, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getHeight()I

    move-result v5

    .line 180
    .local v5, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v25

    .line 181
    .local v25, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v17

    add-int v32, v19, v25

    move-object/from16 v0, v30

    move/from16 v1, v17

    move/from16 v2, v19

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 186
    .local v27, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 188
    .local v23, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/StarRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 190
    .local v20, "ratingLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 192
    .local v7, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 195
    .local v13, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    add-int v30, v19, v25

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mVerticalOverlap:I

    move/from16 v31, v0

    sub-int v28, v30, v31

    .line 196
    .local v28, "titleTop":I
    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    add-int v26, v17, v30

    .line 197
    .local v26, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v28

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v28

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 200
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v30, v0

    add-int v15, v28, v30

    .line 201
    .local v15, "overflowTop":I
    sub-int v30, v29, v18

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v14, v30, v31

    .line 202
    .local v14, "overflowRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v31

    sub-int v31, v14, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v15

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v15, v14, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 205
    sub-int v30, v5, v16

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    sub-int v30, v30, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mVerticalOverlap:I

    move/from16 v31, v0

    add-int v6, v30, v31

    .line 206
    .local v6, "labelBottom":I
    sub-int v30, v29, v18

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v8, v30, v31

    .line 207
    .local v8, "labelRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v31

    sub-int v31, v8, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v32

    sub-int v32, v6, v32

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2, v8, v6}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLineCount()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v30

    add-int v30, v30, v28

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v24, v30, v31

    .line 214
    .local v24, "subtitleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v24

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v24

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 225
    .end local v24    # "subtitleTop":I
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/StarRatingBar;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_1

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayCardLabelView;->getTop()I

    move-result v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v31

    add-int v30, v30, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/StarRatingBar;->getBaseline()I

    move-result v31

    sub-int v22, v30, v31

    .line 228
    .local v22, "ratingStarsTop":I
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    add-int v21, v17, v30

    .line 229
    .local v21, "ratingStarsLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v22

    move-object/from16 v0, v30

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/StarRatingBar;->layout(IIII)V

    .line 234
    .end local v21    # "ratingStarsLeft":I
    .end local v22    # "ratingStarsTop":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    .line 235
    .local v12, "loadingWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 236
    .local v9, "loadingHeight":I
    sub-int v30, v29, v17

    sub-int v30, v30, v18

    sub-int v30, v30, v12

    div-int/lit8 v30, v30, 0x2

    add-int v10, v17, v30

    .line 237
    .local v10, "loadingLeft":I
    sub-int v30, v5, v19

    sub-int v30, v30, v16

    sub-int v30, v30, v9

    div-int/lit8 v30, v30, 0x2

    add-int v11, v19, v30

    .line 238
    .local v11, "loadingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v11

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 242
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->recomputeOverflowAreaIfNeeded()V

    .line 243
    return-void

    .line 218
    .end local v9    # "loadingHeight":I
    .end local v10    # "loadingLeft":I
    .end local v11    # "loadingTop":I
    .end local v12    # "loadingWidth":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v30

    sub-int v30, v6, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v31

    add-int v30, v30, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayTextView;->getBaseline()I

    move-result v31

    sub-int v24, v30, v31

    .line 220
    .restart local v24    # "subtitleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v24

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v24

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 32
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 66
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/PlayCardViewMini;->measureThumbnailSpanningWidth(I)V

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 70
    .local v19, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 71
    .local v7, "heightSpecMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 75
    .local v8, "heightSpecSize":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTextContentHeight:I

    move/from16 v26, v0

    add-int v25, v25, v26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingTop()I

    move-result v26

    add-int v25, v25, v26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingBottom()I

    move-result v26

    add-int v9, v25, v26

    .line 77
    .local v9, "idealHeight":I
    const/high16 v25, 0x40000000    # 2.0f

    move/from16 v0, v25

    if-ne v7, v0, :cond_1

    if-lez v8, :cond_1

    move v6, v8

    .line 79
    .local v6, "height":I
    :goto_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v23

    .line 84
    .local v23, "width":I
    const/16 v25, 0x0

    sub-int v26, v9, v6

    div-int/lit8 v26, v26, 0x3

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/play/layout/PlayCardViewMini;->mVerticalOverlap:I

    .line 86
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingLeft()I

    move-result v13

    .line 87
    .local v13, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingRight()I

    move-result v14

    .line 89
    .local v14, "paddingRight":I
    sub-int v25, v23, v13

    sub-int v4, v25, v14

    .line 90
    .local v4, "contentWidth":I
    const/high16 v25, 0x40000000    # 2.0f

    move/from16 v0, v25

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 91
    .local v5, "contentWidthSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v25, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 96
    .local v20, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 98
    .local v17, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/StarRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 100
    .local v15, "ratingLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 104
    .local v10, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v12, 0x0

    .line 105
    .local v12, "mToShowLabel":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mBackendId:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2

    .line 106
    const/4 v12, 0x1

    .line 110
    :goto_1
    if-eqz v12, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayCardLabelView;->getVisibility()I

    move-result v25

    const/16 v26, 0x8

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_5

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    const/high16 v26, -0x80000000

    move/from16 v0, v26

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 122
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v25

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    add-int v11, v25, v26

    .line 125
    .local v11, "labelWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/ImageView;->measure(II)V

    .line 128
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v4, v25

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    sub-int v21, v25, v26

    .line 129
    .local v21, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, 0x40000000    # 2.0f

    move/from16 v0, v21

    move/from16 v1, v26

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-eqz v25, :cond_0

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v25

    if-eqz v25, :cond_7

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/google/android/play/layout/StarRatingBar;->measure(II)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredWidth()I

    move-result v16

    .line 138
    .local v16, "ratingStarsWidth":I
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    add-int v25, v25, v16

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int v22, v25, v11

    .line 140
    .local v22, "totalWidth":I
    move/from16 v0, v22

    if-le v0, v4, :cond_6

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v25, v0

    const/16 v26, 0x4

    invoke-virtual/range {v25 .. v26}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 165
    .end local v16    # "ratingStarsWidth":I
    .end local v22    # "totalWidth":I
    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/view/View;->measure(II)V

    .line 167
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Lcom/google/android/play/layout/PlayCardViewMini;->setMeasuredDimension(II)V

    .line 168
    return-void

    .end local v4    # "contentWidth":I
    .end local v5    # "contentWidthSpec":I
    .end local v6    # "height":I
    .end local v10    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v11    # "labelWidth":I
    .end local v12    # "mToShowLabel":Z
    .end local v13    # "paddingLeft":I
    .end local v14    # "paddingRight":I
    .end local v15    # "ratingLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v17    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v20    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v21    # "titleWidth":I
    .end local v23    # "width":I
    :cond_1
    move v6, v9

    .line 77
    goto/16 :goto_0

    .line 108
    .restart local v4    # "contentWidth":I
    .restart local v5    # "contentWidthSpec":I
    .restart local v6    # "height":I
    .restart local v10    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v12    # "mToShowLabel":Z
    .restart local v13    # "paddingLeft":I
    .restart local v14    # "paddingRight":I
    .restart local v15    # "ratingLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v17    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v20    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v23    # "width":I
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mIsItemOwned:Z

    move/from16 v25, v0

    if-nez v25, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabelThreshold:I

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v25

    if-lt v0, v1, :cond_4

    :cond_3
    const/4 v12, 0x1

    :goto_4
    goto/16 :goto_1

    :cond_4
    const/4 v12, 0x0

    goto :goto_4

    .line 118
    :cond_5
    const/16 v25, 0x0

    const/high16 v26, 0x40000000    # 2.0f

    invoke-static/range {v25 .. v26}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    .line 119
    .local v24, "zeroSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v24

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    goto/16 :goto_2

    .line 143
    .end local v24    # "zeroSpec":I
    .restart local v11    # "labelWidth":I
    .restart local v16    # "ratingStarsWidth":I
    .restart local v21    # "titleWidth":I
    .restart local v22    # "totalWidth":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_3

    .line 148
    .end local v16    # "ratingStarsWidth":I
    .end local v22    # "totalWidth":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getLineCount()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/layout/PlayCardLabelView;->getVisibility()I

    move-result v25

    if-eqz v25, :cond_9

    :cond_8
    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v4, v25

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    sub-int v18, v25, v26

    .line 154
    .local v18, "subtitleWidth":I
    :goto_5
    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v26, v0

    const-wide v28, 0x3fd3333333333333L    # 0.3

    int-to-double v0, v4

    move-wide/from16 v30, v0

    mul-double v28, v28, v30

    cmpg-double v25, v26, v28

    if-gez v25, :cond_a

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v25, v0

    const/16 v26, 0x4

    invoke-virtual/range {v25 .. v26}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 148
    .end local v18    # "subtitleWidth":I
    :cond_9
    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v4, v25

    sub-int v18, v25, v11

    goto :goto_5

    .line 157
    .restart local v18    # "subtitleWidth":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v25, v0

    const/high16 v26, 0x40000000    # 2.0f

    move/from16 v0, v18

    move/from16 v1, v26

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    goto/16 :goto_3
.end method
