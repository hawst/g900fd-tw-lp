.class public final Lcom/google/android/play/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f090041

.field public static final play_avatar_decoration_threshold_max:I = 0x7f09008c

.field public static final play_avatar_decoration_threshold_min:I = 0x7f09008b

.field public static final play_avatar_drop_shadow_max:I = 0x7f090090

.field public static final play_avatar_drop_shadow_min:I = 0x7f09008f

.field public static final play_avatar_noring_outline:I = 0x7f090091

.field public static final play_avatar_ring_size_max:I = 0x7f09008e

.field public static final play_avatar_ring_size_min:I = 0x7f09008d

.field public static final play_card_default_inset:I = 0x7f0900ab

.field public static final play_card_extra_vspace:I = 0x7f090070

.field public static final play_card_label_icon_gap:I = 0x7f09007f

.field public static final play_card_label_texts_gap:I = 0x7f090080

.field public static final play_card_overflow_touch_extend:I = 0x7f090073

.field public static final play_card_snippet_avatar_large_size:I = 0x7f090077

.field public static final play_card_snippet_avatar_size:I = 0x7f090076

.field public static final play_card_snippet_text_extra_margin_left:I = 0x7f09007b

.field public static final play_drawer_max_width:I = 0x7f090088

.field public static final play_hairline_separator_thickness:I = 0x7f09006b

.field public static final play_header_list_banner_height:I = 0x7f090093

.field public static final play_header_list_floating_elevation:I = 0x7f090099

.field public static final play_header_list_tab_floating_padding:I = 0x7f09009b

.field public static final play_header_list_tab_strip_height:I = 0x7f090094

.field public static final play_header_list_tab_strip_selected_underline_height:I = 0x7f090095

.field public static final play_medium_size:I = 0x7f0900bf

.field public static final play_mini_card_content_height:I = 0x7f09007d

.field public static final play_mini_card_label_threshold:I = 0x7f09007e

.field public static final play_onboard__onboard_nav_footer_height:I = 0x7f0900e0

.field public static final play_onboard__onboard_simple_quiz_row_spacing:I = 0x7f0900e6

.field public static final play_onboard__page_indicator_dot_diameter:I = 0x7f0900e2

.field public static final play_search_one_suggestion_height:I = 0x7f0900b8

.field public static final play_small_card_content_min_height:I = 0x7f09007c

.field public static final play_snippet_large_size:I = 0x7f0900c6

.field public static final play_snippet_regular_size:I = 0x7f0900c5

.field public static final play_star_height_default:I = 0x7f0900a8

.field public static final play_tab_strip_selected_underline_height:I = 0x7f090066

.field public static final play_tab_strip_title_offset:I = 0x7f090069

.field public static final play_text_view_fadeout:I = 0x7f09006c

.field public static final play_text_view_fadeout_hint_margin:I = 0x7f09006d

.field public static final play_text_view_outline:I = 0x7f09006e
