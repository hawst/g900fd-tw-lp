.class public Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
.super Landroid/support/v7/widget/RecyclerView$LayoutParams;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field static sViewHolderField:Ljava/lang/reflect/Field;


# instance fields
.field public bottomMarginCompound:I

.field public endMarginCompound:I

.field public firstLineTopMarginCompound:I

.field public flow:I

.field public flowHeightCompound:I

.field public flowInsetBottomCompound:I

.field public flowInsetEndCompound:I

.field public flowInsetStartCompound:I

.field public flowInsetTopCompound:I

.field public flowWidthCompound:I

.field public gridColumnCount:F

.field public gridInsetEnd:I

.field public gridInsetStart:I

.field public gridMinCellSize:I

.field public heightCompound:I

.field public lastLineBottomMarginCompound:I

.field public lineWrap:I

.field public maxGridWidth:I

.field public startMarginCompound:I

.field public topMarginCompound:I

.field public vAlign:I

.field public widthCompound:I


# direct methods
.method public constructor <init>(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 373
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 374
    iput p1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    .line 375
    iput p2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v8, 0x1

    const/4 v4, -0x1

    const/4 v7, 0x0

    const v3, 0x7fffffff

    const/4 v6, 0x0

    .line 264
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 185
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 189
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 193
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 197
    iput v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 201
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 205
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 209
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 213
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 217
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 221
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 225
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 229
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 233
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 237
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 241
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 245
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 249
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 253
    iput v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 257
    iput v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 261
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 268
    sget-object v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_Style:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 270
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_Style_layout_flmStyle:I

    sget v4, Lcom/google/android/play/R$style;->FlowLayoutManager_Layout_Default:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 273
    .local v1, "defaultStyle":I
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 276
    sget-object v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout:[I

    invoke-virtual {p1, p2, v3, v6, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 279
    const-string v3, "layout_flmWidth"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmWidth:I

    iget v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundLayoutDimension(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    .line 281
    const-string v3, "layout_flmHeight"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmHeight:I

    iget v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundLayoutDimension(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 283
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmGridInsetStart:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 285
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmGridInsetEnd:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 287
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMaxGridWidth:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 289
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmGridColumnCount:I

    invoke-virtual {v2, v3, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 291
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmGridMinCellSize:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 294
    const-string v3, "layout_flmMargin"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMargin:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v0

    .line 296
    .local v0, "defaultMargin":I
    const-string v3, "layout_flmMarginTop"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginTop:I

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundMargin(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 298
    const-string v3, "layout_flmMarginStart"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginStart:I

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundMargin(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 300
    const-string v3, "layout_flmMarginEnd"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginEnd:I

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundMargin(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 302
    const-string v3, "layout_flmMarginBottom"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginBottom:I

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getCompoundMargin(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 304
    const-string v3, "layout_flmMarginTopForFirstLine"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginTopForFirstLine:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 307
    const-string v3, "layout_flmMarginBottomForLastLine"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmMarginBottomForLastLine:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 311
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmVAlign:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 314
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlow:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 316
    const-string v3, "layout_flmFlowInsetTop"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowInsetTop:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 318
    const-string v3, "layout_flmFlowInsetStart"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowInsetStart:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 320
    const-string v3, "layout_flmFlowInsetEnd"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowInsetEnd:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 322
    const-string v3, "layout_flmFlowInsetBottom"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowInsetBottom:I

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 324
    const-string v3, "layout_flmFlowWidth"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowWidth:I

    invoke-static {v2, v3, v4, v8}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 326
    const-string v3, "layout_flmFlowHeight"

    sget v4, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmFlowHeight:I

    invoke-static {v2, v3, v4, v8}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 329
    sget v3, Lcom/google/android/play/R$styleable;->FlowLayoutManager_Layout_layout_flmLineWrap:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 331
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 332
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "source"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 367
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 368
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    .line 369
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 370
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 4
    .param p1, "source"    # Landroid/view/ViewGroup$MarginLayoutParams;

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 361
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 362
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    .line 363
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 364
    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;)V
    .locals 4
    .param p1, "source"    # Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 335
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 336
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    .line 337
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 338
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 339
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 340
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 341
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 342
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    .line 343
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    .line 344
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    .line 345
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    .line 346
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    .line 347
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    .line 348
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    .line 349
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    .line 350
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    .line 351
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    .line 352
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    .line 353
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    .line 354
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    .line 355
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    .line 356
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    .line 357
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    .line 358
    return-void
.end method

.method private static getCompoundLayoutDimension(Landroid/content/res/TypedArray;Ljava/lang/String;II)I
    .locals 3
    .param p0, "typedArray"    # Landroid/content/res/TypedArray;
    .param p1, "attrName"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "androidNamespaceParam"    # I

    .prologue
    .line 400
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    if-ne p3, v0, :cond_2

    .line 401
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result p3

    .line 407
    .end local p3    # "androidNamespaceParam":I
    :cond_1
    return p3

    .line 405
    .restart local p3    # "androidNamespaceParam":I
    :cond_2
    const/4 v0, -0x2

    if-gt v0, p3, :cond_3

    const v0, 0xffffff

    if-le p3, v0, :cond_1

    .line 409
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": out-of-range dimension length for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getCompoundMargin(Landroid/content/res/TypedArray;Ljava/lang/String;II)I
    .locals 1
    .param p0, "typedArray"    # Landroid/content/res/TypedArray;
    .param p1, "attrName"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "defaultMarginCompound"    # I

    .prologue
    .line 423
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/utils/Compound;->getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result p3

    .line 426
    .end local p3    # "defaultMarginCompound":I
    :cond_0
    return p3
.end method

.method private getPixelSize(Ljava/lang/String;IFZ)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "compound"    # I
    .param p3, "unitSize"    # F
    .param p4, "isSize"    # Z

    .prologue
    .line 608
    invoke-static {p2}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 618
    .end local p2    # "compound":I
    :cond_0
    :goto_0
    return p2

    .line 611
    .restart local p2    # "compound":I
    :cond_1
    if-eqz p4, :cond_2

    if-ltz p2, :cond_0

    .line 614
    :cond_2
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uses grid-based measurement, which is disabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :cond_3
    invoke-static {p2}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    mul-float/2addr v0, p3

    float-to-int p2, v0

    goto :goto_0
.end method


# virtual methods
.method debugGetViewHolderDump()Ljava/lang/String;
    .locals 4

    .prologue
    .line 762
    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->sViewHolderField:Ljava/lang/reflect/Field;

    if-nez v2, :cond_0

    .line 764
    :try_start_0
    const-class v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const-string v3, "mViewHolder"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 765
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 766
    sput-object v1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->sViewHolderField:Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :try_start_1
    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->sViewHolderField:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 775
    :goto_0
    return-object v2

    .line 767
    :catch_0
    move-exception v0

    .line 769
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 774
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 775
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method getBottomMargin(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 564
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 565
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMargin:I

    .line 567
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginBottom"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->bottomMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method getBottomMarginForLastLine(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 573
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 574
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getBottomMargin(F)I

    move-result v0

    .line 576
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginBottomForLastLine"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lastLineBottomMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method getEffectiveLineWrapAction()I
    .locals 2

    .prologue
    .line 437
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    if-eqz v0, :cond_1

    .line 438
    :cond_0
    const/4 v0, 0x2

    .line 440
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    and-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method getEndMargin(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 556
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 557
    invoke-static {p0}, Landroid/support/v4/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 559
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginEnd"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->endMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method getFlowHeight(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 586
    const-string v0, "layout_flmFlowHeight"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getFlowInsetBottom(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 603
    const-string v0, "layout_flmFlowInsetBottom"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetBottomCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getFlowInsetEnd(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 599
    const-string v0, "layout_flmFlowInsetEnd"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetEndCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getFlowInsetStart(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 594
    const-string v0, "layout_flmFlowInsetStart"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getFlowInsetTop(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 590
    const-string v0, "layout_flmFlowInsetTop"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetTopCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getFlowWidth(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 582
    const-string v0, "layout_flmFlowWidth"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowWidthCompound:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method getGridDefinition(ILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)F
    .locals 10
    .param p1, "fullContextWidth"    # I
    .param p2, "item"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .prologue
    const/4 v8, 0x0

    .line 482
    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetStart:I

    .line 483
    .local v2, "effectiveGridInsetStart":I
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridInsetEnd:I

    .line 484
    .local v1, "effectiveGridInsetEnd":I
    sub-int v6, p1, v2

    sub-int v3, v6, v1

    .line 486
    .local v3, "effectiveGridWidth":I
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    if-lez v6, :cond_0

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    if-le v3, v6, :cond_0

    .line 487
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    sub-int v5, v3, v6

    .line 488
    .local v5, "unused":I
    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->maxGridWidth:I

    .line 491
    shr-int/lit8 v6, v5, 0x1

    add-int/2addr v2, v6

    .line 492
    shr-int/lit8 v6, v5, 0x1

    and-int/lit8 v7, v5, 0x1

    add-int/2addr v6, v7

    add-int/2addr v1, v6

    .line 496
    .end local v5    # "unused":I
    :cond_0
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    if-gtz v6, :cond_3

    .line 498
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    cmpg-float v6, v6, v8

    if-gtz v6, :cond_2

    const/high16 v0, 0x7fc00000    # NaNf

    .line 509
    .local v0, "effectiveColumnCount":F
    :goto_0
    if-eqz p2, :cond_1

    .line 510
    const/4 v6, 0x0

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v0

    iput v6, p2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    .line 511
    iput v2, p2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    .line 512
    iput v1, p2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    .line 514
    :cond_1
    return v0

    .line 498
    .end local v0    # "effectiveColumnCount":F
    :cond_2
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    goto :goto_0

    .line 501
    :cond_3
    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridMinCellSize:I

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 502
    .local v4, "maxColumnCount":F
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    cmpg-float v6, v8, v6

    if-gez v6, :cond_4

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    cmpg-float v6, v6, v4

    if-gtz v6, :cond_4

    .line 503
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .restart local v0    # "effectiveColumnCount":F
    goto :goto_0

    .line 505
    .end local v0    # "effectiveColumnCount":F
    :cond_4
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    double-to-float v0, v6

    .restart local v0    # "effectiveColumnCount":F
    goto :goto_0
.end method

.method getLineWrapFlow()I
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->lineWrap:I

    and-int/lit8 v0, v0, 0xc

    return v0
.end method

.method getStartMargin(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 548
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 549
    invoke-static {p0}, Landroid/support/v4/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 551
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginStart"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->startMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method getTopMargin(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 531
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 532
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMargin:I

    .line 534
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginTop"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->topMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method getTopMarginForFirstLine(F)I
    .locals 3
    .param p1, "cellSize"    # F

    .prologue
    .line 539
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 540
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getTopMargin(F)I

    move-result v0

    .line 542
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginTopForFirstLine"

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->firstLineTopMarginCompound:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method isAnchorCandidate()Z
    .locals 2

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getEffectiveLineWrapAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getLineWrapFlow()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method resolveHeightAndMakeMeasureSpec(FILcom/google/android/play/layout/FlowLayoutManager;)I
    .locals 7
    .param p1, "cellSize"    # F
    .param p2, "verticalDecorationSize"    # I
    .param p3, "manager"    # Lcom/google/android/play/layout/FlowLayoutManager;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 676
    const-string v2, "layout_flmHeight"

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    const/4 v4, 0x1

    invoke-direct {p0, v2, v3, p1, v4}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v1

    .line 678
    .local v1, "pixelSize":I
    packed-switch v1, :pswitch_data_0

    .line 707
    :pswitch_0
    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 709
    sub-int v2, v1, p2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 714
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 715
    .local v0, "measureMode":I
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    .line 718
    :goto_0
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    return v2

    .line 680
    .end local v0    # "measureMode":I
    :pswitch_1
    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->getHeight()I

    move-result v2

    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, p2

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getTopMargin(F)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getBottomMargin(F)I

    move-result v3

    sub-int v1, v2, v3

    .line 683
    const/high16 v0, 0x40000000    # 2.0f

    .line 684
    .restart local v0    # "measureMode":I
    iput v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    goto :goto_0

    .line 687
    .end local v0    # "measureMode":I
    :pswitch_2
    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowHeightCompound:I

    if-ltz v2, :cond_1

    .line 689
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowHeight(F)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetTop(F)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetBottom(F)I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 691
    const/high16 v0, 0x40000000    # 2.0f

    .line 699
    .restart local v0    # "measureMode":I
    :goto_1
    iput v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    goto :goto_0

    .line 694
    .end local v0    # "measureMode":I
    :cond_1
    const/4 v1, 0x0

    .line 695
    const/4 v0, 0x0

    .restart local v0    # "measureMode":I
    goto :goto_1

    .line 702
    .end local v0    # "measureMode":I
    :pswitch_3
    const/4 v1, 0x0

    .line 703
    const/4 v0, 0x0

    .line 704
    .restart local v0    # "measureMode":I
    const/4 v2, -0x2

    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    goto :goto_0

    .line 710
    .end local v0    # "measureMode":I
    :cond_2
    if-gez v1, :cond_0

    .line 711
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown value for layout_flmHeight: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 678
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method resolveWidthAndMakeMeasureSpec(FII)I
    .locals 5
    .param p1, "cellSize"    # F
    .param p2, "remainingLineWidth"    # I
    .param p3, "horizontalDecorationSize"    # I

    .prologue
    .line 632
    const-string v2, "layout_flmWidth"

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    const/4 v4, 0x1

    invoke-direct {p0, v2, v3, p1, v4}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getPixelSize(Ljava/lang/String;IFZ)I

    move-result v1

    .line 634
    .local v1, "pixelSize":I
    packed-switch v1, :pswitch_data_0

    .line 651
    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 653
    const/4 v2, 0x0

    sub-int v3, v1, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 658
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 659
    .local v0, "measureMode":I
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    .line 662
    :goto_0
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    return v2

    .line 636
    .end local v0    # "measureMode":I
    :pswitch_0
    move v1, p2

    .line 637
    const/high16 v0, 0x40000000    # 2.0f

    .line 638
    .restart local v0    # "measureMode":I
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    goto :goto_0

    .line 641
    .end local v0    # "measureMode":I
    :pswitch_1
    move v1, p2

    .line 642
    const/high16 v0, 0x40000000    # 2.0f

    .line 643
    .restart local v0    # "measureMode":I
    iput p2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    goto :goto_0

    .line 646
    .end local v0    # "measureMode":I
    :pswitch_2
    move v1, p2

    .line 647
    const/high16 v0, -0x80000000

    .line 648
    .restart local v0    # "measureMode":I
    const/4 v2, -0x2

    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    goto :goto_0

    .line 654
    .end local v0    # "measureMode":I
    :cond_1
    if-gez v1, :cond_0

    .line 655
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown enum value for layout_flmWidth: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 634
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .locals 2
    .param p1, "typedArray"    # Landroid/content/res/TypedArray;
    .param p2, "widthAttr"    # I
    .param p3, "heightAttr"    # I

    .prologue
    const v1, 0x7fffffff

    .line 384
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->width:I

    .line 385
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->height:I

    .line 386
    return-void
.end method
