.class public Lcom/google/android/play/layout/PlayCardSnippet;
.super Lcom/google/android/play/layout/PlaySeparatorLayout;
.source "PlayCardSnippet.java"


# instance fields
.field private final mAvatarLargeSize:I

.field private mAvatarMarginLeft:I

.field private final mAvatarRegularSize:I

.field private mAvatarSize:I

.field private mMode:I

.field private mSnippetAvatar:Landroid/widget/ImageView;

.field private mSnippetText:Landroid/widget/TextView;

.field private final mTextLargeSize:I

.field private mTextOnlyMarginLeft:I

.field private final mTextRegularSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardSnippet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlaySeparatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_card_snippet_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarRegularSize:I

    .line 72
    sget v1, Lcom/google/android/play/R$dimen;->play_card_snippet_avatar_large_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarLargeSize:I

    .line 73
    sget v1, Lcom/google/android/play/R$dimen;->play_snippet_regular_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextRegularSize:I

    .line 74
    sget v1, Lcom/google/android/play/R$dimen;->play_snippet_large_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextLargeSize:I

    .line 77
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mMode:I

    .line 78
    return-void
.end method

.method private syncWithCurrentSizeMode()V
    .locals 3

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mMode:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarRegularSize:I

    :goto_0
    iput v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarSize:I

    .line 92
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mMode:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextRegularSize:I

    int-to-float v0, v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 94
    return-void

    .line 91
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarLargeSize:I

    goto :goto_0

    .line 92
    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextLargeSize:I

    int-to-float v0, v0

    goto :goto_1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/android/play/layout/PlaySeparatorLayout;->onFinishInflate()V

    .line 84
    sget v0, Lcom/google/android/play/R$id;->li_snippet_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    .line 85
    sget v0, Lcom/google/android/play/R$id;->li_snippet_avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    .line 87
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->syncWithCurrentSizeMode()V

    .line 88
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getHeight()I

    move-result v2

    .line 148
    .local v2, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingTop()I

    move-result v7

    .line 150
    .local v7, "paddingTop":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    .line 151
    .local v8, "snippetHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    .line 152
    .local v9, "snippetWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-ne v13, v14, :cond_0

    .line 153
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v10, v7, v13

    .line 154
    .local v10, "textTop":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextOnlyMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextOnlyMarginLeft:I

    add-int/2addr v15, v9

    add-int v16, v10, v8

    move/from16 v0, v16

    invoke-virtual {v13, v14, v10, v15, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 181
    .end local v10    # "textTop":I
    :goto_0
    return-void

    .line 157
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    .line 158
    .local v3, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    .line 159
    .local v5, "imageWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 161
    .local v4, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    add-int/2addr v13, v5

    iget v14, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v11, v13, v14

    .line 163
    .local v11, "textX":I
    if-le v3, v8, :cond_1

    .line 166
    sub-int v13, v2, v3

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v6, v7, v13

    .line 167
    .local v6, "imageY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    add-int/2addr v15, v5

    add-int v16, v6, v3

    move/from16 v0, v16

    invoke-virtual {v13, v14, v6, v15, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 169
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v12, v7, v13

    .line 170
    .local v12, "textY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    add-int v14, v11, v9

    add-int v15, v12, v8

    invoke-virtual {v13, v11, v12, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0

    .line 174
    .end local v6    # "imageY":I
    .end local v12    # "textY":I
    :cond_1
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v1, v7, v13

    .line 175
    .local v1, "contentY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    add-int/2addr v15, v5

    add-int v16, v1, v3

    move/from16 v0, v16

    invoke-virtual {v13, v14, v1, v15, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 177
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    add-int v14, v11, v9

    add-int v15, v1, v8

    invoke-virtual {v13, v11, v1, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 121
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 122
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingLeft()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingRight()I

    move-result v9

    sub-int v5, v8, v9

    .line 123
    .local v5, "snippetWidth":I
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_0

    const/4 v4, 0x1

    .line 125
    .local v4, "isImageGone":Z
    :goto_0
    if-eqz v4, :cond_1

    .line 126
    iget v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mTextOnlyMarginLeft:I

    sub-int/2addr v5, v8

    .line 136
    :goto_1
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v7}, Landroid/widget/TextView;->measure(II)V

    .line 139
    iget v7, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarSize:I

    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 140
    .local v1, "contentHeight":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingTop()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingBottom()I

    move-result v8

    add-int v2, v7, v8

    .line 142
    .local v2, "height":I
    invoke-virtual {p0, v6, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setMeasuredDimension(II)V

    .line 143
    return-void

    .end local v1    # "contentHeight":I
    .end local v2    # "height":I
    .end local v4    # "isImageGone":Z
    :cond_0
    move v4, v7

    .line 123
    goto :goto_0

    .line 129
    .restart local v4    # "isImageGone":Z
    :cond_1
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 131
    .local v3, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarSize:I

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 132
    .local v0, "avatarSpec":I
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mSnippetAvatar:Landroid/widget/ImageView;

    invoke-virtual {v8, v0, v0}, Landroid/widget/ImageView;->measure(II)V

    .line 133
    iget v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarSize:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v8, v9

    iget v9, p0, Lcom/google/android/play/layout/PlayCardSnippet;->mAvatarMarginLeft:I

    add-int/2addr v8, v9

    sub-int/2addr v5, v8

    goto :goto_1
.end method
