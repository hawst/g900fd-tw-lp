.class abstract Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;
.super Ljava/lang/Object;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "MultiItemInfo"
.end annotation


# instance fields
.field public mMeasureMode:I

.field public mPositionStart:I

.field public mTotalHeight:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/layout/FlowLayoutManager$1;

    .prologue
    .line 989
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public final arrangeIfNecessary(I)I
    .locals 6
    .param p1, "totalItemCount"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 1034
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mPositionStart:I

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->validPositionEnd()I

    move-result v4

    if-ne v4, p1, :cond_3

    const/4 v4, 0x2

    :goto_1
    or-int v0, v1, v4

    .line 1036
    .local v0, "newMeasureMode":I
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mTotalHeight:I

    if-eq v1, v5, :cond_0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mMeasureMode:I

    if-eq v0, v1, :cond_1

    .line 1037
    :cond_0
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mTotalHeight:I

    if-ne v1, v5, :cond_4

    :goto_2
    invoke-virtual {p0, v2, p1}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->onArrange(ZI)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mTotalHeight:I

    .line 1038
    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mMeasureMode:I

    .line 1040
    :cond_1
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mTotalHeight:I

    return v1

    .end local v0    # "newMeasureMode":I
    :cond_2
    move v1, v3

    .line 1034
    goto :goto_0

    :cond_3
    move v4, v3

    goto :goto_1

    .restart local v0    # "newMeasureMode":I
    :cond_4
    move v2, v3

    .line 1037
    goto :goto_2
.end method

.method public final invalidateFrom(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1063
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mPositionStart:I

    if-gt p1, v0, :cond_0

    .line 1064
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->invalidateHeight()V

    .line 1065
    const/4 v0, 0x2

    .line 1071
    :goto_0
    return v0

    .line 1067
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->invalidateFromInternal(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1068
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->invalidateHeight()V

    .line 1069
    const/4 v0, 0x1

    goto :goto_0

    .line 1071
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract invalidateFromInternal(I)Z
.end method

.method public final invalidateHeight()V
    .locals 1

    .prologue
    .line 1014
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mTotalHeight:I

    .line 1015
    return-void
.end method

.method public offsetPositions(I)V
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 1086
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mPositionStart:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mPositionStart:I

    .line 1087
    return-void
.end method

.method protected abstract onArrange(ZI)I
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->invalidateHeight()V

    .line 1091
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mPositionStart:I

    .line 1092
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->mMeasureMode:I

    .line 1093
    return-void
.end method

.method public abstract validPositionEnd()I
.end method
