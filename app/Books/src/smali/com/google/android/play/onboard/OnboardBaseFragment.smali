.class public abstract Lcom/google/android/play/onboard/OnboardBaseFragment;
.super Landroid/support/v4/app/Fragment;
.source "OnboardBaseFragment.java"


# instance fields
.field protected final mHandler:Landroid/os/Handler;

.field private final mLayoutResourceId:I

.field private mRootView:Landroid/view/View;


# direct methods
.method protected constructor <init>(I)V
    .locals 1
    .param p1, "layoutResourceId"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mHandler:Landroid/os/Handler;

    .line 22
    iput p1, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mLayoutResourceId:I

    .line 23
    return-void
.end method

.method private makeSafeRunnable(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/play/onboard/OnboardBaseFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/onboard/OnboardBaseFragment$1;-><init>(Lcom/google/android/play/onboard/OnboardBaseFragment;Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method protected getOrCreateArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardBaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 42
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 43
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mLayoutResourceId:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mRootView:Landroid/view/View;

    .line 29
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method protected final safelyPost(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/OnboardBaseFragment;->makeSafeRunnable(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 73
    return-void
.end method

.method protected final safelyPostDelayed(Ljava/lang/Runnable;J)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "delayMillis"    # J

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardBaseFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/OnboardBaseFragment;->makeSafeRunnable(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 65
    return-void
.end method
