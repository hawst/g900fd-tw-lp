.class Lcom/google/analytics/tracking/android/EasyTracker$NoopTracker;
.super Lcom/google/analytics/tracking/android/Tracker;
.source "EasyTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/analytics/tracking/android/EasyTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NoopTracker"
.end annotation


# instance fields
.field private mIsAnonymizeIp:Z

.field private mSampleRate:D

.field final synthetic this$0:Lcom/google/analytics/tracking/android/EasyTracker;


# direct methods
.method constructor <init>(Lcom/google/analytics/tracking/android/EasyTracker;)V
    .locals 2

    .prologue
    .line 455
    iput-object p1, p0, Lcom/google/analytics/tracking/android/EasyTracker$NoopTracker;->this$0:Lcom/google/analytics/tracking/android/EasyTracker;

    invoke-direct {p0}, Lcom/google/analytics/tracking/android/Tracker;-><init>()V

    .line 458
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    iput-wide v0, p0, Lcom/google/analytics/tracking/android/EasyTracker$NoopTracker;->mSampleRate:D

    return-void
.end method


# virtual methods
.method public constructEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Ljava/util/Map;
    .locals 1
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public constructException(Ljava/lang/String;Z)Ljava/util/Map;
    .locals 1
    .param p1, "exceptionDescription"    # Ljava/lang/String;
    .param p2, "fatal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public constructTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "intervalInMilliseconds"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/Long;

    .prologue
    .line 490
    return-void
.end method

.method public sendException(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "fatal"    # Z

    .prologue
    .line 498
    return-void
.end method

.method public sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "intervalInMilliseconds"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "label"    # Ljava/lang/String;

    .prologue
    .line 507
    return-void
.end method

.method public sendView(Ljava/lang/String;)V
    .locals 0
    .param p1, "appScreen"    # Ljava/lang/String;

    .prologue
    .line 486
    return-void
.end method

.method public setAnonymizeIp(Z)V
    .locals 0
    .param p1, "anonymizeIp"    # Z

    .prologue
    .line 537
    iput-boolean p1, p0, Lcom/google/analytics/tracking/android/EasyTracker$NoopTracker;->mIsAnonymizeIp:Z

    .line 538
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 470
    return-void
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 474
    return-void
.end method

.method public setCustomDimension(ILjava/lang/String;)V
    .locals 0
    .param p1, "slot"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 639
    return-void
.end method

.method public setCustomMetric(ILjava/lang/Long;)V
    .locals 0
    .param p1, "slot"    # I
    .param p2, "value"    # Ljava/lang/Long;

    .prologue
    .line 643
    return-void
.end method

.method public setSampleRate(D)V
    .locals 1
    .param p1, "sampleRate"    # D

    .prologue
    .line 547
    iput-wide p1, p0, Lcom/google/analytics/tracking/android/EasyTracker$NoopTracker;->mSampleRate:D

    .line 548
    return-void
.end method

.method public setStartSession(Z)V
    .locals 0
    .param p1, "startSession"    # Z

    .prologue
    .line 466
    return-void
.end method
