.class public abstract Lorg/codehaus/jackson/node/NumericNode;
.super Lorg/codehaus/jackson/node/ValueNode;
.source "NumericNode.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lorg/codehaus/jackson/node/ValueNode;-><init>()V

    return-void
.end method


# virtual methods
.method public asDouble()D
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lorg/codehaus/jackson/node/NumericNode;->getDoubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public asDouble(D)D
    .locals 2
    .param p1, "defaultValue"    # D

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/codehaus/jackson/node/NumericNode;->getDoubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public asInt()I
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lorg/codehaus/jackson/node/NumericNode;->getIntValue()I

    move-result v0

    return v0
.end method

.method public asInt(I)I
    .locals 1
    .param p1, "defaultValue"    # I

    .prologue
    .line 52
    invoke-virtual {p0}, Lorg/codehaus/jackson/node/NumericNode;->getIntValue()I

    move-result v0

    return v0
.end method

.method public abstract getDoubleValue()D
.end method

.method public abstract getIntValue()I
.end method
