.class public Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;
.super Lorg/codehaus/jackson/map/SerializerFactory$Config;
.source "BeanSerializerFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/jackson/map/ser/BeanSerializerFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConfigImpl"
.end annotation


# static fields
.field protected static final NO_MODIFIERS:[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

.field protected static final NO_SERIALIZERS:[Lorg/codehaus/jackson/map/Serializers;


# instance fields
.field protected final _additionalKeySerializers:[Lorg/codehaus/jackson/map/Serializers;

.field protected final _additionalSerializers:[Lorg/codehaus/jackson/map/Serializers;

.field protected final _modifiers:[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    new-array v0, v1, [Lorg/codehaus/jackson/map/Serializers;

    sput-object v0, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->NO_SERIALIZERS:[Lorg/codehaus/jackson/map/Serializers;

    .line 85
    new-array v0, v1, [Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

    sput-object v0, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->NO_MODIFIERS:[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, v0, v0, v0}, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;-><init>([Lorg/codehaus/jackson/map/Serializers;[Lorg/codehaus/jackson/map/Serializers;[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;)V

    .line 108
    return-void
.end method

.method protected constructor <init>([Lorg/codehaus/jackson/map/Serializers;[Lorg/codehaus/jackson/map/Serializers;[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;)V
    .locals 0
    .param p1, "allAdditionalSerializers"    # [Lorg/codehaus/jackson/map/Serializers;
    .param p2, "allAdditionalKeySerializers"    # [Lorg/codehaus/jackson/map/Serializers;
    .param p3, "modifiers"    # [Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

    .prologue
    .line 113
    invoke-direct {p0}, Lorg/codehaus/jackson/map/SerializerFactory$Config;-><init>()V

    .line 114
    if-nez p1, :cond_0

    sget-object p1, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->NO_SERIALIZERS:[Lorg/codehaus/jackson/map/Serializers;

    .end local p1    # "allAdditionalSerializers":[Lorg/codehaus/jackson/map/Serializers;
    :cond_0
    iput-object p1, p0, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->_additionalSerializers:[Lorg/codehaus/jackson/map/Serializers;

    .line 116
    if-nez p2, :cond_1

    sget-object p2, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->NO_SERIALIZERS:[Lorg/codehaus/jackson/map/Serializers;

    .end local p2    # "allAdditionalKeySerializers":[Lorg/codehaus/jackson/map/Serializers;
    :cond_1
    iput-object p2, p0, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->_additionalKeySerializers:[Lorg/codehaus/jackson/map/Serializers;

    .line 118
    if-nez p3, :cond_2

    sget-object p3, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->NO_MODIFIERS:[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

    .end local p3    # "modifiers":[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;
    :cond_2
    iput-object p3, p0, Lorg/codehaus/jackson/map/ser/BeanSerializerFactory$ConfigImpl;->_modifiers:[Lorg/codehaus/jackson/map/ser/BeanSerializerModifier;

    .line 119
    return-void
.end method
