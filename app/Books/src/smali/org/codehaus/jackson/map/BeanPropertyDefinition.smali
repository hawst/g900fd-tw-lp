.class public abstract Lorg/codehaus/jackson/map/BeanPropertyDefinition;
.super Ljava/lang/Object;
.source "BeanPropertyDefinition.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getField()Lorg/codehaus/jackson/map/introspect/AnnotatedField;
.end method

.method public abstract getGetter()Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;
.end method

.method public abstract getMutator()Lorg/codehaus/jackson/map/introspect/AnnotatedMember;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSetter()Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;
.end method

.method public abstract hasConstructorParameter()Z
.end method

.method public abstract hasField()Z
.end method

.method public abstract hasGetter()Z
.end method

.method public abstract hasSetter()Z
.end method
