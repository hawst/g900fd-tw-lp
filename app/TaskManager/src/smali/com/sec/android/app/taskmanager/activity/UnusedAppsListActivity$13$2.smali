.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 660
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "MSG_REFRESH_LIST"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v1, v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->ReadLastUsedOverPeriodList(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v1

    # setter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;
    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$402(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->sort()V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # invokes: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->updateStatus()V
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$500(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->notifyDataSetChanged()V

    .line 666
    return-void
.end method
