.class public Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
.super Landroid/widget/BaseAdapter;
.source "RunningAppsPackageInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDefaultButtonTextColor:Landroid/content/res/ColorStateList;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mSortOrder:I

.field private mTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskManager:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->TAG:Ljava/lang/String;

    .line 319
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mSortOrder:I

    .line 85
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 87
    return-void
.end method

.method private getDetailString(Lcom/sec/android/app/taskmanager/PackageInfoItem;)Ljava/lang/String;
    .locals 5
    .param p1, "p"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuRateFormatted()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "cpu":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsageString()Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "mem":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f080009

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f08000a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public checkOrder(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 282
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    const/4 v0, 0x0

    .line 283
    .local v0, "bChanged":Z
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getSortComparator()Ljava/util/Comparator;

    move-result-object v3

    .line 285
    .local v3, "sortComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-nez v3, :cond_0

    move v1, v0

    .line 302
    .end local v0    # "bChanged":Z
    .local v1, "bChanged":I
    :goto_0
    return v1

    .line 289
    .end local v1    # "bChanged":I
    .restart local v0    # "bChanged":Z
    :cond_0
    instance-of v4, v3, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;

    if-eqz v4, :cond_1

    move-object v4, v3

    .line 290
    check-cast v4, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;->updateStartTime(Ljava/util/List;)V

    .line 293
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_2

    .line 294
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_2

    .line 295
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_3

    .line 296
    const/4 v0, 0x1

    .end local v2    # "i":I
    :cond_2
    move v1, v0

    .line 302
    .restart local v1    # "bChanged":I
    goto :goto_0

    .line 294
    .end local v1    # "bChanged":I
    .restart local v2    # "i":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->TAG:Ljava/lang/String;

    const-string v1, "getItem() : List is NULL."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v0, 0x0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getItem(I)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 118
    int-to-long v0, p1

    return-wide v0
.end method

.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    return-object v0
.end method

.method public getSortComparator()Ljava/util/Comparator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getSortOrder()I

    move-result v0

    .line 330
    .local v0, "sortOrder":I
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mSortOrder:I

    if-eq v1, v0, :cond_1

    .line 331
    :cond_0
    if-nez v0, :cond_2

    .line 332
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    .line 342
    :cond_1
    :goto_0
    iput v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mSortOrder:I

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    return-object v1

    .line 333
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 334
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/comparator/CpuUsageComparator;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/activity/comparator/CpuUsageComparator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    goto :goto_0

    .line 335
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 336
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/comparator/MemoryUsageComparator;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/activity/comparator/MemoryUsageComparator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    goto :goto_0

    .line 337
    :cond_4
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 338
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mComparator:Ljava/util/Comparator;

    goto :goto_0
.end method

.method public getSortOrder()I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mSortOrder:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 160
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getItem(I)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v1

    .line 162
    .local v1, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    if-nez v1, :cond_0

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->TAG:Ljava/lang/String;

    const-string v3, "getView() : pkgInfo is NULL."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/4 v2, 0x0

    .line 223
    :goto_0
    return-object v2

    .line 169
    :cond_0
    if-nez p2, :cond_2

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/high16 v3, 0x7f030000

    invoke-virtual {v2, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 171
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;-><init>()V

    .line 172
    .local v0, "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    const/high16 v2, 0x7f0c0000

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->itemLayout:Landroid/widget/LinearLayout;

    .line 173
    const v2, 0x7f0c0002

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    .line 174
    const v2, 0x7f0c0001

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    .line 175
    const v2, 0x7f0c0003

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    .line 177
    const v2, 0x7f0c0004

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->serviceDrawable:Landroid/graphics/drawable/Drawable;

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mDefaultButtonTextColor:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_1

    .line 182
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mDefaultButtonTextColor:Landroid/content/res/ColorStateList;

    .line 185
    :cond_1
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 186
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 187
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 188
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 189
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 195
    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setViewHolder(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V

    .line 196
    iput-object v1, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->pkgInfo:Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 197
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 199
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_3

    .line 200
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    :goto_2
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 206
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 207
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 208
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setFocusable(Z)V

    .line 209
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setClickable(Z)V

    .line 211
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 214
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_4

    .line 216
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->itemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 222
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->updateUi(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V

    move-object v2, p2

    .line 223
    goto/16 :goto_0

    .line 191
    .end local v0    # "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .restart local v0    # "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    goto :goto_1

    .line 202
    :cond_3
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 219
    :cond_4
    iget-object v2, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->itemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_3
.end method

.method public removeItem(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 111
    .local v0, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 112
    invoke-virtual {p0, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->setList(Ljava/util/List;)V

    .line 114
    .end local v0    # "newList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    :cond_0
    return-void
.end method

.method public setList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    .line 127
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 131
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "touchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 135
    return-void
.end method

.method public sort(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getSortComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 258
    .local v0, "sortComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-nez v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 262
    :cond_0
    instance-of v1, v0, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 263
    check-cast v1, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;->updateStartTime(Ljava/util/List;)V

    .line 272
    :cond_1
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public updateButtonTextColor(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V
    .locals 4
    .param p1, "holder"    # Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .prologue
    .line 240
    iget-object v1, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->pkgInfo:Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 241
    .local v1, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    iget-object v0, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    .line 243
    .local v0, "btn":Landroid/widget/Button;
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuUsage()F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 245
    const/high16 v2, -0x10000

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 253
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mDefaultButtonTextColor:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->mDefaultButtonTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 250
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->TAG:Ljava/lang/String;

    const-string v3, "updateButtonTextColor() : pkgInfo is NULL."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateUi(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V
    .locals 3
    .param p1, "holder"    # Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .prologue
    .line 227
    iget-object v0, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->pkgInfo:Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 229
    .local v0, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isCanKilled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 235
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->updateButtonTextColor(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V

    .line 236
    iget-object v1, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getDetailString(Lcom/sec/android/app/taskmanager/PackageInfoItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    return-void

    .line 232
    :cond_0
    iget-object v1, p1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
