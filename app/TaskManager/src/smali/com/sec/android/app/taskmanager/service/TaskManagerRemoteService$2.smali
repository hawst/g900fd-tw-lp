.class Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;
.super Ljava/lang/Object;
.source "TaskManagerRemoteService.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 24
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 298
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 424
    :cond_0
    :goto_0
    :pswitch_0
    const/16 v18, 0x0

    return v18

    .line 300
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->update()V

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->calcCPUUsage()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$400(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 304
    new-instance v9, Landroid/content/Intent;

    const-string v18, "com.sec.android.app.controlpanel.service.CPU_USAGE_WARN"

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 306
    .local v9, "i":Landroid/content/Intent;
    const-string v18, "cpu_warning"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 307
    const-string v18, "cpu_warning_level"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const-string v18, "cpu_warning_msg"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending cpu warning message broadcast: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 313
    .end local v9    # "i":Landroid/content/Intent;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const-wide/16 v20, 0x1388

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 320
    :pswitch_2
    const-string v18, "TaskManager:TaskManagerRemoteService"

    const-string v19, "case SEND_BROADCAST_RUNNING_APPS_COUNT"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 324
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList()Ljava/util/List;

    move-result-object v13

    .line 326
    .local v13, "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v16

    .line 327
    .local v16, "running_count":I
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "old_running_count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$700(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , running_count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 331
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 332
    .local v17, "running_id":[I
    move/from16 v0, v16

    new-array v11, v0, [Ljava/lang/String;

    .line 333
    .local v11, "package_name":[Ljava/lang/String;
    move/from16 v0, v16

    new-array v4, v0, [Ljava/lang/String;

    .line 334
    .local v4, "app_name":[Ljava/lang/String;
    move/from16 v0, v16

    new-array v5, v0, [Ljava/lang/String;

    .line 337
    .local v5, "class_name":[Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "r":I
    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_3

    .line 338
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getRecentTaskId()I

    move-result v18

    aput v18, v17, v12

    .line 339
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "running_id="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    aget v20, v17, v12

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v11, v12

    .line 341
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    aput-object v18, v4, v12

    .line 342
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 344
    .local v10, "intent":Landroid/content/Intent;
    if-eqz v10, :cond_2

    .line 345
    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v12

    .line 337
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 347
    :cond_2
    const/16 v18, 0x0

    aput-object v18, v5, v12
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 363
    .end local v4    # "app_name":[Ljava/lang/String;
    .end local v5    # "class_name":[Ljava/lang/String;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v11    # "package_name":[Ljava/lang/String;
    .end local v12    # "r":I
    .end local v13    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .end local v16    # "running_count":I
    .end local v17    # "running_id":[I
    :catch_0
    move-exception v8

    .line 364
    .local v8, "ex":Ljava/lang/RuntimeException;
    :try_start_1
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getRunningAppPackageList() error : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    const-wide/16 v20, 0x2710

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 351
    .end local v8    # "ex":Ljava/lang/RuntimeException;
    .restart local v4    # "app_name":[Ljava/lang/String;
    .restart local v5    # "class_name":[Ljava/lang/String;
    .restart local v11    # "package_name":[Ljava/lang/String;
    .restart local v12    # "r":I
    .restart local v13    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .restart local v16    # "running_count":I
    .restart local v17    # "running_id":[I
    :cond_3
    :try_start_2
    new-instance v15, Landroid/content/Intent;

    const-string v18, "com.sec.android.app.controlpanel.service.RUNNING_COUNT"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 353
    .local v15, "runningCountIntent":Landroid/content/Intent;
    const-string v18, "runningAppsCount"

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 354
    const-string v18, "runningId"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 355
    const-string v18, "packageName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    const-string v18, "appName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    const-string v18, "className"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending new running_count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 362
    .end local v4    # "app_name":[Ljava/lang/String;
    .end local v5    # "class_name":[Ljava/lang/String;
    .end local v11    # "package_name":[Ljava/lang/String;
    .end local v12    # "r":I
    .end local v15    # "runningCountIntent":Landroid/content/Intent;
    .end local v17    # "running_id":[I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v16

    # setter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I
    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$702(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;I)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    const-wide/16 v20, 0x2710

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 366
    .end local v13    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .end local v16    # "running_count":I
    :catchall_0
    move-exception v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    const-wide/16 v22, 0x2710

    move-object/from16 v0, v19

    move/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_5
    throw v18

    .line 373
    :pswitch_3
    const-string v18, "TaskManager:TaskManagerRemoteService"

    const-string v19, "case SEND_BROADCAST_ACTIVE_APPS"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->update()V

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->calcCPUUsage()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$400(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-nez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$700(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 379
    :cond_6
    new-instance v9, Landroid/content/Intent;

    const-string v18, "com.sec.android.app.controlpanel.service.CPU_USAGE_WARN"

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    .restart local v9    # "i":Landroid/content/Intent;
    const-string v18, "cpu_warning"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 382
    const-string v18, "cpu_warning_level"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    const-string v18, "cpu_warning_msg"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending cpu warning message broadcast: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 389
    .end local v9    # "i":Landroid/content/Intent;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v6

    .line 390
    .local v6, "count":I
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "PrevCount : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$700(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , RunningCount : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    new-instance v15, Landroid/content/Intent;

    const-string v18, "com.sec.android.app.controlpanel.service.RUNNING_COUNT"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 394
    .restart local v15    # "runningCountIntent":Landroid/content/Intent;
    const-string v18, "runningAppsCount"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 395
    const-string v18, "TaskManager:TaskManagerRemoteService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending new running_count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # setter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I
    invoke-static {v0, v6}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$702(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;I)I

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    const-wide/16 v20, 0x1388

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 407
    .end local v6    # "count":I
    .end local v15    # "runningCountIntent":Landroid/content/Intent;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList()Ljava/util/List;

    move-result-object v13

    .line 409
    .restart local v13    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    .line 411
    .local v14, "runningCount":I
    if-nez v14, :cond_8

    .line 412
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 413
    .local v7, "endAllProcessRefresh":Landroid/content/Intent;
    const-string v18, "com.sec.android.app.controlpanel.service.ENDALL_PROCESS_REFRESH"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 417
    .end local v7    # "endAllProcessRefresh":Landroid/content/Intent;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    const-wide/16 v20, 0x1388

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
