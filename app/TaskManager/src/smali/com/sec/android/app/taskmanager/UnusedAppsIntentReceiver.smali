.class public Lcom/sec/android/app/taskmanager/UnusedAppsIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UnusedAppsIntentReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private AddUnusedAppNotification(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v13, 0x0

    new-instance v14, Landroid/content/Intent;

    const-class v15, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    move-object/from16 v0, p1

    invoke-direct {v14, v0, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14, v15}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 56
    .local v10, "pintent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/android/app/taskmanager/PackageInfo;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v9

    .line 57
    .local v9, "packageInfo":Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstallAppPackageList()Ljava/util/List;

    move-result-object v2

    .line 58
    .local v2, "allList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->ReadLastUsedOverPeriodList(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v1

    .line 59
    .local v1, "OverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->removeOverPeriodPackage(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v3

    .line 60
    .local v3, "filteredList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 62
    .local v4, "nAppCount":I
    if-gtz v4, :cond_0

    .line 63
    const-string v13, "UnusedAppsIntentReceiver"

    const-string v14, "Unused over period is 0"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f080014

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    .local v12, "sbTitle":Ljava/lang/StringBuilder;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    new-instance v13, Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v10}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    const v14, 0x7f020003

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 78
    .local v8, "notiBuilder":Landroid/app/Notification$Builder;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v5

    .line 80
    .local v5, "nIsTestMode":I
    if-nez v5, :cond_1

    .line 81
    const v13, 0x7f080012

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodCurrentValue(Landroid/content/Context;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 89
    :goto_1
    const-string v13, "notification"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    .line 90
    .local v7, "nm":Landroid/app/NotificationManager;
    const/16 v13, 0x205d

    invoke-virtual {v7, v13}, Landroid/app/NotificationManager;->cancel(I)V

    .line 91
    const/16 v13, 0x205d

    invoke-virtual {v8}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v14

    invoke-virtual {v7, v13, v14}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 83
    .end local v7    # "nm":Landroid/app/NotificationManager;
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedPeriod(Landroid/content/Context;)I

    move-result v6

    .line 84
    .local v6, "nWhich":I
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 85
    .local v11, "sHour":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\ufffd\ub8de\ufffd\ube1e \ufffd\uad97\ufffd\uc29c\ufffd\ube2f\uf9de\ufffd \ufffd\ube21\ufffd\ufffd \ufffd\ube4b\ufffd\ubc7e\ufffd\uc4e3 \ufffd\uc197\ufffd\uc524\ufffd\ube2f\ufffd\uaf6d\ufffd\uc282."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    invoke-static {p1}, Lcom/sec/android/app/taskmanager/Utils;->isEnableUnusedAppsControl(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "com.sec.android.app.taskmanager.action.NOTIFY_UNUSED_APPS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const-string v0, "UnusedAppsIntentReceiver"

    const-string v1, "ACTION_NOTIFY_UNUSED_APPS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/taskmanager/UnusedAppsIntentReceiver;->AddUnusedAppNotification(Landroid/content/Context;)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    const-string v0, "UnusedAppsIntentReceiver"

    const-string v1, "BOOT_COMPLETED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-static {p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->calculateAndSetNextNotifyingTime(Landroid/content/Context;)V

    goto :goto_0

    .line 46
    :cond_2
    const-string v0, "android.intent.action.TIME_SET"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "UnusedAppsIntentReceiver"

    const-string v1, "ACTION_TIME_CHANGED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-static {p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->calculateAndSetNextNotifyingTime(Landroid/content/Context;)V

    goto :goto_0
.end method
