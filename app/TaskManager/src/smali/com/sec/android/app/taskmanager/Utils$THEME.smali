.class public final enum Lcom/sec/android/app/taskmanager/Utils$THEME;
.super Ljava/lang/Enum;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "THEME"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/taskmanager/Utils$THEME;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/taskmanager/Utils$THEME;

.field public static final enum DARK:Lcom/sec/android/app/taskmanager/Utils$THEME;

.field public static final enum LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 441
    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$THEME;

    const-string v1, "DARK"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/taskmanager/Utils$THEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$THEME;->DARK:Lcom/sec/android/app/taskmanager/Utils$THEME;

    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$THEME;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/taskmanager/Utils$THEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    .line 440
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/taskmanager/Utils$THEME;

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$THEME;->DARK:Lcom/sec/android/app/taskmanager/Utils$THEME;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$THEME;->$VALUES:[Lcom/sec/android/app/taskmanager/Utils$THEME;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/Utils$THEME;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 440
    const-class v0, Lcom/sec/android/app/taskmanager/Utils$THEME;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/Utils$THEME;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/taskmanager/Utils$THEME;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils$THEME;->$VALUES:[Lcom/sec/android/app/taskmanager/Utils$THEME;

    invoke-virtual {v0}, [Lcom/sec/android/app/taskmanager/Utils$THEME;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/taskmanager/Utils$THEME;

    return-object v0
.end method
