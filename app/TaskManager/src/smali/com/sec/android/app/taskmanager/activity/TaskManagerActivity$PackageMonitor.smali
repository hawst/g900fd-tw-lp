.class Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;
.super Lcom/sec/android/app/taskmanager/monitor/Monitor;
.source "TaskManagerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageMonitor"
.end annotation


# instance fields
.field mFirstTime:Z

.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 1

    .prologue
    .line 427
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;-><init>()V

    .line 428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->mFirstTime:Z

    return-void
.end method


# virtual methods
.method protected getInterval()I
    .locals 1

    .prologue
    .line 431
    const/16 v0, 0xbb8

    return v0
.end method

.method protected onMonitor()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 441
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getList()Ljava/util/List;

    move-result-object v4

    .line 442
    .local v4, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$500(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList(Z)Ljava/util/List;

    move-result-object v3

    .line 443
    .local v3, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-nez v4, :cond_3

    .line 444
    .local v1, "isChanged":Z
    :goto_0
    if-nez v1, :cond_0

    .line 445
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-eq v6, v7, :cond_4

    .line 446
    const/4 v1, 0x1

    .line 458
    :cond_0
    if-nez v1, :cond_1

    iget-boolean v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->mFirstTime:Z

    if-eqz v6, :cond_9

    .line 459
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 460
    .local v2, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->isRunning()Z

    move-result v6

    if-nez v6, :cond_6

    .line 481
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_2
    :goto_2
    return-void

    .end local v1    # "isChanged":Z
    :cond_3
    move v1, v5

    .line 443
    goto :goto_0

    .line 448
    .restart local v1    # "isChanged":Z
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 449
    .restart local v2    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->isRunning()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 452
    invoke-interface {v4, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 453
    const/4 v1, 0x1

    goto :goto_3

    .line 463
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setIntent(Landroid/content/Intent;)V

    .line 464
    const/4 v6, -0x1

    invoke-virtual {v2, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setRecentTaskId(I)V

    .line 465
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$500(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/taskmanager/PackageInfo;->loadIcon(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 466
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$500(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/taskmanager/PackageInfo;->loadLabel(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 467
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new package : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 469
    .end local v2    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$500(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfo;->setRecentTaskIntentInfo()V

    .line 470
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-static {v6}, Lcom/sec/android/app/taskmanager/Utils;->isEnableUnusedAppsControl(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 471
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-static {v6}, Lcom/sec/android/app/taskmanager/Utils;->isLocaleChange(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 472
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->TAG:Ljava/lang/String;

    const-string v7, "Locale Changed Completed!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->saveCache()V

    .line 474
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-static {v6}, Lcom/sec/android/app/taskmanager/Utils;->writeLocaleConfig(Landroid/content/Context;)V

    .line 477
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->sort(Ljava/util/List;)V

    .line 478
    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->publishUi(Ljava/lang/Object;)V

    .line 480
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_9
    iput-boolean v5, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->mFirstTime:Z

    goto/16 :goto_2
.end method

.method protected onStart()Z
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->mFirstTime:Z

    .line 436
    const/4 v0, 0x0

    return v0
.end method

.method protected onUpdateUi(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 485
    move-object v0, p1

    check-cast v0, Ljava/util/List;

    .line 486
    .local v0, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->onChangedPackageList(Ljava/util/List;)V

    .line 487
    return-void
.end method
