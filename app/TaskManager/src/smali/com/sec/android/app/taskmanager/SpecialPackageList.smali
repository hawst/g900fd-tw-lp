.class public Lcom/sec/android/app/taskmanager/SpecialPackageList;
.super Ljava/lang/Object;
.source "SpecialPackageList.java"


# static fields
.field private static instance:Lcom/sec/android/app/taskmanager/SpecialPackageList;

.field private static final mHiddenPackageStringArray:[Ljava/lang/String;

.field private static final mIntentActionAfterKilledPackageStringArray:[[Ljava/lang/String;

.field private static final mMemoryClearHiddennPackageStringArray:[Ljava/lang/String;

.field private static final mMemoryClearHiddennPackageandProcessArray:[Ljava/lang/String;

.field private static final mMustBeShownPackageStringArray:[Ljava/lang/String;


# instance fields
.field private mHiddenPackage:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIntentActionAfterKilledPackage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMemoryClearHiddennPackage:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMemoryClearHiddennPackageForRunningService:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMustBeShownPackage:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.samsung.music"

    aput-object v1, v0, v3

    const-string v1, "com.google.android.youtube"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.music"

    aput-object v1, v0, v5

    const-string v1, "com.android.music"

    aput-object v1, v0, v6

    const-string v1, "com.google.android.music"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "com.ensight.android.radioalarm"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.android.app.voicerecorder"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.sec.android.app.fm"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.samsung.app.fmradio"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.iloen.melon"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.mnet.polgtapp"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.mnet.app"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.mnet.lgtapp"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.neowiz.android.bugs"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.skt.nate.A00000000D"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackageStringArray:[Ljava/lang/String;

    .line 49
    const/16 v0, 0x44

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.sec.android.app.launcher"

    aput-object v1, v0, v3

    const-string v1, "com.sec.android.app.seniorlauncher"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.easylauncher"

    aput-object v1, v0, v5

    const-string v1, "com.sec.android.app.taskmanager"

    aput-object v1, v0, v6

    const-string v1, "com.sec.android.app.controlpanel"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "com.sec.android.app.dialertop"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.android.app.dialer"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.contacts"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.android.phone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.android.providers.telephony"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "android"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "system"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.android.stk"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.android.settings"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.android.bluetoothtest"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.samsung.sec.android.application.csc"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.sec.android.app.callsetting"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.samsung.crashnotifier"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.sec.android.app.factorytest"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.android.settings.mt"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.samsung.mobileTracker.ui"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "com.osp.app.signin"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "com.wipereceiver"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "com.sec.android.app.personalization"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "com.android.Preconfig"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "com.sec.android.app.servicemodeapp"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "com.sec.android.app.wlantest"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "com.sec.android.app.dialertab"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "com.wssyncmldm"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "com.android.samsungtest.DataCreate"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "com.android.setupwizard"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "com.google.android.googleapps"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "com.android.wallpaper"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "com.android.wallpaper.livepicker"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "com.android.magicsmoke"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "com.sec.android.voip"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "com.android.samsungtest.FactoryTest"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "com.sec.android.widgetapp.dualclock"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "com.spritemobile.backup.samsung"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "com.sec.android.app.samsungapps.una"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "com.palmia.qspider"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "com.android.providers.downloads.ui"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "com.sec.android.widgetapp.buddiesnow"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "com.kt.iwlan"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "com.sec.android.provider.logsprovider"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "com.sec.android.widgetapp.SPlannerAppWidget"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "com.sec.android.app.snotebook"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "com.android.mms"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "com.android.systemui"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "com.sec.imsphone"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "com.sec.android.app.simcardmanagement"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "com.android.simcardmanagement"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "com.samsung.simcardmanagement"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "com.sec.android.app.utk"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "com.sec.KidsSettings"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "com.samsung.android.app.assistantmenu"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "jp.netstar.familysmile"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "com.android.incallui"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "com.skt.prod.phone"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "com.skt.prod.phonebook"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "com.skt.prod.dialer"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "com.samsung.android.app.advsounddetector"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "com.samsung.android.app.sounddetector"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "com.google.android.marvin.talkback"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "com.samsung.android.app.cocktailbarservice"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackageStringArray:[Ljava/lang/String;

    .line 122
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageStringArray:[Ljava/lang/String;

    .line 126
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "com.samsung.music"

    aput-object v1, v0, v3

    const-string v1, "com.sec.android.app.music"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageandProcessArray:[Ljava/lang/String;

    .line 131
    new-array v0, v5, [[Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "com.android.email"

    aput-object v2, v1, v3

    const-string v2, "com.android.email.RESTART_SYNCMANAGER"

    aput-object v2, v1, v4

    aput-object v1, v0, v3

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "com.sec.android.socialhub"

    aput-object v2, v1, v3

    const-string v2, "com.sec.android.socialhub.RESTART_PACKAGE_ACTION"

    aput-object v2, v1, v4

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mIntentActionAfterKilledPackageStringArray:[[Ljava/lang/String;

    .line 148
    new-instance v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/SpecialPackageList;-><init>()V

    sput-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->instance:Lcom/sec/android/app/taskmanager/SpecialPackageList;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackage:Ljava/util/Set;

    .line 139
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackage:Ljava/util/Set;

    .line 141
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackage:Ljava/util/Set;

    .line 143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mIntentActionAfterKilledPackage:Ljava/util/HashMap;

    .line 146
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageForRunningService:Ljava/util/Set;

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->updateExcludePackageList()V

    .line 156
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->instance:Lcom/sec/android/app/taskmanager/SpecialPackageList;

    return-object v0
.end method

.method private updateExcludePackageList()V
    .locals 6

    .prologue
    .line 159
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackageStringArray:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 160
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addMustBeShownPackage(Ljava/lang/String;)V

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackageStringArray:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 164
    .restart local v3    # "s":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addHiddenPackage(Ljava/lang/String;)V

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 167
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageStringArray:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 168
    .restart local v3    # "s":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addMemoryClearHiddennPackage(Ljava/lang/String;)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 171
    .end local v3    # "s":Ljava/lang/String;
    :cond_2
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mIntentActionAfterKilledPackageStringArray:[[Ljava/lang/String;

    .local v0, "arr$":[[Ljava/lang/String;
    array-length v2, v0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 172
    .local v3, "s":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v3, v4

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addIntentActionAfterKilledPackage(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 176
    .end local v3    # "s":[Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageandProcessArray:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 177
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addMemoryClearHiddennPackageandProcessPackage(Ljava/lang/String;)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 179
    .end local v3    # "s":Ljava/lang/String;
    :cond_4
    return-void
.end method


# virtual methods
.method public addHiddenPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_0
    return-void
.end method

.method public addIntentActionAfterKilledPackage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mIntentActionAfterKilledPackage:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mIntentActionAfterKilledPackage:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_0
    return-void
.end method

.method public addMemoryClearHiddennPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_0
    return-void
.end method

.method public addMemoryClearHiddennPackageandProcessPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageForRunningService:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMemoryClearHiddennPackageForRunningService:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_0
    return-void
.end method

.method public addMustBeShownPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_0
    return-void
.end method

.method public isHiddenPkg(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mHiddenPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMustBeShownPkg(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/SpecialPackageList;->mMustBeShownPackage:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
