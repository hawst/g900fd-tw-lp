.class public Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;
.super Ljava/lang/Object;
.source "PackageInfoItemFactory.java"


# static fields
.field private static instance:Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private canClearUserData(Landroid/content/pm/ApplicationInfo;)Z
    .locals 1
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 323
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkPermission(Lcom/sec/android/app/taskmanager/PackageInfoItem;Landroid/content/pm/ApplicationInfo;)V
    .locals 6
    .param p1, "packageInfoItem"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "appInfo"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    const-string v0, "com.sec.android.app.memo"

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.ve"

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 121
    :cond_0
    invoke-virtual {p1, v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setHideRunningAppList(Z)V

    .line 131
    :goto_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->isPersistentApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {p1, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setHideRunningAppList(Z)V

    .line 137
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isHideRunningAppList()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    const-string v0, "TaskManager:PackageInfoItemFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HIDE_RUNNING_APP_LIST : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addHiddenPackage(Ljava/lang/String;)V

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "com.sec.android.app.controlpanel.permission.MUST_BE_SHOW_RUNNING_APP_LIST"

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setMustBeShowRunningAppList(Z)V

    .line 146
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isMustBeShowRunningAppList()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    const-string v0, "TaskManager:PackageInfoItemFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MUST_BE_SHOW_RUNNING_APP_LIST : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-static {}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addMustBeShownPackage(Ljava/lang/String;)V

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "com.sec.android.app.controlpanel.permission.GET_INTENT_ACTION_AFTER_FORCESTOP"

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setGetIntentActionAfterForceStop(Z)V

    .line 159
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isGetIntentActionAfterForceStop()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    const-string v0, "TaskManager:PackageInfoItemFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GET_INTENT_ACTION_AFTER_FORCESTOP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-static {}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".RESTART_PACKAGE_ACTION"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addIntentActionAfterKilledPackage(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "com.sec.android.app.controlpanel.permission.EXCLUDE_MEMORY_CLEAN"

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    :goto_3
    invoke-virtual {p1, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setGetIntentActionAfterForceStop(Z)V

    .line 171
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isGetIntentActionAfterForceStop()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 172
    const-string v0, "TaskManager:PackageInfoItemFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXCLUDE_MEMORY_CLEAN : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-static {}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->addMemoryClearHiddennPackage(Ljava/lang/String;)V

    .line 176
    :cond_5
    return-void

    .line 123
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "com.sec.android.app.controlpanel.permission.HIDE_RUNNING_APP_LIST"

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setHideRunningAppList(Z)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v0, v2

    .line 142
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 152
    goto/16 :goto_2

    :cond_a
    move v1, v2

    .line 166
    goto :goto_3
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->instance:Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->instance:Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->instance:Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    return-object v0
.end method

.method private isPersistentApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 1
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 317
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStoredDataOnly(Landroid/content/pm/ApplicationInfo;)Z
    .locals 6
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    const/4 v2, 0x0

    .line 335
    const/4 v0, 0x0

    .line 338
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz p1, :cond_0

    .line 339
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 347
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    .line 341
    :catch_0
    move-exception v1

    .line 342
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "TaskManager:PackageInfoItemFactory"

    const-string v4, "isStoredDataOnly()"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 343
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 344
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "TaskManager:PackageInfoItemFactory"

    const-string v4, "isStoredDataOnly()"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private isStoredExternalStorage(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 351
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSystemApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 1
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 327
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSystemAppUpdate(Landroid/content/pm/ApplicationInfo;)Z
    .locals 1
    .param p1, "a"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 331
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadPackageCacheFile()Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
    .locals 8

    .prologue
    .line 242
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "CachedFile"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 244
    .local v1, "appNameCache":Ljava/io/File;
    const/4 v4, 0x0

    .line 245
    .local v4, "ois":Ljava/io/ObjectInputStream;
    const/4 v2, 0x0

    .line 248
    .local v2, "cache":Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
    :try_start_0
    new-instance v5, Ljava/io/ObjectInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .local v5, "ois":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;

    move-object v2, v0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 255
    if-eqz v5, :cond_3

    .line 257
    :try_start_2
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    :cond_0
    :goto_0
    move-object v6, v2

    .line 263
    :cond_1
    :goto_1
    return-object v6

    .line 258
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v6

    move-object v4, v5

    .line 259
    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_0

    .line 250
    :catch_1
    move-exception v3

    .line 251
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const/4 v6, 0x0

    .line 255
    if-eqz v4, :cond_1

    .line 257
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 258
    :catch_2
    move-exception v7

    goto :goto_1

    .line 252
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 253
    .local v3, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    const-string v6, "TaskManager:PackageInfoItemFactory"

    const-string v7, "Load Cache Exception"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 255
    if-eqz v4, :cond_0

    .line 257
    :try_start_5
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 258
    :catch_4
    move-exception v6

    goto :goto_0

    .line 255
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v4, :cond_2

    .line 257
    :try_start_6
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 259
    :cond_2
    :goto_5
    throw v6

    .line 258
    :catch_5
    move-exception v7

    goto :goto_5

    .line 255
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_4

    .line 252
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_6
    move-exception v3

    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_3

    .line 250
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_7
    move-exception v3

    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_2

    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :cond_3
    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_0
.end method

.method private savePackageCacheFile(Lcom/sec/android/app/taskmanager/PackageInfoItemCache;)Z
    .locals 9
    .param p1, "packageCache"    # Lcom/sec/android/app/taskmanager/PackageInfoItemCache;

    .prologue
    .line 270
    new-instance v0, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "CachedFile"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .local v0, "appNameCache":Ljava/io/File;
    const/4 v4, 0x0

    .line 273
    .local v4, "oos":Ljava/io/ObjectOutputStream;
    const/4 v2, 0x0

    .line 274
    .local v2, "fos":Ljava/io/FileOutputStream;
    const/4 v6, 0x1

    .line 277
    .local v6, "result":Z
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 279
    .end local v4    # "oos":Ljava/io/ObjectOutputStream;
    .local v5, "oos":Ljava/io/ObjectOutputStream;
    :try_start_2
    invoke-virtual {v5, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 280
    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 286
    .end local v5    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v4    # "oos":Ljava/io/ObjectOutputStream;
    :goto_0
    if-eqz v4, :cond_0

    .line 288
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 289
    const/4 v4, 0x0

    .line 294
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 296
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 297
    const/4 v2, 0x0

    .line 302
    :cond_1
    :goto_2
    return v6

    .line 281
    :catch_0
    move-exception v1

    .line 282
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    const-string v7, "TaskManager:PackageInfoItemFactory"

    const-string v8, "Save Cache Exception"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    const/4 v6, 0x0

    goto :goto_0

    .line 290
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_1

    .line 298
    :catch_2
    move-exception v7

    goto :goto_2

    .line 281
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "oos":Ljava/io/ObjectOutputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v4    # "oos":Ljava/io/ObjectOutputStream;
    goto :goto_3
.end method

.method private updatePackageInfoItem(Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageInfoItem"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 86
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 88
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->canClearUserData(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setCanClearData(Z)V

    .line 89
    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setSystemApp(Z)V

    .line 90
    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->isSystemAppUpdate(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setSystemAppUpdate(Z)V

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 93
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-wide v4, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setInstalledTime(J)V

    .line 94
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setSizeComputed(Z)V

    .line 95
    sget-object v3, Lcom/sec/android/app/taskmanager/PackageInfoItem;->DefaultAppIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 96
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppName(Ljava/lang/CharSequence;)V

    .line 98
    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->isStoredDataOnly(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    const/4 v3, 0x2

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setStoredExternalStorage(I)V

    .line 112
    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->checkPermission(Lcom/sec/android/app/taskmanager/PackageInfoItem;Landroid/content/pm/ApplicationInfo;)V

    .line 116
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    return-void

    .line 100
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->isStoredExternalStorage(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setStoredExternalStorage(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "TaskManager:PackageInfoItemFactory"

    const-string v4, "getPackageInfoItem() NameNotFoundException"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 104
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {p2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setStoredExternalStorage(I)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public clearCache()V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CachedFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 229
    .local v0, "appNameCache":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 231
    const-string v1, "TaskManager:PackageInfoItemFactory"

    const-string v2, "clearCache() deleted!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    const-string v1, "TaskManager:PackageInfoItemFactory"

    const-string v2, "clearCache() not deleted!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 75
    .local v0, "packageInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local v0    # "packageInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-direct {v0, p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;-><init>(Ljava/lang/String;)V

    .line 77
    .restart local v0    # "packageInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->updatePackageInfoItem(Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 81
    :cond_0
    return-object v0
.end method

.method public loadCache()V
    .locals 8

    .prologue
    .line 182
    const-string v5, "TaskManager:PackageInfoItemFactory"

    const-string v6, "loadCache() start!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->loadPackageCacheFile()Lcom/sec/android/app/taskmanager/PackageInfoItemCache;

    move-result-object v0

    .line 185
    .local v0, "cache":Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
    if-eqz v0, :cond_1

    .line 186
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 187
    .local v2, "entryItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 188
    .local v4, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;->get(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;

    move-result-object v1

    .line 190
    .local v1, "cachedItem":Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;
    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppName(Ljava/lang/CharSequence;)V

    .line 192
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->getCodeSize()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setCodeSize(J)V

    .line 193
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->getDataSize()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setDataSize(J)V

    .line 194
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setSizeComputed(Z)V

    goto :goto_0

    .line 199
    .end local v1    # "cachedItem":Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;
    .end local v2    # "entryItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    const-string v5, "TaskManager:PackageInfoItemFactory"

    const-string v6, "loadCache() end!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    return-void
.end method

.method public reloadAppName()V
    .locals 4

    .prologue
    .line 309
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 310
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 311
    .local v2, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppName(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 313
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .end local v2    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_0
    return-void
.end method

.method public saveCache()V
    .locals 10

    .prologue
    .line 206
    const-string v2, "TaskManager:PackageInfoItemFactory"

    const-string v3, "saveCache() start!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    new-instance v1, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;-><init>()V

    .line 209
    .local v1, "cache":Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->mPackageInfoMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 210
    .local v0, "entryItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 212
    .local v9, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 213
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCodeSize()J

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getDataSize()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;->put(Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_0

    .line 218
    .end local v0    # "entryItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .end local v9    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->savePackageCacheFile(Lcom/sec/android/app/taskmanager/PackageInfoItemCache;)Z

    .line 219
    const-string v2, "TaskManager:PackageInfoItemFactory"

    const-string v3, "saveCache() end!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return-void
.end method

.method public updatePackageInfoItem(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->updatePackageInfoItem(Ljava/lang/String;Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 64
    return-void
.end method
