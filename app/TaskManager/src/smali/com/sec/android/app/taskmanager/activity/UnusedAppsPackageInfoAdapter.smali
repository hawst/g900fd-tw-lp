.class public Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
.super Landroid/widget/BaseAdapter;
.source "UnusedAppsPackageInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;
    }
.end annotation


# instance fields
.field private buttonPressed:Z

.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private scrollState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->buttonPressed:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method

.method private getDateString(J)Ljava/lang/String;
    .locals 5
    .param p1, "timestamp"    # J

    .prologue
    .line 88
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy/MM/dd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 89
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 90
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "dateTime":Ljava/lang/String;
    return-object v1
.end method


# virtual methods
.method public getBaseAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 73
    int-to-long v0, p1

    return-wide v0
.end method

.method public getScrollState()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->scrollState:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 94
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    move/from16 v0, p1

    if-lt v0, v9, :cond_0

    .line 95
    const-string v9, "UnusedAppsPackageInfoAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid view position:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", actual size is:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v9, 0x0

    .line 175
    :goto_0
    return-object v9

    .line 101
    :cond_0
    if-nez p2, :cond_3

    .line 102
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f030001

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 103
    new-instance v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;

    invoke-direct {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;-><init>()V

    .line 104
    .local v4, "holder":Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;
    const v9, 0x7f0c0002

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    .line 105
    const v9, 0x7f0c0001

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    .line 106
    const v9, 0x7f0c0003

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    .line 107
    const v9, 0x7f0c0005

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    .line 108
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const v10, 0x7f08001d

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setText(I)V

    .line 109
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 114
    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 116
    .local v5, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    if-eqz v5, :cond_2

    .line 117
    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isCanKilled()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 130
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 131
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 141
    :goto_2
    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v9

    if-nez v9, :cond_6

    .line 142
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :goto_3
    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getlastAccessedOverPeriodTime()J

    move-result-wide v6

    .line 150
    .local v6, "t":J
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-gez v9, :cond_1

    .line 151
    const-wide/16 v6, 0x0

    .line 154
    :cond_1
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 155
    .local v3, "date":Ljava/util/Date;
    invoke-virtual {v3}, Ljava/util/Date;->getYear()I

    move-result v9

    add-int/lit16 v8, v9, 0x76c

    .line 156
    .local v8, "year":I
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getDateString(J)Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "Ldate":[Ljava/lang/String;
    add-int/lit8 v9, v8, -0x5

    const/4 v10, 0x0

    aget-object v10, v2, v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v9, v10, :cond_7

    .line 159
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f08001e

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getInstalledTime()J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getDateString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 166
    :goto_4
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setFocusable(Z)V

    .line 167
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setClickable(Z)V

    .line 168
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 169
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 170
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08001d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 172
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .end local v2    # "Ldate":[Ljava/lang/String;
    .end local v3    # "date":Ljava/util/Date;
    .end local v6    # "t":J
    .end local v8    # "year":I
    :cond_2
    move-object/from16 v9, p2

    .line 175
    goto/16 :goto_0

    .line 111
    .end local v4    # "holder":Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;
    .end local v5    # "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;

    .restart local v4    # "holder":Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;
    goto/16 :goto_1

    .line 134
    .restart local v5    # "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isCanKilled()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 135
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_2

    .line 137
    :cond_5
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->button:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_2

    .line 144
    :cond_6
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 162
    .restart local v2    # "Ldate":[Ljava/lang/String;
    .restart local v3    # "date":Ljava/util/Date;
    .restart local v6    # "t":J
    .restart local v8    # "year":I
    :cond_7
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appDetail:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f08001e

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getDateString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v9, v4, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter$AppViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4
.end method

.method public isButtonPressed()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->buttonPressed:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->isButtonPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getScrollState()I

    move-result v0

    if-nez v0, :cond_0

    .line 192
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 194
    :cond_0
    return-void
.end method

.method public setButtonPressed(Z)V
    .locals 0
    .param p1, "buttonPressed"    # Z

    .prologue
    .line 187
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->buttonPressed:Z

    .line 188
    return-void
.end method

.method public setList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    .line 80
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 83
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "touchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 86
    return-void
.end method

.method public setScrollState(I)V
    .locals 0
    .param p1, "scrollState"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->scrollState:I

    .line 182
    return-void
.end method

.method public sort()V
    .locals 4

    .prologue
    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mAppList:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UnusedAppsPackageInfoAdapter"

    const-string v2, "sort() exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
