.class public Lcom/sec/android/app/taskmanager/PackageInfoItem;
.super Ljava/lang/Object;
.source "PackageInfoItem.java"


# static fields
.field public static DefaultAppIcon:Landroid/graphics/drawable/Drawable;

.field private static sLastIdx:I


# instance fields
.field private appIcon:Landroid/graphics/drawable/Drawable;

.field private appName:Ljava/lang/CharSequence;

.field private canClearData:Z

.field private canKilled:Z

.field private codeSize:J

.field private cpuUsage:F

.field private cpuUsageString:Ljava/lang/String;

.field private dataSize:J

.field private getIntentActionAfterForceStop:Z

.field private hideRunningAppList:Z

.field private holder:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

.field private id:I

.field private installedTime:J

.field private intent:Landroid/content/Intent;

.field private mFreqCount:I

.field private mLastUsedOverPeriodCount:J

.field private mPidList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPids:[I

.field private memUsage:J

.field private memUsageString:Ljava/lang/String;

.field private mustBeShowRunningAppList:Z

.field private packageName:Ljava/lang/String;

.field private processStartTime:J

.field private recentId:I

.field private recentTaskOrder:I

.field private service:Z

.field private sizeComputed:Z

.field private storedExternalStorage:I

.field private systemApp:Z

.field private systemAppUpdate:Z

.field private task:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->DefaultAppIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->canKilled:Z

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mFreqCount:I

    .line 114
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->processStartTime:J

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->recentTaskOrder:I

    .line 127
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->packageName:Ljava/lang/String;

    .line 128
    sget-object v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->DefaultAppIcon:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 129
    sget v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->sLastIdx:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;->sLastIdx:I

    iput v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->id:I

    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setCpuUsage(F)V

    .line 131
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setMemUsage(J)V

    .line 132
    return-void
.end method


# virtual methods
.method public clearPids()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 190
    monitor-enter p0

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 192
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    .line 196
    return-void

    .line 192
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getAppIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->appIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppName()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->appName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isTask()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    const/4 v0, 0x1

    .line 382
    :goto_0
    return v0

    .line 378
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isService()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    const/4 v0, 0x2

    goto :goto_0

    .line 382
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getCodeSize()J
    .locals 2

    .prologue
    .line 331
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->codeSize:J

    return-wide v0
.end method

.method public getCpuRateFormatted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->cpuUsageString:Ljava/lang/String;

    return-object v0
.end method

.method public getCpuUsage()F
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->cpuUsage:F

    return v0
.end method

.method public getDataSize()J
    .locals 2

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->dataSize:J

    return-wide v0
.end method

.method public getInstalledTime()J
    .locals 2

    .prologue
    .line 488
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->installedTime:J

    return-wide v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getLastAccessedOverPeriodCount(Landroid/content/Context;Ljava/lang/String;)J
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "AppPackageName"    # Ljava/lang/String;

    .prologue
    .line 493
    const-string v4, "TaskManager:PackageInfoItem"

    const-string v5, "getLastAccessedCountOverPeriod()"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    move-object v1, p1

    .line 495
    .local v1, "mContext":Landroid/content/Context;
    check-cast v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .end local v1    # "mContext":Landroid/content/Context;
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->returnLastUsedOverPeriodList()Ljava/util/HashMap;

    move-result-object v0

    .line 498
    .local v0, "lastOverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 499
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 500
    .local v2, "lastUsedOverPeriodCount":J
    const-string v4, "TaskManager:PackageInfoItem"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "lastUsedOverPeriodCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iput-wide v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mLastUsedOverPeriodCount:J

    .line 507
    .end local v2    # "lastUsedOverPeriodCount":J
    :goto_0
    return-wide v2

    .line 505
    :cond_0
    const-string v4, "TaskManager:PackageInfoItem"

    const-string v5, "lastUsedOverPeriodCount = install time"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getInstalledTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mLastUsedOverPeriodCount:J

    .line 507
    iget-wide v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mLastUsedOverPeriodCount:J

    goto :goto_0
.end method

.method public getMemUsage()J
    .locals 2

    .prologue
    .line 284
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->memUsage:J

    return-wide v0
.end method

.method public getMemUsageString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->memUsageString:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPids()[I
    .locals 5

    .prologue
    .line 158
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    if-eqz v4, :cond_1

    .line 159
    monitor-enter p0

    .line 160
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    .line 161
    const/4 v0, 0x0

    .line 163
    .local v0, "i":I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 164
    .local v3, "pid":I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aput v3, v4, v1

    move v1, v0

    .line 165
    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 166
    .end local v3    # "pid":I
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    return-object v4

    .line 166
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public getProcessStartTime()J
    .locals 2

    .prologue
    .line 530
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->processStartTime:J

    return-wide v0
.end method

.method public getRecentTaskId()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->recentId:I

    return v0
.end method

.method public getViewHolder()Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->holder:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->holder:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;->pkgInfo:Lcom/sec/android/app/taskmanager/PackageInfoItem;

    if-ne v0, p0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->holder:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .line 557
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getlastAccessedOverPeriodTime()J
    .locals 2

    .prologue
    .line 512
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mLastUsedOverPeriodCount:J

    return-wide v0
.end method

.method public isCanKilled()Z
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->canKilled:Z

    return v0
.end method

.method public isGetIntentActionAfterForceStop()Z
    .locals 1

    .prologue
    .line 480
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getIntentActionAfterForceStop:Z

    return v0
.end method

.method public isHideRunningAppList()Z
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->hideRunningAppList:Z

    return v0
.end method

.method public isMustBeShowRunningAppList()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mustBeShowRunningAppList:Z

    return v0
.end method

.method public isService()Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->service:Z

    return v0
.end method

.method public isSystemApp()Z
    .locals 1

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->systemApp:Z

    return v0
.end method

.method public isSystemAppUpdate()Z
    .locals 1

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->systemAppUpdate:Z

    return v0
.end method

.method public isTask()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->task:Z

    return v0
.end method

.method public putPid(I)V
    .locals 2
    .param p1, "pid"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    .line 206
    :cond_0
    monitor-enter p0

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPidList:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mPids:[I

    .line 211
    return-void

    .line 208
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setAppIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "appIcon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 271
    return-void
.end method

.method public setAppName(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/CharSequence;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->appName:Ljava/lang/CharSequence;

    .line 225
    return-void
.end method

.method public setCanClearData(Z)V
    .locals 0
    .param p1, "canClearData"    # Z

    .prologue
    .line 424
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->canClearData:Z

    .line 425
    return-void
.end method

.method public setCodeSize(J)V
    .locals 1
    .param p1, "codeSize"    # J

    .prologue
    .line 338
    iput-wide p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->codeSize:J

    .line 339
    return-void
.end method

.method public setCpuUsage(F)V
    .locals 5
    .param p1, "cpuUsage"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 135
    iput p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->cpuUsage:F

    .line 136
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 138
    .local v0, "f":Ljava/util/Formatter;
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->isRTL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string v1, "%%%2.2f"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->cpuUsageString:Ljava/lang/String;

    .line 144
    :goto_0
    invoke-virtual {v0}, Ljava/util/Formatter;->close()V

    .line 145
    return-void

    .line 141
    :cond_0
    const-string v1, "%2.2f%%"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->cpuUsageString:Ljava/lang/String;

    goto :goto_0
.end method

.method public setDataSize(J)V
    .locals 1
    .param p1, "storageData"    # J

    .prologue
    .line 324
    iput-wide p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->dataSize:J

    .line 325
    return-void
.end method

.method public setGetIntentActionAfterForceStop(Z)V
    .locals 0
    .param p1, "getIntentActionAfterForceStop"    # Z

    .prologue
    .line 476
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getIntentActionAfterForceStop:Z

    .line 477
    return-void
.end method

.method public setHideRunningAppList(Z)V
    .locals 0
    .param p1, "hideRunningAppList"    # Z

    .prologue
    .line 460
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->hideRunningAppList:Z

    .line 461
    return-void
.end method

.method public setInstalledTime(J)V
    .locals 1
    .param p1, "installedTime"    # J

    .prologue
    .line 484
    iput-wide p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->installedTime:J

    .line 485
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 404
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->intent:Landroid/content/Intent;

    .line 405
    return-void
.end method

.method public setMemUsage(J)V
    .locals 9
    .param p1, "memUsage"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x44800000    # 1024.0f

    .line 291
    iput-wide p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->memUsage:J

    .line 292
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 294
    .local v0, "f":Ljava/util/Formatter;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "megabyteShort"

    const-string v4, "string"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 296
    .local v1, "resId":I
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->isRTL()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%2.2f"

    new-array v4, v8, [Ljava/lang/Object;

    long-to-float v5, p1

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->memUsageString:Ljava/lang/String;

    .line 303
    :goto_0
    invoke-virtual {v0}, Ljava/util/Formatter;->close()V

    .line 304
    return-void

    .line 300
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%2.2f"

    new-array v4, v8, [Ljava/lang/Object;

    long-to-float v5, p1

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->memUsageString:Ljava/lang/String;

    goto :goto_0
.end method

.method public setMustBeShowRunningAppList(Z)V
    .locals 0
    .param p1, "mustBeShowRunningAppList"    # Z

    .prologue
    .line 468
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->mustBeShowRunningAppList:Z

    .line 469
    return-void
.end method

.method public setProcessStartTime(J)V
    .locals 1
    .param p1, "processStartTime"    # J

    .prologue
    .line 526
    iput-wide p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->processStartTime:J

    .line 527
    return-void
.end method

.method public setRecentTaskId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->recentId:I

    .line 255
    return-void
.end method

.method public setRecentTaskOrder(I)V
    .locals 0
    .param p1, "recentTaskOrder"    # I

    .prologue
    .line 534
    iput p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->recentTaskOrder:I

    .line 535
    return-void
.end method

.method public setService(Z)V
    .locals 0
    .param p1, "service"    # Z

    .prologue
    .line 448
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->service:Z

    .line 449
    return-void
.end method

.method public setSizeComputed(Z)V
    .locals 0
    .param p1, "sizeComputed"    # Z

    .prologue
    .line 408
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->sizeComputed:Z

    .line 409
    return-void
.end method

.method public setStoredExternalStorage(I)V
    .locals 0
    .param p1, "storedExternalStorage"    # I

    .prologue
    .line 452
    iput p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->storedExternalStorage:I

    .line 453
    return-void
.end method

.method public setSystemApp(Z)V
    .locals 0
    .param p1, "systemApp"    # Z

    .prologue
    .line 432
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->systemApp:Z

    .line 433
    return-void
.end method

.method public setSystemAppUpdate(Z)V
    .locals 0
    .param p1, "systemAppUpdate"    # Z

    .prologue
    .line 436
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->systemAppUpdate:Z

    .line 437
    return-void
.end method

.method public setTask(Z)V
    .locals 0
    .param p1, "task"    # Z

    .prologue
    .line 444
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->task:Z

    .line 445
    return-void
.end method

.method public setViewHolder(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V
    .locals 0
    .param p1, "holder"    # Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .prologue
    .line 564
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItem;->holder:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    .line 565
    return-void
.end method
