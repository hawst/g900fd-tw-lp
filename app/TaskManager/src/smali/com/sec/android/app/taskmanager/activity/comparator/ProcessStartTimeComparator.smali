.class public Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;
.super Ljava/lang/Object;
.source "ProcessStartTimeComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I
    .locals 7
    .param p1, "a"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "b"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 44
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v6

    if-nez v6, :cond_2

    :cond_0
    move v4, v5

    .line 63
    :cond_1
    :goto_0
    return v4

    .line 48
    :cond_2
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 55
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getProcessStartTime()J

    move-result-wide v0

    .line 56
    .local v0, "aTime":J
    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getProcessStartTime()J

    move-result-wide v2

    .line 58
    .local v2, "bTime":J
    cmp-long v6, v0, v2

    if-gtz v6, :cond_1

    .line 60
    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    .line 61
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    move v4, v5

    .line 63
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/comparator/ProcessStartTimeComparator;->compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I

    move-result v0

    return v0
.end method

.method public final updateStartTime(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getBootTime()J

    move-result-wide v0

    .line 30
    .local v0, "bootTime":J
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 31
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 32
    .local v3, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPids()[I

    move-result-object v4

    .line 34
    .local v4, "pids":[I
    if-eqz v4, :cond_0

    array-length v5, v4

    if-lez v5, :cond_0

    .line 35
    const/4 v5, 0x0

    aget v5, v4, v5

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/Utils;->getProcessStartTime(I)J

    move-result-wide v6

    add-long/2addr v6, v0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setProcessStartTime(J)V

    goto :goto_0

    .line 37
    :cond_0
    const-wide/16 v6, -0x1

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setProcessStartTime(J)V

    goto :goto_0

    .line 41
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v4    # "pids":[I
    :cond_1
    return-void
.end method
