.class Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;
.super Ljava/lang/Object;
.source "TaskManagerActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 334
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 349
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 336
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/Utils;->isLocaleChange(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "Locale Changed!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->reloadAppName()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->clearCache()V

    goto :goto_0

    .line 343
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # invokes: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateTotalMemory()V
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$200(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 334
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method
