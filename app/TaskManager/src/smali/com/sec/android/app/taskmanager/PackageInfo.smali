.class public Lcom/sec/android/app/taskmanager/PackageInfo;
.super Ljava/lang/Object;
.source "PackageInfo.java"


# static fields
.field private static DEBUG:Z

.field private static instance:Lcom/sec/android/app/taskmanager/PackageInfo;


# instance fields
.field private killMethod:Ljava/lang/reflect/Method;

.field private killMethod_force:Ljava/lang/reflect/Method;

.field private mAm:Landroid/app/ActivityManager;

.field private mContext:Landroid/content/Context;

.field private mDbgRunningAppProcInfos:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceType:Ljava/lang/String;

.field private mPm:Landroid/content/pm/PackageManager;

.field mRunningAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field mRunningAppPidMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field public mUpdateHandle:Landroid/os/Handler;

.field private m_bUpdateRunningApplicationInfoOnProgress:Z

.field private toast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    .line 77
    new-instance v0, Lcom/sec/android/app/taskmanager/PackageInfo;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/PackageInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/taskmanager/PackageInfo;->instance:Lcom/sec/android/app/taskmanager/PackageInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppList:Ljava/util/List;

    .line 83
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    .line 87
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/taskmanager/PackageInfo$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/PackageInfo$1;-><init>(Lcom/sec/android/app/taskmanager/PackageInfo;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mUpdateHandle:Landroid/os/Handler;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->toast:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/taskmanager/PackageInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/PackageInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z

    return p1
.end method

.method private getInstalledAppList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    const-string v4, "TaskManager:PackageInfo"

    const-string v5, "getInstalledAppList()"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 373
    .local v3, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    const/16 v5, 0x2000

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v2

    .line 376
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 377
    .local v0, "entry":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 381
    .end local v0    # "entry":Landroid/content/pm/ApplicationInfo;
    :cond_0
    return-object v3
.end method

.method public static getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/taskmanager/PackageInfo;->instance:Lcom/sec/android/app/taskmanager/PackageInfo;

    return-object v0
.end method

.method private getKillPackageMethod(Z)Ljava/lang/reflect/Method;
    .locals 7
    .param p1, "bForce"    # Z

    .prologue
    .line 851
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 853
    .local v1, "sdkVersion":I
    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 855
    :try_start_0
    const-class v2, Landroid/app/ActivityManager;

    const-string v3, "restartPackage"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 880
    :goto_0
    return-object v2

    .line 856
    :catch_0
    move-exception v0

    .line 857
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 880
    .end local v0    # "e":Ljava/lang/SecurityException;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 858
    :catch_1
    move-exception v0

    .line 859
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 868
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :cond_0
    if-eqz p1, :cond_1

    .line 869
    :try_start_1
    const-class v2, Landroid/app/ActivityManager;

    const-string v3, "forceStopPackage"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    goto :goto_0

    .line 871
    :cond_1
    const-class v2, Landroid/app/ActivityManager;

    const-string v3, "killBackgroundProcesses"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    goto :goto_0

    .line 873
    :catch_2
    move-exception v0

    .line 874
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 875
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_3
    move-exception v0

    .line 876
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1
.end method

.method private getLaunchers()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 559
    .local v4, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 560
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 561
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 562
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 564
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 565
    .local v3, "r":Landroid/content/pm/ResolveInfo;
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v5

    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 569
    .end local v3    # "r":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-object v4
.end method

.method private getRunningAppProcesses()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 512
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 513
    .local v5, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mAm:Landroid/app/ActivityManager;

    invoke-virtual {v7}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v6

    .line 514
    .local v6, "runningAppProcInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 517
    .local v0, "existCheck":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_4

    .line 518
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 519
    .local v4, "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v7, v7

    if-ge v1, v7, :cond_0

    .line 520
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v7

    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v8, v8, v1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v3

    .line 523
    .local v3, "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 524
    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->clearPids()V

    .line 530
    :cond_1
    iget v7, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v3, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->putPid(I)V

    .line 531
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDbgRunningAppProcInfos:Ljava/util/Set;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDbgRunningAppProcInfos:Ljava/util/Set;

    invoke-interface {v7, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 536
    :cond_2
    const-string v7, "TaskManager:PackageInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getRunningAppProcesses() - proc:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", pkg:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", uid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", pid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 544
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v4    # "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_4
    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDbgRunningAppProcInfos:Ljava/util/Set;

    .line 545
    return-object v5
.end method

.method private getRunningServices()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 480
    .local v2, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mAm:Landroid/app/ActivityManager;

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v4

    .line 482
    .local v4, "serviceInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v4, :cond_1

    .line 483
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 484
    .local v3, "s":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-boolean v5, v3, Landroid/app/ActivityManager$RunningServiceInfo;->started:Z

    if-eqz v5, :cond_0

    .line 489
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v5

    iget-object v6, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v1

    .line 491
    .local v1, "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setService(Z)V

    .line 492
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 497
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v3    # "s":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_1
    return-object v2
.end method

.method private getRunningTasks(Z)Ljava/util/List;
    .locals 7
    .param p1, "bAppMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 395
    .local v3, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mAm:Landroid/app/ActivityManager;

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 397
    .local v4, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v4, :cond_2

    .line 398
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 399
    .local v0, "i":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->numRunning:I

    if-lez v5, :cond_0

    .line 402
    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 407
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v2

    .line 409
    .local v2, "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 416
    .end local v2    # "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v2

    .line 418
    .restart local v2    # "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v2

    .line 421
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 427
    .end local v0    # "i":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pkgInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_2
    if-nez p1, :cond_3

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->setRecentTaskIntentInfo()V

    .line 431
    :cond_3
    return-object v3
.end method


# virtual methods
.method public getInstallAppPackageList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    const-string v4, "TaskManager:PackageInfo"

    const-string v5, "getInstallAppPackageList()"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v3, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstalledAppList()Ljava/util/List;

    move-result-object v1

    .line 188
    .local v1, "installedPackgeInfoItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 189
    .local v2, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isSystemApp()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isSystemAppUpdate()Z

    move-result v4

    if-nez v4, :cond_0

    .line 190
    const-string v4, "TaskManager:PackageInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getInstallAppPackageList : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v2    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    return-object v3
.end method

.method public getRunningAppPackageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRunningAppPackageList(Z)Ljava/util/List;
    .locals 28
    .param p1, "bAppMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_0

    .line 234
    const-string v22, "TaskManager:PackageInfo"

    const-string v23, "getRunningAppPackageList()"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .local v14, "retList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 239
    .local v3, "existCheck":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 240
    .local v15, "runnningPackageInfoItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v16

    .line 241
    .local v16, "runnningProcess":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->getLaunchers()Ljava/util/List;

    move-result-object v9

    .line 243
    .local v9, "lunchers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 244
    .local v13, "process":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setTask(Z)V

    .line 245
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setService(Z)V

    goto :goto_0

    .line 248
    .end local v13    # "process":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningTasks(Z)Ljava/util/List;

    move-result-object v18

    .line 250
    .local v18, "runnningTask":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 251
    .local v21, "task":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 252
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 253
    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setTask(Z)V

    .line 254
    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 256
    :cond_3
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_2

    .line 257
    const-string v22, "TaskManager:PackageInfo"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "[NO PID]Ignore task : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 263
    .end local v21    # "task":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningServices()Ljava/util/List;

    move-result-object v17

    .line 265
    .local v17, "runnningService":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 266
    .local v19, "service":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 267
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 268
    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setService(Z)V

    .line 269
    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 271
    :cond_6
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_5

    .line 272
    const-string v22, "TaskManager:PackageInfo"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "[NO PID]Ignore service : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 278
    .end local v19    # "service":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_7
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 279
    .local v8, "luncher":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-interface {v15, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 282
    .end local v8    # "luncher":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 283
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/util/SparseArray;->clear()V

    .line 284
    invoke-static {}, Lcom/sec/android/app/taskmanager/MemoryManager;->getInstance()Lcom/sec/android/app/taskmanager/MemoryManager;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/MemoryManager;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/MemoryManager;

    move-result-object v10

    .line 285
    .local v10, "memInfo":Lcom/sec/android/app/taskmanager/MemoryManager;
    invoke-static {}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->getInstance()Lcom/sec/android/app/taskmanager/SpecialPackageList;

    move-result-object v20

    .line 287
    .local v20, "specialPackage":Lcom/sec/android/app/taskmanager/SpecialPackageList;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_9
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 291
    .local v6, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->isMustBeShownPkg(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_a

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isTask()Z

    move-result v22

    if-nez v22, :cond_a

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isService()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 293
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_9

    .line 294
    const-string v22, "TaskManager:PackageInfo"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "RunningApp[Service] - pkg:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 344
    .end local v6    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v10    # "memInfo":Lcom/sec/android/app/taskmanager/MemoryManager;
    .end local v20    # "specialPackage":Lcom/sec/android/app/taskmanager/SpecialPackageList;
    :catchall_0
    move-exception v22

    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v22

    .line 302
    .restart local v6    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .restart local v10    # "memInfo":Lcom/sec/android/app/taskmanager/MemoryManager;
    .restart local v20    # "specialPackage":Lcom/sec/android/app/taskmanager/SpecialPackageList;
    :cond_a
    :try_start_1
    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/SpecialPackageList;->isHiddenPkg(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 303
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_9

    .line 304
    const-string v22, "TaskManager:PackageInfo"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "RunningApp[Hidden Pkg] - pkg:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 312
    :cond_b
    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsage()J

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmp-long v22, v24, v26

    if-nez v22, :cond_c

    .line 313
    invoke-virtual {v10, v6}, Lcom/sec/android/app/taskmanager/MemoryManager;->getPackageUsedMem(Lcom/sec/android/app/taskmanager/PackageInfoItem;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setMemUsage(J)V

    .line 315
    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsage()J

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmp-long v22, v24, v26

    if-nez v22, :cond_c

    .line 316
    sget-boolean v22, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-eqz v22, :cond_9

    .line 317
    const-string v22, "TaskManager:PackageInfo"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "RunningApp[Zero Mem] - pkg:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 335
    :cond_c
    invoke-interface {v14, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    invoke-virtual {v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPids()[I

    move-result-object v12

    .line 338
    .local v12, "pids":[I
    if-eqz v12, :cond_9

    .line 339
    move-object v2, v12

    .local v2, "arr$":[I
    array-length v7, v2

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_5
    if-ge v5, v7, :cond_9

    aget v11, v2, v5

    .line 340
    .local v11, "p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 339
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 344
    .end local v2    # "arr$":[I
    .end local v5    # "i$":I
    .end local v6    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v7    # "len$":I
    .end local v11    # "p":I
    .end local v12    # "pids":[I
    :cond_d
    monitor-exit v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    return-object v14
.end method

.method public getRunningApplicationList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    const-string v0, "TaskManager:PackageInfo"

    const-string v1, "getRunningApplicationList() = "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppList:Ljava/util/List;

    return-object v0
.end method

.method public getRunningPids()[I
    .locals 5

    .prologue
    .line 356
    const/4 v1, 0x0

    .line 358
    .local v1, "pids":[I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    monitor-enter v4

    .line 359
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 360
    .local v2, "size":I
    new-array v1, v2, [I

    .line 362
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 363
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppPidMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    aput v3, v1, v0

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 365
    :cond_0
    monitor-exit v4

    .line 367
    return-object v1

    .line 365
    .end local v0    # "i":I
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfo;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 119
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x103012b

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    .line 129
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mAm:Landroid/app/ActivityManager;

    .line 131
    sget-boolean v0, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/Utils;->isDebug(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    sput-boolean v0, Lcom/sec/android/app/taskmanager/PackageInfo;->DEBUG:Z

    .line 134
    :cond_1
    return-object p0

    .line 126
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 131
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isLightTheme()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    const-string v1, "lightTheme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 143
    :goto_0
    return v0

    .line 142
    :cond_0
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mDeviceType:Ljava/lang/String;

    const-string v1, "lightTheme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isThereAppCanBeKilled(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 884
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-eqz p1, :cond_1

    .line 885
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 886
    .local v1, "p":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isCanKilled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 887
    const/4 v2, 0x1

    .line 892
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "p":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isUpdateRunningApplicationListOnProgress()Z
    .locals 3

    .prologue
    .line 206
    const-string v0, "TaskManager:PackageInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isUpdateRunningApplicationListOnProgress() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z

    return v0
.end method

.method public killPackage(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "bForce"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 818
    const/4 v1, 0x0

    .line 820
    .local v1, "curMethod":Ljava/lang/reflect/Method;
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod:Ljava/lang/reflect/Method;

    if-nez v3, :cond_0

    .line 821
    invoke-direct {p0, v4}, Lcom/sec/android/app/taskmanager/PackageInfo;->getKillPackageMethod(Z)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod:Ljava/lang/reflect/Method;

    .line 824
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod_force:Ljava/lang/reflect/Method;

    if-nez v3, :cond_1

    .line 825
    invoke-direct {p0, v5}, Lcom/sec/android/app/taskmanager/PackageInfo;->getKillPackageMethod(Z)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod_force:Ljava/lang/reflect/Method;

    .line 828
    :cond_1
    if-eqz p2, :cond_3

    .line 829
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod_force:Ljava/lang/reflect/Method;

    .line 834
    :goto_0
    if-eqz v1, :cond_2

    .line 835
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 839
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 848
    .end local v0    # "am":Landroid/app/ActivityManager;
    :cond_2
    :goto_1
    return-void

    .line 831
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->killMethod:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 840
    .restart local v0    # "am":Landroid/app/ActivityManager;
    :catch_0
    move-exception v2

    .line 841
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 842
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 843
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 844
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 845
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public loadIcon(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 10
    .param p1, "item"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 896
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/taskmanager/PackageInfoItem;->DefaultAppIcon:Landroid/graphics/drawable/Drawable;

    if-ne v7, v8, :cond_1

    .line 898
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x2000

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 900
    .local v3, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v7}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 902
    .local v1, "appIconDrawable":Landroid/graphics/drawable/Drawable;
    instance-of v7, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v7, :cond_0

    .line 903
    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v7, v0

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 905
    .local v5, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_0

    .line 906
    iget-object v7, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 907
    .local v6, "res":Landroid/content/res/Resources;
    const/high16 v7, 0x1050000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 909
    .local v2, "appIconSize":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    mul-int/2addr v7, v8

    mul-int v8, v2, v2

    mul-int/lit8 v8, v8, 0x4

    if-le v7, v8, :cond_0

    .line 911
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "appIconDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v7, 0x1

    invoke-static {v5, v2, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 917
    .end local v2    # "appIconSize":I
    .end local v5    # "iconBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "res":Landroid/content/res/Resources;
    .restart local v1    # "appIconDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p1, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppIcon(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 924
    .end local v1    # "appIconDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :goto_0
    return-void

    .line 918
    :catch_0
    move-exception v4

    .line 919
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "TaskManager:PackageInfo"

    const-string v8, "loadLabel() NameNotFoundException"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 920
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v4

    .line 921
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    const-string v7, "TaskManager:PackageInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadIcon() OutOfMemoryError : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadLabel(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 927
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_0

    .line 929
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 931
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setAppName(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 936
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-void

    .line 932
    :catch_0
    move-exception v1

    .line 933
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "TaskManager:PackageInfo"

    const-string v3, "loadLabel() NameNotFoundException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRecentTaskIntentInfo()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 438
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mAm:Landroid/app/ActivityManager;

    const/16 v10, 0x64

    invoke-virtual {v9, v10, v11}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v7

    .line 439
    .local v7, "recentTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    const/4 v4, 0x0

    .line 441
    .local v4, "order":I
    if-eqz v7, :cond_2

    .line 442
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 443
    .local v2, "info":Landroid/app/ActivityManager$RecentTaskInfo;
    new-instance v3, Landroid/content/Intent;

    iget-object v9, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 444
    .local v3, "intent":Landroid/content/Intent;
    iget v1, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    .line 446
    .local v1, "id":I
    iget-object v9, v2, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    if-eqz v9, :cond_1

    .line 447
    iget-object v9, v2, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 450
    :cond_1
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    move-result v9

    const v10, -0x200001

    and-int/2addr v9, v10

    const/high16 v10, 0x10000000

    or-int/2addr v9, v10

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 452
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v9, v3, v11}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v8

    .line 454
    .local v8, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v8, :cond_0

    .line 455
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v9

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getPackageInfoItem(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v6

    .line 457
    .local v6, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v6, v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setIntent(Landroid/content/Intent;)V

    .line 458
    invoke-virtual {v6, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setRecentTaskId(I)V

    .line 459
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "order":I
    .local v5, "order":I
    invoke-virtual {v6, v4}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setRecentTaskOrder(I)V

    move v4, v5

    .end local v5    # "order":I
    .restart local v4    # "order":I
    goto :goto_0

    .line 467
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "id":I
    .end local v2    # "info":Landroid/app/ActivityManager$RecentTaskInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v6    # "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_2
    return-void
.end method

.method public setRunningApplicationList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "runnningPkgList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    const-string v0, "TaskManager:PackageInfo"

    const-string v1, "setRunningApplicationList()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mRunningAppList:Ljava/util/List;

    .line 214
    return-void
.end method

.method public updateRunningApplicationListInfo()V
    .locals 4

    .prologue
    .line 222
    const-string v0, "TaskManager:PackageInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateRunningApplicationListInfo() m_bUpdateRunningApplicationInfoOnProgress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mUpdateHandle:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfo;->mUpdateHandle:Landroid/os/Handler;

    const v2, 0xc8596

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 226
    return-void
.end method
