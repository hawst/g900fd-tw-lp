.class Lcom/sec/android/app/taskmanager/PackageInfo$1;
.super Ljava/lang/Object;
.source "PackageInfo.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/PackageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/PackageInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/PackageInfo;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfo$1;->this$0:Lcom/sec/android/app/taskmanager/PackageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 90
    const-string v0, "speedTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mApplicationListUpdateHandler msg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 105
    :goto_0
    return v3

    .line 94
    :pswitch_0
    const-string v0, "speedTest"

    const-string v1, "ApplicationListUpdateThread UPDATE_RUNNING_APP_LIST start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo$1;->this$0:Lcom/sec/android/app/taskmanager/PackageInfo;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->access$002(Lcom/sec/android/app/taskmanager/PackageInfo;Z)Z

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo$1;->this$0:Lcom/sec/android/app/taskmanager/PackageInfo;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/PackageInfo$1;->this$0:Lcom/sec/android/app/taskmanager/PackageInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->setRunningApplicationList(Ljava/util/List;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfo$1;->this$0:Lcom/sec/android/app/taskmanager/PackageInfo;

    # setter for: Lcom/sec/android/app/taskmanager/PackageInfo;->m_bUpdateRunningApplicationInfoOnProgress:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/taskmanager/PackageInfo;->access$002(Lcom/sec/android/app/taskmanager/PackageInfo;Z)Z

    .line 99
    const-string v0, "speedTest"

    const-string v1, "ApplicationListUpdateThread UPDATE_RUNNING_APP_LIST end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0xc8596
        :pswitch_0
    .end packed-switch
.end method
