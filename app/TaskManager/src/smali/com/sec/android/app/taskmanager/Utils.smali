.class public Lcom/sec/android/app/taskmanager/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/Utils$THEME;,
        Lcom/sec/android/app/taskmanager/Utils$ScafeSize;
    }
.end annotation


# static fields
.field private static DEBUG:Ljava/lang/Boolean;

.field private static mIsEnableUnusedAppsControl:Ljava/lang/Boolean;

.field private static mIsTablet:Ljava/lang/Boolean;

.field private static mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;


# direct methods
.method public static getBootTime()J
    .locals 12

    .prologue
    .line 143
    const-string v10, "^btime\\s+(\\d+)"

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 144
    .local v4, "btimePattern":Ljava/util/regex/Pattern;
    const-wide/16 v0, -0x1

    .line 145
    .local v0, "bootTime":J
    const/4 v2, 0x0

    .line 149
    .local v2, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v7, Ljava/io/File;

    const-string v10, "/proc/stat"

    invoke-direct {v7, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 152
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v7}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    const/16 v11, 0x2000

    invoke-direct {v3, v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .end local v2    # "br":Ljava/io/BufferedReader;
    .local v3, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .local v9, "text":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 156
    invoke-virtual {v4, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 158
    .local v8, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 159
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    move-object v2, v3

    .line 174
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "m":Ljava/util/regex/Matcher;
    .end local v9    # "text":Ljava/lang/String;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 175
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .end local v7    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    move-wide v10, v0

    .line 182
    :goto_2
    return-wide v10

    .line 162
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "m":Ljava/util/regex/Matcher;
    .restart local v9    # "text":Ljava/lang/String;
    :cond_2
    const-wide/16 v10, -0x1

    .line 174
    if-eqz v3, :cond_3

    .line 175
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    :goto_3
    move-object v2, v3

    .line 179
    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 177
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v6

    .line 178
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 177
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v6    # "e1":Ljava/io/IOException;
    .end local v8    # "m":Ljava/util/regex/Matcher;
    .end local v9    # "text":Ljava/lang/String;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_1
    move-exception v6

    .line 178
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 166
    .end local v6    # "e1":Ljava/io/IOException;
    .end local v7    # "file":Ljava/io/File;
    :catch_2
    move-exception v5

    .line 167
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_4
    const-string v10, "TaskManager:Utils"

    const-string v11, "getBootTime() FileNotFoundException "

    invoke-static {v10, v11, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 174
    if-eqz v2, :cond_1

    .line 175
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 177
    :catch_3
    move-exception v6

    .line 178
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 168
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .end local v6    # "e1":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 169
    .local v5, "e":Ljava/io/IOException;
    :goto_5
    :try_start_6
    const-string v10, "TaskManager:Utils"

    const-string v11, "getBootTime() IOException "

    invoke-static {v10, v11, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 174
    if-eqz v2, :cond_1

    .line 175
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_1

    .line 177
    :catch_5
    move-exception v6

    .line 178
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 170
    .end local v5    # "e":Ljava/io/IOException;
    .end local v6    # "e1":Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 171
    .local v5, "e":Ljava/lang/Exception;
    :goto_6
    :try_start_8
    const-string v10, "TaskManager:Utils"

    const-string v11, "getBootTime() Exception "

    invoke-static {v10, v11, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 174
    if-eqz v2, :cond_1

    .line 175
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_1

    .line 177
    :catch_7
    move-exception v6

    .line 178
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 173
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 174
    :goto_7
    if-eqz v2, :cond_4

    .line 175
    :try_start_a
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 179
    :cond_4
    :goto_8
    throw v10

    .line 177
    :catch_8
    move-exception v6

    .line 178
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 173
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v6    # "e1":Ljava/io/IOException;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v7    # "file":Ljava/io/File;
    :catchall_1
    move-exception v10

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_7

    .line 170
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catch_9
    move-exception v5

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_6

    .line 168
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catch_a
    move-exception v5

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .line 166
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catch_b
    move-exception v5

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "text":Ljava/lang/String;
    :cond_5
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static getIntPref(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "def"    # I

    .prologue
    .line 55
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 56
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getLongPref(Landroid/content/Context;Ljava/lang/String;J)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "def"    # J

    .prologue
    .line 91
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 92
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getProcessStartTime(I)J
    .locals 14
    .param p0, "pid"    # I

    .prologue
    .line 193
    const-string v11, "(-?\\d+)\\s(\\S+)\\s([A-Z])\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)\\s(-?\\d+)"

    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 195
    .local v7, "processStatPattern":Ljava/util/regex/Pattern;
    const-wide/16 v8, -0x1

    .line 196
    .local v8, "startTime":J
    const/4 v0, 0x0

    .line 197
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 200
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "/proc/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/stat"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v3, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 202
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 203
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v11, 0x2000

    invoke-direct {v1, v5, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 207
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .local v10, "text":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 208
    invoke-virtual {v7, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 210
    .local v6, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 211
    const/16 v11, 0x16

    invoke-virtual {v6, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    goto :goto_0

    .line 213
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 214
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 215
    const-wide/16 v12, -0x1

    .line 224
    if-eqz v1, :cond_a

    .line 226
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 227
    const/4 v0, 0x0

    .line 232
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_1
    if-eqz v5, :cond_9

    .line 234
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 235
    const/4 v4, 0x0

    .line 241
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fr":Ljava/io/FileReader;
    .end local v6    # "m":Ljava/util/regex/Matcher;
    .end local v10    # "text":Ljava/lang/String;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :goto_2
    return-wide v12

    .line 228
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "m":Ljava/util/regex/Matcher;
    .restart local v10    # "text":Ljava/lang/String;
    :catch_0
    move-exception v11

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 236
    :catch_1
    move-exception v11

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .end local v6    # "m":Ljava/util/regex/Matcher;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :cond_1
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 224
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v10    # "text":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_2
    if-eqz v0, :cond_3

    .line 226
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 227
    const/4 v0, 0x0

    .line 232
    :cond_3
    :goto_3
    if-eqz v4, :cond_4

    .line 234
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 235
    const/4 v4, 0x0

    .end local v3    # "file":Ljava/io/File;
    :cond_4
    :goto_4
    move-wide v12, v8

    .line 241
    goto :goto_2

    .line 219
    :catch_2
    move-exception v2

    .line 220
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_5
    :try_start_7
    const-string v11, "TaskManager:Utils"

    const-string v12, "getProcessStartTime() FileNotFoundException "

    invoke-static {v11, v12, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 224
    if-eqz v0, :cond_5

    .line 226
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 227
    const/4 v0, 0x0

    .line 232
    :cond_5
    :goto_6
    if-eqz v4, :cond_4

    .line 234
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 235
    const/4 v4, 0x0

    goto :goto_4

    .line 221
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 222
    .local v2, "e":Ljava/io/IOException;
    :goto_7
    :try_start_a
    const-string v11, "TaskManager:Utils"

    const-string v12, "getProcessStartTime() IOException "

    invoke-static {v11, v12, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 224
    if-eqz v0, :cond_6

    .line 226
    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 227
    const/4 v0, 0x0

    .line 232
    :cond_6
    :goto_8
    if-eqz v4, :cond_4

    .line 234
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 235
    const/4 v4, 0x0

    goto :goto_4

    .line 224
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_9
    if-eqz v0, :cond_7

    .line 226
    :try_start_d
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 227
    const/4 v0, 0x0

    .line 232
    :cond_7
    :goto_a
    if-eqz v4, :cond_8

    .line 234
    :try_start_e
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 235
    const/4 v4, 0x0

    .line 237
    :cond_8
    :goto_b
    throw v11

    .line 228
    .restart local v3    # "file":Ljava/io/File;
    :catch_4
    move-exception v11

    goto :goto_3

    .line 236
    :catch_5
    move-exception v11

    goto :goto_4

    .line 228
    .end local v3    # "file":Ljava/io/File;
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v11

    goto :goto_6

    .line 236
    :catch_7
    move-exception v11

    goto :goto_4

    .line 228
    .local v2, "e":Ljava/io/IOException;
    :catch_8
    move-exception v11

    goto :goto_8

    .line 236
    :catch_9
    move-exception v11

    goto :goto_4

    .line 228
    .end local v2    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v12

    goto :goto_a

    .line 236
    :catch_b
    move-exception v12

    goto :goto_b

    .line 224
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v11

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_9

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v11

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_9

    .line 221
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_7

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_7

    .line 219
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "m":Ljava/util/regex/Matcher;
    .restart local v10    # "text":Ljava/lang/String;
    :cond_9
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :cond_a
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public static getScafeSize()Lcom/sec/android/app/taskmanager/Utils$ScafeSize;
    .locals 2

    .prologue
    .line 413
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    if-nez v1, :cond_0

    .line 414
    const-string v1, "ro.build.scafe.size"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    .local v0, "val":Ljava/lang/String;
    const-string v1, "short"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 417
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->SHORT:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    sput-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    .line 427
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    return-object v1

    .line 418
    :cond_1
    const-string v1, "tall"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 419
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->TALL:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    sput-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    goto :goto_0

    .line 420
    :cond_2
    const-string v1, "grande"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 421
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->GRANDE:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    sput-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    goto :goto_0

    .line 423
    :cond_3
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->VENTI:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    sput-object v1, Lcom/sec/android/app/taskmanager/Utils;->mScafeSize:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    goto :goto_0
.end method

.method public static getSidePanelColor(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 479
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getTheme()Lcom/sec/android/app/taskmanager/Utils$THEME;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    if-ne v0, v1, :cond_0

    .line 480
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 482
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public static getStringPref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "def"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 68
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTextColor(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 455
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getTheme()Lcom/sec/android/app/taskmanager/Utils$THEME;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    if-ne v0, v1, :cond_0

    .line 456
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 458
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public static getTheme()Lcom/sec/android/app/taskmanager/Utils$THEME;
    .locals 1

    .prologue
    .line 446
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    return-object v0
.end method

.method public static isDebug(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils;->DEBUG:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 386
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils;->DEBUG:Ljava/lang/Boolean;

    .line 389
    :cond_0
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils;->DEBUG:Ljava/lang/Boolean;

    return-object v0
.end method

.method public static isEnableUnusedAppsControl(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 395
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils;->mIsEnableUnusedAppsControl:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 396
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_JobManager_EnableUnusedAppsControl"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils;->mIsEnableUnusedAppsControl:Ljava/lang/Boolean;

    .line 400
    :cond_0
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils;->mIsEnableUnusedAppsControl:Ljava/lang/Boolean;

    return-object v0
.end method

.method public static isLocaleChange(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, -0x1

    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 107
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v8, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "locale":Ljava/lang/String;
    const-string v8, "locale"

    const-string v9, ""

    invoke-static {p0, v8, v9}, Lcom/sec/android/app/taskmanager/Utils;->getStringPref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "previousLocale":Ljava/lang/String;
    iget v3, v0, Landroid/content/res/Configuration;->mcc:I

    .line 110
    .local v3, "mcc":I
    const-string v8, "mcc"

    invoke-static {p0, v8, v10}, Lcom/sec/android/app/taskmanager/Utils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v6

    .line 111
    .local v6, "previousMcc":I
    iget v4, v0, Landroid/content/res/Configuration;->mnc:I

    .line 112
    .local v4, "mnc":I
    const-string v8, "mnc"

    invoke-static {p0, v8, v10}, Lcom/sec/android/app/taskmanager/Utils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    .line 115
    .local v7, "previousMnc":I
    const-string v8, ""

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 116
    const/4 v2, 0x1

    .line 122
    .local v2, "mLocaleChanged":Z
    :goto_0
    return v2

    .line 118
    .end local v2    # "mLocaleChanged":Z
    :cond_0
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-ne v3, v6, :cond_1

    if-eq v4, v7, :cond_2

    :cond_1
    const/4 v2, 0x1

    .restart local v2    # "mLocaleChanged":Z
    :goto_1
    goto :goto_0

    .end local v2    # "mLocaleChanged":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isRTL()Z
    .locals 1

    .prologue
    .line 431
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/Utils;->isRTL(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public static isRTL(Ljava/util/Locale;)Z
    .locals 4
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 435
    invoke-virtual {p0}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    .line 436
    .local v0, "directionality":I
    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    return v1
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 374
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils;->mIsTablet:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 375
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "val":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/taskmanager/Utils;->mIsTablet:Ljava/lang/Boolean;

    .line 379
    .end local v0    # "val":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/sec/android/app/taskmanager/Utils;->mIsTablet:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1

    .line 376
    .restart local v0    # "val":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setIntPref(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 60
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 61
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 62
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 63
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    return-void
.end method

.method public static setLongPref(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 96
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 97
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 98
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 99
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 100
    return-void
.end method

.method public static writeLocaleConfig(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 127
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v6, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "locale":Ljava/lang/String;
    iget v3, v0, Landroid/content/res/Configuration;->mcc:I

    .line 129
    .local v3, "mcc":I
    iget v4, v0, Landroid/content/res/Configuration;->mnc:I

    .line 130
    .local v4, "mnc":I
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 131
    .local v5, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 132
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v6, "locale"

    invoke-interface {v1, v6, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 133
    const-string v6, "mcc"

    invoke-interface {v1, v6, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 134
    const-string v6, "mnc"

    invoke-interface {v1, v6, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method
