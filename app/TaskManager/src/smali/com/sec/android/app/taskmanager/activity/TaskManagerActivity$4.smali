.class Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$4;
.super Ljava/lang/Object;
.source "TaskManagerActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$4;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 302
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 311
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$4;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->updateButtonTextColor(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V

    .line 312
    return v2

    .line 304
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 308
    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
