.class public Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
.super Landroid/app/ListActivity;
.source "UnusedAppsListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    }
.end annotation


# static fields
.field public static final DEBUGGABLE:Z

.field private static mVersion:I


# instance fields
.field private csNotificationPeriod:[Ljava/lang/CharSequence;

.field private csUnusedPeriod:[Ljava/lang/CharSequence;

.field private intent:Landroid/content/Intent;

.field private lastusedOverPeriodList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

.field private mAllPackagInfoItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

.field private mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

.field private mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

.field private mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

.field private mStatus1:Landroid/widget/TextView;

.field onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->DEBUGGABLE:Z

    .line 150
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mVersion:I

    return-void

    :cond_0
    move v0, v1

    .line 148
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 560
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 591
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$11;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

    .line 599
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

    .line 639
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    .line 918
    return-void
.end method

.method public static ReadLastUsedOverPeriodList(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 224
    .local v14, "lastOverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v3, 0x0

    .line 225
    .local v3, "uri":Landroid/net/Uri;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getVersionOfContextProviders(Landroid/content/Context;)I

    move-result v19

    .line 226
    .local v19, "version":I
    const-string v2, "UnusedAppsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SW] version is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_2

    .line 229
    const-string v2, "content://com.samsung.android.providers.context.profile/app_used/log?last_access=recent&time_span=0099-00-00"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 242
    :goto_0
    const/4 v9, 0x0

    .line 244
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v3, :cond_0

    .line 245
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 248
    :cond_0
    if-nez v9, :cond_3

    .line 249
    const-string v2, "UnusedAppsListActivity"

    const-string v4, "[SW] lastusedOverPeriodCursor is null"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v14, 0x0

    .line 302
    .end local v14    # "lastOverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_1
    return-object v14

    .line 232
    .end local v9    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "lastOverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_2
    const-string v2, "content://com.samsung.android.providers.context/app_usage/uninstall/last_used_date"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 253
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v2, "UnusedAppsListActivity"

    const-string v4, "[SW] Success to get lastusedOverPeriodCursor"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 255
    .local v15, "rowcount":I
    const-string v2, "UnusedAppsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SW] cursor rowcount() :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 261
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 263
    .local v8, "cal":Ljava/util/Calendar;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v16

    .line 264
    .local v16, "timeZone":Ljava/util/TimeZone;
    invoke-virtual/range {v16 .. v16}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v17

    .line 267
    .local v17, "timeZoneOffset":I
    :cond_4
    const/4 v2, 0x2

    move/from16 v0, v19

    if-lt v0, v2, :cond_6

    .line 268
    const-string v2, "app_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "latest_time"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v17

    invoke-static {v4, v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->parseTextToLongTime(Ljava/lang/String;I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_4

    .line 281
    .end local v8    # "cal":Ljava/util/Calendar;
    .end local v16    # "timeZone":Ljava/util/TimeZone;
    .end local v17    # "timeZoneOffset":I
    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 285
    sget-boolean v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->DEBUGGABLE:Z

    if-eqz v2, :cond_1

    .line 286
    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v13

    .line 287
    .local v13, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 288
    .local v11, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 289
    .local v12, "key":Ljava/lang/String;
    const/16 v18, 0x0

    .line 290
    .local v18, "value":Ljava/lang/Long;
    const/4 v10, 0x0

    .line 292
    .local v10, "date":Ljava/util/Date;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "key":Ljava/lang/String;
    check-cast v12, Ljava/lang/String;

    .line 294
    .restart local v12    # "key":Ljava/lang/String;
    invoke-virtual {v14, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "value":Ljava/lang/Long;
    check-cast v18, Ljava/lang/Long;

    .line 295
    .restart local v18    # "value":Ljava/lang/Long;
    new-instance v10, Ljava/util/Date;

    .end local v10    # "date":Ljava/util/Date;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v10, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 296
    .restart local v10    # "date":Ljava/util/Date;
    const-string v2, "UnusedAppsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Final state : package_name ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]   last used time time ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 274
    .end local v10    # "date":Ljava/util/Date;
    .end local v11    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v12    # "key":Ljava/lang/String;
    .end local v13    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v18    # "value":Ljava/lang/Long;
    .restart local v8    # "cal":Ljava/util/Calendar;
    .restart local v16    # "timeZone":Ljava/util/TimeZone;
    .restart local v17    # "timeZoneOffset":I
    :cond_6
    :try_start_1
    const-string v2, "package_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "max_last_time"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 281
    .end local v8    # "cal":Ljava/util/Calendar;
    .end local v16    # "timeZone":Ljava/util/TimeZone;
    .end local v17    # "timeZoneOffset":I
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method static synthetic access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mAllPackagInfoItem:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mAllPackagInfoItem:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->updateStatus()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
    .param p1, "x1"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->manageTestMode(II)V

    return-void
.end method

.method public static calculateAndSetNextNotifyingTime(Landroid/content/Context;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1133
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationPeriod(Landroid/content/Context;)I

    move-result v6

    .line 1135
    .local v6, "nofifyingCycleOptionValue":I
    if-eqz v6, :cond_1

    .line 1136
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->removeAlarm(Landroid/content/Context;)V

    .line 1137
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationCycleTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 1138
    .local v2, "cycleTimeLong":J
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNextNotifyingTime(Landroid/content/Context;)J

    move-result-wide v4

    .line 1146
    .local v4, "nextNotiTime":J
    move-wide v8, v4

    .line 1147
    .local v8, "originalNextNotiTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1149
    .local v0, "currentTime":J
    const-wide/16 v10, 0x0

    cmp-long v7, v4, v10

    if-gez v7, :cond_0

    .line 1152
    add-long v4, v0, v2

    .line 1153
    move-wide v8, v4

    .line 1154
    invoke-static {p0, v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNextNotifyingTime(Landroid/content/Context;J)V

    .line 1158
    :cond_0
    cmp-long v7, v0, v4

    if-gez v7, :cond_3

    .line 1161
    :goto_0
    sub-long v10, v4, v2

    cmp-long v7, v0, v10

    if-ltz v7, :cond_2

    .line 1163
    invoke-static {p0, v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNextNotifyingTime(Landroid/content/Context;J)V

    .line 1164
    invoke-static {p0, v4, v5, v2, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setAlarm(Landroid/content/Context;JJ)V

    .line 1165
    const-string v7, "UnusedAppsListActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Before : Current Time > nextNotiTime / After : currentTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",  originalNextNotiTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", cycleOption="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    .end local v0    # "currentTime":J
    .end local v2    # "cycleTimeLong":J
    .end local v4    # "nextNotiTime":J
    .end local v8    # "originalNextNotiTime":J
    :cond_1
    :goto_1
    return-void

    .line 1172
    .restart local v0    # "currentTime":J
    .restart local v2    # "cycleTimeLong":J
    .restart local v4    # "nextNotiTime":J
    .restart local v8    # "originalNextNotiTime":J
    :cond_2
    sub-long/2addr v4, v2

    goto :goto_0

    .line 1178
    :cond_3
    add-long/2addr v4, v2

    .line 1180
    cmp-long v7, v0, v4

    if-gez v7, :cond_3

    .line 1182
    invoke-static {p0, v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNextNotifyingTime(Landroid/content/Context;J)V

    .line 1183
    invoke-static {p0, v4, v5, v2, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setAlarm(Landroid/content/Context;JJ)V

    .line 1184
    const-string v7, "UnusedAppsListActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Before : nextNotiTime >= Current Time / After : currentTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "  ,  originalNextNotiTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", cycleOption="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getIsTestMode(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1122
    const-string v0, "unusedAppList_is_test_mode"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/taskmanager/Utils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getNextNotifyingTime(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1106
    const-string v0, "unusedAppList_next_notifying_time"

    const-wide/16 v2, -0x1

    invoke-static {p0, v0, v2, v3}, Lcom/sec/android/app/taskmanager/Utils;->getLongPref(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNotificationCycleTime(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 868
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationPeriod(Landroid/content/Context;)I

    move-result v3

    .line 869
    .local v3, "nNotifyCycleSaved":I
    const-wide/16 v0, 0x0

    .line 870
    .local v0, "lReturnTime":J
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v2

    .line 872
    .local v2, "nIsTestMode":I
    if-nez v2, :cond_0

    .line 874
    packed-switch v3, :pswitch_data_0

    .line 906
    :goto_0
    return-wide v0

    .line 876
    :pswitch_0
    const-wide/16 v0, 0x0

    .line 877
    goto :goto_0

    .line 879
    :pswitch_1
    const-wide/32 v0, 0x240c8400

    .line 880
    goto :goto_0

    .line 882
    :pswitch_2
    const-wide/32 v0, 0x48190800

    .line 883
    goto :goto_0

    .line 885
    :pswitch_3
    const-wide v0, 0x9a7ec800L

    goto :goto_0

    .line 890
    :cond_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 892
    :pswitch_4
    const-wide/16 v0, 0x0

    .line 893
    goto :goto_0

    .line 895
    :pswitch_5
    const-wide/32 v0, 0x927c0

    .line 896
    goto :goto_0

    .line 898
    :pswitch_6
    const-wide/32 v0, 0x124f80

    .line 899
    goto :goto_0

    .line 901
    :pswitch_7
    const-wide/32 v0, 0x1b7740

    goto :goto_0

    .line 874
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 890
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getNotificationPeriod(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1099
    const-string v1, "unusedAppList_notification_period"

    const/4 v2, 0x3

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/taskmanager/Utils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 1101
    .local v0, "nResult":I
    const-string v1, "UnusedAppsListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNotificationPeriod["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    return v0
.end method

.method public static getPeriodCurrentValue(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 822
    const/4 v2, 0x0

    .line 823
    .local v2, "resultValue":I
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedPeriod(Landroid/content/Context;)I

    move-result v1

    .line 824
    .local v1, "nSavedPeriod":I
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v0

    .line 826
    .local v0, "nIsTestMode":I
    if-nez v0, :cond_0

    .line 828
    packed-switch v1, :pswitch_data_0

    .line 864
    :goto_0
    return v2

    .line 830
    :pswitch_0
    const/4 v2, 0x1

    .line 831
    goto :goto_0

    .line 833
    :pswitch_1
    const/4 v2, 0x2

    .line 834
    goto :goto_0

    .line 836
    :pswitch_2
    const/4 v2, 0x3

    .line 837
    goto :goto_0

    .line 839
    :pswitch_3
    const/4 v2, 0x6

    .line 840
    goto :goto_0

    .line 846
    :cond_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 848
    :pswitch_4
    const/4 v2, 0x1

    .line 849
    goto :goto_0

    .line 851
    :pswitch_5
    const/4 v2, 0x2

    .line 852
    goto :goto_0

    .line 854
    :pswitch_6
    const/4 v2, 0x3

    .line 855
    goto :goto_0

    .line 857
    :pswitch_7
    const/4 v2, 0x4

    .line 858
    goto :goto_0

    .line 828
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 846
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getPeriodString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nWhich"    # I

    .prologue
    const v2, 0x7f080016

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 777
    const/4 v1, 0x0

    .line 778
    .local v1, "resultString":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v0

    .line 780
    .local v0, "nIsTestMode":I
    if-nez v0, :cond_0

    .line 782
    packed-switch p1, :pswitch_data_0

    .line 818
    :goto_0
    return-object v1

    .line 784
    :pswitch_0
    const v2, 0x7f080015

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 785
    goto :goto_0

    .line 787
    :pswitch_1
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 788
    goto :goto_0

    .line 790
    :pswitch_2
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 791
    goto :goto_0

    .line 793
    :pswitch_3
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 794
    goto :goto_0

    .line 800
    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 802
    :pswitch_4
    const-string v1, "1 \uc2dc\uac04"

    .line 803
    goto :goto_0

    .line 805
    :pswitch_5
    const-string v1, "2 \uc2dc\uac04"

    .line 806
    goto :goto_0

    .line 808
    :pswitch_6
    const-string v1, "3 \uc2dc\uac04"

    .line 809
    goto :goto_0

    .line 811
    :pswitch_7
    const-string v1, "4 \uc2dc\uac04"

    .line 812
    goto :goto_0

    .line 782
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 800
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getUnusedCriterionTime(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 735
    const-wide/16 v0, 0x0

    .line 736
    .local v0, "lReturnTime":J
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedPeriod(Landroid/content/Context;)I

    move-result v3

    .line 737
    .local v3, "nUnusePeriodSaved":I
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v2

    .line 739
    .local v2, "nIsTestMode":I
    if-nez v2, :cond_0

    .line 741
    packed-switch v3, :pswitch_data_0

    .line 773
    :goto_0
    return-wide v0

    .line 743
    :pswitch_0
    const-wide v0, 0x9a7ec800L

    .line 744
    goto :goto_0

    .line 746
    :pswitch_1
    const-wide v0, 0x134fd9000L

    .line 747
    goto :goto_0

    .line 749
    :pswitch_2
    const-wide v0, 0x1cf7c5800L

    .line 750
    goto :goto_0

    .line 752
    :pswitch_3
    const-wide v0, 0x39ef8b000L

    goto :goto_0

    .line 757
    :cond_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 759
    :pswitch_4
    const-wide/32 v0, 0x36ee80

    .line 760
    goto :goto_0

    .line 762
    :pswitch_5
    const-wide/32 v0, 0x6ddd00

    .line 763
    goto :goto_0

    .line 765
    :pswitch_6
    const-wide/32 v0, 0xa4cb80

    .line 766
    goto :goto_0

    .line 768
    :pswitch_7
    const-wide/32 v0, 0xdbba00

    goto :goto_0

    .line 741
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 757
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getUnusedCriterionTimeFromToday(Landroid/content/Context;)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 727
    const-wide/16 v0, 0x0

    .local v0, "lCurrentTime":J
    const-wide/16 v2, 0x0

    .line 728
    .local v2, "lReturnTime":J
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedCriterionTime(Landroid/content/Context;)J

    move-result-wide v4

    .line 729
    .local v4, "lUnusedCriterionTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 730
    sub-long v2, v0, v4

    .line 731
    return-wide v2
.end method

.method public static getUnusedPeriod(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1083
    const-string v0, "unusedAppList_unused_period"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/taskmanager/Utils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    sget v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mVersion:I

    if-lez v2, :cond_0

    .line 200
    sget v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mVersion:I

    .line 211
    :goto_0
    return v2

    .line 203
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.samsung.android.providers.context"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 205
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mVersion:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    sget v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mVersion:I

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "UnusedAppsListActivity"

    const-string v3, "[SW] Could not find ContextProvider"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private manageTestMode(II)V
    .locals 3
    .param p1, "nOld"    # I
    .param p2, "nNew"    # I

    .prologue
    .line 1201
    if-eq p1, p2, :cond_0

    .line 1202
    const-string v0, "UnusedAppsListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "manageTestMode : ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->calculateAndSetNextNotifyingTime(Landroid/content/Context;)V

    .line 1205
    :cond_0
    return-void
.end method

.method public static parseTextToLongTime(Ljava/lang/String;I)J
    .locals 8
    .param p0, "time"    # Ljava/lang/String;
    .param p1, "timeZoneOffset"    # I

    .prologue
    .line 308
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 312
    .local v1, "mSimpleDateFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 318
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    int-to-long v6, p1

    add-long/2addr v4, v6

    .end local v0    # "date":Ljava/util/Date;
    :goto_0
    return-wide v4

    .line 313
    :catch_0
    move-exception v2

    .line 314
    .local v2, "pe":Ljava/text/ParseException;
    const-string v3, "UnusedAppsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "can not parse latest time format : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const-wide/16 v4, 0x0

    goto :goto_0
.end method

.method private static removeAlarm(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1064
    const-string v3, "UnusedAppsListActivity"

    const-string v4, "removeAlarm"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 1066
    .local v1, "am":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/taskmanager/UnusedAppsIntentReceiver;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1068
    .local v0, "alarmintent":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.taskmanager.action.NOTIFY_UNUSED_APPS"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1069
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1071
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1072
    return-void
.end method

.method public static removeOverPeriodPackage(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)Ljava/util/List;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    .local p1, "allList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    .local p2, "OverPeriodList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedCriterionTimeFromToday(Landroid/content/Context;)J

    move-result-wide v4

    .line 701
    .local v4, "lCriterion":J
    const-wide/16 v2, 0x0

    .line 702
    .local v2, "lAppLastUsedTime":J
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 705
    .local v0, "filteredList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 706
    .local v7, "thisPackageItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 708
    .local v6, "packageName":Ljava/lang/String;
    if-eqz p2, :cond_1

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 710
    invoke-virtual {p2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 712
    cmp-long v8, v2, v4

    if-gez v8, :cond_0

    .line 714
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 716
    :cond_1
    invoke-virtual {v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getInstalledTime()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-gez v8, :cond_0

    .line 719
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 723
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "thisPackageItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_2
    return-object v0
.end method

.method public static setAlarm(Landroid/content/Context;JJ)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "lNextAlarmTime"    # J
    .param p3, "cycleTime"    # J

    .prologue
    const/4 v1, 0x0

    .line 1053
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1054
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/taskmanager/UnusedAppsIntentReceiver;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1056
    .local v7, "alarmintent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.taskmanager.action.NOTIFY_UNUSED_APPS"

    invoke-virtual {v7, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1057
    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .local v6, "sender":Landroid/app/PendingIntent;
    move-wide v2, p1

    move-wide v4, p3

    .line 1059
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 1060
    const-string v1, "UnusedAppsListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAlarm - Next Alarm time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    return-void
.end method

.method public static setIsTestMode(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nValue"    # I

    .prologue
    .line 1126
    const-string v0, "UnusedAppsListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIsTestMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    const-string v0, "unusedAppList_is_test_mode"

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/taskmanager/Utils;->setIntPref(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1128
    return-void
.end method

.method public static setNextNotifyingTime(Landroid/content/Context;J)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nextTime"    # J

    .prologue
    .line 1117
    const-string v0, "unusedAppList_next_notifying_time"

    invoke-static {p0, v0, p1, p2}, Lcom/sec/android/app/taskmanager/Utils;->setLongPref(Landroid/content/Context;Ljava/lang/String;J)V

    .line 1118
    return-void
.end method

.method public static setNotificationPeriod(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "period"    # I

    .prologue
    .line 1090
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-ge v0, p1, :cond_1

    .line 1091
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1094
    :cond_1
    const-string v0, "UnusedAppsListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNotificationPeriod["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    const-string v0, "unusedAppList_notification_period"

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/taskmanager/Utils;->setIntPref(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1096
    return-void
.end method

.method public static setUnusedPeriod(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "period"    # I

    .prologue
    .line 1075
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-ge v0, p1, :cond_1

    .line 1076
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1079
    :cond_1
    const-string v0, "unusedAppList_unused_period"

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/taskmanager/Utils;->setIntPref(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1080
    return-void
.end method

.method private updateStatus()V
    .locals 3

    .prologue
    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 364
    .local v0, "sbUnusedAppsSummary":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mStatus1:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    return-void
.end method


# virtual methods
.method public manageAlarm(IIJ)V
    .locals 7
    .param p1, "nOldWhich"    # I
    .param p2, "nNewWhich"    # I
    .param p3, "lCurrentTime"    # J

    .prologue
    .line 1031
    const-string v1, "UnusedAppsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MA o["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] n["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    if-ne p1, p2, :cond_1

    .line 1034
    const-string v1, "UnusedAppsListActivity"

    const-string v4, "Nothing changed"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    :cond_0
    :goto_0
    return-void

    .line 1038
    :cond_1
    if-eqz p1, :cond_2

    .line 1040
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->removeAlarm(Landroid/content/Context;)V

    .line 1041
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNextNotifyingTime(Landroid/content/Context;J)V

    .line 1044
    :cond_2
    const/4 v1, 0x1

    if-gt v1, p2, :cond_0

    const/4 v1, 0x3

    if-gt p2, v1, :cond_0

    .line 1045
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1046
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationCycleTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 1047
    .local v2, "lCycleTime":J
    add-long v4, p3, v2

    invoke-static {v0, v4, v5, v2, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setAlarm(Landroid/content/Context;JJ)V

    .line 1048
    add-long v4, p3, v2

    invoke-static {v0, v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNextNotifyingTime(Landroid/content/Context;J)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 574
    packed-switch p1, :pswitch_data_0

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 577
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->intent:Landroid/content/Intent;

    const-string v1, "pkg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->intent:Landroid/content/Intent;

    const-string v2, "pkg"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->updatePackageInfoItem(Ljava/lang/String;)V

    goto :goto_0

    .line 574
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 154
    const-string v1, "UnusedAppsListActivity"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getTheme()Lcom/sec/android/app/taskmanager/Utils$THEME;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    if-ne v1, v2, :cond_1

    .line 157
    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setTheme(I)V

    .line 162
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 163
    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setContentView(I)V

    .line 164
    const v1, 0x7f0c000e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mStatus1:Landroid/widget/TextView;

    .line 165
    invoke-static {}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/PackageInfo;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    .line 166
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 169
    const v1, 0x102000a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 170
    .local v0, "lv":Landroid/widget/ListView;
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 172
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$1;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->ReadLastUsedOverPeriodList(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    .line 186
    :cond_0
    new-array v1, v7, [Ljava/lang/CharSequence;

    invoke-static {p0, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p0, v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->csUnusedPeriod:[Ljava/lang/CharSequence;

    .line 192
    new-array v1, v7, [Ljava/lang/CharSequence;

    const v2, 0x7f08001c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f080017

    invoke-virtual {p0, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x7f080018

    invoke-virtual {p0, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const v2, 0x7f080019

    invoke-virtual {p0, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->csNotificationPeriod:[Ljava/lang/CharSequence;

    .line 196
    return-void

    .line 159
    .end local v0    # "lv":Landroid/widget/ListView;
    :cond_1
    const v1, 0x7f0a0001

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setTheme(I)V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 982
    packed-switch p1, :pswitch_data_0

    .line 1027
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 985
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v0

    .line 986
    .local v0, "nTestMode":I
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "\uc77c\ubc18 / \ud14c\uc2a4\ud2b8 \ubaa8\ub4dc \uc124\uc815\nENG \uc804\uc6a9 \uba54\ub274\uc785\ub2c8\ub2e4"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v4, "\uc77c\ubc18 \ubaa8\ub4dc : \uc6d0\ub798\uc758 \ubbf8\uc0ac\uc6a9 \uae30\uac04 / \uc54c\ub9bc \uc8fc\uae30 \ub2e8\uc704\ub85c \uc124\uc815\ub418\uc5b4 \ub3d9\uc791\ud569\ub2c8\ub2e4."

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "\ud14c\uc2a4\ud2b8 \ubaa8\ub4dc : \ubbf8\uc0ac\uc6a9 \uae30\uac04\uc740 1\uc2dc\uac04, \uc54c\ub9bc \uc8fc\uae30\ub294 10\ubd84 \ub2e8\uc704\ub85c \uc124\uc815 \ub418\uc5b4 \ub3d9\uc791\ud569\ub2c8\ub2e4."

    aput-object v4, v2, v3

    new-instance v3, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;

    invoke-direct {v3, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$14;

    invoke-direct {v3, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$14;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 982
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x0

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_showNavigationBar"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 374
    .local v0, "resId":I
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 375
    const/16 v1, 0xb

    const v2, 0x7f08001b

    invoke-interface {p1, v5, v1, v5, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 376
    const/16 v1, 0xc

    const v2, 0x7f080011

    invoke-interface {p1, v5, v1, v5, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 377
    const/16 v1, 0xd

    const v2, 0x7f080013

    invoke-interface {p1, v5, v1, v5, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 380
    sget-boolean v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 381
    const/16 v1, 0xe

    const-string v2, "\uc77c\ubc18 / \ud14c\uc2a4\ud2b8 \ubaa8\ub4dc \uc124\uc815"

    invoke-interface {p1, v5, v1, v5, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 387
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->saveCache()V

    .line 356
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    .line 358
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/high16 v7, 0x1040000

    .line 397
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 398
    .local v0, "menuId":I
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v1

    .line 400
    .local v1, "nTestMode":I
    const/16 v4, 0xc

    if-ne v0, v4, :cond_2

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getUnusedPeriod(Landroid/content/Context;)I

    move-result v3

    .line 403
    .local v3, "unusedPeriod":I
    if-nez v1, :cond_1

    .line 405
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->csUnusedPeriod:[Ljava/lang/CharSequence;

    new-instance v6, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$3;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$2;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 552
    .end local v3    # "unusedPeriod":I
    :cond_0
    :goto_0
    return v8

    .line 437
    .restart local v3    # "unusedPeriod":I
    :cond_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/CharSequence;

    const-string v6, "1 \uc2dc\uac04"

    aput-object v6, v5, v10

    const-string v6, "2 \uc2dc\uac04"

    aput-object v6, v5, v8

    const-string v6, "3 \uc2dc\uac04"

    aput-object v6, v5, v11

    const-string v6, "4 \uc2dc\uac04"

    aput-object v6, v5, v9

    new-instance v6, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$5;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$4;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 469
    .end local v3    # "unusedPeriod":I
    :cond_2
    const/16 v4, 0xd

    if-ne v0, v4, :cond_4

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationPeriod(Landroid/content/Context;)I

    move-result v2

    .line 473
    .local v2, "notificationPeriod":I
    if-nez v1, :cond_3

    .line 475
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f080013

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->csNotificationPeriod:[Ljava/lang/CharSequence;

    new-instance v6, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;

    invoke-direct {v6, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v5, v2, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$6;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 509
    :cond_3
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f080013

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/CharSequence;

    const-string v6, "\uc5c6\uc74c"

    aput-object v6, v5, v10

    const-string v6, "10\ubd84 \ub9c8\ub2e4"

    aput-object v6, v5, v8

    const-string v6, "20\ubd84 \ub9c8\ub2e4"

    aput-object v6, v5, v11

    const-string v6, "30\ubd84 \ub9c8\ub2e4"

    aput-object v6, v5, v9

    new-instance v6, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$9;

    invoke-direct {v6, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$9;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v5, v2, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$8;

    invoke-direct {v5, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$8;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 543
    .end local v2    # "notificationPeriod":I
    :cond_4
    const/16 v4, 0xe

    if-ne v0, v4, :cond_5

    .line 545
    invoke-virtual {p0, v9}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 547
    :cond_5
    const/16 v4, 0xb

    if-ne v0, v4, :cond_0

    .line 548
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->startActivity(Landroid/content/Intent;)V

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 339
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->cancel(Z)Z

    .line 345
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 392
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 330
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 335
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 323
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 325
    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    .line 326
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 349
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    .line 351
    return-void
.end method

.method public returnLastUsedOverPeriodList()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 216
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "lastusedOverPeriodList is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;

    return-object v0
.end method
