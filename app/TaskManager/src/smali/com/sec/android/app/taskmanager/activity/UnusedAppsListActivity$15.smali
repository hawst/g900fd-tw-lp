.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0

    .prologue
    .line 992
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 994
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getIsTestMode(Landroid/content/Context;)I

    move-result v0

    .line 995
    .local v0, "nOld":I
    const-string v1, "UnusedAppsListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TEST mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setIsTestMode(Landroid/content/Context;I)V

    .line 997
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # invokes: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->manageTestMode(II)V
    invoke-static {v1, v0, p2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$700(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;II)V

    .line 998
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1011
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$15;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1012
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1013
    return-void
.end method
