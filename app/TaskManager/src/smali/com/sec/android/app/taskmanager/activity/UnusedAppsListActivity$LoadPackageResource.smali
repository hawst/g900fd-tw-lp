.class public Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
.super Landroid/os/AsyncTask;
.source "UnusedAppsListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoadPackageResource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field running:Z

.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 923
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 921
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->running:Z

    .line 924
    iput-object p2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->list:Ljava/util/List;

    .line 925
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 918
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 929
    const-wide/16 v2, 0x0

    .line 930
    .local v2, "minCheck":J
    const-string v6, "UnusedAppsListActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " start"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->list:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 933
    .local v1, "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 935
    .local v4, "temp":J
    iget-boolean v6, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->running:Z

    if-nez v6, :cond_1

    .line 950
    .end local v1    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v4    # "temp":J
    :cond_0
    const-string v6, "UnusedAppsListActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " end"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    const/4 v6, 0x0

    return-object v6

    .line 939
    .restart local v1    # "item":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .restart local v4    # "temp":J
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$300(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->loadIcon(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 940
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$300(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->loadLabel(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 942
    const-wide/16 v6, 0x1f4

    cmp-long v6, v2, v6

    if-lez v6, :cond_2

    .line 943
    const-wide/16 v2, 0x0

    .line 944
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->publishProgress([Ljava/lang/Object;)V

    .line 947
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    add-long/2addr v2, v6

    .line 948
    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 3

    .prologue
    .line 956
    const-string v0, "UnusedAppsListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " canceled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->running:Z

    .line 958
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 959
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 918
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 968
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 970
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/Utils;->isLocaleChange(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 971
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "Locale Changed Completed!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->saveCache()V

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-static {v0}, Lcom/sec/android/app/taskmanager/Utils;->writeLocaleConfig(Landroid/content/Context;)V

    .line 976
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 977
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 918
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 2
    .param p1, "progress"    # [Ljava/lang/Void;

    .prologue
    .line 963
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 964
    return-void
.end method
