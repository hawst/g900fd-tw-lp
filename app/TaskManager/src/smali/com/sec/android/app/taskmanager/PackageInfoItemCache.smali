.class public Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
.super Ljava/lang/Object;
.source "PackageInfoItemCache.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2ecd227026L


# instance fields
.field items:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;->items:Ljava/util/HashMap;

    .line 27
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;->items:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;

    return-object v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "codeSize"    # J
    .param p5, "dataSize"    # J

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache;->items:Ljava/util/HashMap;

    new-instance v1, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method
