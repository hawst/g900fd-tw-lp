.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 562
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 564
    .local v1, "packageInfoItem":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setSizeComputed(Z)V

    .line 565
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    const-string v3, "package"

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 567
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "pkg"

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const/high16 v2, 0x800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 569
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$10;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v2, v0, v6}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 570
    return-void
.end method
