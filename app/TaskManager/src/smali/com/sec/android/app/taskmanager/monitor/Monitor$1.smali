.class final Lcom/sec/android/app/taskmanager/monitor/Monitor$1;
.super Ljava/lang/Object;
.source "Monitor.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/monitor/Monitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 35
    # getter for: Lcom/sec/android/app/taskmanager/monitor/Monitor;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->access$000()Landroid/os/Handler;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 37
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 51
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 39
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;

    .line 41
    .local v0, "mm":Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;
    iget-object v1, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mMonitor:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mMonitor:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    iget-object v2, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mObj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->onUpdateUi(Ljava/lang/Object;)V

    .line 43
    iget-object v1, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mMonitor:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    iget-object v2, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mObj:Ljava/lang/Object;

    # invokes: Lcom/sec/android/app/taskmanager/monitor/Monitor;->notifyObserverUi(Ljava/lang/Object;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->access$100(Lcom/sec/android/app/taskmanager/monitor/Monitor;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :cond_0
    iget-object v1, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;->mMonitor:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    iget-object v1, v1, Lcom/sec/android/app/taskmanager/monitor/Monitor;->TAG:Ljava/lang/String;

    const-string v2, "Monitor is stop... drop updateUi()."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x23b1
        :pswitch_0
    .end packed-switch
.end method
