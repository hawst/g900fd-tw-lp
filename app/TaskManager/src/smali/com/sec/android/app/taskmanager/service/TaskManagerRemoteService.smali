.class public Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;
.super Landroid/app/Service;
.source "TaskManagerRemoteService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    }
.end annotation


# instance fields
.field private mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

.field private mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

.field private mEnableBroadcastService:Z

.field protected mHandler:Landroid/os/Handler;

.field private mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

.field private mRunningAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningCount:I

.field private final mTaskManagerBinder:Lcom/sec/android/app/taskmanager/service/ITaskManagerService$Stub;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    .line 80
    new-instance v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;-><init>(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I

    .line 100
    new-instance v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;-><init>(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mTaskManagerBinder:Lcom/sec/android/app/taskmanager/service/ITaskManagerService$Stub;

    .line 295
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$2;-><init>(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->calcCPUUsage()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningCount:I

    return p1
.end method

.method private calcCPUUsage()Z
    .locals 20

    .prologue
    .line 199
    const/4 v14, 0x0

    .line 200
    .local v14, "total":F
    const/4 v11, 0x0

    .line 201
    .local v11, "topCpuUsage":F
    const-string v12, ""

    .line 202
    .local v12, "topRunningName":Ljava/lang/String;
    const-string v13, ""

    .line 203
    .local v13, "topRunningPackageName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 204
    .local v9, "numRunning":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList()Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 211
    .local v2, "a":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->getPackageCpuUsage(Lcom/sec/android/app/taskmanager/PackageInfoItem;)F

    move-result v4

    .line 212
    .local v4, "cpuUsage":F
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() packageName:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " usage:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    add-float/2addr v14, v4

    .line 216
    cmpl-float v17, v4, v11

    if-lez v17, :cond_1

    .line 217
    move v11, v4

    .line 218
    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 221
    :cond_1
    const/16 v17, 0x0

    cmpl-float v17, v4, v17

    if-lez v17, :cond_0

    .line 222
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 227
    .end local v2    # "a":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v4    # "cpuUsage":F
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    if-lez v9, :cond_3

    .line 228
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getApplication()Landroid/app/Application;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 231
    .local v10, "pm":Landroid/content/pm/PackageManager;
    const/16 v17, 0x2000

    :try_start_0
    move/from16 v0, v17

    invoke-virtual {v10, v13, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 233
    .local v3, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v3, v10}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 242
    .end local v3    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v10    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    :goto_1
    const/4 v8, 0x0

    .line 243
    .local v8, "isWarning":Z
    const-string v15, ""

    .line 244
    .local v15, "warningLevel":Ljava/lang/String;
    const-string v16, ""

    .line 246
    .local v16, "warningMsg":Ljava/lang/String;
    const/high16 v17, 0x40a00000    # 5.0f

    cmpl-float v17, v14, v17

    if-lez v17, :cond_4

    .line 247
    const/4 v8, 0x1

    .line 248
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getWarningLevel(F)Ljava/lang/String;

    move-result-object v15

    .line 249
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v9}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getWarningMsg(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v16

    .line 252
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v8, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    :cond_5
    const/4 v7, 0x1

    .line 256
    .local v7, "isChanged":Z
    :goto_2
    if-eqz v7, :cond_6

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-boolean v8, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v15, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    .line 260
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() cpu_warning:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() cpu_warning_level:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() mCpuWarningInfo.mMessage:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() top_cpu_usage:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const-string v17, "TaskManager:TaskManagerRemoteService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "calcCPUUsage() total_cpu_usage:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_6
    return v7

    .line 234
    .end local v7    # "isChanged":Z
    .end local v8    # "isWarning":Z
    .end local v15    # "warningLevel":Ljava/lang/String;
    .end local v16    # "warningMsg":Ljava/lang/String;
    .restart local v10    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v5

    .line 235
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v17, "TaskManager:TaskManagerRemoteService"

    const-string v18, "calcCPUUsage() NameNotFoundException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 252
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v10    # "pm":Landroid/content/pm/PackageManager;
    .restart local v8    # "isWarning":Z
    .restart local v15    # "warningLevel":Ljava/lang/String;
    .restart local v16    # "warningMsg":Ljava/lang/String;
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_2
.end method

.method private getWarningLevel(F)Ljava/lang/String;
    .locals 1
    .param p1, "totalCpuUsage"    # F

    .prologue
    .line 271
    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 272
    const-string v0, "level2"

    .line 277
    :goto_0
    return-object v0

    .line 273
    :cond_0
    const/high16 v0, 0x40a00000    # 5.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 274
    const-string v0, "level1"

    goto :goto_0

    .line 277
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private getWarningMsg(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "topRunningName"    # Ljava/lang/String;
    .param p2, "numRunning"    # I

    .prologue
    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 283
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const/4 v1, 0x1

    if-le p2, v1, :cond_0

    .line 287
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080001

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 290
    :cond_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 96
    const-string v0, "TaskManager:TaskManagerRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mTaskManagerBinder:Lcom/sec/android/app/taskmanager/service/ITaskManagerService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "TaskManager:TaskManagerRemoteService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 90
    invoke-static {}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->getInstance()Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    .line 91
    invoke-static {}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    .line 92
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 459
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 460
    const-string v0, "TaskManager:TaskManagerRemoteService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    .line 462
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x5

    .line 429
    if-nez p1, :cond_0

    .line 430
    const-string v2, "TaskManager:TaskManagerRemoteService"

    const-string v3, "onStartCommand() : intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const/4 v2, 0x2

    .line 454
    :goto_0
    return v2

    .line 434
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 435
    const-string v2, "TaskManager:TaskManagerRemoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand() - intent : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " startId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 438
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 439
    const-string v2, "startBroadcastResponse"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 440
    .local v1, "startBroadcastResponse":Z
    const-string v2, "TaskManager:TaskManagerRemoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand() - startBroadcastResponse:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    if-eq v2, v1, :cond_1

    .line 444
    iput-boolean v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    .line 446
    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mEnableBroadcastService:Z

    if-eqz v2, :cond_1

    .line 447
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 448
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->init()V

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 454
    .end local v1    # "startBroadcastResponse":Z
    :cond_1
    const/4 v2, 0x3

    goto :goto_0
.end method
