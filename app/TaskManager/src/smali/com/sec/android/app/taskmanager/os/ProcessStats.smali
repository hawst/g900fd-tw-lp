.class public Lcom/sec/android/app/taskmanager/os/ProcessStats;
.super Ljava/lang/Object;
.source "ProcessStats.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    }
.end annotation


# static fields
.field private static final LOAD_AVERAGE_FORMAT:[I

.field private static final PROCESS_FULL_STATS_FORMAT:[I

.field private static final PROCESS_STATS_FORMAT:[I

.field private static final SYSTEM_CPU_FORMAT:[I

.field private static final sLoadComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBaseIdleTime:J

.field private mBaseIoWaitTime:J

.field private mBaseIrqTime:J

.field private mBaseSoftIrqTime:J

.field private mBaseSystemTime:J

.field private mBaseUserTime:J

.field private mBuffer:[B

.field private mCurPids:[I

.field private mCurThreadPids:[I

.field private mCurrentSampleRealTime:J

.field private mCurrentSampleTime:J

.field private mFirst:Z

.field private final mIncludeThreads:Z

.field private mLastSampleRealTime:J

.field private mLastSampleTime:J

.field private mLoad1:F

.field private mLoad15:F

.field private mLoad5:F

.field private final mLoadAverageData:[F

.field private final mProcStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field private final mProcessFullStatsData:[J

.field private final mProcessFullStatsStringData:[Ljava/lang/String;

.field private final mProcessStatsData:[J

.field private mRelIdleTime:I

.field private mRelIoWaitTime:I

.field private mRelIrqTime:I

.field private mRelSoftIrqTime:I

.field private mRelSystemTime:I

.field private mRelUserTime:I

.field private final mSinglePidStatsData:[J

.field private final mSystemCpuData:[J

.field private final mWorkingProcs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field private final mWorkingProcsMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkingProcsSorted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->PROCESS_STATS_FORMAT:[I

    .line 72
    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->PROCESS_FULL_STATS_FORMAT:[I

    .line 106
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->SYSTEM_CPU_FORMAT:[I

    .line 119
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->LOAD_AVERAGE_FORMAT:[I

    .line 240
    new-instance v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$1;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/os/ProcessStats$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x20
        0x220
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x2020
        0x20
        0x2020
        0x20
        0x2020
        0x2020
    .end array-data

    .line 72
    :array_1
    .array-data 4
        0x20
        0x1220
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x2020
        0x20
        0x2020
        0x20
        0x2020
        0x2020
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x2020
    .end array-data

    .line 106
    :array_2
    .array-data 4
        0x120
        0x2020
        0x2020
        0x2020
        0x2020
        0x2020
        0x2020
        0x2020
    .end array-data

    .line 119
    :array_3
    .array-data 4
        0x4020
        0x4020
        0x4020
    .end array-data
.end method

.method public constructor <init>(Z)V
    .locals 4
    .param p1, "includeThreads"    # Z

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessStatsData:[J

    .line 70
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mSinglePidStatsData:[J

    .line 103
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessFullStatsStringData:[Ljava/lang/String;

    .line 104
    new-array v0, v3, [J

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessFullStatsData:[J

    .line 117
    const/4 v0, 0x7

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mSystemCpuData:[J

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoadAverageData:[F

    .line 129
    iput v1, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad1:F

    .line 130
    iput v1, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad5:F

    .line 131
    iput v1, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad15:F

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mFirst:Z

    .line 161
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBuffer:[B

    .line 956
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsMap:Landroid/util/SparseArray;

    .line 264
    iput-boolean p1, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mIncludeThreads:Z

    .line 265
    return-void
.end method

.method private collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I
    .locals 28
    .param p1, "statsFile"    # Ljava/lang/String;
    .param p2, "parentPid"    # I
    .param p3, "first"    # Z
    .param p4, "curPids"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ[I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 358
    .local p5, "allProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;>;"
    const/4 v4, -0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningPids()[I

    move-result-object v18

    .line 361
    .local v18, "pids":[I
    :goto_0
    if-nez v18, :cond_2

    const/4 v10, 0x0

    .line 362
    .local v10, "NP":I
    :goto_1
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 363
    .local v11, "NS":I
    const/4 v12, 0x0

    .line 365
    .local v12, "curStatsIndex":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    if-ge v13, v10, :cond_0

    .line 366
    aget v6, v18, v13

    .line 368
    .local v6, "pid":I
    if-gez v6, :cond_3

    .line 369
    move v10, v6

    .line 537
    .end local v6    # "pid":I
    :cond_0
    :goto_3
    if-ge v12, v11, :cond_10

    .line 539
    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    .line 540
    .local v21, "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    .line 541
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    .line 542
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_minfaults:I

    .line 543
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_majfaults:I

    .line 544
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->removed:Z

    .line 545
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    .line 546
    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 547
    add-int/lit8 v11, v11, -0x1

    .line 552
    goto :goto_3

    .line 358
    .end local v10    # "NP":I
    .end local v11    # "NS":I
    .end local v12    # "curStatsIndex":I
    .end local v13    # "i":I
    .end local v18    # "pids":[I
    .end local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Landroid/os/Process;->getPids(Ljava/lang/String;[I)[I

    move-result-object v18

    goto :goto_0

    .line 361
    .restart local v18    # "pids":[I
    :cond_2
    move-object/from16 v0, v18

    array-length v10, v0

    goto :goto_1

    .line 373
    .restart local v6    # "pid":I
    .restart local v10    # "NP":I
    .restart local v11    # "NS":I
    .restart local v12    # "curStatsIndex":I
    .restart local v13    # "i":I
    :cond_3
    if-ge v12, v11, :cond_5

    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    move-object/from16 v21, v4

    .line 375
    .restart local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :goto_4
    if-eqz v21, :cond_a

    move-object/from16 v0, v21

    iget v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->pid:I

    if-ne v4, v6, :cond_a

    .line 377
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->added:Z

    .line 378
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    .line 379
    add-int/lit8 v12, v12, 0x1

    .line 385
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->interesting:Z

    if-eqz v4, :cond_4

    .line 386
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 387
    .local v24, "uptime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessStatsData:[J

    move-object/from16 v19, v0

    .line 389
    .local v19, "procStats":[J
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/taskmanager/os/ProcessStats;->PROCESS_STATS_FORMAT:[I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-static {v4, v5, v7, v0, v8}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v4

    if-nez v4, :cond_6

    .line 365
    .end local v19    # "procStats":[J
    .end local v24    # "uptime":J
    :cond_4
    :goto_5
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 373
    .end local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_5
    const/16 v21, 0x0

    goto :goto_4

    .line 394
    .restart local v19    # "procStats":[J
    .restart local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    .restart local v24    # "uptime":J
    :cond_6
    const/4 v4, 0x0

    aget-wide v16, v19, v4

    .line 395
    .local v16, "minfaults":J
    const/4 v4, 0x1

    aget-wide v14, v19, v4

    .line 396
    .local v14, "majfaults":J
    const/4 v4, 0x2

    aget-wide v26, v19, v4

    .line 397
    .local v26, "utime":J
    const/4 v4, 0x3

    aget-wide v22, v19, v4

    .line 399
    .local v22, "stime":J
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_utime:J

    cmp-long v4, v26, v4

    if-nez v4, :cond_7

    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_stime:J

    cmp-long v4, v22, v4

    if-nez v4, :cond_7

    .line 400
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    .line 401
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    .line 402
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_minfaults:I

    .line 403
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_majfaults:I

    .line 405
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->active:Z

    if-eqz v4, :cond_4

    .line 406
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->active:Z

    goto :goto_5

    .line 412
    :cond_7
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->active:Z

    if-nez v4, :cond_8

    .line 413
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->active:Z

    .line 416
    :cond_8
    if-gez p2, :cond_9

    .line 417
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getName(Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;Ljava/lang/String;)V

    .line 419
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    if-eqz v4, :cond_9

    .line 420
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurThreadPids:[I

    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurThreadPids:[I

    .line 431
    :cond_9
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_uptime:J

    sub-long v4, v24, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_uptime:J

    .line 432
    move-wide/from16 v0, v24

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_uptime:J

    .line 433
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_utime:J

    sub-long v4, v26, v4

    long-to-int v4, v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    .line 434
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_stime:J

    sub-long v4, v22, v4

    long-to-int v4, v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    .line 435
    move-wide/from16 v0, v26

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_utime:J

    .line 436
    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_stime:J

    .line 437
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_minfaults:J

    sub-long v4, v16, v4

    long-to-int v4, v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_minfaults:I

    .line 438
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_majfaults:J

    sub-long v4, v14, v4

    long-to-int v4, v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_majfaults:I

    .line 439
    move-wide/from16 v0, v16

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_minfaults:J

    .line 440
    move-object/from16 v0, v21

    iput-wide v14, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_majfaults:J

    .line 441
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    goto/16 :goto_5

    .line 447
    .end local v14    # "majfaults":J
    .end local v16    # "minfaults":J
    .end local v19    # "procStats":[J
    .end local v22    # "stime":J
    .end local v24    # "uptime":J
    .end local v26    # "utime":J
    :cond_a
    if-eqz v21, :cond_b

    move-object/from16 v0, v21

    iget v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->pid:I

    if-le v4, v6, :cond_f

    .line 449
    :cond_b
    new-instance v21, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    .end local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mIncludeThreads:Z

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-direct {v0, v6, v1, v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;-><init>(IIZ)V

    .line 450
    .restart local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    move-object/from16 v0, p5

    move-object/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 451
    add-int/lit8 v12, v12, 0x1

    .line 452
    add-int/lit8 v11, v11, 0x1

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessFullStatsStringData:[Ljava/lang/String;

    move-object/from16 v20, v0

    .line 459
    .local v20, "procStatsString":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcessFullStatsData:[J

    move-object/from16 v19, v0

    .line 460
    .restart local v19    # "procStats":[J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_uptime:J

    .line 462
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/taskmanager/os/ProcessStats;->PROCESS_FULL_STATS_FORMAT:[I

    const/4 v7, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v4, v5, v0, v1, v7}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 470
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->interesting:Z

    .line 471
    const/4 v4, 0x0

    aget-object v4, v20, v4

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    .line 472
    const/4 v4, 0x1

    aget-wide v4, v19, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_minfaults:J

    .line 473
    const/4 v4, 0x2

    aget-wide v4, v19, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_majfaults:J

    .line 474
    const/4 v4, 0x3

    aget-wide v4, v19, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_utime:J

    .line 475
    const/4 v4, 0x4

    aget-wide v4, v19, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_stime:J

    .line 488
    :goto_6
    if-gez p2, :cond_e

    .line 489
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getName(Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;Ljava/lang/String;)V

    .line 491
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    if-eqz v4, :cond_c

    .line 492
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurThreadPids:[I

    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurThreadPids:[I

    .line 504
    :cond_c
    :goto_7
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    .line 505
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    .line 506
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_minfaults:I

    .line 507
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_majfaults:I

    .line 508
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->added:Z

    .line 510
    if-nez p3, :cond_4

    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->interesting:Z

    if-eqz v4, :cond_4

    .line 511
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    goto/16 :goto_5

    .line 482
    :cond_d
    const-string v4, "TaskManager:ProcessStats"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Skipping unknown process pid "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const-string v4, "<unknown>"

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    .line 484
    const-wide/16 v4, 0x0

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_stime:J

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_utime:J

    .line 485
    const-wide/16 v4, 0x0

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_majfaults:J

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->base_minfaults:J

    goto/16 :goto_6

    .line 495
    :cond_e
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->interesting:Z

    if-eqz v4, :cond_c

    .line 496
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    .line 497
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->nameWidth:I

    goto :goto_7

    .line 518
    .end local v19    # "procStats":[J
    .end local v20    # "procStatsString":[Ljava/lang/String;
    :cond_f
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    .line 519
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    .line 520
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_minfaults:I

    .line 521
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_majfaults:I

    .line 522
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->removed:Z

    .line 523
    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    .line 524
    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 525
    add-int/lit8 v11, v11, -0x1

    .line 533
    add-int/lit8 v13, v13, -0x1

    .line 534
    goto/16 :goto_5

    .line 554
    .end local v6    # "pid":I
    .end local v21    # "st":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_10
    return-object v18
.end method

.method private getName(Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;Ljava/lang/String;)V
    .locals 5
    .param p1, "st"    # Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    .param p2, "cmdlineFile"    # Ljava/lang/String;

    .prologue
    .line 929
    iget-object v2, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    .line 931
    .local v2, "newName":Ljava/lang/String;
    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    const-string v4, "app_process"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    const-string v4, "<pre-initialized>"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 933
    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, p2, v3}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    .line 935
    .local v0, "cmdName":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 936
    move-object v2, v0

    .line 937
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 939
    .local v1, "i":I
    if-lez v1, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_1

    .line 940
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 944
    .end local v1    # "i":I
    :cond_1
    if-nez v2, :cond_2

    .line 945
    iget-object v2, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    .line 949
    .end local v0    # "cmdName":Ljava/lang/String;
    :cond_2
    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 950
    :cond_3
    iput-object v2, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    .line 951
    iget-object v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->name:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->nameWidth:I

    .line 953
    :cond_4
    return-void
.end method

.method private readFile(Ljava/lang/String;C)Ljava/lang/String;
    .locals 8
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "endChar"    # C

    .prologue
    .line 893
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v4

    .line 894
    .local v4, "savedPolicy":Landroid/os/StrictMode$ThreadPolicy;
    const/4 v1, 0x0

    .line 897
    .local v1, "is":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 898
    .end local v1    # "is":Ljava/io/FileInputStream;
    .local v2, "is":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBuffer:[B

    invoke-virtual {v2, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    .line 899
    .local v3, "len":I
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 901
    if-lez v3, :cond_3

    .line 904
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 905
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBuffer:[B

    aget-byte v5, v5, v0

    if-ne v5, p2, :cond_2

    .line 910
    :cond_0
    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBuffer:[B

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7, v0}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 915
    if-eqz v2, :cond_1

    .line 917
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 922
    :cond_1
    :goto_1
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    move-object v1, v2

    .line 925
    .end local v0    # "i":I
    .end local v2    # "is":Ljava/io/FileInputStream;
    .end local v3    # "len":I
    .restart local v1    # "is":Ljava/io/FileInputStream;
    :goto_2
    return-object v5

    .line 904
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v0    # "i":I
    .restart local v2    # "is":Ljava/io/FileInputStream;
    .restart local v3    # "len":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 915
    .end local v0    # "i":I
    :cond_3
    if-eqz v2, :cond_4

    .line 917
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 922
    :cond_4
    :goto_3
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    move-object v1, v2

    .line 925
    .end local v2    # "is":Ljava/io/FileInputStream;
    .end local v3    # "len":I
    .restart local v1    # "is":Ljava/io/FileInputStream;
    :goto_4
    const/4 v5, 0x0

    goto :goto_2

    .line 912
    :catch_0
    move-exception v5

    .line 915
    :goto_5
    if-eqz v1, :cond_5

    .line 917
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 922
    :cond_5
    :goto_6
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_4

    .line 913
    :catch_1
    move-exception v5

    .line 915
    :goto_7
    if-eqz v1, :cond_6

    .line 917
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 922
    :cond_6
    :goto_8
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_4

    .line 915
    :catchall_0
    move-exception v5

    :goto_9
    if-eqz v1, :cond_7

    .line 917
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 922
    :cond_7
    :goto_a
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v5

    .line 918
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v0    # "i":I
    .restart local v2    # "is":Ljava/io/FileInputStream;
    .restart local v3    # "len":I
    :catch_2
    move-exception v6

    goto :goto_1

    .end local v0    # "i":I
    :catch_3
    move-exception v5

    goto :goto_3

    .end local v2    # "is":Ljava/io/FileInputStream;
    .end local v3    # "len":I
    .restart local v1    # "is":Ljava/io/FileInputStream;
    :catch_4
    move-exception v5

    goto :goto_6

    :catch_5
    move-exception v5

    goto :goto_8

    :catch_6
    move-exception v6

    goto :goto_a

    .line 915
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v2    # "is":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "is":Ljava/io/FileInputStream;
    goto :goto_9

    .line 913
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v2    # "is":Ljava/io/FileInputStream;
    :catch_7
    move-exception v5

    move-object v1, v2

    .end local v2    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "is":Ljava/io/FileInputStream;
    goto :goto_7

    .line 912
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v2    # "is":Ljava/io/FileInputStream;
    :catch_8
    move-exception v5

    move-object v1, v2

    .end local v2    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "is":Ljava/io/FileInputStream;
    goto :goto_5
.end method


# virtual methods
.method final buildWorkingProcs()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 691
    iget-boolean v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsSorted:Z

    if-nez v6, :cond_4

    .line 692
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 693
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 695
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 696
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    .line 698
    .local v4, "stats":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    iget-boolean v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    if-eqz v6, :cond_2

    .line 699
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v8, :cond_2

    .line 702
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 703
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 705
    .local v0, "M":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v0, :cond_1

    .line 706
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    .line 708
    .local v5, "tstats":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    iget-boolean v6, v5, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->working:Z

    if-eqz v6, :cond_0

    .line 709
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 705
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 713
    .end local v5    # "tstats":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_1
    iget-object v6, v4, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    sget-object v7, Lcom/sec/android/app/taskmanager/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 695
    .end local v0    # "M":I
    .end local v3    # "j":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 718
    .end local v4    # "stats":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    sget-object v7, Lcom/sec/android/app/taskmanager/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 719
    iput-boolean v8, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsSorted:Z

    .line 721
    .end local v1    # "N":I
    .end local v2    # "i":I
    :cond_4
    return-void
.end method

.method public final buildWorkingProcsMap()V
    .locals 4

    .prologue
    .line 959
    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsSorted:Z

    if-nez v2, :cond_0

    .line 960
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->buildWorkingProcs()V

    .line 961
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 963
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    .line 964
    .local v1, "s":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsMap:Landroid/util/SparseArray;

    iget v3, v1, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->pid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 967
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "s":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_0
    return-void
.end method

.method public final getLastIdleTime()I
    .locals 1

    .prologue
    .line 677
    iget v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelIdleTime:I

    return v0
.end method

.method public final getLastIrqTime()I
    .locals 1

    .prologue
    .line 669
    iget v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelIrqTime:I

    return v0
.end method

.method public final getLastSystemTime()I
    .locals 1

    .prologue
    .line 661
    iget v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelSystemTime:I

    return v0
.end method

.method public final getLastUserTime()I
    .locals 1

    .prologue
    .line 657
    iget v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelUserTime:I

    return v0
.end method

.method public final getWorkingStatByPid(I)Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    .locals 1
    .param p1, "pid"    # I

    .prologue
    .line 970
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    return-object v0
.end method

.method public init()V
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mFirst:Z

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->update()V

    .line 281
    return-void
.end method

.method public onLoadChanged(FFF)V
    .locals 0
    .param p1, "load1"    # F
    .param p2, "load5"    # F
    .param p3, "load15"    # F

    .prologue
    .line 268
    return-void
.end method

.method public onMeasureProcessName(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 271
    const/4 v0, 0x0

    return v0
.end method

.method public update()V
    .locals 28

    .prologue
    .line 288
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurrentSampleTime:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLastSampleTime:J

    .line 289
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurrentSampleTime:J

    .line 290
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurrentSampleRealTime:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLastSampleRealTime:J

    .line 291
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurrentSampleRealTime:J

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mSystemCpuData:[J

    move-object/from16 v22, v0

    .line 294
    .local v22, "sysCpu":[J
    const-string v4, "/proc/stat"

    sget-object v5, Lcom/sec/android/app/taskmanager/os/ProcessStats;->SYSTEM_CPU_FORMAT:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-static {v4, v5, v6, v0, v7}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 297
    const/4 v4, 0x0

    aget-wide v4, v22, v4

    const/4 v6, 0x1

    aget-wide v6, v22, v6

    add-long v26, v4, v6

    .line 299
    .local v26, "usertime":J
    const/4 v4, 0x2

    aget-wide v24, v22, v4

    .line 301
    .local v24, "systemtime":J
    const/4 v4, 0x3

    aget-wide v10, v22, v4

    .line 303
    .local v10, "idletime":J
    const/4 v4, 0x4

    aget-wide v12, v22, v4

    .line 304
    .local v12, "iowaittime":J
    const/4 v4, 0x5

    aget-wide v14, v22, v4

    .line 305
    .local v14, "irqtime":J
    const/4 v4, 0x6

    aget-wide v20, v22, v4

    .line 306
    .local v20, "softirqtime":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseUserTime:J

    sub-long v4, v26, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelUserTime:I

    .line 307
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseSystemTime:J

    sub-long v4, v24, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelSystemTime:I

    .line 308
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIoWaitTime:J

    sub-long v4, v12, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelIoWaitTime:I

    .line 309
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIrqTime:J

    sub-long v4, v14, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelIrqTime:I

    .line 310
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseSoftIrqTime:J

    sub-long v4, v20, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelSoftIrqTime:I

    .line 311
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIdleTime:J

    sub-long v4, v10, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mRelIdleTime:I

    .line 322
    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseUserTime:J

    .line 323
    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseSystemTime:J

    .line 324
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIoWaitTime:J

    .line 325
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIrqTime:J

    .line 326
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseSoftIrqTime:J

    .line 327
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mBaseIdleTime:J

    .line 330
    .end local v10    # "idletime":J
    .end local v12    # "iowaittime":J
    .end local v14    # "irqtime":J
    .end local v20    # "softirqtime":J
    .end local v24    # "systemtime":J
    .end local v26    # "usertime":J
    :cond_0
    const-string v5, "/proc"

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mFirst:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurPids:[I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mCurPids:[I

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoadAverageData:[F

    move-object/from16 v19, v0

    .line 333
    .local v19, "loadAverages":[F
    const-string v4, "/proc/loadavg"

    sget-object v5, Lcom/sec/android/app/taskmanager/os/ProcessStats;->LOAD_AVERAGE_FORMAT:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-static {v4, v5, v6, v7, v0}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 335
    const/4 v4, 0x0

    aget v16, v19, v4

    .line 336
    .local v16, "load1":F
    const/4 v4, 0x1

    aget v18, v19, v4

    .line 337
    .local v18, "load5":F
    const/4 v4, 0x2

    aget v17, v19, v4

    .line 339
    .local v17, "load15":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad1:F

    cmpl-float v4, v16, v4

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad5:F

    cmpl-float v4, v18, v4

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad15:F

    cmpl-float v4, v17, v4

    if-eqz v4, :cond_2

    .line 340
    :cond_1
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad1:F

    .line 341
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad5:F

    .line 342
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mLoad15:F

    .line 343
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->onLoadChanged(FFF)V

    .line 350
    .end local v16    # "load1":F
    .end local v17    # "load15":F
    .end local v18    # "load5":F
    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mWorkingProcsSorted:Z

    .line 351
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;->mFirst:Z

    .line 352
    return-void
.end method
