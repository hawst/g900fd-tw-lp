.class Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;
.super Ljava/lang/Object;
.source "TaskManagerActivity.java"

# interfaces
.implements Lcom/sec/android/app/taskmanager/monitor/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V
    .locals 14
    .param p1, "monitor"    # Lcom/sec/android/app/taskmanager/monitor/Monitor;

    .prologue
    .line 369
    const/4 v1, 0x0

    .line 371
    .local v1, "changedMem":Z
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getList()Ljava/util/List;

    move-result-object v0

    .line 372
    .local v0, "appInfos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-nez v0, :cond_1

    .line 412
    .end local v1    # "changedMem":Z
    :cond_0
    :goto_0
    return-void

    .line 375
    .restart local v1    # "changedMem":Z
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v1    # "changedMem":Z
    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 376
    .local v8, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->isRunning()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 379
    const/4 v5, 0x0

    .line 380
    .local v5, "needUpdateUi":Z
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$300(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/MemoryManager;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/sec/android/app/taskmanager/MemoryManager;->getPackageUsedMem(Lcom/sec/android/app/taskmanager/PackageInfoItem;)J

    move-result-wide v6

    .line 381
    .local v6, "memUsage":J
    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsage()J

    move-result-wide v10

    cmp-long v9, v10, v6

    if-eqz v9, :cond_3

    .line 382
    const/4 v5, 0x1

    move v1, v5

    .line 383
    .local v1, "changedMem":I
    invoke-virtual {v8, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setMemUsage(J)V

    .line 384
    const-string v9, "TaskManager:TaskManagerActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Observer(Mem)-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsageString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    .end local v1    # "changedMem":I
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$400(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->getPackageCpuUsage(Lcom/sec/android/app/taskmanager/PackageInfoItem;)F

    move-result v2

    .line 390
    .local v2, "cpuUsage":F
    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuUsage()F

    move-result v9

    sub-float v9, v2, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    const-wide v12, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v9, v10, v12

    if-ltz v9, :cond_4

    .line 391
    const/4 v5, 0x1

    .line 392
    invoke-virtual {v8, v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->setCpuUsage(F)V

    .line 393
    const-string v9, "TaskManager:TaskManagerActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Observer(Cpu)-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuRateFormatted()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_4
    if-eqz v5, :cond_2

    .line 398
    invoke-virtual {v8}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getViewHolder()Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    move-result-object v3

    .line 399
    .local v3, "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    if-eqz v3, :cond_2

    .line 400
    invoke-virtual {p1, v8}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->publishUi(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 405
    .end local v2    # "cpuUsage":F
    .end local v3    # "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    .end local v5    # "needUpdateUi":Z
    .end local v6    # "memUsage":J
    .end local v8    # "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_5
    if-eqz v1, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getSortOrder()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 407
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->checkOrder(Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 408
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v9}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->sort(Ljava/util/List;)V

    .line 409
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->publishUi(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onUpdateUi(Ljava/lang/Object;)V
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 415
    if-eqz p1, :cond_1

    move-object v1, p1

    .line 416
    check-cast v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 417
    .local v1, "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getViewHolder()Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;

    move-result-object v0

    .line 418
    .local v0, "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    if-eqz v0, :cond_0

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->updateUi(Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;)V

    .line 425
    .end local v0    # "holder":Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter$AppViewHolder;
    .end local v1    # "pkgInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    const-string v2, "TaskManager:TaskManagerActivity"

    const-string v3, "onUpdateUi : obj == null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->onChangedPackageList(Ljava/util/List;)V

    goto :goto_0
.end method
