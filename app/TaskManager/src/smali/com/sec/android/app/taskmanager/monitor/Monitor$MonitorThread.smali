.class Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;
.super Ljava/lang/Thread;
.source "Monitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/monitor/Monitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MonitorThread"
.end annotation


# instance fields
.field private interval:I

.field running:Z

.field final synthetic this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->running:Z

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->getInterval()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->interval:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;Lcom/sec/android/app/taskmanager/monitor/Monitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/taskmanager/monitor/Monitor;
    .param p2, "x1"    # Lcom/sec/android/app/taskmanager/monitor/Monitor$1;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;-><init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->running:Z

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->interrupt()V

    .line 73
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->onStart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->interval:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->running:Z

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->onMonitor()V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    # invokes: Lcom/sec/android/app/taskmanager/monitor/Monitor;->notifyObserver()V
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->access$200(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V

    .line 89
    :try_start_1
    iget v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->interval:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->this$0:Lcom/sec/android/app/taskmanager/monitor/Monitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->onStop()V

    .line 95
    return-void

    .line 80
    :catch_1
    move-exception v0

    goto :goto_0
.end method
