.class public Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;
.super Ljava/lang/Object;
.source "PackageInfoItemCache.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/PackageInfoItemCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CachedItem"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2ecd227220L


# instance fields
.field private appName:Ljava/lang/String;

.field private codeSize:J

.field private dataSize:J

.field private packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "codeSize"    # J
    .param p5, "dataSize"    # J

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->packageName:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->appName:Ljava/lang/String;

    .line 47
    iput-wide p3, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->codeSize:J

    .line 48
    iput-wide p5, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->dataSize:J

    .line 49
    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getCodeSize()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->codeSize:J

    return-wide v0
.end method

.method public getDataSize()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/sec/android/app/taskmanager/PackageInfoItemCache$CachedItem;->dataSize:J

    return-wide v0
.end method
