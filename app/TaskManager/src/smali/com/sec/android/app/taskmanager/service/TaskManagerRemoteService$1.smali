.class Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;
.super Lcom/sec/android/app/taskmanager/service/ITaskManagerService$Stub;
.source "TaskManagerRemoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/service/ITaskManagerService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getCPUWarningLevel()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    const-string v0, "TaskManager:TaskManagerRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCPUWarningLevel() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mLevel:Ljava/lang/String;

    return-object v0
.end method

.method public getCPUWarningMessage()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 168
    const-string v0, "TaskManager:TaskManagerRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCPUWarningMessage() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getRunningApplicationCount()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    const-string v2, "TaskManager:TaskManagerRemoteService"

    const-string v3, "getRunningApplicationCount() start"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v1, 0x0

    .line 106
    .local v1, "size":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v3}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningApplicationList()Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$102(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;Ljava/util/List;)Ljava/util/List;

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 115
    :cond_0
    :goto_0
    const-string v2, "TaskManager:TaskManagerRemoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRunningApplicationCount() end "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initCPUUsage()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 140
    const-string v1, "TaskManager:TaskManagerRemoteService"

    const-string v2, "initCPUUsage()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->init()V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->update()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isCPUWarning()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 160
    const-string v0, "TaskManager:TaskManagerRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCPUWarning() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuWarningInfo:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$500(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$CpuWarningInfo;->mIsWarning:Z

    return v0
.end method

.method public isUpdateRunningApplicationListOnProgress()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 131
    .local v0, "bRet":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfo;->isUpdateRunningApplicationListOnProgress()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 136
    :goto_0
    const-string v2, "TaskManager:TaskManagerRemoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isUpdateRunningApplicationListOnProgress() bRet = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return v0

    .line 132
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public killAll()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x3

    .line 172
    const-string v2, "TaskManager:TaskManagerRemoteService"

    const-string v3, "killAll() "

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v3}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfo;->getRunningAppPackageList()Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$102(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;Ljava/util/List;)Ljava/util/List;

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mRunningAppList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$100(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 177
    .local v1, "r":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    const-string v2, "TaskManager:TaskManagerRemoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "kill "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/taskmanager/PackageInfo;->killPackage(Ljava/lang/String;Z)V

    goto :goto_0

    .line 181
    .end local v1    # "r":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 186
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public killPackage(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageNmae"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 188
    const-string v0, "TaskManager:TaskManagerRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "killPackage() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    if-eqz p1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->killPackage(Ljava/lang/String;Z)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 195
    :cond_0
    return-void
.end method

.method public updateCPUUsage()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v1, "TaskManager:TaskManagerRemoteService"

    const-string v2, "updateCPUUsage()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mCpuInfo:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$300(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->update()V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # invokes: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->calcCPUUsage()Z
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$400(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :goto_0
    return-void

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public updateRunningApplicationListInfo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 119
    const-string v1, "TaskManager:TaskManagerRemoteService"

    const-string v2, "updateRunningApplicationListInfo()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService$1;->this$0:Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;

    # getter for: Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->mPackageInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;->access$200(Lcom/sec/android/app/taskmanager/service/TaskManagerRemoteService;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->updateRunningApplicationListInfo()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
