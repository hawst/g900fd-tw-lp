.class public Lcom/sec/android/app/taskmanager/activity/comparator/CpuUsageComparator;
.super Ljava/lang/Object;
.source "CpuUsageComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I
    .locals 3
    .param p1, "a"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "b"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuUsage()F

    move-result v0

    .line 25
    .local v0, "aCpu":F
    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCpuUsage()F

    move-result v1

    .line 27
    .local v1, "bCpu":F
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .line 28
    const/4 v2, -0x1

    .line 32
    :goto_0
    return v2

    .line 29
    :cond_0
    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    .line 30
    const/4 v2, 0x1

    goto :goto_0

    .line 32
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/comparator/CpuUsageComparator;->compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I

    move-result v0

    return v0
.end method
