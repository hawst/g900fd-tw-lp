.class public Lcom/sec/android/app/taskmanager/activity/comparator/MemoryUsageComparator;
.super Ljava/lang/Object;
.source "MemoryUsageComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I
    .locals 8
    .param p1, "a"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "b"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 25
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsage()J

    move-result-wide v6

    long-to-float v1, v6

    .line 26
    .local v1, "aMem":F
    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getMemUsage()J

    move-result-wide v6

    long-to-float v3, v6

    .line 27
    .local v3, "bMem":F
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCategory()I

    move-result v0

    .line 28
    .local v0, "aCategory":I
    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getCategory()I

    move-result v2

    .line 31
    .local v2, "bCategory":I
    if-le v0, v2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v4

    .line 33
    :cond_1
    if-ge v0, v2, :cond_2

    move v4, v5

    .line 34
    goto :goto_0

    .line 37
    :cond_2
    cmpl-float v6, v1, v3

    if-lez v6, :cond_3

    move v4, v5

    .line 38
    goto :goto_0

    .line 39
    :cond_3
    cmpg-float v5, v1, v3

    if-ltz v5, :cond_0

    .line 42
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/comparator/MemoryUsageComparator;->compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I

    move-result v0

    return v0
.end method
