.class public final enum Lcom/sec/android/app/taskmanager/Utils$ScafeSize;
.super Ljava/lang/Enum;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScafeSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/taskmanager/Utils$ScafeSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

.field public static final enum GRANDE:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

.field public static final enum SHORT:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

.field public static final enum TALL:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

.field public static final enum VENTI:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 406
    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->SHORT:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    .line 407
    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    const-string v1, "TALL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->TALL:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    .line 408
    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    const-string v1, "GRANDE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->GRANDE:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    .line 409
    new-instance v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    const-string v1, "VENTI"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->VENTI:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    .line 405
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->SHORT:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->TALL:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->GRANDE:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->VENTI:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->$VALUES:[Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 405
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/taskmanager/Utils$ScafeSize;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 405
    const-class v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/taskmanager/Utils$ScafeSize;
    .locals 1

    .prologue
    .line 405
    sget-object v0, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->$VALUES:[Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    invoke-virtual {v0}, [Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    return-object v0
.end method
