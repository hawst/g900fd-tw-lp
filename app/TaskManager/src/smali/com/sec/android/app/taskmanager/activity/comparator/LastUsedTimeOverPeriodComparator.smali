.class public Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;
.super Ljava/lang/Object;
.source "LastUsedTimeOverPeriodComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public final compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I
    .locals 8
    .param p1, "a"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "b"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 33
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v6

    if-nez v6, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v4

    .line 37
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v6

    if-nez v6, :cond_3

    :cond_2
    move v4, v5

    .line 38
    goto :goto_0

    .line 41
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getLastAccessedOverPeriodCount(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    .line 42
    .local v0, "aCount":J
    iget-object v6, p0, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getLastAccessedOverPeriodCount(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    .line 44
    .local v2, "bCount":J
    cmp-long v6, v0, v2

    if-gtz v6, :cond_0

    .line 46
    cmp-long v4, v0, v2

    if-nez v4, :cond_4

    .line 47
    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    move v4, v5

    .line 49
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/comparator/LastUsedTimeOverPeriodComparator;->compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I

    move-result v0

    return v0
.end method
