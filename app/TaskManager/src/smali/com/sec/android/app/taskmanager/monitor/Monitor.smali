.class public abstract Lcom/sec/android/app/taskmanager/monitor/Monitor;
.super Ljava/lang/Object;
.source "Monitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;,
        Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;
    }
.end annotation


# static fields
.field private static mHandler:Landroid/os/Handler;


# instance fields
.field protected final TAG:Ljava/lang/String;

.field private mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

.field private mObserver:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/app/taskmanager/monitor/Observer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/taskmanager/monitor/Monitor$1;

    invoke-direct {v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$1;-><init>()V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    sput-object v0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskManager:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->TAG:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    .line 100
    new-instance v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;-><init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;Lcom/sec/android/app/taskmanager/monitor/Monitor$1;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/taskmanager/monitor/Monitor;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/monitor/Monitor;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->notifyObserverUi(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/monitor/Monitor;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->notifyObserver()V

    return-void
.end method

.method private final notifyObserver()V
    .locals 3

    .prologue
    .line 142
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/monitor/Observer;

    .line 143
    .local v1, "o":Lcom/sec/android/app/taskmanager/monitor/Observer;
    invoke-interface {v1, p0}, Lcom/sec/android/app/taskmanager/monitor/Observer;->onUpdate(Lcom/sec/android/app/taskmanager/monitor/Monitor;)V

    goto :goto_0

    .line 145
    .end local v1    # "o":Lcom/sec/android/app/taskmanager/monitor/Observer;
    :cond_0
    return-void
.end method

.method private final notifyObserverUi(Ljava/lang/Object;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 148
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/taskmanager/monitor/Observer;

    .line 149
    .local v1, "o":Lcom/sec/android/app/taskmanager/monitor/Observer;
    invoke-interface {v1, p1}, Lcom/sec/android/app/taskmanager/monitor/Observer;->onUpdateUi(Ljava/lang/Object;)V

    goto :goto_0

    .line 151
    .end local v1    # "o":Lcom/sec/android/app/taskmanager/monitor/Observer;
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract getInterval()I
.end method

.method public final declared-synchronized isRunning()Z
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    iget-boolean v0, v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->running:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract onMonitor()V
.end method

.method protected onStart()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method protected onUpdateUi(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 172
    return-void
.end method

.method public final publishUi(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 154
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 155
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x23b1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 156
    new-instance v1, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MessageObj;-><init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 157
    sget-object v1, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 158
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    :cond_0
    new-instance v0, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;-><init>(Lcom/sec/android/app/taskmanager/monitor/Monitor;Lcom/sec/android/app/taskmanager/monitor/Monitor$1;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->setPriority(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->start()V

    .line 116
    :cond_1
    return-void
.end method

.method public final start(Lcom/sec/android/app/taskmanager/monitor/Observer;)V
    .locals 1
    .param p1, "o"    # Lcom/sec/android/app/taskmanager/monitor/Observer;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->start()V

    .line 108
    :cond_0
    return-void
.end method

.method public final declared-synchronized stop()V
    .locals 1

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;->cancel()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mMonitorThread:Lcom/sec/android/app/taskmanager/monitor/Monitor$MonitorThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final stop(Lcom/sec/android/app/taskmanager/monitor/Observer;)V
    .locals 1
    .param p1, "o"    # Lcom/sec/android/app/taskmanager/monitor/Observer;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/Monitor;->mObserver:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;->stop()V

    .line 124
    :cond_0
    return-void
.end method
