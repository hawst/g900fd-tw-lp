.class public Lcom/sec/android/app/taskmanager/MemoryManager;
.super Ljava/lang/Object;
.source "MemoryManager.java"


# static fields
.field private static instance:Lcom/sec/android/app/taskmanager/MemoryManager;


# instance fields
.field protected final TAG:Ljava/lang/String;

.field private mAm:Landroid/app/ActivityManager;

.field private mContext:Landroid/content/Context;

.field private mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

.field mPrevAvailMem:J

.field mPrevLowMemory:Z

.field mPrevTotalMem:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/taskmanager/MemoryManager;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/MemoryManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/taskmanager/MemoryManager;->instance:Lcom/sec/android/app/taskmanager/MemoryManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskManager:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->TAG:Ljava/lang/String;

    .line 29
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/taskmanager/MemoryManager;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/taskmanager/MemoryManager;->instance:Lcom/sec/android/app/taskmanager/MemoryManager;

    return-object v0
.end method


# virtual methods
.method public getAvailMem()J
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    return-wide v0
.end method

.method public getPackageUsedMem(Lcom/sec/android/app/taskmanager/PackageInfoItem;)J
    .locals 10
    .param p1, "pkgInfo"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 57
    const-wide/16 v6, 0x0

    .line 58
    .local v6, "pss":J
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPids()[I

    move-result-object v5

    .line 60
    .local v5, "pids":[I
    if-eqz v5, :cond_1

    .line 61
    iget-object v8, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mAm:Landroid/app/ActivityManager;

    invoke-virtual {v8, v5}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v2

    .line 63
    .local v2, "debugMemInfos":[Landroid/os/Debug$MemoryInfo;
    if-nez v2, :cond_0

    .line 64
    const-wide/16 v8, 0x0

    .line 81
    .end local v2    # "debugMemInfos":[Landroid/os/Debug$MemoryInfo;
    :goto_0
    return-wide v8

    .line 67
    .restart local v2    # "debugMemInfos":[Landroid/os/Debug$MemoryInfo;
    :cond_0
    move-object v0, v2

    .local v0, "arr$":[Landroid/os/Debug$MemoryInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 68
    .local v1, "debugMemInfo":Landroid/os/Debug$MemoryInfo;
    invoke-virtual {v1}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v8

    int-to-long v8, v8

    add-long/2addr v6, v8

    .line 67
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "arr$":[Landroid/os/Debug$MemoryInfo;
    .end local v1    # "debugMemInfo":Landroid/os/Debug$MemoryInfo;
    .end local v2    # "debugMemInfos":[Landroid/os/Debug$MemoryInfo;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    move-wide v8, v6

    .line 81
    goto :goto_0
.end method

.method public getTotalMemory()J
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    return-wide v0
.end method

.method public init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/MemoryManager;
    .locals 2
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mContext:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mAm:Landroid/app/ActivityManager;

    .line 53
    :cond_0
    return-object p0
.end method

.method public updateMemInfo()Z
    .locals 6

    .prologue
    .line 85
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mAm:Landroid/app/ActivityManager;

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    invoke-virtual {v1, v2}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    iget-wide v4, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevAvailMem:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    iget-wide v4, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevTotalMem:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-boolean v1, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    iget-boolean v2, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevLowMemory:Z

    if-eq v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 88
    .local v0, "bChanged":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    iput-wide v2, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevAvailMem:J

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    iput-wide v2, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevTotalMem:J

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-boolean v1, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    iput-boolean v1, p0, Lcom/sec/android/app/taskmanager/MemoryManager;->mPrevLowMemory:Z

    .line 91
    return v0

    .line 86
    .end local v0    # "bChanged":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
