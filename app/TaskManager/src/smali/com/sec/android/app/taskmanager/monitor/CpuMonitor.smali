.class public Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
.super Lcom/sec/android/app/taskmanager/monitor/Monitor;
.source "CpuMonitor.java"


# static fields
.field private static instance:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;


# instance fields
.field final mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

.field private mTotalCpuTime:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    invoke-direct {v0}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;-><init>()V

    sput-object v0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->instance:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/monitor/Monitor;-><init>()V

    .line 39
    new-instance v0, Lcom/sec/android/app/taskmanager/os/ProcessStats;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/taskmanager/os/ProcessStats;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->instance:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    return-object v0
.end method


# virtual methods
.method protected getInterval()I
    .locals 1

    .prologue
    .line 120
    const/16 v0, 0xbb8

    return v0
.end method

.method public getPackageCpuUsage(Lcom/sec/android/app/taskmanager/PackageInfoItem;)F
    .locals 10
    .param p1, "packageInfo"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 70
    const/4 v7, 0x0

    .line 71
    .local v7, "usage":F
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPids()[I

    move-result-object v4

    .line 73
    .local v4, "pids":[I
    if-eqz v4, :cond_2

    .line 74
    const/4 v6, 0x0

    .line 76
    .local v6, "time":I
    move-object v0, v4

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 77
    .local v3, "pid":I
    iget-object v8, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v8, v3}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getWorkingStatByPid(I)Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;

    move-result-object v5

    .line 79
    .local v5, "stat":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    if-eqz v5, :cond_0

    .line 80
    iget v8, v5, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_utime:I

    iget v9, v5, Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;->rel_stime:I

    add-int/2addr v8, v9

    add-int/2addr v6, v8

    .line 76
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    .end local v3    # "pid":I
    .end local v5    # "stat":Lcom/sec/android/app/taskmanager/os/ProcessStats$Stats;
    :cond_1
    iget v8, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 85
    mul-int/lit8 v8, v6, 0x64

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    div-float v7, v8, v9

    .line 89
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v6    # "time":I
    :cond_2
    return v7
.end method

.method public init()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    .line 94
    return-void
.end method

.method protected onMonitor()V
    .locals 0

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->update()V

    .line 132
    return-void
.end method

.method protected onStart()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->init()V

    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public update()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 97
    iget v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    .line 98
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->init()V

    .line 103
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->buildWorkingProcsMap()V

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getLastUserTime()I

    move-result v3

    .line 105
    .local v3, "user":I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getLastSystemTime()I

    move-result v2

    .line 107
    .local v2, "system":I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getLastIrqTime()I

    move-result v1

    .line 109
    .local v1, "irq":I
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->getLastIdleTime()I

    move-result v0

    .line 110
    .local v0, "idle":I
    add-int v4, v3, v2

    add-int/2addr v4, v1

    add-int/2addr v4, v0

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    .line 112
    iget v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update() : mTotalCpuTime is wrong!!! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mTotalCpuTime:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    return-void

    .line 100
    .end local v0    # "idle":I
    .end local v1    # "irq":I
    .end local v2    # "system":I
    .end local v3    # "user":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->mProcessStats:Lcom/sec/android/app/taskmanager/os/ProcessStats;

    invoke-virtual {v4}, Lcom/sec/android/app/taskmanager/os/ProcessStats;->update()V

    goto :goto_0
.end method
