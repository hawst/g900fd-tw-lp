.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;)V
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 645
    const-string v0, "UnusedAppsListActivity"

    const-string v1, "MSG_GET_LIST START"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v1, v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$300(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstallAppPackageList()Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mAllPackagInfoItem:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$202(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/List;)Ljava/util/List;

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v1, v1, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mAllPackagInfoItem:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$200(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v3, v3, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->lastusedOverPeriodList:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$400(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->removeOverPeriodPackage(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setList(Ljava/util/List;)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v0, v0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->notifyDataSetChanged()V

    .line 653
    const-string v0, "UnusedAppsListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MSG_GET_LIST END "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;->this$1:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;

    iget-object v2, v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    return-void
.end method
