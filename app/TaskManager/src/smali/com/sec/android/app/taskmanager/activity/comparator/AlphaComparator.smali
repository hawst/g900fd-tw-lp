.class public Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;
.super Ljava/lang/Object;
.source "AlphaComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final sCollator:Ljava/text/Collator;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;->sCollator:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public final compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I
    .locals 3
    .param p1, "a"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .param p2, "b"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 28
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    .line 29
    :cond_0
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    .line 32
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_3

    .line 33
    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    .line 36
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;->sCollator:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getAppName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/taskmanager/activity/comparator/AlphaComparator;->compare(Lcom/sec/android/app/taskmanager/PackageInfoItem;Lcom/sec/android/app/taskmanager/PackageInfoItem;)I

    move-result v0

    return v0
.end method
