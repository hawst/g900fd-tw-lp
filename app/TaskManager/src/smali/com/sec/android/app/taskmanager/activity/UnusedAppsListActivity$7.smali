.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 480
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getNotificationPeriod(Landroid/content/Context;)I

    move-result v2

    .line 481
    .local v2, "nOld":I
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->setNotificationPeriod(Landroid/content/Context;I)V

    .line 482
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 494
    .local v0, "currentTime":J
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v3, v2, p2, v0, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->manageAlarm(IIJ)V

    .line 496
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$7;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$100(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 497
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 498
    return-void
.end method
