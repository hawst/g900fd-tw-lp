.class public Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.super Landroid/app/Activity;
.source "TaskManagerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;
    }
.end annotation


# instance fields
.field private mActiveStatus:Landroid/widget/TextView;

.field private mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

.field private mDialogKill:Landroid/app/AlertDialog;

.field private mDialogKillAll:Landroid/app/AlertDialog;

.field private mEndAllButtonClickListener:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private mIsLandscape:Z

.field private mKillAll:Landroid/widget/Button;

.field private mKillAllPanel:Landroid/widget/LinearLayout;

.field private mLeftPanel:Landroid/view/View;

.field private mListView:Landroid/widget/ListView;

.field private mMemGraph:Landroid/widget/ProgressBar;

.field private mMemoryAvailable:Landroid/widget/TextView;

.field private mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

.field private mMenuItemEndall:Landroid/view/MenuItem;

.field private mObserver:Lcom/sec/android/app/taskmanager/monitor/Observer;

.field private mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

.field private mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

.field private mOnListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

.field private mPackageMonitor:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;

.field private mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

.field private mRightPanel:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 88
    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    .line 89
    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    .line 238
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$1;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mEndAllButtonClickListener:Landroid/view/View$OnClickListener;

    .line 253
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 289
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$3;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

    .line 298
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$4;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

    .line 330
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$5;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;

    .line 366
    new-instance v0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$6;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mObserver:Lcom/sec/android/app/taskmanager/monitor/Observer;

    .line 427
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateTotalMemory()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/MemoryManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/PackageInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    return-object v0
.end method

.method private updateKillAllBtn(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/taskmanager/PackageInfo;->isThereAppCanBeKilled(Ljava/util/List;)Z

    move-result v0

    .line 323
    .local v0, "canKillAll":Z
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    if-eqz v1, :cond_1

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 329
    :cond_1
    return-void
.end method

.method private updateStatus(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    const/4 v1, 0x0

    .line 316
    if-nez p1, :cond_0

    move v0, v1

    .line 318
    .local v0, "count":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mActiveStatus:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    return-void

    .line 316
    .end local v0    # "count":I
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method private updateTotalMemory()V
    .locals 12

    .prologue
    .line 353
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/MemoryManager;->updateMemInfo()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 354
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/MemoryManager;->getAvailMem()J

    move-result-wide v0

    .line 355
    .local v0, "avail":J
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

    invoke-virtual {v9}, Lcom/sec/android/app/taskmanager/MemoryManager;->getTotalMemory()J

    move-result-wide v4

    .line 356
    .local v4, "total":J
    sub-long v6, v4, v0

    .line 357
    .local v6, "used":J
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    invoke-static {v9, v6, v7}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 358
    .local v8, "usedStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    invoke-static {v9, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 359
    .local v3, "totalStr":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "<font color=\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->getTextColor(Landroid/content/Context;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</font>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 361
    .local v2, "memory":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryAvailable:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    sget-object v11, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 362
    iget-object v9, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemGraph:Landroid/widget/ProgressBar;

    const-wide/16 v10, 0x64

    mul-long/2addr v10, v6

    div-long/2addr v10, v4

    long-to-int v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 363
    const-string v9, "TaskManager:TaskManagerActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateTotalMemory()"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v0    # "avail":J
    .end local v2    # "memory":Ljava/lang/String;
    .end local v3    # "totalStr":Ljava/lang/String;
    .end local v4    # "total":J
    .end local v6    # "used":J
    .end local v8    # "usedStr":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method displayDialog(ILcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "item"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f08000c

    const v4, 0x104000a

    const/high16 v3, 0x1040000

    const/4 v2, 0x1

    .line 490
    packed-switch p1, :pswitch_data_0

    .line 524
    :goto_0
    return-void

    .line 492
    :pswitch_0
    if-eqz p2, :cond_0

    .line 493
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$7;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$7;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 505
    :cond_0
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "displayDialog() : item is NULL."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$8;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 490
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method kill(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .prologue
    .line 562
    invoke-virtual {p1}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 563
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "TaskManager:TaskManagerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Kill runningApplication! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/taskmanager/PackageInfo;->killPackage(Ljava/lang/String;Z)V

    .line 566
    :try_start_0
    const-string v2, "com.android.browser"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 582
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->removeItem(Lcom/sec/android/app/taskmanager/PackageInfoItem;)V

    .line 583
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->onChangedPackageList(Ljava/util/List;)V

    .line 584
    return-void

    .line 568
    :cond_1
    :try_start_1
    const-string v2, "com.sec.android.app.videoplayer"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 570
    const-string v2, "com.samsung.music"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.android.music"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.sec.android.app.music"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 574
    const-string v2, "com.android.settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 576
    const-string v2, "com.sec.android.app.camera"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "TaskManager:TaskManagerActivity"

    const-string v3, "MSG_KILL - not support SecHardwareInterface"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method killAll()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 586
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getList()Ljava/util/List;

    move-result-object v1

    .line 587
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-eqz v1, :cond_2

    .line 588
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 589
    .local v2, "p":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->isCanKilled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 590
    const-string v3, "TaskManager:TaskManagerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Kill runningApplication! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/taskmanager/PackageInfo;->killPackage(Ljava/lang/String;Z)V

    goto :goto_0

    .line 594
    .end local v2    # "p":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->setList(Ljava/util/List;)V

    .line 595
    invoke-virtual {p0, v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->onChangedPackageList(Ljava/util/List;)V

    .line 607
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    const v3, 0x7f08000b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 609
    return-void
.end method

.method onChangedPackageList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/taskmanager/PackageInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    if-eqz p1, :cond_0

    .line 551
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "onChangedPackageList() : packageList != null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->setList(Ljava/util/List;)V

    .line 557
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateStatus(Ljava/util/List;)V

    .line 558
    invoke-direct {p0, p1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateKillAllBtn(Ljava/util/List;)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->notifyDataSetChanged()V

    .line 560
    return-void

    .line 554
    :cond_0
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "onChangedPackageList() : packageList == null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getList()Ljava/util/List;

    move-result-object p1

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 248
    invoke-virtual {p0, p1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateLayout(Landroid/content/res/Configuration;)V

    .line 249
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f0c000d

    const v7, 0x7f0c000c

    const/16 v5, 0x400

    const/4 v3, 0x0

    .line 99
    const-string v1, "TaskManager:TaskManagerActivity"

    const-string v4, "onCreate()"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getTheme()Lcom/sec/android/app/taskmanager/Utils$THEME;

    move-result-object v1

    sget-object v4, Lcom/sec/android/app/taskmanager/Utils$THEME;->LIGHT:Lcom/sec/android/app/taskmanager/Utils$THEME;

    if-ne v1, v4, :cond_1

    .line 101
    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->setTheme(I)V

    .line 105
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->setUiOptions(I)V

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 116
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 120
    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->setContentView(I)V

    .line 121
    invoke-virtual {p0, v7}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->getSidePanelColor(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 122
    invoke-virtual {p0, v8}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->getSidePanelColor(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 126
    :goto_1
    invoke-static {}, Lcom/sec/android/app/taskmanager/PackageInfo;->getInstance()Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/taskmanager/PackageInfo;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPkgInfo:Lcom/sec/android/app/taskmanager/PackageInfo;

    .line 127
    invoke-static {}, Lcom/sec/android/app/taskmanager/MemoryManager;->getInstance()Lcom/sec/android/app/taskmanager/MemoryManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/taskmanager/MemoryManager;->init(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/MemoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryManager:Lcom/sec/android/app/taskmanager/MemoryManager;

    .line 128
    invoke-static {}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->getInstance()Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    .line 129
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;-><init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageMonitor:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;

    .line 130
    const v1, 0x7f0c0006

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemoryAvailable:Landroid/widget/TextView;

    .line 131
    const v1, 0x7f0c0007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMemGraph:Landroid/widget/ProgressBar;

    .line 132
    const v1, 0x7f0c0008

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mActiveStatus:Landroid/widget/TextView;

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mActiveStatus:Landroid/widget/TextView;

    const v4, 0x7f080006

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 134
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 135
    const v1, 0x7f0c0009

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mEndAllButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :goto_2
    new-instance v1, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListButtonItemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListButtonItemTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 147
    const v1, 0x102000a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 150
    .local v6, "resources":Landroid/content/res/Resources;
    const v1, 0x7f070005

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    const v4, 0x7f07000c

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int v2, v1, v4

    .line 151
    .local v2, "divider_inset_size":I
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 152
    .local v0, "insetdivider":Landroid/graphics/drawable/InsetDrawable;
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mOnListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 159
    invoke-virtual {p0, v7}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mLeftPanel:Landroid/view/View;

    .line 160
    invoke-virtual {p0, v8}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mRightPanel:Landroid/view/View;

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateLayout(Landroid/content/res/Configuration;)V

    .line 162
    return-void

    .line 103
    .end local v0    # "insetdivider":Landroid/graphics/drawable/InsetDrawable;
    .end local v2    # "divider_inset_size":I
    .end local v6    # "resources":Landroid/content/res/Resources;
    :cond_1
    const v1, 0x7f0a0001

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 124
    :cond_2
    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->setContentView(I)V

    goto/16 :goto_1

    .line 139
    :cond_3
    const v1, 0x7f0c000b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAll:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mEndAllButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    const v1, 0x7f0c000a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAllPanel:Landroid/widget/LinearLayout;

    goto/16 :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 205
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 208
    const v0, 0x7f0c000f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    .line 211
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isEnableUnusedAppsControl(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    const v0, 0x7f0c0010

    const v1, 0x7f08001a

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->updateLayout(Landroid/content/res/Configuration;)V

    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 224
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 232
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 226
    :pswitch_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->displayDialog(ILcom/sec/android/app/taskmanager/PackageInfoItem;)V

    goto :goto_0

    .line 229
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x7f0c000f
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 181
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->releaseResource()V

    .line 183
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 184
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 220
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 173
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageMonitor:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->start()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mObserver:Lcom/sec/android/app/taskmanager/monitor/Observer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->start(Lcom/sec/android/app/taskmanager/monitor/Observer;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 178
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 165
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 167
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isEnableUnusedAppsControl(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    :cond_0
    return-void
.end method

.method releaseResource()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageMonitor:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$PackageMonitor;->stop()V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mCpuMonitor:Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;

    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mObserver:Lcom/sec/android/app/taskmanager/monitor/Observer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/taskmanager/monitor/CpuMonitor;->stop(Lcom/sec/android/app/taskmanager/monitor/Observer;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKill:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 192
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "mDialogKill was dismissed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mDialogKillAll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 198
    const-string v0, "TaskManager:TaskManagerActivity"

    const-string v1, "mDialogKillAll was dismissed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_1
    return-void
.end method

.method updateLayout(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 526
    if-eqz p1, :cond_0

    .line 527
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    .line 528
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    if-nez v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 529
    iput-boolean v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    .line 532
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/taskmanager/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 533
    iget-boolean v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/taskmanager/Utils;->getScafeSize()Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/taskmanager/Utils$ScafeSize;->TALL:Lcom/sec/android/app/taskmanager/Utils$ScafeSize;

    if-ne v0, v1, :cond_4

    .line 534
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mLeftPanel:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mRightPanel:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 548
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 527
    goto :goto_0

    .line 537
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mLeftPanel:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mRightPanel:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 541
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    if-eqz v0, :cond_6

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mMenuItemEndall:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 544
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAllPanel:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mKillAllPanel:Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mIsLandscape:Z

    if-eqz v1, :cond_7

    move v2, v3

    :cond_7
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method
