.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;
.super Ljava/lang/Object;
.source "UnusedAppsListActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    .line 601
    const-string v4, "UnusedAppsListActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onTouch ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " class:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 606
    .local v1, "id":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 627
    :goto_0
    :pswitch_0
    return v7

    .line 608
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setButtonPressed(Z)V

    goto :goto_0

    .line 611
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setButtonPressed(Z)V

    .line 612
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/taskmanager/PackageInfoItem;

    .line 613
    .local v0, "appInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 614
    .local v2, "packageURI":Landroid/net/Uri;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.DELETE"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 615
    .local v3, "uninstallIntent":Landroid/content/Intent;
    const/high16 v4, 0x10800000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 620
    .end local v0    # "appInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    .end local v2    # "packageURI":Landroid/net/Uri;
    .end local v3    # "uninstallIntent":Landroid/content/Intent;
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$12;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v4}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->setButtonPressed(Z)V

    .line 621
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 606
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
