.class Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;
.super Landroid/os/Handler;
.source "UnusedAppsListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 641
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 694
    :goto_0
    return-void

    .line 643
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    new-instance v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$1;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 658
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    new-instance v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13$2;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 671
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$600(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 672
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$600(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->cancel(Z)Z

    .line 675
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 676
    .local v0, "cloneObject":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsPackageInfoAdapter;->getBaseAppList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    new-instance v2, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    iget-object v3, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;-><init>(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Ljava/util/List;)V

    # setter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    invoke-static {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$602(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->loadPackageResource:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;
    invoke-static {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->access$600(Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;)Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$LoadPackageResource;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 682
    .end local v0    # "cloneObject":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/taskmanager/PackageInfoItem;>;"
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/Utils;->isLocaleChange(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 683
    const-string v1, "UnusedAppsListActivity"

    const-string v2, "Locale Changed!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->reloadAppName()V

    .line 685
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->clearCache()V

    goto :goto_0

    .line 687
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity$13;->this$0:Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/activity/UnusedAppsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/taskmanager/PackageInfoItemFactory;->loadCache()V

    goto/16 :goto_0

    .line 641
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
