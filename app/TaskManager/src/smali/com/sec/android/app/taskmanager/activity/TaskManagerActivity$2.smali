.class Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;
.super Ljava/lang/Object;
.source "TaskManagerActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    # getter for: Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->mPackageInfoAdapter:Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;
    invoke-static {v5}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->access$000(Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;)Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/sec/android/app/taskmanager/activity/RunningAppsPackageInfoAdapter;->getItem(I)Lcom/sec/android/app/taskmanager/PackageInfoItem;

    move-result-object v3

    .line 257
    .local v3, "packInfo":Lcom/sec/android/app/taskmanager/PackageInfoItem;
    if-nez v3, :cond_0

    .line 258
    const-string v5, "TaskManager:TaskManagerActivity"

    const-string v6, "onItemClick() : packInfo is NULL"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :goto_0
    return-void

    .line 261
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 262
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getRecentTaskId()I

    move-result v4

    .line 263
    .local v4, "recentTaskId":I
    if-ltz v4, :cond_1

    .line 264
    const-string v5, "TaskManager:TaskManagerActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "call moveTaskToFront() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 266
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/ActivityManager;->moveTaskToFront(II)V

    goto :goto_0

    .line 267
    .end local v0    # "am":Landroid/app/ActivityManager;
    :cond_1
    if-eqz v2, :cond_2

    .line 268
    const-string v5, "TaskManager:TaskManagerActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "call startActivity() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/sec/android/app/taskmanager/PackageInfoItem;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const/high16 v5, 0x20100000

    :try_start_0
    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 272
    iget-object v5, p0, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity$2;->this$0:Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/taskmanager/activity/TaskManagerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 273
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v5, "TaskManager:TaskManagerActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onItemClick() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 276
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 277
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "TaskManager:TaskManagerActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onItemClick() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 282
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v5, "TaskManager:TaskManagerActivity"

    const-string v6, "Can\'t start intent cause it\'s null!!"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
