.class public Lcom/dsi/ant/AntSupportChecker;
.super Ljava/lang/Object;
.source "AntSupportChecker.java"


# static fields
.field private static final ANT_LIBRARY_NAME:Ljava/lang/String; = "com.dsi.ant.antradio_library"

.field private static final INTENT_ACTION_QUERY_SERVICE_INFO:Ljava/lang/String; = "com.dsi.ant.intent.request.SERVICE_INFO"

.field private static final META_DATA_ANT_CHIP_SERVICE_INTERFACE_TYPE_REMOTE:Ljava/lang/String; = "remote"

.field private static final META_DATA_ANT_CHIP_SERVICE_INTERFACE_TYPE_TAG:Ljava/lang/String; = "ANT_AdapterType"

.field private static final META_DATA_ANT_HARDWARE_TYPES_TAG:Ljava/lang/String; = "ANT_HardwareType"

.field private static final META_DATA_ANT_HARDWARE_TYPE_BUILTIN:Ljava/lang/String; = "built-in"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasAntAddOn(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 185
    .local v0, "antAddonDetected":Z
    invoke-static {p0}, Lcom/dsi/ant/AntSupportChecker;->queryForAntServices(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 187
    .local v3, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 190
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    :try_start_0
    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-static {v4}, Lcom/dsi/ant/AntSupportChecker;->isBuiltIn(Landroid/content/pm/ServiceInfo;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 205
    .end local v2    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    return v0

    .line 200
    .restart local v2    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static hasAntFeature(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 138
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 141
    .local v0, "antSupported":Z
    invoke-virtual {v2}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const-string v6, "com.dsi.ant.antradio_library"

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 144
    if-nez v0, :cond_1

    .line 150
    invoke-static {p0}, Lcom/dsi/ant/AntSupportChecker;->queryForAntServices(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 152
    .local v4, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 154
    .local v3, "resolveInfo":Landroid/content/pm/ResolveInfo;
    :try_start_0
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-static {v5}, Lcom/dsi/ant/AntSupportChecker;->isBuiltIn(Landroid/content/pm/ServiceInfo;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 166
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v4    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_1
    return v0

    .line 160
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .restart local v4    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private static isBuiltIn(Landroid/content/pm/ServiceInfo;)Z
    .locals 5
    .param p0, "serviceInterfaceInfo"    # Landroid/content/pm/ServiceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 91
    .local v0, "builtInDetected":Z
    if-eqz p0, :cond_2

    iget-object v3, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    .line 92
    iget-object v3, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "ANT_AdapterType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "serviceInterfaceType":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 97
    const-string v3, "remote"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    iget-object v3, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "ANT_HardwareType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "hardwareTypes":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "built-in"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    const/4 v0, 0x1

    .line 120
    .end local v1    # "hardwareTypes":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 117
    .end local v2    # "serviceInterfaceType":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "No meta data"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private static queryForAntServices(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.dsi.ant.intent.request.SERVICE_INFO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
