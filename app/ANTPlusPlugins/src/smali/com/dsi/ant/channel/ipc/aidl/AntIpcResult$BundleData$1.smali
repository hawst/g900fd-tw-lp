.class final Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData$1;
.super Ljava/lang/Object;
.source "AntIpcResult.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 129
    .local v1, "version":I
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$1;)V

    .line 131
    .local v0, "bundleData":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBundleReturned:Landroid/os/Bundle;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->createBinderArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v0, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;->mBindersReturned:Ljava/util/ArrayList;

    .line 134
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 138
    :cond_0
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 143
    new-array v0, p1, [Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData$1;->newArray(I)[Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult$BundleData;

    move-result-object v0

    return-object v0
.end method
