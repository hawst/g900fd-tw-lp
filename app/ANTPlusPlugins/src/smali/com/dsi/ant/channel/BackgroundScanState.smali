.class public Lcom/dsi/ant/channel/BackgroundScanState;
.super Ljava/lang/Object;
.source "BackgroundScanState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;,
        Lcom/dsi/ant/channel/BackgroundScanState$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/BackgroundScanState;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_BUNDLE_DATA:Ljava/lang/String; = "com.dsi.ant.channel.backgroundscanstate.bundledata"

.field private static final PARCEL_VERSION_INITIAL:I = 0x1

.field private static final PARCEL_VERSION_WITH_BUNDLE:I = 0x2


# instance fields
.field private mBundleData:Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

.field private mConfigured:Z

.field private mInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BackgroundScanState$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/BackgroundScanState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, v0, v0}, Lcom/dsi/ant/channel/BackgroundScanState;-><init>(ZZ)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v1, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    invoke-direct {v1}, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mBundleData:Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 171
    .local v0, "version":I
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState;->createFromParcelVersionInitial(Landroid/os/Parcel;)V

    .line 173
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 174
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState;->createFromParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 176
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dsi/ant/channel/BackgroundScanState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/dsi/ant/channel/BackgroundScanState$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(ZZ)V
    .locals 1
    .param p1, "configured"    # Z
    .param p2, "inProgress"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mBundleData:Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    .line 37
    iput-boolean p1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    .line 38
    iput-boolean p2, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    .line 39
    return-void
.end method

.method private createFromParcelVersionInitial(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 181
    .local v1, "detailsArraySize":I
    invoke-static {}, Lcom/dsi/ant/channel/BackgroundScanState;->getBackgroundScanStateArraySize()I

    move-result v3

    if-le v1, v3, :cond_0

    move v2, v1

    .line 184
    .local v2, "requiredArraySize":I
    :goto_0
    new-array v0, v2, [Z

    .line 185
    .local v0, "detailsArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 187
    sget-object v3, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->CONFIGURED:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v0, v3

    iput-boolean v3, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    .line 188
    sget-object v3, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->IN_PROGRESS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v0, v3

    iput-boolean v3, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    .line 189
    return-void

    .line 181
    .end local v0    # "detailsArray":[Z
    .end local v2    # "requiredArraySize":I
    :cond_0
    invoke-static {}, Lcom/dsi/ant/channel/BackgroundScanState;->getBackgroundScanStateArraySize()I

    move-result v2

    goto :goto_0
.end method

.method private createFromParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 193
    .local v0, "received":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 194
    const-string v1, "com.dsi.ant.channel.backgroundscanstate.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mBundleData:Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    .line 195
    return-void
.end method

.method static getBackgroundScanStateArraySize()I
    .locals 1

    .prologue
    .line 245
    sget-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->ordinal()I

    move-result v0

    return v0
.end method

.method private writeToParcelVersionInitial(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 214
    invoke-static {}, Lcom/dsi/ant/channel/BackgroundScanState;->getBackgroundScanStateArraySize()I

    move-result v1

    new-array v0, v1, [Z

    .line 216
    .local v0, "detailsArray":[Z
    sget-object v1, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->CONFIGURED:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    aput-boolean v2, v0, v1

    .line 217
    sget-object v1, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->IN_PROGRESS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    aput-boolean v2, v0, v1

    .line 219
    invoke-static {}, Lcom/dsi/ant/channel/BackgroundScanState;->getBackgroundScanStateArraySize()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 221
    return-void
.end method

.method private writeToParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 224
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 225
    .local v0, "bundleVersion":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.backgroundscanstate.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mBundleData:Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 226
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 227
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p0, p1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 80
    goto :goto_0

    .line 83
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/channel/BackgroundScanState;

    if-nez v3, :cond_3

    move v1, v2

    .line 84
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 87
    check-cast v0, Lcom/dsi/ant/channel/BackgroundScanState;

    .line 89
    .local v0, "other":Lcom/dsi/ant/channel/BackgroundScanState;
    iget-boolean v3, v0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 91
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 63
    const/16 v0, 0x1f

    .line 64
    .local v0, "prime":I
    const/4 v1, 0x7

    .line 66
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit16 v1, v2, 0xd9

    .line 67
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    if-eqz v5, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 69
    return v1

    :cond_0
    move v2, v4

    .line 66
    goto :goto_0

    :cond_1
    move v3, v4

    .line 67
    goto :goto_1
.end method

.method public isConfigured()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    return v0
.end method

.method public isInProgress()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    return v0
.end method

.method setConfigured(Z)V
    .locals 0
    .param p1, "isConfigured"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    return-void
.end method

.method setInProgress(Z)V
    .locals 0
    .param p1, "isInProgress"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Background Scan State:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mInProgress:Z

    if-eqz v1, :cond_0

    .line 103
    const-string v1, " Scan In Progress"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 104
    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/channel/BackgroundScanState;->mConfigured:Z

    if-eqz v1, :cond_1

    .line 105
    const-string v1, " Channel Configured"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 107
    :cond_1
    const-string v1, " No Background Scan Channels"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 204
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState;->writeToParcelVersionInitial(Landroid/os/Parcel;)V

    .line 208
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState;->writeToParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 211
    :cond_0
    return-void
.end method
