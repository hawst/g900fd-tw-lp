.class final Lcom/dsi/ant/channel/BurstState$BundleData;
.super Ljava/lang/Object;
.source "BurstState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/BurstState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BundleData"
.end annotation


# static fields
.field private static final BUNDLE_PARCEL_VERSION_INITIAL:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/BurstState$BundleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mMaxBurstSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/dsi/ant/channel/BurstState$BundleData$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BurstState$BundleData$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/BurstState$BundleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const/16 v0, 0x2710

    iput v0, p0, Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/BurstState$BundleData;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/BurstState$BundleData;

    .prologue
    .line 137
    iget v0, p0, Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I

    return v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/channel/BurstState$BundleData;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/BurstState$BundleData;
    .param p1, "x1"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I

    return p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 152
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    iget v0, p0, Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    return-void
.end method
