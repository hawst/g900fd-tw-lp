.class final enum Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;
.super Ljava/lang/Enum;
.source "BackgroundScanState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/BackgroundScanState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BackgroundScanStateArrayIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

.field public static final enum CONFIGURED:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

.field public static final enum IN_PROGRESS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

.field public static final enum NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    const-string v1, "CONFIGURED"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->CONFIGURED:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    .line 165
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->IN_PROGRESS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    .line 166
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    const-string v1, "NUMBER_OF_DETAILS"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    .line 163
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    sget-object v1, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->CONFIGURED:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->IN_PROGRESS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 163
    const-class v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/BackgroundScanState$BackgroundScanStateArrayIndex;

    return-object v0
.end method
