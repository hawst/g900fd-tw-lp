.class final Lcom/dsi/ant/channel/NetworkKey$BundleData;
.super Ljava/lang/Object;
.source "NetworkKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/NetworkKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BundleData"
.end annotation


# static fields
.field private static final BUNDLE_PARCEL_VERSION_INITIAL:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/NetworkKey$BundleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public networkKey:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/dsi/ant/channel/NetworkKey$BundleData$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/NetworkKey$BundleData$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKey$BundleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/NetworkKey$BundleData;->networkKey:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/channel/NetworkKey$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/channel/NetworkKey$1;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/dsi/ant/channel/NetworkKey$BundleData;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 114
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkKey$BundleData;->networkKey:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 119
    return-void
.end method
