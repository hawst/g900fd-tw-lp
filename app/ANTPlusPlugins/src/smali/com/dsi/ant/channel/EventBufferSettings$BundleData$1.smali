.class final Lcom/dsi/ant/channel/EventBufferSettings$BundleData$1;
.super Ljava/lang/Object;
.source "EventBufferSettings.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/EventBufferSettings$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/EventBufferSettings$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/EventBufferSettings$BundleData;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 181
    .local v1, "version":I
    new-instance v0, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/dsi/ant/channel/EventBufferSettings$BundleData;-><init>(Lcom/dsi/ant/channel/EventBufferSettings$1;)V

    .line 183
    .local v0, "bundleData":Lcom/dsi/ant/channel/EventBufferSettings$BundleData;
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 187
    :cond_0
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/EventBufferSettings$BundleData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/EventBufferSettings$BundleData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/EventBufferSettings$BundleData$1;->newArray(I)[Lcom/dsi/ant/channel/EventBufferSettings$BundleData;

    move-result-object v0

    return-object v0
.end method
