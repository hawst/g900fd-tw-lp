.class final Lcom/dsi/ant/channel/Capabilities$BundleData$1;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/Capabilities$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities$BundleData;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 435
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 436
    .local v3, "version":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 438
    .local v2, "capabilitiesArraySize":I
    new-array v1, v2, [Z

    .line 439
    .local v1, "capabilitiesArray":[Z
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 442
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData;-><init>()V

    .line 449
    .local v0, "bundledData":Lcom/dsi/ant/channel/Capabilities$BundleData;
    sget-object v4, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v4

    aget-boolean v4, v1, v4

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z
    invoke-static {v0, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$002(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 451
    sget-object v4, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v4

    aget-boolean v4, v1, v4

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z
    invoke-static {v0, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$102(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 453
    sget-object v4, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v4

    aget-boolean v4, v1, v4

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z
    invoke-static {v0, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$202(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z

    .line 458
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I
    invoke-static {v0, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$302(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I

    .line 459
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    # setter for: Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I
    invoke-static {v0, v4}, Lcom/dsi/ant/channel/Capabilities$BundleData;->access$402(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I

    .line 461
    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 465
    :cond_0
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 431
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/Capabilities$BundleData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities$BundleData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/Capabilities$BundleData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 470
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 431
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/Capabilities$BundleData$1;->newArray(I)[Lcom/dsi/ant/channel/Capabilities$BundleData;

    move-result-object v0

    return-object v0
.end method
