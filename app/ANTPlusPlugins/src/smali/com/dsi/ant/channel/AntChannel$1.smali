.class final Lcom/dsi/ant/channel/AntChannel$1;
.super Ljava/lang/Object;
.source "AntChannel.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/AntChannel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/AntChannel;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 1035
    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    .line 1038
    .local v0, "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    new-instance v1, Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {v1, v0}, Lcom/dsi/ant/channel/AntChannel;-><init>(Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 1032
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/AntChannel$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/AntChannel;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 1043
    new-array v0, p1, [Lcom/dsi/ant/channel/AntChannel;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1032
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/AntChannel$1;->newArray(I)[Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method
