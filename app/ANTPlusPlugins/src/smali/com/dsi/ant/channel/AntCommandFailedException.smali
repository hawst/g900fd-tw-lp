.class public Lcom/dsi/ant/channel/AntCommandFailedException;
.super Ljava/lang/Exception;
.source "AntCommandFailedException.java"


# static fields
.field private static final serialVersionUID:J = -0x352b2f9b01257912L


# instance fields
.field private mAntCommandFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field private mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

.field private mAttemptedMessageType:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Lcom/dsi/ant/channel/ipc/ServiceResult;)V
    .locals 1
    .param p1, "attemptedMessageType"    # Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .param p2, "serviceResult"    # Lcom/dsi/ant/channel/ipc/ServiceResult;

    .prologue
    .line 32
    invoke-static {p2}, Lcom/dsi/ant/channel/AntCommandFailedException;->getResultDetailMessage(Lcom/dsi/ant/channel/ipc/ServiceResult;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntCommandFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 34
    iput-object p1, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAttemptedMessageType:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntCommandFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 38
    invoke-virtual {p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->getAntMessage()Lcom/dsi/ant/message/ipc/AntMessageParcel;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 40
    :cond_0
    return-void
.end method

.method private static getResultDetailMessage(Lcom/dsi/ant/channel/ipc/ServiceResult;)Ljava/lang/String;
    .locals 1
    .param p0, "serviceResult"    # Lcom/dsi/ant/channel/ipc/ServiceResult;

    .prologue
    .line 43
    if-nez p0, :cond_0

    .line 44
    const-string v0, "Null result"

    .line 46
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->getDetailMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getAntMessage()Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    return-object v0
.end method

.method public getAttemptedMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAttemptedMessageType:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntCommandFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-object v0
.end method

.method public getResponseMessage()Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iget-object v2, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntCommandFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_0

    .line 86
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .end local v0    # "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntCommandFailedException;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 89
    .restart local v0    # "responseMessage":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    :cond_0
    return-object v0
.end method
