.class public final Lcom/dsi/ant/channel/AntChannelProvider;
.super Ljava/lang/Object;
.source "AntChannelProvider.java"


# static fields
.field public static final ACTION_CHANNEL_PROVIDER_STATE_CHANGED:Ljava/lang/String; = "com.dsi.ant.intent.action.CHANNEL_PROVIDER_STATE_CHANGED"

.field public static final LEGACY_INTERFACE_IN_USE:Ljava/lang/String; = "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_LEGACY_INTERFACE_IN_USE"

.field public static final NEW_CHANNELS_AVAILABLE:Ljava/lang/String; = "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NEW_CHANNELS_AVAILABLE"

.field public static final NUM_CHANNELS_AVAILABLE:Ljava/lang/String; = "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NUM_CHANNELS_AVAILABLE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/dsi/ant/channel/AntChannelProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/AntChannelProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;

    invoke-direct {v0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    .line 94
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given service does not seem to be an ANT channel pool."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    return-void
.end method

.method private static voteEventBufferToDefault(Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p0, "antChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/dsi/ant/channel/AntChannel;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/channel/AntChannel;->setEventBufferToDefault()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "network"    # Lcom/dsi/ant/channel/PredefinedNetwork;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method public acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "network"    # Lcom/dsi/ant/channel/PredefinedNetwork;
    .param p3, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method public acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "network"    # Lcom/dsi/ant/channel/PredefinedNetwork;
    .param p3, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 190
    sget-object v0, Lcom/dsi/ant/channel/PredefinedNetwork;->INVALID:Lcom/dsi/ant/channel/PredefinedNetwork;

    if-ne v0, p2, :cond_0

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid predefined network requested"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 196
    .local v5, "error":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    invoke-virtual {p2}, Lcom/dsi/ant/channel/PredefinedNetwork;->getRawValue()I

    move-result v2

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;->acquireChannel(Landroid/content/Context;ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    move-result-object v6

    .line 200
    .local v6, "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    if-nez v6, :cond_1

    .line 201
    const-class v0, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 202
    const-string v0, "error"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    throw v0

    .line 205
    :cond_1
    new-instance v7, Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {v7, v6}, Lcom/dsi/ant/channel/AntChannel;-><init>(Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;)V

    .line 207
    .local v7, "newChannel":Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v7}, Lcom/dsi/ant/channel/AntChannelProvider;->voteEventBufferToDefault(Lcom/dsi/ant/channel/AntChannel;)V

    .line 209
    return-object v7
.end method

.method public acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "privateNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Lcom/dsi/ant/channel/UnsupportedFeatureException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 246
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method public acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "privateNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .param p3, "requiredCapapbilities"    # Lcom/dsi/ant/channel/Capabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Lcom/dsi/ant/channel/UnsupportedFeatureException;
        }
    .end annotation

    .prologue
    .line 285
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method public acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "privateNetworkKey"    # Lcom/dsi/ant/channel/NetworkKey;
    .param p3, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/ChannelNotAvailableException;,
            Lcom/dsi/ant/channel/UnsupportedFeatureException;
        }
    .end annotation

    .prologue
    .line 330
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 332
    .local v5, "error":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;->acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    move-result-object v6

    .line 336
    .local v6, "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    if-nez v6, :cond_0

    .line 337
    const-class v0, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 338
    const-string v0, "error"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ChannelNotAvailableException;

    throw v0

    .line 341
    :cond_0
    new-instance v7, Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {v7, v6}, Lcom/dsi/ant/channel/AntChannel;-><init>(Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;)V

    .line 343
    .local v7, "newChannel":Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v7}, Lcom/dsi/ant/channel/AntChannelProvider;->voteEventBufferToDefault(Lcom/dsi/ant/channel/AntChannel;)V

    .line 345
    return-object v7
.end method

.method public getNumChannelsAvailable()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 392
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannelProvider;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v0

    return v0
.end method

.method public getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I
    .locals 1
    .param p1, "caps"    # Lcom/dsi/ant/channel/Capabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 380
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v0

    return v0
.end method

.method public isLegacyInterfaceInUse()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 408
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannelProvider;->mAntChannelProvider:Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;->isLegacyInterfaceInUse()Z

    move-result v0

    return v0
.end method
