.class final enum Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;
.super Ljava/lang/Enum;
.source "AntChannelCommunicatorAidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AntIpcCommunicatorMessageWhat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum GET_BACKGROUND_SCAN_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum GET_BURST_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum GET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum GET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum SET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum SET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field private static final sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 474
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 476
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "SET_LIB_CONFIG"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 477
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "SET_EVENT_BUFFER_SETTINGS"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 479
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "GET_BURST_STATE"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BURST_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 480
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "GET_LIB_CONFIG"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 481
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "GET_BACKGROUND_SCAN_STATE"

    const/4 v2, 0x5

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BACKGROUND_SCAN_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 482
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "GET_EVENT_BUFFER_SETTINGS"

    const/4 v2, 0x6

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 473
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BURST_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BACKGROUND_SCAN_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 488
    invoke-static {}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    return-void
.end method

.method static create(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 497
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 499
    .local v0, "code":Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 500
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 501
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aget-object v0, v2, v1

    .line 506
    :cond_0
    return-object v0

    .line 499
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 494
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 473
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 1

    .prologue
    .line 473
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-object v0
.end method


# virtual methods
.method getRawValue()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    return v0
.end method
