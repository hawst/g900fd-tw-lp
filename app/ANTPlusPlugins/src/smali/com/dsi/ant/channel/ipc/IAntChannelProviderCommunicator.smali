.class public interface abstract Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;
.super Ljava/lang/Object;
.source "IAntChannelProviderCommunicator.java"


# virtual methods
.method public abstract acquireChannel(Landroid/content/Context;ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract acquireChannelOnPrivateNetwork(Landroid/content/Context;Lcom/dsi/ant/channel/NetworkKey;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/UnsupportedFeatureException;
        }
    .end annotation
.end method

.method public abstract getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isLegacyInterfaceInUse()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
