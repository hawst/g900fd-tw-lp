.class public abstract Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;
.super Landroid/os/Binder;
.source "IAntChannelAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

.field static final TRANSACTION_acknowledgedTransfer:I = 0x5

.field static final TRANSACTION_addDeathNotifier:I = 0x8

.field static final TRANSACTION_addEventReceiver:I = 0xa

.field static final TRANSACTION_burstTransfer:I = 0x6

.field static final TRANSACTION_cancelTransfer:I = 0x7

.field static final TRANSACTION_getCapabilities:I = 0xc

.field static final TRANSACTION_handleMessage:I = 0xe

.field static final TRANSACTION_releaseChannel:I = 0xd

.field static final TRANSACTION_removeDeathNotifier:I = 0x9

.field static final TRANSACTION_removeEventReceiver:I = 0xb

.field static final TRANSACTION_requestResponse:I = 0x2

.field static final TRANSACTION_setBroadcastData:I = 0x3

.field static final TRANSACTION_startAcknowledgedTransfer:I = 0x4

.field static final TRANSACTION_writeMessage:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p0, p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 286
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 42
    :sswitch_0
    const-string v3, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    .line 50
    sget-object v5, Lcom/dsi/ant/message/ipc/AntMessageParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 56
    .local v0, "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v1, "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    if-eqz v1, :cond_1

    .line 60
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    goto :goto_1

    .line 64
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 70
    .end local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_2
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    .line 73
    sget-object v5, Lcom/dsi/ant/message/ipc/AntMessageParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 79
    .restart local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :goto_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 80
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->requestResponse(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)Lcom/dsi/ant/message/ipc/AntMessageParcel;

    move-result-object v2

    .line 81
    .local v2, "_result":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    if-eqz v2, :cond_3

    .line 83
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    invoke-virtual {v2, p3, v4}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 89
    :goto_3
    if-eqz v1, :cond_4

    .line 90
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v2    # "_result":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    goto :goto_2

    .line 87
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    .restart local v2    # "_result":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :cond_3
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 94
    :cond_4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 100
    .end local v0    # "_arg0":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v2    # "_result":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :sswitch_3
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 104
    .local v0, "_arg0":[B
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 105
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->setBroadcastData([BLandroid/os/Bundle;)V

    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v1, :cond_5

    .line 108
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 112
    :cond_5
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 118
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_4
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 122
    .restart local v0    # "_arg0":[B
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 123
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->startAcknowledgedTransfer([BLandroid/os/Bundle;)V

    .line 124
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 125
    if-eqz v1, :cond_6

    .line 126
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 130
    :cond_6
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 136
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_5
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 140
    .restart local v0    # "_arg0":[B
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 141
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->acknowledgedTransfer([BLandroid/os/Bundle;)V

    .line 142
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 143
    if-eqz v1, :cond_7

    .line 144
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 145
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 148
    :cond_7
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 154
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_6
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 158
    .restart local v0    # "_arg0":[B
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 159
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->burstTransfer([BLandroid/os/Bundle;)V

    .line 160
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 161
    if-eqz v1, :cond_8

    .line 162
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 166
    :cond_8
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 172
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_7
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v0, "_arg0":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->cancelTransfer(Landroid/os/Bundle;)V

    .line 176
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    if-eqz v0, :cond_9

    .line 178
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    invoke-virtual {v0, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 182
    :cond_9
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_8
    const-string v3, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 191
    .local v0, "_arg0":Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->addDeathNotifier(Landroid/os/IBinder;)V

    .line 192
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 197
    .end local v0    # "_arg0":Landroid/os/IBinder;
    :sswitch_9
    const-string v3, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 200
    .restart local v0    # "_arg0":Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->removeDeathNotifier(Landroid/os/IBinder;)V

    .line 201
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 206
    .end local v0    # "_arg0":Landroid/os/IBinder;
    :sswitch_a
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 208
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_b

    .line 209
    sget-object v5, Landroid/os/Messenger;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 214
    .local v0, "_arg0":Landroid/os/Messenger;
    :goto_4
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->addEventReceiver(Landroid/os/Messenger;)Z

    move-result v2

    .line 215
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 216
    if-eqz v2, :cond_a

    move v3, v4

    :cond_a
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 212
    .end local v0    # "_arg0":Landroid/os/Messenger;
    .end local v2    # "_result":Z
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Messenger;
    goto :goto_4

    .line 221
    .end local v0    # "_arg0":Landroid/os/Messenger;
    :sswitch_b
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_d

    .line 224
    sget-object v5, Landroid/os/Messenger;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 229
    .restart local v0    # "_arg0":Landroid/os/Messenger;
    :goto_5
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->removeEventReceiver(Landroid/os/Messenger;)Z

    move-result v2

    .line 230
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v2, :cond_c

    move v3, v4

    :cond_c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 227
    .end local v0    # "_arg0":Landroid/os/Messenger;
    .end local v2    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Messenger;
    goto :goto_5

    .line 236
    .end local v0    # "_arg0":Landroid/os/Messenger;
    :sswitch_c
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v2

    .line 238
    .local v2, "_result":Lcom/dsi/ant/channel/Capabilities;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 239
    if-eqz v2, :cond_e

    .line 240
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 241
    invoke-virtual {v2, p3, v4}, Lcom/dsi/ant/channel/Capabilities;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 244
    :cond_e
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 250
    .end local v2    # "_result":Lcom/dsi/ant/channel/Capabilities;
    :sswitch_d
    const-string v3, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->releaseChannel()V

    .line 252
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 257
    :sswitch_e
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 259
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_f

    .line 260
    sget-object v5, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 266
    .local v0, "_arg0":Landroid/os/Message;
    :goto_6
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 267
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 268
    .local v2, "_result":Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 269
    if-eqz v2, :cond_10

    .line 270
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 271
    invoke-virtual {v2, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 276
    :goto_7
    if-eqz v1, :cond_11

    .line 277
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 263
    .end local v0    # "_arg0":Landroid/os/Message;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v2    # "_result":Landroid/os/Bundle;
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Message;
    goto :goto_6

    .line 274
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    .restart local v2    # "_result":Landroid/os/Bundle;
    :cond_10
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_7

    .line 281
    :cond_11
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
