.class final enum Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
.super Ljava/lang/Enum;
.source "AntChannelCommunicatorAidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AntIpcReceiverMessageWhat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum BACKGROUND_SCAN_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum BURST_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum CHANNEL_DEATH:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum EVENT_BUFFER_SETTINGS_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum LIB_CONFIG_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum RX_ANT_MESSAGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

.field private static final sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 267
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 272
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "RX_ANT_MESSAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->RX_ANT_MESSAGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 275
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "CHANNEL_DEATH"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->CHANNEL_DEATH:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 280
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "BURST_STATE_CHANGE"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BURST_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 282
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "LIB_CONFIG_CHANGE"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->LIB_CONFIG_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 284
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "BACKGROUND_SCAN_STATE_CHANGE"

    const/4 v2, 0x5

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BACKGROUND_SCAN_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 286
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    const-string v1, "EVENT_BUFFER_SETTINGS_CHANGE"

    const/4 v2, 0x6

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->EVENT_BUFFER_SETTINGS_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 266
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->RX_ANT_MESSAGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->CHANNEL_DEATH:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BURST_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->LIB_CONFIG_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->BACKGROUND_SCAN_STATE_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->EVENT_BUFFER_SETTINGS_CHANGE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 292
    invoke-static {}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->mRawValue:I

    return-void
.end method

.method static create(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 301
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    .line 303
    .local v0, "code":Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 304
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 305
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    aget-object v0, v2, v1

    .line 310
    :cond_0
    return-object v0

    .line 303
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 298
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 266
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;
    .locals 1

    .prologue
    .line 266
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;

    return-object v0
.end method


# virtual methods
.method getRawValue()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;->mRawValue:I

    return v0
.end method
