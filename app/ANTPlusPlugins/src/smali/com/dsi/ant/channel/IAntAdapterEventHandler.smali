.class public interface abstract Lcom/dsi/ant/channel/IAntAdapterEventHandler;
.super Ljava/lang/Object;
.source "IAntAdapterEventHandler.java"


# virtual methods
.method public abstract onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V
.end method

.method public abstract onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V
.end method

.method public abstract onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V
.end method

.method public abstract onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V
.end method
