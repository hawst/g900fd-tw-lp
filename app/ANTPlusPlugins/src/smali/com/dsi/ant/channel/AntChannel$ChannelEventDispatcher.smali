.class final Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;
.super Ljava/lang/Object;
.source "AntChannel.java"

# interfaces
.implements Lcom/dsi/ant/channel/IAntChannelEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/AntChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ChannelEventDispatcher"
.end annotation


# instance fields
.field private mReceivedChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

.field final synthetic this$0:Lcom/dsi/ant/channel/AntChannel;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/channel/AntChannel;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->this$0:Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/channel/AntChannel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "x1"    # Lcom/dsi/ant/channel/AntChannel$1;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;-><init>(Lcom/dsi/ant/channel/AntChannel;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;
    .param p1, "x1"    # Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->setReceivedChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    return-void
.end method

.method private setReceivedChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    .locals 0
    .param p1, "receivedHandler"    # Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->mReceivedChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 137
    return-void
.end method


# virtual methods
.method public onChannelDeath()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->this$0:Lcom/dsi/ant/channel/AntChannel;

    # getter for: Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z
    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannel;->access$100(Lcom/dsi/ant/channel/AntChannel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->mReceivedChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 151
    :cond_0
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->this$0:Lcom/dsi/ant/channel/AntChannel;

    # getter for: Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z
    invoke-static {v0}, Lcom/dsi/ant/channel/AntChannel;->access$100(Lcom/dsi/ant/channel/AntChannel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->mReceivedChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 144
    :cond_0
    return-void
.end method
