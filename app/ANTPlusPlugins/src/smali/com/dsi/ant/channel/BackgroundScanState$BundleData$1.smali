.class final Lcom/dsi/ant/channel/BackgroundScanState$BundleData$1;
.super Ljava/lang/Object;
.source "BackgroundScanState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/BackgroundScanState$BundleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/BackgroundScanState$BundleData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/BackgroundScanState$BundleData;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 142
    .local v1, "version":I
    new-instance v0, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BackgroundScanState$BundleData;-><init>()V

    .line 144
    .local v0, "bundleData":Lcom/dsi/ant/channel/BackgroundScanState$BundleData;
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 148
    :cond_0
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState$BundleData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/BackgroundScanState$BundleData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/BackgroundScanState$BundleData$1;->newArray(I)[Lcom/dsi/ant/channel/BackgroundScanState$BundleData;

    move-result-object v0

    return-object v0
.end method
