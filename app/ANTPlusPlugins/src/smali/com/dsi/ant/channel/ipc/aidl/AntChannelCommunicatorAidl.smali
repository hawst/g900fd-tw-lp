.class public Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.super Ljava/lang/Object;
.source "AntChannelCommunicatorAidl.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$2;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcReceiverMessageWhat;,
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;",
            ">;"
        }
    .end annotation
.end field

.field static final KEY_ANT_MESSAGE_PARCEL:Ljava/lang/String; = "com.dsi.ant.channel.data.antmessageparcel"

.field static final KEY_BACKGROUND_SCAN_STATE:Ljava/lang/String; = "com.dsi.ant.channel.data.backgroundscanstate"

.field static final KEY_BURST_STATE:Ljava/lang/String; = "com.dsi.ant.channel.data.burststate"

.field static final KEY_EVENT_BUFFER_SETTINGS:Ljava/lang/String; = "com.dsi.ant.channel.data.eventbuffersettings"

.field static final KEY_LIB_CONFIG:Ljava/lang/String; = "com.dsi.ant.channel.data.libconfig"

.field private static final PARCEL_VERSION_INITIAL:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

.field final mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

.field private mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

.field mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

.field final mChannelEventHandlerChange_Lock:Ljava/lang/Object;

.field private mEventHandlerThread_Lock:Ljava/lang/Object;

.field private mEventReceiverMessenger:Landroid/os/Messenger;

.field private mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

.field private mReceiveThread:Landroid/os/HandlerThread;

.field private final mReferenceToken:Landroid/os/Binder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    .line 757
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;)V
    .locals 1
    .param p1, "acquiredChannelAidl"    # Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .prologue
    .line 544
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;-><init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;Z)V

    .line 545
    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;Z)V
    .locals 2
    .param p1, "acquiredChannelAidl"    # Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    .param p2, "notifyAntServiceOnDeath"    # Z

    .prologue
    const/4 v1, 0x0

    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    .line 357
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    .line 359
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    .line 364
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 369
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 623
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .line 514
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    .line 516
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->setupIpcEventReceiverThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 522
    :cond_0
    if-eqz p2, :cond_1

    .line 523
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReferenceToken:Landroid/os/Binder;

    .line 525
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReferenceToken:Landroid/os/Binder;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->addDeathNotifier(Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :goto_0
    return-void

    .line 532
    :cond_1
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReferenceToken:Landroid/os/Binder;

    goto :goto_0

    .line 526
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BurstState;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/BurstState;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/message/LibConfig;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/BackgroundScanState;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/BackgroundScanState;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Lcom/dsi/ant/channel/EventBufferSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .param p1, "x1"    # Lcom/dsi/ant/channel/EventBufferSettings;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V

    return-void
.end method

.method private onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V
    .locals 2
    .param p1, "newBackgroundScanState"    # Lcom/dsi/ant/channel/BackgroundScanState;

    .prologue
    .line 255
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBackgroundScanStateChange(Lcom/dsi/ant/channel/BackgroundScanState;)V

    .line 262
    :cond_0
    monitor-exit v1

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V
    .locals 2
    .param p1, "newBurstState"    # Lcom/dsi/ant/channel/BurstState;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onBurstStateChange(Lcom/dsi/ant/channel/BurstState;)V

    .line 240
    :cond_0
    monitor-exit v1

    .line 241
    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V
    .locals 2
    .param p1, "newEventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;

    .prologue
    .line 222
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onEventBufferSettingsChange(Lcom/dsi/ant/channel/EventBufferSettings;)V

    .line 229
    :cond_0
    monitor-exit v1

    .line 230
    return-void

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V
    .locals 2
    .param p1, "newLibConfig"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 244
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/IAntAdapterEventHandler;->onLibConfigChange(Lcom/dsi/ant/message/LibConfig;)V

    .line 251
    :cond_0
    monitor-exit v1

    .line 252
    return-void

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setupIpcEventReceiverThread()Z
    .locals 6

    .prologue
    .line 390
    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    monitor-enter v3

    .line 391
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    monitor-exit v3

    .line 413
    :goto_0
    return v0

    .line 393
    :cond_0
    new-instance v2, Landroid/os/HandlerThread;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Receive thread"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 394
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 396
    new-instance v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;-><init>(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    const/4 v0, 0x0

    .line 401
    .local v0, "addSuccess":Z
    :try_start_1
    new-instance v2, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    invoke-direct {v2, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 402
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    iget-object v4, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    invoke-interface {v2, v4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->addEventReceiver(Landroid/os/Messenger;)Z

    move-result v0

    .line 404
    if-nez v0, :cond_1

    .line 405
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->teardownIpcEventReceiverThread()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    .line 414
    .end local v0    # "addSuccess":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 407
    .restart local v0    # "addSuccess":Z
    :catch_0
    move-exception v1

    .line 409
    .local v1, "remoteException":Landroid/os/RemoteException;
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 410
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    const-string v4, "Could not setup IPC Event receiver with remote service."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private teardownIpcEventReceiverThread()V
    .locals 4

    .prologue
    .line 422
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventHandlerThread_Lock:Ljava/lang/Object;

    monitor-enter v2

    .line 423
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    if-nez v1, :cond_0

    monitor-exit v2

    .line 441
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 426
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mReceiveThread:Landroid/os/HandlerThread;

    .line 428
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;

    # invokes: Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->destroy()V
    invoke-static {v1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;->access$400(Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;)V

    .line 429
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAntIpcEventReceiver:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcEventReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    iget-object v3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    invoke-interface {v1, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->removeEventReceiver(Landroid/os/Messenger;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    :goto_1
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mEventReceiverMessenger:Landroid/os/Messenger;

    .line 440
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 433
    :catch_0
    move-exception v0

    .line 437
    .local v0, "remoteException":Landroid/os/RemoteException;
    :try_start_3
    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->TAG:Ljava/lang/String;

    const-string v3, "Could not remove IPC Event receiver with remote service."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "message"    # Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 634
    new-instance v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-direct {v0, p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-virtual {p0, v0, p2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 635
    return-void
.end method


# virtual methods
.method public addChannelId(Lcom/dsi/ant/message/ChannelId;ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "index"    # I
    .param p3, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 670
    new-instance v0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;

    invoke-direct {v0, p1, p2}, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;-><init>(Lcom/dsi/ant/message/ChannelId;I)V

    invoke-direct {p0, v0, p3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->writeMessage(Lcom/dsi/ant/message/fromhost/AntMessageFromHost;Landroid/os/Bundle;)V

    .line 671
    return-void
.end method

.method public addDeathNotifier(Landroid/os/IBinder;)V
    .locals 1
    .param p1, "ref"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 639
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->addDeathNotifier(Landroid/os/IBinder;)V

    .line 640
    return-void
.end method

.method public assign(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "channelType"    # Lcom/dsi/ant/message/ChannelType;
    .param p2, "extendedAssignment"    # Lcom/dsi/ant/message/ExtendedAssignment;
    .param p3, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 653
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;

    invoke-direct {v2, p1, p2}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;-><init>(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 654
    return-void
.end method

.method public burstTransfer([BLandroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 725
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->burstTransfer([BLandroid/os/Bundle;)V

    .line 726
    return-void
.end method

.method public clearAdapterEventHandler()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 466
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 467
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 468
    monitor-exit v1

    .line 469
    return-void

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearChannelEventHandler()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 452
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 453
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 454
    monitor-exit v1

    .line 455
    return-void

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 707
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;

    invoke-direct {v2}, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;-><init>()V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 708
    return-void
.end method

.method public configIdList(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "listSize"    # I
    .param p2, "exclude"    # Z
    .param p3, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 674
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;

    invoke-direct {v2, p1, p2}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;-><init>(IZ)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 675
    return-void
.end method

.method public configureFrequencyAgility(IIILandroid/os/Bundle;)V
    .locals 3
    .param p1, "freq1"    # I
    .param p2, "freq2"    # I
    .param p3, "freq3"    # I
    .param p4, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 690
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;

    invoke-direct {v2, p1, p2, p3}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;-><init>(III)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 691
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 747
    const/4 v0, 0x0

    return v0
.end method

.method public getBackgroundScanState()Lcom/dsi/ant/channel/BackgroundScanState;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 612
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 614
    .local v0, "message":Landroid/os/Message;
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BACKGROUND_SCAN_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v0, Landroid/os/Message;->what:I

    .line 616
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v2, v0, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 617
    .local v1, "result":Landroid/os/Bundle;
    const-class v2, Lcom/dsi/ant/channel/BackgroundScanState;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 619
    const-string v2, "com.dsi.ant.channel.data.backgroundscanstate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/BackgroundScanState;

    return-object v2
.end method

.method public getBurstState()Lcom/dsi/ant/channel/BurstState;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 589
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 591
    .local v0, "message":Landroid/os/Message;
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_BURST_STATE:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v0, Landroid/os/Message;->what:I

    .line 593
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v2, v0, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 594
    .local v1, "result":Landroid/os/Bundle;
    const-class v2, Lcom/dsi/ant/channel/BurstState;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 596
    const-string v2, "com.dsi.ant.channel.data.burststate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/BurstState;

    return-object v2
.end method

.method public getCapabilities()Lcom/dsi/ant/channel/Capabilities;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 731
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getEventBufferSettings()Lcom/dsi/ant/channel/EventBufferSettings;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 577
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 579
    .local v0, "message":Landroid/os/Message;
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v0, Landroid/os/Message;->what:I

    .line 581
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v2, v0, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 582
    .local v1, "result":Landroid/os/Bundle;
    const-class v2, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 584
    const-string v2, "com.dsi.ant.channel.data.eventbuffersettings"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/EventBufferSettings;

    return-object v2
.end method

.method public getLibConfig()Lcom/dsi/ant/message/LibConfig;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 601
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 603
    .local v0, "message":Landroid/os/Message;
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->GET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v0, Landroid/os/Message;->what:I

    .line 605
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v2, v0, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 606
    .local v1, "result":Landroid/os/Bundle;
    const-class v2, Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 608
    const-string v2, "com.dsi.ant.channel.data.libconfig"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/message/LibConfig;

    return-object v2
.end method

.method public onChannelDeathMessage()V
    .locals 2

    .prologue
    .line 211
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onChannelDeath()V

    .line 216
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->teardownIpcEventReceiverThread()V

    .line 218
    :cond_0
    monitor-exit v1

    .line 219
    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onRxAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 203
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/IAntChannelEventHandler;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 207
    :cond_0
    monitor-exit v1

    .line 208
    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public open(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 703
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;

    invoke-direct {v2}, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;-><init>()V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 704
    return-void
.end method

.method public releaseChannel()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 736
    invoke-direct {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->teardownIpcEventReceiverThread()V

    .line 738
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->releaseChannel()V

    .line 739
    return-void
.end method

.method public removeDeathNotifier(Landroid/os/IBinder;)V
    .locals 1
    .param p1, "ref"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 642
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->removeDeathNotifier(Landroid/os/IBinder;)V

    .line 643
    return-void
.end method

.method public requestMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Landroid/os/Bundle;)Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .locals 3
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 712
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/RequestMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/RequestMessage;-><init>(Lcom/dsi/ant/message/fromant/MessageFromAntType;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->requestResponse(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)Lcom/dsi/ant/message/ipc/AntMessageParcel;

    move-result-object v0

    return-object v0
.end method

.method public setAdapterEventHandler(Lcom/dsi/ant/channel/IAntAdapterEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/dsi/ant/channel/IAntAdapterEventHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 459
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 460
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mAdapterEventHandler:Lcom/dsi/ant/channel/IAntAdapterEventHandler;

    .line 461
    monitor-exit v1

    .line 462
    return-void

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setBroadcastData([BLandroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 717
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->setBroadcastData([BLandroid/os/Bundle;)V

    .line 718
    return-void
.end method

.method public setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/dsi/ant/channel/IAntChannelEventHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 445
    iget-object v1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandlerChange_Lock:Ljava/lang/Object;

    monitor-enter v1

    .line 446
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mChannelEventHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;

    .line 447
    monitor-exit v1

    .line 448
    return-void

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setChannelTransmitPower(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "txPower"    # I
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 698
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;-><init>(ILcom/dsi/ant/channel/Capabilities;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 699
    return-void
.end method

.method public setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "eventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 563
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 566
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    const-class v2, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 567
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "com.dsi.ant.channel.data.eventbuffersettings"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 570
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 572
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v2, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 573
    return-void
.end method

.method public setHighPrioritySearchTimeout(Lcom/dsi/ant/message/HighPrioritySearchTimeout;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "timeout"    # Lcom/dsi/ant/message/HighPrioritySearchTimeout;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 682
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;-><init>(Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 683
    return-void
.end method

.method public setId(Lcom/dsi/ant/message/ChannelId;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 658
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;-><init>(Lcom/dsi/ant/message/ChannelId;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 659
    return-void
.end method

.method public setIdWithSerialNumber(ZIILandroid/os/Bundle;)V
    .locals 3
    .param p1, "pair"    # Z
    .param p2, "deviceType"    # I
    .param p3, "transmissionType"    # I
    .param p4, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 686
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;

    invoke-direct {v2, p1, p2, p3}, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;-><init>(ZII)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 687
    return-void
.end method

.method public setLibConfig(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "libConfig"    # Lcom/dsi/ant/message/LibConfig;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 549
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 552
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    const-class v2, Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 553
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "com.dsi.ant.channel.data.libconfig"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 555
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->SET_LIB_CONFIG:Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$AntIpcCommunicatorMessageWhat;->getRawValue()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 556
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 558
    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v2, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 559
    return-void
.end method

.method public setLowPrioritySearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "timeout"    # Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 678
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;-><init>(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 679
    return-void
.end method

.method public setPeriod(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "period"    # I
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 662
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;-><init>(I)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 663
    return-void
.end method

.method public setProximityThreshold(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "threshold"    # I
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 694
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;-><init>(I)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 695
    return-void
.end method

.method public setRfFrequency(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "frequency"    # I
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 666
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;

    invoke-direct {v2, p1}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;-><init>(I)V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 667
    return-void
.end method

.method public startAcknowledgedTransfer([BLandroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 721
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->startAcknowledgedTransfer([BLandroid/os/Bundle;)V

    .line 722
    return-void
.end method

.method public unassign(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 649
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    new-instance v1, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    new-instance v2, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    invoke-direct {v2}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>()V

    invoke-direct {v1, v2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(Lcom/dsi/ant/message/AntMessage;)V

    invoke-interface {v0, v1, p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 650
    return-void
.end method

.method public writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "antMessageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .param p2, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->writeMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Landroid/os/Bundle;)V

    .line 631
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 752
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 754
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->mIAntChannelAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 755
    return-void
.end method
