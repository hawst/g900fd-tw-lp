.class public Lcom/dsi/ant/StoreDownloader;
.super Ljava/lang/Object;
.source "StoreDownloader.java"


# static fields
.field private static final PACKAGE_NAME_ANT_RADIO_SERVICE:Ljava/lang/String; = "com.dsi.ant.service.socket"

.field private static final PACKAGE_NAME_ANT_USB_SERVICE:Ljava/lang/String; = "com.dsi.ant.usbservice"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAntRadioService(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-string v0, "com.dsi.ant.service.socket"

    invoke-static {p0, v0}, Lcom/dsi/ant/StoreDownloader;->goToStore(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public static getAntUsbService(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const-string v0, "com.dsi.ant.usbservice"

    invoke-static {p0, v0}, Lcom/dsi/ant/StoreDownloader;->goToStore(Landroid/content/Context;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method private static goToStore(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "startStore":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "startStore":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "market://details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 38
    .restart local v0    # "startStore":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 40
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 41
    return-void
.end method
