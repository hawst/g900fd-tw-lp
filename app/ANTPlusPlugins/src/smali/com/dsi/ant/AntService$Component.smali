.class public final enum Lcom/dsi/ant/AntService$Component;
.super Ljava/lang/Enum;
.source "AntService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/AntService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Component"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/AntService$Component;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/AntService$Component;

.field public static final enum CHANNEL_PROVIDER:Lcom/dsi/ant/AntService$Component;

.field public static final enum INVALID:Lcom/dsi/ant/AntService$Component;

.field private static final sValues:[Lcom/dsi/ant/AntService$Component;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 53
    new-instance v0, Lcom/dsi/ant/AntService$Component;

    const-string v1, "INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/AntService$Component;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/AntService$Component;->INVALID:Lcom/dsi/ant/AntService$Component;

    .line 58
    new-instance v0, Lcom/dsi/ant/AntService$Component;

    const-string v1, "CHANNEL_PROVIDER"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/AntService$Component;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/AntService$Component;->CHANNEL_PROVIDER:Lcom/dsi/ant/AntService$Component;

    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/AntService$Component;

    sget-object v1, Lcom/dsi/ant/AntService$Component;->INVALID:Lcom/dsi/ant/AntService$Component;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/AntService$Component;->CHANNEL_PROVIDER:Lcom/dsi/ant/AntService$Component;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dsi/ant/AntService$Component;->$VALUES:[Lcom/dsi/ant/AntService$Component;

    .line 63
    invoke-static {}, Lcom/dsi/ant/AntService$Component;->values()[Lcom/dsi/ant/AntService$Component;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/AntService$Component;->sValues:[Lcom/dsi/ant/AntService$Component;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/AntService$Component;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/AntService$Component;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 85
    sget-object v0, Lcom/dsi/ant/AntService$Component;->INVALID:Lcom/dsi/ant/AntService$Component;

    .line 87
    .local v0, "code":Lcom/dsi/ant/AntService$Component;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/AntService$Component;->sValues:[Lcom/dsi/ant/AntService$Component;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 88
    sget-object v2, Lcom/dsi/ant/AntService$Component;->sValues:[Lcom/dsi/ant/AntService$Component;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/AntService$Component;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    sget-object v2, Lcom/dsi/ant/AntService$Component;->sValues:[Lcom/dsi/ant/AntService$Component;

    aget-object v0, v2, v1

    .line 94
    :cond_0
    return-object v0

    .line 87
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 74
    iget v0, p0, Lcom/dsi/ant/AntService$Component;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/AntService$Component;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/dsi/ant/AntService$Component;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/AntService$Component;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/AntService$Component;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/dsi/ant/AntService$Component;->$VALUES:[Lcom/dsi/ant/AntService$Component;

    invoke-virtual {v0}, [Lcom/dsi/ant/AntService$Component;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/AntService$Component;

    return-object v0
.end method


# virtual methods
.method public getRawValue()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/dsi/ant/AntService$Component;->mRawValue:I

    return v0
.end method
