.class public Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
.super Landroid/app/Activity;
.source "Activity_SearchAllDevices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;,
        Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    }
.end annotation


# static fields
.field static final SAVEDSTATE_KEY_intCURRENTPROXIMITYSETTING:Ljava/lang/String; = "int_CurrentProximitySetting"

.field static final SAVEDSTATE_KEY_searchResultInfoArrayListDETECTEDDEVICELIST:Ljava/lang/String; = "searchResultInfoArrayList_DetectedDevices"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field adapter_connDevNameList:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field adapter_devNameList:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cancellingLock:Ljava/lang/Object;

.field private consecutiveModeActiveSearch_listIndex:I

.field currentProximityThreshold:I

.field protected isScanStopTimeoutTriggered:Z

.field protected isSingleSearchInProgress:Z

.field mAlreadyConnectedDeviceNumbers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field mIsConfigChange:Z

.field mIsReplyMessageSent:Z

.field mIsUserCancelled:Z

.field mLatch_DevConnected:Ljava/util/concurrent/CountDownLatch;

.field mPluginName:Ljava/lang/String;

.field mSavedSearchResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;",
            ">;"
        }
    .end annotation
.end field

.field mTextView_Status:Landroid/widget/TextView;

.field private final replySentLock:Ljava/lang/Object;

.field reqParams:Landroid/os/Bundle;

.field searchList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;",
            ">;"
        }
    .end annotation
.end field

.field searchLock:Ljava/lang/Object;

.field singleSearchResult:Lcom/dsi/ant/message/ChannelId;

.field private singleSearchResultCode:I

.field textView_ProxOffLabel:Landroid/widget/TextView;

.field textView_ProxValueLabel:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 194
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchLock:Ljava/lang/Object;

    .line 216
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    .line 219
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->replySentLock:Ljava/lang/Object;

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;II)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->deviceFound(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->stopScanSearches(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    return v0
.end method

.method static synthetic access$702(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
    .param p1, "x1"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResultCode:I

    return p1
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cycleConsecutiveModeChannel()V

    return-void
.end method

.method static synthetic access$900(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->refreshProximityUi()V

    return-void
.end method

.method private cycleConsecutiveModeChannel()V
    .locals 7

    .prologue
    .line 609
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 612
    :try_start_0
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_2

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isSingleSearchInProgress:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    if-nez v3, :cond_2

    .line 614
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 616
    .local v0, "currentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    .line 619
    .local v1, "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 620
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v1, v3, :cond_1

    .line 621
    const/4 v1, 0x0

    .line 623
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 624
    .local v2, "si":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-nez v3, :cond_0

    .line 626
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iput-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 627
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 628
    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v5, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 634
    .end local v0    # "currentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v1    # "i":I
    .end local v2    # "si":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$7;

    invoke-direct {v4, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$7;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    const-wide/16 v5, 0x1388

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 644
    return-void

    .line 634
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private deviceFound(II)V
    .locals 10
    .param p1, "parentSearchIndex"    # I
    .param p2, "dev_chanDevId"    # I

    .prologue
    .line 1057
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    monitor-enter v8

    .line 1062
    :try_start_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    .line 1064
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    iget v7, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    if-ne v7, p1, :cond_0

    iget-object v7, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v7, v7, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, p2, :cond_0

    .line 1067
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v9, "Found already discovered device"

    invoke-static {v7, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    monitor-exit v8

    .line 1115
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    :goto_0
    return-void

    .line 1073
    :cond_1
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mAlreadyConnectedDeviceNumbers:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1077
    .local v1, "i":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, p2, :cond_2

    .line 1079
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v9, "Found already connected device"

    invoke-static {v7, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    monitor-exit v8

    goto :goto_0

    .line 1113
    .end local v1    # "i":Ljava/lang/Integer;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 1087
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1088
    .local v0, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    const/4 v3, 0x0

    .line 1092
    .local v3, "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    const/4 v5, 0x0

    .line 1093
    .local v5, "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :try_start_2
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mPluginName:Ljava/lang/String;

    invoke-virtual {v0, p2, v7}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->getDeviceInfoByChanDevId(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v5

    .line 1094
    if-nez v5, :cond_4

    .line 1096
    new-instance v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1097
    .end local v5    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .local v6, "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ID <"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v9, -0x20000001

    and-int/2addr v9, p2

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "> -not saved-"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 1098
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 1099
    const/4 v7, 0x0

    iput-object v7, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->device_dbId:Ljava/lang/Long;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v5, v6

    .line 1102
    .end local v6    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .restart local v5    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :cond_4
    const/high16 v7, 0x20000000

    and-int/2addr v7, p2

    if-lez v7, :cond_5

    .line 1103
    :try_start_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v5, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " (S&C)"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 1104
    :cond_5
    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    invoke-direct {v4, p1, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;-><init>(ILcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1105
    .end local v3    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    .local v4, "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    :try_start_5
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1109
    :try_start_6
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 1112
    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->addDevToDisplayList(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;)V

    .line 1113
    monitor-exit v8

    goto/16 :goto_0

    .line 1109
    .end local v4    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    .restart local v3    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    :catchall_1
    move-exception v7

    :goto_1
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .end local v5    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .restart local v6    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :catchall_2
    move-exception v7

    move-object v5, v6

    .end local v6    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .restart local v5    # "savedInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    goto :goto_1

    .end local v3    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    .restart local v4    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    :catchall_3
    move-exception v7

    move-object v3, v4

    .end local v4    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    .restart local v3    # "resultInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    goto :goto_1
.end method

.method private ensureCanceledSearch()V
    .locals 5

    .prologue
    .line 1119
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 1121
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1123
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1125
    if-eqz v1, :cond_1

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsConfigChange:Z

    if-nez v3, :cond_1

    const/4 v2, 0x1

    .line 1126
    .local v2, "releaseChannel":Z
    :goto_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v0, v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 1127
    .local v0, "ace":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    if-eqz v0, :cond_0

    .line 1128
    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 1123
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1125
    .end local v0    # "ace":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .end local v2    # "releaseChannel":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1130
    :cond_2
    monitor-exit v4

    .line 1132
    .end local v1    # "i":I
    :cond_3
    return-void

    .line 1130
    .restart local v1    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private initSearchScan()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 545
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v5, "antchannel_Channel"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/AntChannel;

    .line 547
    .local v1, "channel":Lcom/dsi/ant/channel/AntChannel;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v5, "int_ProximityBin"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->initProxValue(I)V

    .line 549
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    .line 550
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    new-instance v5, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    invoke-direct {v5, p0, v1, v6, v7}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    const/4 v4, -0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    .line 554
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v5, "bundle_AlternativeSearchParams"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 555
    .local v0, "altParams":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 557
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v5, "antchannel_AlternativeSearchChannel"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/AntChannel;

    .line 558
    .local v2, "channel2":Lcom/dsi/ant/channel/AntChannel;
    if-nez v2, :cond_0

    .line 559
    iput v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    .line 561
    :cond_0
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    const/4 v4, 0x1

    invoke-direct {v3, p0, v2, v0, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V

    .line 562
    .local v3, "si":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget v4, v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 564
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v5, "Unsupported multi scan requested"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Multi search is only supported for bike speed and cadence combo devices"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 567
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    .end local v2    # "channel2":Lcom/dsi/ant/channel/AntChannel;
    .end local v3    # "si":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_2
    return-void
.end method

.method private refreshProximityUi()V
    .locals 1

    .prologue
    .line 748
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 765
    return-void
.end method

.method private reportCancelled()V
    .locals 2

    .prologue
    .line 1023
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1024
    .local v0, "respToPlugin":Landroid/os/Message;
    const/4 v1, -0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1026
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    .line 1027
    return-void
.end method

.method private sendReply(Landroid/os/Message;)V
    .locals 6
    .param p1, "respToPlugin"    # Landroid/os/Message;

    .prologue
    .line 1031
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->replySentLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1033
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    if-eqz v2, :cond_0

    .line 1035
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Prevented erroneous sending of an extra reply: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    monitor-exit v3

    .line 1051
    :goto_0
    return-void

    .line 1039
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v4, "msgr_SearchResultReceiver"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1043
    .local v1, "retMsgr":Landroid/os/Messenger;
    :try_start_1
    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049
    :goto_1
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    .line 1050
    monitor-exit v3

    goto :goto_0

    .end local v1    # "retMsgr":Landroid/os/Messenger;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 1044
    .restart local v1    # "retMsgr":Landroid/os/Messenger;
    :catch_0
    move-exception v0

    .line 1047
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v4, "RemoteException trying to send result to client"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private stopScanSearches(Z)V
    .locals 7
    .param p1, "isImmediate"    # Z

    .prologue
    .line 576
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 578
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    if-eqz v3, :cond_0

    .line 579
    monitor-exit v4

    .line 604
    :goto_0
    return-void

    .line 581
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    .line 583
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 585
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v3, :cond_1

    .line 586
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto :goto_1

    .line 603
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 589
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$6;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$6;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    .line 599
    .local v2, "setUiState":Ljava/lang/Runnable;
    if-eqz p1, :cond_3

    .line 600
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 603
    :goto_2
    monitor-exit v4

    goto :goto_0

    .line 602
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    const-wide/16 v5, 0xbb8

    invoke-virtual {v3, v2, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method protected addDevToDisplayList(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;)V
    .locals 2
    .param p1, "i"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    .prologue
    .line 1008
    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 1009
    .local v0, "displayName":Ljava/lang/String;
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$13;

    invoke-direct {v1, p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$13;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1018
    return-void
.end method

.method public initProxValue(I)V
    .locals 5
    .param p1, "requestedProxValue"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 652
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    if-ne v2, v3, :cond_0

    .line 654
    if-ne p1, v3, :cond_1

    .line 656
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 658
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    .line 667
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    :goto_0
    const v2, 0x7f060007

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxValueLabel:Landroid/widget/TextView;

    .line 668
    const v2, 0x7f060008

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxOffLabel:Landroid/widget/TextView;

    .line 669
    const v2, 0x7f060006

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 670
    .local v0, "imageview_ProxIcon":Landroid/widget/ImageView;
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 743
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->refreshProximityUi()V

    .line 744
    return-void

    .line 663
    .end local v0    # "imageview_ProxIcon":Landroid/widget/ImageView;
    :cond_1
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 818
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    monitor-enter v1

    .line 820
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    .line 822
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    .line 824
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    if-nez v0, :cond_0

    .line 825
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reportCancelled()V

    .line 826
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 829
    return-void

    .line 826
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x1090003

    const v10, 0x1020014

    const/4 v8, -0x1

    const/4 v9, 0x0

    .line 228
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 230
    if-eqz p1, :cond_0

    .line 232
    const-string v5, "int_CurrentProximitySetting"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    .line 233
    const-string v5, "searchResultInfoArrayList_DetectedDevices"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    .line 241
    :goto_0
    const v5, 0x7f030004

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->setContentView(I)V

    .line 243
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsConfigChange:Z

    .line 244
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    .line 245
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    .line 249
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.dsi.ant.plugins.antplus.pcc.plugindata"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    .line 253
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    if-nez v5, :cond_1

    .line 254
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v6, "Attempt to start activity with illegal arguments"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    .line 256
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 541
    :goto_1
    return-void

    .line 237
    :cond_0
    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    .line 238
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    goto :goto_0

    .line 261
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v6, "str_PluginName"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mPluginName:Ljava/lang/String;

    .line 263
    const v5, 0x7f060001

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mTextView_Status:Landroid/widget/TextView;

    .line 264
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mTextView_Status:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Searching for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mPluginName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v6, "intarl_AvailableConnectedDevices"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mAlreadyConnectedDeviceNumbers:Ljava/util/ArrayList;

    .line 268
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mAlreadyConnectedDeviceNumbers:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 270
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v6, "intarl_AvailableConnectedDevNames"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 271
    .local v0, "alreadyConnectedNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v11, v10, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_connDevNameList:Landroid/widget/ArrayAdapter;

    .line 272
    const v5, 0x7f06000a

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 273
    .local v4, "listView_alreadyConnectedDevs":Landroid/widget/ListView;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_connDevNameList:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 274
    new-instance v5, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$1;

    invoke-direct {v5, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 283
    invoke-virtual {v4, v9}, Landroid/widget/ListView;->setVisibility(I)V

    .line 284
    const v5, 0x7f060009

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 287
    .end local v0    # "alreadyConnectedNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "listView_alreadyConnectedDevs":Landroid/widget/ListView;
    :cond_2
    const v5, 0x7f060005

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$2;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v11, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_devNameList:Landroid/widget/ArrayAdapter;

    .line 321
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    .line 322
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_devNameList:Landroid/widget/ArrayAdapter;

    iget-object v6, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_2

    .line 323
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    :cond_3
    const v5, 0x7f06000c

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 324
    .local v3, "listView_Devices":Landroid/widget/ListView;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_devNameList:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 325
    new-instance v5, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    invoke-direct {v5, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 510
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->initSearchScan()V

    .line 513
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 515
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v5, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v5, :cond_4

    .line 516
    iget-object v5, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v6, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto :goto_3

    .line 519
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_5
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    .line 520
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I

    if-eq v5, v8, :cond_6

    .line 522
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$4;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$4;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    const-wide/16 v7, 0x1388

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 532
    :cond_6
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    .line 533
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$5;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$5;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    const-wide/16 v7, 0x4e20

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 795
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    monitor-enter v1

    .line 797
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    .line 802
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsConfigChange:Z

    if-nez v0, :cond_0

    .line 804
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    .line 805
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsReplyMessageSent:Z

    if-nez v0, :cond_0

    .line 806
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reportCancelled()V

    .line 808
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 811
    return-void

    .line 808
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->stopScanSearches(Z)V

    .line 789
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 790
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 781
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsConfigChange:Z

    .line 782
    invoke-super {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 770
    const-string v0, "int_CurrentProximitySetting"

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 771
    const-string v0, "searchResultInfoArrayList_DetectedDevices"

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 772
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 773
    return-void
.end method

.method protected returnExistingAndFinish(I)V
    .locals 4
    .param p1, "devId"    # I

    .prologue
    const/4 v3, 0x1

    .line 834
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    monitor-enter v2

    .line 836
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    .line 837
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    .line 838
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 842
    .local v0, "respToPlugin":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 843
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 844
    iput p1, v0, Landroid/os/Message;->arg2:I

    .line 846
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    .line 848
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 850
    return-void

    .line 838
    .end local v0    # "respToPlugin":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected returnNewAndFinish(ILjava/lang/String;)V
    .locals 7
    .param p1, "devId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 955
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;

    monitor-enter v4

    .line 957
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    if-eqz v3, :cond_0

    .line 958
    monitor-exit v4

    .line 1002
    :goto_0
    return-void

    .line 960
    :cond_0
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResultCode:I

    sparse-switch v3, :sswitch_data_0

    .line 993
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Search reported failure: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResultCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 995
    .local v0, "failMsg":Landroid/os/Message;
    const/16 v3, -0x64

    iput v3, v0, Landroid/os/Message;->what:I

    .line 996
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResultCode:I

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 997
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    .line 998
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 1001
    .end local v0    # "failMsg":Landroid/os/Message;
    :goto_1
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 964
    :sswitch_0
    :try_start_1
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    .line 965
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 966
    .local v2, "respToPlugin":Landroid/os/Message;
    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    .line 967
    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 968
    const v3, -0x20000001

    and-int/2addr v3, p1

    iput v3, v2, Landroid/os/Message;->arg2:I

    .line 969
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 970
    .local v1, "respData":Landroid/os/Bundle;
    const-string v3, "str_SelectedDeviceName"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 973
    invoke-direct {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    .line 974
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    goto :goto_1

    .line 979
    .end local v1    # "respData":Landroid/os/Bundle;
    .end local v2    # "respToPlugin":Landroid/os/Message;
    :sswitch_1
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$12;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$12;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 960
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method

.method protected waitForNewThenFinish(ILjava/lang/String;)V
    .locals 12
    .param p1, "devId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 861
    :try_start_0
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mLatch_DevConnected:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v9, 0x0

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v9, v10, v11}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 863
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->returnNewAndFinish(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 949
    :goto_0
    return-void

    .line 866
    :catch_0
    move-exception v1

    .line 868
    .local v1, "e1":Ljava/lang/InterruptedException;
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;

    const-string v9, "InterruptedException while waiting on search for selected device"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V

    .line 870
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 871
    .local v3, "respToPlugin":Landroid/os/Message;
    const/4 v8, -0x4

    iput v8, v3, Landroid/os/Message;->what:I

    .line 872
    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V

    .line 873
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    goto :goto_0

    .line 878
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    .end local v3    # "respToPlugin":Landroid/os/Message;
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 879
    .local v2, "li":Landroid/view/LayoutInflater;
    const v8, 0x7f030006

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 881
    .local v4, "searchView":Landroid/view/View;
    const v8, 0x7f060012

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 883
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 884
    .local v0, "adlgBldr":Landroid/app/AlertDialog$Builder;
    const-string v8, "Acquiring Signal"

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 885
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 886
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 887
    const-string v8, "Cancel"

    new-instance v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;

    invoke-direct {v9, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 920
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 921
    .local v7, "waitDialog":Landroid/app/AlertDialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 923
    const v8, 0x7f060001

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 924
    .local v5, "statusDisplay":Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Connecting to \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 926
    new-instance v6, Ljava/lang/Thread;

    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;

    invoke-direct {v8, p0, v7, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/app/AlertDialog;ILjava/lang/String;)V

    invoke-direct {v6, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 948
    .local v6, "t":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method
