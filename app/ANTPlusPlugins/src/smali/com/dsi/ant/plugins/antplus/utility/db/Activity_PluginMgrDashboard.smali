.class public Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;
.super Landroid/app/ListActivity;
.source "Activity_PluginMgrDashboard.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 32
    :try_start_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ANT+ Plugin Settings Version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->setContentView(I)V

    .line 40
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v2, "menuItems":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard$1;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard$2;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x1090004

    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v4, v8

    const/4 v1, 0x1

    const-string v5, "desc"

    aput-object v5, v4, v1

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 48
    .local v0, "adapter":Landroid/widget/SimpleAdapter;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 49
    return-void

    .line 33
    .end local v0    # "adapter":Landroid/widget/SimpleAdapter;
    .end local v2    # "menuItems":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    :catch_0
    move-exception v6

    .line 35
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ANT+ Plugin Settings Version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 55
    packed-switch p3, :pswitch_data_0

    .line 82
    :pswitch_0
    const-string v1, "This menu item is not implemented"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 84
    :goto_0
    return-void

    .line 59
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .restart local v0    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 71
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .restart local v0    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 77
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_4
    const-string v1, "saved_devices.db"

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_PluginMgrDashboard;->deleteDatabase(Ljava/lang/String;)Z

    .line 78
    const-string v1, "Database Cleared"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
