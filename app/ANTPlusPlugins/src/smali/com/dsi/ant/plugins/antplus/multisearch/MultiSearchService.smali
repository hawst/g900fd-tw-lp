.class public Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "MultiSearchService.java"


# instance fields
.field private final mClients:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

.field private final mState_LOCK:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    .line 34
    new-instance v0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mClients:Ljava/util/HashSet;

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mState_LOCK:Ljava/lang/Object;

    return-void
.end method

.method private addClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
    .locals 2
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mClients:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {p1, v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->attach(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V

    .line 163
    monitor-exit v1

    .line 164
    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private initializeScanController(Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 8
    .param p1, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 123
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 125
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->isStarted()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 127
    monitor-exit v5

    .line 148
    :goto_0
    return v3

    .line 130
    :cond_0
    sget-object v6, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7, p1, p2}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 133
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_1

    .line 135
    monitor-exit v5

    move v3, v4

    goto :goto_0

    .line 138
    :cond_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {v6, v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->start(Lcom/dsi/ant/channel/AntChannel;)I

    move-result v1

    .line 140
    .local v1, "result":I
    if-nez v1, :cond_2

    .line 142
    monitor-exit v5

    goto :goto_0

    .line 150
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v1    # "result":I
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 145
    .restart local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .restart local v1    # "result":I
    :cond_2
    :try_start_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 146
    .local v2, "resultMsg":Landroid/os/Message;
    iput v1, v2, Landroid/os/Message;->what:I

    .line 147
    invoke-virtual {p0, p1, v2}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 148
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v4

    goto :goto_0
.end method

.method private sendConnectedDevices(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
    .locals 5
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 197
    new-instance v2, Landroid/os/Messenger;

    new-instance v3, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;

    invoke-direct {v3, p0, p1}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    invoke-direct {v2, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 222
    .local v2, "resultMessenger":Landroid/os/Messenger;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.dsi.ant.plugins.antplus.queryalreadyconnecteddevices"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    .local v1, "query":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 225
    .local v0, "params":Landroid/os/Bundle;
    const-string v3, "Version_int"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 226
    const-string v3, "CmdSeqNum_int"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 227
    const-string v3, "ResultMsgr_messenger"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 230
    const-string v3, "Mode_int"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 232
    const-string v3, "DevType_int"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 236
    const-string v3, "com.dsi.ant.plugins.antplus.queryalreadyconnecteddevices.params"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 238
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->sendBroadcast(Landroid/content/Intent;)V

    .line 239
    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 2
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 53
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Multi device search does not allow deviceconnection."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 61
    packed-switch p1, :pswitch_data_0

    .line 108
    :goto_0
    return v4

    .line 65
    :pswitch_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 67
    :try_start_0
    invoke-direct {p0, p2, p3}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->initializeScanController(Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 70
    monitor-exit v5

    .line 108
    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    .line 73
    :cond_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-direct {v1, p4, p3, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;-><init>(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;)V

    .line 74
    .local v1, "filter":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
    invoke-direct {p0, v1}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->addClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    .line 75
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 78
    .local v2, "reply":Landroid/os/Message;
    iput v4, v2, Landroid/os/Message;->what:I

    .line 79
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 87
    .local v3, "resultInfo":Landroid/os/Bundle;
    const-string v4, "uuid_AccessToken"

    iget-object v5, v1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->uid:Ljava/util/UUID;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 89
    const-string v4, "msgr_PluginComm"

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getCmdHandler()Landroid/os/Messenger;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 92
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 95
    :try_start_1
    invoke-virtual {p2, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 100
    :goto_2
    invoke-direct {p0, v1}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->sendConnectedDevices(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_1

    .line 75
    .end local v1    # "filter":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
    .end local v2    # "reply":Landroid/os/Message;
    .end local v3    # "resultInfo":Landroid/os/Bundle;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 96
    .restart local v1    # "filter":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
    .restart local v2    # "reply":Landroid/os/Message;
    .restart local v3    # "resultInfo":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_2

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
    .locals 2
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 173
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mClients:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->detach()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mClients:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->mScanner:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->stop()V

    .line 184
    :cond_1
    monitor-exit v1

    .line 185
    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
