.class public Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P4_BaseData.java"


# static fields
.field private static final TIMESTAMP_1024THS_ROLLOVER:I = 0xffff


# instance fields
.field private heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

.field private p4Evt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;)V
    .locals 2
    .param p1, "eventTimestamp_1024ths"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p2, "rrIntervalDecoder"    # Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->p4Evt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const v1, 0xffff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 37
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 38
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    .line 39
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 14
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 58
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v13

    .line 62
    .local v13, "newPreviousHeartBeatEventTime":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 66
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v12

    .line 71
    .local v12, "newCurrentHeartBeatEventTimestamp":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v12}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 72
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v10

    .line 74
    .local v10, "newAccumulatedEventTimestamp":J
    sub-int v9, v12, v13

    .line 76
    .local v9, "diff":I
    if-gez v9, :cond_2

    .line 78
    const v2, 0xffff

    add-int/2addr v9, v2

    .line 81
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    int-to-long v3, v9

    sub-long v3, v10, v3

    invoke-virtual {v2, v3, v4, v13}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->reset(JI)V

    .line 84
    .end local v9    # "diff":I
    .end local v10    # "newAccumulatedEventTimestamp":J
    .end local v12    # "newCurrentHeartBeatEventTimestamp":I
    :cond_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v13}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 85
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    move-wide v3, p1

    move-wide/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodePageFourAccumulators(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 87
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->p4Evt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 91
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    const-string v2, "int_manufacturerSpecificByte"

    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    const-string v2, "decimal_timestampOfPreviousToLastHeartBeatEvent"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0x400

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v5, 0xa

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 96
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->p4Evt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->p4Evt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;->previousHeartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 108
    return-void
.end method
