.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P71_CommandStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus$1;
    }
.end annotation


# instance fields
.field private cmdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xdb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;->cmdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 19
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 44
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;->cmdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v14}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v14

    if-nez v14, :cond_0

    .line 101
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v2, "b":Landroid/os/Bundle;
    const-string v14, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v2, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 49
    const-string v14, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v2, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 51
    new-instance v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;

    invoke-direct {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;-><init>()V

    .line 53
    .local v10, "status":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x2

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 54
    .local v7, "id":I
    invoke-static {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    move-result-object v14

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->lastReceivedCommandId:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 56
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x3

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v9

    .line 57
    .local v9, "sequence":I
    const/16 v14, 0xff

    if-ne v9, v14, :cond_1

    .line 58
    const/4 v9, -0x1

    .line 59
    :cond_1
    iput v9, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->lastReceivedSequenceNumber:I

    .line 61
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x4

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    .line 62
    .local v3, "cmdStatus":I
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$Status;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$Status;

    move-result-object v14

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$Status;

    .line 64
    const/4 v14, 0x4

    new-array v14, v14, [B

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->rawResponseData:[B

    .line 65
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x5

    iget-object v0, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->rawResponseData:[B

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x4

    invoke-static/range {v14 .. v18}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    sget-object v14, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusFitnessEquipmentPcc$CommandStatus$CommandId:[I

    iget-object v15, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->lastReceivedCommandId:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v15}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    .line 99
    :goto_1
    const-string v14, "parcelable_CommandStatus"

    invoke-virtual {v2, v14, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;->cmdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v14, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0

    .line 70
    :pswitch_0
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/16 v15, 0x8

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v8

    .line 71
    .local v8, "rawTotRes":I
    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v8}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x2

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x1

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->totalResistance:Ljava/math/BigDecimal;

    goto :goto_1

    .line 74
    .end local v8    # "rawTotRes":I
    :pswitch_1
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x7

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v11

    .line 75
    .local v11, "targetPower":I
    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v11}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x4

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x2

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->targetPower:Ljava/math/BigDecimal;

    goto :goto_1

    .line 78
    .end local v11    # "targetPower":I
    :pswitch_2
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x6

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v12

    .line 79
    .local v12, "wndResCoeff":I
    const/16 v14, 0xff

    if-ne v12, v14, :cond_2

    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "0.51"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    :goto_2
    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->windResistanceCoefficient:Ljava/math/BigDecimal;

    .line 82
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x7

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v13

    .line 83
    .local v13, "wndSpd":I
    const/16 v14, 0xff

    if-ne v13, v14, :cond_3

    const/4 v14, 0x0

    :goto_3
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->windSpeed:Ljava/lang/Integer;

    .line 85
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/16 v15, 0x8

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    .line 86
    .local v5, "drftFac":I
    const/16 v14, 0xff

    if-ne v5, v14, :cond_4

    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "1.00"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    :goto_4
    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->draftingFactor:Ljava/math/BigDecimal;

    goto/16 :goto_1

    .line 79
    .end local v5    # "drftFac":I
    .end local v13    # "wndSpd":I
    :cond_2
    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v12}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x64

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x2

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    goto :goto_2

    .line 83
    .restart local v13    # "wndSpd":I
    :cond_3
    add-int/lit8 v14, v13, -0x7f

    goto :goto_3

    .line 86
    .restart local v5    # "drftFac":I
    :cond_4
    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x64

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x2

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    goto :goto_4

    .line 89
    .end local v5    # "drftFac":I
    .end local v12    # "wndResCoeff":I
    .end local v13    # "wndSpd":I
    :pswitch_3
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/4 v15, 0x6

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    .line 90
    .local v6, "grade":I
    const v14, 0xffff

    if-eq v6, v14, :cond_5

    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v6}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x64

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x2

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0xc8

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v14, v15}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v14

    :goto_5
    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->grade:Ljava/math/BigDecimal;

    .line 92
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v14

    const/16 v15, 0x8

    aget-byte v14, v14, v15

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    .line 93
    .local v4, "coeff":I
    const/16 v14, 0xff

    if-eq v4, v14, :cond_6

    new-instance v14, Ljava/math/BigDecimal;

    invoke-direct {v14, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v15, Ljava/math/BigDecimal;

    const/16 v16, 0x4e20

    invoke-direct/range {v15 .. v16}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v16, 0x5

    sget-object v17, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual/range {v14 .. v17}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v14

    :goto_6
    iput-object v14, v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;->rollingResistanceCoefficient:Ljava/math/BigDecimal;

    goto/16 :goto_1

    .line 90
    .end local v4    # "coeff":I
    :cond_5
    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "0.00"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 93
    .restart local v4    # "coeff":I
    :cond_6
    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "0.004"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;->cmdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
