.class public Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "ControllableDeviceService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 1
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v1, "str_PluginName"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->CONTROLLABLE_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v1, "predefinednetwork_NetKey"

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 41
    const-string v1, "int_RfFreq"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    const-string v1, "int_TransType"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    const-string v1, "int_DevType"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->CONTROLLABLE_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    const-string v1, "int_Period"

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 17
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v14

    move-object/from16 v0, p3

    iput-object v14, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 66
    invoke-static/range {p1 .. p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getControlsModeFromRequestAccessValue(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    move-result-object v9

    .line 68
    .local v9, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isModeValid(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 69
    const/4 v14, 0x0

    .line 174
    :goto_0
    return v14

    .line 72
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v14}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isDeviceClosed()Z

    move-result v14

    if-nez v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v14, v9}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isModeInUse(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 74
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 75
    .local v12, "response":Landroid/os/Message;
    const/4 v14, -0x6

    iput v14, v12, Landroid/os/Message;->what:I

    .line 76
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 77
    const/4 v14, 0x1

    goto :goto_0

    .line 81
    .end local v12    # "response":Landroid/os/Message;
    :cond_1
    const-string v14, "int_ChannelDeviceId"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 83
    .local v11, "requestedDeviceNumber":I
    if-ltz v11, :cond_2

    const v14, 0xffff

    if-le v11, v14, :cond_3

    .line 86
    :cond_2
    sget-object v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Requested device number out of range, value: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 88
    .restart local v12    # "response":Landroid/os/Message;
    const/16 v14, -0x9

    iput v14, v12, Landroid/os/Message;->what:I

    .line 89
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 90
    const/4 v14, 0x1

    goto :goto_0

    .line 94
    .end local v12    # "response":Landroid/os/Message;
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v14}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isDeviceClosed()Z

    move-result v14

    if-nez v14, :cond_4

    .line 97
    if-eqz v11, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    iget-object v14, v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v14, v14, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    if-eq v11, v14, :cond_8

    .line 101
    sget-object v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->TAG:Ljava/lang/String;

    const-string v15, "Client requested a custom DeviceNumber when one is already set"

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 103
    .restart local v12    # "response":Landroid/os/Message;
    const/4 v14, -0x6

    iput v14, v12, Landroid/os/Message;->what:I

    .line 104
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 105
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 111
    .end local v12    # "response":Landroid/os/Message;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v6

    .line 115
    .local v6, "deviceParams":Landroid/os/Bundle;
    if-eqz v11, :cond_6

    .line 118
    move v5, v11

    .line 135
    .local v5, "deviceNumber":I
    :cond_5
    sget-object v14, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v14, v15, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v3

    .line 136
    .local v3, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v3, :cond_7

    .line 137
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 123
    .end local v3    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    .end local v5    # "deviceNumber":I
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getTwoByteUniqueId(Landroid/content/Context;)I

    move-result v5

    .line 125
    .restart local v5    # "deviceNumber":I
    const/4 v14, -0x1

    if-ne v5, v14, :cond_5

    .line 127
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 128
    .restart local v12    # "response":Landroid/os/Message;
    const/4 v14, -0x4

    iput v14, v12, Landroid/os/Message;->what:I

    .line 129
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 130
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 139
    .end local v12    # "response":Landroid/os/Message;
    .restart local v3    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_7
    const/4 v13, 0x0

    .line 141
    .local v13, "swVersion":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->getPackageName()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x80

    invoke-virtual/range {v14 .. v16}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    .line 142
    .local v10, "pInfo":Landroid/content/pm/PackageInfo;
    iget v13, v10, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    .end local v10    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    :try_start_1
    new-instance v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    .line 150
    .local v4, "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    iput-object v14, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 151
    new-instance v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-direct {v14, v4, v3, v6, v13}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_1

    .line 163
    .end local v3    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    .end local v4    # "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v5    # "deviceNumber":I
    .end local v6    # "deviceParams":Landroid/os/Bundle;
    .end local v13    # "swVersion":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v14, v2, v15}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v14

    if-nez v14, :cond_a

    .line 166
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    iget-object v14, v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-nez v14, :cond_9

    .line 167
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v14}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->closeDevice()V

    .line 174
    :cond_9
    :goto_2
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 143
    .restart local v3    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    .restart local v5    # "deviceNumber":I
    .restart local v6    # "deviceParams":Landroid/os/Bundle;
    .restart local v13    # "swVersion":I
    :catch_0
    move-exception v8

    .line 144
    .local v8, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->TAG:Ljava/lang/String;

    const-string v15, "Unable to retrieve software version: "

    invoke-static {v14, v15, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 152
    .end local v8    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v7

    .line 154
    .local v7, "e":Ljava/nio/channels/ClosedChannelException;
    sget-object v14, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->TAG:Ljava/lang/String;

    const-string v15, "Failed to instantiate device: Constructor threw ClosedChannelException."

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 156
    .restart local v12    # "response":Landroid/os/Message;
    const/4 v14, -0x4

    iput v14, v12, Landroid/os/Message;->what:I

    .line 157
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 158
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 171
    .end local v3    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    .end local v5    # "deviceNumber":I
    .end local v6    # "deviceParams":Landroid/os/Bundle;
    .end local v7    # "e":Ljava/nio/channels/ClosedChannelException;
    .end local v12    # "response":Landroid/os/Message;
    .end local v13    # "swVersion":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDeviceService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v14, v9, v15}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->enableMode(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)V

    goto :goto_2
.end method
