.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V
    .locals 6
    .param p1, "stateCode"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    .param p2, "reason"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    .prologue
    const-wide/16 v2, 0x0

    .line 290
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$1;->$SwitchMap$com$dsi$ant$plugins$antplus$utility$antfs$AntFsHostSession$AntFsHostState:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 305
    :goto_0
    return-void

    .line 293
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    const/16 v1, 0x1f4

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    const/16 v1, 0x226

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    goto :goto_0

    .line 299
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->inTransportState:Z

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
