.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchResultInfo"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field private final ipcVersionNumber:I

.field parentSearch_listIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)V
    .locals 1
    .param p1, "parentSearch_listIndex"    # I
    .param p2, "devInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->ipcVersionNumber:I

    .line 171
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    .line 172
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 173
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->ipcVersionNumber:I

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 166
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 156
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->ipcVersionNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 159
    return-void
.end method
