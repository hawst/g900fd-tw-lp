.class Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;
.super Ljava/lang/Object;
.source "AntFsHostSession.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestConnectToTransport(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

.field final synthetic val$passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->val$passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 206
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mSerialNumberOfHost:J
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$300(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkRf:I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$400(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v4

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkPeriod:I
    invoke-static {v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$500(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v5

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mBeaconInterval:I
    invoke-static {v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$600(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JIII)V

    .line 207
    .local v0, "linkTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    invoke-static {v2, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v9

    .line 208
    .local v9, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v9, v2, :cond_0

    move-object v2, v9

    .line 219
    :goto_0
    return-object v2

    .line 211
    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->getClientManufacturerId()I

    move-result v5

    .line 212
    .local v5, "clientMfgId":I
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->getClientDeviceType()I

    move-result v6

    .line 214
    .local v6, "clientDevType":I
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->val$passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mAntDeviceNumberOfClient:I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$800(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v4

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mSerialNumberOfHost:J
    invoke-static {v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$300(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)J

    move-result-wide v7

    invoke-direct/range {v1 .. v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;IIIJ)V

    .line 215
    .local v1, "authTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v9

    .line 216
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v9, v2, :cond_1

    move-object v2, v9

    .line 217
    goto :goto_0

    .line 219
    :cond_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    goto :goto_0
.end method
