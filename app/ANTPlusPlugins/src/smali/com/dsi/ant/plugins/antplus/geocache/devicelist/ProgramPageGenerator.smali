.class public Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;
.super Ljava/lang/Object;
.source "ProgramPageGenerator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compose_Hint_pp(ILjava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "startPageNum"    # I
    .param p1, "hint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v2, "pps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v0, v5, :cond_2

    .line 94
    add-int/lit8 v4, p0, 0x1

    .end local p0    # "startPageNum":I
    .local v4, "startPageNum":I
    const/4 v5, 0x2

    invoke-static {p0, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v3

    .line 95
    .local v3, "programArray":[B
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v5, 0x6

    if-ge v1, v5, :cond_1

    .line 97
    add-int v5, v0, v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 98
    add-int/lit8 v5, v1, 0x2

    add-int v6, v0, v1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 95
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 100
    :cond_0
    add-int/lit8 v5, v1, 0x2

    const/4 v6, 0x0

    aput-byte v6, v3, v5

    goto :goto_2

    .line 102
    :cond_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    add-int/lit8 v0, v0, 0x6

    move p0, v4

    .end local v4    # "startPageNum":I
    .restart local p0    # "startPageNum":I
    goto :goto_0

    .line 105
    .end local v1    # "j":I
    .end local v3    # "programArray":[B
    :cond_2
    return-object v2
.end method

.method public static compose_Latitude_pp(ILjava/math/BigDecimal;)[B
    .locals 7
    .param p0, "pageNum"    # I
    .param p1, "latitude"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v6, 0x0

    .line 70
    invoke-static {p0, v6}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v0

    .line 72
    .local v0, "programArray":[B
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

    const/16 v4, 0x9

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {p1, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v6, v4}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v1

    .line 73
    .local v1, "semicircles":J
    const/4 v3, 0x2

    invoke-static {v0, v3, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutSignedNumIn4LeBytes([BIJ)V

    .line 75
    return-object v0
.end method

.method public static compose_Longitude_pp(ILjava/math/BigDecimal;)[B
    .locals 6
    .param p0, "pageNum"    # I
    .param p1, "longitude"    # Ljava/math/BigDecimal;

    .prologue
    .line 80
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v0

    .line 82
    .local v0, "programArray":[B
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

    const/16 v4, 0x9

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {p1, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v1

    .line 83
    .local v1, "semicircles":J
    const/4 v3, 0x2

    invoke-static {v0, v3, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutSignedNumIn4LeBytes([BIJ)V

    .line 85
    return-object v0
.end method

.method public static compose_Page0ID_pp(Ljava/lang/String;)[B
    .locals 11
    .param p0, "idString"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 24
    invoke-static {v6, v6}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v3

    .line 26
    .local v3, "programArray":[B
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x9

    if-le v4, v5, :cond_0

    .line 27
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "String is over 8 characters long"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 29
    :cond_0
    const/16 v4, 0x9

    new-array v0, v4, [I

    .line 30
    .local v0, "ascii6":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v4, 0x9

    if-ge v2, v4, :cond_4

    .line 32
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 34
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 35
    .local v1, "c":C
    const/16 v4, 0x20

    if-lt v1, v4, :cond_1

    const/16 v4, 0x5f

    if-le v1, v4, :cond_2

    .line 36
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not in range of supported characters"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 38
    :cond_2
    add-int/lit8 v4, v1, -0x20

    aput v4, v0, v2

    .line 30
    .end local v1    # "c":C
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 42
    :cond_3
    aput v6, v0, v2

    goto :goto_1

    .line 46
    :cond_4
    aget v4, v0, v6

    shl-int/lit8 v4, v4, 0x2

    aget v5, v0, v7

    and-int/lit8 v5, v5, 0x30

    shr-int/lit8 v5, v5, 0x4

    or-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    invoke-static {v3, v7, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 47
    aget v4, v0, v7

    and-int/lit8 v4, v4, 0xf

    shl-int/lit8 v4, v4, 0x4

    aget v5, v0, v8

    and-int/lit8 v5, v5, 0x3c

    shr-int/lit8 v5, v5, 0x2

    or-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    invoke-static {v3, v8, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 48
    const/4 v4, 0x3

    aget v5, v0, v8

    and-int/lit8 v5, v5, 0x3

    shl-int/lit8 v5, v5, 0x6

    const/4 v6, 0x3

    aget v6, v0, v6

    or-int/2addr v5, v6

    and-int/lit16 v5, v5, 0xff

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 49
    const/4 v4, 0x4

    const/4 v5, 0x4

    aget v5, v0, v5

    shl-int/lit8 v5, v5, 0x2

    aget v6, v0, v9

    and-int/lit8 v6, v6, 0x30

    shr-int/lit8 v6, v6, 0x4

    or-int/2addr v5, v6

    and-int/lit16 v5, v5, 0xff

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 50
    aget v4, v0, v9

    and-int/lit8 v4, v4, 0xf

    shl-int/lit8 v4, v4, 0x4

    aget v5, v0, v10

    and-int/lit8 v5, v5, 0x3c

    shr-int/lit8 v5, v5, 0x2

    or-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    invoke-static {v3, v9, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 51
    aget v4, v0, v10

    and-int/lit8 v4, v4, 0x3

    shl-int/lit8 v4, v4, 0x6

    const/4 v5, 0x7

    aget v5, v0, v5

    or-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    invoke-static {v3, v10, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 52
    const/4 v4, 0x7

    const/16 v5, 0x8

    aget v5, v0, v5

    shl-int/lit8 v5, v5, 0x2

    and-int/lit16 v5, v5, 0xff

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 54
    return-object v3
.end method

.method public static compose_Page1PIN_pp(JI)[B
    .locals 3
    .param p0, "PIN"    # J
    .param p2, "numProgrammedPages"    # I

    .prologue
    const/16 v2, 0xff

    .line 59
    const/4 v1, 0x1

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v0

    .line 61
    .local v0, "programArray":[B
    const/4 v1, 0x2

    invoke-static {v0, v1, p0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 62
    const/4 v1, 0x6

    invoke-static {v0, v1, p2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 63
    const/4 v1, 0x7

    invoke-static {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 65
    return-object v0
.end method

.method public static compose_VisitCount_pp(IIJ)[B
    .locals 5
    .param p0, "pageNum"    # I
    .param p1, "visitCount"    # I
    .param p2, "lastVisitTime_unix_secs"    # J

    .prologue
    .line 111
    const/4 v3, 0x4

    invoke-static {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->createArray(II)[B

    move-result-object v2

    .line 113
    .local v2, "programArray":[B
    const-wide/32 v3, 0x259d4c00

    sub-long v0, p2, v3

    .line 114
    .local v0, "garminTime":J
    const/4 v3, 0x2

    invoke-static {v2, v3, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 115
    const/4 v3, 0x6

    invoke-static {v2, v3, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 117
    return-object v2
.end method

.method private static createArray(II)[B
    .locals 2
    .param p0, "pageNum"    # I
    .param p1, "pageType"    # I

    .prologue
    .line 16
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 17
    .local v0, "programArray":[B
    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 18
    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 19
    return-object v0
.end method

.method public static getAllProgrammingPages(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Z)Ljava/util/List;
    .locals 11
    .param p0, "deviceData"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    .param p1, "backfillInvalidPages"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;",
            "Z)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    .line 122
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v3, "pps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 126
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Required Identification String was null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 127
    :cond_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    if-nez v5, :cond_1

    .line 128
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Required PIN was null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 131
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_Page0ID_pp(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v4, "progpagesonly":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const/4 v1, 0x2

    .line 138
    .local v1, "pageNumCounter":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    if-eqz v5, :cond_2

    .line 139
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "pageNumCounter":I
    .local v2, "pageNumCounter":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    invoke-static {v1, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_Latitude_pp(ILjava/math/BigDecimal;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 140
    .end local v2    # "pageNumCounter":I
    .restart local v1    # "pageNumCounter":I
    :cond_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    if-eqz v5, :cond_3

    .line 141
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "pageNumCounter":I
    .restart local v2    # "pageNumCounter":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    invoke-static {v1, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_Longitude_pp(ILjava/math/BigDecimal;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 142
    .end local v2    # "pageNumCounter":I
    .restart local v1    # "pageNumCounter":I
    :cond_3
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 144
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_Hint_pp(ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 145
    .local v0, "hintPages":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v1, v5

    .line 146
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 148
    .end local v0    # "hintPages":Ljava/util/List;, "Ljava/util/List<[B>;"
    :cond_4
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    if-eqz v5, :cond_5

    .line 150
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "pageNumCounter":I
    .restart local v2    # "pageNumCounter":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v1, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_VisitCount_pp(IIJ)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 154
    .end local v2    # "pageNumCounter":I
    .restart local v1    # "pageNumCounter":I
    :cond_5
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6, v1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_Page1PIN_pp(JI)[B

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 157
    .end local v1    # "pageNumCounter":I
    .restart local v2    # "pageNumCounter":I
    :goto_0
    if-eqz p1, :cond_6

    const/16 v5, 0x20

    if-ge v2, v5, :cond_6

    .line 159
    const/16 v5, 0x8

    new-array v5, v5, [B

    const/4 v6, 0x0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "pageNumCounter":I
    .restart local v1    # "pageNumCounter":I
    int-to-byte v7, v2

    aput-byte v7, v5, v6

    const/4 v6, 0x1

    aput-byte v10, v5, v6

    const/4 v6, 0x2

    aput-byte v10, v5, v6

    const/4 v6, 0x3

    aput-byte v10, v5, v6

    const/4 v6, 0x4

    aput-byte v10, v5, v6

    const/4 v6, 0x5

    aput-byte v10, v5, v6

    const/4 v6, 0x6

    aput-byte v10, v5, v6

    const/4 v6, 0x7

    aput-byte v10, v5, v6

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    .end local v1    # "pageNumCounter":I
    .restart local v2    # "pageNumCounter":I
    goto :goto_0

    .line 163
    :cond_6
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 164
    return-object v3
.end method
