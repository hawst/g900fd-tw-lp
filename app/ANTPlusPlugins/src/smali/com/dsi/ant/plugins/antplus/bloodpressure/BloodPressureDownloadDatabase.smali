.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;
.super Ljava/lang/Object;
.source "BloodPressureDownloadDatabase.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "bpm_download.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dbContext:Landroid/content/Context;

.field private final instanceDeviceNumber:I

.field private mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "contextForDb"    # Landroid/content/Context;
    .param p2, "deviceNumber"    # I

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    .line 92
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->dbContext:Landroid/content/Context;

    .line 93
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->instanceDeviceNumber:I

    .line 94
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getAppDbId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "appPkgName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 215
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT App_Id FROM Applications WHERE (AppPkgName == \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ");"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 221
    .local v2, "cPlugin":Landroid/database/Cursor;
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 223
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 224
    .local v0, "app_DbId":J
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 235
    :goto_0
    return-wide v0

    .line 228
    .end local v0    # "app_DbId":J
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 230
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 231
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "AppPkgName"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v4, "Applications"

    invoke-virtual {p1, v4, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .restart local v0    # "app_DbId":J
    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->close()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    .line 111
    :cond_0
    return-void
.end method

.method public getDeviceDbId()J
    .locals 6

    .prologue
    .line 196
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 198
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT AntFsDeviceInfo_Id FROM AntFsDeviceInfo WHERE (AntDeviceNumber == \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->instanceDeviceNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\');"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 201
    .local v0, "cPlugin":Landroid/database/Cursor;
    const-wide/16 v2, -0x1

    .line 202
    .local v2, "dev_DbId":J
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 204
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 207
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 208
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 209
    return-wide v2
.end method

.method public getLastDownloadGarminTime(Ljava/lang/String;J)J
    .locals 6
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "dev_dbId"    # J

    .prologue
    .line 268
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 270
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT LastDownloadedGarminTime FROM LastDownloadRecords, Applications WHERE (Applications.AppPkgName == \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND Applications.App_Id == LastDownloadRecords.App_Id);"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 277
    .local v0, "cPlugin":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 279
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 286
    .local v2, "lastDownloadGarminTime":J
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 287
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 288
    return-wide v2

    .line 283
    .end local v2    # "lastDownloadGarminTime":J
    :cond_0
    const-wide/16 v2, 0x0

    .restart local v2    # "lastDownloadGarminTime":J
    goto :goto_0
.end method

.method public getLastDownloadedMeasurementGarminTime(Ljava/lang/String;J)J
    .locals 6
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "dev_dbId"    # J

    .prologue
    .line 321
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 323
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT LastDeliveredMeasurementGarminTimestamp FROM LastDownloadRecords, Applications WHERE (Applications.AppPkgName == \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND Applications.App_Id == LastDownloadRecords.App_Id);"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 330
    .local v0, "cPlugin":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 332
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 339
    .local v2, "lastDownloadGarminTime":J
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 340
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 341
    return-wide v2

    .line 336
    .end local v2    # "lastDownloadGarminTime":J
    :cond_0
    const-wide/16 v2, 0x0

    .restart local v2    # "lastDownloadGarminTime":J
    goto :goto_0
.end method

.method public getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    .locals 14
    .param p1, "antFsManufacturerId"    # I
    .param p2, "antFsDeviceType"    # I
    .param p3, "antDeviceNumber"    # I
    .param p4, "antFsSerialNumber"    # J

    .prologue
    .line 123
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    .line 124
    .local v12, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT AntFsDeviceInfo_Id, Passkey FROM AntFsDeviceInfo WHERE (AntFsManufacturerId == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntFsDeviceType == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntDeviceNumber == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntFsSerialNumber == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ");"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 132
    .local v11, "cPlugin":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 133
    .local v2, "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 135
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 136
    .local v13, "passkey_dbID":I
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 138
    .local v5, "passkey":[B
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .end local v2    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    int-to-long v3, v13

    move v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move-wide/from16 v9, p4

    invoke-direct/range {v2 .. v10}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;-><init>(J[BIIIJ)V

    .line 141
    .end local v5    # "passkey":[B
    .end local v13    # "passkey_dbID":I
    .restart local v2    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 142
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 143
    return-object v2
.end method

.method public insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 12
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    const/4 v11, 0x0

    .line 153
    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    iget v2, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    iget v3, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    iget-wide v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    move-result-object v9

    .line 154
    .local v9, "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 155
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v9, :cond_1

    .line 157
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 158
    .local v10, "values":Landroid/content/ContentValues;
    const-string v0, "Passkey"

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 159
    const-string v0, "AntFsDeviceInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AntFsDeviceInfo_Id == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v10, v1, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 161
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->TAG:Ljava/lang/String;

    const-string v1, "SQL Update() did not change exactly 1 passkey row"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_0
    iget-wide v0, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    iput-wide v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    .line 176
    :goto_0
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 177
    return-void

    .line 167
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_1
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 168
    .restart local v10    # "values":Landroid/content/ContentValues;
    const-string v0, "Passkey"

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 169
    const-string v0, "AntFsManufacturerId"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 170
    const-string v0, "AntFsDeviceType"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 171
    const-string v0, "AntDeviceNumber"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 172
    const-string v0, "AntFsSerialNumber"

    iget-wide v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 173
    const-string v0, "AntFsDeviceInfo"

    invoke-virtual {v6, v0, v11, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    .line 174
    .local v7, "dbID":J
    iput-wide v7, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    goto :goto_0
.end method

.method public invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 5
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    .line 185
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 186
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AntFsDeviceInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntFsDeviceInfo_Id == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 188
    sget-object v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->TAG:Ljava/lang/String;

    const-string v2, "SQL Delete() failed to remove exactly 1 passkey"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 191
    return-void
.end method

.method public open()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->dbContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    .line 101
    :cond_0
    return-void
.end method

.method public setLastDownloadGarminTime(Ljava/lang/String;JJ)V
    .locals 9
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "device_dbId"    # J
    .param p4, "garminTime"    # J

    .prologue
    .line 240
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 241
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 244
    :try_start_0
    invoke-direct {p0, v2, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getAppDbId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v0

    .line 247
    .local v0, "app_dbId":J
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 248
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "LastDownloadedGarminTime"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 249
    const-string v6, "LastDownloadRecords"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "App_Id == "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    int-to-long v3, v6

    .line 251
    .local v3, "updateId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-nez v6, :cond_0

    .line 253
    const-string v6, "App_Id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 254
    const-string v6, "AntFsDeviceInfo_Id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 255
    const-string v6, "LastDownloadRecords"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 257
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 262
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 264
    return-void

    .line 261
    .end local v0    # "app_dbId":J
    .end local v3    # "updateId":J
    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v6

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 262
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v6
.end method

.method public setLastDownloadedMeasurementGarminTime(Ljava/lang/String;JJ)V
    .locals 9
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "device_dbId"    # J
    .param p4, "garminTime"    # J

    .prologue
    .line 293
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 294
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 297
    :try_start_0
    invoke-direct {p0, v2, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getAppDbId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v0

    .line 300
    .local v0, "app_dbId":J
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 301
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "LastDeliveredMeasurementGarminTimestamp"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 302
    const-string v6, "LastDownloadRecords"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "App_Id == "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    int-to-long v3, v6

    .line 304
    .local v3, "updateId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-nez v6, :cond_0

    .line 306
    const-string v6, "App_Id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 307
    const-string v6, "AntFsDeviceInfo_Id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 308
    const-string v6, "LastDownloadRecords"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 310
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 315
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 317
    return-void

    .line 314
    .end local v0    # "app_dbId":J
    .end local v3    # "updateId":J
    .end local v5    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v6

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 315
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v6
.end method
