.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "SpeedAndDistanceDecoder.java"


# instance fields
.field private calcDistEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private calcSpdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field numMsgsUntilZeroSpeed:I

.field private rawEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0xffff

    .line 18
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 20
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcSpdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcDistEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 22
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->rawEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 24
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 27
    const/16 v0, 0x8

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 0
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 43
    return-void
.end method

.method public decodeSpeedAndDistance(JJII)V
    .locals 9
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "revTimestamp_1024ths"    # I
    .param p6, "revCounterValue"    # I

    .prologue
    const/16 v8, 0x400

    const/4 v7, 0x0

    const/16 v6, 0xa

    .line 48
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 49
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 51
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->rawEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 55
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 57
    const-string v2, "decimal_timestampOfLastEvent"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    invoke-direct {v4, v8}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v6, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 59
    const-string v2, "long_cumulativeRevolutions"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 61
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->rawEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 64
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-nez v2, :cond_5

    .line 66
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcSpdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 69
    const/4 v1, 0x0

    .line 70
    .local v1, "calcSpeedVal":Ljava/math/BigDecimal;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-nez v2, :cond_6

    .line 72
    :cond_1
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    if-lez v2, :cond_2

    .line 73
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    .line 75
    :cond_2
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    if-nez v2, :cond_3

    .line 76
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v2, v7}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v1

    .line 87
    :cond_3
    :goto_0
    if-eqz v1, :cond_4

    .line 89
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 91
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    const-string v2, "decimal_calculatedSpeed"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 95
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcSpdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 99
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "calcSpeedVal":Ljava/math/BigDecimal;
    :cond_4
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcDistEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-eqz v2, :cond_5

    .line 101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 103
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 105
    const-string v2, "decimal_calculatedAccumulatedDistance"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 108
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcDistEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 111
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_5
    return-void

    .line 80
    .restart local v1    # "calcSpeedVal":Ljava/math/BigDecimal;
    :cond_6
    new-instance v2, Ljava/math/BigDecimal;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v6, v4}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    new-instance v3, Ljava/math/BigDecimal;

    invoke-direct {v3, v8}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v6, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 84
    const/16 v2, 0x10

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->numMsgsUntilZeroSpeed:I

    goto :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcSpdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->calcDistEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->rawEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->eventTimestamp_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 121
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->revolutionCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 122
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 123
    return-void
.end method
