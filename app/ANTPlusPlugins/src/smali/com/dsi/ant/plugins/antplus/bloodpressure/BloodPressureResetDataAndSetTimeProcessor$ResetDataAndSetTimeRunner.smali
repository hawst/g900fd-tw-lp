.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;
.super Ljava/lang/Object;
.source "BloodPressureResetDataAndSetTimeProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResetDataAndSetTimeRunner"
.end annotation


# instance fields
.field afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

.field stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;)V
    .locals 1

    .prologue
    .line 145
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner$1;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    return-void
.end method

.method private checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;)Z
    .locals 4
    .param p1, "downloadResult"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .prologue
    .line 307
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    if-ne p1, v1, :cond_0

    .line 309
    const/4 v1, 0x0

    .line 318
    :goto_0
    return v1

    .line 313
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS download request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const/16 v0, -0x28

    .line 317
    .local v0, "resultToReport":I
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->handleMonitorFailure(I)V

    .line 318
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z
    .locals 4
    .param p1, "result"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .prologue
    .line 272
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-ne p1, v1, :cond_0

    .line 274
    const/4 v1, 0x0

    .line 301
    :goto_0
    return v1

    .line 278
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    sget-object v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$1;->$SwitchMap$com$dsi$ant$plugins$antplus$utility$antfs$AntFsHostSession$AntFsRequestResult:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 296
    const/16 v0, -0x28

    .line 300
    .local v0, "resultToReport":I
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->handleMonitorFailure(I)V

    .line 301
    const/4 v1, 0x1

    goto :goto_0

    .line 284
    .end local v0    # "resultToReport":I
    :pswitch_0
    const/16 v0, -0x3d

    .line 285
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 288
    .end local v0    # "resultToReport":I
    :pswitch_1
    const/16 v0, -0x410

    .line 289
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 292
    .end local v0    # "resultToReport":I
    :pswitch_2
    const/16 v0, -0x29

    .line 293
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private doResetData()Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v13, 0x0

    .line 226
    const/16 v1, 0x320

    move-object v0, p0

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->handleStateChange(IJJ)V

    .line 229
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner$2;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner$2;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;)V

    invoke-virtual {v0, v13, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v7

    .line 237
    .local v7, "downloadResult":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v13

    .line 267
    :goto_0
    return v0

    .line 243
    :cond_0
    :try_start_0
    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastDownloadedData()[B

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastRequestStartedTimeUtc()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;-><init>([BLjava/util/GregorianCalendar;)V
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .local v6, "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v12, "measurementFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    iget-object v0, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v10, v0, :cond_2

    .line 254
    iget-object v0, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 259
    .local v9, "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    iget v0, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->FIT_DATA_TYPE:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 260
    iget v0, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 244
    .end local v6    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .end local v9    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v10    # "i":I
    .end local v12    # "measurementFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v8

    .line 246
    .local v8, "e":Ljava/util/zip/DataFormatException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ANTFS directory DataFormatException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    const/16 v1, -0x28

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->handleMonitorFailure(I)V

    move v0, v13

    .line 248
    goto :goto_0

    .line 263
    .end local v8    # "e":Ljava/util/zip/DataFormatException;
    .restart local v6    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .restart local v10    # "i":I
    .restart local v12    # "measurementFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .end local v10    # "i":I
    .local v11, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 264
    .local v10, "i":Ljava/lang/Integer;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestErase(I)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v13

    .line 265
    goto/16 :goto_0

    .line 267
    .end local v10    # "i":Ljava/lang/Integer;
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected handleStateChange(IJJ)V
    .locals 6
    .param p1, "antFsStateCode"    # I
    .param p2, "transferredBytes"    # J
    .param p4, "totalBytes"    # J

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->reportAntFsStatus(IJJ)V

    .line 152
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 179
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-wide v2, v2, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->serialNumberForHost:J

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget v4, v4, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->antDeviceNumber:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JI)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .line 180
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget v1, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->linkRfFreq:I

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->linkRfPeriod:I

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->beaconInterval:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->setLinkChannelParameters(III)V

    .line 184
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    const/16 v1, -0x14

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->handleMonitorFailure(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    const/16 v1, 0x64

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->handleStateChange(IJJ)V

    .line 193
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestConnectToTransport(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    :try_start_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v0, v1, :cond_0

    .line 213
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v6

    .line 218
    .local v6, "e1":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ANTFS request InterruptedException"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    const/16 v1, -0x28

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->handleMonitorFailure(I)V

    goto :goto_0

    .line 195
    .end local v6    # "e1":Ljava/lang/InterruptedException;
    :cond_2
    const/16 v1, 0x320

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    :try_start_3
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->handleStateChange(IJJ)V

    .line 197
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->doResetData()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 210
    :try_start_4
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v0, v1, :cond_0

    .line 213
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 200
    :cond_3
    :try_start_5
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->doSetTime:Z

    if-eqz v0, :cond_4

    .line 202
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestSetTime()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 210
    :try_start_6
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v0, v1, :cond_0

    .line 213
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 206
    :cond_4
    :try_start_7
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->sendResult(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 210
    :try_start_8
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v0, v1, :cond_0

    .line 213
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v1, v2, :cond_5

    .line 213
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
.end method
