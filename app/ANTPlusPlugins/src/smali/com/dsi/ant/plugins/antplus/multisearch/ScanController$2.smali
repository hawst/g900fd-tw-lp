.class Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;
.super Ljava/lang/Object;
.source "ScanController.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 186
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->access$000(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 188
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/util/HashSet;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 189
    .local v2, "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    sparse-switch p1, :sswitch_data_0

    .line 208
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .line 210
    .local v1, "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    const/4 v3, -0x4

    invoke-interface {v1, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;->onScanStopped(I)V

    goto :goto_0

    .line 189
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    .end local v2    # "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 194
    .restart local v2    # "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    :sswitch_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .line 196
    .restart local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;->onPingClient()V

    goto :goto_1

    .line 201
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    :sswitch_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .line 203
    .restart local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    invoke-interface {v1, p2, p3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;->onScanResult(Lcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V

    goto :goto_2

    .line 214
    .end local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    :cond_0
    return-void

    .line 191
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method
