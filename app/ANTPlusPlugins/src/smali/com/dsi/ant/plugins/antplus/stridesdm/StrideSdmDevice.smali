.class public Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "StrideSdmDevice.java"


# instance fields
.field private P1_MainData:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;

.field private P3_Calories:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 0
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 34
    return-void
.end method


# virtual methods
.method public getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v1, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;->P1_MainData:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;

    .line 41
    new-instance v1, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;->P3_Calories:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;->P1_MainData:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance v1, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;->P3_Calories:Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/StrideSdmDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 50
    return-object v0
.end method
