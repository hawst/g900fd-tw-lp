.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "BloodPressureDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

.field downloadController:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

.field private mAntFsMfgId:I

.field private mServiceContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/content/Context;)V
    .locals 9
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "serviceContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntFsMfgId:I

    .line 60
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mServiceContext:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mServiceContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    .line 63
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mServiceContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v3

    iget-object v5, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0x39

    const/16 v7, 0x2000

    const/16 v8, 0x1e

    invoke-direct/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;-><init>(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;JIIII)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->downloadController:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    .line 71
    return-void
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 13
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 121
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 122
    .local v10, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 123
    .local v12, "response":Landroid/os/Message;
    iget v1, p2, Landroid/os/Message;->what:I

    iput v1, v12, Landroid/os/Message;->what:I

    .line 124
    iget v1, p2, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 202
    invoke-virtual {v12}, Landroid/os/Message;->recycle()V

    .line 203
    const/4 v12, 0x0

    .line 204
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 208
    :goto_0
    return-void

    .line 128
    :pswitch_0
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 129
    .local v11, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 130
    invoke-virtual {p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 132
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;

    const-string v3, "bool_UseAntFsProgressUpdates"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v2, p0, v3, v10}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mServiceContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0x39

    const/16 v7, 0x2000

    const/16 v8, 0x1e

    invoke-static/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;

    goto :goto_0

    .line 148
    .end local v11    # "params":Landroid/os/Bundle;
    :pswitch_1
    const/4 v1, 0x0

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 149
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntFsMfgId:I

    iput v1, v12, Landroid/os/Message;->arg2:I

    .line 150
    invoke-virtual {p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 155
    :pswitch_2
    const/4 v1, 0x0

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 156
    invoke-virtual {p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 158
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 159
    .restart local v11    # "params":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->downloadController:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    const-string v1, "bool_downloadNewOnly"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v1, "bool_monitorForNewMeasurements"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v1, "bool_UseAntFsProgressUpdates"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object v1, v10

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ZZZLcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;)V

    invoke-virtual {v6, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->addWatcher(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;)V

    goto :goto_0

    .line 171
    .end local v11    # "params":Landroid/os/Bundle;
    :pswitch_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->downloadController:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-virtual {v1, v10}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->removeWatcher(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    .line 173
    const/4 v1, 0x0

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 174
    invoke-virtual {p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 179
    :pswitch_4
    const/4 v1, 0x0

    iput v1, v12, Landroid/os/Message;->arg1:I

    .line 180
    invoke-virtual {p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 182
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 184
    .restart local v11    # "params":Landroid/os/Bundle;
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    const-string v2, "bool_doSetTime"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "bool_UseAntFsProgressUpdates"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v1, v10, v2, v3, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ZZLcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;)V

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mServiceContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x39

    const/16 v8, 0x2000

    const/16 v9, 0x1e

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;JIIII)V

    .line 197
    .local v0, "requestProcessor":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->processRequest()V

    goto/16 :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x4e21
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public checkChannelState(Z)V
    .locals 4
    .param p1, "isInit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->checkChannelState(Z)V

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v2, 0x1000

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 84
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v2, 0x39

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE in bpm checkChannelState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 89
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->downloadController:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->destroyMonitor()V

    .line 244
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 245
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->closeDevice()V

    .line 246
    return-void
.end method

.method public getPageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x1

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    const/16 v1, 0x43

    if-eq v0, v1, :cond_1

    .line 101
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsManufacturerId([B)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->mAntFsMfgId:I

    goto :goto_0
.end method
