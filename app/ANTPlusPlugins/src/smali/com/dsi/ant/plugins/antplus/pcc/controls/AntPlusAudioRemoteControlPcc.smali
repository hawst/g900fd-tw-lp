.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;
.super Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;
.source "AntPlusAudioRemoteControlPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final INVALIDCOMMANDDATA:I = 0xff

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAudioCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;

.field mAudioStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;-><init>()V

    .line 97
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 178
    return-void
.end method

.method public static requestAccessByDeviceNumber(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "antDeviceNumber"    # I
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;>;"
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;-><init>()V

    .local v6, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 174
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestRemoteControlAsyncScanController(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;
    .locals 2
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "searchProximityThreshold"    # I
    .param p3, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;-><init>()V

    .line 127
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;
    invoke-static {p0, p1, p2, v0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public RequestAudioCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V
    .locals 1
    .param p1, "audioCommandFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;
    .param p2, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .prologue
    .line 298
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->RequestAudioCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;I)V

    .line 299
    return-void
.end method

.method public RequestAudioCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;I)V
    .locals 6
    .param p1, "audioCommandFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;
    .param p2, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .param p3, "commandData"    # I

    .prologue
    .line 256
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 258
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestAudioCommand failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :goto_0
    return-void

    .line 262
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;

    .line 264
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 265
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e23

    iput v3, v0, Landroid/os/Message;->what:I

    .line 266
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 267
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 269
    const-string v3, "int_commandData"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 270
    const-string v3, "int_commandNumber"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 272
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 274
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 276
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestAudioCommand died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 281
    :cond_1
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    .line 284
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd RequestAudioCommand failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 286
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "RequestAudioCommand cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 289
    :cond_2
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    const-string v0, "ANT+ Plugin: Audio Remote Control"

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 16
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 189
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    sparse-switch v1, :sswitch_data_0

    .line 225
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 193
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;

    if-eqz v1, :cond_0

    .line 196
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 197
    .local v11, "b":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 198
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 199
    .local v2, "estTimestamp":J
    const-string v1, "int_volume"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 200
    .local v4, "volume":I
    const-string v1, "int_totalTrackTime"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 201
    .local v5, "totalTrackTime":I
    const-string v1, "int_currentTrackTime"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 202
    .local v6, "currentTrackTime":I
    const-string v1, "int_audioState"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    move-result-object v8

    .line 203
    .local v8, "audioState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;
    const-string v1, "int_repeatState"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    move-result-object v9

    .line 204
    .local v9, "repeatState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;
    const-string v1, "int_shuffleState"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    move-result-object v10

    .line 205
    .local v10, "shuffleState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;

    invoke-static {v11}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->readFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    move-result-object v7

    invoke-interface/range {v1 .. v10}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;->onNewAudioStatus(JIIILcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;)V

    goto :goto_0

    .line 211
    .end local v2    # "estTimestamp":J
    .end local v4    # "volume":I
    .end local v5    # "totalTrackTime":I
    .end local v6    # "currentTrackTime":I
    .end local v8    # "audioState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;
    .end local v9    # "repeatState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;
    .end local v10    # "shuffleState":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;
    .end local v11    # "b":Landroid/os/Bundle;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;

    if-eqz v1, :cond_0

    .line 214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 216
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 217
    .restart local v11    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 218
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v12

    .line 219
    .local v12, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_requestStatus"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v13

    .line 220
    .local v13, "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;

    invoke-interface {v1, v2, v3, v12, v13}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioCommandFinishedReceiver;->onAudioCommandFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto/16 :goto_0

    .line 189
    :sswitch_data_0
    .sparse-switch
        0xca -> :sswitch_0
        0xce -> :sswitch_1
    .end sparse-switch
.end method

.method public subscribeAudioStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;)V
    .locals 1
    .param p1, "AudioStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;

    .prologue
    const/16 v0, 0xca

    .line 238
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->mAudioStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc$IAudioStatusReceiver;

    .line 239
    if-eqz p1, :cond_0

    .line 241
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->subscribeToEvent(I)Z

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioRemoteControlPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
