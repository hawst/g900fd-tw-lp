.class Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb$DbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SavedDeviceDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DbHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const-string v0, "saved_devices.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 56
    const-string v0, "PRAGMA foreign_keys = ON"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    const-string v0, "CREATE TABLE Plugins(Plugin_Id INTEGER PRIMARY KEY,VisibleName TEXT UNIQUE NOT NULL,Chan_NetKey INTEGER NOT NULL,Chan_DevType INTEGER NOT NULL,Chan_TransType INTEGER NOT NULL,Chan_Period INTEGER NOT NULL,Chan_RfFreq INTEGER NOT NULL,UNIQUE (Chan_NetKey,Chan_DevType,Chan_TransType,Chan_Period,Chan_RfFreq))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    const-string v0, "CREATE TABLE Devices(Device_Id INTEGER PRIMARY KEY,Plugin_Id INTEGER NOT NULL REFERENCES Plugins (Plugin_Id),ChanDevId INTEGER NOT NULL,PreferredDevicePos INTEGER CHECK (PreferredDevicePos == 0 OR PreferredDevicePos == 1),VisibleName TEXT NOT NULL,UNIQUE (Plugin_Id,VisibleName))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 81
    const-string v0, "CREATE TABLE BikeSpdCadComboDeviceIds(Spd_Device_Id INTEGER REFERENCES Devices (Device_Id),Cad_Device_Id INTEGER REFERENCES Devices (Device_Id),UNIQUE (Spd_Device_Id)UNIQUE (Cad_Device_Id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    const-string v0, "CREATE TRIGGER oneDeviceIdPerPlugin_Insert BEFORE INSERT ON Devices FOR EACH ROW WHEN(EXISTS(SELECT 1 FROM Devices WHERE (NEW.ChanDevId == ChanDevId AND NEW.Plugin_Id == Plugin_Id AND 1 != (SELECT COUNT(*) FROM BikeSpdCadComboDeviceIds            WHERE ((Spd_Device_Id == NEW.ChanDevId OR Cad_Device_Id == NEW.ChanDevId)                OR (Spd_Device_Id == ChanDevId OR Cad_Device_Id == ChanDevId)))))) BEGIN SELECT RAISE(ROLLBACK, \'PreferredDevice Conflict\'); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 102
    const-string v0, "CREATE TRIGGER onePreferredDevicePerPlugin_Insert BEFORE INSERT ON Devices FOR EACH ROW WHEN(NEW.PreferredDevicePos != 0 AND EXISTS(SELECT 1 FROM Devices WHERE NEW.PreferredDevicePos == PreferredDevicePos AND NEW.Plugin_Id == Plugin_Id)) BEGIN SELECT RAISE(ROLLBACK, \'PreferredDevice Conflict\'); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 108
    const-string v0, "CREATE TRIGGER onePreferredDevicePerPlugin_Update BEFORE UPDATE ON Devices FOR EACH ROW WHEN(NEW.PreferredDevicePos != 0 AND NEW.PreferredDevicePos != OLD.PreferredDevicePos AND EXISTS(SELECT 1 FROM Devices WHERE NEW.PreferredDevicePos == PreferredDevicePos AND NEW.Plugin_Id == Plugin_Id)) BEGIN SELECT RAISE(ROLLBACK, \'PreferredDevice Conflict\'); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 49
    return-void
.end method
