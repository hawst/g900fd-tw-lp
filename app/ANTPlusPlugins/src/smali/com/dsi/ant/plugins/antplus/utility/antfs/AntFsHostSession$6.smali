.class Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;
.super Ljava/lang/Object;
.source "AntFsHostSession.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestStartAntFsMode(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

.field final synthetic val$customTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->val$customTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 320
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$002(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 321
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->val$customTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    .line 322
    .local v0, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v0, v1, :cond_0

    .line 326
    .end local v0    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :goto_0
    return-object v0

    .line 325
    .restart local v0    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->LINK_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$002(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 326
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    goto :goto_0
.end method
