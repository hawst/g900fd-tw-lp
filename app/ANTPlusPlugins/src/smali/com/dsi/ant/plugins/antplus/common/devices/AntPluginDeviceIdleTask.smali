.class public final Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "AntPluginDeviceIdleTask.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private deathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

.field private pluginAntDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

.field private taskCancelledLatch:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)V
    .locals 2
    .param p1, "pluginAntDevice"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->taskCancelledLatch:Ljava/util/concurrent/CountDownLatch;

    .line 43
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->pluginAntDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    .line 44
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask$1;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->deathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->pluginAntDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    return-object v0
.end method

.method public static createPluginDeviceExecutor(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .locals 4
    .param p0, "pluginAntDevice"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)V

    .line 27
    .local v1, "idleForwarder":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->clearChannelEventHandler()V

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->deathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

    invoke-direct {v0, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    .line 31
    .local v0, "executor":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 33
    return-object v0
.end method


# virtual methods
.method public doWork()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->pluginAntDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->checkChannelState(Z)V

    .line 84
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->enableMessageProcessing()V

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->taskCancelledLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted while idling"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "AntPluginAntDevice Default Processing"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 64
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->handleInterruptRequest(I)Z

    .line 65
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 1
    .param p1, "interruptingTaskRank"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->taskCancelledLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public initTask()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->taskCancelledLatch:Ljava/util/concurrent/CountDownLatch;

    .line 78
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->pluginAntDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->handleNewAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 72
    return-void
.end method
