.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusStrideSdmPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IpcDefines;
    }
.end annotation


# instance fields
.field mCalorieDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;

.field mComputationTimestampReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;

.field mDataLatencyReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;

.field mDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;

.field mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;

.field mInstantaneousSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;

.field mSensorStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;

.field mStrideCountReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 644
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 508
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 510
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 608
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;-><init>()V

    .line 640
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 657
    const-string v0, "ANT+ Plugin: Stride Based Speed and Distance"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 649
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 650
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.stridesdm.StrideSdmService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 651
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 27
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 663
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 773
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 667
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;

    if-eqz v1, :cond_0

    .line 670
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 671
    .local v21, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 672
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 673
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_instantaneousSpeed"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v24

    check-cast v24, Ljava/math/BigDecimal;

    .line 674
    .local v24, "instantaneousSpeed":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;

    move-object/from16 v0, v24

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;->onNewInstantaneousSpeed(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 680
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v21    # "b":Landroid/os/Bundle;
    .end local v24    # "instantaneousSpeed":Ljava/math/BigDecimal;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;

    if-eqz v1, :cond_0

    .line 683
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 684
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 685
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 686
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_instantaneousCadence"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v23

    check-cast v23, Ljava/math/BigDecimal;

    .line 687
    .local v23, "instantaneousCadence":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;

    move-object/from16 v0, v23

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;->onNewInstantaneousCadence(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 693
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v21    # "b":Landroid/os/Bundle;
    .end local v23    # "instantaneousCadence":Ljava/math/BigDecimal;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;

    if-eqz v1, :cond_0

    .line 696
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 697
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 698
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 699
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_cumulativeDistance"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v22

    check-cast v22, Ljava/math/BigDecimal;

    .line 700
    .local v22, "cumulativeDistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;

    move-object/from16 v0, v22

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;->onNewDistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 706
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v21    # "b":Landroid/os/Bundle;
    .end local v22    # "cumulativeDistance":Ljava/math/BigDecimal;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mStrideCountReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;

    if-eqz v1, :cond_0

    .line 709
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 710
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 711
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 712
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrides"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 713
    .local v5, "cumulativeStrides":J
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mStrideCountReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;->onNewStrideCount(JLjava/util/EnumSet;J)V

    goto/16 :goto_0

    .line 719
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "cumulativeStrides":J
    .end local v21    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mComputationTimestampReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;

    if-eqz v1, :cond_0

    .line 722
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 723
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 724
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 725
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_timestampOfLastComputation"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v25

    check-cast v25, Ljava/math/BigDecimal;

    .line 726
    .local v25, "timestampOfLastComputation":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mComputationTimestampReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;

    move-object/from16 v0, v25

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;->onNewComputationTimestamp(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 732
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v21    # "b":Landroid/os/Bundle;
    .end local v25    # "timestampOfLastComputation":Ljava/math/BigDecimal;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDataLatencyReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;

    if-eqz v1, :cond_0

    .line 735
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 736
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 737
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 738
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_updateLatency"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v26

    check-cast v26, Ljava/math/BigDecimal;

    .line 739
    .local v26, "updateLatency":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDataLatencyReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;

    move-object/from16 v0, v26

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;->onNewDataLatency(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 745
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v21    # "b":Landroid/os/Bundle;
    .end local v26    # "updateLatency":Ljava/math/BigDecimal;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mCalorieDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;

    if-eqz v1, :cond_0

    .line 748
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 749
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 750
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 751
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeCalories"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    .line 752
    .local v11, "cumulativeCalories":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mCalorieDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;

    move-wide v8, v2

    move-object v10, v4

    invoke-interface/range {v7 .. v12}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;->onNewCalorieData(JLjava/util/EnumSet;J)V

    goto/16 :goto_0

    .line 758
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v11    # "cumulativeCalories":J
    .end local v21    # "b":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mSensorStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;

    if-eqz v1, :cond_0

    .line 761
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v21

    .line 762
    .restart local v21    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 763
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 764
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_SensorLocation"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;

    move-result-object v17

    .line 765
    .local v17, "sensorLocation":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;
    const-string v1, "int_BatteryStatus"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    move-result-object v18

    .line 766
    .local v18, "batteryStatus":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    const-string v1, "int_SensorHealth"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;

    move-result-object v19

    .line 767
    .local v19, "sensorHealth":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;
    const-string v1, "int_UseState"

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;

    move-result-object v20

    .line 768
    .local v20, "useState":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mSensorStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;

    move-wide v14, v2

    move-object/from16 v16, v4

    invoke-interface/range {v13 .. v20}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;->onNewSensorStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;)V

    goto/16 :goto_0

    .line 663
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public subscribeCalorieDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;)V
    .locals 1
    .param p1, "CalorieDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;

    .prologue
    const/16 v0, 0xcf

    .line 900
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mCalorieDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ICalorieDataReceiver;

    .line 901
    if-eqz p1, :cond_0

    .line 903
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 909
    :goto_0
    return-void

    .line 907
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeComputationTimestampEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;)V
    .locals 1
    .param p1, "ComputationTimestampReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 862
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mComputationTimestampReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IComputationTimestampReceiver;

    .line 863
    if-eqz p1, :cond_0

    .line 865
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 871
    :goto_0
    return-void

    .line 869
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeDataLatencyEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;)V
    .locals 1
    .param p1, "DataLatencyReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;

    .prologue
    const/16 v0, 0xce

    .line 881
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDataLatencyReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDataLatencyReceiver;

    .line 882
    if-eqz p1, :cond_0

    .line 884
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 890
    :goto_0
    return-void

    .line 888
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeDistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;)V
    .locals 1
    .param p1, "DistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 824
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IDistanceReceiver;

    .line 825
    if-eqz p1, :cond_0

    .line 827
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 833
    :goto_0
    return-void

    .line 831
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeInstantaneousCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;)V
    .locals 1
    .param p1, "InstantaneousCadenceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;

    .prologue
    const/16 v0, 0xca

    .line 805
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousCadenceReceiver;

    .line 806
    if-eqz p1, :cond_0

    .line 808
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 814
    :goto_0
    return-void

    .line 812
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeInstantaneousSpeedEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;)V
    .locals 1
    .param p1, "InstantaneousSpeedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 786
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mInstantaneousSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IInstantaneousSpeedReceiver;

    .line 787
    if-eqz p1, :cond_0

    .line 789
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 795
    :goto_0
    return-void

    .line 793
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeSensorStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;)V
    .locals 1
    .param p1, "SensorStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;

    .prologue
    const/16 v0, 0xd0

    .line 919
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mSensorStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;

    .line 920
    if-eqz p1, :cond_0

    .line 922
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 928
    :goto_0
    return-void

    .line 926
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeStrideCountEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;)V
    .locals 1
    .param p1, "StrideCountReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 843
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->mStrideCountReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;

    .line 844
    if-eqz p1, :cond_0

    .line 846
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->subscribeToEvent(I)Z

    .line 852
    :goto_0
    return-void

    .line 850
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
