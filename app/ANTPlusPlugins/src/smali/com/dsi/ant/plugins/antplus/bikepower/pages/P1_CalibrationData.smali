.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P1_CalibrationData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$1;,
        Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;
    }
.end annotation


# static fields
.field private static final CTF_ZERO_OFFSET_ID:I = 0x1


# instance fields
.field private autoZeroStatus:I

.field private calEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

.field private zeroEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;)V
    .locals 2
    .param p1, "ctfOffsetReceiver"    # Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 24
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->calEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->zeroEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 26
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    .line 37
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

    .line 38
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 14
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 58
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x2

    aget-byte v10, v10, v11

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v10

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    move-result-object v4

    .line 62
    .local v4, "calibrationId":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;
    sget-object v10, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CalibrationId:[I

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->calEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->CAPABILITIES:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    if-eq v4, v10, :cond_2

    .line 95
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 96
    .local v2, "b":Landroid/os/Bundle;
    const-string v10, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v2, v10, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 97
    const-string v10, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v2, v10, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 99
    const/4 v3, 0x0

    .line 100
    .local v3, "calibrationData":Ljava/lang/Integer;
    const/4 v6, 0x0

    .line 101
    .local v6, "ctfOffset":Ljava/lang/Integer;
    const/4 v8, 0x0

    .line 103
    .local v8, "manufacturerSpecificData":[B
    sget-object v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->CTF_MESSAGE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    if-ne v4, v10, :cond_1

    .line 105
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x2

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v10

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    move-result-object v10

    sget-object v11, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->CTF_ZERO_OFFSET:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    if-ne v10, v11, :cond_6

    .line 108
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->CTF_ZERO_OFFSET:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    .line 117
    :cond_1
    :goto_1
    sget-object v10, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CalibrationId:[I

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_1

    .line 135
    :goto_2
    :pswitch_0
    const-string v10, "parcelable_CalibrationMessage"

    new-instance v11, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;

    invoke-direct {v11, v4, v3, v6, v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;Ljava/lang/Integer;Ljava/lang/Integer;[B)V

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 137
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->calEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v10, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 140
    .end local v2    # "b":Landroid/os/Bundle;
    .end local v3    # "calibrationData":Ljava/lang/Integer;
    .end local v6    # "ctfOffset":Ljava/lang/Integer;
    .end local v8    # "manufacturerSpecificData":[B
    :cond_2
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->zeroEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 142
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 143
    .restart local v2    # "b":Landroid/os/Bundle;
    const-string v10, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v2, v10, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v10, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v2, v10, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 144
    const-string v10, "int_autoZeroStatus"

    iget v11, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->zeroEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v10, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 148
    .end local v2    # "b":Landroid/os/Bundle;
    :cond_3
    return-void

    .line 67
    :pswitch_1
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x3

    aget-byte v10, v10, v11

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v10

    iput v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    goto/16 :goto_0

    .line 71
    :pswitch_2
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x3

    aget-byte v10, v10, v11

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    .line 72
    .local v5, "configField":I
    and-int/lit8 v10, v5, 0x1

    if-nez v10, :cond_4

    .line 73
    sget-object v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getIntValue()I

    move-result v10

    iput v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    goto/16 :goto_0

    .line 74
    :cond_4
    and-int/lit8 v10, v5, 0x2

    if-nez v10, :cond_5

    .line 75
    sget-object v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->OFF:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getIntValue()I

    move-result v10

    iput v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    goto/16 :goto_0

    .line 77
    :cond_5
    sget-object v10, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->ON:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getIntValue()I

    move-result v10

    iput v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->autoZeroStatus:I

    goto/16 :goto_0

    .line 80
    .end local v5    # "configField":I
    :pswitch_3
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x3

    aget-byte v10, v10, v11

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 81
    .local v7, "ctfPage":I
    const/4 v10, 0x1

    if-ne v10, v7, :cond_0

    .line 83
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x7

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v9

    .line 84
    .local v9, "rxCtfOffset":I
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

    invoke-interface {v10, v9}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;->onNewCtfOffsetReceived(I)V

    goto/16 :goto_0

    .line 112
    .end local v7    # "ctfPage":I
    .end local v9    # "rxCtfOffset":I
    .restart local v2    # "b":Landroid/os/Bundle;
    .restart local v3    # "calibrationData":Ljava/lang/Integer;
    .restart local v6    # "ctfOffset":Ljava/lang/Integer;
    .restart local v8    # "manufacturerSpecificData":[B
    :cond_6
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x2

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom3BeBytes([BI)I

    move-result v10

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;

    move-result-object v4

    goto/16 :goto_1

    .line 120
    :pswitch_4
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x7

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 121
    goto/16 :goto_2

    .line 124
    :pswitch_5
    const/4 v10, 0x6

    new-array v8, v10, [B

    .line 125
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x3

    const/4 v12, 0x0

    const/4 v13, 0x6

    invoke-static {v10, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_2

    .line 129
    :pswitch_6
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x7

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom2LeBytes([BI)S

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 130
    goto/16 :goto_2

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->zeroEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;->calEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 52
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
