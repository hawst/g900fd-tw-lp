.class public Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;
.super Ljava/lang/Object;
.source "P81_ProductInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "P81_ProductInformation_CachedDecoder"
.end annotation


# instance fields
.field public mainSoftwareRevision:I

.field public serialNumber:J

.field public supplementalSoftwareRevision:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->mainSoftwareRevision:I

    .line 48
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->supplementalSoftwareRevision:I

    .line 50
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->serialNumber:J

    .line 51
    return-void
.end method
