.class final Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;
.super Ljava/lang/Object;
.source "DialogBuilder_ProximityPicker.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker;->build(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$relativeLayout_ProxValueControls:Landroid/widget/RelativeLayout;

.field final synthetic val$seekbar_ProxValue:Landroid/widget/SeekBar;

.field final synthetic val$textView_ProxValueDisplay:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/RelativeLayout;Landroid/widget/SeekBar;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$relativeLayout_ProxValueControls:Landroid/widget/RelativeLayout;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$seekbar_ProxValue:Landroid/widget/SeekBar;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$textView_ProxValueDisplay:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$relativeLayout_ProxValueControls:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$relativeLayout_ProxValueControls:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    if-nez p2, :cond_1

    .line 49
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$seekbar_ProxValue:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 50
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$textView_ProxValueDisplay:Landroid/widget/TextView;

    const-string v2, "Threshold Rating: OFF"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    :goto_1
    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;->val$seekbar_ProxValue:Landroid/widget/SeekBar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1
.end method
