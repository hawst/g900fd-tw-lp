.class public Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P81_ProductInformation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;
    }
.end annotation


# instance fields
.field private decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

.field private prodEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 54
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    .line 56
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0x65

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->prodEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 73
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->prodEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    invoke-virtual {v1, p5}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 81
    const-string v1, "int_softwareRevision"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->mainSoftwareRevision:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v1, "int_supplementaryRevision"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->supplementalSoftwareRevision:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    const-string v1, "long_serialNumber"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    iget-wide v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->serialNumber:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 85
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->prodEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;->prodEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
