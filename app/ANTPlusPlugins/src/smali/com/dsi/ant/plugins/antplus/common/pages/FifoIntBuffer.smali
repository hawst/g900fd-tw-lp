.class public Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
.super Ljava/lang/Object;
.source "FifoIntBuffer.java"


# instance fields
.field private mCapacity:I

.field private mIntBuffer:Ljava/nio/IntBuffer;

.field private wasFilled:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->wasFilled:Z

    .line 13
    invoke-static {p1}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    .line 14
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mCapacity:I

    .line 15
    return-void
.end method


# virtual methods
.method public getMean()D
    .locals 4

    .prologue
    .line 46
    const/4 v1, 0x0

    .line 48
    .local v1, "sum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v2}, Ljava/nio/IntBuffer;->capacity()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v2}, Ljava/nio/IntBuffer;->capacity()I

    move-result v2

    div-int v2, v1, v2

    int-to-double v2, v2

    return-wide v2
.end method

.method public getStandardDeviation()D
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getVariance()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public getVariance()D
    .locals 9

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getMean()D

    move-result-wide v1

    .line 59
    .local v1, "mean":D
    const-wide/16 v3, 0x0

    .line 60
    .local v3, "temp":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v5}, Ljava/nio/IntBuffer;->capacity()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 62
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    int-to-double v5, v5

    sub-double v5, v1, v5

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    int-to-double v7, v7

    sub-double v7, v1, v7

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v5}, Ljava/nio/IntBuffer;->capacity()I

    move-result v5

    int-to-double v5, v5

    div-double v5, v3, v5

    return-wide v5
.end method

.method public isContentsWithinRange(I)Z
    .locals 8
    .param p1, "limit"    # I

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getMean()D

    move-result-wide v1

    .line 77
    .local v1, "mean":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v4}, Ljava/nio/IntBuffer;->capacity()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 79
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v3

    .line 80
    .local v3, "value":I
    int-to-double v4, v3

    int-to-double v6, p1

    add-double/2addr v6, v1

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_0

    int-to-double v4, v3

    int-to-double v6, p1

    sub-double v6, v1, v6

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 81
    :cond_0
    const/4 v4, 0x0

    .line 84
    .end local v3    # "value":I
    :goto_1
    return v4

    .line 77
    .restart local v3    # "value":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    .end local v3    # "value":I
    :cond_2
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public isValueWithinRange(II)Z
    .locals 7
    .param p1, "i"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v0, 0x0

    .line 89
    int-to-double v1, p1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getMean()D

    move-result-wide v3

    int-to-double v5, p2

    sub-double/2addr v3, v5

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    int-to-double v1, p1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getMean()D

    move-result-wide v3

    int-to-double v5, p2

    add-double/2addr v3, v5

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_0

    .line 95
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public put(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    .line 23
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->wasFilled:Z

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mCapacity:I

    invoke-static {v0}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->mIntBuffer:Ljava/nio/IntBuffer;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->wasFilled:Z

    .line 42
    return-void
.end method

.method public wasFilled()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->wasFilled:Z

    return v0
.end method
