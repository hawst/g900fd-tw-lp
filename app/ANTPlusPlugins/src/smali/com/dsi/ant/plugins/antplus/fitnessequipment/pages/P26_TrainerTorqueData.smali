.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P26_TrainerTorqueData.java"


# static fields
.field private static final ACCUMULATOR_TIMEOUT:I = 0x2ee0


# instance fields
.field private accEvtCnt:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private accTorq:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private accWheelPeriod:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private accWheelTicks:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private trnrTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V
    .locals 5
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;
    .param p2, "powerDecoder"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .prologue
    const v4, 0xffff

    const/16 v3, 0xff

    const/16 v2, 0x2ee0

    .line 33
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->trnrTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-direct {v0, v3, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accEvtCnt:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-direct {v0, v3, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelTicks:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-direct {v0, v4, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelPeriod:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-direct {v0, v4, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accTorq:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 34
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 35
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 36
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 18
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 58
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v15

    .line 59
    .local v15, "updateEventCount":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accEvtCnt:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v15, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 61
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v17

    .line 62
    .local v17, "wheelTicks":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelTicks:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move/from16 v0, v17

    move-wide/from16 v1, p1

    invoke-virtual {v3, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 64
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v16

    .line 65
    .local v16, "wheelPeriod":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelPeriod:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v3, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 67
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v14

    .line 68
    .local v14, "torq":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accTorq:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v14, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 70
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v8

    .line 71
    .local v8, "pageNumber":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accEvtCnt:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelTicks:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelPeriod:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accTorq:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v12}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodeTorquePage(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 73
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->trnrTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v3

    if-nez v3, :cond_0

    .line 91
    :goto_0
    return-void

    .line 76
    :cond_0
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 77
    .local v13, "b":Landroid/os/Bundle;
    const-string v3, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v13, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    const-string v3, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v13, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    const-string v3, "long_updateEventCount"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accEvtCnt:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v4

    invoke-virtual {v13, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 82
    const-string v3, "long_accumulatedWheelTicks"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelTicks:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v4

    invoke-virtual {v13, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 84
    const-string v3, "decimal_accumulatedWheelPeriod"

    new-instance v4, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accWheelPeriod:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x800

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v6, 0xb

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    const-string v3, "decimal_accumulatedTorque"

    new-instance v4, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->accTorq:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x20

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v6, 0x5

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->trnrTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3, v13}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;->trnrTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
