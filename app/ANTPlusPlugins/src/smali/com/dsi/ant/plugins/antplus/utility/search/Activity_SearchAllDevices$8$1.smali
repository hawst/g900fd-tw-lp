.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChanged(II)V
    .locals 8
    .param p1, "oldValue"    # I
    .param p2, "newValue"    # I

    .prologue
    .line 681
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iput p2, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    .line 687
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v5, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 692
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v4, :cond_0

    .line 693
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto :goto_0

    .line 726
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 727
    :catch_0
    move-exception v0

    .line 729
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InterruptedException trying to restart scan task"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 731
    .local v1, "failMsg":Landroid/os/Message;
    const/16 v4, -0x64

    iput v4, v1, Landroid/os/Message;->what:I

    .line 732
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V
    invoke-static {v4, v1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/os/Message;)V

    .line 733
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 738
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "failMsg":Landroid/os/Message;
    :goto_1
    return-void

    .line 697
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 699
    .restart local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v4, :cond_2

    .line 700
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v6, 0x7d0

    invoke-virtual {v4, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->cancelCurrentTask(I)Z

    goto :goto_2

    .line 704
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_3
    if-eqz p2, :cond_5

    if-lt p2, p1, :cond_4

    if-nez p1, :cond_5

    .line 706
    :cond_4
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 707
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_devNameList:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->clear()V

    .line 711
    :cond_5
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v6, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 713
    :try_start_4
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 714
    .restart local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget v7, v7, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    invoke-virtual {v4, v7}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;->setProximityThreshold(I)V

    goto :goto_3

    .line 715
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :catchall_1
    move-exception v4

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_6
    :try_start_6
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 718
    :try_start_7
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-boolean v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    if-nez v4, :cond_8

    .line 720
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 722
    .restart local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v4, :cond_7

    .line 723
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v6, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    invoke-virtual {v4, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto :goto_4

    .line 726
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_8
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 737
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$8;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->refreshProximityUi()V
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$900(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    goto/16 :goto_1
.end method
