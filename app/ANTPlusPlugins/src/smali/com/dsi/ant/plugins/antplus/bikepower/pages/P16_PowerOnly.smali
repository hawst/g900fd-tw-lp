.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P16_PowerOnly.java"


# static fields
.field private static MAX_ACC_TIMEOUT:I


# instance fields
.field private balEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private powerAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0x2ee0

    sput v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->MAX_ACC_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V
    .locals 3
    .param p1, "evtCntAcc"    # Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;
    .param p2, "decoder"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 24
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->balEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const v1, 0xffff

    sget v2, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->powerAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 32
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 33
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 34
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 15
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 58
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v14

    .line 59
    .local v14, "receivedEventCount":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v14, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 62
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v13

    .line 63
    .local v13, "receivedAccumPower":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->powerAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v13, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 65
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->powerAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    invoke-virtual/range {v2 .. v8}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodePwrOnlyPage(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 68
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    const/16 v7, 0x10

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v9, p5

    invoke-virtual/range {v2 .. v9}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodeInstantaneousCadence(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 70
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 73
    .local v10, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 74
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 76
    const-string v2, "long_powerOnlyUpdateEventCount"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v10, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    const-string v2, "long_accumulatedPower"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->powerAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v10, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 81
    const-string v2, "int_instantaneousPower"

    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 87
    .end local v10    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->balEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 91
    .local v11, "pedalPower":I
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 92
    .restart local v10    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 93
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 95
    const/16 v2, 0xff

    if-eq v11, v2, :cond_5

    .line 98
    and-int/lit8 v12, v11, 0x7f

    .line 100
    .local v12, "pedalPowerPercentage":I
    if-ltz v12, :cond_3

    const/16 v2, 0x64

    if-gt v12, v2, :cond_2

    .line 102
    :goto_0
    const-string v2, "int_pedalPowerPercentage"

    invoke-virtual {v10, v2, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v3, "bool_rightPedalIndicator"

    and-int/lit16 v2, v11, 0x80

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v10, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    .end local v12    # "pedalPowerPercentage":I
    :goto_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->balEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 116
    .end local v10    # "b":Landroid/os/Bundle;
    .end local v11    # "pedalPower":I
    :cond_1
    return-void

    .line 100
    .restart local v10    # "b":Landroid/os/Bundle;
    .restart local v11    # "pedalPower":I
    .restart local v12    # "pedalPowerPercentage":I
    :cond_2
    const/4 v12, -0x1

    goto :goto_0

    :cond_3
    const/4 v12, -0x1

    goto :goto_0

    .line 105
    :cond_4
    const/4 v2, 0x1

    goto :goto_1

    .line 110
    .end local v12    # "pedalPowerPercentage":I
    :cond_5
    const-string v2, "int_pedalPowerPercentage"

    const/4 v3, -0x1

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    const-string v2, "bool_rightPedalIndicator"

    const/4 v3, 0x0

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 41
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;->balEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
