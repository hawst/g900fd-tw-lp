.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P0_SpdCadCombinedDefault.java"


# instance fields
.field private cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

.field private spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 15
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    .line 16
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->decodeCadence(JJII)V

    .line 48
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->decodeSpeedAndDistance(JJII)V

    .line 51
    return-void
.end method

.method public getCadenceEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->getEventList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 22
    .local v0, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 23
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSpeedEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->getEventList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->cadDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->onDropToSearch()V

    .line 57
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->spdDistDecoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->onDropToSearch()V

    .line 58
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 59
    return-void
.end method
