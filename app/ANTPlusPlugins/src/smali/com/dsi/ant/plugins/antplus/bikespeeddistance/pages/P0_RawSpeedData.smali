.class public Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P0_RawSpeedData.java"


# instance fields
.field private decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 15
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->decodeSpeedAndDistance(JJII)V

    .line 35
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->getEventList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/SpeedAndDistanceDecoder;->onDropToSearch()V

    .line 41
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 42
    return-void
.end method
