.class public abstract Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;
.source "AntPluginAntPlusReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field pageIndex:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 41
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->initPageIndex()V

    .line 43
    :cond_0
    return-void
.end method

.method private initPageIndex()V
    .locals 8

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getPageList()Ljava/util/List;

    move-result-object v3

    .line 68
    .local v3, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v5, Landroid/util/SparseArray;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    .line 69
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    .line 71
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getPageNumbers()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 73
    .local v4, "pn":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v5

    if-ltz v5, :cond_1

    .line 74
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Page number collision on page number "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 75
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 78
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "pn":Ljava/lang/Integer;
    :cond_2
    return-void
.end method


# virtual methods
.method public checkChannelState(Z)V
    .locals 5
    .param p1, "isInit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->checkChannelState(Z)V

    .line 129
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v3, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    .line 132
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    .line 133
    .local v1, "status":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver$1;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 158
    :goto_0
    return-void

    .line 136
    :pswitch_0
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    .end local v1    # "status":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE in checkChannelState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 156
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2

    .line 139
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v1    # "status":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :pswitch_1
    const/4 v2, 0x2

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V

    goto :goto_0

    .line 142
    :pswitch_2
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V

    .line 143
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->openChannel()Z

    goto :goto_0

    .line 146
    :pswitch_3
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    const-string v3, "Can\'t recover from channel in unassigned state."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 148
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getAllCommonPages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    const/4 v1, 0x0

    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P240_P255_ManufacturerSpecificData;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P240_P255_ManufacturerSpecificData;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;-><init>()V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->initPageIndex()V

    .line 55
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 57
    .local v0, "eventSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getEventList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 62
    :cond_1
    return-object v0
.end method

.method public abstract getPageList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation
.end method

.method public handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 182
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    .line 184
    .local v6, "pageNum":I
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    .line 186
    .local v0, "p":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getEstimatedTimestamp()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getEventFlags()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 190
    :goto_0
    return-void

    .line 189
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown page received, page "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v3, 0x3

    .line 83
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 120
    :goto_0
    return-void

    .line 87
    :pswitch_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getCurrentState()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 88
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V

    .line 89
    :cond_0
    invoke-virtual {p0, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 92
    :pswitch_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 95
    :pswitch_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    const-string v2, "Channel search timeout"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :pswitch_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    const-string v2, "Channel closed, attempting reopen..."

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->openChannel()Z

    goto :goto_0

    .line 107
    :pswitch_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 92
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public openChannel()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 164
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 165
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->setCurrentState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 166
    const/4 v1, 0x1

    .line 176
    :goto_0
    return v1

    .line 167
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException trying to reopen channel"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->onChannelDeath()V

    goto :goto_0

    .line 172
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE reopening channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0
.end method
