.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
.super Ljava/lang/Enum;
.source "DeviceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum BIKE_POWER:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum BLOOD_PRESSURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum CONTROLLABLE_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum ENVIRONMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum FITNESS_EQUIPMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum GEOCACHE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum HEARTRATE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum STRIDE_SDM:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field public static final enum WEIGHT_SCALE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;


# instance fields
.field private final intValue:I

.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xb

    .line 13
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "BIKE_POWER"

    const-string v2, "Bike Power Sensors"

    invoke-direct {v0, v1, v6, v5, v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_POWER:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 18
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "CONTROLLABLE_DEVICE"

    const/16 v2, 0x10

    const-string v3, "Controls"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->CONTROLLABLE_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "FITNESS_EQUIPMENT"

    const/16 v2, 0x11

    const-string v3, "Fitness Equipment Devices"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->FITNESS_EQUIPMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "BLOOD_PRESSURE"

    const/16 v2, 0x12

    const-string v3, "Blood Pressure Monitors"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BLOOD_PRESSURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 33
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "GEOCACHE"

    const/4 v2, 0x4

    const/16 v3, 0x13

    const-string v4, "Geocache Transmitters"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->GEOCACHE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 38
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "ENVIRONMENT"

    const/4 v2, 0x5

    const/16 v3, 0x19

    const-string v4, "Environment Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->ENVIRONMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 43
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "WEIGHT_SCALE"

    const/4 v2, 0x6

    const/16 v3, 0x77

    const-string v4, "Weight Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->WEIGHT_SCALE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 48
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "HEARTRATE"

    const/4 v2, 0x7

    const/16 v3, 0x78

    const-string v4, "Heart Rate Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->HEARTRATE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 53
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "BIKE_SPDCAD"

    const/16 v2, 0x8

    const/16 v3, 0x79

    const-string v4, "Bike Speed and Cadence Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 58
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "BIKE_CADENCE"

    const/16 v2, 0x9

    const/16 v3, 0x7a

    const-string v4, "Bike Cadence Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 63
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "BIKE_SPD"

    const/16 v2, 0xa

    const/16 v3, 0x7b

    const-string v4, "Bike Speed Sensors"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 68
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "STRIDE_SDM"

    const/16 v2, 0x7c

    const-string v3, "Stride-Based Speed and Distance Sensors"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->STRIDE_SDM:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 73
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, "Unknown"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 8
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_POWER:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->CONTROLLABLE_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->FITNESS_EQUIPMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BLOOD_PRESSURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->GEOCACHE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->ENVIRONMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->WEIGHT_SCALE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->HEARTRATE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->STRIDE_SDM:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v1, v0, v5

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "intValue"    # I
    .param p4, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->intValue:I

    .line 83
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->name:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .locals 5
    .param p0, "intValue"    # I

    .prologue
    .line 102
    and-int/lit16 p0, p0, -0x81

    .line 104
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 106
    .local v1, "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    iget v4, v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->intValue:I

    if-ne v4, p0, :cond_0

    .line 109
    .end local v1    # "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    :goto_1
    return-object v1

    .line 104
    .restart local v1    # "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 109
    .end local v1    # "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->intValue:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->name:Ljava/lang/String;

    return-object v0
.end method
