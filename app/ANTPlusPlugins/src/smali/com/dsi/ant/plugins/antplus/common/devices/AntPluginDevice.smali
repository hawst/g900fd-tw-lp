.class public abstract Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.super Ljava/lang/Object;
.source "AntPluginDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final connectedClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private currentStateCode:I

.field public deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field private eventList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field public mIsOpen:Z

.field public token_ClientMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;I)V
    .locals 6
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "initialState"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    .line 37
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->token_ClientMap:Ljava/util/HashMap;

    .line 38
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    .line 39
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->mIsOpen:Z

    .line 53
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 54
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->mIsOpen:Z

    .line 55
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->currentStateCode:I

    .line 57
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->getEventSet()Ljava/util/Set;

    move-result-object v0

    .line 58
    .local v0, "eligibleEvents":Ljava/util/Set;, "Ljava/util/Set<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 60
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    iget-object v5, v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 61
    .local v1, "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    if-nez v1, :cond_0

    .line 63
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .restart local v1    # "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    iget-object v5, v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 66
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    .end local v1    # "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    :cond_1
    return-void
.end method

.method private unsubscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 6
    .param p1, "eventID"    # I
    .param p2, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 166
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 167
    .local v0, "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    if-nez v0, :cond_0

    .line 169
    const/4 v3, 0x0

    .line 179
    :goto_0
    return v3

    .line 173
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 175
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v3, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->unsubscribeFromEvent(Ljava/util/UUID;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 176
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App requesting to unsubscribe without being subscribed. Event ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 179
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 6
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    const/4 v5, -0x3

    const/4 v4, 0x0

    .line 103
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 104
    .local v0, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 105
    .local v2, "response":Landroid/os/Message;
    iget v3, p2, Landroid/os/Message;->what:I

    iput v3, v2, Landroid/os/Message;->what:I

    .line 106
    iget v3, p2, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 137
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received unhandled command from PCC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const v3, -0x5f5e0ff

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 143
    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 144
    return-void

    .line 111
    :pswitch_0
    iget v1, p2, Landroid/os/Message;->arg1:I

    .line 112
    .local v1, "eventID":I
    invoke-virtual {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->subscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 115
    :cond_0
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 121
    .end local v1    # "eventID":I
    :pswitch_1
    iget v1, p2, Landroid/os/Message;->arg1:I

    .line 122
    .restart local v1    # "eventID":I
    invoke-direct {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->unsubscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 125
    :cond_1
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 130
    .end local v1    # "eventID":I
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->removeClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 131
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    monitor-enter v1

    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->token_ClientMap:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' somehow double subscribed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    monitor-exit v1

    .line 192
    const/4 v0, 0x1

    return v0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public closeDevice()V
    .locals 1

    .prologue
    .line 254
    const/16 v0, -0x64

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->setCurrentState(I)V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->mIsOpen:Z

    .line 258
    return-void
.end method

.method public getCurrentState()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->currentStateCode:I

    return v0
.end method

.method public abstract getEventSet()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation
.end method

.method public hasAccess(Ljava/lang/String;)Z
    .locals 4
    .param p1, "appNamePkg"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    monitor-enter v3

    .line 238
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 240
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    const/4 v2, 0x1

    monitor-exit v3

    .line 246
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    :goto_0
    return v2

    .line 245
    :cond_1
    monitor-exit v3

    .line 246
    const/4 v2, 0x0

    goto :goto_0

    .line 245
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isDeviceClosed()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->mIsOpen:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected removeClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 8
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 199
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 201
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 202
    .local v0, "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 203
    .local v3, "j":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->unsubscribeFromEvent(Ljava/util/UUID;)Z

    goto :goto_1

    .line 199
    .end local v3    # "j":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 206
    .end local v0    # "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    monitor-enter v5

    .line 208
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 209
    sget-object v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Client \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' somehow requesting remove without being on list of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v7, v7, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->token_ClientMap:Ljava/util/HashMap;

    iget-object v6, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 215
    :cond_3
    return-void

    .line 211
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 264
    :try_start_0
    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    const/4 v1, 0x1

    .line 272
    :goto_0
    return v1

    .line 266
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Remote Exception sending client message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCurrentState(I)V
    .locals 6
    .param p1, "newState"    # I

    .prologue
    .line 79
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->currentStateCode:I

    if-ne p1, v3, :cond_1

    .line 81
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring plugin attempt to set current state to existing state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    return-void

    .line 85
    :cond_1
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->currentStateCode:I

    .line 86
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 88
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 89
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x3

    iput v3, v2, Landroid/os/Message;->what:I

    .line 90
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 91
    invoke-virtual {p0, v0, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public subscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 6
    .param p1, "eventID"    # I
    .param p2, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 148
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->eventList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 149
    .local v0, "eventsWithSameId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    if-nez v0, :cond_0

    .line 151
    const/4 v3, 0x0

    .line 160
    :goto_0
    return v3

    .line 155
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 157
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v3, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v4, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 158
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App attempting to subscribe to event twice. Event ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 160
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method
