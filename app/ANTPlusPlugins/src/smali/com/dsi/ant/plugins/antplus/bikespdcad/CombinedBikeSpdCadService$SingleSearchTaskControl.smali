.class Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
.super Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;
.source "CombinedBikeSpdCadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SingleSearchTaskControl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V
    .locals 0
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "resultSentSemaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    .line 528
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V

    .line 529
    return-void
.end method


# virtual methods
.method public beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
    .locals 3
    .param p1, "task"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .prologue
    .line 542
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 543
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 547
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 553
    :goto_0
    return-void

    .line 548
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Plugin search by deviceNumber failed: executor failed to start task"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/4 v1, -0x4

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V

    goto :goto_0
.end method

.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 8
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 558
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->resultSentLock:Ljava/lang/Object;

    monitor-enter v4

    .line 560
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v3

    if-nez v3, :cond_0

    .line 561
    monitor-exit v4

    .line 626
    :goto_0
    return-void

    .line 563
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 621
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Plugin search by deviceNumber search failed internally with search result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    const/4 v3, -0x4

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V

    .line 625
    :cond_1
    :goto_1
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 567
    :sswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 568
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_2

    .line 570
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Plugin search by deviceNumber failed to shutdown executor cleanly"

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const/4 v3, -0x4

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V

    goto :goto_1

    .line 576
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-boolean v3, v3, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    if-nez v3, :cond_3

    .line 578
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-object v5, v3, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 581
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    .line 582
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 586
    :cond_3
    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v2

    .line 587
    .local v2, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v2, :cond_4

    .line 589
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 596
    :goto_2
    if-nez v2, :cond_5

    .line 599
    const/4 v3, -0x4

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 582
    .end local v2    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :catchall_1
    move-exception v3

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 593
    .restart local v2    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    const/4 v6, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v3, v5, v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$100(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v1

    .line 594
    .local v1, "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {v3, v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v2

    goto :goto_2

    .line 603
    .end local v1    # "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :cond_5
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_6

    .line 605
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Could not acquire resultSent semaphore before sending result"

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const/4 v3, -0x4

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V

    goto :goto_1

    .line 610
    :cond_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v2, v6, v7}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 611
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_1

    .line 616
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v2    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :sswitch_1
    const/4 v3, -0x7

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->handleSearchFailure(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 563
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method

.method public sendFailure(I)V
    .locals 3
    .param p1, "resultToSend"    # I

    .prologue
    .line 534
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 535
    .local v0, "response":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 536
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v2, v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 537
    return-void
.end method
