.class public Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;
.super Lcom/dsi/ant/plugins/antplus/stridesdm/pages/CommonSpeedData;
.source "P1_MainData.java"


# instance fields
.field private ctimeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private latencyEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private strideEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/CommonSpeedData;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->ctimeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 34
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xce

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->latencyEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 37
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const v1, 0xc7ff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 39
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const/16 v1, 0xfff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 65
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x2

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x3

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    mul-int/lit16 v6, v6, 0xc8

    add-int v3, v5, v6

    .line 70
    .local v3, "receivedTime_200ths_s":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v5, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 73
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x5

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x4

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    mul-int/lit8 v6, v6, 0x10

    add-int v1, v5, v6

    .line 78
    .local v1, "receivedDistance_16ths_m":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v5, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 81
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    .line 83
    .local v2, "receivedStrides":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v5, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 85
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->ctimeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 89
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 90
    const-string v5, "decimal_timestampOfLastComputation"

    new-instance v6, Ljava/math/BigDecimal;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v7, Ljava/math/BigDecimal;

    const/16 v8, 0xc8

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x3

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 96
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->ctimeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 99
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 103
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 104
    const-string v5, "decimal_cumulativeDistance"

    new-instance v6, Ljava/math/BigDecimal;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v7, Ljava/math/BigDecimal;

    const/16 v8, 0x10

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 110
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 113
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 117
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 118
    const-string v5, "long_cumulativeStrides"

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 122
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 125
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->latencyEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 127
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 128
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 129
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 131
    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/16 v7, 0x8

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0x20

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v4

    .line 134
    .local v4, "updateLatency":Ljava/math/BigDecimal;
    const-string v5, "decimal_updateLatency"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 138
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->latencyEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 141
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v4    # "updateLatency":Ljava/math/BigDecimal;
    :cond_3
    invoke-super/range {p0 .. p5}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/CommonSpeedData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 142
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->ctimeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->latencyEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/CommonSpeedData;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 52
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 58
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 149
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 150
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P1_MainData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 151
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/CommonSpeedData;->onDropToSearch()V

    .line 152
    return-void
.end method
