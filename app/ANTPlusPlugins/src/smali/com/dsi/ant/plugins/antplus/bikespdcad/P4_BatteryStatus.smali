.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P4_BatteryStatus.java"


# instance fields
.field private batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 20
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    .line 37
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v3

    if-nez v3, :cond_0

    .line 59
    :goto_0
    return-void

    .line 40
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    .local v0, "b":Landroid/os/Bundle;
    const-string v3, "long_EstTimestamp"

    invoke-virtual {v0, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 42
    const-string v3, "long_EventFlags"

    invoke-virtual {v0, v3, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 46
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    aget-byte v3, v3, v7

    and-int/lit8 v3, v3, 0xf

    const/16 v4, 0xf

    if-eq v3, v4, :cond_1

    .line 47
    new-instance v3, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v7

    and-int/lit8 v4, v4, 0xf

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v4, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x100

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v8, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v2

    .line 53
    .local v2, "batteryVoltage":Ljava/math/BigDecimal;
    :goto_1
    const-string v3, "decimal_batteryVoltage"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 55
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    aget-byte v3, v3, v7

    and-int/lit8 v3, v3, 0x70

    shr-int/lit8 v1, v3, 0x4

    .line 56
    .local v1, "batteryStatusCode":I
    const-string v3, "int_batteryStatus"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0

    .line 51
    .end local v1    # "batteryStatusCode":I
    .end local v2    # "batteryVoltage":Ljava/math/BigDecimal;
    :cond_1
    new-instance v2, Ljava/math/BigDecimal;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(I)V

    .restart local v2    # "batteryVoltage":Ljava/math/BigDecimal;
    goto :goto_1
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
