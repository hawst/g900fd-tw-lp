.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostSetTimeChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask$1;
    }
.end annotation


# static fields
.field static final BEACON_BUSY:I = 0x3

.field private static final BEACON_ID:B = 0x43t

.field static final BEACON_TRANS:I = 0x2

.field static final BEACON_UNKNOWN:I = -0x1

.field static final COMMAND_PIPE_INDEX:I = 0xfffe

.field static final COMMAND_RESPONSE_ID:B = 0x44t

.field static final CONTINUE_UPLOAD:J = -0x1L

.field static final PING_REQUEST_ID:B = 0x5t

.field private static final TAG:Ljava/lang/String;

.field static final TIME_COMMAND:I = 0x3

.field static final TIME_FORMAT_TIME_ZONE:B = -0x80t

.field static final TIME_MESSAGE_SIZE:I = 0x10

.field static final UPLOAD_DATA_ID:B = 0xct

.field static final UPLOAD_REQUEST_ID:B = 0xat

.field static final UPLOAD_RESPONSE_ID:B = -0x76t

.field static final UPLOAD_RESPONSE_NOT_ENOUGH_SPACE:I = 0x3

.field static final UPLOAD_RESPONSE_NOT_EXIST:I = 0x1

.field static final UPLOAD_RESPONSE_NOT_READY:I = 0x5

.field static final UPLOAD_RESPONSE_NOT_WRITABLE:I = 0x2

.field static final UPLOAD_RESPONSE_OK:I = 0x0

.field static final UPLOAD_RESPONSE_REQUEST_INVALID:I = 0x4

.field static final pingCommand:[B


# instance fields
.field private beaconState:I

.field private burstResponse:[B

.field private burstRx:Ljava/io/ByteArrayOutputStream;

.field private busyCount:I

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

.field private isCommandSent:Z

.field private mNewTime:J

.field private mSequenceNumber:I

.field private mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field private mTimeZoneOffset:B

.field private msgRetries:I

.field private noResponseCount:I

.field private transferInProgress:Z

.field private uploadCommand:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    .line 54
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->pingCommand:[B

    return-void

    :array_0
    .array-data 1
        0x44t
        0x5t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;IJB)V
    .locals 2
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "sequenceNumber"    # I
    .param p3, "newTime"    # J
    .param p5, "timeZoneOffset"    # B

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 56
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 64
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->beaconState:I

    .line 69
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    .line 71
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    .line 72
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 73
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    .line 74
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    .line 75
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    .line 80
    iput-wide p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mNewTime:J

    .line 81
    iput-byte p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mTimeZoneOffset:B

    .line 82
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mSequenceNumber:I

    .line 83
    return-void
.end method

.method private processUploadRequestResponse()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 446
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    .line 447
    .local v0, "response":[B
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    .line 449
    array-length v3, v0

    const/16 v4, 0x10

    if-ge v3, v4, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v2

    .line 452
    :cond_1
    const/16 v3, 0xa

    aget-byte v3, v0, v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    .line 454
    .local v1, "responseCode":I
    if-nez v1, :cond_0

    .line 455
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    .line 280
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_9

    .line 286
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 296
    .local v0, "channelState":Lcom/dsi/ant/message/ChannelState;
    :try_start_1
    sget-object v3, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v3, :cond_0

    .line 299
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed: Connection lost"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 301
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 302
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 442
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :goto_1
    return-void

    .line 288
    :catch_0
    move-exception v1

    .line 290
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE occurred requesting status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 292
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 293
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 436
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v1

    .line 438
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Interrupted waiting for result"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 440
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 307
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_0
    :try_start_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Wait for transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const/4 v3, -0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->beaconState:I

    .line 309
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 310
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->enableMessageProcessing()V

    .line 311
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0xa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 313
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->beaconState:I

    if-eq v3, v9, :cond_2

    .line 315
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Timed out waiting for transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 322
    :cond_2
    const/16 v3, 0x10

    new-array v3, v3, [B

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    .line 324
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/4 v4, 0x2

    const v5, 0xfffe

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 325
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/4 v4, 0x4

    const-wide/16 v5, 0x10

    invoke-static {v3, v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 327
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    .line 328
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    .line 329
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 330
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 331
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->enableMessageProcessing()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 335
    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 341
    :goto_3
    :try_start_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 343
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    if-nez v3, :cond_3

    .line 345
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed: Tx retries exceeded"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 336
    :catch_2
    move-exception v1

    .line 339
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception sending burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 350
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    if-nez v3, :cond_4

    .line 352
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Wait for upload response"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    .line 354
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    .line 355
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 356
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 357
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->enableMessageProcessing()V

    .line 358
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 362
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    if-eqz v3, :cond_1

    .line 366
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->processUploadRequestResponse()Z

    move-result v3

    if-nez v3, :cond_5

    .line 368
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_1

    .line 374
    :cond_5
    const/16 v3, 0x20

    new-array v3, v3, [B

    fill-array-data v3, :array_1

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    .line 378
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/16 v4, 0xb

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mSequenceNumber:I

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 379
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/16 v4, 0xc

    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mNewTime:J

    invoke-static {v3, v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 380
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/16 v4, 0x15

    iget-byte v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->mTimeZoneOffset:B

    aput-byte v5, v3, v4

    .line 381
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/16 v5, 0x8

    const/16 v6, 0x10

    invoke-virtual {v3, v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->update([BII)V

    .line 382
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    const/16 v4, 0x1e

    const-wide/32 v5, 0xffff

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v7

    and-long/2addr v5, v7

    long-to-int v5, v5

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 384
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    .line 385
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    .line 386
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 387
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 388
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->enableMessageProcessing()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    .line 392
    :try_start_5
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_5
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 398
    :goto_4
    :try_start_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 400
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    if-nez v3, :cond_6

    .line 402
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed: Tx retries exceeded"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 393
    :catch_3
    move-exception v1

    .line 396
    .restart local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception sending burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 407
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    if-nez v3, :cond_7

    .line 409
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Wait for upload response"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    .line 411
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    .line 412
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 413
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 414
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->enableMessageProcessing()V

    .line 415
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 419
    :cond_7
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    if-eqz v3, :cond_1

    .line 423
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->processUploadRequestResponse()Z

    move-result v3

    if-nez v3, :cond_8

    .line 425
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_1

    .line 429
    :cond_8
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_1

    .line 434
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_9
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_1

    .line 322
    :array_0
    .array-data 1
        0x44t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 374
    :array_1
    .array-data 1
        0x44t
        0xct
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    const-string v0, "ANT-FS Host Set Time Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 88
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x43

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 99
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 103
    :pswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v4, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v4, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 106
    :pswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timeout occured"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 244
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_8

    .line 250
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_IN_PROGRESS error sending burst msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 113
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 267
    :catch_1
    move-exception v1

    .line 269
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException receiving burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 271
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 116
    .end local v1    # "e":Ljava/io/IOException;
    :pswitch_3
    const/4 v3, 0x0

    :try_start_2
    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    .line 117
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 118
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    if-nez v3, :cond_0

    .line 122
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    .line 123
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 124
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 131
    :pswitch_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    goto :goto_0

    .line 134
    :pswitch_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Transfer Rx fail"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 136
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 146
    :pswitch_6
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_2

    .line 148
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v2, v3, v4

    .line 149
    .local v2, "state":I
    if-ne v6, v2, :cond_1

    .line 150
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    .line 152
    :cond_1
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    const/16 v4, 0x28

    if-le v3, v4, :cond_2

    .line 154
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "No response. Client seems stuck in busy state. Ping."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->pingCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 156
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->busyCount:I

    .line 157
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 158
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 162
    .end local v2    # "state":I
    :cond_2
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->beaconState:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 165
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    if-ne v7, v3, :cond_0

    .line 168
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Got transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v3, 0x2

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->beaconState:I

    .line 170
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 171
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 176
    :cond_3
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    if-nez v3, :cond_5

    .line 178
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    .line 180
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    const/16 v4, 0x1e

    if-le v3, v4, :cond_4

    .line 183
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 184
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 189
    :cond_4
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    if-ne v7, v3, :cond_0

    .line 192
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->msgRetries:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_0

    .line 194
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 195
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Retrying erase command"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->uploadCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V

    goto/16 :goto_0

    .line 203
    :cond_5
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    .line 205
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v2, v3, v4

    .line 207
    .restart local v2    # "state":I
    if-ne v7, v2, :cond_6

    .line 208
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    .line 210
    :cond_6
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->noResponseCount:I

    const/16 v4, 0x28

    if-le v3, v4, :cond_0

    .line 212
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "No response."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 214
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 221
    .end local v2    # "state":I
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 222
    .local v0, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 224
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    .line 225
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 228
    :cond_7
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 230
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getSequenceNumber()I

    move-result v3

    and-int/lit8 v3, v3, 0x4

    if-lez v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->burstResponse:[B

    .line 233
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->isCommandSent:Z

    .line 234
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 235
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 252
    .end local v0    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_8
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_9

    .line 256
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->transferInProgress:Z

    .line 257
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_FAILED error sending burst msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 261
    :cond_9
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->disableMessageProcessing()V

    .line 263
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 103
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
