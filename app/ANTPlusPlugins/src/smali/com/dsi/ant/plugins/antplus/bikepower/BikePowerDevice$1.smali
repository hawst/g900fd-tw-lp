.class Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;
.super Ljava/lang/Object;
.source "BikePowerDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 82
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    const/4 v2, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$002(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;Z)Z

    .line 83
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    const/4 v2, 0x1

    # setter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->savedAutoCtfOffset:Z
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$102(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;Z)Z

    .line 87
    :try_start_0
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$400(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mDeviceNumber:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$200(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I

    move-result v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$300(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIJ)V

    .line 89
    .local v0, "newCal":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$400(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "newCal":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v6

    .line 93
    .local v6, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to update db: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
