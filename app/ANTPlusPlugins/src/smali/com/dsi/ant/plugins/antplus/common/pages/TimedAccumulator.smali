.class public Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;
.super Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
.source "TimedAccumulator.java"


# instance fields
.field private mMaxTime:I

.field private previousTimestamp:J


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "rolloverValue"    # I
    .param p2, "maxTime"    # I

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    .line 11
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->mMaxTime:I

    .line 12
    int-to-long v0, p2

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->previousTimestamp:J

    .line 13
    return-void
.end method


# virtual methods
.method public accumulate(IJ)V
    .locals 3
    .param p1, "newValue"    # I
    .param p2, "timestamp"    # J

    .prologue
    .line 17
    iget-wide v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->previousTimestamp:J

    sub-long v1, p2, v1

    long-to-int v0, v1

    .line 19
    .local v0, "timeDifference":I
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->mMaxTime:I

    if-le v0, v1, :cond_0

    .line 20
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 22
    :cond_0
    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->previousTimestamp:J

    .line 23
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 24
    return-void
.end method
