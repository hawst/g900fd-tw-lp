.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P17_GeneralSettings.java"


# instance fields
.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private generalSettingsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V
    .locals 2
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->generalSettingsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 28
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 29
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 14
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 48
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->generalSettingsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-nez v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 97
    :goto_0
    return-void

    .line 54
    :cond_0
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 56
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 59
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 62
    .local v11, "intCycleLength":I
    const/16 v2, 0xff

    if-ne v11, v2, :cond_2

    .line 63
    new-instance v9, Ljava/math/BigDecimal;

    const/4 v2, -0x1

    invoke-direct {v9, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 67
    .local v9, "cycleLength_100ths_m":Ljava/math/BigDecimal;
    :goto_1
    const-string v2, "decimal_cycleLength"

    invoke-virtual {v8, v2, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 72
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom2LeBytes([BI)S

    move-result v12

    .line 75
    .local v12, "intInclinePercentage":I
    const/16 v2, 0x7fff

    if-ne v12, v2, :cond_3

    .line 76
    new-instance v10, Ljava/math/BigDecimal;

    const/16 v2, 0x7fff

    invoke-direct {v10, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 80
    .local v10, "inclinePercentage_100ths_m":Ljava/math/BigDecimal;
    :goto_2
    const-string v2, "decimal_inclinePercentage"

    invoke-virtual {v8, v2, v10}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v13

    .line 87
    .local v13, "resistanceLevel":I
    const/16 v2, 0xff

    if-ne v13, v2, :cond_1

    .line 88
    const/4 v13, -0x1

    .line 90
    :cond_1
    const-string v2, "int_resistanceLevel"

    invoke-virtual {v8, v2, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->generalSettingsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 96
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 65
    .end local v9    # "cycleLength_100ths_m":Ljava/math/BigDecimal;
    .end local v10    # "inclinePercentage_100ths_m":Ljava/math/BigDecimal;
    .end local v12    # "intInclinePercentage":I
    .end local v13    # "resistanceLevel":I
    :cond_2
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v11}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v4, 0x2

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v9

    .restart local v9    # "cycleLength_100ths_m":Ljava/math/BigDecimal;
    goto :goto_1

    .line 78
    .restart local v12    # "intInclinePercentage":I
    :cond_3
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v12}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v4, 0x2

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v10

    .restart local v10    # "inclinePercentage_100ths_m":Ljava/math/BigDecimal;
    goto :goto_2
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->generalSettingsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->onDropToSearch()V

    .line 104
    return-void
.end method
