.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;
.source "AntPlusBikeCadencePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCalculatedCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;

.field mMotionAndCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;

.field mRawCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;-><init>(Z)V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 8
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;>;"
    new-instance v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;

    invoke-direct {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;-><init>()V

    .line 182
    .local v7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;
    const/4 v0, 0x1

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->requestAccessBSC_helper(ZLandroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 8
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p3, "isSpdCadCombinedSensor"    # Z
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;>;"
    new-instance v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;

    invoke-direct {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;-><init>()V

    .line 286
    .local v7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;
    const/4 v0, 0x1

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->requestAccessBSC_helper(ZLandroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 316
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;-><init>()V

    .line 318
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;
    const/4 v1, 0x1

    invoke-static {v1, p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->requestAccessBSC_Helper_AsyncScanController(ZLandroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    const-string v0, "ANT+ Plugin: Bike Cadence"

    return-object v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 326
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 327
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.bikespdcad.CombinedBikeSpdCadService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 328
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 12
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 340
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 383
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 344
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mCalculatedCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 348
    .local v7, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 349
    .local v1, "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 350
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_calculatedCadence"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 351
    .local v8, "calculatedCadence":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mCalculatedCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;

    invoke-interface {v0, v1, v2, v3, v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;->onNewCalculatedCadence(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 357
    .end local v1    # "estTimestamp":J
    .end local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v7    # "b":Landroid/os/Bundle;
    .end local v8    # "calculatedCadence":Ljava/math/BigDecimal;
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mRawCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 361
    .restart local v7    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 362
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 363
    .restart local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_timestampOfLastEvent"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    .line 364
    .local v4, "timestampOfLastEvent":Ljava/math/BigDecimal;
    const-string v0, "long_cumulativeRevolutions"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 365
    .local v5, "cumulativeRevolutions":J
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mRawCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;

    invoke-interface/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;->onNewRawCadenceData(JLjava/util/EnumSet;Ljava/math/BigDecimal;J)V

    goto :goto_0

    .line 371
    .end local v1    # "estTimestamp":J
    .end local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v4    # "timestampOfLastEvent":Ljava/math/BigDecimal;
    .end local v5    # "cumulativeRevolutions":J
    .end local v7    # "b":Landroid/os/Bundle;
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mMotionAndCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;

    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 375
    .restart local v7    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 376
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 377
    .restart local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "bool_isStopped"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 378
    .local v9, "isStopped":Z
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mMotionAndCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;

    invoke-interface {v0, v1, v2, v3, v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;->onNewMotionAndCadenceData(JLjava/util/EnumSet;Z)V

    goto :goto_0

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public subscribeCalculatedCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;)V
    .locals 1
    .param p1, "CalculatedCadenceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;

    .prologue
    const/16 v0, 0x12d

    .line 396
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mCalculatedCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$ICalculatedCadenceReceiver;

    .line 397
    if-eqz p1, :cond_0

    .line 399
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->subscribeToEvent(I)Z

    .line 405
    :goto_0
    return-void

    .line 403
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeMotionAndCadenceDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;)Z
    .locals 3
    .param p1, "motionAndCadenceDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;

    .prologue
    const/16 v2, 0x12f

    .line 436
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->reportedServiceVersion:I

    const/16 v1, 0x4ef0

    if-ge v0, v1, :cond_0

    .line 438
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeMotionAndCadenceDataEvent requires ANT+ Plugins Service >20208, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const/4 v0, 0x0

    .line 450
    :goto_0
    return v0

    .line 442
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mMotionAndCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IMotionAndCadenceDataReceiver;

    .line 443
    if-eqz p1, :cond_1

    .line 445
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->subscribeToEvent(I)Z

    move-result v0

    goto :goto_0

    .line 449
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->unsubscribeFromEvent(I)V

    .line 450
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeRawCadenceDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;)V
    .locals 1
    .param p1, "RawCadenceDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;

    .prologue
    const/16 v0, 0x12e

    .line 415
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->mRawCadenceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;

    .line 416
    if-eqz p1, :cond_0

    .line 418
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->subscribeToEvent(I)Z

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
