.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;
.super Ljava/lang/Object;
.source "AvailableDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public currentDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V
    .locals 2
    .param p1, "updateDeviceListEvent"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    .line 64
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 65
    return-void
.end method

.method private makeCurrentEventBundle(ILcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)Landroid/os/Bundle;
    .locals 5
    .param p1, "listUpdateCode"    # I
    .param p2, "deviceChanging"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 80
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 82
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .line 83
    .local v1, "currentList":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 84
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    aput-object v3, v1, v2

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 85
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "b":Landroid/os/Bundle;
    const-string v3, "arrayParcelable_deviceInfos"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 89
    const-string v3, "int_listUpdateCode"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    if-eqz p2, :cond_1

    .line 91
    const-string v3, "parcelable_changingDeviceInfo"

    iget-object v4, p2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 92
    :cond_1
    return-object v0

    .line 85
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "currentList":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method


# virtual methods
.method public reportDeviceExpired(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V
    .locals 3
    .param p1, "oldDev"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->DEVICE_REMOVED_FROM_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->getIntValue()I

    move-result v2

    invoke-direct {p0, v2, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->makeCurrentEventBundle(ILcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 101
    :cond_0
    monitor-exit v1

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportDeviceFound(Lcom/dsi/ant/message/ChannelId;[BLcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .locals 14
    .param p1, "antChannelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "beaconContents"    # [B
    .param p3, "watchDatabase"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    .prologue
    .line 106
    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v11

    .line 108
    :try_start_0
    invoke-static/range {p2 .. p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsManufacturerId([B)I

    move-result v2

    .line 109
    .local v2, "beaconManufacturerId":I
    const/4 v10, 0x1

    if-eq v2, v10, :cond_0

    .line 110
    const/4 v7, 0x0

    monitor-exit v11

    .line 192
    :goto_0
    return-object v7

    .line 112
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsDeviceType([B)I

    move-result v1

    .line 113
    .local v1, "beaconDeviceType":I
    invoke-static/range {p2 .. p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getDataAvailableFlag([B)Z

    move-result v4

    .line 114
    .local v4, "dataAvailableFlag":Z
    invoke-static/range {p2 .. p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getBeaconChannelPeriod([B)I

    move-result v3

    .line 115
    .local v3, "channelPeriodFlags":I
    sget-object v10, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "DETECT: dev#"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", type-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", dataFlagUp: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", period:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 122
    .local v7, "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v10

    iget-object v12, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v12}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    if-ne v10, v12, :cond_1

    iget-object v10, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getAntfsDeviceType()I

    move-result v10

    if-ne v1, v10, :cond_1

    .line 126
    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->updateLastSeenToNow()V

    .line 127
    monitor-exit v11

    goto :goto_0

    .line 193
    .end local v1    # "beaconDeviceType":I
    .end local v2    # "beaconManufacturerId":I
    .end local v3    # "channelPeriodFlags":I
    .end local v4    # "dataAvailableFlag":Z
    .end local v7    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v8    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 132
    .restart local v1    # "beaconDeviceType":I
    .restart local v2    # "beaconManufacturerId":I
    .restart local v3    # "channelPeriodFlags":I
    .restart local v4    # "dataAvailableFlag":Z
    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v1, v10}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getDevice(III)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    move-result-object v5

    .line 134
    .local v5, "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    if-nez v5, :cond_3

    .line 137
    sparse-switch v1, :sswitch_data_0

    .line 184
    const/4 v7, 0x0

    monitor-exit v11

    goto/16 :goto_0

    .line 150
    :sswitch_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR60-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 187
    .local v6, "displayName":Ljava/lang/String;
    :goto_1
    new-instance v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .end local v5    # "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v10

    invoke-direct {v5, v10, v2, v1, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;-><init>(Ljava/util/UUID;IILjava/lang/String;)V

    .line 189
    .end local v6    # "displayName":Ljava/lang/String;
    .restart local v5    # "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    :cond_3
    new-instance v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-direct {v9, p1, v5, v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;-><init>(Lcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;I)V

    .line 190
    .local v9, "newDev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v12, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->DEVICE_ADDED_TO_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;

    invoke-virtual {v12}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->getIntValue()I

    move-result v12

    invoke-direct {p0, v12, v9}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->makeCurrentEventBundle(ILcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)Landroid/os/Bundle;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 192
    monitor-exit v11

    move-object v7, v9

    goto/16 :goto_0

    .line 153
    .end local v9    # "newDev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    :sswitch_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR310XT-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 154
    .restart local v6    # "displayName":Ljava/lang/String;
    goto :goto_1

    .line 156
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR70-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 157
    .restart local v6    # "displayName":Ljava/lang/String;
    goto :goto_1

    .line 159
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Swim-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 160
    .restart local v6    # "displayName":Ljava/lang/String;
    goto :goto_1

    .line 164
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR910XT-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 165
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 167
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR910XT-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 168
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 170
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR910XT-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 171
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 173
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR910XT-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 174
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 178
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_8
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR610-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 179
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 181
    .end local v6    # "displayName":Ljava/lang/String;
    :sswitch_9
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FR610-"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 182
    .restart local v6    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 137
    nop

    :sswitch_data_0
    .sparse-switch
        0x3dc -> :sswitch_0
        0x3fa -> :sswitch_1
        0x530 -> :sswitch_4
        0x541 -> :sswitch_8
        0x582 -> :sswitch_9
        0x59c -> :sswitch_2
        0x5db -> :sswitch_3
        0x601 -> :sswitch_5
        0x640 -> :sswitch_6
        0x680 -> :sswitch_7
    .end sparse-switch
.end method

.method public requestInstantUpdate(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 69
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v1

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 72
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->NO_CHANGE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->getIntValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->makeCurrentEventBundle(ILcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->unsubscribeFromEvent(Ljava/util/UUID;)Z

    .line 74
    monitor-exit v1

    .line 75
    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
