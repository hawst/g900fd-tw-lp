.class public abstract Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;
.source "AntPlusBikeSpdCadCommonPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerUIBikeSC;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerAsyncSearchBikeSC;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field isInstanceCadencePcc:Z

.field private mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;

.field private mIsSpeedAndCadenceCombinedSensor:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 1
    .param p1, "isInstanceCadencePcc"    # Z

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;-><init>()V

    .line 524
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mIsSpeedAndCadenceCombinedSensor:Ljava/lang/Boolean;

    .line 332
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->isInstanceCadencePcc:Z

    .line 333
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method protected static requestAccessBSC_Helper_AsyncScanController(ZLandroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;
    .locals 3
    .param p0, "isCadence"    # Z
    .param p1, "bindingContext"    # Landroid/content/Context;
    .param p2, "searchProximityThreshold"    # I
    .param p4, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;",
            ">(Z",
            "Landroid/content/Context;",
            "ITT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 471
    .local p3, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;, "TT;"
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;

    invoke-direct {v1, p4, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)V

    .line 474
    .local v1, "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController<TT;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 475
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "bool_IsCadencePcc"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 477
    invoke-static {p1, p2, v0, p3, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->requestAsyncScan_Helper_SubMain(Landroid/content/Context;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .line 479
    return-object v1
.end method

.method protected static requestAccessBSC_helper(ZLandroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "isCadence"    # Z
    .param p1, "userActivity"    # Landroid/app/Activity;
    .param p2, "bindToContext"    # Landroid/content/Context;
    .param p3, "skipPreferredSearch"    # Z
    .param p4, "searchProximityThreshold"    # I
    .param p6, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;",
            ">(Z",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "TT;)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 391
    .local p5, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    .local p7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;, "TT;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 392
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 393
    const-string v0, "b_ForceManualSelect"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 394
    const-string v0, "int_ProximityBin"

    invoke-virtual {v1, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 395
    const-string v0, "bool_IsCadencePcc"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 398
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerUIBikeSC;

    invoke-direct {v3, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerUIBikeSC;-><init>(Landroid/app/Activity;)V

    move-object v0, p2

    move-object v2, p7

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method protected static requestAccessBSC_helper(ZLandroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "isCadence"    # Z
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "antDeviceNumber"    # I
    .param p3, "searchProximityThreshold"    # I
    .param p4, "isSpdCadCombinedSensor"    # Z
    .param p6, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;",
            ">(Z",
            "Landroid/content/Context;",
            "IIZ",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "TT;)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 452
    .local p5, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    .local p7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;, "TT;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 453
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 454
    const-string v0, "int_AntDeviceID"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455
    const-string v0, "int_ProximityBin"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    const-string v0, "bool_IsSpdCadCombinedSensor"

    invoke-virtual {v1, v0, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 457
    const-string v0, "bool_IsCadencePcc"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 460
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerAsyncSearchBikeSC;

    invoke-direct {v3, p7}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$RequestAccessResultHandlerAsyncSearchBikeSC;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)V

    move-object v0, p1

    move-object v2, p7

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;
    .locals 2
    .param p1, "cmdCode"    # I
    .param p2, "msgData"    # Landroid/os/Bundle;

    .prologue
    .line 547
    if-nez p2, :cond_0

    .line 548
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "msgData":Landroid/os/Bundle;
    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 549
    .restart local p2    # "msgData":Landroid/os/Bundle;
    :cond_0
    const-string v0, "bool_IsCadencePcc"

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->isInstanceCadencePcc:Z

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 550
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x2a30

    return v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 9
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 558
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 572
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 561
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;

    if-eqz v0, :cond_0

    .line 564
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 565
    .local v6, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 566
    .local v1, "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 567
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_batteryVoltage"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    .line 568
    .local v4, "batteryVoltage":Ljava/math/BigDecimal;
    const-string v0, "int_batteryStatus"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    move-result-object v5

    .line 569
    .local v5, "batteryStatus":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;->onNewBatteryStatus(JLjava/util/EnumSet;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;)V

    goto :goto_0

    .line 558
    nop

    :pswitch_data_0
    .packed-switch 0xcf
        :pswitch_0
    .end packed-switch
.end method

.method public isSpeedAndCadenceCombinedSensor()Z
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mIsSpeedAndCadenceCombinedSensor:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected setIsSpeedAndCadence(Z)V
    .locals 2
    .param p1, "isSpeedAndCadence"    # Z

    .prologue
    .line 528
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mIsSpeedAndCadenceCombinedSensor:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 529
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t reinitialize isSpeedAndCadence"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 531
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mIsSpeedAndCadenceCombinedSensor:Ljava/lang/Boolean;

    .line 532
    return-void
.end method

.method public subscribeBatteryStatusEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;)Z
    .locals 3
    .param p1, "batteryStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;

    .prologue
    const/16 v2, 0xcf

    .line 587
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->reportedServiceVersion:I

    const/16 v1, 0x4ef0

    if-ge v0, v1, :cond_0

    .line 589
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeBatteryStatusEvent requires ANT+ Plugins Service >20208, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const/4 v0, 0x0

    .line 601
    :goto_0
    return v0

    .line 593
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBatteryStatusReceiver;

    .line 594
    if-eqz p1, :cond_1

    .line 596
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->subscribeToEvent(I)Z

    move-result v0

    goto :goto_0

    .line 600
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->unsubscribeFromEvent(I)V

    .line 601
    const/4 v0, 0x1

    goto :goto_0
.end method
