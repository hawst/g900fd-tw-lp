.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusWeightScalePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$1;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IpcDefines;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAdvancedMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;

.field mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

.field mBasicMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;

.field mBodyWeightBroadcastReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;

.field mCapabilitiesRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;

.field mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 907
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    .line 714
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 907
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 824
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 771
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 773
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 869
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 871
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 901
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;-><init>()V

    .line 903
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 920
    const-string v0, "ANT+ Plugin: Weight Scale"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 912
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 913
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.weightscale.WeightScaleService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 914
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 37
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 926
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 1035
    :pswitch_0
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 1038
    :cond_0
    :goto_0
    return-void

    .line 930
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBasicMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;

    if-eqz v1, :cond_0

    .line 933
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 935
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 936
    .local v34, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 937
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 938
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_statusCode"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    move-result-object v5

    .line 939
    .local v5, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    const-string v1, "decimal_bodyWeight"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    .line 940
    .local v6, "bodyWeight":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBasicMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;->onBasicMeasurementFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 946
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    .end local v6    # "bodyWeight":Ljava/math/BigDecimal;
    .end local v34    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAdvancedMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;

    if-eqz v1, :cond_0

    .line 949
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 951
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 952
    .restart local v34    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 953
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 954
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 955
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_statusCode"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    move-result-object v5

    .line 957
    .restart local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    const-string v1, "parcelable_AdvancedMeasurement"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    .line 958
    .local v12, "m":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAdvancedMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;

    move-wide v8, v2

    move-object v10, v4

    move-object v11, v5

    invoke-interface/range {v7 .. v12}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;->onAdvancedMeasurementFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;)V

    goto/16 :goto_0

    .line 964
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    .end local v12    # "m":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;
    .end local v34    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCapabilitiesRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;

    if-eqz v1, :cond_0

    .line 967
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 969
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 970
    .restart local v34    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 971
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 972
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_statusCode"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    move-result-object v5

    .line 973
    .restart local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    const-string v1, "int_userProfileID"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 974
    .local v18, "userProfileID":I
    const-string v1, "bool_historySupport"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v19

    .line 975
    .local v19, "historySupport":Z
    const-string v1, "bool_userProfileExchangeSupport"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    .line 976
    .local v20, "userProfileExchangeSupport":Z
    const-string v1, "bool_userProfileSelected"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v21

    .line 977
    .local v21, "userProfileSelected":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCapabilitiesRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;

    move-wide v14, v2

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    invoke-interface/range {v13 .. v21}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;->onCapabilitiesRequestFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;IZZZ)V

    goto/16 :goto_0

    .line 983
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;
    .end local v18    # "userProfileID":I
    .end local v19    # "historySupport":Z
    .end local v20    # "userProfileExchangeSupport":Z
    .end local v21    # "userProfileSelected":Z
    .end local v34    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    if-eqz v1, :cond_0

    .line 986
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 987
    .restart local v34    # "b":Landroid/os/Bundle;
    const-string v1, "int_stateCode"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;

    move-result-object v23

    .line 988
    .local v23, "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;
    const-string v1, "long_transferredBytes"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    .line 989
    .local v24, "transferredBytes":J
    const-string v1, "long_totalBytes"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v26

    .line 990
    .local v26, "totalBytes":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v27}, Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;->onNewAntFsProgressUpdate(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;JJ)V

    goto/16 :goto_0

    .line 996
    .end local v23    # "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;
    .end local v24    # "transferredBytes":J
    .end local v26    # "totalBytes":J
    .end local v34    # "b":Landroid/os/Bundle;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    if-eqz v1, :cond_0

    .line 999
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 1000
    .restart local v34    # "b":Landroid/os/Bundle;
    const-string v1, "arrayByte_rawFileBytes"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v36

    .line 1003
    .local v36, "fileBytes":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    new-instance v7, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    move-object/from16 v0, v36

    invoke-direct {v7, v0}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;-><init>([B)V

    invoke-interface {v1, v7}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;->onNewFitFileDownloaded(Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V

    goto/16 :goto_0

    .line 1009
    .end local v34    # "b":Landroid/os/Bundle;
    .end local v36    # "fileBytes":[B
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;

    if-eqz v1, :cond_0

    .line 1012
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1014
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 1015
    .restart local v34    # "b":Landroid/os/Bundle;
    const-string v1, "int_statusCode"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    move-result-object v5

    .line 1016
    .local v5, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;

    invoke-interface {v1, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;->onDownloadAllHistoryFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    goto/16 :goto_0

    .line 1022
    .end local v5    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    .end local v34    # "b":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBodyWeightBroadcastReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;

    if-eqz v1, :cond_0

    .line 1025
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v34

    .line 1026
    .restart local v34    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1027
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1028
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_bodyWeightStatus"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v35

    .line 1029
    .local v35, "bodyWeightStatus":I
    const-string v1, "decimal_bodyWeight"

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    .line 1030
    .restart local v6    # "bodyWeight":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBodyWeightBroadcastReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;

    move-object/from16 v28, v0

    invoke-static/range {v35 .. v35}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;

    move-result-object v32

    move-wide/from16 v29, v2

    move-object/from16 v31, v4

    move-object/from16 v33, v6

    invoke-interface/range {v28 .. v33}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;->onNewBodyWeightBroadcast(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 926
    nop

    :pswitch_data_0
    .packed-switch 0xbe
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public requestAdvancedMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)Z
    .locals 10
    .param p1, "advancedMeasurementFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;
    .param p2, "userProfile"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 1094
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->reportedServiceVersion:I

    if-nez v0, :cond_0

    .line 1096
    const-wide/16 v1, 0x0

    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    move-object v0, p1

    move-object v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;->onAdvancedMeasurementFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;)V

    move v0, v9

    .line 1136
    :goto_0
    return v0

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1102
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v1, "Cmd requestAdvancedMeasurement failed to start because a local command is still processing."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 1103
    goto :goto_0

    .line 1106
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAdvancedMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;

    .line 1108
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 1109
    .local v6, "cmdMsg":Landroid/os/Message;
    const/16 v0, 0x4e22

    iput v0, v6, Landroid/os/Message;->what:I

    .line 1111
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1112
    .local v7, "params":Landroid/os/Bundle;
    if-eqz p2, :cond_2

    .line 1114
    const-string v0, "parcelable_UserProfile"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1117
    :cond_2
    invoke-virtual {v6, v7}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1118
    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v8

    .line 1120
    .local v8, "ret":Landroid/os/Message;
    if-nez v8, :cond_3

    .line 1122
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v1, "Cmd requestAdvancedMeasurement died in sendPluginCommand()"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    move v0, v9

    .line 1124
    goto :goto_0

    .line 1127
    :cond_3
    iget v0, v8, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_4

    .line 1130
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cmd requestAdvancedMeasurement failed with code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v8, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1132
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requestAdvancedMeasurement cmd failed internally"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1135
    :cond_4
    invoke-virtual {v8}, Landroid/os/Message;->recycle()V

    .line 1136
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public requestBasicMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;)Z
    .locals 5
    .param p1, "basicMeasurementFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;

    .prologue
    const/4 v2, 0x0

    .line 1047
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1049
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestBasicMeasurement failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    :goto_0
    return v2

    .line 1053
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBasicMeasurementFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;

    .line 1055
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1056
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e21

    iput v3, v0, Landroid/os/Message;->what:I

    .line 1058
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 1060
    .local v1, "ret":Landroid/os/Message;
    if-nez v1, :cond_1

    .line 1062
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestBasicMeasurement died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 1067
    :cond_1
    iget v2, v1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_2

    .line 1070
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cmd requestBasicMeasurement failed with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1072
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "requestBasicMeasurement cmd failed internally"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1075
    :cond_2
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    .line 1076
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public requestCapabilities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;)Z
    .locals 11
    .param p1, "capabilitiesReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;

    .prologue
    const/4 v6, 0x0

    .line 1147
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->reportedServiceVersion:I

    if-nez v0, :cond_0

    .line 1149
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCapabilities requires ANT+ Plugins Service >10000, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    const/4 v5, -0x2

    move-object v0, p1

    move v7, v6

    move v8, v6

    invoke-interface/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;->onCapabilitiesRequestFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;IZZZ)V

    .line 1183
    :goto_0
    return v6

    .line 1154
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1156
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v1, "Cmd requestCapabilities failed to start because a local command is still processing."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1160
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCapabilitiesRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;

    .line 1162
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v9

    .line 1163
    .local v9, "cmdMsg":Landroid/os/Message;
    const/16 v0, 0x4e23

    iput v0, v9, Landroid/os/Message;->what:I

    .line 1165
    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v10

    .line 1167
    .local v10, "ret":Landroid/os/Message;
    if-nez v10, :cond_2

    .line 1169
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v1, "Cmd requestCapabilities died in sendPluginCommand()"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 1174
    :cond_2
    iget v0, v10, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3

    .line 1177
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cmd requestCapabilities failed with code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v10, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1179
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requestCapabilities cmd failed internally"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1182
    :cond_3
    invoke-virtual {v10}, Landroid/os/Message;->recycle()V

    .line 1183
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public requestDownloadAllHistory(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "downloadAllHistoryFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;
    .param p2, "fitFileDownloadedReceiver"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;
    .param p3, "antFsProgressUpdateRecevier"    # Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1196
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->reportedServiceVersion:I

    if-nez v3, :cond_1

    .line 1198
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestDownloadAllHistory requires ANT+ Plugins Service >10000, installed: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->reportedServiceVersion:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    invoke-interface {p1, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;->onDownloadAllHistoryFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    .line 1236
    :cond_0
    :goto_0
    return v5

    .line 1203
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1206
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .line 1207
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    .line 1208
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;

    .line 1210
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1211
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e24

    iput v3, v0, Landroid/os/Message;->what:I

    .line 1212
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1213
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1215
    const-string v6, "bool_UseAntFsProgressUpdates"

    if-eqz p3, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1217
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 1219
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_3

    .line 1221
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestAllHistory died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_2
    move v3, v5

    .line 1215
    goto :goto_1

    .line 1226
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_3
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    .line 1229
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestAllHistory failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1231
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "requestAllHistory cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1234
    :cond_4
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 1236
    goto :goto_0
.end method

.method public subscribeBodyWeightBroadcastEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;)V
    .locals 1
    .param p1, "BodyWeightBroadcastReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 1247
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->mBodyWeightBroadcastReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;

    .line 1248
    if-eqz p1, :cond_0

    .line 1250
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->subscribeToEvent(I)Z

    .line 1256
    :goto_0
    return-void

    .line 1254
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
