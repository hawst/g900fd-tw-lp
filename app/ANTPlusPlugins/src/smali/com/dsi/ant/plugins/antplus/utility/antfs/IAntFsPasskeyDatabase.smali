.class public interface abstract Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;
.super Ljava/lang/Object;
.source "IAntFsPasskeyDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
.end method

.method public abstract insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
.end method

.method public abstract invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
.end method

.method public abstract open()V
.end method
