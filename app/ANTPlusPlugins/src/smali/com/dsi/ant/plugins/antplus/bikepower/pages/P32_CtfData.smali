.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P32_CtfData.java"


# static fields
.field private static MAX_ACC_TIMEOUT:I


# instance fields
.field private ctfEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private timeStampAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private torqTicksAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x2ee0

    sput v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->MAX_ACC_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V
    .locals 4
    .param p1, "decoder"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .prologue
    const v3, 0xffff

    .line 34
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->ctfEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const/16 v1, 0xff

    sget v2, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v3, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->timeStampAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v3, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->torqTicksAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 35
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 36
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 15
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 56
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v12

    .line 57
    .local v12, "receivedEventCount":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v12, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 60
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v8

    .line 63
    .local v8, "rxSlope":I
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v13

    .line 64
    .local v13, "rxTimeStamp":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->timeStampAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v13, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 67
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2BeBytes([BI)I

    move-result v14

    .line 68
    .local v14, "rxTorqTicks":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->torqTicksAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v14, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 70
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->timeStampAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->torqTicksAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    invoke-virtual/range {v2 .. v10}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodeTorquePage(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;ILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 72
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->ctfEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v11, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v11, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 77
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v11, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 79
    const-string v2, "long_ctfUpdateEventCount"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v11, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 81
    const-string v2, "decimal_instantaneousSlope"

    new-instance v3, Ljava/math/BigDecimal;

    invoke-direct {v3, v8}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0xa

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v5, 0x1

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 84
    const-string v2, "decimal_accumulatedTimeStamp"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->timeStampAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0x7d0

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v5, 0x4

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    const-string v2, "long_accumulatedTorqueTicksStamp"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->torqTicksAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v11, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 90
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->ctfEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v11}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 92
    .end local v11    # "b":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;->ctfEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
