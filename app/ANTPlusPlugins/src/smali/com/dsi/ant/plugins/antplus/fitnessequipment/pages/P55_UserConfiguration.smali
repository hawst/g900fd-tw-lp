.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P55_UserConfiguration.java"


# instance fields
.field private configEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xe1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;->configEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 42
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;->configEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v6

    if-nez v6, :cond_0

    .line 66
    :goto_0
    return-void

    .line 45
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 46
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "long_EstTimestamp"

    invoke-virtual {v0, v6, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 47
    const-string v6, "long_EventFlags"

    invoke-virtual {v0, v6, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 49
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;-><init>()V

    .line 51
    .local v1, "config":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    .line 52
    .local v5, "userWeight":I
    const v6, 0xffff

    if-eq v5, v6, :cond_1

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/16 v8, 0x64

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x2

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    :goto_1
    iput-object v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    .line 54
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromUpper1And1HalfLeBytes([BI)I

    move-result v2

    .line 55
    .local v2, "cycleWeight":I
    const/16 v6, 0xfff

    if-eq v2, v6, :cond_2

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/16 v8, 0x14

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x2

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    :goto_2
    iput-object v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    .line 57
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x7

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    .line 58
    .local v3, "diameter":I
    const/16 v6, 0xff

    if-eq v3, v6, :cond_3

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v3}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/16 v8, 0x64

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x2

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    :goto_3
    iput-object v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    .line 60
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/16 v7, 0x8

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    .line 61
    .local v4, "ratio":I
    if-eqz v4, :cond_4

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const-string v8, "0.03"

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    const/4 v7, 0x2

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    :goto_4
    iput-object v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    .line 63
    const-string v6, "parcelable_UserConfiguration"

    invoke-virtual {v0, v6, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 65
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;->configEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v6, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 52
    .end local v2    # "cycleWeight":I
    .end local v3    # "diameter":I
    .end local v4    # "ratio":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 55
    .restart local v2    # "cycleWeight":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 58
    .restart local v3    # "diameter":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    .line 61
    .restart local v4    # "ratio":I
    :cond_4
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;->configEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
