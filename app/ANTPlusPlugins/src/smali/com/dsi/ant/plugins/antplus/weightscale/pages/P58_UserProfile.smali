.class public final Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;
.super Ljava/lang/Object;
.source "P58_UserProfile.java"


# static fields
.field public static final UNASSIGNED:I = -0x1


# instance fields
.field displayUserProfileExchange:Z

.field profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

.field scaleAntFs:Z

.field scaleUserProfileExchange:Z

.field scaleUserProfileSelected:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 15
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 42
    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    .line 44
    .local v4, "in":[B
    const/4 v13, 0x2

    invoke-static {v4, v13}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v11

    .line 46
    .local v11, "rawProfileID":I
    const v13, 0xffff

    if-ne v11, v13, :cond_4

    .line 47
    const/4 v12, -0x1

    .line 51
    .local v12, "userProfileID":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 52
    .local v2, "gender":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    const/4 v13, 0x6

    aget-byte v13, v4, v13

    and-int/lit16 v13, v13, 0x80

    if-lez v13, :cond_5

    const/4 v5, 0x1

    .line 53
    .local v5, "isMale":Z
    :goto_1
    if-eqz v5, :cond_6

    .line 54
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 58
    :cond_0
    :goto_2
    const/4 v13, 0x6

    aget-byte v13, v4, v13

    and-int/lit8 v9, v13, 0x7f

    .line 59
    .local v9, "rawAge":I
    move v1, v9

    .line 60
    .local v1, "age":I
    const/4 v13, -0x1

    if-ne v12, v13, :cond_1

    if-nez v9, :cond_1

    .line 61
    const/4 v1, -0x1

    .line 63
    :cond_1
    const/4 v13, 0x7

    aget-byte v13, v4, v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v10

    .line 64
    .local v10, "rawHeight":I
    move v3, v10

    .line 65
    .local v3, "height":I
    const/4 v13, -0x1

    if-ne v12, v13, :cond_2

    if-nez v10, :cond_2

    .line 66
    const/4 v3, -0x1

    .line 68
    :cond_2
    const/16 v13, 0x8

    aget-byte v13, v4, v13

    and-int/lit8 v8, v13, 0x7

    .line 69
    .local v8, "rawActivityLevel":I
    move v0, v8

    .line 70
    .local v0, "activityLevel":I
    const/4 v13, -0x1

    if-ne v12, v13, :cond_3

    if-nez v8, :cond_3

    .line 71
    const/4 v0, -0x1

    .line 73
    :cond_3
    const/16 v13, 0x8

    aget-byte v13, v4, v13

    and-int/lit16 v13, v13, 0x80

    if-lez v13, :cond_7

    const/4 v6, 0x1

    .line 76
    .local v6, "lifetimeAthlete":Z
    :goto_3
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    .line 77
    .local v7, "p":Landroid/os/Parcel;
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    invoke-virtual {v7, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->getIntValue()I

    move-result v13

    invoke-virtual {v7, v13}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    invoke-virtual {v7, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    invoke-virtual {v7, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    invoke-virtual {v7, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 83
    if-eqz v6, :cond_8

    const/4 v13, 0x1

    :goto_4
    int-to-byte v13, v13

    invoke-virtual {v7, v13}, Landroid/os/Parcel;->writeByte(B)V

    .line 84
    new-instance v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-direct {v13, v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;-><init>(Landroid/os/Parcel;)V

    iput-object v13, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .line 85
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v13

    const/4 v14, 0x4

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0x80

    if-nez v13, :cond_9

    const/4 v13, 0x1

    :goto_5
    iput-boolean v13, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->displayUserProfileExchange:Z

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v13

    const/4 v14, 0x4

    aget-byte v13, v13, v14

    and-int/lit8 v13, v13, 0x4

    if-lez v13, :cond_a

    const/4 v13, 0x1

    :goto_6
    iput-boolean v13, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleAntFs:Z

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v13

    const/4 v14, 0x4

    aget-byte v13, v13, v14

    and-int/lit8 v13, v13, 0x2

    if-lez v13, :cond_b

    const/4 v13, 0x1

    :goto_7
    iput-boolean v13, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileExchange:Z

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v13

    const/4 v14, 0x4

    aget-byte v13, v13, v14

    and-int/lit8 v13, v13, 0x1

    if-lez v13, :cond_c

    const/4 v13, 0x1

    :goto_8
    iput-boolean v13, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileSelected:Z

    .line 91
    return-void

    .line 49
    .end local v0    # "activityLevel":I
    .end local v1    # "age":I
    .end local v2    # "gender":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    .end local v3    # "height":I
    .end local v5    # "isMale":Z
    .end local v6    # "lifetimeAthlete":Z
    .end local v7    # "p":Landroid/os/Parcel;
    .end local v8    # "rawActivityLevel":I
    .end local v9    # "rawAge":I
    .end local v10    # "rawHeight":I
    .end local v12    # "userProfileID":I
    :cond_4
    move v12, v11

    .restart local v12    # "userProfileID":I
    goto/16 :goto_0

    .line 52
    .restart local v2    # "gender":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 55
    .restart local v5    # "isMale":Z
    :cond_6
    const/4 v13, -0x1

    if-eq v12, v13, :cond_0

    .line 56
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto/16 :goto_2

    .line 73
    .restart local v0    # "activityLevel":I
    .restart local v1    # "age":I
    .restart local v3    # "height":I
    .restart local v8    # "rawActivityLevel":I
    .restart local v9    # "rawAge":I
    .restart local v10    # "rawHeight":I
    :cond_7
    const/4 v6, 0x0

    goto :goto_3

    .line 83
    .restart local v6    # "lifetimeAthlete":Z
    .restart local v7    # "p":Landroid/os/Parcel;
    :cond_8
    const/4 v13, 0x0

    goto :goto_4

    .line 87
    :cond_9
    const/4 v13, 0x0

    goto :goto_5

    .line 88
    :cond_a
    const/4 v13, 0x0

    goto :goto_6

    .line 89
    :cond_b
    const/4 v13, 0x0

    goto :goto_7

    .line 90
    :cond_c
    const/4 v13, 0x0

    goto :goto_8
.end method

.method public encodePage([B)V
    .locals 6
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x5

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 95
    const/16 v0, 0x3a

    aput-byte v0, p1, v3

    .line 97
    aput-byte v3, p1, v1

    .line 98
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileSelected:Z

    if-eqz v0, :cond_0

    .line 99
    aget-byte v0, p1, v1

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 100
    :cond_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileExchange:Z

    if-eqz v0, :cond_1

    .line 101
    aget-byte v0, p1, v1

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 102
    :cond_1
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleAntFs:Z

    if-eqz v0, :cond_2

    .line 103
    aget-byte v0, p1, v1

    or-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 104
    :cond_2
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->displayUserProfileExchange:Z

    if-nez v0, :cond_3

    .line 105
    aget-byte v0, p1, v1

    or-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 107
    :cond_3
    const/4 v0, 0x4

    aput-byte v2, p1, v0

    .line 109
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    if-eqz v0, :cond_a

    .line 111
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 113
    const/4 v0, 0x1

    aput-byte v2, p1, v0

    .line 114
    const/4 v0, 0x2

    aput-byte v2, p1, v0

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    if-ne v0, v2, :cond_7

    .line 123
    aput-byte v3, p1, v4

    .line 127
    :goto_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    if-ne v0, v1, :cond_4

    .line 128
    aget-byte v0, p1, v4

    or-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 130
    :cond_4
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    if-ne v0, v2, :cond_8

    .line 131
    const/4 v0, 0x6

    aput-byte v3, p1, v0

    .line 135
    :goto_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    if-ne v0, v2, :cond_9

    .line 136
    aput-byte v3, p1, v5

    .line 140
    :goto_3
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    if-eqz v0, :cond_5

    .line 141
    aget-byte v0, p1, v5

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, p1, v5

    .line 151
    :cond_5
    :goto_4
    return-void

    .line 118
    :cond_6
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 119
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v1

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_0

    .line 125
    :cond_7
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    goto :goto_1

    .line 133
    :cond_8
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_2

    .line 138
    :cond_9
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    and-int/lit8 v0, v0, 0x7

    int-to-byte v0, v0

    aput-byte v0, p1, v5

    goto :goto_3

    .line 145
    :cond_a
    const/4 v0, 0x1

    aput-byte v2, p1, v0

    .line 146
    const/4 v0, 0x2

    aput-byte v2, p1, v0

    .line 147
    aput-byte v3, p1, v4

    .line 148
    const/4 v0, 0x6

    aput-byte v3, p1, v0

    .line 149
    aput-byte v3, p1, v5

    goto :goto_4
.end method

.method public getDisplayUserProfileExchange()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->displayUserProfileExchange:Z

    return v0
.end method

.method public getScaleAntFs()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleAntFs:Z

    return v0
.end method

.method public getScaleUserProfileExchange()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileExchange:Z

    return v0
.end method

.method public getScaleUserProfileSelected()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileSelected:Z

    return v0
.end method

.method public getUserProfile()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    return-object v0
.end method

.method public setDisplayUserProfileExchange(Z)V
    .locals 0
    .param p1, "displayUserProfileExchange"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->displayUserProfileExchange:Z

    return-void
.end method

.method public setScaleAntFs(Z)V
    .locals 0
    .param p1, "scaleAntFs"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleAntFs:Z

    return-void
.end method

.method public setScaleUserProfileExchange(Z)V
    .locals 0
    .param p1, "scaleUserProfileExchange"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileExchange:Z

    return-void
.end method

.method public setScaleUserProfileSelected(Z)V
    .locals 0
    .param p1, "scaleUserProfileSelected"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->scaleUserProfileSelected:Z

    return-void
.end method

.method public setUserProfile(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)V
    .locals 0
    .param p1, "profile"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->profile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    return-void
.end method
