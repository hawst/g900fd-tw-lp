.class Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;
.super Ljava/lang/Object;
.source "PluginDownloadFilesHelper.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V
    .locals 6
    .param p1, "stateCode"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    .param p2, "reason"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    .prologue
    const-wide/16 v2, 0x0

    .line 52
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$2;->$SwitchMap$com$dsi$ant$plugins$antplus$utility$antfs$AntFsHostSession$AntFsHostState:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_0
    return-void

    .line 55
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;

    const/16 v1, 0x1f4

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->handleStateChange(IJJ)V

    goto :goto_0

    .line 58
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;

    const/16 v1, 0x226

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->handleStateChange(IJJ)V

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
