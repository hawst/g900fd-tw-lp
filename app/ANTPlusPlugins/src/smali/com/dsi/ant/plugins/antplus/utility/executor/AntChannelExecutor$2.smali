.class Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;
.super Ljava/lang/Object;
.source "AntChannelExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startWorkerThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 101
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$300(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 104
    const/4 v1, 0x0

    .line 107
    .local v1, "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->idleTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$400(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    if-nez v2, :cond_3

    .line 109
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$500(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/util/concurrent/Exchanger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 125
    .restart local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :goto_1
    if-eqz v1, :cond_0

    .line 139
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$200(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 141
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 142
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$600(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->initTaskInternal(Lcom/dsi/ant/channel/AntChannel;)V

    .line 144
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$300(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->handleInterruptRequestInternal(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 148
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 149
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 199
    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$200(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 201
    :try_start_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 202
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->handleInterruptRequestInternal(I)Z

    .line 203
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 204
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 205
    return-void

    .line 116
    .restart local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :cond_3
    :try_start_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$500(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/util/concurrent/Exchanger;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x64

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5, v6}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .restart local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    goto :goto_1

    .line 117
    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_4
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->idleTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$400(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    .restart local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    goto :goto_1

    .line 127
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :catch_1
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$300(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 134
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const-string v3, "Worker thread interrupted unexpectedly"

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$000(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Ljava/lang/String;)V

    goto :goto_2

    .line 151
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :cond_4
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 157
    :goto_3
    :try_start_6
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Begin doWork() in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->doWorkInternal()V

    .line 159
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "End doWork() in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$200(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    .line 163
    :try_start_7
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 164
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->cancelling:Z
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$800(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 166
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->cancelling:Z
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$802(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Z)Z

    .line 167
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 174
    :goto_4
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    if-nez v2, :cond_6

    .line 176
    monitor-exit v3

    goto/16 :goto_0

    .line 190
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v2
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2

    .line 193
    :catch_2
    move-exception v0

    .line 195
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException occured executing DoWork() in task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$000(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 151
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_1
    move-exception v2

    :try_start_9
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v2

    .line 171
    :cond_5
    :try_start_a
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->getNextTaskToExecute()Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v4

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    goto :goto_4

    .line 180
    :cond_6
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$600(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->initTaskInternal(Lcom/dsi/ant/channel/AntChannel;)V

    .line 182
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$300(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->handleInterruptRequestInternal(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 186
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$102(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 187
    monitor-exit v3

    goto/16 :goto_0

    .line 190
    :cond_7
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 204
    .end local v1    # "nextTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :catchall_2
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v2
.end method
