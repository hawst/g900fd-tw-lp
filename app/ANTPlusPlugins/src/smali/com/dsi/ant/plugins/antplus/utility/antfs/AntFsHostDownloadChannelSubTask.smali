.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostDownloadChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask$1;
    }
.end annotation


# static fields
.field static final BEACON_BUSY:I = 0x3

.field static final BEACON_ID:B = 0x43t

.field static final BEACON_TRANS:I = 0x2

.field static final BEACON_UNKNOWN:I = -0x1

.field static final COMMAND_RESPONSE_ID:B = 0x44t

.field static final DOWNLOAD_REQUEST_ID:B = 0x9t

.field static final DOWNLOAD_RESPONSE_CRC_INCORRECT:I = 0x5

.field static final DOWNLOAD_RESPONSE_ID:B = -0x77t

.field static final DOWNLOAD_RESPONSE_NOT_DOWNLOADABLE:I = 0x2

.field static final DOWNLOAD_RESPONSE_NOT_EXIST:I = 0x1

.field static final DOWNLOAD_RESPONSE_NOT_READY:I = 0x3

.field static final DOWNLOAD_RESPONSE_OK:I = 0x0

.field static final DOWNLOAD_RESPONSE_REQUEST_INVALID:I = 0x4

.field static final FOOTER_SIZE:I = 0x8

.field static final HEADER_SIZE:I = 0x18

.field static final INITIAL_REQUEST:B = 0x1t

.field static final PING_REQUEST_ID:B = 0x5t

.field private static final TAG:Ljava/lang/String;

.field static final pingCommand:[B


# instance fields
.field badResponseCount:I

.field beaconState:I

.field burstRx:Ljava/io/ByteArrayOutputStream;

.field burstRxFailed:Z

.field busyCount:I

.field data:Ljava/io/ByteArrayOutputStream;

.field dataOffset:I

.field downloadCommand:[B

.field downloadData:[B

.field downloadUpdateThreshold:I

.field fileSize:I

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

.field isDownloadCommandSent:Z

.field mDownloadReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;

.field mIndex:I

.field mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field msgRetries:I

.field noResponseCount:I

.field numPacketsSinceProgressUpdate:I

.field private result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

.field transferInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    .line 52
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->pingCommand:[B

    return-void

    :array_0
    .array-data 1
        0x44t
        0x5t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;I)V
    .locals 2
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "downloadReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;
    .param p3, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 55
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->beaconState:I

    .line 64
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    .line 65
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    .line 66
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    .line 68
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    .line 69
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    .line 70
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    .line 71
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    .line 72
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->noResponseCount:I

    .line 73
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I

    .line 77
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    .line 83
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_OTHER:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 88
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mDownloadReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;

    .line 89
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mIndex:I

    .line 90
    return-void
.end method

.method private processDownloadResponse()Z
    .locals 14

    .prologue
    .line 438
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    .line 440
    .local v5, "response":[B
    array-length v8, v5

    const/16 v9, 0x18

    if-ge v8, v9, :cond_0

    .line 441
    const/4 v8, 0x0

    .line 567
    :goto_0
    return v8

    .line 444
    :cond_0
    const/16 v8, 0x43

    const/4 v9, 0x0

    aget-byte v9, v5, v9

    if-ne v8, v9, :cond_1

    const/16 v8, 0x44

    const/16 v9, 0x8

    aget-byte v9, v5, v9

    if-ne v8, v9, :cond_1

    const/16 v8, -0x77

    const/16 v9, 0x9

    aget-byte v9, v5, v9

    if-eq v8, v9, :cond_2

    .line 446
    :cond_1
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Invalid burst response received. Will retry request..."

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const/4 v8, 0x0

    goto :goto_0

    .line 450
    :cond_2
    const/16 v8, 0xa

    aget-byte v6, v5, v8

    .line 451
    .local v6, "responseCode":I
    packed-switch v6, :pswitch_data_0

    .line 492
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid download response received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_INVALID_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 494
    const/4 v8, 0x1

    goto :goto_0

    .line 455
    :pswitch_0
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Download data does not exist."

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_FILE_DOES_NOT_EXIST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 457
    const/4 v8, 0x1

    goto :goto_0

    .line 461
    :pswitch_1
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Download data exists but is not downloadable."

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_FILE_NOT_READABLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 463
    const/4 v8, 0x1

    goto :goto_0

    .line 467
    :pswitch_2
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Client device rejected download: not ready"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_NOT_READY:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 469
    const/4 v8, 0x1

    goto :goto_0

    .line 473
    :pswitch_3
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Client device rejected download: invalid request"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_INVALID_REQUEST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 475
    const/4 v8, 0x1

    goto :goto_0

    .line 479
    :pswitch_4
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Client device rejected download: CRC incorrect"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_INCORRECT_CRC:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 482
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->restartDownloadFromScratch()V

    .line 483
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 487
    :pswitch_5
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 499
    const/16 v8, 0xc

    invoke-static {v5, v8}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v8

    long-to-int v7, v8

    .line 500
    .local v7, "totalRemainingDataLength":I
    const/16 v8, 0x10

    invoke-static {v5, v8}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    .line 501
    const/16 v8, 0x14

    invoke-static {v5, v8}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    .line 502
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Download response: OK, Length: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Offset: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " File Size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v2

    .line 506
    .local v2, "currentOffset":I
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    if-le v8, v2, :cond_3

    .line 508
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The offset received ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is larger than expected ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "). Looks like the client skipped data. Retry the previous request."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 513
    :cond_3
    array-length v8, v5

    add-int/lit8 v0, v8, -0x18

    .line 514
    .local v0, "actualBlockLength":I
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    if-le v0, v8, :cond_4

    .line 515
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    .line 516
    :cond_4
    if-lez v7, :cond_5

    if-ge v7, v0, :cond_5

    .line 517
    move v0, v7

    .line 520
    :cond_5
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    if-ge v8, v2, :cond_6

    .line 521
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    sub-int v8, v2, v8

    sub-int/2addr v0, v8

    .line 523
    :cond_6
    const-wide/32 v8, 0xffff

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v10

    and-long/2addr v8, v10

    long-to-int v4, v8

    .line 528
    .local v4, "previousCrc":I
    :try_start_0
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    sub-int v9, v2, v9

    add-int/lit8 v9, v9, 0x18

    invoke-virtual {v8, v5, v9, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->update([BII)V

    .line 531
    iget-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    if-nez v8, :cond_7

    .line 533
    array-length v8, v5

    add-int/lit8 v8, v8, -0x2

    invoke-static {v5, v8}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v1

    .line 534
    .local v1, "clientCrc":I
    int-to-long v8, v1

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-eqz v8, :cond_7

    .line 536
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CRC mismatch: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (client) / "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (host). Retry request."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_REJECTED_INCORRECT_CRC:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 538
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I

    .line 539
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v8, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->reset(I)V

    .line 540
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 545
    .end local v1    # "clientCrc":I
    :cond_7
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    sub-int v9, v2, v9

    add-int/lit8 v9, v9, 0x18

    invoke-virtual {v8, v5, v9, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 557
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    if-lt v8, v9, :cond_8

    iget-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    if-nez v8, :cond_8

    .line 559
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 547
    :catch_0
    move-exception v3

    .line 549
    .local v3, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error updating data/CRC: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Response length: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Current Offset: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Data Offset: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Actual block length: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_INVALID_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 552
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I

    .line 553
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v8, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->reset(I)V

    .line 554
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 564
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_8
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/4 v9, 0x4

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v10

    int-to-long v10, v10

    invoke-static {v8, v9, v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 565
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/16 v9, 0x9

    const/4 v10, 0x0

    aput-byte v10, v8, v9

    .line 566
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/16 v9, 0xa

    const-wide/32 v10, 0xffff

    iget-object v12, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v12}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v12

    and-long/2addr v10, v12

    long-to-int v10, v10

    invoke-static {v8, v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 567
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private restartDownloadFromScratch()V
    .locals 4

    .prologue
    .line 573
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->hostCrc:Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->reset()V

    .line 574
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 575
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/4 v1, 0x4

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 576
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/16 v1, 0x9

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 577
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 578
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 317
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 322
    const/16 v5, 0x10

    new-array v5, v5, [B

    fill-array-data v5, :array_0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    .line 324
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mIndex:I

    invoke-static {v5, v10, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 333
    :cond_0
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v5}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 343
    .local v0, "channelState":Lcom/dsi/ant/message/ChannelState;
    :try_start_1
    sget-object v5, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v5, :cond_1

    sget-object v5, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v5, :cond_1

    .line 346
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Failed: Connection lost"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 348
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 349
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 434
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :goto_1
    return-void

    .line 335
    :catch_0
    move-exception v1

    .line 337
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACFE occurred requesting status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 339
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v5}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 340
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 427
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v1

    .line 429
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Interrupted waiting for result"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 431
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->FAIL_OTHER:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .line 432
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 354
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_1
    :try_start_2
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Wait for transport beacon"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const/4 v5, -0x1

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->beaconState:I

    .line 356
    new-instance v5, Ljava/util/concurrent/CountDownLatch;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 357
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->enableMessageProcessing()V

    .line 358
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v6, 0xa

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 360
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->beaconState:I

    if-eq v5, v10, :cond_2

    .line 362
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Timed out waiting for transport beacon"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 366
    :cond_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    .line 367
    .local v2, "theIndex":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v5

    long-to-int v4, v5

    .line 368
    .local v4, "theOffset":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    const/16 v6, 0x9

    aget-byte v3, v5, v6

    .line 369
    .local v3, "theInitial":B
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Sending download request command. Index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Initial: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_DOWNLOADING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 371
    if-ne v9, v3, :cond_3

    .line 372
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->DOWNLOAD_START:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 375
    :goto_2
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    .line 376
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    .line 377
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    .line 378
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->numPacketsSinceProgressUpdate:I

    .line 379
    new-instance v5, Ljava/util/concurrent/CountDownLatch;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 380
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->enableMessageProcessing()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 384
    :try_start_3
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    invoke-virtual {v5, v6}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 390
    :goto_3
    :try_start_4
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 396
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    if-nez v5, :cond_4

    .line 398
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Failed: Download Tx retries exceeded"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    :cond_3
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->DOWNLOAD_RESUME:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    goto :goto_2

    .line 385
    :catch_2
    move-exception v1

    .line 388
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception sending burst: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 403
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_4
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v6, "Wait for download response"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    .line 405
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->noResponseCount:I

    .line 406
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 407
    new-instance v5, Ljava/util/concurrent/CountDownLatch;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 408
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->enableMessageProcessing()V

    .line 409
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 412
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->processDownloadResponse()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 421
    :goto_4
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadData:[B

    .line 422
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Download completed. Total downloaded data: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadData:[B

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bytes"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 424
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->DOWNLOAD_FINISHED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    goto/16 :goto_1

    .line 417
    :cond_5
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->badResponseCount:I
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    const/16 v6, 0x32

    if-le v5, v6, :cond_0

    goto :goto_4

    .line 322
    nop

    :array_0
    .array-data 1
        0x44t
        0x9t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method public getDownloadData()[B
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadData:[B

    return-object v0
.end method

.method public getDownloadResult()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    return-object v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    const-string v0, "ANT-FS Host Download Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 105
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x43

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v9, 0x0

    .line 117
    :try_start_0
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 121
    :pswitch_0
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v5, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v5, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 124
    :pswitch_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Search timeout occured"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 283
    :catch_0
    move-exception v2

    .line 285
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v4

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v4, v5, :cond_b

    .line 289
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "TRANSFER_IN_PROGRESS error sending burst msg"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Channel closed"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    .line 131
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 132
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 306
    :catch_1
    move-exception v2

    .line 308
    .local v2, "e":Ljava/io/IOException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException receiving burst: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 310
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 135
    .end local v2    # "e":Ljava/io/IOException;
    :pswitch_3
    const/4 v4, 0x0

    :try_start_2
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    .line 136
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    .line 137
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    if-nez v4, :cond_0

    .line 141
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    .line 142
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 143
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 150
    :pswitch_4
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    goto/16 :goto_0

    .line 153
    :pswitch_5
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Transfer Rx fail"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    .line 155
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 156
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 166
    :pswitch_6
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v8, v4, :cond_2

    .line 168
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    aget-byte v3, v4, v5

    .line 169
    .local v3, "state":I
    if-ne v6, v3, :cond_1

    .line 170
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    .line 172
    :cond_1
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    const/16 v5, 0x28

    if-le v4, v5, :cond_2

    .line 174
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "No response. Client seems stuck in busy state. Ping."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->restartDownloadFromScratch()V

    .line 176
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->pingCommand:[B

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 177
    const/4 v4, 0x0

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->busyCount:I

    .line 178
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 179
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 183
    .end local v3    # "state":I
    :cond_2
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->beaconState:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 186
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v8, v4, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    if-ne v7, v4, :cond_0

    .line 189
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Got transport beacon"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v4, 0x2

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->beaconState:I

    .line 191
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 192
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 197
    :cond_3
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->isDownloadCommandSent:Z

    if-nez v4, :cond_5

    .line 199
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    .line 201
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    const/16 v5, 0x1e

    if-le v4, v5, :cond_4

    .line 204
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 205
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 210
    :cond_4
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v8, v4, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    if-ne v7, v4, :cond_0

    .line 213
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->msgRetries:I

    rem-int/lit8 v4, v4, 0x3

    if-nez v4, :cond_0

    .line 215
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    .line 216
    const/4 v4, 0x0

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->numPacketsSinceProgressUpdate:I

    .line 217
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Retrying download command"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadCommand:[B

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V

    goto/16 :goto_0

    .line 225
    :cond_5
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v8, v4, :cond_0

    .line 227
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    aget-byte v3, v4, v5

    .line 229
    .restart local v3    # "state":I
    if-ne v7, v3, :cond_6

    .line 230
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->noResponseCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->noResponseCount:I

    .line 232
    :cond_6
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->noResponseCount:I

    const/16 v5, 0x28

    if-le v4, v5, :cond_0

    .line 234
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "No response."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 236
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 243
    .end local v3    # "state":I
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 244
    .local v0, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 245
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 247
    :cond_7
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 249
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    const/16 v5, 0x18

    if-ne v4, v5, :cond_8

    .line 251
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    .line 252
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    .line 253
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    div-int/lit8 v4, v4, 0x64

    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x64

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadUpdateThreshold:I

    .line 256
    :cond_8
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getSequenceNumber()I

    move-result v4

    and-int/lit8 v4, v4, 0x4

    if-lez v4, :cond_9

    .line 258
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRxFailed:Z

    .line 259
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 260
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 265
    :cond_9
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    const/16 v5, 0x18

    if-le v4, v5, :cond_0

    .line 267
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x18

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->dataOffset:I

    add-int v1, v4, v5

    .line 268
    .local v1, "currentBytes":I
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    if-le v1, v4, :cond_a

    .line 269
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    .line 270
    :cond_a
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->numPacketsSinceProgressUpdate:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->numPacketsSinceProgressUpdate:I

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->downloadUpdateThreshold:I

    if-le v4, v5, :cond_0

    .line 272
    const/4 v4, 0x0

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->numPacketsSinceProgressUpdate:I

    .line 273
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->mDownloadReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;

    int-to-long v5, v1

    iget v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    int-to-long v7, v7

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;->onTransferUpdate(JJ)V

    .line 274
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Download progress: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->fileSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 291
    .end local v0    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    .end local v1    # "currentBytes":I
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_b
    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v4

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v4, v5, :cond_c

    .line 295
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->transferInProgress:Z

    .line 296
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "TRANSFER_FAILED error sending burst msg"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 300
    :cond_c
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACFE handling message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->disableMessageProcessing()V

    .line 302
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
