.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MonitorRunner"
.end annotation


# instance fields
.field afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

.field downloadRoundWatchers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;",
            ">;"
        }
    .end annotation
.end field

.field inTransportState:Z

.field stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;)V
    .locals 1

    .prologue
    .line 266
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->inTransportState:Z

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    .line 285
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$1;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    return-void
.end method

.method private checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;)Z
    .locals 4
    .param p1, "downloadResult"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    .prologue
    .line 676
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    if-ne p1, v1, :cond_0

    .line 678
    const/4 v1, 0x0

    .line 687
    :goto_0
    return v1

    .line 682
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS download request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const/16 v0, -0x28

    .line 686
    .local v0, "resultToReport":I
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    .line 687
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z
    .locals 4
    .param p1, "result"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .prologue
    .line 641
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-ne p1, v1, :cond_0

    .line 643
    const/4 v1, 0x0

    .line 670
    :goto_0
    return v1

    .line 647
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    sget-object v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$1;->$SwitchMap$com$dsi$ant$plugins$antplus$utility$antfs$AntFsHostSession$AntFsRequestResult:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 665
    const/16 v0, -0x28

    .line 669
    .local v0, "resultToReport":I
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    .line 670
    const/4 v1, 0x1

    goto :goto_0

    .line 653
    .end local v0    # "resultToReport":I
    :pswitch_0
    const/16 v0, -0x3d

    .line 654
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 657
    .end local v0    # "resultToReport":I
    :pswitch_1
    const/16 v0, -0x410

    .line 658
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 661
    .end local v0    # "resultToReport":I
    :pswitch_2
    const/16 v0, -0x29

    .line 662
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 650
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private doDownloadLoop()V
    .locals 50
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 348
    const-wide v27, 0x7fffffffffffffffL

    .line 349
    .local v27, "currentOldestFileTimestamp":J
    const-wide/16 v41, 0x0

    .line 352
    .local v41, "lastSeenDirectoryModifiedTimestamp":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 354
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->open()V

    .line 357
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .local v38, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 359
    .local v37, "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getDeviceDbId()J

    move-result-wide v9

    move-object/from16 v0, v37

    iput-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->deviceDbId:J

    .line 362
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->PROGRESS_SYNCING_WITH_DEVICE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendStatusUpdate(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;)V

    .line 363
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    move-object/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    move-object/from16 v0, v37

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->downloadNewOnly:Z

    if-nez v3, :cond_1

    .line 368
    const-wide/16 v9, 0x0

    move-object/from16 v0, v37

    iput-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    .line 369
    const-wide/16 v9, 0x0

    move-object/from16 v0, v37

    iput-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenMeasurementTime:J

    .line 378
    :goto_2
    move-object/from16 v0, v37

    iget-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    cmp-long v3, v9, v27

    if-gez v3, :cond_0

    .line 380
    move-object/from16 v0, v37

    iget-wide v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    move-wide/from16 v27, v0

    .line 381
    const-wide/16 v41, 0x0

    goto :goto_1

    .line 373
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    move-object/from16 v0, v37

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-wide v10, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->deviceDbId:J

    invoke-virtual {v3, v9, v10, v11}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getLastDownloadGarminTime(Ljava/lang/String;J)J

    move-result-wide v9

    move-object/from16 v0, v37

    iput-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    move-object/from16 v0, v37

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-wide v10, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->deviceDbId:J

    invoke-virtual {v3, v9, v10, v11}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->getLastDownloadedMeasurementGarminTime(Ljava/lang/String;J)J

    move-result-wide v9

    move-object/from16 v0, v37

    iput-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenMeasurementTime:J

    goto :goto_2

    .line 393
    .end local v37    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v38    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 386
    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->close()V

    .line 387
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    .line 393
    .end local v38    # "i$":Ljava/util/Iterator;
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->inTransportState:Z

    if-nez v3, :cond_7

    .line 398
    const/16 v4, 0x64

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    .line 399
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestConnectToTransport(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 519
    :cond_4
    :goto_3
    return-void

    .line 389
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 391
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 402
    :cond_6
    const/16 v4, 0x320

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    .line 403
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->inTransportState:Z

    .line 408
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    const/4 v4, 0x0

    new-instance v9, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$2;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$2;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;)V

    invoke-virtual {v3, v4, v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v31

    .line 416
    .local v31, "downloadResult":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 422
    :try_start_3
    new-instance v29, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastDownloadedData()[B

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastRequestStartedTimeUtc()Ljava/util/GregorianCalendar;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-direct {v0, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;-><init>([BLjava/util/GregorianCalendar;)V
    :try_end_3
    .catch Ljava/util/zip/DataFormatException; {:try_start_3 .. :try_end_3} :catch_0

    .line 429
    .local v29, "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    const-wide/16 v19, 0x0

    .line 435
    .local v19, "newestFileSeenTimestamp":J
    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    cmp-long v3, v3, v41

    if-lez v3, :cond_19

    .line 439
    const-wide/16 v48, 0x0

    .line 440
    .local v48, "totalDlBytes":J
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    .line 441
    .local v36, "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    const/16 v37, 0x0

    .local v37, "i":I
    :goto_4
    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    move/from16 v0, v37

    if-ge v0, v3, :cond_d

    .line 443
    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    move/from16 v0, v37

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 444
    .local v35, "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    move-object/from16 v0, v35

    iget-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    cmp-long v3, v3, v19

    if-lez v3, :cond_8

    .line 445
    move-object/from16 v0, v35

    iget-wide v0, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    move-wide/from16 v19, v0

    .line 450
    :cond_8
    move-object/from16 v0, v35

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    sget-object v4, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->FIT_DATA_TYPE:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->getIntValue()I

    move-result v4

    if-ne v3, v4, :cond_c

    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v3, v4}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, v35

    iget-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    cmp-long v3, v3, v27

    if-lez v3, :cond_c

    .line 456
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 458
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_9
    :goto_5
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 460
    .local v33, "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move-object/from16 v0, v33

    iget-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    move-object/from16 v0, v35

    iget-wide v11, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    cmp-long v3, v9, v11

    if-gez v3, :cond_9

    .line 462
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 463
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    :cond_a
    const/4 v3, 0x1

    move-object/from16 v0, v33

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->needsDownloadThisRound:Z

    goto :goto_5

    .line 467
    .end local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v38    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    .line 423
    .end local v19    # "newestFileSeenTimestamp":J
    .end local v29    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .end local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v36    # "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    .end local v37    # "i":I
    .end local v48    # "totalDlBytes":J
    :catch_0
    move-exception v34

    .line 425
    .local v34, "e":Ljava/util/zip/DataFormatException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ANTFS directory DataFormatException: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v34 .. v34}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/16 v4, -0x28

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    goto/16 :goto_3

    .line 467
    .end local v34    # "e":Ljava/util/zip/DataFormatException;
    .restart local v19    # "newestFileSeenTimestamp":J
    .restart local v29    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .restart local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .restart local v36    # "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    .restart local v37    # "i":I
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v48    # "totalDlBytes":J
    :cond_b
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 469
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    move-object/from16 v0, v35

    iget-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    add-long v48, v48, v3

    .line 441
    .end local v38    # "i$":Ljava/util/Iterator;
    :cond_c
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_4

    .line 476
    .end local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    :cond_d
    invoke-virtual/range {v36 .. v36}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_16

    .line 479
    const-wide/16 v46, 0x0

    .line 480
    .local v46, "totalBytesDownloaded":J
    move-wide/from16 v7, v48

    .line 481
    .local v7, "totalBytesToDownload":J
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v45}, Ljava/util/ArrayList;-><init>()V

    .line 483
    .local v45, "measurements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/garmin/fit/BloodPressureMesg;>;"
    const/16 v30, 0x0

    .line 484
    .local v30, "downloadFailed":Z
    invoke-virtual/range {v36 .. v36}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_e
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 488
    .restart local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    move-wide/from16 v5, v46

    .line 489
    .local v5, "cachedCurrentCumulativeBytes":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    move-object/from16 v0, v35

    iget v10, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    new-instance v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;JJ)V

    invoke-virtual {v9, v10, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v31

    .line 501
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->checkReturn(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 503
    const/16 v30, 0x1

    .line 518
    .end local v5    # "cachedCurrentCumulativeBytes":J
    .end local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    :cond_f
    if-nez v30, :cond_4

    .line 522
    new-instance v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$4;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;)V

    move-object/from16 v0, v45

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 536
    :try_start_6
    invoke-virtual/range {v45 .. v45}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .end local v38    # "i$":Ljava/util/Iterator;
    :cond_10
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/garmin/fit/BloodPressureMesg;

    .line 538
    .local v43, "m":Lcom/garmin/fit/BloodPressureMesg;
    invoke-virtual/range {v43 .. v43}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v3

    invoke-virtual {v3}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    .line 539
    .local v13, "m_timestamp":J
    const/16 v44, 0x0

    .line 540
    .local v44, "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .local v39, "i$":Ljava/util/Iterator;
    :cond_11
    :goto_6
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 542
    .restart local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move-object/from16 v0, v33

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    if-nez v3, :cond_11

    move-object/from16 v0, v33

    iget-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenMeasurementTime:J

    cmp-long v3, v9, v13

    if-gez v3, :cond_11

    .line 545
    if-nez v44, :cond_12

    .line 546
    new-instance v44, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;

    .end local v44    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    move-object/from16 v0, v44

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;-><init>(Lcom/garmin/fit/BloodPressureMesg;)V

    .line 547
    .restart local v44    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    :cond_12
    move-object/from16 v0, v33

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 550
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v9, "Sending measurement failed, removing watcher"

    invoke-static {v3, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/4 v3, 0x1

    move-object/from16 v0, v33

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    goto :goto_6

    .line 566
    .end local v13    # "m_timestamp":J
    .end local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v39    # "i$":Ljava/util/Iterator;
    .end local v43    # "m":Lcom/garmin/fit/BloodPressureMesg;
    .end local v44    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    :catchall_2
    move-exception v3

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v3

    .line 507
    .restart local v5    # "cachedCurrentCumulativeBytes":J
    .restart local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastDownloadedData()[B

    move-result-object v32

    .line 508
    .local v32, "downloadedFile":[B
    move-object/from16 v0, v35

    iget-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    add-long v46, v46, v3

    .line 511
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v45

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->parseMeasurementFile([BLjava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 513
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/16 v4, -0x28

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    goto/16 :goto_3

    .line 557
    .end local v5    # "cachedCurrentCumulativeBytes":J
    .end local v32    # "downloadedFile":[B
    .end local v35    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v38    # "i$":Ljava/util/Iterator;
    .restart local v13    # "m_timestamp":J
    .restart local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .restart local v39    # "i$":Ljava/util/Iterator;
    .restart local v43    # "m":Lcom/garmin/fit/BloodPressureMesg;
    .restart local v44    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    :cond_14
    const/4 v3, 0x1

    :try_start_7
    move-object/from16 v0, v33

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->gotMeasurementThisRound:Z

    .line 558
    move-object/from16 v0, v33

    iput-wide v13, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenMeasurementTime:J

    .line 559
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->open()V

    .line 560
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v9, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    move-object/from16 v0, v33

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v10, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v33

    iget-wide v11, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->deviceDbId:J

    invoke-virtual/range {v9 .. v14}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->setLastDownloadedMeasurementGarminTime(Ljava/lang/String;JJ)V

    .line 561
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->close()V

    goto/16 :goto_6

    .line 566
    .end local v13    # "m_timestamp":J
    .end local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v39    # "i$":Ljava/util/Iterator;
    .end local v43    # "m":Lcom/garmin/fit/BloodPressureMesg;
    .end local v44    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    :cond_15
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 572
    .end local v7    # "totalBytesToDownload":J
    .end local v30    # "downloadFailed":Z
    .end local v45    # "measurements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/garmin/fit/BloodPressureMesg;>;"
    .end local v46    # "totalBytesDownloaded":J
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 574
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->open()V

    .line 575
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_17
    :goto_7
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 577
    .restart local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move-object/from16 v0, v33

    iget-wide v9, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    cmp-long v3, v9, v19

    if-eqz v3, :cond_17

    .line 579
    move-wide/from16 v0, v19

    move-object/from16 v2, v33

    iput-wide v0, v2, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->lastSeenFileTimeThreshold:J

    .line 580
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v15, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    move-object/from16 v0, v33

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v0, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v33

    iget-wide v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->deviceDbId:J

    move-wide/from16 v17, v0

    invoke-virtual/range {v15 .. v20}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->setLastDownloadGarminTime(Ljava/lang/String;JJ)V

    goto :goto_7

    .line 584
    .end local v33    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v38    # "i$":Ljava/util/Iterator;
    :catchall_3
    move-exception v3

    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v3

    .line 583
    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_18
    :try_start_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->close()V

    .line 584
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 587
    move-wide/from16 v27, v19

    .line 588
    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v0, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    move-wide/from16 v41, v0

    .line 592
    .end local v36    # "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    .end local v37    # "i":I
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v48    # "totalDlBytes":J
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 594
    :try_start_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 596
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v40

    .local v40, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :goto_8
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 598
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 601
    .local v37, "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move-object/from16 v0, v37

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    if-eqz v3, :cond_1a

    .line 603
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    .line 633
    .end local v37    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v40    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :catchall_4
    move-exception v3

    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v3

    .line 608
    .restart local v37    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .restart local v40    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :cond_1a
    :try_start_b
    move-object/from16 v0, v37

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->monitorForNewMeasurements:Z

    if-nez v3, :cond_1b

    .line 610
    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendFinished(I)V

    .line 611
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    .line 616
    :cond_1b
    move-object/from16 v0, v37

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->needsDownloadThisRound:Z

    if-eqz v3, :cond_1c

    .line 618
    const/16 v22, 0x320

    const-wide/16 v23, 0x0

    const-wide/16 v25, 0x0

    move-object/from16 v21, p0

    invoke-virtual/range {v21 .. v26}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    .line 622
    :cond_1c
    move-object/from16 v0, v37

    iget-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->hasSynced:Z

    if-nez v3, :cond_1d

    .line 624
    const/4 v3, 0x1

    move-object/from16 v0, v37

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->hasSynced:Z

    .line 626
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->PROGRESS_MONITORING:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendStatusUpdate(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;)V

    .line 630
    :cond_1d
    const/4 v3, 0x0

    move-object/from16 v0, v37

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->needsDownloadThisRound:Z

    .line 631
    const/4 v3, 0x0

    move-object/from16 v0, v37

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->gotMeasurementThisRound:Z

    goto :goto_8

    .line 633
    .end local v37    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    :cond_1e
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 635
    const-wide/16 v3, 0x7d0

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    goto/16 :goto_0
.end method

.method private parseMeasurementFile([BLjava/util/ArrayList;)Z
    .locals 8
    .param p1, "downloadedFile"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/BloodPressureMesg;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "measurements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/garmin/fit/BloodPressureMesg;>;"
    const/4 v4, 0x0

    .line 694
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 696
    .local v2, "fitFile":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/garmin/fit/Decode;->checkIntegrity(Ljava/io/InputStream;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 698
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "FIT file integrity check failed."

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :goto_0
    return v4

    .line 705
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 711
    :goto_1
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$5;

    invoke-direct {v0, p0, p2}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$5;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;Ljava/util/ArrayList;)V

    .line 721
    .local v0, "bloodPressureMesgListener":Lcom/garmin/fit/BloodPressureMesgListener;
    new-instance v3, Lcom/garmin/fit/MesgBroadcaster;

    invoke-direct {v3}, Lcom/garmin/fit/MesgBroadcaster;-><init>()V

    .line 722
    .local v3, "mesgBroadcaster":Lcom/garmin/fit/MesgBroadcaster;
    invoke-virtual {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/BloodPressureMesgListener;)V

    .line 726
    :try_start_1
    invoke-virtual {v3, v2}, Lcom/garmin/fit/MesgBroadcaster;->run(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 734
    const/4 v4, 0x1

    goto :goto_0

    .line 728
    :catch_0
    move-exception v1

    .line 730
    .local v1, "e":Lcom/garmin/fit/FitRuntimeException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error decoding FIT file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/garmin/fit/FitRuntimeException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 706
    .end local v0    # "bloodPressureMesgListener":Lcom/garmin/fit/BloodPressureMesgListener;
    .end local v1    # "e":Lcom/garmin/fit/FitRuntimeException;
    .end local v3    # "mesgBroadcaster":Lcom/garmin/fit/MesgBroadcaster;
    :catch_1
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method protected handleStateChange(IJJ)V
    .locals 8
    .param p1, "antFsStateCode"    # I
    .param p2, "transferredBytes"    # J
    .param p4, "totalBytes"    # J

    .prologue
    .line 276
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->inTransportState:Z

    if-nez v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v7, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    .line 281
    .local v7, "recipients":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :goto_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .local v0, "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 282
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->reportAntFsStatus(IJJ)V

    goto :goto_1

    .line 279
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "recipients":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :cond_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    .restart local v7    # "recipients":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    goto :goto_0

    .line 283
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 311
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v2, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->runningLock:Ljava/lang/Object;

    monitor-enter v2

    .line 313
    :try_start_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-wide v4, v4, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->serialNumberForHost:J

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget v6, v6, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->antDeviceNumber:I

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JI)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .line 314
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget v3, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->linkRfFreq:I

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget v4, v4, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->linkRfPeriod:I

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->beaconInterval:I

    invoke-virtual {v1, v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->setLinkChannelParameters(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    const/16 v4, 0x3e8

    invoke-virtual {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/16 v3, -0x14

    invoke-virtual {v1, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 344
    :goto_0
    return-void

    .line 326
    :cond_0
    :try_start_3
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->doDownloadLoop()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 330
    :try_start_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->isRunning:Z

    .line 331
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v1, v3, :cond_1

    .line 334
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v3, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 343
    :cond_1
    :try_start_5
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    .line 330
    :catchall_1
    move-exception v1

    :try_start_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->isRunning:Z

    .line 331
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->afs:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-eq v3, v4, :cond_2

    .line 334
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DownloadMeasurements Failed to close ANTFS session."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v1
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 337
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e1":Ljava/lang/InterruptedException;
    :try_start_7
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ANTFS request InterruptedException"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    const/16 v3, -0x28

    invoke-virtual {v1, v3}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    .line 341
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method
