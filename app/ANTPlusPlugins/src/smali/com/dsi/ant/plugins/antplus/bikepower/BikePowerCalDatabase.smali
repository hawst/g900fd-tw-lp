.class public Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
.super Ljava/lang/Object;
.source "BikePowerCalDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$1;,
        Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;,
        Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "saved_bikepwrcals.db"

.field private static final DATABASE_VERSION:I = 0x1


# instance fields
.field private mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    .line 83
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    .line 84
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;->close()V

    .line 90
    return-void
.end method

.method public getCalibration(I)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    .locals 10
    .param p1, "antDeviceNumber"    # I

    .prologue
    const/4 v7, 0x0

    .line 99
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 100
    .local v9, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT Calibration_Id, AntDeviceNumber, CalibrationValue, Timestamp FROM Calibrations WHERE (AntDeviceNumber == "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ");"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 105
    .local v8, "cPlugin":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 106
    .local v0, "ci":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 109
    .local v2, "calibration_dbId":I
    const/4 v1, 0x2

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 110
    .local v4, "calibrationValue":I
    const/4 v1, 0x3

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 112
    .local v5, "timestamp":J
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    .end local v0    # "ci":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIIJLcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$1;)V

    .line 115
    .end local v2    # "calibration_dbId":I
    .end local v4    # "calibrationValue":I
    .end local v5    # "timestamp":J
    .restart local v0    # "ci":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 116
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 117
    return-object v0
.end method

.method public insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)V
    .locals 9
    .param p1, "calibrationInfo"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    .prologue
    const/4 v8, 0x0

    .line 127
    iget v5, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->antDeviceNumber:I

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->getCalibration(I)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    move-result-object v0

    .line 128
    .local v0, "ci":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 129
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v0, :cond_0

    .line 131
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 132
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "CalibrationValue"

    iget v6, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibrationValue:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    const-string v5, "Timestamp"

    iget-wide v6, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->timestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 134
    const-string v5, "Calibrations"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calibration_Id == "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # invokes: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->getCalibration_dbId()I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v4, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 135
    # invokes: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->getCalibration_dbId()I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)I

    move-result v5

    # invokes: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->setCalibration_dbId(I)V
    invoke-static {p1, v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->access$200(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;I)V

    .line 146
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 147
    return-void

    .line 139
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 140
    .restart local v4    # "values":Landroid/content/ContentValues;
    const-string v5, "AntDeviceNumber"

    iget v6, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->antDeviceNumber:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 141
    const-string v5, "CalibrationValue"

    iget v6, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibrationValue:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 142
    const-string v5, "Timestamp"

    iget-wide v6, p1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->timestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 143
    const-string v5, "Calibrations"

    invoke-virtual {v1, v5, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 144
    .local v2, "dbId":J
    long-to-int v5, v2

    # invokes: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->setCalibration_dbId(I)V
    invoke-static {p1, v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->access$200(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;I)V

    goto :goto_0
.end method

.method public removeCalibration(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)V
    .locals 4
    .param p1, "calibrationInfo"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 156
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "Calibrations"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calibration_Id == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # invokes: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->getCalibration_dbId()I
    invoke-static {p1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 157
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 158
    return-void
.end method
