.class public Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;
.super Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;
.source "ChannelTask_DownloadData.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

.field private final reqDlFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private reqDlProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field requestor:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field private responseSent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IZZ)V
    .locals 3
    .param p1, "geocacheReceiver"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "deviceID"    # I
    .param p4, "updateVisitCount"    # Z
    .param p5, "useProgressUpdates"    # Z

    .prologue
    .line 41
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    invoke-virtual {v0, p3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    move-result-object v0

    invoke-direct {p0, p1, v0, p4}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;Z)V

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->responseSent:Z

    .line 42
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .line 43
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->requestor:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 46
    if-eqz p5, :cond_0

    .line 47
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 133
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    .line 135
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->doWork()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 151
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-eqz v1, :cond_0

    .line 152
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 155
    :goto_1
    return-void

    .line 139
    :catchall_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v4, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    throw v1
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 141
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE setting search timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 147
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 148
    throw v0

    .line 154
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Geocache Data Download "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->deviceID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleInterruptRequest(I)Z
    .locals 1
    .param p1, "interruptingTaskRank"    # I

    .prologue
    .line 178
    const v0, 0x7fffffff

    if-ne p1, v0, :cond_0

    .line 180
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 181
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->handleInterruptRequest(I)Z

    move-result v0

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleProgressUpdate(II)V
    .locals 2
    .param p1, "done"    # I
    .param p2, "total"    # I

    .prologue
    .line 160
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    :goto_0
    return-void

    .line 163
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_workUnitsFinished"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    const-string v1, "int_totalUnitsWork"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public processRequest()V
    .locals 4

    .prologue
    .line 82
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-nez v2, :cond_0

    .line 84
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_NOT_IN_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 118
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v2

    if-gez v2, :cond_2

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->updateVisitCount:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->isVisitCountUpdated()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    :cond_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 97
    :cond_2
    const/4 v1, 0x0

    .line 100
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 107
    :goto_1
    if-nez v1, :cond_3

    .line 109
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 114
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->setCurrentState(I)V

    goto :goto_0
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V
    .locals 5
    .param p1, "statusCode"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    .prologue
    .line 52
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v3

    .line 54
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->responseSent:Z

    if-eqz v2, :cond_0

    .line 55
    monitor-exit v3

    .line 77
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getIntValue()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    if-ne p1, v2, :cond_1

    .line 62
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v1, "d":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->requestor:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->pluginLibVersion:I

    if-nez v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyGeocacheCompat$GeocacheDeviceDataCompat_v1;->writeGddToBundleCompat_v1(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;Landroid/os/Bundle;)V

    .line 71
    :goto_1
    const-string v2, "bundle_downloadedData"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 74
    .end local v1    # "d":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->reqDlFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 75
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->responseSent:Z

    .line 76
    monitor-exit v3

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 69
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "d":Landroid/os/Bundle;
    :cond_2
    :try_start_1
    const-string v2, "parcelable_GeocacheDeviceData"

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
