.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "WatchCommunicatorService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 1
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 30
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 50
    const/16 v4, 0x12c

    if-eq p1, v4, :cond_1

    .line 51
    const/4 v3, 0x0

    .line 79
    :cond_0
    :goto_0
    return v3

    .line 53
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->isDeviceClosed()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 55
    :cond_2
    sget-object v4, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_FS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {p0, v4, v5, p2, p3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 56
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v0, :cond_0

    .line 61
    :try_start_0
    new-instance v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-direct {v4, v0, p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v0    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-virtual {p0, p3, v4, p2, v5}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 76
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->activeController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->closeDevice()V

    goto :goto_0

    .line 62
    .restart local v0    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/nio/channels/ClosedChannelException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->TAG:Ljava/lang/String;

    const-string v5, "Failed to instantiate device: Constructor threw ClosedChannelException."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 66
    .local v2, "response":Landroid/os/Message;
    const/4 v4, -0x4

    iput v4, v2, Landroid/os/Message;->what:I

    .line 67
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0
.end method
