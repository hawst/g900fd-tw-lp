.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
.super Ljava/lang/Object;
.source "AudioDeviceCapabilities.java"


# instance fields
.field public customRepeatModeSupport:Z

.field public customShuffleModeSupport:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customRepeatModeSupport:Z

    .line 18
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customShuffleModeSupport:Z

    return-void
.end method

.method public static readFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
    .locals 2
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;-><init>()V

    .line 28
    .local v0, "c":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
    const-string v1, "bool_customRepeatModeSupported"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customRepeatModeSupport:Z

    .line 29
    const-string v1, "boolCustomShuffleModeSupport"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customShuffleModeSupport:Z

    .line 30
    return-object v0
.end method


# virtual methods
.method public writeToBundle(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const-string v0, "bool_customRepeatModeSupported"

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customRepeatModeSupport:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    const-string v0, "boolCustomShuffleModeSupport"

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customShuffleModeSupport:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 41
    return-void
.end method
