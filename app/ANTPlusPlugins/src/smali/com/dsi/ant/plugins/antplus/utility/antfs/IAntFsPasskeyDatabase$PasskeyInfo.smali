.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
.super Ljava/lang/Object;
.source "IAntFsPasskeyDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PasskeyInfo"
.end annotation


# instance fields
.field public antDeviceNumber:I

.field public antFsDeviceType:I

.field public antFsManufacturerId:I

.field public antFsPasskey:[B

.field public antFsSerialNumber:J

.field public passkey_dbID:J


# direct methods
.method public constructor <init>(J[BIIIJ)V
    .locals 0
    .param p1, "passkey_dbID"    # J
    .param p3, "antFsPasskey"    # [B
    .param p4, "antFsManufacturerId"    # I
    .param p5, "antFsDeviceType"    # I
    .param p6, "antDeviceNumber"    # I
    .param p7, "antFsSerialNumber"    # J

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-wide p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    .line 33
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    .line 34
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    .line 35
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    .line 36
    iput p6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    .line 37
    iput-wide p7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    .line 38
    return-void
.end method

.method public constructor <init>([BIIIJ)V
    .locals 0
    .param p1, "antFsPasskey"    # [B
    .param p2, "antFsManufacturerId"    # I
    .param p3, "antFsDeviceType"    # I
    .param p4, "antDeviceNumber"    # I
    .param p5, "antFsSerialNumber"    # J

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    .line 24
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    .line 25
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    .line 26
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    .line 27
    iput-wide p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    .line 28
    return-void
.end method
