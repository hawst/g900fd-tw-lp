.class Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;
.super Ljava/lang/Object;
.source "CombinedBikeSpdCadService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

.field final synthetic val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;)V
    .locals 0

    .prologue
    .line 1003
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;ILcom/dsi/ant/message/ChannelId;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    .line 1003
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->handleSearchResult(ILcom/dsi/ant/message/ChannelId;)V

    return-void
.end method

.method private handleSearchResult(ILcom/dsi/ant/message/ChannelId;)V
    .locals 9
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x4

    .line 1019
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Async connect rcv result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 1021
    .local v3, "response":Landroid/os/Message;
    sparse-switch p1, :sswitch_data_0

    .line 1063
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Plugin async scan connect failed internally with search result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1065
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1066
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$1100(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V

    .line 1069
    :goto_0
    return-void

    .line 1024
    :sswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 1025
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_2

    .line 1027
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin async scan connect failed to shutdown executor cleanly"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1029
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1047
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1048
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Could not acquire resultSent semaphore after successful connection"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$1000(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V

    goto :goto_0

    .line 1033
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v4, v5, v8}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$900(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    .line 1034
    .local v2, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {v4, v0, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v1

    .line 1035
    .local v1, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-nez v1, :cond_3

    .line 1037
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin async scan connect failed internally: Device instantiation failed."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1039
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_1

    .line 1043
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v1, v6, v8}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1044
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_1

    .line 1054
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v1    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v2    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :sswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    .line 1055
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v4, :cond_4

    .line 1056
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$scanControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 1057
    :cond_4
    const/4 v4, -0x7

    iput v4, v3, Landroid/os/Message;->what:I

    .line 1058
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 1021
    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mPccHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$800(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1$1;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;ILcom/dsi/ant/message/ChannelId;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1015
    return-void
.end method
