.class public Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "BikePowerDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder$1;
    }
.end annotation


# static fields
.field private static final COAST_DETECTION_TIMEOUT:I = 0xb3b

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final coastDetectedFlag:I

.field private coastDetectionSent:Z

.field private final distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final distIpc:Ljava/lang/String;

.field private final distSourceIpc:Ljava/lang/String;

.field private instCadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private nonPwrOnlyPageSeen:Z

.field private possibleCoastDetected:Z

.field private possibleCoastStartTimestamp:J

.field private pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

.field private final pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final pwrIpc:Ljava/lang/String;

.field private final pwrSourceIpc:Ljava/lang/String;

.field private final spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final spdIpc:Ljava/lang/String;

.field private final spdSourceIpc:Ljava/lang/String;

.field private startingValuePwrOnly:Z

.field private startingValueTorque:Z

.field private final torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final torqIpc:Ljava/lang/String;

.field private final torqSourceIpc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 50
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    .line 52
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValuePwrOnly:Z

    .line 53
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    .line 55
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 56
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    .line 87
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 88
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    .line 89
    const-string v0, "decimal_calculatedPower"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    .line 91
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xda

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 92
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqSourceIpc:Ljava/lang/String;

    .line 93
    const-string v0, "decimal_calculatedTorque"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqIpc:Ljava/lang/String;

    .line 95
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 96
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdSourceIpc:Ljava/lang/String;

    .line 97
    const-string v0, "decimal_calculatedSpeed"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdIpc:Ljava/lang/String;

    .line 99
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 100
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distSourceIpc:Ljava/lang/String;

    .line 101
    const-string v0, "decimal_calculatedDistance"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distIpc:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->COAST_OR_STOP_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectedFlag:I

    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V
    .locals 2
    .param p1, "pwrDevice"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 50
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    .line 52
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValuePwrOnly:Z

    .line 53
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    .line 55
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 56
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    .line 61
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .line 63
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 64
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    .line 65
    const-string v0, "decimal_calculatedPower"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 68
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqSourceIpc:Ljava/lang/String;

    .line 69
    const-string v0, "decimal_calculatedTorque"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqIpc:Ljava/lang/String;

    .line 71
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 72
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdSourceIpc:Ljava/lang/String;

    .line 73
    const-string v0, "decimal_calculatedWheelSpeed"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdIpc:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 76
    const-string v0, "int_dataSource"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distSourceIpc:Ljava/lang/String;

    .line 77
    const-string v0, "decimal_calculatedWheelDistance"

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distIpc:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 80
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->instCadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 82
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->COAST_OR_STOP_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectedFlag:I

    .line 83
    return-void
.end method

.method private determineTorqueDataSourceCode(I)I
    .locals 5
    .param p1, "intPageType"    # I

    .prologue
    const/4 v4, 0x0

    .line 477
    invoke-static {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v0

    .line 478
    .local v0, "bikePowerPageType":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    invoke-static {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    move-result-object v2

    .line 482
    .local v2, "fitnessEquipmentPageType":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    if-eqz v3, :cond_1

    .line 484
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    if-eqz v3, :cond_0

    .line 486
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    .line 488
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$DataSource:[I

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 500
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 554
    .local v1, "dataSourceCode":I
    :goto_0
    return v1

    .line 491
    .end local v1    # "dataSourceCode":I
    :pswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_CRANK_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 492
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 494
    .end local v1    # "dataSourceCode":I
    :pswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_WHEEL_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 495
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 497
    .end local v1    # "dataSourceCode":I
    :pswitch_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_CTF_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 498
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 506
    .end local v1    # "dataSourceCode":I
    :cond_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$DataSource:[I

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 518
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 519
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 509
    .end local v1    # "dataSourceCode":I
    :pswitch_3
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->CRANK_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 510
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 512
    .end local v1    # "dataSourceCode":I
    :pswitch_4
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->WHEEL_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 513
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 515
    .end local v1    # "dataSourceCode":I
    :pswitch_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->CTF_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 516
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 525
    .end local v1    # "dataSourceCode":I
    :cond_1
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    if-eqz v3, :cond_2

    .line 527
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    .line 529
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusFitnessEquipmentPcc$TrainerDataSource:[I

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    .line 535
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v1

    .line 536
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 532
    .end local v1    # "dataSourceCode":I
    :pswitch_6
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->INITIAL_VALUE_TRAINER_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v1

    .line 533
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 542
    .end local v1    # "dataSourceCode":I
    :cond_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusFitnessEquipmentPcc$TrainerDataSource:[I

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_3

    .line 548
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v1

    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 545
    .end local v1    # "dataSourceCode":I
    :pswitch_7
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->TRAINER_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v1

    .line 546
    .restart local v1    # "dataSourceCode":I
    goto :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 506
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 529
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch

    .line 542
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method private isCoasting(JLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;I)Z
    .locals 6
    .param p1, "estTimestamp"    # J
    .param p3, "accEvtCnt"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p4, "dataSourceCode"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 561
    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_CRANK_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v2

    if-ne p4, v2, :cond_0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_WHEEL_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v2

    if-ne p4, v2, :cond_0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->INITIAL_VALUE_TRAINER_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v2

    if-ne p4, v2, :cond_0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_CTF_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v2

    if-eq p4, v2, :cond_3

    .line 568
    :cond_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    if-nez v2, :cond_2

    .line 570
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 571
    iput-wide p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastStartTimestamp:J

    .line 590
    :cond_1
    :goto_0
    return v0

    .line 575
    :cond_2
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastStartTimestamp:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0xb3b

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 577
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    .line 578
    iget p4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectedFlag:I

    :goto_1
    move v0, v1

    .line 590
    goto :goto_0

    .line 586
    :cond_3
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 587
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    goto :goto_1
.end method


# virtual methods
.method public decodeInstantaneousCadence(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 5
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "pageType"    # I
    .param p6, "accEvtCnt"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p7, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 443
    const/16 v3, 0x10

    if-ne p5, v3, :cond_1

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    if-eqz v3, :cond_1

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->instCadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 449
    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v3

    if-eqz v3, :cond_0

    .line 452
    invoke-virtual {p7}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x4

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    .line 454
    .local v2, "intCadence":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 455
    .local v0, "b":Landroid/os/Bundle;
    const-string v3, "long_EstTimestamp"

    invoke-virtual {v0, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 456
    const-string v3, "long_EventFlags"

    invoke-virtual {v0, v3, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 458
    const/16 v3, 0xff

    if-eq v2, v3, :cond_2

    .line 459
    const-string v3, "int_instantaneousCadence"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 463
    :goto_1
    invoke-virtual {p7}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    .line 464
    .local v1, "dataPage":I
    const-string v3, "int_dataSource"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 466
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->instCadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0

    .line 461
    .end local v1    # "dataPage":I
    :cond_2
    const-string v3, "int_instantaneousCadence"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 0
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 597
    return-void
.end method

.method public decodePwrOnlyPage(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "accEvtCnt"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p6, "accPower"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .prologue
    .line 148
    :try_start_0
    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValuePwrOnly:Z

    .line 153
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 154
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    .line 221
    :cond_1
    :goto_0
    return-void

    .line 159
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    if-nez v2, :cond_1

    .line 163
    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-nez v2, :cond_4

    .line 166
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    if-nez v2, :cond_3

    .line 168
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 169
    iput-wide p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastStartTimestamp:J
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v1

    .line 219
    .local v1, "e":Ljava/lang/ArithmeticException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor sent an unexpected value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/ArithmeticException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    .end local v1    # "e":Ljava/lang/ArithmeticException;
    :cond_3
    :try_start_1
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastStartTimestamp:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0xb3b

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 175
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 176
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 177
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 178
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectedFlag:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "0"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 181
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 182
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    goto :goto_0

    .line 191
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->possibleCoastDetected:Z

    .line 192
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    .line 195
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 196
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 197
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 200
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValuePwrOnly:Z

    if-eqz v2, :cond_6

    .line 202
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValuePwrOnly:Z

    .line 203
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    if-eqz v2, :cond_5

    .line 204
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INITIAL_VALUE_POWER_ONLY_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    new-instance v3, Ljava/math/BigDecimal;

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v4, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v5, 0x5

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 215
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 206
    :cond_5
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->INITIAL_VALUE_TRAINER_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 208
    :cond_6
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    if-eqz v2, :cond_7

    .line 209
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->POWER_ONLY_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 211
    :cond_7
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->TRAINER_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public decodeTorquePage(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V
    .locals 8
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "pageType"    # I
    .param p6, "accEvtCnt"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p7, "accTicks"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p8, "accPeriod"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p9, "accTorq"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .prologue
    .line 231
    :try_start_0
    invoke-direct {p0, p5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->determineTorqueDataSourceCode(I)I

    move-result v1

    .line 234
    .local v1, "dataSourceCode":I
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    .line 237
    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual/range {p9 .. p9}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 239
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z

    .line 344
    .end local v1    # "dataSourceCode":I
    :cond_1
    :goto_0
    return-void

    .line 245
    .restart local v1    # "dataSourceCode":I
    :cond_2
    invoke-direct {p0, p1, p2, p6, v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->isCoasting(JLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 248
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 250
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 251
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 252
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 253
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 256
    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v4

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v4, :cond_b

    .line 257
    :cond_3
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    .line 262
    .local v3, "retVal":Ljava/math/BigDecimal;
    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 263
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 266
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_4
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 268
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 269
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 270
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 271
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 273
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v4, :cond_c

    .line 274
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    .line 279
    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    :goto_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 280
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 283
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_5
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->WHEEL_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v4

    if-eq p5, v4, :cond_6

    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->TRAINER_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getIntValue()I

    move-result v4

    if-ne p5, v4, :cond_9

    .line 286
    :cond_6
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 288
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 289
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 290
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 291
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 294
    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v4

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v4, :cond_d

    .line 295
    :cond_7
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    .line 300
    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    :goto_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 301
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 304
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_8
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 306
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 307
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 308
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 311
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 313
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distIpc:Ljava/lang/String;

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual {p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 315
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 319
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_9
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->CRANK_TORQUE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v4

    if-ne p5, v4, :cond_1

    .line 321
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 323
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 324
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 325
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 326
    const-string v4, "int_dataSource"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 329
    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v4

    if-eqz v4, :cond_a

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v4, :cond_e

    .line 330
    :cond_a
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    .line 335
    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    :goto_4
    const-string v4, "decimal_calculatedCrankCadence"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 336
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 340
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "dataSourceCode":I
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :catch_0
    move-exception v2

    .line 342
    .local v2, "e":Ljava/lang/ArithmeticException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sensor sent an unexpected value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/ArithmeticException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    .end local v2    # "e":Ljava/lang/ArithmeticException;
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "dataSourceCode":I
    :cond_b
    :try_start_1
    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v5, Ljava/math/BigDecimal;

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p9 .. p9}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v6, 0x9

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_1

    .line 276
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_c
    new-instance v4, Ljava/math/BigDecimal;

    invoke-virtual/range {p9 .. p9}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x20

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v6, 0xf

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v6, 0xd

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_2

    .line 297
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_d
    new-instance v4, Ljava/math/BigDecimal;

    const v5, 0x12000

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0xa

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v6, 0x6

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v6, 0x4

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_3

    .line 332
    .end local v3    # "retVal":Ljava/math/BigDecimal;
    :cond_e
    new-instance v4, Ljava/math/BigDecimal;

    const v5, 0x1e000

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v6, 0x3

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .restart local v3    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_4
.end method

.method public decodeTorquePage(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;ILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V
    .locals 9
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "accEvtCnt"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p6, "slope"    # I
    .param p7, "accTimeStmp"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p8, "accTorqTicks"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .prologue
    .line 348
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    if-nez v5, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    :try_start_0
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->CTF_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->determineTorqueDataSourceCode(I)I

    move-result v1

    .line 356
    .local v1, "dataSourceCode":I
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v3

    .line 358
    .local v3, "offset":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->getCtfOffset()I

    move-result v3

    .line 360
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v5

    if-ne v3, v5, :cond_2

    .line 361
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    .line 363
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->nonPwrOnlyPageSeen:Z

    .line 366
    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 368
    :cond_3
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->startingValueTorque:Z
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 434
    .end local v1    # "dataSourceCode":I
    .end local v3    # "offset":I
    :catch_0
    move-exception v2

    .line 436
    .local v2, "e":Ljava/lang/ArithmeticException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Sensor sent an unexpected value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/ArithmeticException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 374
    .end local v2    # "e":Ljava/lang/ArithmeticException;
    .restart local v1    # "dataSourceCode":I
    .restart local v3    # "offset":I
    :cond_4
    :try_start_1
    invoke-direct {p0, p1, p2, p5, v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->isCoasting(JLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 377
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 379
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 380
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 381
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 382
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 384
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v5

    if-ne v3, v5, :cond_8

    .line 385
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "-1"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 394
    .local v4, "retVal":Ljava/math/BigDecimal;
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrIpc:Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 395
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 398
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_5
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 400
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 401
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 402
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 403
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqSourceIpc:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 405
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v5

    if-ne v3, v5, :cond_a

    .line 406
    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "-1"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 413
    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    :goto_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqIpc:Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 414
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 417
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_6
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 419
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 420
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 421
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 422
    const-string v5, "int_dataSource"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 424
    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v5

    if-eqz v5, :cond_7

    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v5, :cond_c

    .line 425
    :cond_7
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v4

    .line 430
    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    :goto_3
    const-string v5, "decimal_calculatedCrankCadence"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 431
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 386
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_8
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v5, :cond_9

    .line 387
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v4

    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_1

    .line 389
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_9
    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0x7d0

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v7, 0xb

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v3}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const v7, 0x9c40

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const-wide v7, 0x400921fb54442d18L    # Math.PI

    invoke-direct {v6, v7, v8}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, p6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v7, 0xb

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v7, 0xb

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_1

    .line 407
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_a
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->coastDetectionSent:Z

    if-eqz v5, :cond_b

    .line 408
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v4

    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_2

    .line 410
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_b
    new-instance v5, Ljava/math/BigDecimal;

    invoke-virtual/range {p8 .. p8}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0x7d0

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v7, 0xf

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v3}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0xa

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, p6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v7, 0xd

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_2

    .line 427
    .end local v4    # "retVal":Ljava/math/BigDecimal;
    :cond_c
    new-instance v5, Ljava/math/BigDecimal;

    const v6, 0x1d4c0

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    invoke-virtual/range {p7 .. p7}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v7, 0x3

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    .restart local v4    # "retVal":Ljava/math/BigDecimal;
    goto/16 :goto_3
.end method

.method public getEventList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrDevice:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->cadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->instCadEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v6, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->pwrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->torqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->spdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->distEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v1, v0, v5

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 0

    .prologue
    .line 473
    return-void
.end method
