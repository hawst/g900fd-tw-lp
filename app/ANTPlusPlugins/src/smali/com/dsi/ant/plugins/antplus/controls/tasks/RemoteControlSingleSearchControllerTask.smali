.class public Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;
.super Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;
.source "RemoteControlSingleSearchControllerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/EnumSet;IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    .locals 0
    .param p2, "deviceNumber"    # I
    .param p3, "rfFreq"    # I
    .param p4, "period"    # I
    .param p5, "devType"    # I
    .param p6, "transType"    # I
    .param p7, "proximityThreshold"    # I
    .param p8, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;IIIIII",
            "Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    invoke-direct/range {p0 .. p8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;-><init>(Ljava/util/EnumSet;IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 41
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "interrupted":Z
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->channelFailureOccurred:Z

    .line 115
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 116
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->initSearch()Z

    move-result v2

    if-nez v2, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->enableMessageProcessing()V

    .line 120
    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->setPerSearchParamsAndOpenSearch(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_2

    .line 126
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    .line 136
    :cond_3
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->dumbfireCloseChannel()V

    .line 139
    :cond_4
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->channelFailureOccurred:Z

    if-eqz v2, :cond_5

    .line 140
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2

    .line 128
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->disableMessageProcessing()V

    .line 131
    const/4 v1, 0x1

    goto :goto_1

    .line 142
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_0

    .line 144
    if-eqz v1, :cond_6

    .line 146
    const/16 v2, -0x16

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 148
    :cond_6
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    if-nez v2, :cond_7

    .line 150
    const/4 v2, -0x4

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 152
    :cond_7
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->mModesMatchCapabilities:Z

    if-nez v2, :cond_8

    .line 154
    const/16 v2, -0x1a

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 158
    :cond_8
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;

    const/4 v5, 0x1

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->reportResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;Z)V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    const-string v0, "Remote Control Single Search Controller"

    return-object v0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 48
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 51
    :pswitch_0
    new-instance v3, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v3, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    .line 54
    .local v0, "channelEvent":Lcom/dsi/ant/message/EventCode;
    sget-object v3, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_1

    .line 56
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timeout occured"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->disableMessageProcessing()V

    .line 104
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->channelFailureOccurred:Z

    .line 105
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 58
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_0

    .line 60
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->disableMessageProcessing()V

    .line 62
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 69
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :pswitch_1
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->cancelled:Z

    if-nez v3, :cond_0

    .line 71
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->mReceivedPageCounter:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->mReceivedPageCounter:I

    .line 72
    invoke-virtual {p0, p2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->DetermineCapabilities(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 73
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->mCapabilitiesDetermined:Z

    if-eqz v3, :cond_0

    .line 76
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 79
    invoke-static {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/message/fromant/DataMessage;

    .line 80
    .local v2, "msg":Lcom/dsi/ant/message/fromant/DataMessage;
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getMeasurementType()Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    if-ne v3, v4, :cond_2

    .line 83
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getRssiValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;

    .line 89
    :goto_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->disableMessageProcessing()V

    .line 90
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 86
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
