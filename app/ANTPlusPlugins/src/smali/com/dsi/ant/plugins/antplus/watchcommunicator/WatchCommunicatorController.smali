.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.source "WatchCommunicatorController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;,
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field public deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

.field public newActivityListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;",
            ">;"
        }
    .end annotation
.end field

.field private parentService:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

.field private updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;)V
    .locals 3
    .param p1, "antChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "watchService"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 73
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;I)V

    .line 63
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    .line 74
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->parentService:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

    .line 76
    new-instance v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 77
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 78
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    const-string v2, "WatchCommunicator"

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 79
    new-instance v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-direct {v1, p2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    .line 81
    new-instance v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    .line 85
    :try_start_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$1;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)V

    invoke-direct {v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 101
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException during initizalization"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->closeDevice()V

    .line 97
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->parentService:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

    return-object v0
.end method

.method private getTargetDeviceDetailedInfo(Ljava/util/UUID;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .locals 5
    .param p1, "requestTarget"    # Ljava/util/UUID;

    .prologue
    .line 492
    const/4 v0, 0x0

    .line 493
    .local v0, "devToConnect":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v4, v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 495
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 497
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    iget-object v3, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 499
    move-object v0, v1

    .line 503
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    if-nez v0, :cond_2

    .line 508
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->open()V

    .line 509
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getDevice(Ljava/util/UUID;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    move-result-object v0

    .line 510
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->close()V

    .line 513
    if-nez v0, :cond_3

    .line 514
    const/4 v3, 0x0

    .line 523
    :goto_0
    return-object v3

    .line 503
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 519
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->open()V

    .line 520
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->insertDeviceIfNotExist(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J

    .line 521
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->close()V

    :cond_3
    move-object v3, v0

    .line 523
    goto :goto_0
.end method

.method private prepareAndStartDownloadActivitiesTask(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Ljava/util/UUID;ZZ)V
    .locals 19
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "requestTarget"    # Ljava/util/UUID;
    .param p3, "useProgressUpdates"    # Z
    .param p4, "isDownloadNewOnly"    # Z

    .prologue
    .line 465
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->getTargetDeviceDetailedInfo(Ljava/util/UUID;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    move-result-object v7

    .line 467
    .local v7, "devToConnect":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    if-nez v7, :cond_0

    .line 469
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v18

    .line 470
    .local v18, "response":Landroid/os/Message;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    iput v3, v0, Landroid/os/Message;->what:I

    .line 471
    const/16 v3, 0xca

    move-object/from16 v0, v18

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 472
    new-instance v17, Landroid/os/Bundle;

    invoke-direct/range {v17 .. v17}, Landroid/os/Bundle;-><init>()V

    .line 473
    .local v17, "b":Landroid/os/Bundle;
    const-string v3, "int_statusCode"

    const/16 v4, -0x32

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 474
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 475
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 488
    .end local v17    # "b":Landroid/os/Bundle;
    .end local v18    # "response":Landroid/os/Message;
    :goto_0
    return-void

    .line 479
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    new-instance v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;

    move-object/from16 v4, p0

    move/from16 v5, p4

    move-object/from16 v6, p1

    move/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->parentService:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

    invoke-static {v4}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v11

    iget-object v4, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v4}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v13

    iget v14, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkRfFreq:I

    iget v15, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkPeriod:I

    const/16 v16, 0x1e

    move-object v8, v10

    move-object v10, v3

    invoke-static/range {v8 .. v16}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;

    goto :goto_0
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 12
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 131
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 132
    .local v2, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    .line 133
    .local v8, "response":Landroid/os/Message;
    iget v9, p2, Landroid/os/Message;->what:I

    iput v9, v8, Landroid/os/Message;->what:I

    .line 134
    iget v9, p2, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    .line 208
    invoke-virtual {v8}, Landroid/os/Message;->recycle()V

    .line 209
    const/4 v8, 0x0

    .line 210
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 213
    :goto_0
    return-void

    .line 138
    :pswitch_0
    const/4 v9, 0x0

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 139
    invoke-virtual {p0, v2, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 140
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    invoke-virtual {v9, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->requestInstantUpdate(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    goto :goto_0

    .line 146
    :pswitch_1
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 148
    .local v5, "params":Landroid/os/Bundle;
    const-string v9, "uuid_targetDeviceUUID"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/UUID;

    .line 150
    .local v6, "reqTarget":Ljava/util/UUID;
    const/4 v9, 0x0

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 151
    invoke-virtual {p0, v2, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 152
    const-string v9, "bool_UseAntFsProgressUpdates"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    iget v9, p2, Landroid/os/Message;->what:I

    const/16 v11, 0x4e23

    if-ne v9, v11, :cond_0

    const/4 v9, 0x1

    :goto_1
    invoke-direct {p0, v2, v6, v10, v9}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->prepareAndStartDownloadActivitiesTask(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Ljava/util/UUID;ZZ)V

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    goto :goto_1

    .line 161
    .end local v5    # "params":Landroid/os/Bundle;
    .end local v6    # "reqTarget":Ljava/util/UUID;
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 162
    .restart local v5    # "params":Landroid/os/Bundle;
    const-string v9, "uuid_targetDeviceUUID"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/UUID;

    .line 164
    .local v7, "requestTarget":Ljava/util/UUID;
    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->getTargetDeviceDetailedInfo(Ljava/util/UUID;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    move-result-object v3

    .line 166
    .local v3, "devToConnect":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    if-nez v3, :cond_1

    .line 168
    const/4 v9, -0x3

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 169
    invoke-virtual {p0, v2, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 173
    :cond_1
    const/4 v9, 0x0

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 174
    invoke-virtual {p0, v2, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 175
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    .line 176
    .local v4, "deviceWaitController":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    if-nez v4, :cond_2

    .line 178
    new-instance v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    .end local v4    # "deviceWaitController":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    const/4 v9, 0x1

    invoke-direct {v4, p0, v9, v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;ZLcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V

    .line 179
    .restart local v4    # "deviceWaitController":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    invoke-interface {v9, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_2
    invoke-virtual {v4, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->addNewWaitClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    goto :goto_0

    .line 187
    .end local v3    # "devToConnect":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v4    # "deviceWaitController":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    .end local v5    # "params":Landroid/os/Bundle;
    .end local v7    # "requestTarget":Ljava/util/UUID;
    :pswitch_3
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 188
    .restart local v5    # "params":Landroid/os/Bundle;
    const-string v9, "uuid_targetDeviceUUID"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/UUID;

    .line 190
    .restart local v7    # "requestTarget":Ljava/util/UUID;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    .line 191
    .restart local v4    # "deviceWaitController":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    if-eqz v4, :cond_3

    .line 192
    invoke-virtual {v4, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->removeWaitClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    .line 194
    :cond_3
    const/4 v9, 0x0

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 195
    invoke-virtual {p0, v2, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 197
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 198
    .local v1, "cancelEvent":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 199
    .local v0, "b":Landroid/os/Bundle;
    const/4 v9, 0x1

    iput v9, v1, Landroid/os/Message;->what:I

    .line 200
    const/16 v9, 0xcb

    iput v9, v1, Landroid/os/Message;->arg1:I

    .line 201
    const-string v9, "uuid_targetDeviceUUID"

    invoke-virtual {v0, v9, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 202
    const/16 v9, 0xca

    iput v9, v1, Landroid/os/Message;->arg2:I

    .line 203
    const-string v9, "int_statusCode"

    const/4 v10, -0x2

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 204
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x4e21
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 3
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 117
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v0

    return v0
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 125
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 126
    return-void
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v2, 0xc9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 108
    .local v0, "eventSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    return-object v0
.end method
