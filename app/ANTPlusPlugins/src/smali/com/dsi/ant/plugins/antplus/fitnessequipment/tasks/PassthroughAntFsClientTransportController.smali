.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "PassthroughAntFsClientTransportController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$1;,
        Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;,
        Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;
    }
.end annotation


# static fields
.field private static final ANTFS_COMMAND_RESPONSE:B = 0x44t

.field private static final AUTH_COMMAND:B = 0x4t

.field private static final AUTH_COMMAND_PASSTHRU:B = 0x0t

.field private static final AUTH_COMMAND_SERIAL:B = 0x1t

.field private static final AUTH_RESPONSE:B = -0x7ct

.field private static final AUTH_RESPONSE_ACCEPT:B = 0x1t

.field private static final AUTH_RESPONSE_NA:B = 0x0t

.field private static final AUTH_RESPONSE_REJECT:B = 0x2t

.field private static final DOWNLOAD_COMMAND:B = 0x9t

.field private static final DOWNLOAD_RESPONSE:B = -0x77t

.field private static final ERASE_COMMAND:B = 0xbt

.field private static final ERASE_RESPONSE:B = -0x75t

.field private static final LINK_COMMAND:B = 0x2t

.field private static final TAG:Ljava/lang/String;

.field private static final UPLOAD_COMMAND:B = 0xat

.field private static final UPLOAD_RESPONSE:B = -0x76t


# instance fields
.field private authSucceeded:Z

.field private beacon:[B

.field private connectedFreq:I

.field private connectedPeriod:I

.field private currentState:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

.field private devType:I

.field private deviceNumber:I

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

.field private hostReqPeriod:I

.field private hostSerial:J

.field private linkBeacon:[B

.field private period:I

.field private resultReceiver:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;

.field private rfFreq:I

.field private selectedFiles:[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

.field private final sendStateLock:Ljava/lang/Object;

.field private settingsFile:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

.field private transType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIIIILcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V
    .locals 6
    .param p1, "deviceId"    # I
    .param p2, "rfFreq"    # I
    .param p3, "period"    # I
    .param p4, "devType"    # I
    .param p5, "transType"    # I
    .param p6, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;
    .param p7, "settingsFile"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .param p8, "selectedFiles"    # [Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 51
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->sendStateLock:Ljava/lang/Object;

    .line 66
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    .line 68
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authSucceeded:Z

    .line 71
    const/16 v3, 0x8

    new-array v3, v3, [B

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->linkBeacon:[B

    .line 83
    const/16 v3, 0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    .line 101
    iput-object p6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->resultReceiver:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;

    .line 102
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->deviceNumber:I

    .line 103
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->rfFreq:I

    .line 104
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->period:I

    .line 105
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->devType:I

    .line 106
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->transType:I

    .line 107
    iput-object p7, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->settingsFile:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    .line 108
    iput-object p8, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->selectedFiles:[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    .line 111
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;-><init>()V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    .line 112
    if-eqz p7, :cond_0

    .line 114
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;-><init>()V

    .line 115
    .local v2, "settings":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    const/4 v3, 0x1

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    .line 116
    const/16 v3, 0x80

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    .line 117
    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    const/4 v4, 0x0

    invoke-virtual {p7}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getFileType()S

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 118
    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 119
    const/4 v3, 0x0

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataTypeSpecificFlags:I

    .line 120
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    iput-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    .line 121
    invoke-virtual {p7}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getRawBytes()[B

    move-result-object v3

    array-length v3, v3

    int-to-long v3, v3

    iput-wide v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    .line 122
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    iput-wide v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    .line 123
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    iget v4, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 126
    .end local v2    # "settings":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    :cond_0
    if-eqz p8, :cond_1

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p8

    if-ge v1, v3, :cond_1

    .line 130
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;-><init>()V

    .line 131
    .local v0, "entry":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    add-int/lit8 v3, v1, 0x2

    iput v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    .line 132
    const/16 v3, 0x80

    iput v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    .line 133
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    const/4 v4, 0x0

    aget-object v5, p8, v1

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getFileType()S

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 134
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    const/4 v4, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-static {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 135
    const/4 v3, 0x1

    iput v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataTypeSpecificFlags:I

    .line 136
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    .line 137
    aget-object v3, p8, v1

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getRawBytes()[B

    move-result-object v3

    array-length v3, v3

    int-to-long v3, v3

    iput-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    .line 138
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    iput-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    .line 139
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    iget v4, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    invoke-virtual {v3, v4, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 128
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "entry":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v1    # "i":I
    :cond_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->LINK:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->currentState:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    .line 144
    return-void

    .line 71
    nop

    :array_0
    .array-data 1
        0x43t
        0x23t
        0x0t
        0x0t
        0x1t
        0x0t
        0xft
        -0x80t
    .end array-data
.end method

.method private authStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 11
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 212
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 218
    :pswitch_0
    sget-object v5, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v5, :cond_1

    .line 220
    new-instance v3, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v3, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 221
    .local v3, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 226
    .end local v3    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    :cond_1
    const/16 v5, 0x44

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    aget-byte v6, v6, v8

    if-ne v5, v6, :cond_0

    const/4 v5, 0x4

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    aget-byte v6, v6, v9

    if-ne v5, v6, :cond_0

    .line 229
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v2, v5, v6

    .line 230
    .local v2, "authRequested":B
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom4LeBytes([BI)I

    move-result v5

    int-to-long v0, v5

    .line 232
    .local v0, "authHostSerial":J
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received authentication request, command type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    if-ne v2, v8, :cond_2

    .line 237
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupAuthenticationResponse(B)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v8, v5, v9

    .line 243
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-virtual {v5, v6}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto :goto_0

    .line 238
    :catch_0
    move-exception v4

    .line 240
    .local v4, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to transmit serial number, reason: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 245
    .end local v4    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    if-nez v2, :cond_3

    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    cmp-long v5, v5, v0

    if-nez v5, :cond_3

    .line 247
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authSucceeded:Z

    .line 248
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->TRANSITION:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v5}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 249
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->disableMessageProcessing()V

    .line 250
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 257
    :cond_3
    :try_start_1
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v6, "Authentication request rejected"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupAuthenticationResponse(B)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 264
    :goto_2
    iput-boolean v10, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authSucceeded:Z

    .line 265
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->TRANSITION:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v5}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 266
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->disableMessageProcessing()V

    .line 267
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 259
    :catch_1
    move-exception v4

    .line 261
    .restart local v4    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to send authentication response, reason: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->currentState:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    .line 154
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->TRANSITION:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    if-eq p1, v0, :cond_0

    .line 155
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->reportState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 156
    :cond_0
    return-void
.end method

.method private combineArrays([B[B)[B
    .locals 4
    .param p1, "one"    # [B
    .param p2, "two"    # [B

    .prologue
    const/4 v3, 0x0

    .line 548
    array-length v1, p1

    array-length v2, p2

    add-int/2addr v1, v2

    new-array v0, v1, [B

    .line 550
    .local v0, "combined":[B
    array-length v1, p1

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 551
    array-length v1, p1

    array-length v2, p2

    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 553
    return-object v0
.end method

.method private linkStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 193
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-eq p1, v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    const/16 v0, 0x44

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v3

    if-ne v3, v0, :cond_0

    .line 199
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedFreq:I

    .line 200
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostReqPeriod:I

    .line 201
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom4LeBytes([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    .line 203
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->TRANSITION:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 204
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->disableMessageProcessing()V

    .line 205
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0
.end method

.method private readFile(I)[B
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 456
    if-nez p1, :cond_1

    .line 458
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->toByteArray()[B

    move-result-object v0

    .line 476
    :cond_0
    :goto_0
    return-object v0

    .line 463
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->fsDir:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 468
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 470
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->settingsFile:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getRawBytes()[B

    move-result-object v0

    goto :goto_0

    .line 473
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->selectedFiles:[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    if-gt p1, v1, :cond_0

    .line 476
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->selectedFiles:[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    add-int/lit8 v1, p1, -0x2

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getRawBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private setupAuthenticationResponse(B)[B
    .locals 4
    .param p1, "response"    # B

    .prologue
    const/4 v3, 0x2

    .line 481
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x3

    aput-byte v2, v1, v3

    .line 482
    const/16 v1, 0x8

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 484
    .local v0, "authResponse":[B
    aput-byte p1, v0, v3

    .line 485
    const/4 v1, 0x4

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->deviceNumber:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutSignedNumIn4LeBytes([BIJ)V

    .line 486
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-direct {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->combineArrays([B[B)[B

    move-result-object v1

    return-object v1

    .line 482
    nop

    :array_0
    .array-data 1
        0x44t
        -0x7ct
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private setupDownloadResponse(I)[B
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v9, 0x0

    .line 491
    const/16 v5, 0x10

    new-array v3, v5, [B

    fill-array-data v3, :array_0

    .line 497
    .local v3, "header":[B
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->readFile(I)[B

    move-result-object v2

    .line 499
    .local v2, "fileData":[B
    if-nez v2, :cond_0

    .line 501
    const/4 v5, 0x1

    aput-byte v5, v3, v8

    .line 503
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-direct {p0, v5, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->combineArrays([B[B)[B

    move-result-object v4

    .line 521
    :goto_0
    return-object v4

    .line 506
    :cond_0
    const/4 v5, 0x4

    array-length v6, v2

    int-to-long v6, v6

    invoke-static {v3, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 507
    const/16 v5, 0xc

    array-length v6, v2

    int-to-long v6, v6

    invoke-static {v3, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 510
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;-><init>()V

    .line 511
    .local v0, "crc":Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;
    array-length v5, v2

    invoke-virtual {v0, v2, v9, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->update([BII)V

    .line 514
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v6, 0x3

    aput-byte v6, v5, v8

    .line 515
    array-length v5, v2

    int-to-double v5, v5

    const-wide/high16 v7, 0x4020000000000000L    # 8.0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/lit8 v1, v5, 0x8

    .line 516
    .local v1, "dataSize":I
    add-int/lit8 v5, v1, 0x20

    new-array v4, v5, [B

    .line 517
    .local v4, "response":[B
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    array-length v6, v6

    invoke-static {v5, v9, v4, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 518
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    array-length v5, v5

    array-length v6, v3

    invoke-static {v3, v9, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 519
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    array-length v5, v5

    array-length v6, v3

    add-int/2addr v5, v6

    array-length v6, v2

    invoke-static {v2, v9, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 520
    array-length v5, v4

    add-int/lit8 v5, v5, -0x2

    const-wide/32 v6, 0xffff

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->getValue()J

    move-result-wide v8

    and-long/2addr v6, v8

    long-to-int v6, v6

    invoke-static {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    goto :goto_0

    .line 491
    :array_0
    .array-data 1
        0x44t
        -0x77t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private setupEraseResponseReject()[B
    .locals 4

    .prologue
    .line 540
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x2

    const/4 v3, 0x3

    aput-byte v3, v1, v2

    .line 541
    const/16 v1, 0x8

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 543
    .local v0, "eraseResponse":[B
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-direct {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->combineArrays([B[B)[B

    move-result-object v1

    return-object v1

    .line 541
    :array_0
    .array-data 1
        0x44t
        -0x75t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private setupUploadResponseReject()[B
    .locals 4

    .prologue
    .line 527
    const/16 v1, 0x18

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 534
    .local v0, "uploadResponse":[B
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x2

    const/4 v3, 0x3

    aput-byte v3, v1, v2

    .line 535
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-direct {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->combineArrays([B[B)[B

    move-result-object v1

    return-object v1

    .line 527
    :array_0
    .array-data 1
        0x44t
        -0x76t
        0x4t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private startAuthState()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const-wide/16 v8, 0xff

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 383
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 384
    iput-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authSucceeded:Z

    .line 385
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/16 v2, 0x43

    aput-byte v2, v1, v6

    .line 387
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->updateBeaconChannelPeriod()V

    .line 389
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x2

    aput-byte v3, v1, v2

    .line 390
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v6, v1, v7

    .line 391
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    and-long/2addr v3, v8

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 392
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    const/16 v5, 0x8

    shr-long/2addr v3, v5

    and-long/2addr v3, v8

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 393
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x6

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    const/16 v5, 0x10

    shr-long/2addr v3, v5

    and-long/2addr v3, v8

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 394
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/4 v2, 0x7

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostSerial:J

    const/16 v5, 0x18

    shr-long/2addr v3, v5

    and-long/2addr v3, v8

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 397
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestAntVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->getProductFamily()Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "productFamily":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v7, :cond_0

    .line 399
    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_0
    const-string v1, "AJK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "AP2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1, v7}, Lcom/dsi/ant/channel/AntChannel;->setTransmitPower(I)V

    .line 404
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 405
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 406
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->AUTHENTICATE:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 407
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->enableMessageProcessing()V

    .line 408
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedFreq:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 409
    return-void
.end method

.method private startLinkState()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    .line 360
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->flushAndEnsureClosedChannel()V

    .line 362
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 363
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/message/ChannelId;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->deviceNumber:I

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->devType:I

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->transType:I

    invoke-direct {v2, v3, v4, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 364
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->period:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 365
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->linkBeacon:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 366
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->LINK:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 367
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->enableMessageProcessing()V

    .line 368
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->rfFreq:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 371
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestAntVersion()Lcom/dsi/ant/message/fromant/AntVersionMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->getProductFamily()Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "productFamily":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v6, :cond_0

    .line 373
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 375
    :cond_0
    const-string v1, "AJK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "AP2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 376
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setTransmitPower(I)V

    .line 378
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 379
    return-void
.end method

.method private startTransportState()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 444
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 445
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v2, v0, v2

    .line 447
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->TRANSPORT:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->changeState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 448
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->enableMessageProcessing()V

    .line 450
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 451
    return-void
.end method

.method private transportStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    .line 279
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 284
    :pswitch_0
    sget-object v4, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v4, :cond_1

    .line 286
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 287
    .local v0, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 291
    .end local v0    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    :cond_1
    const/16 v4, 0x44

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v4, v5, :cond_0

    .line 293
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v1, v4, v7

    .line 294
    .local v1, "command":B
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received transport command: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/16 v4, 0x9

    if-ne v4, v1, :cond_3

    .line 299
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    invoke-static {v4, v8}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    .line 302
    .local v3, "requestedIndex":I
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupDownloadResponse(I)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    .end local v3    # "requestedIndex":I
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v7, v4, v7

    .line 350
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto :goto_0

    .line 303
    .restart local v3    # "requestedIndex":I
    :catch_0
    move-exception v2

    .line 305
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to send download response, reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 308
    .end local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .end local v3    # "requestedIndex":I
    :cond_3
    const/16 v4, 0xa

    if-ne v4, v1, :cond_4

    .line 312
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupUploadResponseReject()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 313
    :catch_1
    move-exception v2

    .line 315
    .restart local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to transmit upload response, reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    .end local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_4
    const/16 v4, 0xb

    if-ne v4, v1, :cond_5

    .line 322
    :try_start_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupEraseResponseReject()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 323
    :catch_2
    move-exception v2

    .line 325
    .restart local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to transmit erase response, reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 328
    .end local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    if-ne v7, v1, :cond_2

    .line 330
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v8

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedFreq:I

    .line 331
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x4

    aget-byte v4, v4, v5

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostReqPeriod:I

    .line 332
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->updateBeaconChannelPeriod()V

    .line 335
    :try_start_3
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Changing channel period to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and Rf to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedFreq:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 337
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 338
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedFreq:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 339
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 340
    :catch_3
    move-exception v2

    .line 343
    .restart local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACFE occurred when handling link request in transport state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_1

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateBeaconChannelPeriod()V
    .locals 4

    .prologue
    const v3, 0xffff

    const/16 v1, 0x20

    const/4 v2, 0x1

    .line 413
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->hostReqPeriod:I

    packed-switch v0, :pswitch_data_0

    .line 436
    iput v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 437
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v1, v0, v2

    .line 440
    :goto_0
    return-void

    .line 416
    :pswitch_0
    iput v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 417
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    aput-byte v1, v0, v2

    goto :goto_0

    .line 420
    :pswitch_1
    const v0, 0x8000

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 421
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/16 v1, 0x21

    aput-byte v1, v0, v2

    goto :goto_0

    .line 424
    :pswitch_2
    const/16 v0, 0x4000

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 425
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/16 v1, 0x22

    aput-byte v1, v0, v2

    goto :goto_0

    .line 428
    :pswitch_3
    const/16 v0, 0x2000

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 429
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/16 v1, 0x23

    aput-byte v1, v0, v2

    goto :goto_0

    .line 432
    :pswitch_4
    const/16 v0, 0x1000

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->connectedPeriod:I

    .line 433
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->beacon:[B

    const/16 v1, 0x24

    aput-byte v1, v0, v2

    goto :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public doWork()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    .line 562
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->flushAndEnsureUnassignedChannel()V

    .line 563
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v4, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 569
    :goto_0
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->startLinkState()V

    .line 570
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 574
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->startAuthState()V

    .line 575
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0xa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 577
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Timed out in Auth state, dropping back to link state"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 619
    :catch_0
    move-exception v1

    .line 621
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE occurred: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 630
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :goto_1
    return-void

    .line 581
    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authSucceeded:Z

    if-nez v3, :cond_1

    .line 583
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Authentication rejected, dropping back to link state"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 624
    :catch_1
    move-exception v1

    .line 626
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Interrupted waiting for result"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 628
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 588
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Send authentication response"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->setupAuthenticationResponse(B)[B
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    .line 590
    .local v0, "authResponse":[B
    const/4 v2, 0x0

    .line 591
    .local v2, "msgRetries":I
    :goto_2
    if-ge v2, v7, :cond_2

    .line 595
    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3, v0}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 605
    :cond_2
    if-le v2, v7, :cond_3

    .line 607
    :try_start_4
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Retry limit exceeded for auth response, dropping back to link state"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    :catch_2
    move-exception v1

    .line 600
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to transmit auth response, reason: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    add-int/lit8 v2, v2, 0x1

    .line 602
    goto :goto_2

    .line 615
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_3
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v4, "Start transport state"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->startTransportState()V

    .line 617
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1
.end method

.method public getCurrentState()Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->currentState:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    return-object v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    const-string v0, "Passthrough ANT-FS Transport Controller"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 648
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 170
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$1;->$SwitchMap$com$dsi$ant$plugins$antplus$fitnessequipment$tasks$PassthroughAntFsClientTransportController$AntFsStates:[I

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->currentState:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 184
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->TAG:Ljava/lang/String;

    const-string v1, "Message received but lost because task is in unknown state"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :goto_0
    :pswitch_0
    return-void

    .line 173
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->linkStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 176
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->authStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 179
    :pswitch_3
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->transportStateMessageHandler(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected reportState(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V
    .locals 2
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->sendStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;->resultReceiver:Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;

    invoke-interface {v0, p1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;->onStateChange(Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStates;)V

    .line 163
    monitor-exit v1

    .line 164
    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
