.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
.super Ljava/lang/Enum;
.source "AntPlusBikePowerPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SensorSoftwareMismatchStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

.field public static final enum MISMATCH_LEFT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

.field public static final enum MISMATCH_RIGHT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

.field public static final enum SW_MATCHES:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

.field public static final enum UNDEFINED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1124
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->UNDEFINED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    .line 1129
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    const-string v1, "MISMATCH_RIGHT_SENSOR_OLDER"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_RIGHT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    .line 1134
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    const-string v1, "MISMATCH_LEFT_SENSOR_OLDER"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_LEFT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    .line 1139
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    const-string v1, "SW_MATCHES"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->SW_MATCHES:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    .line 1119
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->UNDEFINED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_RIGHT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_LEFT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->SW_MATCHES:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    .locals 2
    .param p0, "intValue"    # I

    .prologue
    .line 1157
    packed-switch p0, :pswitch_data_0

    .line 1168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Undefined Sensor Software Mismatch Status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->UNDEFINED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    .line 1166
    :goto_0
    return-object v0

    .line 1162
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_RIGHT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    goto :goto_0

    .line 1164
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->MISMATCH_LEFT_SENSOR_OLDER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    goto :goto_0

    .line 1166
    :pswitch_3
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->SW_MATCHES:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    goto :goto_0

    .line 1157
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1119
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    .locals 1

    .prologue
    .line 1119
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 1147
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->ordinal()I

    move-result v0

    return v0
.end method
