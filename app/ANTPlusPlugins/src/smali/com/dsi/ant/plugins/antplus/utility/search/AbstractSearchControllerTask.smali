.class public abstract Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "AbstractSearchControllerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected cancelled:Z

.field protected mChanId:Lcom/dsi/ant/message/ChannelId;

.field protected mPeriod:I

.field protected mProximityThreshold:I

.field protected mRfFreq:I

.field private resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

.field protected resultSent:Z

.field protected searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private final sendResultLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIILcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    .locals 2
    .param p1, "rfFreq"    # I
    .param p2, "period"    # I
    .param p3, "proximityThreshold"    # I
    .param p4, "channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p5, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->sendResultLock:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 47
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mPeriod:I

    .line 48
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mRfFreq:I

    .line 49
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mProximityThreshold:I

    .line 51
    if-nez p4, :cond_0

    .line 52
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ChannelId is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    .line 55
    if-nez p5, :cond_1

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ResultReceiver is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    .line 58
    return-void
.end method


# virtual methods
.method protected dumbfireCloseChannel()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->close()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-eq v1, v2, :cond_0

    .line 199
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing channel in single search cleanup failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 77
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->handleInterruptRequest(I)Z

    .line 78
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 2
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v1, 0x1

    .line 68
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    .line 69
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->disableMessageProcessing()V

    .line 70
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 71
    return v1
.end method

.method protected initSearch()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 123
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->flushAndEnsureUnassignedChannel()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_0
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-eqz v4, :cond_2

    .line 155
    :cond_1
    :goto_0
    return v2

    .line 124
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Ljava/lang/InterruptedException;
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-nez v4, :cond_0

    .line 128
    const/16 v3, -0x16

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 138
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v5, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 139
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mRfFreq:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 140
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mPeriod:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 141
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    .line 142
    new-instance v0, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v0}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    .line 143
    .local v0, "config":Lcom/dsi/ant/message/LibConfig;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/dsi/ant/message/LibConfig;->setEnableRssiOutput(Z)V

    .line 144
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/channel/AntChannel;->setAdapterWideLibConfig(Lcom/dsi/ant/message/LibConfig;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 152
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-nez v4, :cond_1

    move v2, v3

    .line 155
    goto :goto_0

    .line 146
    .end local v0    # "config":Lcom/dsi/ant/message/LibConfig;
    :catch_1
    move-exception v1

    .line 148
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE initializing channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2
.end method

.method public initTask()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 110
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    .line 111
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->resultSent:Z

    .line 112
    return-void
.end method

.method protected reportFailure(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 86
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->reportResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;Z)V

    .line 87
    return-void
.end method

.method protected reportResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;Z)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;
    .param p4, "isLastResult"    # Z

    .prologue
    .line 97
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->sendResultLock:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->resultSent:Z

    if-nez v0, :cond_0

    .line 101
    iput-boolean p4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->resultSent:Z

    .line 102
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    invoke-interface {v0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;->onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V

    .line 104
    :cond_0
    monitor-exit v1

    .line 105
    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected setPerSearchParamsAndOpenSearch(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)Z
    .locals 5
    .param p1, "timeout"    # Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 167
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-eqz v3, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v1

    .line 172
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 173
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->mProximityThreshold:I

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setProximityThreshold(I)V

    .line 174
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3, p1}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    .line 176
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 178
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->cancelled:Z

    if-nez v3, :cond_0

    .line 181
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 188
    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE opening channel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method
