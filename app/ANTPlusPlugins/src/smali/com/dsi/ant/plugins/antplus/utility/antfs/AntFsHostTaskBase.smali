.class public abstract Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "AntFsHostTaskBase.java"


# instance fields
.field mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field private result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V
    .locals 0
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .line 19
    return-void
.end method


# virtual methods
.method public getTaskResult()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 3

    .prologue
    .line 24
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 25
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 26
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public abstract isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
.end method

.method public setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    .locals 0
    .param p1, "result"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->result:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 36
    return-void
.end method
