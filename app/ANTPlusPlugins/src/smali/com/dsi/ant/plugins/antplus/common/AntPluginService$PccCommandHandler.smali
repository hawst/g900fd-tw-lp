.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;
.super Landroid/os/Handler;
.source "AntPluginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PccCommandHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1435
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    .line 1436
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1437
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v13, 0x0

    const/16 v12, 0x2775

    const/16 v11, 0x2774

    .line 1442
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PCC Msg Handler received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 1445
    .local v1, "b":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1446
    const-string v8, "uuid_AccessToken"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/UUID;

    .line 1448
    .local v7, "token":Ljava/util/UUID;
    if-eqz v7, :cond_6

    .line 1452
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_DeviceList:Ljava/util/TreeMap;

    invoke-virtual {v8, v7}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .local v5, "selectedDevice":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v5, :cond_3

    .line 1455
    iget v8, p1, Landroid/os/Message;->what:I

    if-eq v8, v12, :cond_0

    iget v8, p1, Landroid/os/Message;->what:I

    if-ne v8, v11, :cond_1

    .line 1458
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "cmd handler: async message received when target is a device. AntLib probably needs to be upgraded."

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    :cond_1
    invoke-virtual {v5, v7, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 1465
    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0x2712

    if-ne v8, v9, :cond_2

    .line 1466
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v8, v7, v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->revokeAccess(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;)V

    .line 1513
    .end local v5    # "selectedDevice":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :cond_2
    :goto_0
    return-void

    .line 1469
    .restart local v5    # "selectedDevice":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :cond_3
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    .local v0, "asyncScanInfo":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    if-eqz v0, :cond_6

    .line 1471
    iget v8, p1, Landroid/os/Message;->what:I

    if-ne v8, v11, :cond_4

    .line 1473
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 1474
    .local v3, "connectingMsg":Landroid/os/Message;
    iput v11, v3, Landroid/os/Message;->what:I

    .line 1475
    iput v13, v3, Landroid/os/Message;->arg1:I

    .line 1478
    :try_start_0
    iget-object v8, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1486
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v8, v0, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1479
    :catch_0
    move-exception v4

    .line 1481
    .local v4, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "RemoteException sending response to CONNECTTOASYNCRESULT cmd, closing scan."

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1482
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v8, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 1488
    .end local v3    # "connectingMsg":Landroid/os/Message;
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_4
    iget v8, p1, Landroid/os/Message;->what:I

    if-ne v8, v12, :cond_5

    .line 1490
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 1491
    .local v6, "successMsg":Landroid/os/Message;
    iput v12, v6, Landroid/os/Message;->what:I

    .line 1492
    iput v13, v6, Landroid/os/Message;->arg1:I

    .line 1493
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v9, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1495
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v8, v7}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 1499
    .end local v6    # "successMsg":Landroid/os/Message;
    :cond_5
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 1500
    .local v2, "badidMsg":Landroid/os/Message;
    iget v8, p1, Landroid/os/Message;->what:I

    iput v8, v2, Landroid/os/Message;->what:I

    .line 1501
    const v8, -0x5f5e0ff

    iput v8, v2, Landroid/os/Message;->arg1:I

    .line 1502
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v9, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1512
    .end local v0    # "asyncScanInfo":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    .end local v2    # "badidMsg":Landroid/os/Message;
    .end local v5    # "selectedDevice":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :cond_6
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Cmd Handler: Token missing or invalid!"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
