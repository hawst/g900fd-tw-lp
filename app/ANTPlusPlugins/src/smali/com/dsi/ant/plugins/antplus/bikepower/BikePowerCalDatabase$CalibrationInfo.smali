.class public Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
.super Ljava/lang/Object;
.source "BikePowerCalDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CalibrationInfo"
.end annotation


# instance fields
.field public antDeviceNumber:I

.field public calibrationValue:I

.field private calibration_dbId:I

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

.field public timestamp:J


# direct methods
.method private constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIIJ)V
    .locals 6
    .param p2, "calibration_dbId"    # I
    .param p3, "antDeviceNumber"    # I
    .param p4, "calibrationValue"    # I
    .param p5, "timestamp"    # J

    .prologue
    .line 32
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIJ)V

    .line 33
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibration_dbId:I

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIIJLcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # J
    .param p7, "x5"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$1;

    .prologue
    .line 16
    invoke-direct/range {p0 .. p6}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIIJ)V

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;IIJ)V
    .locals 0
    .param p2, "antDeviceNumber"    # I
    .param p3, "calibrationValue"    # I
    .param p4, "timestamp"    # J

    .prologue
    .line 24
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->antDeviceNumber:I

    .line 26
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibrationValue:I

    .line 27
    iput-wide p4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->timestamp:J

    .line 28
    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->getCalibration_dbId()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    .param p1, "x1"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->setCalibration_dbId(I)V

    return-void
.end method

.method private getCalibration_dbId()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibration_dbId:I

    return v0
.end method

.method private setCalibration_dbId(I)V
    .locals 0
    .param p1, "new_dbId"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibration_dbId:I

    .line 44
    return-void
.end method
