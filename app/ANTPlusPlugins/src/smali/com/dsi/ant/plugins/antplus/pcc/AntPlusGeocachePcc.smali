.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusGeocachePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IpcDefines;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

.field mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

.field mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

.field mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 777
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 737
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 777
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static requestListAndRequestAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "availableDeviceListRecevier"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 768
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 769
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/16 v3, 0x12c

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 771
    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;-><init>()V

    .line 772
    .local v2, "possibleRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;
    iput-object p3, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

    .line 774
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;-><init>()V

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 790
    const-string v0, "ANT+ Plugin: Geocache"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 782
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 783
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.geocache.GeocacheService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 784
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 16
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 796
    move-object/from16 v0, p1

    iget v13, v0, Landroid/os/Message;->arg1:I

    packed-switch v13, :pswitch_data_0

    .line 883
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unrecognized event received: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget v15, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_0
    :goto_0
    return-void

    .line 800
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

    if-eqz v13, :cond_0

    .line 803
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 804
    .local v3, "b":Landroid/os/Bundle;
    const-string v13, "arrayInt_deviceIDs"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v7

    .line 805
    .local v7, "deviceIDs":[I
    const-string v13, "arrayString_deviceIdentifierStrings"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 806
    .local v8, "deviceIdentifierStrings":[Ljava/lang/String;
    const-string v13, "int_changeCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    move-result-object v4

    .line 807
    .local v4, "changeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;
    const-string v13, "int_changingDeviceID"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 808
    .local v5, "changingDeviceID":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

    invoke-interface {v13, v7, v8, v4, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;->onNewAvailableDeviceList([I[Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;I)V

    goto :goto_0

    .line 814
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v4    # "changeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;
    .end local v5    # "changingDeviceID":I
    .end local v7    # "deviceIDs":[I
    .end local v8    # "deviceIdentifierStrings":[Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    if-eqz v13, :cond_0

    .line 817
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 818
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_workUnitsFinished"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 819
    .local v12, "workUnitsFinished":I
    const-string v13, "int_totalUnitsWork"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 820
    .local v11, "totalUnitsWork":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    invoke-interface {v13, v12, v11}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;->onNewSimpleProgressUpdate(II)V

    goto :goto_0

    .line 826
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v11    # "totalUnitsWork":I
    .end local v12    # "workUnitsFinished":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

    if-eqz v13, :cond_0

    .line 829
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 831
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 832
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v10

    .line 833
    .local v10, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

    invoke-interface {v13, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;->onNewProgrammingFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 839
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    if-eqz v13, :cond_0

    .line 842
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 844
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 845
    .restart local v3    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 846
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v10

    .line 847
    .restart local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getIntValue()I

    move-result v13

    if-ltz v13, :cond_2

    .line 849
    const-string v13, "bundle_downloadedData"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 851
    .local v9, "downloadedData":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->reportedServiceVersion:I

    if-nez v13, :cond_1

    .line 853
    invoke-static {v9}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyGeocacheCompat$GeocacheDeviceDataCompat_v1;->readGddFromBundleCompat_v1(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    move-result-object v6

    .line 859
    .local v6, "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    invoke-interface {v13, v10, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;->onNewDataDownloadFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;)V

    goto/16 :goto_0

    .line 857
    .end local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    :cond_1
    const-string v13, "parcelable_GeocacheDeviceData"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    .restart local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    goto :goto_1

    .line 863
    .end local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    .end local v9    # "downloadedData":Landroid/os/Bundle;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    const/4 v14, 0x0

    invoke-interface {v13, v10, v14}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;->onNewDataDownloadFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;)V

    goto/16 :goto_0

    .line 870
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

    if-eqz v13, :cond_0

    .line 873
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 875
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 876
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 877
    .local v10, "statusCode":I
    const-string v13, "long_authToken"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 878
    .local v1, "authToken":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v14

    invoke-interface {v13, v14, v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;->onNewAuthTokenRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;J)V

    goto/16 :goto_0

    .line 796
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public requestAuthToken(IIJLcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "targetDeviceID"    # I
    .param p2, "nonce"    # I
    .param p3, "serialNumber"    # J
    .param p5, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;
    .param p6, "progressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 982
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 984
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    :goto_0
    return v5

    .line 988
    :cond_0
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

    .line 989
    iput-object p6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .line 991
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 992
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e24

    iput v3, v0, Landroid/os/Message;->what:I

    .line 993
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 994
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 996
    const-string v3, "int_TARGETDEVICEID"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 997
    const-string v3, "int_nonce"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 998
    const-string v3, "long_serialNumber"

    invoke-virtual {v1, v3, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 999
    const-string v6, "bool_subscribeProgressUpdates"

    if-eqz p6, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1001
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 1003
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_2

    .line 1005
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestAuthToken died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_1
    move v3, v5

    .line 999
    goto :goto_1

    .line 1010
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_2
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_3

    .line 1013
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestAuthToken failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1015
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "requestAuthToken cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1018
    :cond_3
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 1019
    goto :goto_0
.end method

.method public requestCurrentDeviceList()Z
    .locals 5

    .prologue
    .line 897
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 898
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v2, 0x4e22

    iput v2, v0, Landroid/os/Message;->what:I

    .line 900
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 902
    .local v1, "ret":Landroid/os/Message;
    if-nez v1, :cond_0

    .line 904
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v3, "Cmd requestCurrentDeviceList died in sendPluginCommand()"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    const/4 v2, 0x0

    .line 916
    :goto_0
    return v2

    .line 908
    :cond_0
    iget v2, v1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    .line 911
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cmd requestCurrentDeviceList failed with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "requestCurrentDeviceList cmd failed internally"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 915
    :cond_1
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    .line 916
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public requestDeviceData(IZLcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "targetDeviceID"    # I
    .param p2, "updateVisitCount"    # Z
    .param p3, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;
    .param p4, "progressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 931
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 933
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    :goto_0
    return v5

    .line 937
    :cond_0
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    .line 938
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .line 940
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 941
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e23

    iput v3, v0, Landroid/os/Message;->what:I

    .line 942
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 943
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 945
    const-string v3, "int_TARGETDEVICEID"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 946
    const-string v3, "bool_updateVisitCount"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 947
    const-string v6, "bool_subscribeProgressUpdates"

    if-eqz p4, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 949
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 951
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_2

    .line 953
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestDeviceData died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_1
    move v3, v5

    .line 947
    goto :goto_1

    .line 958
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_2
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_3

    .line 961
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestDeviceData failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 963
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "requestDeviceData cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 966
    :cond_3
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 967
    goto :goto_0
.end method

.method public requestDeviceProgramming(IJZLcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;)Z
    .locals 8
    .param p1, "targetDeviceID"    # I
    .param p2, "programmingPIN"    # J
    .param p4, "clearAllExistingData"    # Z
    .param p5, "programmingData"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    .param p6, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;
    .param p7, "progressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1044
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1046
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v5, "Cmd failed to start because a local command is still processing."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    :goto_0
    return v6

    .line 1050
    :cond_0
    iput-object p6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

    .line 1051
    iput-object p7, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    .line 1053
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1054
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v4, 0x4e25

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1055
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1056
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1058
    const-string v4, "int_TARGETDEVICEID"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1059
    const-string v4, "long_ProgrammingPIN"

    invoke-virtual {v1, v4, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1060
    const-string v4, "bool_clearAllExistingData"

    invoke-virtual {v1, v4, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1061
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->reportedServiceVersion:I

    if-nez v4, :cond_1

    .line 1063
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1064
    .local v2, "programDataBundle":Landroid/os/Bundle;
    invoke-static {p5, v2}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyGeocacheCompat$GeocacheDeviceDataCompat_v1;->writePgddToBundle(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Landroid/os/Bundle;)V

    .line 1065
    const-string v4, "bundle_programmingData"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1071
    .end local v2    # "programDataBundle":Landroid/os/Bundle;
    :goto_1
    const-string v7, "bool_subscribeProgressUpdates"

    if-eqz p7, :cond_2

    move v4, v5

    :goto_2
    invoke-virtual {v1, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1073
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v3

    .line 1075
    .local v3, "ret":Landroid/os/Message;
    if-nez v3, :cond_3

    .line 1077
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    const-string v5, "Cmd requestDeviceProgramming died in sendPluginCommand()"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 1069
    .end local v3    # "ret":Landroid/os/Message;
    :cond_1
    const-string v4, "parcelable_ProgrammableGeocacheDeviceData"

    invoke-virtual {v1, v4, p5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    :cond_2
    move v4, v6

    .line 1071
    goto :goto_2

    .line 1082
    .restart local v3    # "ret":Landroid/os/Message;
    :cond_3
    iget v4, v3, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_4

    .line 1085
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cmd requestDeviceProgramming failed with code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1087
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "requestDeviceProgramming cmd failed internally"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1090
    :cond_4
    invoke-virtual {v3}, Landroid/os/Message;->recycle()V

    move v6, v5

    .line 1091
    goto/16 :goto_0
.end method
