.class public Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;
.super Landroid/app/Service;
.source "Service_DeviceDbProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;Landroid/os/Bundle;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->serviceRequest(Landroid/os/Bundle;I)V

    return-void
.end method

.method private serviceRequest(Landroid/os/Bundle;I)V
    .locals 26
    .param p1, "request"    # Landroid/os/Bundle;
    .param p2, "startId"    # I

    .prologue
    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 50
    const-string v22, "int_RequestType"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 52
    .local v18, "requestType":I
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v19, "result":Landroid/os/Bundle;
    const-string v22, "int_ResultCode"

    const/16 v23, -0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    packed-switch v18, :pswitch_data_0

    .line 119
    :pswitch_0
    :try_start_0
    sget-object v22, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    const-string v23, "Unrecognized command"

    invoke-static/range {v22 .. v23}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v22, "int_ResultCode"

    const/16 v23, -0x2

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :goto_0
    const-string v22, "msgr_ResultReceiver"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Landroid/os/Messenger;

    .line 129
    .local v20, "resultMessenger":Landroid/os/Messenger;
    :try_start_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v21

    .line 130
    .local v21, "ret":Landroid/os/Message;
    move/from16 v0, v18

    move-object/from16 v1, v21

    iput v0, v1, Landroid/os/Message;->what:I

    .line 131
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual/range {v20 .. v21}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 133
    sget-object v22, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "sent db result: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 139
    .end local v21    # "ret":Landroid/os/Message;
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->stopSelf(I)V

    .line 141
    return-void

    .line 60
    .end local v20    # "resultMessenger":Landroid/os/Messenger;
    :pswitch_1
    :try_start_2
    const-string v22, "int_AntDeviceNumber"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 61
    .local v11, "antDevId":I
    const-string v22, "string_PluginName"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 63
    .local v5, "pluginName":Ljava/lang/String;
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 66
    .local v3, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_3
    invoke-virtual {v3, v11, v5}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->getDeviceInfoByChanDevId(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v16

    .line 67
    .local v16, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    if-eqz v16, :cond_0

    .line 68
    const-string v22, "parcelable_DeviceDbInfo"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    :cond_0
    const-string v22, "int_ResultCode"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 73
    :try_start_4
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 126
    .end local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    .end local v5    # "pluginName":Ljava/lang/String;
    .end local v11    # "antDevId":I
    .end local v16    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :catchall_0
    move-exception v22

    const-string v23, "msgr_ResultReceiver"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Landroid/os/Messenger;

    .line 129
    .restart local v20    # "resultMessenger":Landroid/os/Messenger;
    :try_start_5
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v21

    .line 130
    .restart local v21    # "ret":Landroid/os/Message;
    move/from16 v0, v18

    move-object/from16 v1, v21

    iput v0, v1, Landroid/os/Message;->what:I

    .line 131
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual/range {v20 .. v21}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 133
    sget-object v23, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "sent db result: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    .line 139
    .end local v21    # "ret":Landroid/os/Message;
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->stopSelf(I)V

    .line 140
    throw v22

    .line 73
    .end local v20    # "resultMessenger":Landroid/os/Messenger;
    .restart local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    .restart local v5    # "pluginName":Ljava/lang/String;
    .restart local v11    # "antDevId":I
    :catchall_1
    move-exception v22

    :try_start_6
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v22

    .line 79
    .end local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    .end local v5    # "pluginName":Ljava/lang/String;
    .end local v11    # "antDevId":I
    :pswitch_2
    const-string v22, "parcelable_DeviceDbInfo"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 80
    .local v4, "devInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const-string v22, "string_PluginName"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 81
    .restart local v5    # "pluginName":Ljava/lang/String;
    const-string v22, "bundle_PluginDeviceParams"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v15

    .line 82
    .local v15, "deviceParams":Landroid/os/Bundle;
    const-string v22, "int_DevType"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 83
    .local v7, "devType":I
    const-string v22, "int_TransType"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 84
    .local v8, "transType":I
    const-string v22, "int_Period"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 85
    .local v9, "period":I
    const-string v22, "int_RfFreq"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 86
    .local v10, "rfFreq":I
    const-string v22, "predefinednetwork_NetKey"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 88
    .local v6, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 91
    .restart local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_7
    invoke-virtual/range {v3 .. v10}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->addDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Lcom/dsi/ant/channel/PredefinedNetwork;IIII)V

    .line 92
    const-string v22, "int_ResultCode"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 96
    :try_start_8
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v22

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v22

    .line 102
    .end local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    .end local v4    # "devInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v5    # "pluginName":Ljava/lang/String;
    .end local v6    # "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    .end local v7    # "devType":I
    .end local v8    # "transType":I
    .end local v9    # "period":I
    .end local v10    # "rfFreq":I
    .end local v15    # "deviceParams":Landroid/os/Bundle;
    :pswitch_3
    const-string v22, "parcelable_DeviceDbInfo"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 103
    .local v14, "dev":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const-string v22, "string_DISPLAYNAME"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 104
    .local v13, "chgVisibleName":Ljava/lang/String;
    const-string v22, "boolean_IsUserPreferredDevice"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 106
    .local v12, "chgIsPreferredDevice":Z
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 109
    .restart local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_9
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v3, v14, v13, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->updateDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 110
    const-string v22, "int_ResultCode"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 114
    :try_start_a
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    goto/16 :goto_0

    :catchall_3
    move-exception v22

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v22
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 134
    .end local v3    # "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    .end local v12    # "chgIsPreferredDevice":Z
    .end local v13    # "chgVisibleName":Ljava/lang/String;
    .end local v14    # "dev":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .restart local v20    # "resultMessenger":Landroid/os/Messenger;
    :catch_0
    move-exception v17

    .line 136
    .local v17, "e":Landroid/os/RemoteException;
    sget-object v22, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    const-string v23, "RemoteException sending result to query"

    invoke-static/range {v22 .. v23}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 134
    .end local v17    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v17

    .line 136
    .restart local v17    # "e":Landroid/os/RemoteException;
    sget-object v23, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    const-string v24, "RemoteException sending result to query"

    invoke-static/range {v23 .. v24}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    invoke-virtual {p0, p3}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->stopSelf(I)V

    .line 31
    :cond_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "received db request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v2, "com.dsi.ant.plugins.antplus.utility.db.devicedbrequest"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 34
    .local v0, "request":Landroid/os/Bundle;
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider$1;

    invoke-direct {v2, p0, v0, p3}, Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Service_DeviceDbProvider;Landroid/os/Bundle;I)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 42
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 44
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v2

    return v2
.end method
