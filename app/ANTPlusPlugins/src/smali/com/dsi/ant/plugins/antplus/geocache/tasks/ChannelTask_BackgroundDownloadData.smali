.class public Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_BackgroundDownloadData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field currentAckReq:[B

.field currentReqPageNum:I

.field deviceID:I

.field downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private final geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

.field msgsSinceFirstPageRequest:I

.field requestor:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field updateVisitCount:Z

.field updatingVisitCountState:Z

.field useProgressUpdates:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;Z)V
    .locals 2
    .param p1, "geocacheReceiver"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
    .param p2, "device"    # Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    .param p3, "updateVisitCount"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 41
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updatingVisitCountState:Z

    .line 42
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    .line 52
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .line 53
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 55
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->requestor:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 56
    iget-object v0, p2, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->deviceID:I

    .line 57
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->useProgressUpdates:Z

    .line 58
    iput-boolean p3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updateVisitCount:Z

    .line 59
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 220
    const/4 v2, -0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentReqPageNum:I

    .line 224
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->flushAndEnsureClosedChannel()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 235
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v3, Lcom/dsi/ant/message/ChannelId;

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->deviceID:I

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v5, 0x13

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 236
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->enableMessageProcessing()V

    .line 237
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->open()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 246
    :try_start_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 253
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e1":Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    goto :goto_0

    .line 238
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 240
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE initializing channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 247
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_2
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "Interrupted waiting for result"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Background Geocache Data Download "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->deviceID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 73
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->handleInterruptRequest(I)Z

    .line 74
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 2
    .param p1, "interruptingTaskRank"    # I

    .prologue
    .line 65
    sget-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v1, "Background download cancelling"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public handleProgressUpdate(II)V
    .locals 0
    .param p1, "done"    # I
    .param p2, "total"    # I

    .prologue
    .line 209
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v8, 0x0

    .line 81
    :try_start_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 84
    :pswitch_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v3, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v3, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 87
    :pswitch_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "Timed out attempting to find device"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v2, v3, :cond_5

    .line 187
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "Channel closed."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 94
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->deviceID:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->removeDeviceFromList(I)Z

    .line 95
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 96
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 99
    :pswitch_3
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updatingVisitCountState:Z

    if-eqz v2, :cond_0

    .line 103
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "Updated last visit count"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->reportVisitCountUpdated([B)V

    .line 105
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updatingVisitCountState:Z

    .line 106
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 107
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 116
    :pswitch_4
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v3, v3, 0x3

    rsub-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 126
    :pswitch_5
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2, p2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->DecodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 127
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v1

    .line 129
    .local v1, "nextPage":I
    if-gez v1, :cond_2

    .line 133
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updateVisitCount:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->isVisitCountUpdated()Z

    move-result v2

    if-nez v2, :cond_1

    .line 135
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->updatingVisitCountState:Z

    .line 136
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getVisitCountPageNum()I

    move-result v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->compose_VisitCount_pp(IIJ)[B

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    .line 137
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 138
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getTotalPageCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->handleProgressUpdate(II)V

    goto/16 :goto_0

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 144
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 148
    :cond_2
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentReqPageNum:I

    if-eq v2, v4, :cond_3

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentReqPageNum:I

    if-ne v2, v1, :cond_3

    .line 150
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    .line 162
    :goto_1
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    .line 163
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    const/16 v3, 0x21

    if-le v2, v3, :cond_4

    .line 165
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    const-string v3, "Did not receive message after 10 requests"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 167
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 168
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 154
    :cond_3
    const/4 v2, -0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    .line 155
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentReqPageNum:I

    .line 156
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentReqPageNum:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->composePageRequest(I)[B

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    .line 158
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded()I

    move-result v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getTotalPageCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->handleProgressUpdate(II)V

    goto :goto_1

    .line 171
    :cond_4
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v2, v2, 0x3

    if-nez v2, :cond_0

    .line 173
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->currentAckReq:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 191
    .end local v1    # "nextPage":I
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE handling message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->downloadingDeviceInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 193
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->disableMessageProcessing()V

    .line 194
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 84
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
