.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->waitForNewThenFinish(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

.field final synthetic val$devId:I

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$waitDialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/app/AlertDialog;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 927
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$waitDialog:Landroid/app/AlertDialog;

    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$devId:I

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$name:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 933
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mLatch_DevConnected:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 934
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$waitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 935
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$devId:I

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->val$name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->returnNewAndFinish(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 946
    :goto_0
    return-void

    .line 936
    :catch_0
    move-exception v0

    .line 938
    .local v0, "e1":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InterruptedException while waiting on search for selected device"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->ensureCanceledSearch()V
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$1000(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    .line 940
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 941
    .local v1, "respToPlugin":Landroid/os/Message;
    const/4 v2, -0x4

    iput v2, v1, Landroid/os/Message;->what:I

    .line 942
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V
    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/os/Message;)V

    .line 943
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$11;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    goto :goto_0
.end method
