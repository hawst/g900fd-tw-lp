.class Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;
.source "WatchCommunicatorController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestBasedDownloadController"
.end annotation


# instance fields
.field private client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field protected deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

.field protected directoryDownloadTime:I

.field protected downloadFilesSinceTime:J

.field private isDownloadNewOnly:Z

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Z)V
    .locals 1
    .param p2, "isDownloadNewOnly"    # Z
    .param p3, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .param p5, "useProgressUpdates"    # Z

    .prologue
    .line 224
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .line 225
    const/16 v0, 0xca

    invoke-direct {p0, v0, p5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;-><init>(IZ)V

    .line 226
    iput-boolean p2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->isDownloadNewOnly:Z

    .line 227
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 228
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 229
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->directoryDownloadTime:I

    .line 230
    return-void
.end method


# virtual methods
.method public getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .locals 3

    .prologue
    .line 241
    new-instance v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->getAntFsStateReceiver()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V

    return-object v0
.end method

.method public handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V
    .locals 6
    .param p1, "file"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .param p2, "targetDownloadBytes"    # J
    .param p4, "fileDownloadedBytes"    # [B

    .prologue
    .line 302
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getLastDownloadGarminTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->downloadFilesSinceTime:J

    .line 303
    iget-wide v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    .line 304
    .local v0, "lastDownloadTime":J
    iget-wide v2, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    iget-wide v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 305
    iget-wide v2, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->directoryDownloadTime:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 306
    :cond_0
    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->downloadFilesSinceTime:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 307
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->setLastDownloadTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;J)V

    .line 309
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V

    .line 310
    return-void
.end method

.method public handleFinished(I)V
    .locals 1
    .param p1, "antFsRequestStatusCode"    # I

    .prologue
    .line 265
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->close()V

    .line 266
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->handleFinished(I)V

    .line 267
    return-void
.end method

.method public handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 2
    .param p1, "afs"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->open()V

    .line 248
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    # getter for: Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Detected device parameter collision with only serial number different, this situation is currently unsupported. If you see this contact thisisant.com and we can discuss adding the ability to handle this."

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const/16 v0, -0x3d

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->handleFinished(I)V

    .line 255
    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z

    move-result v0

    goto :goto_0
.end method

.method public handlePreFileHandling(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 3
    .param p1, "afs"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getLastDownloadGarminTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->downloadFilesSinceTime:J

    .line 273
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastRequestDuration()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->directoryDownloadTime:I

    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z
    .locals 5
    .param p1, "file"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .prologue
    const/4 v0, 0x0

    .line 281
    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->FIT_DATA_TYPE:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->getIntValue()I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    aget-byte v1, v1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->isDownloadNewOnly:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->isDownloadNewOnly:Z

    if-eqz v1, :cond_0

    iget-wide v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->downloadFilesSinceTime:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->isDownloadNewOnly:Z

    if-eqz v1, :cond_2

    iget-wide v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    .line 286
    :cond_1
    const/4 v0, 0x1

    .line 290
    :cond_2
    return v0
.end method

.method protected sendMessageToClient(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 236
    return-void
.end method
