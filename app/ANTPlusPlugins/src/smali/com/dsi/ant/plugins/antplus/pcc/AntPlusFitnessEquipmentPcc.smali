.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IpcDefines;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

.field mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

.field private mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

.field mCalculatedTrainerDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;

.field mCalculatedTrainerPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;

.field mCalculatedTrainerSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;

.field mCalculatedTrainerTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;

.field mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

.field mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

.field mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

.field mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

.field private mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

.field mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

.field mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

.field private mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

.field mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

.field mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

.field mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

.field mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

.field final mIsTrainer:Z

.field mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

.field mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

.field private mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

.field mRawTrainerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;

.field mRawTrainerTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;

.field mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

.field mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

.field mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

.field mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

.field mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

.field mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

.field mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

.field mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

.field mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

.field mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

.field private mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

.field mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

.field mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

.field private mTrainerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;

.field mTrainerStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;

.field mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

.field private mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

.field mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

.field mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

.field pccHandler:Landroid/os/Handler;

.field final unsubscribeRequestedCalibrationInProgressReceiver:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "isFEC"    # Z

    .prologue
    .line 2953
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    .line 2574
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->pccHandler:Landroid/os/Handler;

    .line 2577
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$1;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeRequestedCalibrationInProgressReceiver:Ljava/lang/Runnable;

    .line 3462
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    .line 3501
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    .line 3540
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    .line 3826
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    .line 3865
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    .line 3904
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    .line 3943
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrainerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;

    .line 2954
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mIsTrainer:Z

    .line 2955
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCalibrationInProgressEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$1700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$1800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$2100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$2200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$2500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$2600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$2900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$3000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$3300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$3400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$3600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$3700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    return v0
.end method

.method static synthetic access$3800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method private requestCalibration(ZZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z
    .locals 6
    .param p1, "requestZeroOffsetCalibration"    # Z
    .param p2, "requestSpinDownCalibration"    # Z
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p4, "calibrationResponseReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;
    .param p5, "calibrationInProgressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    .line 5146
    const-string v1, "requestCalibration"

    .line 5147
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e2d

    .line 5148
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 5149
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "bool_zeroOffsetCalibration"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5150
    const-string v0, "bool_spinDownCalibration"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5152
    invoke-direct {p0, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCalibrationResponseEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;)V

    .line 5153
    invoke-direct {p0, p5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCalibrationInProgressEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)V

    .line 5155
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public static requestNewOpenAccess(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2945
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;-><init>(Z)V

    .line 2946
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    iput-object p3, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    .line 2948
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method

.method public static requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p4, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2786
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p6, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2851
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;-><init>(Z)V

    .line 2852
    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    iput-object p6, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 2854
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewOpenAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p5, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2905
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;-><init>(Z)V

    .line 2906
    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    iput-object p5, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 2908
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;I)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .param p4, "channelDeviceNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            "I)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    const/4 v5, 0x0

    .line 2631
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;ILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;ILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 11
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .param p4, "channelDeviceNumber"    # I
    .param p5, "settings"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    .param p6, "selectedFiles"    # [Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;",
            "[",
            "Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2689
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2690
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "int_RequestAccessMode"

    const/16 v3, 0x12c

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2691
    const-string v2, "int_ChannelDeviceId"

    invoke-virtual {v8, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2693
    move-object/from16 v1, p5

    .line 2694
    .local v1, "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    if-nez p5, :cond_0

    .line 2695
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;

    .end local v1    # "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    const-string v2, "Invalid"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;-><init>(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;SFF)V

    .line 2698
    .restart local v1    # "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    :cond_0
    if-nez p4, :cond_2

    .line 2702
    :try_start_0
    const-string v2, "com.dsi.ant.plugins.antplus"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v10

    .line 2703
    .local v10, "pluginContext":Landroid/content/Context;
    invoke-static {v10}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->setSerialNumber(J)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2716
    .end local v10    # "pluginContext":Landroid/content/Context;
    :goto_0
    const-string v2, "parcelable_settings"

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->toFitFile()Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2718
    if-eqz p6, :cond_1

    move-object/from16 v0, p6

    array-length v2, v0

    if-eqz v2, :cond_1

    .line 2719
    const-string v2, "arrayParcelable_fitFiles"

    move-object/from16 v0, p6

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2723
    :cond_1
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;-><init>(Z)V

    .line 2724
    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    iput-object p3, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    .line 2726
    new-instance v5, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;

    invoke-direct {v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;-><init>()V

    move-object v2, p0

    move-object v3, v8

    move-object v6, p1

    move-object v7, p2

    invoke-static/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v2

    return-object v2

    .line 2705
    .end local v4    # "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    :catch_0
    move-exception v9

    .line 2707
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v9}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2713
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->setSerialNumber(J)V

    goto :goto_0
.end method

.method private subscribeRequestedBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)V
    .locals 2
    .param p1, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    const/16 v1, 0xdc

    .line 4690
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-nez v0, :cond_0

    .line 4692
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-nez v0, :cond_1

    .line 4693
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4697
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .line 4698
    return-void

    .line 4694
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v0, :cond_0

    .line 4695
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedCalibrationInProgressEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)V
    .locals 2
    .param p1, "calibrationInProgressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    const/16 v1, 0xe3

    .line 4608
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-nez v0, :cond_0

    .line 4610
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-nez v0, :cond_1

    .line 4611
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4615
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .line 4616
    return-void

    .line 4612
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-eqz v0, :cond_0

    .line 4613
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedCalibrationResponseEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;)V
    .locals 2
    .param p1, "calibrationResponseReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    .prologue
    const/16 v1, 0xe2

    .line 4568
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-nez v0, :cond_0

    .line 4570
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-nez v0, :cond_1

    .line 4571
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4575
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    .line 4576
    return-void

    .line 4572
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-eqz v0, :cond_0

    .line 4573
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedCapabilitiesEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;)V
    .locals 2
    .param p1, "capabilitiesReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    .prologue
    const/16 v1, 0xe0

    .line 4487
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-nez v0, :cond_0

    .line 4489
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-nez v0, :cond_1

    .line 4490
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4494
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    .line 4495
    return-void

    .line 4491
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-eqz v0, :cond_0

    .line 4492
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)V
    .locals 2
    .param p1, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    const/16 v1, 0xdb

    .line 4650
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-nez v0, :cond_0

    .line 4652
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-nez v0, :cond_1

    .line 4653
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4657
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .line 4658
    return-void

    .line 4654
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-eqz v0, :cond_0

    .line 4655
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)V
    .locals 2
    .param p1, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    const/16 v1, 0xdd

    .line 4730
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-nez v0, :cond_0

    .line 4732
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-nez v0, :cond_1

    .line 4733
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4737
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .line 4738
    return-void

    .line 4734
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-eqz v0, :cond_0

    .line 4735
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)V
    .locals 2
    .param p1, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    const/16 v1, 0xdf

    .line 4810
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-nez v0, :cond_0

    .line 4812
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-nez v0, :cond_1

    .line 4813
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4817
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .line 4818
    return-void

    .line 4814
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-eqz v0, :cond_0

    .line 4815
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedUserConfigurationEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;)V
    .locals 2
    .param p1, "userConfigurationReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    .prologue
    const/16 v1, 0xe1

    .line 4527
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-nez v0, :cond_0

    .line 4529
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-nez v0, :cond_1

    .line 4530
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4534
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    .line 4535
    return-void

    .line 4531
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-eqz v0, :cond_0

    .line 4532
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestedWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)V
    .locals 2
    .param p1, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    const/16 v1, 0xde

    .line 4770
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-nez v0, :cond_0

    .line 4772
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-nez v0, :cond_1

    .line 4773
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4777
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .line 4778
    return-void

    .line 4774
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-eqz v0, :cond_0

    .line 4775
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method


# virtual methods
.method public getBikeMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;
    .locals 1

    .prologue
    .line 3549
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    return-object v0
.end method

.method public getClimberMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;
    .locals 1

    .prologue
    .line 3874
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    return-object v0
.end method

.method public getEllipticalMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;
    .locals 1

    .prologue
    .line 3510
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    return-object v0
.end method

.method public getNordicSkierMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;
    .locals 1

    .prologue
    .line 3913
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    return-object v0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2968
    const-string v0, "ANT+ Plugin: Fitness Equipment"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mIsTrainer:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4ef1

    .line 47
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2774

    goto :goto_0
.end method

.method public getRowerMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;
    .locals 1

    .prologue
    .line 3835
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    return-object v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2960
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2961
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.fitnessequipment.FitnessEquipmentService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2962
    return-object v0
.end method

.method public getTrainerMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;
    .locals 1

    .prologue
    .line 3952
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrainerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;

    return-object v0
.end method

.method public getTreadmillMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;
    .locals 1

    .prologue
    .line 3471
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 124
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 2974
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 3456
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 3459
    :cond_0
    :goto_0
    return-void

    .line 2978
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    if-eqz v1, :cond_0

    .line 2981
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 2982
    .local v112, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2983
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2984
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_lapCount"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v117

    .line 2985
    .local v117, "lapCount":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    move/from16 v0, v117

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;->onNewLapOccured(JLjava/util/EnumSet;I)V

    goto :goto_0

    .line 2991
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v117    # "lapCount":I
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    if-eqz v1, :cond_0

    .line 2994
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 2995
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2996
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2997
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_equipmentTypeCode"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;

    move-result-object v5

    .line 2998
    .local v5, "equipmentTypeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;
    const-string v1, "int_stateCode"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    move-result-object v6

    .line 2999
    .local v6, "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;->onNewFitnessEquipmentState(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;)V

    goto :goto_0

    .line 3005
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "equipmentTypeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;
    .end local v6    # "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    if-eqz v1, :cond_0

    .line 3008
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3009
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3010
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3011
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_elapsedTime"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v11

    check-cast v11, Ljava/math/BigDecimal;

    .line 3012
    .local v11, "elapsedTime":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeDistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 3013
    .local v12, "cumulativeDistance":J
    const-string v1, "decimal_instantaneousSpeed"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v14

    check-cast v14, Ljava/math/BigDecimal;

    .line 3015
    .local v14, "instantaneousSpeed":Ljava/math/BigDecimal;
    const-string v1, "bool_virtualInstantaneousSpeed"

    const/4 v7, 0x0

    move-object/from16 v0, v112

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    .line 3016
    .local v15, "virtualSpeed":Z
    const-string v1, "int_instantaneousHeartRate"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 3017
    .local v16, "instantaneousHeartRate":I
    const-string v1, "int_heartRateDataSourceCode"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    move-result-object v17

    .line 3018
    .local v17, "heartRateDataSourceCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    move-wide v8, v2

    move-object v10, v4

    invoke-interface/range {v7 .. v17}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;->onNewGeneralFitnessEquipmentData(JLjava/util/EnumSet;Ljava/math/BigDecimal;JLjava/math/BigDecimal;ZILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;)V

    goto/16 :goto_0

    .line 3024
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v11    # "elapsedTime":Ljava/math/BigDecimal;
    .end local v12    # "cumulativeDistance":J
    .end local v14    # "instantaneousSpeed":Ljava/math/BigDecimal;
    .end local v15    # "virtualSpeed":Z
    .end local v16    # "instantaneousHeartRate":I
    .end local v17    # "heartRateDataSourceCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    if-eqz v1, :cond_0

    .line 3027
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3028
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3029
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3030
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_cycleLength"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v22

    check-cast v22, Ljava/math/BigDecimal;

    .line 3031
    .local v22, "cycleLength":Ljava/math/BigDecimal;
    const-string v1, "decimal_inclinePercentage"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v23

    check-cast v23, Ljava/math/BigDecimal;

    .line 3032
    .local v23, "inclinePercentage":Ljava/math/BigDecimal;
    const-string v1, "int_resistanceLevel"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 3033
    .local v24, "resistanceLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    move-object/from16 v18, v0

    move-wide/from16 v19, v2

    move-object/from16 v21, v4

    invoke-interface/range {v18 .. v24}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;->onNewGeneralSettings(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;I)V

    goto/16 :goto_0

    .line 3039
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v22    # "cycleLength":Ljava/math/BigDecimal;
    .end local v23    # "inclinePercentage":Ljava/math/BigDecimal;
    .end local v24    # "resistanceLevel":I
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    if-eqz v1, :cond_0

    .line 3042
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3043
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3044
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3045
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_instantaneousMetabolicEquivalents"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v29

    check-cast v29, Ljava/math/BigDecimal;

    .line 3046
    .local v29, "instantaneousMetabolicEquivalents":Ljava/math/BigDecimal;
    const-string v1, "decimal_instantaneousCaloricBurn"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v30

    check-cast v30, Ljava/math/BigDecimal;

    .line 3047
    .local v30, "instantaneousCaloricBurn":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeCalories"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v31

    .line 3048
    .local v31, "cumulativeCalories":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    move-object/from16 v25, v0

    move-wide/from16 v26, v2

    move-object/from16 v28, v4

    invoke-interface/range {v25 .. v32}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;->onNewGeneralMetabolicData(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;J)V

    goto/16 :goto_0

    .line 3054
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v29    # "instantaneousMetabolicEquivalents":Ljava/math/BigDecimal;
    .end local v30    # "instantaneousCaloricBurn":Ljava/math/BigDecimal;
    .end local v31    # "cumulativeCalories":J
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

    if-eqz v1, :cond_0

    .line 3057
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3058
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3059
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3060
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3061
    .local v37, "instantaneousCadence":I
    const-string v1, "decimal_cumulativeNegVertDistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v38

    check-cast v38, Ljava/math/BigDecimal;

    .line 3062
    .local v38, "cumulativeNegVertDistance":Ljava/math/BigDecimal;
    const-string v1, "decimal_cumulativePosVertDistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v39

    check-cast v39, Ljava/math/BigDecimal;

    .line 3063
    .local v39, "cumulativePosVertDistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

    move-object/from16 v33, v0

    move-wide/from16 v34, v2

    move-object/from16 v36, v4

    invoke-interface/range {v33 .. v39}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;->onNewTreadmillData(JLjava/util/EnumSet;ILjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3069
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v38    # "cumulativeNegVertDistance":Ljava/math/BigDecimal;
    .end local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

    if-eqz v1, :cond_0

    .line 3072
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3073
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3074
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3075
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_cumulativePosVertDistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v39

    check-cast v39, Ljava/math/BigDecimal;

    .line 3076
    .restart local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeStrides"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v45

    .line 3077
    .local v45, "cumulativeStrides":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3078
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3079
    .local v48, "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

    move-object/from16 v40, v0

    move-wide/from16 v41, v2

    move-object/from16 v43, v4

    move-object/from16 v44, v39

    move/from16 v47, v37

    invoke-interface/range {v40 .. v48}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;->onNewEllipticalData(JLjava/util/EnumSet;Ljava/math/BigDecimal;JII)V

    goto/16 :goto_0

    .line 3085
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    .end local v45    # "cumulativeStrides":J
    .end local v48    # "instantaneousPower":I
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    if-eqz v1, :cond_0

    .line 3088
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3089
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3090
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3091
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3092
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3093
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    move-object/from16 v49, v0

    move-wide/from16 v50, v2

    move-object/from16 v52, v4

    move/from16 v53, v37

    move/from16 v54, v48

    invoke-interface/range {v49 .. v54}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;->onNewBikeData(JLjava/util/EnumSet;II)V

    goto/16 :goto_0

    .line 3099
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

    if-eqz v1, :cond_0

    .line 3102
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3103
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3104
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3105
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrokes"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v53

    .line 3106
    .local v53, "cumulativeStrokes":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3107
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3108
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

    move-object/from16 v49, v0

    move-wide/from16 v50, v2

    move-object/from16 v52, v4

    move/from16 v55, v37

    move/from16 v56, v48

    invoke-interface/range {v49 .. v56}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;->onNewRowerData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 3114
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v53    # "cumulativeStrokes":J
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

    if-eqz v1, :cond_0

    .line 3117
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3118
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3119
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3120
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrideCycles"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v59

    .line 3121
    .local v59, "cumulativeStrideCycles":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3122
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3123
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

    move-object/from16 v55, v0

    move-wide/from16 v56, v2

    move-object/from16 v58, v4

    move/from16 v61, v37

    move/from16 v62, v48

    invoke-interface/range {v55 .. v62}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;->onNewClimberData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 3129
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v59    # "cumulativeStrideCycles":J
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

    if-eqz v1, :cond_0

    .line 3132
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3133
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3134
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3135
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrides"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v45

    .line 3136
    .restart local v45    # "cumulativeStrides":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3137
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3138
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

    move-object/from16 v41, v0

    move-wide/from16 v42, v2

    move-object/from16 v44, v4

    move/from16 v47, v37

    invoke-interface/range {v41 .. v48}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;->onNewNordicSkierData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 3144
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v45    # "cumulativeStrides":J
    .end local v48    # "instantaneousPower":I
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;

    if-eqz v1, :cond_0

    .line 3147
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3148
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3149
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3150
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_updateEventCount"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v65

    .line 3151
    .local v65, "updateEventCount":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 3152
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 3153
    .restart local v48    # "instantaneousPower":I
    const-string v1, "long_accumulatedPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v69

    .line 3154
    .local v69, "accumulatedPower":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;

    move-object/from16 v61, v0

    move-wide/from16 v62, v2

    move-object/from16 v64, v4

    move/from16 v67, v37

    move/from16 v68, v48

    invoke-interface/range {v61 .. v70}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;->onNewRawTrainerData(JLjava/util/EnumSet;JIIJ)V

    goto/16 :goto_0

    .line 3160
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v65    # "updateEventCount":J
    .end local v69    # "accumulatedPower":J
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;

    if-eqz v1, :cond_0

    .line 3163
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3164
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3165
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3166
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_updateEventCount"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v65

    .line 3167
    .restart local v65    # "updateEventCount":J
    const-string v1, "long_accumulatedWheelTicks"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v77

    .line 3168
    .local v77, "accumulatedWheelTicks":J
    const-string v1, "decimal_accumulatedWheelPeriod"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v79

    check-cast v79, Ljava/math/BigDecimal;

    .line 3169
    .local v79, "accumulatedWheelPeriod":Ljava/math/BigDecimal;
    const-string v1, "decimal_accumulatedTorque"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v80

    check-cast v80, Ljava/math/BigDecimal;

    .line 3170
    .local v80, "accumulatedTorque":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;

    move-object/from16 v71, v0

    move-wide/from16 v72, v2

    move-object/from16 v74, v4

    move-wide/from16 v75, v65

    invoke-interface/range {v71 .. v80}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;->onNewRawTrainerTorqueData(JLjava/util/EnumSet;JJLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3176
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v65    # "updateEventCount":J
    .end local v77    # "accumulatedWheelTicks":J
    .end local v79    # "accumulatedWheelPeriod":Ljava/math/BigDecimal;
    .end local v80    # "accumulatedTorque":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;

    if-eqz v1, :cond_0

    .line 3179
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3180
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3181
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3182
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    move-result-object v75

    .line 3183
    .local v75, "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    const-string v1, "decimal_calculatedDistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v76

    check-cast v76, Ljava/math/BigDecimal;

    .line 3184
    .local v76, "rawCalculatedTrainerDistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;

    move-object/from16 v71, v0

    move-wide/from16 v72, v2

    move-object/from16 v74, v4

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;->onNewRawCalculatedTrainerDistance(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    invoke-static/range {v71 .. v76}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;->access$200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3190
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .end local v76    # "rawCalculatedTrainerDistance":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;

    if-eqz v1, :cond_0

    .line 3193
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3194
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3195
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3196
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    move-result-object v75

    .line 3197
    .restart local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    const-string v1, "decimal_calculatedSpeed"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v86

    check-cast v86, Ljava/math/BigDecimal;

    .line 3198
    .local v86, "rawCalculatedTrainerSpeed":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;

    move-object/from16 v81, v0

    move-wide/from16 v82, v2

    move-object/from16 v84, v4

    move-object/from16 v85, v75

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->onNewRawCalculatedTrainerSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    invoke-static/range {v81 .. v86}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->access$300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3204
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .end local v86    # "rawCalculatedTrainerSpeed":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;

    if-eqz v1, :cond_0

    .line 3207
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3208
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3209
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3210
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    move-result-object v75

    .line 3211
    .restart local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    const-string v1, "decimal_calculatedPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v92

    check-cast v92, Ljava/math/BigDecimal;

    .line 3212
    .local v92, "calculatedPower":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;

    move-object/from16 v87, v0

    move-wide/from16 v88, v2

    move-object/from16 v90, v4

    move-object/from16 v91, v75

    invoke-interface/range {v87 .. v92}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;->onNewCalculatedTrainerPower(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3218
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .end local v92    # "calculatedPower":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;

    if-eqz v1, :cond_0

    .line 3221
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3222
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3223
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3224
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;

    move-result-object v75

    .line 3225
    .restart local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    const-string v1, "decimal_calculatedTorque"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v98

    check-cast v98, Ljava/math/BigDecimal;

    .line 3226
    .local v98, "calculatedTorque":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;

    move-object/from16 v93, v0

    move-wide/from16 v94, v2

    move-object/from16 v96, v4

    move-object/from16 v97, v75

    invoke-interface/range {v93 .. v98}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;->onNewCalculatedTrainerTorque(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 3232
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v75    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .end local v98    # "calculatedTorque":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrainerStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;

    if-eqz v1, :cond_0

    .line 3235
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3236
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3237
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3238
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_trainerStatusFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->getEnumSet(J)Ljava/util/EnumSet;

    move-result-object v122

    .line 3239
    .local v122, "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrainerStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;

    move-object/from16 v0, v122

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;->onNewTrainerStatus(JLjava/util/EnumSet;Ljava/util/EnumSet;)V

    goto/16 :goto_0

    .line 3245
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v122    # "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3248
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3249
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3250
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3251
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_totalResistance"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v121

    check-cast v121, Ljava/math/BigDecimal;

    .line 3253
    .local v121, "totalResistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v1, :cond_2

    .line 3254
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    move-object/from16 v0, v121

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;->onNewBasicResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    .line 3256
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3258
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    move-object/from16 v0, v121

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;->onNewBasicResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    .line 3259
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)V

    goto/16 :goto_0

    .line 3266
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v121    # "totalResistance":Ljava/math/BigDecimal;
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-eqz v1, :cond_0

    .line 3269
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3270
    .restart local v112    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 3271
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3272
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3273
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "parcelable_Capabilities"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v115

    check-cast v115, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;

    .line 3275
    .local v115, "capabilities":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-eqz v1, :cond_4

    .line 3276
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    move-object/from16 v0, v115

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;->onNewCapabilities(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;)V

    .line 3278
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    move-object/from16 v0, v115

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;->onNewCapabilities(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;)V

    .line 3281
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCapabilitiesEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;)V

    goto/16 :goto_0

    .line 3288
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v115    # "capabilities":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-nez v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-eqz v1, :cond_0

    .line 3291
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3292
    .restart local v112    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 3293
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3294
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3295
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "parcelable_CommandStatus"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v116

    check-cast v116, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;

    .line 3297
    .local v116, "commandStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-eqz v1, :cond_6

    .line 3298
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    move-object/from16 v0, v116

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;->onNewCommandStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;)V

    .line 3300
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-eqz v1, :cond_0

    .line 3302
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    move-object/from16 v0, v116

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;->onNewCommandStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;)V

    .line 3303
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)V

    goto/16 :goto_0

    .line 3310
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v116    # "commandStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-eqz v1, :cond_0

    .line 3313
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3314
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3315
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3316
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_targetPower"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v119

    check-cast v119, Ljava/math/BigDecimal;

    .line 3318
    .local v119, "targetPower":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-eqz v1, :cond_8

    .line 3319
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    move-object/from16 v0, v119

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;->onNewTargetPower(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    .line 3321
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-eqz v1, :cond_0

    .line 3323
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    move-object/from16 v0, v119

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;->onNewTargetPower(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    .line 3324
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)V

    goto/16 :goto_0

    .line 3331
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v119    # "targetPower":Ljava/math/BigDecimal;
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3334
    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3335
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3336
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3337
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_grade"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v103

    check-cast v103, Ljava/math/BigDecimal;

    .line 3338
    .local v103, "grade":Ljava/math/BigDecimal;
    const-string v1, "decimal_rollingResistanceCoefficient"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v104

    check-cast v104, Ljava/math/BigDecimal;

    .line 3340
    .local v104, "rollingResistanceCoefficient":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-eqz v1, :cond_a

    .line 3341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    move-object/from16 v99, v0

    move-wide/from16 v100, v2

    move-object/from16 v102, v4

    invoke-interface/range {v99 .. v104}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;->onNewTrackResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 3343
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    move-object/from16 v99, v0

    move-wide/from16 v100, v2

    move-object/from16 v102, v4

    invoke-interface/range {v99 .. v104}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;->onNewTrackResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 3346
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)V

    goto/16 :goto_0

    .line 3353
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v103    # "grade":Ljava/math/BigDecimal;
    .end local v104    # "rollingResistanceCoefficient":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-nez v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-eqz v1, :cond_0

    .line 3356
    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3357
    .restart local v112    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 3358
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3359
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3360
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "parcelable_UserConfiguration"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v123

    check-cast v123, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;

    .line 3362
    .local v123, "userConfiguration":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-eqz v1, :cond_c

    .line 3363
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    move-object/from16 v0, v123

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;->onNewUserConfiguration(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;)V

    .line 3365
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-eqz v1, :cond_0

    .line 3367
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    move-object/from16 v0, v123

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;->onNewUserConfiguration(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;)V

    .line 3368
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedUserConfigurationEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;)V

    goto/16 :goto_0

    .line 3375
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v123    # "userConfiguration":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-nez v1, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3378
    :cond_d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3379
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3380
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3381
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_windResistanceCoefficient"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v109

    check-cast v109, Ljava/math/BigDecimal;

    .line 3382
    .local v109, "windResistanceCoefficient":Ljava/math/BigDecimal;
    const-string v1, "int_windSpeed"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v110

    .line 3383
    .local v110, "windSpeed":I
    const-string v1, "decimal_draftingFactor"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v111

    check-cast v111, Ljava/math/BigDecimal;

    .line 3385
    .local v111, "draftingFactor":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-eqz v1, :cond_e

    .line 3386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    move-object/from16 v105, v0

    move-wide/from16 v106, v2

    move-object/from16 v108, v4

    invoke-interface/range {v105 .. v111}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;->onNewWindResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;ILjava/math/BigDecimal;)V

    .line 3388
    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-eqz v1, :cond_0

    .line 3390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    move-object/from16 v105, v0

    move-wide/from16 v106, v2

    move-object/from16 v108, v4

    invoke-interface/range {v105 .. v111}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;->onNewWindResistance(JLjava/util/EnumSet;Ljava/math/BigDecimal;ILjava/math/BigDecimal;)V

    .line 3391
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)V

    goto/16 :goto_0

    .line 3398
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v109    # "windResistanceCoefficient":Ljava/math/BigDecimal;
    .end local v110    # "windSpeed":I
    .end local v111    # "draftingFactor":Ljava/math/BigDecimal;
    .end local v112    # "b":Landroid/os/Bundle;
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-nez v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-eqz v1, :cond_0

    .line 3401
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3402
    .restart local v112    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 3403
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3404
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3405
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "parcelable_CalibrationResponse"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v114

    check-cast v114, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;

    .line 3407
    .local v114, "calibrationResponse":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-eqz v1, :cond_10

    .line 3408
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    move-object/from16 v0, v114

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;->onNewCalibrationResponse(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;)V

    .line 3410
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-eqz v1, :cond_0

    .line 3412
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    move-object/from16 v0, v114

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;->onNewCalibrationResponse(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;)V

    .line 3413
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCalibrationResponseEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;)V

    goto/16 :goto_0

    .line 3420
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v114    # "calibrationResponse":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-nez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-eqz v1, :cond_0

    .line 3423
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3424
    .restart local v112    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 3425
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3426
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 3427
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "parcelable_CalibrationInProgress"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v113

    check-cast v113, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;

    .line 3429
    .local v113, "calibrationInProgress":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-eqz v1, :cond_12

    .line 3430
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    move-object/from16 v0, v113

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;->onNewCalibrationInProgress(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;)V

    .line 3432
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-eqz v1, :cond_0

    .line 3434
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    move-object/from16 v0, v113

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;->onNewCalibrationInProgress(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;)V

    .line 3435
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeRequestedCalibrationInProgressReceiver:Ljava/lang/Runnable;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3436
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeRequestedCalibrationInProgressReceiver:Ljava/lang/Runnable;

    const-wide/16 v8, 0x1388

    invoke-virtual {v1, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 3443
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v112    # "b":Landroid/os/Bundle;
    .end local v113    # "calibrationInProgress":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    move-object/from16 v120, v0

    .line 3444
    .local v120, "tempReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 3445
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 3447
    if-eqz v120, :cond_0

    .line 3449
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v112

    .line 3450
    .restart local v112    # "b":Landroid/os/Bundle;
    const-string v1, "int_requestStatus"

    move-object/from16 v0, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v118

    .line 3451
    .local v118, "requestStatus":I
    invoke-static/range {v118 .. v118}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v1

    move-object/from16 v0, v120

    invoke-interface {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;->onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto/16 :goto_0

    .line 2974
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_11
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_14
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_16
        :pswitch_13
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method protected requestBasicResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    .line 4849
    const-string v0, "requestBasicResistance"

    .line 4851
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e21

    .line 4853
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)V

    .line 4855
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method public requestCapabilities(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "capabilitiesReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    .prologue
    .line 5081
    const-string v0, "requestCapabilities"

    .line 5083
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e2a

    .line 5085
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCapabilitiesEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;)V

    .line 5087
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method protected requestCommandStatus(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    .line 4830
    const-string v0, "requestCommandStatus"

    .line 4832
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x69

    .line 4834
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)V

    .line 4836
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method protected requestSetBasicResistance(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "totalResistance"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4871
    const-string v1, "requestSetBasicResistance"

    .line 4873
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e22

    .line 4874
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 4875
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "decimal_totalResistance"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4877
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected requestSetTargetPower(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "targetPower"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4913
    const-string v1, "requestSetTargetPower"

    .line 4915
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e24

    .line 4916
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 4917
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "decimal_targetPower"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4919
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected requestSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "grade"    # Ljava/math/BigDecimal;
    .param p2, "rollingResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 5061
    const-string v1, "requestSetTrackResistance"

    .line 5063
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e29

    .line 5064
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 5065
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "decimal_grade"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5066
    const-string v0, "decimal_rollingResistanceCoefficient"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5068
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public requestSetUserConfiguration(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "userConfig"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 5122
    const-string v1, "requestSetUserConfiguration"

    .line 5124
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e2c

    .line 5125
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 5126
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "parcelable_UserConfiguration"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 5128
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected requestSetWindResistance(Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "windResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p2, "windSpeed"    # Ljava/lang/Integer;
    .param p3, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p4, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4961
    const-string v1, "requestSetWindResistance"

    .line 4963
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e26

    .line 4964
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 4965
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "decimal_windResistanceCoefficient"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4966
    const-string v4, "int_windSpeed"

    if-nez p2, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4967
    const-string v0, "decimal_draftingFactor"

    invoke-virtual {v3, v0, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4969
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0

    .line 4966
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method protected requestSetWindResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "frontalSurfaceArea"    # Ljava/math/BigDecimal;
    .param p2, "dragCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "airDensity"    # Ljava/math/BigDecimal;
    .param p4, "windSpeed"    # Ljava/lang/Integer;
    .param p5, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p6, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 5004
    const-string v1, "requestSetWindResistance"

    .line 5006
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e27

    .line 5007
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 5008
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "decimal_frontalSurfaceArea"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5009
    const-string v0, "decimal_dragCoefficient"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5010
    const-string v0, "decimal_airDensity"

    invoke-virtual {v3, v0, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5011
    const-string v4, "int_windSpeed"

    if-nez p4, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 5012
    const-string v0, "decimal_draftingFactor"

    invoke-virtual {v3, v0, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 5014
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p6

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0

    .line 5011
    :cond_0
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public requestSpinDownCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z
    .locals 6
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "calibrationResponseReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;
    .param p3, "calibrationInProgressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    .line 5187
    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestCalibration(ZZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z

    move-result v0

    return v0
.end method

.method protected requestTargetPower(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    .line 4892
    const-string v0, "requestTargetPower"

    .line 4894
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e23

    .line 4896
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)V

    .line 4898
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method protected requestTrackResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    .line 5029
    const-string v0, "requestTrackResistance"

    .line 5031
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e28

    .line 5033
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)V

    .line 5035
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method public requestUserConfiguration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "userConfigurationReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    .prologue
    .line 5102
    const-string v0, "requestUserConfiguration"

    .line 5104
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e2b

    .line 5106
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedUserConfigurationEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;)V

    .line 5108
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method protected requestWindResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    .line 4934
    const-string v0, "requestWindResistance"

    .line 4936
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e25

    .line 4938
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeRequestedWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)V

    .line 4940
    const/16 v2, 0x4ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v2

    return v2
.end method

.method public requestZeroOffsetCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z
    .locals 6
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "calibrationResponseReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;
    .param p3, "calibrationInProgressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    .line 5171
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestCalibration(ZZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z

    move-result v0

    return v0
.end method

.method protected subscribeBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 4
    .param p1, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    const/16 v3, 0xdc

    .line 4670
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4672
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeBasicResistanceEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4673
    const/4 v0, 0x0

    .line 4685
    :goto_0
    return v0

    .line 4676
    :cond_0
    const/4 v0, 0x1

    .line 4677
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-nez v1, :cond_1

    .line 4679
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-nez v1, :cond_2

    .line 4680
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4684
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    goto :goto_0

    .line 4681
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBasicResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    if-eqz v1, :cond_1

    .line 4682
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribeCalibrationInProgressEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;)Z
    .locals 4
    .param p1, "calibrationInProgressReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    .prologue
    const/16 v3, 0xe3

    .line 4588
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4590
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeCalibrationInProgressEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4591
    const/4 v0, 0x0

    .line 4603
    :goto_0
    return v0

    .line 4594
    :cond_0
    const/4 v0, 0x1

    .line 4595
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-nez v1, :cond_1

    .line 4597
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-nez v1, :cond_2

    .line 4598
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4602
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    goto :goto_0

    .line 4599
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationInProgressReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationInProgressReceiver;

    if-eqz v1, :cond_1

    .line 4600
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribeCalibrationResponseEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;)Z
    .locals 4
    .param p1, "calibrationResponseReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    .prologue
    const/16 v3, 0xe2

    .line 4548
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4550
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeCalibrationResponseEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4551
    const/4 v0, 0x0

    .line 4563
    :goto_0
    return v0

    .line 4554
    :cond_0
    const/4 v0, 0x1

    .line 4555
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-nez v1, :cond_1

    .line 4557
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-nez v1, :cond_2

    .line 4558
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4562
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    goto :goto_0

    .line 4559
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalibrationResponseReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalibrationResponseReceiver;

    if-eqz v1, :cond_1

    .line 4560
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribeCapabilitiesEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;)Z
    .locals 4
    .param p1, "capabilitiesReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    .prologue
    const/16 v3, 0xe0

    .line 4467
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4469
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeCapabilitiesEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4470
    const/4 v0, 0x0

    .line 4482
    :goto_0
    return v0

    .line 4473
    :cond_0
    const/4 v0, 0x1

    .line 4474
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-nez v1, :cond_1

    .line 4476
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-nez v1, :cond_2

    .line 4477
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4481
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    goto :goto_0

    .line 4478
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCapabilitiesReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICapabilitiesReceiver;

    if-eqz v1, :cond_1

    .line 4479
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method protected subscribeCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 4
    .param p1, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    const/16 v3, 0xdb

    .line 4630
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4632
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeCommandStatusEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4633
    const/4 v0, 0x0

    .line 4645
    :goto_0
    return v0

    .line 4636
    :cond_0
    const/4 v0, 0x1

    .line 4637
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-nez v1, :cond_1

    .line 4639
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-nez v1, :cond_2

    .line 4640
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4644
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    goto :goto_0

    .line 4641
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCommandStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    if-eqz v1, :cond_1

    .line 4642
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribeGeneralFitnessEquipmentDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;)V
    .locals 1
    .param p1, "GeneralFitnessEquipmentDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 4407
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    .line 4408
    if-eqz p1, :cond_0

    .line 4410
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4416
    :goto_0
    return-void

    .line 4414
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeGeneralMetabolicDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;)V
    .locals 1
    .param p1, "GeneralMetabolicDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 4445
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    .line 4446
    if-eqz p1, :cond_0

    .line 4448
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4454
    :goto_0
    return-void

    .line 4452
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeGeneralSettingsEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;)V
    .locals 1
    .param p1, "GeneralSettingsReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 4426
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    .line 4427
    if-eqz p1, :cond_0

    .line 4429
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4435
    :goto_0
    return-void

    .line 4433
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeLapOccuredEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;)V
    .locals 1
    .param p1, "LapOccuredReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 4388
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    .line 4389
    if-eqz p1, :cond_0

    .line 4391
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 4397
    :goto_0
    return-void

    .line 4395
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method protected subscribeTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 4
    .param p1, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    const/16 v3, 0xdd

    .line 4710
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4712
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeTargetPowerEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4713
    const/4 v0, 0x0

    .line 4725
    :goto_0
    return v0

    .line 4716
    :cond_0
    const/4 v0, 0x1

    .line 4717
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-nez v1, :cond_1

    .line 4719
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-nez v1, :cond_2

    .line 4720
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4724
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    goto :goto_0

    .line 4721
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTargetPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    if-eqz v1, :cond_1

    .line 4722
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method protected subscribeTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 4
    .param p1, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    const/16 v3, 0xdf

    .line 4790
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4792
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeTrackResistanceEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4793
    const/4 v0, 0x0

    .line 4805
    :goto_0
    return v0

    .line 4796
    :cond_0
    const/4 v0, 0x1

    .line 4797
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-nez v1, :cond_1

    .line 4799
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-nez v1, :cond_2

    .line 4800
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4804
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    goto :goto_0

    .line 4801
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrackResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    if-eqz v1, :cond_1

    .line 4802
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribeUserConfigurationEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;)Z
    .locals 4
    .param p1, "userConfigurationReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    .prologue
    const/16 v3, 0xe1

    .line 4507
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4509
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeUserConfigurationEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4510
    const/4 v0, 0x0

    .line 4522
    :goto_0
    return v0

    .line 4513
    :cond_0
    const/4 v0, 0x1

    .line 4514
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-nez v1, :cond_1

    .line 4516
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-nez v1, :cond_2

    .line 4517
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4521
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    goto :goto_0

    .line 4518
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mUserConfigurationReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IUserConfigurationReceiver;

    if-eqz v1, :cond_1

    .line 4519
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method protected subscribeWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 4
    .param p1, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    const/16 v3, 0xde

    .line 4750
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    const/16 v2, 0x4ef1

    if-ge v1, v2, :cond_0

    .line 4752
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subscribeWindResistanceEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4753
    const/4 v0, 0x0

    .line 4765
    :goto_0
    return v0

    .line 4756
    :cond_0
    const/4 v0, 0x1

    .line 4757
    .local v0, "success":Z
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRequestedWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-nez v1, :cond_1

    .line 4759
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-nez v1, :cond_2

    .line 4760
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    .line 4764
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    goto :goto_0

    .line 4761
    :cond_2
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mWindResistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    if-eqz v1, :cond_1

    .line 4762
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method
