.class public Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
.super Ljava/lang/Object;
.source "ClientInterface.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    }
.end annotation


# instance fields
.field private final mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field private final mCmdHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private final mConnectedDevices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;",
            ">;"
        }
    .end annotation
.end field

.field private mController:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

.field private final mDeviceTypeFilter:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;",
            ">;"
        }
    .end annotation
.end field

.field private final mFoundDevices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;",
            ">;"
        }
    .end annotation
.end field

.field private mReleased:Z

.field private final mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

.field private final mState_LOCK:Ljava/lang/Object;

.field private mStopped:Z

.field public final uid:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;)V
    .locals 8
    .param p1, "params"    # Landroid/os/Bundle;
    .param p2, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "service"    # Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    .prologue
    const/4 v7, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mFoundDevices:Ljava/util/HashSet;

    .line 44
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mConnectedDevices:Ljava/util/HashSet;

    .line 47
    const-class v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-static {v6}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v6

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    .line 50
    new-instance v6, Ljava/lang/Object;

    invoke-direct {v6}, Ljava/lang/Object;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    .line 56
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z

    .line 59
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    .line 62
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->uid:Ljava/util/UUID;

    .line 397
    new-instance v6, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mCmdHandler:Landroid/os/Handler;

    .line 72
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v7

    .line 74
    :try_start_0
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    .line 75
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 77
    const-string v6, "intarr_deviceTypeList"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 80
    .local v2, "filter":[I
    move-object v0, v2

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget v1, v0, v3

    .line 82
    .local v1, "devType":I
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v5

    .line 83
    .local v5, "type":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    if-eq v5, v6, :cond_0

    .line 85
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    invoke-virtual {v6, v5}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 88
    .end local v1    # "devType":I
    .end local v5    # "type":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    :cond_1
    monitor-exit v7

    .line 89
    return-void

    .line 88
    .end local v0    # "arr$":[I
    .end local v2    # "filter":[I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z

    return v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z

    return p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method private checkFilter(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Z
    .locals 3
    .param p1, "deviceType"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .prologue
    .line 313
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 315
    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    if-ne p1, v0, :cond_2

    .line 317
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    .line 322
    :goto_1
    return v0

    .line 317
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mDeviceTypeFilter:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_1

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private generateNewDeviceEvent(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;)V
    .locals 3
    .param p1, "result"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 277
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 278
    .local v1, "evt":Landroid/os/Message;
    iput v2, v1, Landroid/os/Message;->what:I

    .line 279
    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 280
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 281
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "dev_Device"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 283
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 284
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 285
    return-void
.end method

.method private generateRssiEvent(II)V
    .locals 3
    .param p1, "resultId"    # I
    .param p2, "rssi"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 296
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 297
    .local v1, "evt":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 298
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 299
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 300
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_resultID"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 301
    const-string v2, "int_rssi"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 302
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 303
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 304
    return-void
.end method

.method private getInfo(II)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 4
    .param p1, "deviceNumber"    # I
    .param p2, "deviceType"    # I

    .prologue
    .line 379
    const/4 v1, 0x0

    .line 381
    .local v1, "info":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 382
    .local v0, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    invoke-static {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->getDeviceInfoByChanDevId(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v1

    .line 383
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 385
    if-nez v1, :cond_0

    .line 387
    new-instance v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .end local v1    # "info":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-direct {v1}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    .line 388
    .restart local v1    # "info":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 389
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    .line 390
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 393
    :cond_0
    return-object v1
.end method

.method private getResult(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    .locals 5
    .param p1, "id"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    .prologue
    .line 334
    iget v1, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceType:I

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 336
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getSpdCadResult(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;

    move-result-object v1

    .line 340
    :goto_0
    return-object v1

    .line 339
    :cond_0
    iget v1, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceNumber:I

    iget v2, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceType:I

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getInfo(II)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    .line 340
    .local v0, "info":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v2

    iget v3, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceType:I

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;-><init>(ILcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    goto :goto_0
.end method

.method private getSpdCadResult(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;
    .locals 6
    .param p1, "id"    # Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    .prologue
    .line 349
    iget v3, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceNumber:I

    const/high16 v4, 0x20000000

    or-int v1, v3, v4

    .line 351
    .local v1, "fakeId":I
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v3

    invoke-direct {p0, v1, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getInfo(II)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    .line 352
    .local v2, "spdResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v3

    invoke-direct {p0, v1, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getInfo(II)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    .line 359
    .local v0, "cadResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v3, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const v4, -0x20000001

    and-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    iput-object v3, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 362
    iget-object v3, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->device_dbId:Ljava/lang/Long;

    if-nez v3, :cond_0

    .line 364
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    iput-object v3, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 367
    :cond_0
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v2, v0, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;-><init>(ILcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    return-object v3
.end method


# virtual methods
.method public attach(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V
    .locals 2
    .param p1, "controller"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mController:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    .line 112
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mController:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->registerReceiver(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;)V

    .line 113
    monitor-exit v1

    .line 114
    return-void

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public detach()V
    .locals 2

    .prologue
    .line 122
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mFoundDevices:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 125
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mController:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-virtual {v0, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->unregisterReceiver(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;)V

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z

    .line 128
    monitor-exit v1

    .line 129
    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCmdHandler()Landroid/os/Messenger;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mCmdHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method public onAlreadyConnectedDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V
    .locals 7
    .param p1, "info"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "deviceType"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .prologue
    .line 190
    const/4 v2, 0x0

    .line 192
    .local v2, "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 194
    :try_start_0
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    if-nez v4, :cond_0

    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->checkFilter(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 196
    :cond_0
    monitor-exit v5

    .line 220
    :cond_1
    :goto_0
    return-void

    .line 199
    :cond_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    iget-object v4, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v6

    invoke-direct {v0, p0, v4, v6}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;II)V

    .line 202
    .local v0, "chanId":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mFoundDevices:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 204
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mConnectedDevices:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 205
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v4

    const/4 v6, 0x1

    invoke-direct {v3, v4, p2, p1, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;-><init>(ILcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    .end local v2    # "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    .local v3, "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    move-object v2, v3

    .line 208
    .end local v3    # "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    .restart local v2    # "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    if-eqz v2, :cond_1

    .line 214
    :try_start_1
    invoke-direct {p0, v2}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->generateNewDeviceEvent(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 215
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    invoke-virtual {v4, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_0

    .line 208
    .end local v0    # "chanId":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    .end local v1    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method public onPingClient()V
    .locals 4

    .prologue
    .line 163
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 165
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    if-eqz v2, :cond_0

    .line 167
    monitor-exit v3

    .line 181
    :goto_0
    return-void

    .line 169
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 172
    .local v1, "evt":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 173
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 176
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    invoke-virtual {v2, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_0

    .line 169
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "evt":Landroid/os/Message;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public onScanResult(Lcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 7
    .param p1, "id"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 225
    const/4 v3, 0x0

    .line 226
    .local v3, "result":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    const/4 v2, 0x0

    .line 229
    .local v2, "isConnected":Z
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 231
    :try_start_0
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v4

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->checkFilter(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 233
    :cond_0
    monitor-exit v5

    .line 268
    :cond_1
    :goto_0
    return-void

    .line 236
    :cond_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v4

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v6

    invoke-direct {v0, p0, v4, v6}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;II)V

    .line 238
    .local v0, "chanId":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mFoundDevices:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 240
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->getResult(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;

    move-result-object v3

    .line 241
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->isAlreadyConnected()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 243
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mConnectedDevices:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mConnectedDevices:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 249
    const/4 v2, 0x1

    .line 251
    :cond_4
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    if-eqz v3, :cond_5

    .line 257
    :try_start_1
    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->generateNewDeviceEvent(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;)V

    .line 260
    :cond_5
    if-nez v2, :cond_1

    if-eqz p2, :cond_1

    .line 262
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v4

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->generateRssiEvent(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v1

    .line 266
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    invoke-virtual {v4, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_0

    .line 251
    .end local v0    # "chanId":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    .end local v1    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method public onScanStopped(I)V
    .locals 4
    .param p1, "resultCode"    # I

    .prologue
    const/4 v3, 0x1

    .line 134
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 136
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    if-eqz v1, :cond_0

    .line 138
    monitor-exit v2

    .line 158
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mStopped:Z

    .line 143
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mFoundDevices:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 144
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 147
    .local v0, "evt":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 148
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 149
    iput p1, v0, Landroid/os/Message;->arg2:I

    .line 152
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 157
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    invoke-virtual {v1, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_0

    .line 144
    .end local v0    # "evt":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 153
    .restart local v0    # "evt":Landroid/os/Message;
    :catch_0
    move-exception v1

    goto :goto_1
.end method
