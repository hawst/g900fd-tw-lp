.class public abstract Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusLegacyCommonPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCumulativeOperatingTimeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;

.field mManufacturerAndSerialReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;

.field mVersionAndModelReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 106
    return-void
.end method


# virtual methods
.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 21
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 127
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 172
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unrecognized event received: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    iget v8, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 131
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mCumulativeOperatingTimeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;

    if-eqz v1, :cond_0

    .line 134
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v20

    .line 135
    .local v20, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 136
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 137
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeOperatingTime"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 138
    .local v5, "cumulativeOperatingTime":J
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mCumulativeOperatingTimeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;->onNewCumulativeOperatingTime(JLjava/util/EnumSet;J)V

    goto :goto_0

    .line 144
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "cumulativeOperatingTime":J
    .end local v20    # "b":Landroid/os/Bundle;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mManufacturerAndSerialReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;

    if-eqz v1, :cond_0

    .line 147
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v20

    .line 148
    .restart local v20    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 149
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 150
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_manufacturerID"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 151
    .local v11, "manufacturerID":I
    const-string v1, "int_serialNumber"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 152
    .local v12, "serialNumber":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mManufacturerAndSerialReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;

    move-wide v8, v2

    move-object v10, v4

    invoke-interface/range {v7 .. v12}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;->onNewManufacturerAndSerial(JLjava/util/EnumSet;II)V

    goto :goto_0

    .line 158
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v11    # "manufacturerID":I
    .end local v12    # "serialNumber":I
    .end local v20    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mVersionAndModelReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v20

    .line 162
    .restart local v20    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 163
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 164
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_hardwareVersion"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 165
    .local v17, "hardwareVersion":I
    const-string v1, "int_softwareVersion"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 166
    .local v18, "softwareVersion":I
    const-string v1, "int_modelNumber"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 167
    .local v19, "modelNumber":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mVersionAndModelReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;

    move-wide v14, v2

    move-object/from16 v16, v4

    invoke-interface/range {v13 .. v19}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;->onNewVersionAndModel(JLjava/util/EnumSet;III)V

    goto/16 :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0xcc
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public subscribeCumulativeOperatingTimeEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;)V
    .locals 1
    .param p1, "CumulativeOperatingTimeReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 185
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mCumulativeOperatingTimeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;

    .line 186
    if-eqz p1, :cond_0

    .line 188
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->subscribeToEvent(I)Z

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeManufacturerAndSerialEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;)V
    .locals 1
    .param p1, "ManufacturerAndSerialReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 204
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mManufacturerAndSerialReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;

    .line 205
    if-eqz p1, :cond_0

    .line 207
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->subscribeToEvent(I)Z

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeVersionAndModelEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;)V
    .locals 1
    .param p1, "VersionAndModelReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;

    .prologue
    const/16 v0, 0xce

    .line 223
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->mVersionAndModelReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;

    .line 224
    if-eqz p1, :cond_0

    .line 226
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->subscribeToEvent(I)Z

    .line 232
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
