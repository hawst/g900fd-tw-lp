.class Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;
.super Ljava/lang/Object;
.source "AntPluginPcc.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V
    .locals 0

    .prologue
    .line 737
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    .line 741
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v2, v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    .line 742
    .local v2, "temp":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle<*>;"
    if-nez v2, :cond_0

    .line 787
    :goto_0
    return v7

    .line 748
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 749
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->stateLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 751
    :try_start_1
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->isActive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 753
    monitor-exit v4

    goto :goto_0

    .line 779
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 780
    :catch_0
    move-exception v0

    .line 782
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Plugin event thread interrupted while waiting for initialization to complete."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 755
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_3
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 776
    :pswitch_0
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unrecognized plugin event received: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    :goto_1
    monitor-exit v4

    goto :goto_0

    .line 758
    :pswitch_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v3, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handlePluginEvent(Landroid/os/Message;)V

    goto :goto_1

    .line 761
    :pswitch_2
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 762
    .local v1, "newState":I
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;

    .line 764
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "State event: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/16 v3, -0x64

    if-ne v1, v3, :cond_2

    .line 768
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string v5, "Device dead"

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    goto :goto_1

    .line 772
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;->onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 755
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
