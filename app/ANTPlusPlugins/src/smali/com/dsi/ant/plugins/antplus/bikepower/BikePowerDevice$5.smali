.class synthetic Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;
.super Ljava/lang/Object;
.source "BikePowerDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 322
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I

    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->AUTO_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->MANUAL_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method
