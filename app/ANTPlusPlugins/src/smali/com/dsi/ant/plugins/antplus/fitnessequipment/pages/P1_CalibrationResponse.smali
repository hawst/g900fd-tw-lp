.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P1_CalibrationResponse.java"


# instance fields
.field private calRspsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xe2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;->calRspsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const v9, 0xffff

    const/4 v6, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 41
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;->calRspsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-nez v5, :cond_0

    .line 67
    :goto_0
    return-void

    .line 44
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 46
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 48
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;-><init>()V

    .line 50
    .local v1, "calibrationResponse":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    aget-byte v5, v5, v8

    and-int/lit8 v5, v5, 0x40

    if-nez v5, :cond_4

    move v5, v6

    :goto_1
    iput-boolean v5, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;->zeroOffsetCalibrationSuccess:Z

    .line 51
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    aget-byte v5, v5, v8

    and-int/lit16 v5, v5, 0x80

    if-nez v5, :cond_5

    :goto_2
    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;->spinDownCalibrationSuccess:Z

    .line 53
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    .line 54
    .local v2, "intTemp":I
    const/16 v5, 0xff

    if-eq v2, v5, :cond_1

    .line 55
    new-instance v5, Ljava/math/BigDecimal;

    invoke-direct {v5, v2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v8}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v8, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0x19

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    iput-object v5, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;->temperature:Ljava/math/BigDecimal;

    .line 57
    :cond_1
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v4

    .line 58
    .local v4, "zeroOffset":I
    if-eq v4, v9, :cond_2

    .line 59
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;->zeroOffset:Ljava/lang/Integer;

    .line 61
    :cond_2
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    .line 62
    .local v3, "spinDownTime":I
    if-eq v3, v9, :cond_3

    .line 63
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationResponse;->spinDownTime:Ljava/lang/Integer;

    .line 65
    :cond_3
    const-string v5, "parcelable_CalibrationResponse"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;->calRspsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .end local v2    # "intTemp":I
    .end local v3    # "spinDownTime":I
    .end local v4    # "zeroOffset":I
    :cond_4
    move v5, v7

    .line 50
    goto :goto_1

    :cond_5
    move v6, v7

    .line 51
    goto :goto_2
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;->calRspsEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 35
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
