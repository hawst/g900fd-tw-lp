.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P16_AudioVideoCommand.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# instance fields
.field audioEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public mCommandData:I

.field public mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public mControllingVideo:Z

.field public mRemoteSerialNumber:I

.field public mSequenceNumber:B

.field videoEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mControllingVideo:Z

    .line 32
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->audioEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 33
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->videoEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/16 v5, 0x8

    .line 53
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v5

    and-int/lit16 v2, v2, 0x80

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 54
    .local v1, "isVideo":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->audioEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->videoEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 58
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 61
    const-string v2, "int_serialNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    const-string v2, "int_sequenceNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x4

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    const-string v2, "int_commandNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    aget-byte v3, v3, v5

    and-int/lit8 v3, v3, 0x7f

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v2, "int_commandData"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->videoEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 82
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    :goto_1
    return-void

    .line 53
    .end local v1    # "isVideo":Ljava/lang/Boolean;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 78
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "isVideo":Ljava/lang/Boolean;
    :cond_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->audioEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public encodePage([B)V
    .locals 5
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 87
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 88
    const/4 v0, 0x1

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mRemoteSerialNumber:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 89
    const/4 v0, 0x2

    const v1, 0xff00

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mRemoteSerialNumber:I

    and-int/2addr v1, v2

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 90
    const/4 v0, 0x3

    iget-byte v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mSequenceNumber:B

    aput-byte v1, p1, v0

    .line 91
    const/4 v0, 0x4

    aput-byte v3, p1, v0

    .line 92
    const/4 v0, 0x5

    aput-byte v3, p1, v0

    .line 93
    const/4 v0, 0x6

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandData:I

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 94
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getIntValue()I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 96
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mControllingVideo:Z

    if-eqz v0, :cond_0

    .line 97
    aget-byte v0, p1, v4

    or-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 99
    :cond_0
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->audioEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->videoEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
