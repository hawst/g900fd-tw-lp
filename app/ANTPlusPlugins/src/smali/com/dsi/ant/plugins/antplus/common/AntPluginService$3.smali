.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;
.super Landroid/os/Handler;
.source "AntPluginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->composeActivitySearchParams(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)Landroid/os/Bundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$antChannel:Lcom/dsi/ant/channel/AntChannel;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 0

    .prologue
    .line 616
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 621
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ReqDev Handler received: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    const/4 v5, 0x1

    .line 623
    .local v5, "needToReleaseAntChannel":Z
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 626
    .local v6, "response":Landroid/os/Message;
    :try_start_0
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 690
    :pswitch_0
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown error from ARS activity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v6, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const/4 v7, -0x4

    iput v7, v6, Landroid/os/Message;->what:I

    .line 692
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    :cond_0
    :goto_0
    return-void

    .line 632
    :pswitch_1
    const/4 v2, 0x0

    .line 634
    .local v2, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :try_start_1
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, v7, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v8
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 636
    :try_start_2
    iget v7, p1, Landroid/os/Message;->arg1:I

    packed-switch v7, :pswitch_data_1

    .line 672
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v9, "Plugin UI search failed internally: Unrecognized req acc success code."

    invoke-static {v7, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const/4 v7, -0x4

    iput v7, v6, Landroid/os/Message;->what:I

    .line 674
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v9, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 675
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0

    .line 640
    :pswitch_2
    :try_start_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 641
    .local v1, "deviceInfo":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget v9, p1, Landroid/os/Message;->arg2:I

    const-string v10, "str_SelectedDeviceName"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v3

    .line 642
    .local v3, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7, v9, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v2

    .line 643
    if-nez v2, :cond_1

    .line 645
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v9, "Plugin UI search failed internally: Device instantiation failed."

    invoke-static {v7, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const/4 v7, -0x4

    iput v7, v6, Landroid/os/Message;->what:I

    .line 647
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v9, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 648
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0

    .line 650
    :cond_1
    const/4 v5, 0x0

    .line 677
    .end local v1    # "deviceInfo":Landroid/os/Bundle;
    .end local v3    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :cond_2
    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 680
    :try_start_5
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v2, v9, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v7

    if-nez v7, :cond_3

    .line 681
    const/4 v5, 0x1

    .line 704
    :cond_3
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0

    .line 654
    :pswitch_3
    :try_start_6
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 655
    .local v0, "chan_devId":I
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    const/4 v9, 0x0

    invoke-virtual {v7, v0, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v2

    .line 656
    if-nez v2, :cond_4

    .line 659
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v9, "Already connected device disconnected during search"

    invoke-static {v7, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    const/4 v7, -0x4

    iput v7, v6, Landroid/os/Message;->what:I

    .line 661
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 662
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_0

    .line 664
    :cond_4
    :try_start_7
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 666
    const/4 v7, -0x8

    iput v7, v6, Landroid/os/Message;->what:I

    .line 667
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v9, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 668
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_0

    .line 677
    .end local v0    # "chan_devId":I
    :catchall_0
    move-exception v7

    :try_start_8
    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v7
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 696
    .end local v2    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :catch_0
    move-exception v4

    .line 699
    .local v4, "e":Landroid/os/RemoteException;
    :try_start_a
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "RemoteException sending search result to client"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_0

    .line 686
    .end local v4    # "e":Landroid/os/RemoteException;
    :pswitch_4
    const/4 v7, -0x2

    :try_start_b
    iput v7, v6, Landroid/os/Message;->what:I

    .line 687
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 704
    if-eqz v5, :cond_0

    .line 705
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto/16 :goto_0

    .line 704
    :catchall_1
    move-exception v7

    if-eqz v5, :cond_5

    .line 705
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;->val$antChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v8}, Lcom/dsi/ant/channel/AntChannel;->release()V

    :cond_5
    throw v7

    .line 626
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 636
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
