.class public Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;
.source "BikeSpeedDistanceDevice.java"


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 0
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 19
    return-void
.end method


# virtual methods
.method public addNonLegacyPages(Ljava/util/List;Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;)V
    .locals 1
    .param p2, "defaultPage"    # Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "addToThisList":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P4_BatteryStatus;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P5_MotionAndSpeedData;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P5_MotionAndSpeedData;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method

.method public getDefaultPage()Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/pages/P0_RawSpeedData;-><init>()V

    return-object v0
.end method
