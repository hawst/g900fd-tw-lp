.class public Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker;
.super Ljava/lang/Object;
.source "DialogBuilder_ProximityPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static build(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;)Landroid/app/AlertDialog;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "initialValue"    # I
    .param p2, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;

    .prologue
    .line 27
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v7, "Set Search Proximity Threshold"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 30
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 31
    .local v2, "li":Landroid/view/LayoutInflater;
    const v7, 0x7f030005

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 32
    .local v6, "view":Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 34
    const v7, 0x7f06000d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 35
    .local v1, "checkbox_Enable":Landroid/widget/CheckBox;
    const v7, 0x7f060011

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    .line 36
    .local v4, "seekbar_ProxValue":Landroid/widget/SeekBar;
    const v7, 0x7f060010

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 37
    .local v3, "relativeLayout_ProxValueControls":Landroid/widget/RelativeLayout;
    const v7, 0x7f06000f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 38
    .local v5, "textView_ProxValueDisplay":Landroid/widget/TextView;
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 39
    new-instance v7, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;

    invoke-direct {v7, v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$1;-><init>(Landroid/widget/RelativeLayout;Landroid/widget/SeekBar;Landroid/widget/TextView;)V

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 59
    if-nez p1, :cond_0

    .line 61
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 62
    const-string v7, "Threshold Rating: OFF"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :goto_0
    new-instance v7, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$2;

    invoke-direct {v7, v5}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$2;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v4, v7}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 92
    const-string v7, "Set"

    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;

    invoke-direct {v8, v1, v4, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;-><init>(Landroid/widget/CheckBox;Landroid/widget/SeekBar;ILcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 106
    const-string v7, "Cancel"

    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$4;

    invoke-direct {v8}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$4;-><init>()V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7

    .line 66
    :cond_0
    add-int/lit8 v7, p1, -0x1

    invoke-virtual {v4, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 67
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Threshold Rating: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
