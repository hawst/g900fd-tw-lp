.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P54_Capabilities.java"


# instance fields
.field private capEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 19
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xe0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;->capEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 39
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;->capEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "long_EstTimestamp"

    invoke-virtual {v0, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 44
    const-string v4, "long_EventFlags"

    invoke-virtual {v0, v4, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 46
    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;-><init>()V

    .line 48
    .local v2, "capabilities":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v7, 0x6

    invoke-static {v4, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    .line 49
    .local v3, "maxResistance":I
    const v4, 0xffff

    if-eq v3, v4, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_1
    iput-object v4, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;->maximumResistance:Ljava/lang/Integer;

    .line 51
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/16 v7, 0x8

    aget-byte v4, v4, v7

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    .line 53
    .local v1, "bitField":I
    and-int/lit8 v4, v1, 0x1

    if-nez v4, :cond_2

    move v4, v5

    :goto_2
    iput-boolean v4, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;->basicResistanceModeSupport:Z

    .line 54
    and-int/lit8 v4, v1, 0x2

    if-nez v4, :cond_3

    move v4, v5

    :goto_3
    iput-boolean v4, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;->targetPowerModeSupport:Z

    .line 55
    and-int/lit8 v4, v1, 0x4

    if-nez v4, :cond_4

    :goto_4
    iput-boolean v5, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Capabilities;->simulationModeSupport:Z

    .line 57
    const-string v4, "parcelable_Capabilities"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;->capEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v4, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0

    .line 49
    .end local v1    # "bitField":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .restart local v1    # "bitField":I
    :cond_2
    move v4, v6

    .line 53
    goto :goto_2

    :cond_3
    move v4, v6

    .line 54
    goto :goto_3

    :cond_4
    move v5, v6

    .line 55
    goto :goto_4
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;->capEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
