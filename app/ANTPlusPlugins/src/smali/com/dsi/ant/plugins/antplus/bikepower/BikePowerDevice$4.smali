.class Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;
.super Ljava/lang/Object;
.source "BikePowerDevice.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->getPageList()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewCtfOffsetReceived(I)V
    .locals 5
    .param p1, "ctfOffset"    # I

    .prologue
    const-wide/16 v3, 0x7d0

    .line 159
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$000(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->saveLiveCtfOffset:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$700(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 163
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->saveLiveCtfOffset:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$700(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->unflagManualCalibration:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$900(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # setter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I
    invoke-static {v0, p1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$302(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;I)I

    .line 186
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->savedAutoCtfOffset:Z
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->put(I)V

    .line 175
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->wasFilled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->isContentsWithinRange(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$300(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I

    move-result v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$300(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I

    move-result v1

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->isValueWithinRange(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;->getMean()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    # setter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$302(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;I)I

    .line 179
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->saveLiveCtfOffset:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$700(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->resetCtfOffsetBuffer:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$1000(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->resetCtfOffsetBuffer:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->access$1000(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
