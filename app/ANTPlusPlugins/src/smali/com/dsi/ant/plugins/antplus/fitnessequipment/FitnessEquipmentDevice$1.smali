.class Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;
.super Ljava/lang/Object;
.source "FitnessEquipmentDevice.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExecutorDeath()V
    .locals 2

    .prologue
    .line 119
    # getter for: Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ANTFS Executor died"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    const/4 v1, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->access$102(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 121
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->onChannelDeath()V

    .line 122
    return-void
.end method
