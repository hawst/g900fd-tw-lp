.class public Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "GeocacheService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 1
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v1, "str_PluginName"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->GEOCACHE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v1, "predefinednetwork_NetKey"

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 40
    const-string v1, "int_RfFreq"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    const-string v1, "int_TransType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    const-string v1, "int_DevType"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->GEOCACHE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    const-string v1, "int_Period"

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 50
    const/16 v4, 0x12c

    if-eq p1, v4, :cond_1

    .line 51
    const/4 v3, 0x0

    .line 79
    :cond_0
    :goto_0
    return v3

    .line 53
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->isDeviceClosed()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 55
    :cond_2
    sget-object v4, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {p0, v4, v5, p2, p3}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 56
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v0, :cond_0

    .line 61
    :try_start_0
    new-instance v4, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-direct {v4, v0, p0}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v0    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {p0, p3, v4, p2, v5}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 76
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->activeReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->closeDevice()V

    goto :goto_0

    .line 62
    .restart local v0    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/nio/channels/ClosedChannelException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->TAG:Ljava/lang/String;

    const-string v5, "Failed to instantiate device: Constructor threw ClosedChannelException."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 66
    .local v2, "response":Landroid/os/Message;
    const/4 v4, -0x4

    iput v4, v2, Landroid/os/Message;->what:I

    .line 67
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0
.end method
