.class Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;
.super Ljava/lang/Object;
.source "PluginDownloadFilesHelper.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;

.field final synthetic val$cachedCurrentCumulativeBytes:J

.field final synthetic val$totalBytesToDownload:J


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;JJ)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;

    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->val$cachedCurrentCumulativeBytes:J

    iput-wide p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->val$totalBytesToDownload:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransferUpdate(JJ)V
    .locals 6
    .param p1, "transferredBytes"    # J
    .param p3, "totalBytes"    # J

    .prologue
    .line 221
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v1, 0x352

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->val$cachedCurrentCumulativeBytes:J

    add-long/2addr v2, p1

    iget-wide v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;->val$totalBytesToDownload:J

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handleStateChange(IJJ)V

    .line 223
    return-void
.end method
