.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostAuthChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask$1;
    }
.end annotation


# static fields
.field static AUTH_PAIRING:I = 0x0

.field static AUTH_PASSKEY:I = 0x0

.field static AUTH_PASSTHRU:I = 0x0

.field static AUTH_RESPONSE_ACCEPT:I = 0x0

.field static AUTH_RESPONSE_ID:B = 0x0t

.field static AUTH_RESPONSE_NA:I = 0x0

.field static AUTH_RESPONSE_REJECT:I = 0x0

.field static AUTH_SERIAL_NUMBER:I = 0x0

.field static AUTH_UNKNOWN:I = 0x0

.field static BEACON_AUTH:I = 0x0

.field static BEACON_BUSY:I = 0x0

.field static BEACON_ID:B = 0x0t

.field static BEACON_LINK:I = 0x0

.field static BEACON_TRANS:I = 0x0

.field static BEACON_UNKNOWN:I = 0x0

.field static COMMAND_RESPONSE_ID:B = 0x0t

.field static final PING_REQUEST_ID:B = 0x5t

.field private static final TAG:Ljava/lang/String;

.field static final pingCommand:[B


# instance fields
.field authCmdSent:Z

.field authCommand:[B

.field burstResponse:[B

.field burstRx:Ljava/io/ByteArrayOutputStream;

.field busyCount:I

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field latestBeaconState:I

.field mClientAntDeviceNumber:I

.field mClientAntFsDeviceType:I

.field mClientAntFsManufacturerId:I

.field mClientSerialNumber:J

.field mHostAntFsSerialNumber:J

.field mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field msgRetries:I

.field newBeaconState:I

.field pairingEnabled:Z

.field passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

.field searchTimeoutChannelClosed:Z

.field supportedAuthType:I

.field transferInProgress:Z

.field waitingForPairingResponse:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    .line 30
    const/16 v0, 0x43

    sput-byte v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    .line 31
    const/16 v0, 0x44

    sput-byte v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    .line 32
    const/16 v0, -0x7c

    sput-byte v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    .line 35
    sput v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    .line 36
    sput v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    .line 37
    sput v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_AUTH:I

    .line 38
    sput v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    .line 39
    sput v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_BUSY:I

    .line 41
    sput v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_UNKNOWN:I

    .line 42
    sput v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSTHRU:I

    .line 43
    sput v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_SERIAL_NUMBER:I

    .line 44
    sput v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    .line 45
    sput v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSKEY:I

    .line 47
    sput v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    .line 48
    sput v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ACCEPT:I

    .line 49
    sput v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    .line 51
    const/16 v0, 0x8

    new-array v0, v0, [B

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    aput-byte v1, v0, v2

    const/4 v1, 0x5

    aput-byte v1, v0, v3

    aput-byte v2, v0, v4

    aput-byte v2, v0, v5

    const/4 v1, 0x4

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    aput-byte v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->pingCommand:[B

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;IIIJ)V
    .locals 2
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "passkeyDatabase"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;
    .param p3, "clientAntDeviceNumber"    # I
    .param p4, "clientAntFsManufacturerId"    # I
    .param p5, "clientAntFsDeviceType"    # I
    .param p6, "antFsHostSerialNumber"    # J

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 58
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 61
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    .line 62
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 63
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->latestBeaconState:I

    .line 64
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->pairingEnabled:Z

    .line 66
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    .line 71
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    .line 73
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    .line 74
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    .line 75
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    .line 77
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->busyCount:I

    .line 83
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    .line 84
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntDeviceNumber:I

    .line 85
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsManufacturerId:I

    .line 86
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsDeviceType:I

    .line 87
    iput-wide p6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mHostAntFsSerialNumber:J

    .line 88
    return-void
.end method

.method private requestPairing()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x5

    const/4 v13, 0x4

    const/16 v12, 0xa

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 595
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v12, :cond_0

    .line 598
    const/16 v9, 0x8

    new-array v9, v9, [B

    const/16 v10, 0x44

    aput-byte v10, v9, v8

    aput-byte v13, v9, v7

    const/4 v10, 0x2

    sget v11, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    int-to-byte v11, v11

    aput-byte v11, v9, v10

    const/4 v10, 0x3

    aput-byte v8, v9, v10

    aput-byte v8, v9, v13

    aput-byte v8, v9, v14

    const/4 v10, 0x6

    aput-byte v8, v9, v10

    const/4 v10, 0x7

    aput-byte v8, v9, v10

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    .line 599
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    iget-wide v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mHostAntFsSerialNumber:J

    invoke-static {v9, v13, v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 601
    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 603
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForCommandResponse()Z

    move-result v9

    if-nez v9, :cond_1

    .line 605
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Failed waiting for pairing cmd to be sent"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :cond_0
    :goto_1
    return-void

    .line 610
    :cond_1
    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v10, "Wait for pairing command response"

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    .line 613
    invoke-direct {p0, v14}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 615
    const/4 v6, 0x0

    .line 616
    .local v6, "waitTime":I
    :cond_2
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->latestBeaconState:I

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_BUSY:I

    if-ne v9, v10, :cond_3

    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-nez v9, :cond_3

    .line 618
    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 619
    add-int/lit8 v6, v6, 0x1

    .line 622
    const/16 v9, 0x78

    if-le v6, v9, :cond_2

    .line 624
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Failed waiting for client to send pairing response"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 629
    :cond_3
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v9, v10, :cond_6

    move v2, v7

    .line 630
    .local v2, "reachedTransportState":Z
    :goto_2
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v9, v10, :cond_7

    move v0, v7

    .line 632
    .local v0, "droppedToLink":Z
    :goto_3
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v9, v9

    const/16 v10, 0x10

    if-lt v9, v10, :cond_d

    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    if-nez v9, :cond_d

    .line 634
    sget-byte v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v10, v10, v8

    if-ne v9, v10, :cond_8

    sget-byte v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v11, 0x8

    aget-byte v10, v10, v11

    if-ne v9, v10, :cond_8

    sget-byte v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v11, 0x9

    aget-byte v10, v10, v11

    if-ne v9, v10, :cond_8

    move v3, v7

    .line 637
    .local v3, "receivedAuthResponse":Z
    :goto_4
    if-eqz v3, :cond_9

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v10, v10, v12

    if-ne v9, v10, :cond_9

    move v5, v7

    .line 638
    .local v5, "receivedSerialMessage":Z
    :goto_5
    if-eqz v3, :cond_a

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v10, v10, v12

    if-eq v9, v10, :cond_a

    move v4, v7

    .line 640
    .local v4, "receivedResponse":Z
    :goto_6
    if-eqz v3, :cond_4

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v10, v10, v12

    if-eq v9, v10, :cond_0

    .line 644
    :cond_4
    if-eqz v4, :cond_5

    if-nez v5, :cond_5

    .line 646
    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v10, "Wait for beacon"

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 648
    invoke-direct {p0, v14}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 650
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v9, v10, :cond_b

    move v2, v7

    .line 651
    :goto_7
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v9, v10, :cond_c

    move v0, v7

    .line 665
    .end local v3    # "receivedAuthResponse":Z
    .end local v4    # "receivedResponse":Z
    .end local v5    # "receivedSerialMessage":Z
    :cond_5
    :goto_8
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    .line 595
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .end local v0    # "droppedToLink":Z
    .end local v2    # "reachedTransportState":Z
    :cond_6
    move v2, v8

    .line 629
    goto :goto_2

    .restart local v2    # "reachedTransportState":Z
    :cond_7
    move v0, v8

    .line 630
    goto :goto_3

    .restart local v0    # "droppedToLink":Z
    :cond_8
    move v3, v8

    .line 634
    goto :goto_4

    .restart local v3    # "receivedAuthResponse":Z
    :cond_9
    move v5, v8

    .line 637
    goto :goto_5

    .restart local v5    # "receivedSerialMessage":Z
    :cond_a
    move v4, v8

    .line 638
    goto :goto_6

    .restart local v4    # "receivedResponse":Z
    :cond_b
    move v2, v8

    .line 650
    goto :goto_7

    :cond_c
    move v0, v8

    .line 651
    goto :goto_8

    .line 654
    .end local v3    # "receivedAuthResponse":Z
    .end local v4    # "receivedResponse":Z
    .end local v5    # "receivedSerialMessage":Z
    :cond_d
    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v9, :cond_e

    .line 656
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Did not see a pairing response, channel closed."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 662
    :cond_e
    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v10, "Did not see a pairing response, retry pairing command"

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method private requestPasskey([B)V
    .locals 11
    .param p1, "passkey"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 765
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v7, 0xf

    if-ge v2, v7, :cond_0

    .line 768
    const/16 v7, 0x8

    new-array v0, v7, [B

    const/4 v7, 0x0

    const/16 v8, 0x44

    aput-byte v8, v0, v7

    const/4 v7, 0x1

    const/4 v8, 0x4

    aput-byte v8, v0, v7

    const/4 v7, 0x2

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSKEY:I

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    const/4 v7, 0x3

    array-length v8, p1

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    const/4 v7, 0x4

    const/4 v8, 0x0

    aput-byte v8, v0, v7

    const/4 v7, 0x5

    const/4 v8, 0x0

    aput-byte v8, v0, v7

    const/4 v7, 0x6

    const/4 v8, 0x0

    aput-byte v8, v0, v7

    const/4 v7, 0x7

    const/4 v8, 0x0

    aput-byte v8, v0, v7

    .line 769
    .local v0, "authCommandHeader":[B
    const/4 v7, 0x4

    iget-wide v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mHostAntFsSerialNumber:J

    invoke-static {v0, v7, v8, v9}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 771
    array-length v7, v0

    array-length v8, p1

    add-int/2addr v7, v8

    new-array v7, v7, [B

    iput-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    .line 772
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    const/4 v9, 0x0

    array-length v10, v0

    invoke-static {v0, v7, v8, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 773
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    array-length v9, v0

    array-length v10, p1

    invoke-static {p1, v7, v8, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 775
    sget v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 777
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForCommandResponse()Z

    move-result v7

    if-nez v7, :cond_1

    .line 779
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Failed waiting for passkey cmd to be sent"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    .end local v0    # "authCommandHeader":[B
    :cond_0
    :goto_1
    return-void

    .line 783
    .restart local v0    # "authCommandHeader":[B
    :cond_1
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Wait for passkey command response"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 786
    iget v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v7, v8, :cond_4

    const/4 v3, 0x1

    .line 787
    .local v3, "reachedTransportState":Z
    :goto_2
    iget v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v7, v8, :cond_5

    const/4 v1, 0x1

    .line 789
    .local v1, "droppedToLink":Z
    :goto_3
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v7, v7

    const/16 v8, 0x10

    if-lt v7, v8, :cond_b

    .line 791
    sget-byte v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/4 v9, 0x0

    aget-byte v8, v8, v9

    if-ne v7, v8, :cond_6

    sget-byte v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v9, 0x8

    aget-byte v8, v8, v9

    if-ne v7, v8, :cond_6

    sget-byte v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v9, 0x9

    aget-byte v8, v8, v9

    if-ne v7, v8, :cond_6

    const/4 v4, 0x1

    .line 794
    .local v4, "receivedAuthResponse":Z
    :goto_4
    if-eqz v4, :cond_7

    sget v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v9, 0xa

    aget-byte v8, v8, v9

    if-ne v7, v8, :cond_7

    const/4 v6, 0x1

    .line 795
    .local v6, "receivedSerialMessage":Z
    :goto_5
    if-eqz v4, :cond_8

    sget v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v9, 0xa

    aget-byte v8, v8, v9

    if-eq v7, v8, :cond_8

    const/4 v5, 0x1

    .line 797
    .local v5, "receivedResponse":Z
    :goto_6
    if-eqz v4, :cond_2

    sget v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v9, 0xa

    aget-byte v8, v8, v9

    if-eq v7, v8, :cond_0

    .line 801
    :cond_2
    if-eqz v5, :cond_3

    if-nez v6, :cond_3

    .line 803
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Wait for beacon"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    sget v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 805
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 807
    iget v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v7, v8, :cond_9

    const/4 v3, 0x1

    .line 808
    :goto_7
    iget v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v7, v8, :cond_a

    const/4 v1, 0x1

    .line 822
    .end local v4    # "receivedAuthResponse":Z
    .end local v5    # "receivedResponse":Z
    .end local v6    # "receivedSerialMessage":Z
    :cond_3
    :goto_8
    if-nez v3, :cond_0

    if-nez v1, :cond_0

    .line 765
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 786
    .end local v1    # "droppedToLink":Z
    .end local v3    # "reachedTransportState":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 787
    .restart local v3    # "reachedTransportState":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 791
    .restart local v1    # "droppedToLink":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_4

    .line 794
    .restart local v4    # "receivedAuthResponse":Z
    :cond_7
    const/4 v6, 0x0

    goto :goto_5

    .line 795
    .restart local v6    # "receivedSerialMessage":Z
    :cond_8
    const/4 v5, 0x0

    goto :goto_6

    .line 807
    .restart local v5    # "receivedResponse":Z
    :cond_9
    const/4 v3, 0x0

    goto :goto_7

    .line 808
    :cond_a
    const/4 v1, 0x0

    goto :goto_8

    .line 811
    .end local v4    # "receivedAuthResponse":Z
    .end local v5    # "receivedResponse":Z
    .end local v6    # "receivedSerialMessage":Z
    :cond_b
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v7, :cond_c

    .line 813
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Did not see a passkey response, channel closed."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 819
    :cond_c
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v8, "Did not see a passkey response, retry passkey command"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method private requestPassthrough()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x8

    const/16 v12, 0xa

    const/4 v11, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 704
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v8, 0xf

    if-ge v1, v8, :cond_0

    .line 707
    new-array v8, v13, [B

    const/16 v9, 0x44

    aput-byte v9, v8, v7

    aput-byte v11, v8, v6

    const/4 v9, 0x2

    sget v10, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSTHRU:I

    int-to-byte v10, v10

    aput-byte v10, v8, v9

    const/4 v9, 0x3

    aput-byte v7, v8, v9

    aput-byte v7, v8, v11

    const/4 v9, 0x5

    aput-byte v7, v8, v9

    const/4 v9, 0x6

    aput-byte v7, v8, v9

    const/4 v9, 0x7

    aput-byte v7, v8, v9

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    .line 708
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    iget-wide v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mHostAntFsSerialNumber:J

    invoke-static {v8, v11, v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 710
    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 712
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForCommandResponse()Z

    move-result v8

    if-nez v8, :cond_1

    .line 714
    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed waiting for passthrough cmd to be sent"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_0
    :goto_1
    return-void

    .line 719
    :cond_1
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Wait for passthrough command response"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 722
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v8, v9, :cond_4

    move v2, v6

    .line 723
    .local v2, "reachedTransportState":Z
    :goto_2
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v8, v9, :cond_5

    move v0, v6

    .line 725
    .local v0, "droppedToLink":Z
    :goto_3
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v8, v8

    const/16 v9, 0x10

    if-lt v8, v9, :cond_b

    .line 727
    sget-byte v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v9, v9, v7

    if-ne v8, v9, :cond_6

    sget-byte v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v9, v9, v13

    if-ne v8, v9, :cond_6

    sget-byte v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v10, 0x9

    aget-byte v9, v9, v10

    if-ne v8, v9, :cond_6

    move v3, v6

    .line 730
    .local v3, "receivedAuthResponse":Z
    :goto_4
    if-eqz v3, :cond_7

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v9, v9, v12

    if-ne v8, v9, :cond_7

    move v5, v6

    .line 731
    .local v5, "receivedSerialMessage":Z
    :goto_5
    if-eqz v3, :cond_8

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_NA:I

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v9, v9, v12

    if-eq v8, v9, :cond_8

    move v4, v6

    .line 733
    .local v4, "receivedResponse":Z
    :goto_6
    if-eqz v3, :cond_2

    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    aget-byte v9, v9, v12

    if-eq v8, v9, :cond_0

    .line 737
    :cond_2
    if-eqz v4, :cond_3

    if-nez v5, :cond_3

    .line 739
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Wait for beacon"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    sget v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    iput v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 741
    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 743
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v8, v9, :cond_9

    move v2, v6

    .line 744
    :goto_7
    iget v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v8, v9, :cond_a

    move v0, v6

    .line 758
    .end local v3    # "receivedAuthResponse":Z
    .end local v4    # "receivedResponse":Z
    .end local v5    # "receivedSerialMessage":Z
    :cond_3
    :goto_8
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    .line 704
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .end local v0    # "droppedToLink":Z
    .end local v2    # "reachedTransportState":Z
    :cond_4
    move v2, v7

    .line 722
    goto :goto_2

    .restart local v2    # "reachedTransportState":Z
    :cond_5
    move v0, v7

    .line 723
    goto :goto_3

    .restart local v0    # "droppedToLink":Z
    :cond_6
    move v3, v7

    .line 727
    goto :goto_4

    .restart local v3    # "receivedAuthResponse":Z
    :cond_7
    move v5, v7

    .line 730
    goto :goto_5

    .restart local v5    # "receivedSerialMessage":Z
    :cond_8
    move v4, v7

    .line 731
    goto :goto_6

    .restart local v4    # "receivedResponse":Z
    :cond_9
    move v2, v7

    .line 743
    goto :goto_7

    :cond_a
    move v0, v7

    .line 744
    goto :goto_8

    .line 747
    .end local v3    # "receivedAuthResponse":Z
    .end local v4    # "receivedResponse":Z
    .end local v5    # "receivedSerialMessage":Z
    :cond_b
    iget-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v8, :cond_c

    .line 749
    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v7, "Did not see a passthrough response, channel closed."

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 755
    :cond_c
    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v9, "Did not see a passthrough response, retry passthrough command"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method private requestSerialNumber()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 672
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    .line 674
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    .line 677
    const/16 v1, 0x8

    new-array v1, v1, [B

    const/16 v2, 0x44

    aput-byte v2, v1, v4

    aput-byte v5, v1, v6

    const/4 v2, 0x2

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_SERIAL_NUMBER:I

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    const/4 v2, 0x3

    aput-byte v4, v1, v2

    aput-byte v4, v1, v5

    const/4 v2, 0x5

    aput-byte v4, v1, v2

    const/4 v2, 0x6

    aput-byte v4, v1, v2

    const/4 v2, 0x7

    aput-byte v4, v1, v2

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    .line 678
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mHostAntFsSerialNumber:J

    invoke-static {v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 680
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForCommandResponse()Z

    move-result v1

    if-nez v1, :cond_1

    .line 682
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Failed waiting for serial number cmd to be sent"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    :cond_0
    :goto_1
    return-void

    .line 686
    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Wait for serial number command response"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 689
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v1, :cond_2

    .line 691
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Aborting serial number request on closed channel"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 695
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 696
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Rx failure when receiving client serial number request response, retry"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private responseProcessed()V
    .locals 1

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->disableMessageProcessing()V

    .line 871
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 872
    return-void
.end method

.method private waitForCommandResponse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 829
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    .line 831
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v2, :cond_1

    .line 833
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Not waiting for command response, channel already closed on search timeout"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    :cond_0
    :goto_0
    return v0

    .line 837
    :cond_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Waiting 30 seconds for command response"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/16 v2, 0x1e

    invoke-direct {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 841
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v2, v3, :cond_3

    .line 843
    :cond_2
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    goto :goto_0

    .line 847
    :cond_3
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-nez v2, :cond_0

    .line 850
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    if-nez v2, :cond_4

    .line 852
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Auth Tx retries exceeded"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 855
    :cond_4
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v2, "Command successfully sent"

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 856
    goto :goto_0
.end method

.method private waitForResponse(I)V
    .locals 4
    .param p1, "seconds"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 862
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 863
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->enableMessageProcessing()V

    .line 864
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    int-to-long v1, p1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 865
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 329
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 339
    .local v10, "channelState":Lcom/dsi/ant/message/ChannelState;
    :try_start_1
    sget-object v1, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v10, v1, :cond_0

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v10, v1, :cond_0

    .line 342
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed: Connection lost"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 344
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 345
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 578
    .end local v10    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :goto_0
    return-void

    .line 331
    :catch_0
    move-exception v12

    .line 333
    .local v12, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE occurred requesting status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 335
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 336
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 570
    .end local v12    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v12

    .line 572
    .local v12, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Interrupted waiting for result"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 574
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_FAIL_NO_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 575
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 576
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 349
    .end local v12    # "e":Ljava/lang/InterruptedException;
    .restart local v10    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_0
    :try_start_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 350
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->LINK_PASS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 353
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Wait for auth beacon"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    sget v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_UNKNOWN:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    .line 355
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    .line 356
    const/16 v1, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitForResponse(I)V

    .line 358
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    if-eqz v1, :cond_1

    .line 360
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Aborting authentication process on closed channel"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 362
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 363
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 367
    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSTHRU:I

    if-ne v1, v3, :cond_3

    .line 369
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Sending passthrough command"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_REQUESTING_PASSTHROUGH:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 371
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_BEACON_FOUND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->requestPassthrough()V

    .line 411
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    const/4 v3, 0x2

    aget-byte v1, v1, v3

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_SERIAL_NUMBER:I

    int-to-byte v3, v3

    if-ne v1, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v1, v1

    const/16 v3, 0x10

    if-lt v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    .line 415
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v3, 0xc

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    .line 416
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Successfully received serial number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->open()V

    .line 418
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsManufacturerId:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsDeviceType:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntDeviceNumber:I

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    move-result-object v11

    .line 419
    .local v11, "clientPasskeyInfo":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->close()V

    .line 421
    if-nez v11, :cond_7

    .line 423
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "No passkey stored, request pairing"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_WAITING_FOR_PAIRING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 425
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_BEACON_FOUND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 426
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->requestPairing()V

    .line 465
    .end local v11    # "clientPasskeyInfo":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v1, v1

    const/16 v3, 0x10

    if-lt v1, v3, :cond_10

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_10

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_10

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_10

    .line 470
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v3, 0xa

    aget-byte v8, v1, v3

    .line 472
    .local v8, "authResponse":I
    sget v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ACCEPT:I

    if-eq v8, v1, :cond_b

    .line 474
    sget v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    if-ne v8, v1, :cond_a

    .line 476
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Authentication request rejected"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 479
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    const/4 v3, 0x2

    aget-byte v1, v1, v3

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    int-to-byte v3, v3

    if-ne v1, v3, :cond_9

    .line 480
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT_PAIRING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 484
    :goto_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_AUTHENTICATION_REJECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 374
    .end local v8    # "authResponse":I
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    if-ne v1, v3, :cond_5

    .line 376
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->pairingEnabled:Z

    if-eqz v1, :cond_4

    .line 378
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Sending pairing command"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_WAITING_FOR_PAIRING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 380
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_BEACON_FOUND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 381
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->requestPairing()V

    goto/16 :goto_1

    .line 387
    :cond_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Pairing authentication required, but not enabled in client device"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 389
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT_PAIRING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 390
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_AUTHENTICATION_REJECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 394
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSKEY:I

    if-ne v1, v3, :cond_6

    .line 396
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Sending serial number command"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_REQUESTING_SERIAL:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 398
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_BEACON_FOUND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 399
    invoke-direct/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->requestSerialNumber()V

    goto/16 :goto_1

    .line 403
    :cond_6
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Invalid authentication type in beacon"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 405
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_FAIL_BAD_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 406
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 430
    .restart local v11    # "clientPasskeyInfo":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_7
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Passkey found, request passkey auth"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v1, v11, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->requestPasskey([B)V

    .line 433
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v1, v1

    const/16 v3, 0x10

    if-lt v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->COMMAND_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    sget-byte v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ID:B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    if-ne v1, v3, :cond_2

    .line 436
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v3, 0xa

    aget-byte v8, v1, v3

    .line 438
    .restart local v8    # "authResponse":I
    sget v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_REJECT:I

    if-ne v8, v1, :cond_8

    .line 440
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->open()V

    .line 441
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1, v11}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->close()V

    .line 443
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Passkey authentication request rejected"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 445
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT_BAD_PASSKEY:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 446
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_AUTHENTICATION_REJECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 449
    :cond_8
    sget v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_RESPONSE_ACCEPT:I

    if-eq v8, v1, :cond_2

    .line 451
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->open()V

    .line 452
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1, v11}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V

    .line 453
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->close()V

    .line 454
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Undefined passkey authentication response "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 456
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_FAIL_BAD_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 457
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 482
    .end local v11    # "clientPasskeyInfo":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    goto/16 :goto_2

    .line 489
    :cond_a
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 490
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_FAIL_BAD_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 491
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Undefined authentication response "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 498
    :cond_b
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Authentication request accepted"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    const/4 v3, 0x2

    aget-byte v1, v1, v3

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    int-to-byte v3, v3

    if-ne v1, v3, :cond_10

    .line 502
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v3, 0xb

    aget-byte v9, v1, v3

    .line 504
    .local v9, "authStringLength":I
    if-lez v9, :cond_10

    .line 507
    div-int/lit8 v1, v9, 0x8

    mul-int/lit8 v1, v1, 0x8

    add-int/lit8 v13, v1, 0x10

    .line 508
    .local v13, "expectedResponseLength":I
    rem-int/lit8 v1, v9, 0x8

    if-eqz v1, :cond_c

    .line 509
    add-int/lit8 v13, v13, 0x8

    .line 511
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v1, v1

    if-eq v1, v13, :cond_d

    .line 514
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Client reported mismatched auth message length, told us "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but we received "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    array-length v4, v4

    add-int/lit8 v4, v4, -0x10

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v13, -0x10

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 517
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_FAIL_BAD_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 518
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 522
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    const/16 v3, 0xc

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v6

    .line 526
    .local v6, "clientSerialNumber":J
    const-wide/16 v3, 0x0

    cmp-long v1, v6, v3

    if-nez v1, :cond_e

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    const-wide/16 v15, 0x0

    cmp-long v1, v3, v15

    if-eqz v1, :cond_e

    .line 527
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientSerialNumber:J

    .line 529
    :cond_e
    new-array v2, v9, [B

    .line 531
    .local v2, "passkey":[B
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    if-ge v14, v9, :cond_f

    .line 532
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    add-int/lit8 v3, v14, 0x10

    aget-byte v1, v1, v3

    aput-byte v1, v2, v14

    .line 531
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 535
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->open()V

    .line 536
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsManufacturerId:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsDeviceType:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntDeviceNumber:I

    invoke-direct/range {v1 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;-><init>([BIIIJ)V

    invoke-interface {v15, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V

    .line 538
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->passkeyDatabase:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-interface {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;->close()V

    .line 539
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Passkey for device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " saved after pairing"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    .end local v2    # "passkey":[B
    .end local v6    # "clientSerialNumber":J
    .end local v8    # "authResponse":I
    .end local v9    # "authStringLength":I
    .end local v13    # "expectedResponseLength":I
    .end local v14    # "i":I
    :cond_10
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    if-ne v1, v3, :cond_11

    .line 548
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Successful transition to Transport state"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 550
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_PASS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 551
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 554
    :cond_11
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    if-ne v1, v3, :cond_12

    .line 556
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Auth failed, host dropped to link"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 558
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 559
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_AUTHENTICATION_REJECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 564
    :cond_12
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Timed out waiting for transport beacon"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 566
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->AUTHENTICATE_REJECT:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 567
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    const-string v0, "ANT-FS Host Auth Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 590
    const/4 v0, 0x1

    return v0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 96
    :try_start_0
    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 319
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 100
    :pswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v5, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v5, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 103
    :pswitch_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Search timeout occured"

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 291
    :catch_0
    move-exception v1

    .line 293
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v5, :cond_10

    .line 297
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_3
    :try_start_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Channel closed"

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->searchTimeoutChannelClosed:Z

    .line 110
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 313
    :catch_1
    move-exception v1

    .line 315
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException receiving burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto :goto_0

    .line 113
    .end local v1    # "e":Ljava/io/IOException;
    :pswitch_4
    const/4 v3, 0x0

    :try_start_2
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    .line 114
    const/4 v3, -0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    .line 115
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    if-nez v3, :cond_0

    .line 118
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    .line 120
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 123
    :pswitch_5
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    goto :goto_0

    .line 143
    :pswitch_6
    sget-byte v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x1

    aget-byte v6, v6, v7

    if-ne v5, v6, :cond_1

    .line 145
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->latestBeaconState:I

    .line 148
    :cond_1
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    sget v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_UNKNOWN:I

    if-ne v5, v6, :cond_4

    .line 151
    sget-byte v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x1

    aget-byte v6, v6, v7

    if-ne v5, v6, :cond_0

    sget v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_AUTH:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x3

    aget-byte v6, v6, v7

    if-ne v5, v6, :cond_0

    .line 154
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    .line 157
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsManufacturerId:I

    const/16 v6, 0x16

    if-ne v5, v6, :cond_2

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->mClientAntFsDeviceType:I

    const/16 v6, 0x12

    if-ne v5, v6, :cond_2

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    sget v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PAIRING:I

    if-ne v5, v6, :cond_2

    .line 158
    sget v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->AUTH_PASSTHRU:I

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    .line 160
    :cond_2
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x2

    aget-byte v5, v5, v6

    and-int/lit8 v5, v5, 0x8

    if-lez v5, :cond_3

    :goto_1
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->pairingEnabled:Z

    .line 161
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_AUTH:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 162
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Auth type discovered: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->supportedAuthType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto/16 :goto_0

    :cond_3
    move v3, v4

    .line 160
    goto :goto_1

    .line 169
    :cond_4
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    if-nez v3, :cond_6

    .line 171
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v5, 0x3

    aget-byte v2, v3, v5

    .line 172
    .local v2, "state":I
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_BUSY:I

    if-ne v3, v2, :cond_5

    .line 173
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->busyCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->busyCount:I

    .line 175
    :cond_5
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->busyCount:I

    const/16 v5, 0x28

    if-le v3, v5, :cond_6

    .line 177
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "No response. Client seems stuck in busy state. Ping."

    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->pingCommand:[B

    invoke-virtual {v3, v5}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 179
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->busyCount:I

    goto/16 :goto_0

    .line 184
    .end local v2    # "state":I
    :cond_6
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    if-nez v3, :cond_8

    .line 187
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_7

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_BUSY:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-eq v3, v5, :cond_7

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    if-nez v3, :cond_7

    .line 191
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    .line 195
    :cond_7
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_9

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_9

    .line 199
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 200
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    .line 238
    :cond_8
    :goto_2
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    sget v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_UNKNOWN:I

    if-ne v3, v5, :cond_0

    .line 240
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_0

    .line 245
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_d

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_d

    .line 248
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_TRANS:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 250
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto/16 :goto_0

    .line 202
    :cond_9
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_a

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_a

    .line 206
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 207
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto :goto_2

    .line 209
    :cond_a
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    const/16 v5, 0x2d

    if-le v3, v5, :cond_b

    .line 212
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto :goto_2

    .line 217
    :cond_b
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_8

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_AUTH:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_8

    .line 220
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    if-nez v3, :cond_8

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->msgRetries:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_8

    .line 223
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    array-length v3, v3

    if-ne v3, v8, :cond_c

    .line 225
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    .line 226
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    invoke-virtual {v3, v5}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto/16 :goto_2

    .line 228
    :cond_c
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    array-length v3, v3

    if-le v3, v8, :cond_8

    .line 230
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    .line 231
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCommand:[B

    invoke-virtual {v3, v5}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V

    goto/16 :goto_2

    .line 252
    :cond_d
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_0

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    if-ne v3, v5, :cond_0

    .line 255
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->BEACON_LINK:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->newBeaconState:I

    .line 257
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto/16 :goto_0

    .line 265
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 266
    .local v0, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 268
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 269
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    .line 272
    :cond_e
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 274
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getSequenceNumber()I

    move-result v3

    and-int/lit8 v3, v3, 0x4

    if-lez v3, :cond_0

    .line 276
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    if-lez v3, :cond_f

    .line 277
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->burstResponse:[B

    .line 279
    :cond_f
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->waitingForPairingResponse:Z

    .line 281
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->authCmdSent:Z

    .line 283
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 299
    .end local v0    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_10
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v5, :cond_11

    .line 301
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->transferInProgress:Z

    goto/16 :goto_0

    .line 307
    :cond_11
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostAuthChannelSubTask;->responseProcessed()V

    goto/16 :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 100
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method
