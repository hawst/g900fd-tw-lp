.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IpcDefines;
.super Ljava/lang/Object;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "IpcDefines"
.end annotation


# static fields
.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTBASICRESISTANCE:I = 0x4e21

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTCALIBRATION:I = 0x4e2d

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTCAPABILITIES:I = 0x4e2a

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETBASICRESISTANCE:I = 0x4e22

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETTARGETPOWER:I = 0x4e24

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETTRACKRESISTANCE:I = 0x4e29

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETUSERCONFIGURATION:I = 0x4e2c

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETWINDRESISTANCECOEFFICIENT:I = 0x4e26

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTSETWINDRESISTANCEPARAMETERS:I = 0x4e27

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTTARGETPOWER:I = 0x4e23

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTTRACKRESISTANCE:I = 0x4e28

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTUSERCONFIGURATION:I = 0x4e2b

.field public static final MSG_CMD_FITNESSEQUIPMENT_whatREQUESTWINDRESISTANCE:I = 0x4e25

.field public static final MSG_EVENT_FITNESSEQUIPMENT_BASICRESISTANCE_PARAM_decimalTOTALRESISTANCE:Ljava/lang/String; = "int_totalResistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_BIKEDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_BIKEDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERDISTANCE_PARAM_decimalCALCULATEDDISTANCE:Ljava/lang/String; = "decimal_calculatedDistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERDISTANCE_PARAM_intDATASOURCE:Ljava/lang/String; = "int_dataSource"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERPOWER_PARAM_decimalCALCULATEDPOWER:Ljava/lang/String; = "decimal_calculatedPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERPOWER_PARAM_intDATASOURCE:Ljava/lang/String; = "int_dataSource"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERSPEED_PARAM_decimalCALCULATEDSPEED:Ljava/lang/String; = "decimal_calculatedSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERSPEED_PARAM_intDATASOURCE:Ljava/lang/String; = "int_dataSource"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERTORQUE_PARAM_decimalCALCULATEDTORQUE:Ljava/lang/String; = "decimal_calculatedTorque"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CALCULATEDTRAINERTORQUE_PARAM_intDATASOURCE:Ljava/lang/String; = "int_dataSource"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CLIMBERDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CLIMBERDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_CLIMBERDATA_PARAM_longCUMULATIVESTRIDECYCLES:Ljava/lang/String; = "long_cumulativeStrideCycles"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ELLIPTICALDATA_PARAM_decimalCUMULATIVEPOSVERTDISTANCE:Ljava/lang/String; = "decimal_cumulativePosVertDistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ELLIPTICALDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ELLIPTICALDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ELLIPTICALDATA_PARAM_longCUMULATIVESTRIDES:Ljava/lang/String; = "long_cumulativeStrides"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_FITNESSEQUIPMENTSTATE_PARAM_intEQUIPMENTTYPECODE:Ljava/lang/String; = "int_equipmentTypeCode"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_FITNESSEQUIPMENTSTATE_PARAM_intSTATECODE:Ljava/lang/String; = "int_stateCode"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_boolVIRTUALINSTANTANEOUSSPEED:Ljava/lang/String; = "bool_virtualInstantaneousSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_decimalELAPSEDTIME:Ljava/lang/String; = "decimal_elapsedTime"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_decimalINSTANTANEOUSSPEED:Ljava/lang/String; = "decimal_instantaneousSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_intHEARTRATEDATASOURCECODE:Ljava/lang/String; = "int_heartRateDataSourceCode"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_intINSTANTANEOUSHEARTRATE:Ljava/lang/String; = "int_instantaneousHeartRate"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALFITNESSEQUIPMENTDATA_PARAM_longCUMULATIVEDISTANCE:Ljava/lang/String; = "long_cumulativeDistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALMETABOLICDATA_PARAM_decimalINSTANTANEOUSCALORICBURN:Ljava/lang/String; = "decimal_instantaneousCaloricBurn"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALMETABOLICDATA_PARAM_decimalINSTANTANEOUSMETABOLICEQUIVALENTS:Ljava/lang/String; = "decimal_instantaneousMetabolicEquivalents"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALMETABOLICDATA_PARAM_longCUMULATIVECALORIES:Ljava/lang/String; = "long_cumulativeCalories"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALSETTINGS_PARAM_decimalCYCLELENGTH:Ljava/lang/String; = "decimal_cycleLength"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALSETTINGS_PARAM_decimalINCLINEPERCENTAGE:Ljava/lang/String; = "decimal_inclinePercentage"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_GENERALSETTINGS_PARAM_intRESISTANCELEVEL:Ljava/lang/String; = "int_resistanceLevel"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_LAPOCCURED_PARAM_intLAPCOUNT:Ljava/lang/String; = "int_lapCount"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_NORDICSKIERDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_NORDICSKIERDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_NORDICSKIERDATA_PARAM_longCUMULATIVESTRIDES:Ljava/lang/String; = "long_cumulativeStrides"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERDATA_PARAM_longACCUMULATEDPOWER:Ljava/lang/String; = "long_accumulatedPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERDATA_PARAM_longUPDATEEVENTCOUNT:Ljava/lang/String; = "long_updateEventCount"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERTORQUEDATA_PARAM_decimalACCUMULATEDTORQUE:Ljava/lang/String; = "decimal_accumulatedTorque"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERTORQUEDATA_PARAM_decimalACCUMULATEDWHEELPERIOD:Ljava/lang/String; = "decimal_accumulatedWheelPeriod"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERTORQUEDATA_PARAM_longACCUMULATEDWHEELTICKS:Ljava/lang/String; = "long_accumulatedWheelTicks"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_RAWTRAINERTORQUEDATA_PARAM_longUPDATEEVENTCOUNT:Ljava/lang/String; = "long_updateEventCount"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTCALIBRATION_PARAM_boolSPINDOWNCALIBRATION:Ljava/lang/String; = "bool_spinDownCalibration"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTCALIBRATION_PARAM_boolZEROOFFSETCALIBRATION:Ljava/lang/String; = "bool_zeroOffsetCalibration"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTFINISHED_PARAM_intREQUESTSTATUS:Ljava/lang/String; = "int_requestStatus"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETBASICRESISTANCE_PARAM_decimalTOTALRESISTANCE:Ljava/lang/String; = "decimal_totalResistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETTARGETPOWER_PARAM_decimalTARGETPOWER:Ljava/lang/String; = "decimal_targetPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETTRACKRESISTANCE_PARAM_decimalGRADE:Ljava/lang/String; = "decimal_grade"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETTRACKRESISTANCE_PARAM_decimalROLLINGRESISTANCECOEFFICIENT:Ljava/lang/String; = "decimal_rollingResistanceCoefficient"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCECOEFFICIENT_PARAM_decimalDRAFTINGFACTOR:Ljava/lang/String; = "decimal_draftingFactor"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCECOEFFICIENT_PARAM_decimalWINDRESISTANCECOEFFICIENT:Ljava/lang/String; = "decimal_windResistanceCoefficient"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCECOEFFICIENT_PARAM_intWINDSPEED:Ljava/lang/String; = "int_windSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCEPARAMETERS_PARAM_decimalAIRDENSITY:Ljava/lang/String; = "decimal_airDensity"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCEPARAMETERS_PARAM_decimalDRAFTINGFACTOR:Ljava/lang/String; = "decimal_draftingFactor"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCEPARAMETERS_PARAM_decimalDRAGCOEFFICIENT:Ljava/lang/String; = "decimal_dragCoefficient"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCEPARAMETERS_PARAM_decimalFRONTALSURFACEAREA:Ljava/lang/String; = "decimal_frontalSurfaceArea"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_REQUESTSETWINDRESISTANCEPARAMETERS_PARAM_intWINDSPEED:Ljava/lang/String; = "int_windSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ROWERDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ROWERDATA_PARAM_intINSTANTANEOUSPOWER:Ljava/lang/String; = "int_instantaneousPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_ROWERDATA_PARAM_longCUMULATIVESTROKES:Ljava/lang/String; = "long_cumulativeStrokes"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TARGETPOWER_PARAM_decimalTARGETPOWER:Ljava/lang/String; = "decimal_targetPower"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TRACKRESISTANCE_PARAM_decimalGRADE:Ljava/lang/String; = "decimal_grade"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TRACKRESISTANCE_PARAM_decimalROLLINGRESISTANCECOEFFICIENT:Ljava/lang/String; = "decimal_rollingResistanceCoefficient"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TRAINERSTATUSFLAGS_PARAM_longTRAINERSTATUSFLAGS:Ljava/lang/String; = "long_trainerStatusFlags"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TREADMILLDATA_PARAM_decimalCUMULATIVENEGVERTDISTANCE:Ljava/lang/String; = "decimal_cumulativeNegVertDistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TREADMILLDATA_PARAM_decimalCUMULATIVEPOSVERTDISTANCE:Ljava/lang/String; = "decimal_cumulativePosVertDistance"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_TREADMILLDATA_PARAM_intINSTANTANEOUSCADENCE:Ljava/lang/String; = "int_instantaneousCadence"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_WINDRESISTANCE_PARAM_decimalDRAFTINGFACTOR:Ljava/lang/String; = "decimal_draftingFactor"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_WINDRESISTANCE_PARAM_decimalWINDRESISTANCECOEFFICIENT:Ljava/lang/String; = "decimal_windResistanceCoefficient"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_WINDRESISTANCE_PARAM_intWINDSPEED:Ljava/lang/String; = "int_windSpeed"

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatBASICRESISTANCE:I = 0xdc

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatBIKEDATA:I = 0xd0

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALCULATEDTRAINERDISTANCE:I = 0xd8

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALCULATEDTRAINERPOWER:I = 0xd9

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALCULATEDTRAINERSPEED:I = 0xd7

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALCULATEDTRAINERTORQUE:I = 0xda

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALIBRATIONINPROGRESS:I = 0xe3

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCALIBRATIONRESPONSE:I = 0xe2

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCAPABILITIES:I = 0xe0

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCLIMBERDATA:I = 0xd2

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatCOMMANDSTATUS:I = 0xdb

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatELLIPTICALDATA:I = 0xcf

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatFITNESSEQUIPMENTSTATE:I = 0xca

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatGENERALFITNESSEQUIPMENTDATA:I = 0xcb

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatGENERALMETABOLICDATA:I = 0xcd

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatGENERALSETTINGS:I = 0xcc

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatLAPOCCURED:I = 0xc9

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatNORDICSKIERDATA:I = 0xd3

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatRAWTRAINERDATA:I = 0xd4

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatRAWTRAINERTORQUEDATA:I = 0xd6

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatREQUESTFINISHED:I = 0xe4

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatROWERDATA:I = 0xd1

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatTARGETPOWER:I = 0xdd

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatTRACKRESISTANCE:I = 0xdf

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatTRAINERSTATUS:I = 0xd5

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatTREADMILLDATA:I = 0xce

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatUSERCONFIGURATION:I = 0xe1

.field public static final MSG_EVENT_FITNESSEQUIPMENT_whatWINDRESISTANCE:I = 0xde

.field public static final MSG_REQACC_PARAM_MODE_iFESTARTSESSION:I = 0x12c

.field public static final MSG_REQACC_PARAM_arrayParcelableFITFILES:Ljava/lang/String; = "arrayParcelable_fitFiles"

.field public static final MSG_REQACC_PARAM_parcelableSETTINGS:Ljava/lang/String; = "parcelable_settings"

.field public static final PATH_ANTPLUS_FITNESSEQUIPMENTPLUGIN_PKG:Ljava/lang/String; = "com.dsi.ant.plugins.antplus"

.field public static final PATH_ANTPLUS_FITNESSEQUIPMENTPLUGIN_SERVICE:Ljava/lang/String; = "com.dsi.ant.plugins.antplus.fitnessequipment.FitnessEquipmentService"


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IpcDefines;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
