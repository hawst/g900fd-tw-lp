.class public Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "EnvironmentDevice.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public isPeriodSet:Z

.field mEvent_TempData:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field page0:Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;

.field page1:Lcom/dsi/ant/plugins/antplus/environment/pages/P1_TemperatureData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->isPeriodSet:Z

    .line 33
    return-void
.end method


# virtual methods
.method public getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;-><init>(Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->page0:Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;

    .line 39
    new-instance v1, Lcom/dsi/ant/plugins/antplus/environment/pages/P1_TemperatureData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/environment/pages/P1_TemperatureData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->page1:Lcom/dsi/ant/plugins/antplus/environment/pages/P1_TemperatureData;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->page0:Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->page1:Lcom/dsi/ant/plugins/antplus/environment/pages/P1_TemperatureData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    return-object v0
.end method

.method public setPeriod(I)V
    .locals 4
    .param p1, "period_32768unitsPerSecond"    # I

    .prologue
    .line 53
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 62
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException trying to set period"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 60
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE trying to set period: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
