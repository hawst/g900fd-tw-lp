.class Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;
.super Landroid/os/Handler;
.source "AsyncScanController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConnectResultHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# instance fields
.field controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;"
        }
    .end annotation
.end field

.field resultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;"
        }
    .end annotation
.end field

.field retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;TT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 532
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler<TT;>;"
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    .local p2, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p3, "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 533
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->resultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    .line 534
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 535
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .line 536
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 541
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler<TT;>;"
    iget v1, p1, Landroid/os/Message;->what:I

    .line 542
    .local v1, "resultCode":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 543
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 545
    sparse-switch v1, :sswitch_data_0

    .line 558
    :try_start_0
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    move-result-object v0

    .line 559
    .local v0, "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    if-ne v0, v2, :cond_0

    .line 560
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RequestAccess failed: Unrecognized return code (need app lib upgrade): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getIntValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->reportScanFailure(I)V
    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$800(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;I)V

    .line 564
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->resultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    const/4 v4, 0x0

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->DEAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    invoke-interface {v2, v4, v0, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 565
    monitor-exit v3

    .end local v0    # "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    :goto_1
    return-void

    .line 548
    :sswitch_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    const/4 v4, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z
    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$602(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;Z)Z

    .line 549
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->resultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    invoke-virtual {v2, p1, v4, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->handleReqAccSuccess(Landroid/os/Message;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;)V

    .line 550
    monitor-exit v3

    goto :goto_1

    .line 567
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 554
    :sswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;->resultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    const/4 v4, 0x0

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->SEARCH_TIMEOUT:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->SEARCHING:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    invoke-interface {v2, v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 555
    monitor-exit v3

    goto :goto_1

    .line 562
    .restart local v0    # "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RequestAccess failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 545
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method
