.class public Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;
.super Ljava/lang/Object;
.source "ToggleCounter.java"


# instance fields
.field private isInitialValue:Z

.field private isInitialized:Z

.field private previousValue:Z

.field private toggleCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleCount:I

    .line 13
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialized:Z

    .line 14
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialValue:Z

    .line 15
    return-void
.end method


# virtual methods
.method public getToggleCount()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleCount:I

    return v0
.end method

.method public reinitialize()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 51
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialized:Z

    .line 52
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialValue:Z

    .line 53
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleCount:I

    .line 58
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialized:Z

    .line 59
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialValue:Z

    .line 60
    return-void
.end method

.method public toggleChanged(Z)Z
    .locals 2
    .param p1, "newValue"    # Z

    .prologue
    const/4 v0, 0x0

    .line 19
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialValue:Z

    if-eqz v1, :cond_1

    .line 21
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->previousValue:Z

    .line 22
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialValue:Z

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 26
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialized:Z

    if-eqz v1, :cond_0

    .line 28
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->previousValue:Z

    if-eq p1, v1, :cond_0

    .line 30
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->previousValue:Z

    .line 31
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleCount:I

    .line 32
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public uninitialize()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->isInitialized:Z

    .line 47
    return-void
.end method
