.class public abstract Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
.super Ljava/lang/Object;
.source "PluginDownloadFilesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PluginDownloadController"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAntFsStateReceiver()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
.end method

.method public getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V
.end method

.method public abstract handleFinished(I)V
.end method

.method public handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 1
    .param p1, "afs"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public handlePreFileHandling(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 1
    .param p1, "afs"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public abstract handleStateChange(IJJ)V
.end method

.method public abstract includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z
.end method
