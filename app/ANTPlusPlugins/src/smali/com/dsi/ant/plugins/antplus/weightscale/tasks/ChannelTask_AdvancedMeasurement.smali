.class public Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_AdvancedMeasurement.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

.field bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private inChangingWeightState:Z

.field private isProfileMismatch:Z

.field isProfileSent:Z

.field measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

.field private measurementInProgress:Z

.field metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

.field private msgsSinceFirstPageRequest:I

.field profileDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

.field profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

.field reqAdvancedMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private responseSent:Z

.field scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

.field scaleUserProfileID:I

.field scaleUserProfileSet:Z

.field scaleUserProfileSupport:Z

.field seenBodyComp:Z

.field seenBodyMass:Z

.field seenCapabilities:Z

.field seenMetInfo:Z

.field seenWeight:Z

.field txBuffer:[B

.field userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)V
    .locals 3
    .param p1, "scaleDevice"    # Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "userProfile"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 40
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->responseSent:Z

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    .line 42
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->reqAdvancedMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 44
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    .line 45
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    .line 46
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    .line 47
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    .line 48
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    .line 51
    const v0, 0xffff

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileID:I

    .line 52
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    .line 53
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSet:Z

    .line 54
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenCapabilities:Z

    .line 55
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileSent:Z

    .line 56
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurementInProgress:Z

    .line 58
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    .line 59
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyComp:Z

    .line 60
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyMass:Z

    .line 61
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenMetInfo:Z

    .line 65
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    .line 67
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    .line 73
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    .line 74
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .line 75
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->reqAdvancedMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 76
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 382
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_0

    .line 384
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 517
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyWeight:Ljava/math/BigDecimal;

    .line 390
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyFatPercentage:Ljava/math/BigDecimal;

    .line 391
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->hydrationPercentage:Ljava/math/BigDecimal;

    .line 392
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->boneMass:Ljava/math/BigDecimal;

    .line 393
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->muscleMass:Ljava/math/BigDecimal;

    .line 394
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->basalMetabolicRate:Ljava/math/BigDecimal;

    .line 395
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->activeMetabolicRate:Ljava/math/BigDecimal;

    .line 396
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->resetComputedWeight()V

    .line 398
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 399
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->enableMessageProcessing()V

    .line 402
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 404
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenCapabilities:Z

    if-nez v1, :cond_1

    .line 406
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Timed out receiving capabilities"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntCommandFailedException in dowork(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 459
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 412
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_1
    :try_start_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v1, :cond_a

    .line 414
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    if-nez v1, :cond_9

    .line 415
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 418
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->setUserProfile(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)V

    .line 419
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->setDisplayUserProfileExchange(Z)V

    .line 420
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->setScaleAntFs(Z)V

    .line 421
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->setScaleUserProfileExchange(Z)V

    .line 422
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSet:Z

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->setScaleUserProfileSelected(Z)V

    .line 423
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->encodePage([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 426
    :try_start_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 440
    :goto_2
    :try_start_3
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 441
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 442
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V

    .line 445
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    if-eqz v1, :cond_2

    .line 448
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 449
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->enableMessageProcessing()V

    .line 450
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 451
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    .line 467
    :cond_2
    :goto_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyWeight:Ljava/math/BigDecimal;

    if-eqz v1, :cond_c

    .line 470
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyFatPercentage:Ljava/math/BigDecimal;

    if-nez v1, :cond_3

    .line 471
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyFatPercentage:Ljava/math/BigDecimal;

    .line 472
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->hydrationPercentage:Ljava/math/BigDecimal;

    if-nez v1, :cond_4

    .line 473
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->hydrationPercentage:Ljava/math/BigDecimal;

    .line 474
    :cond_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->activeMetabolicRate:Ljava/math/BigDecimal;

    if-nez v1, :cond_5

    .line 475
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->activeMetabolicRate:Ljava/math/BigDecimal;

    .line 476
    :cond_5
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->basalMetabolicRate:Ljava/math/BigDecimal;

    if-nez v1, :cond_6

    .line 477
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->basalMetabolicRate:Ljava/math/BigDecimal;

    .line 478
    :cond_6
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->muscleMass:Ljava/math/BigDecimal;

    if-nez v1, :cond_7

    .line 479
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->muscleMass:Ljava/math/BigDecimal;

    .line 480
    :cond_7
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->boneMass:Ljava/math/BigDecimal;

    if-nez v1, :cond_8

    .line 481
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->boneMass:Ljava/math/BigDecimal;

    .line 483
    :cond_8
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileMismatch:Z

    if-eqz v1, :cond_b

    .line 484
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_PROFILE_MISMATCH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 490
    :goto_4
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 491
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurementInProgress:Z

    .line 492
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->enableMessageProcessing()V

    .line 496
    :try_start_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 498
    :catch_1
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 417
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_9
    :try_start_5
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;
    :try_end_5
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_1

    .line 461
    :catch_2
    move-exception v0

    .line 463
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_3

    .line 435
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_a
    :try_start_6
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Scale does not support user profile exchange"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileSent:Z
    :try_end_6
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 486
    :cond_b
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto :goto_4

    .line 506
    :cond_c
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->inChangingWeightState:Z

    if-eqz v1, :cond_d

    .line 508
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Timed out waiting for measurement, weight value never stabilized"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_WEIGHT_VALUE_NOT_STABILIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto/16 :goto_0

    .line 513
    :cond_d
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Timed out waiting for measurement"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto/16 :goto_0

    .line 428
    :catch_3
    move-exception v1

    goto/16 :goto_2
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 522
    const-string v0, "Advanced Measurement Request"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 529
    return-void
.end method

.method public initTask()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileMismatch:Z

    .line 375
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 129
    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 133
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 136
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349
    :catch_0
    move-exception v6

    .line 351
    .local v6, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v0, v1, :cond_15

    .line 355
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    .end local v6    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V

    .line 143
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 146
    :pswitch_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileSent:Z

    .line 147
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->encodePage([B)V

    .line 148
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 157
    :pswitch_4
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v1, v1, 0x3

    rsub-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 168
    :pswitch_5
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenCapabilities:Z

    if-nez v0, :cond_1

    .line 170
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x1

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 171
    .local v7, "pageNumber":I
    if-ne v7, v2, :cond_0

    .line 173
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 174
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleUserProfileExchange()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    .line 175
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleUserProfileSelected()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSet:Z

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenCapabilities:Z

    .line 177
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 182
    .end local v7    # "pageNumber":I
    :cond_1
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileSent:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_3

    .line 184
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    .line 185
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    .line 187
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Tx retries exceeded sending user profile"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 192
    :cond_2
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->encodePage([B)V

    .line 195
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto/16 :goto_0

    .line 201
    :cond_3
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x1

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 202
    .restart local v7    # "pageNumber":I
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurementInProgress:Z

    if-eqz v0, :cond_6

    .line 204
    if-ne v7, v2, :cond_5

    .line 206
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 207
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getLastBroadcastWeight()Ljava/math/BigDecimal;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 210
    :cond_4
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Weight scale measurement complete. Now computing"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V

    .line 212
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 216
    :cond_5
    const/16 v0, 0x43

    if-ne v7, v0, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    if-nez v0, :cond_0

    .line 218
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "weight scale measurement complete.  Now beaconing"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V

    .line 220
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 226
    :cond_6
    if-ne v7, v2, :cond_8

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->inChangingWeightState:Z

    .line 229
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 230
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getLastBroadcastWeight()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_7

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->inChangingWeightState:Z

    .line 235
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyWeight:Ljava/math/BigDecimal;

    .line 236
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-nez v0, :cond_7

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    .line 239
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 331
    :cond_7
    :goto_1
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-nez v0, :cond_0

    .line 333
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    .line 334
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->msgsSinceFirstPageRequest:I

    if-ge v0, v8, :cond_0

    .line 336
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->profileEncoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;->encodePage([B)V

    .line 337
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->txBuffer:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto/16 :goto_0

    .line 243
    :cond_8
    const/4 v0, 0x2

    if-ne v7, v0, :cond_c

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-eqz v0, :cond_c

    .line 245
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 246
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->getUserProfileID()I

    move-result v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v1

    if-ne v0, v1, :cond_b

    .line 248
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->getHydrationPercentage()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_9

    .line 249
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->getHydrationPercentage()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->hydrationPercentage:Ljava/math/BigDecimal;

    .line 250
    :cond_9
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->getBodyFatPercentage()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_a

    .line 251
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyCompDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P2_BodyCompositionPercentage;->getBodyFatPercentage()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyFatPercentage:Ljava/math/BigDecimal;

    .line 252
    :cond_a
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->hydrationPercentage:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->bodyFatPercentage:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyComp:Z

    if-nez v0, :cond_7

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyComp:Z

    .line 255
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_1

    .line 260
    :cond_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileMismatch:Z

    .line 261
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyComp:Z

    if-nez v0, :cond_7

    .line 263
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Advanced data received with user ID mismatch - ignore data"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyComp:Z

    .line 265
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 271
    :cond_c
    if-ne v7, v3, :cond_10

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-eqz v0, :cond_10

    .line 273
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 274
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->getUserProfileID()I

    move-result v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v1

    if-ne v0, v1, :cond_f

    .line 276
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->getActiveMetabolicRate()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_d

    .line 277
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->getActiveMetabolicRate()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->activeMetabolicRate:Ljava/math/BigDecimal;

    .line 278
    :cond_d
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->getBasalMetabolicRate()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_e

    .line 279
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->metInfoDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P3_MetabolicInformation;->getBasalMetabolicRate()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->basalMetabolicRate:Ljava/math/BigDecimal;

    .line 280
    :cond_e
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->activeMetabolicRate:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->basalMetabolicRate:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenMetInfo:Z

    if-nez v0, :cond_7

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenMetInfo:Z

    .line 283
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 288
    :cond_f
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileMismatch:Z

    .line 289
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenMetInfo:Z

    if-nez v0, :cond_7

    .line 291
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Advanced data received with user ID mismatch - ignore data"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenMetInfo:Z

    .line 293
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 298
    :cond_10
    if-ne v7, v8, :cond_14

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-eqz v0, :cond_14

    .line 300
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 301
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->getUserProfileID()I

    move-result v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->userProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->getUserProfileID()I

    move-result v1

    if-ne v0, v1, :cond_13

    .line 303
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->getMuscleMass()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_11

    .line 304
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->getMuscleMass()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->muscleMass:Ljava/math/BigDecimal;

    .line 305
    :cond_11
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->getBoneMass()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_12

    .line 306
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->bodyMassDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->getBoneMass()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->boneMass:Ljava/math/BigDecimal;

    .line 307
    :cond_12
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->muscleMass:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;->boneMass:Ljava/math/BigDecimal;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyMass:Z

    if-nez v0, :cond_7

    .line 309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyMass:Z

    .line 310
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 315
    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->isProfileMismatch:Z

    .line 316
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyMass:Z

    if-nez v0, :cond_7

    .line 318
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Advanced data received with user ID mismatch - ignore data"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenBodyMass:Z

    .line 320
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 324
    :cond_14
    const/16 v0, 0xf1

    if-ne v7, v0, :cond_7

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->seenWeight:Z

    if-eqz v0, :cond_7

    .line 328
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 359
    .end local v7    # "pageNumber":I
    .restart local v6    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_15
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACFE handling message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 361
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->disableMessageProcessing()V

    .line 362
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 133
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public processRequest()V
    .locals 4

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 84
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 91
    :goto_0
    if-nez v1, :cond_0

    .line 93
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 101
    :goto_1
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 98
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->setCurrentState(I)V

    goto :goto_1
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    .prologue
    .line 105
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->reqAdvancedMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v3

    .line 107
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->responseSent:Z

    if-eqz v2, :cond_0

    .line 108
    monitor-exit v3

    .line 121
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getIntValue()I

    move-result v1

    .line 112
    .local v1, "statusCode":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 113
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    if-ne p1, v2, :cond_1

    .line 116
    const-string v2, "parcelable_AdvancedMeasurement"

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->measurement:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$AdvancedMeasurement;

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 118
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->reqAdvancedMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 119
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->responseSent:Z

    .line 120
    monitor-exit v3

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "statusCode":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
