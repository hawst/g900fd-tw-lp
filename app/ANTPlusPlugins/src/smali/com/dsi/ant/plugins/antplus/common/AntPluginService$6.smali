.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 935
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;ILcom/dsi/ant/message/ChannelId;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    .line 935
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->handleSearchResult(ILcom/dsi/ant/message/ChannelId;)V

    return-void
.end method

.method private handleSearchResult(ILcom/dsi/ant/message/ChannelId;)V
    .locals 9
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x4

    .line 951
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Async connect rcv result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 953
    .local v3, "response":Landroid/os/Message;
    sparse-switch p1, :sswitch_data_0

    .line 991
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Plugin async scan connect failed internally with search result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    iput v7, v3, Landroid/os/Message;->what:I

    .line 993
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 994
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    .line 997
    :goto_0
    return-void

    .line 956
    :sswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 957
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_1

    .line 959
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin async scan connect failed to shutdown executor cleanly"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    iput v7, v3, Landroid/os/Message;->what:I

    .line 961
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 979
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 965
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    invoke-virtual {v4, v5, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    .line 966
    .local v2, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v4, v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v1

    .line 967
    .local v1, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-nez v1, :cond_2

    .line 969
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin async scan connect failed internally: Device instantiation failed."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    iput v7, v3, Landroid/os/Message;->what:I

    .line 971
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_1

    .line 975
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v1, v6, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 976
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_1

    .line 984
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v1    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v2    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :sswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$asyncScanInfo:Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    .line 985
    const/4 v4, -0x7

    iput v4, v3, Landroid/os/Message;->what:I

    .line 986
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 953
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 941
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccHandler:Landroid/os/Handler;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6$1;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;ILcom/dsi/ant/message/ChannelId;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 947
    return-void
.end method
