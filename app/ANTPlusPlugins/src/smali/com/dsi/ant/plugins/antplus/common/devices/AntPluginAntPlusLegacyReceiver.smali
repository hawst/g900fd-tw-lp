.class public abstract Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "AntPluginAntPlusLegacyReceiver.java"


# static fields
.field private static final TOGGLE_MASK:I = 0x80


# instance fields
.field private defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

.field private isToggleSeen:Z

.field private lastToggle:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->isToggleSeen:Z

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->lastToggle:Ljava/lang/Boolean;

    .line 31
    return-void
.end method


# virtual methods
.method public abstract addNonLegacyPages(Ljava/util/List;Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ")V"
        }
    .end annotation
.end method

.method public abstract getDefaultPage()Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getEventSet()Ljava/util/Set;

    move-result-object v0

    .line 54
    .local v0, "eventSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 55
    return-object v0
.end method

.method public getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, "pages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->getDefaultPage()Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    .line 40
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P1_CumulativeOperatingTime;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P1_CumulativeOperatingTime;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P2_ManufacturerId;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P2_ManufacturerId;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P3_ProductId;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/legacypages/P3_ProductId;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->addNonLegacyPages(Ljava/util/List;Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;)V

    .line 47
    return-object v0
.end method

.method public handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v0, 0x1

    .line 62
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->isToggleSeen:Z

    if-nez v1, :cond_0

    .line 64
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0x80

    if-lez v1, :cond_2

    move v6, v0

    .line 67
    .local v6, "curToggle":Z
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->lastToggle:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 69
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->lastToggle:Ljava/lang/Boolean;

    .line 78
    .end local v6    # "curToggle":Z
    :cond_0
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->isToggleSeen:Z

    if-eqz v1, :cond_1

    .line 81
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, -0x81

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 85
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v0, v1, v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 88
    .local v7, "pageNum":I
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 93
    .end local v7    # "pageNum":I
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->defaultPage:Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->getEstimatedTimestamp()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->getEventFlags()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 94
    return-void

    .line 64
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 71
    .restart local v6    # "curToggle":Z
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->lastToggle:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v6, v1, :cond_0

    .line 73
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;->isToggleSeen:Z

    goto :goto_1
.end method
