.class public Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;
.super Ljava/lang/Object;
.source "ChannelTask_SendDataPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AntPlusBikePowerDataPages"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAutoZeroConfigDataPage(Z)[B
    .locals 5
    .param p0, "b"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 79
    if-eqz p0, :cond_0

    move v1, v2

    :goto_0
    int-to-byte v0, v1

    .line 80
    .local v0, "autoZeroConfig":B
    const/16 v1, 0x8

    new-array v1, v1, [B

    aput-byte v2, v1, v3

    const/16 v3, -0x55

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    aput-byte v0, v1, v2

    const/4 v2, 0x3

    aput-byte v4, v1, v2

    const/4 v2, 0x4

    aput-byte v4, v1, v2

    const/4 v2, 0x5

    aput-byte v4, v1, v2

    const/4 v2, 0x6

    aput-byte v4, v1, v2

    const/4 v2, 0x7

    aput-byte v4, v1, v2

    return-object v1

    .end local v0    # "autoZeroConfig":B
    :cond_0
    move v1, v3

    .line 79
    goto :goto_0
.end method

.method public static getManualCalibrationRequestDataPage()[B
    .locals 1

    .prologue
    .line 74
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 1
        0x1t
        -0x56t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method public static getRequestCustomCalParamsDataPage([B)[B
    .locals 4
    .param p0, "manufacturerSpecificBytes"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 85
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 86
    .local v0, "messagePayload":[B
    aput-byte v2, v0, v3

    .line 87
    const/16 v1, -0x46

    aput-byte v1, v0, v2

    .line 88
    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {p0, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    return-object v0
.end method

.method public static getSaveSlopeDataPage(Ljava/math/BigDecimal;)[B
    .locals 4
    .param p0, "slope"    # Ljava/math/BigDecimal;

    .prologue
    .line 103
    const/16 v2, 0x8

    new-array v1, v2, [B

    fill-array-data v1, :array_0

    .line 104
    .local v1, "messagePayload":[B
    new-instance v2, Ljava/math/BigDecimal;

    const-string v3, "10"

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    .line 105
    .local v0, "intSlope":I
    const/4 v2, 0x6

    invoke-static {v1, v2, v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2BeBytes([BII)V

    .line 106
    return-object v1

    .line 103
    nop

    :array_0
    .array-data 1
        0x1t
        0x10t
        0x2t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method public static getSetCrankParamsDataPage(B)[B
    .locals 5
    .param p0, "crankLengthValue"    # B

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 111
    const/16 v0, 0x8

    new-array v0, v0, [B

    aput-byte v4, v0, v3

    aput-byte v1, v0, v1

    aput-byte v2, v0, v4

    const/4 v1, 0x3

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    aput-byte p0, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x6

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    aput-byte v2, v0, v1

    return-object v0
.end method

.method public static getSetCustomCalParamDataPage([B)[B
    .locals 4
    .param p0, "manufacturerSpecificBytes"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 94
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 95
    .local v0, "messagePayload":[B
    aput-byte v2, v0, v3

    .line 96
    const/16 v1, -0x44

    aput-byte v1, v0, v2

    .line 97
    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {p0, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    return-object v0
.end method
