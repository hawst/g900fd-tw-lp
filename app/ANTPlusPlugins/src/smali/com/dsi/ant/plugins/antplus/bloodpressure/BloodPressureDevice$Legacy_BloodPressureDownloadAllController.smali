.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;
.source "BloodPressureDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Legacy_BloodPressureDownloadAllController"
.end annotation


# instance fields
.field private client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 1
    .param p2, "useProgressUpdates"    # Z
    .param p3, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    .line 216
    const/16 v0, 0xcb

    invoke-direct {p0, v0, p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;-><init>(IZ)V

    .line 217
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 218
    return-void
.end method


# virtual methods
.method public includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z
    .locals 2
    .param p1, "fileEntry"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .prologue
    .line 232
    iget v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->FIT_DATA_TYPE:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 235
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected sendMessageToClient(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;->this$0:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice$Legacy_BloodPressureDownloadAllController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 224
    return-void
.end method
