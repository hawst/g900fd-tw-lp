.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
.super Ljava/lang/Enum;
.source "AntPlusHeartRatePcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

.field public static final enum INITIAL_VALUE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

.field public static final enum LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

.field public static final enum ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 135
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    const-string v1, "LIVE_DATA"

    invoke-direct {v0, v1, v6, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 143
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    const-string v1, "INITIAL_VALUE"

    invoke-direct {v0, v1, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->INITIAL_VALUE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 150
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    const-string v1, "ZERO_DETECTED"

    invoke-direct {v0, v1, v4, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 155
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 130
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->INITIAL_VALUE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->intValue:I

    .line 162
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 180
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 182
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 188
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :goto_1
    return-object v3

    .line 180
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 187
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->intValue:I

    move-object v3, v4

    .line 188
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 130
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->intValue:I

    return v0
.end method
