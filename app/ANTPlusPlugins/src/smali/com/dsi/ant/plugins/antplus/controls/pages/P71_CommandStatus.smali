.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P71_CommandStatus.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# instance fields
.field private commandEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public lastCommandId:I

.field public lastCommandPage:I

.field public lastSequenceNumber:I

.field public status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v0, 0xff

    .line 18
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 20
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandPage:I

    .line 21
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastSequenceNumber:I

    .line 22
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->UNINITIALIZED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    .line 23
    const v0, 0xffff

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0x4e21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->commandEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 83
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->commandEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 90
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->commandEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public encodePage([B)V
    .locals 3
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v2, -0x1

    .line 57
    const/4 v0, 0x0

    const/16 v1, 0x47

    aput-byte v1, p1, v0

    .line 58
    const/4 v0, 0x1

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandPage:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 59
    const/4 v0, 0x2

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastSequenceNumber:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 60
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->getIntValue()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 61
    const/4 v0, 0x4

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 62
    const/4 v0, 0x5

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 63
    const/4 v0, 0x6

    aput-byte v2, p1, v0

    .line 64
    const/4 v0, 0x7

    aput-byte v2, p1, v0

    .line 65
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->commandEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateFromAudioCommand(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 30
    const/16 v0, 0x10

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandPage:I

    .line 31
    const-string v0, "int_sequenceNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastSequenceNumber:I

    .line 32
    const-string v0, "int_commandStatus"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    .line 33
    const-string v0, "int_commandNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    .line 34
    return-void
.end method

.method public updateFromGenericCommand(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const/16 v0, 0x49

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandPage:I

    .line 49
    const-string v0, "int_sequenceNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastSequenceNumber:I

    .line 50
    const-string v0, "int_commandStatus"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    .line 51
    const-string v0, "int_commandNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    .line 52
    return-void
.end method

.method public updateFromVideoCommand(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 38
    const/16 v0, 0x10

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandPage:I

    .line 39
    const-string v0, "int_sequenceNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastSequenceNumber:I

    .line 40
    const-string v0, "int_commandStatus"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->status:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    .line 41
    const-string v0, "int_commandNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    .line 43
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    add-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->lastCommandId:I

    .line 44
    return-void
.end method
