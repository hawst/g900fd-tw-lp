.class public final enum Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
.super Ljava/lang/Enum;
.source "BikeSpdCadComboDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

.field public static final enum BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

.field public static final enum CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

.field public static final enum SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    const-string v1, "CADENCE_ONLY"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 34
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    const-string v1, "SPEED_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 35
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->$VALUES:[Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->$VALUES:[Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    return-object v0
.end method
