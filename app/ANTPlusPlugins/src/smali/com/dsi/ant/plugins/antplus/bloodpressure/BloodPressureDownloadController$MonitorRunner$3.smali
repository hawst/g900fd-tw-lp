.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->doDownloadLoop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

.field final synthetic val$cachedCurrentCumulativeBytes:J

.field final synthetic val$totalBytesToDownload:J


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;JJ)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->val$cachedCurrentCumulativeBytes:J

    iput-wide p4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->val$totalBytesToDownload:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransferUpdate(JJ)V
    .locals 8
    .param p1, "transferredBytes"    # J
    .param p3, "totalBytes"    # J

    .prologue
    .line 494
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->downloadRoundWatchers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 496
    .local v6, "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    const/16 v1, 0x352

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->val$cachedCurrentCumulativeBytes:J

    add-long/2addr v2, p1

    iget-wide v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$3;->val$totalBytesToDownload:J

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->handleStateChange(IJJ)V

    goto :goto_0

    .line 499
    .end local v6    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    :cond_0
    return-void
.end method
