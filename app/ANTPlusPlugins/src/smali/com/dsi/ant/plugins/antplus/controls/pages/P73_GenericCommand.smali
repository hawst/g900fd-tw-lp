.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P73_GenericCommand.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# static fields
.field private static final mManufacturerId:I = 0xf


# instance fields
.field private cmdEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

.field public mRemoteSerialNumber:I

.field public mSequenceNumber:B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->cmdEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 47
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->cmdEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    :goto_0
    return-void

    .line 50
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 52
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 53
    const-string v1, "int_serialNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v1, "int_manufacturerID"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    const-string v1, "int_sequenceNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    const-string v1, "int_commandNumber"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->cmdEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public encodePage([B)V
    .locals 4
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 69
    const/4 v0, 0x1

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mRemoteSerialNumber:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 70
    const/4 v0, 0x2

    const v1, 0xff00

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mRemoteSerialNumber:I

    and-int/2addr v1, v2

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 71
    const/4 v0, 0x3

    const/16 v1, 0xf

    aput-byte v1, p1, v0

    .line 72
    const/4 v0, 0x4

    aput-byte v3, p1, v0

    .line 73
    const/4 v0, 0x5

    iget-byte v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mSequenceNumber:B

    aput-byte v1, p1, v0

    .line 74
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;->getLowerByte()B

    move-result v1

    aput-byte v1, p1, v0

    .line 75
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;->getUpperByte()B

    move-result v1

    aput-byte v1, p1, v0

    .line 76
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->cmdEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
