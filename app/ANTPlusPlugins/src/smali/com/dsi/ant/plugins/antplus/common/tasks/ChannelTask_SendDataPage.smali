.class public Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_SendDataPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$1;,
        Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;,
        Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;,
        Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static TRANSFER_ATTEMPT_LIMIT:I


# instance fields
.field private MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

.field private executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field private finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private isDataPageRequest:Z

.field private isMessageSent:Z

.field private isTransferInProgress:Z

.field private messagePayload:[B

.field private msgsFailedSincePayloadSent:I

.field private final requestStatusIpc:Ljava/lang/String;

.field private responseSent:Z

.field private senderDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

.field private transferAttempts:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    .line 249
    const/16 v0, 0xc

    sput v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TRANSFER_ATTEMPT_LIMIT:I

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;ILjava/lang/String;)V
    .locals 3
    .param p1, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "antPluginAntPlusSender"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;
    .param p3, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p4, "eventIpc"    # I
    .param p5, "requestStatusIpc"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 254
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 248
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    .line 255
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->senderDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

    .line 256
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 257
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 258
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->requestStatusIpc:Ljava/lang/String;

    .line 259
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    .line 260
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    .line 261
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    .line 262
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    .line 263
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    .line 264
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->responseSent:Z

    .line 265
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 266
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
    .locals 5
    .param p1, "req"    # Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    .param p2, "antPluginAntPlusSender"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;
    .param p3, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .prologue
    const/4 v4, 0x0

    .line 269
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 248
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    .line 270
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->senderDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

    .line 271
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 272
    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqEvtIpc:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 273
    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->requestStatusIpc:Ljava/lang/String;

    .line 274
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    .line 275
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    .line 276
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    .line 277
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    .line 278
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    .line 279
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->responseSent:Z

    .line 281
    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->clientList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 282
    .local v1, "requestor":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v3, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v4, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    goto :goto_0

    .line 283
    .end local v1    # "requestor":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    :cond_0
    return-void
.end method

.method private startTransfer()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 390
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    if-eqz v1, :cond_0

    .line 448
    :goto_0
    return-void

    .line 393
    :cond_0
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    sget v2, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TRANSFER_ATTEMPT_LIMIT:I

    if-lt v1, v2, :cond_1

    .line 395
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to send data page after "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TRANSFER_ATTEMPT_LIMIT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " requests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 397
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 402
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    sget v2, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TRANSFER_ATTEMPT_LIMIT:I

    div-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x1

    if-ne v1, v2, :cond_2

    .line 404
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v2, 0x4

    aput-byte v2, v1, v4

    .line 405
    const/4 v1, 0x3

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    .line 409
    :cond_2
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    sget v2, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TRANSFER_ATTEMPT_LIMIT:I

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x1

    if-ne v1, v2, :cond_3

    .line 411
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    aput-byte v5, v1, v4

    .line 412
    const/4 v1, 0x7

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    .line 415
    :cond_3
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->transferAttempts:I

    .line 416
    iput v3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    .line 417
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    .line 418
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    .line 422
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    array-length v1, v1

    if-le v1, v5, :cond_4

    .line 423
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->burstTransfer([B)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_5

    .line 433
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    const-string v2, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 435
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_6

    .line 438
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    goto/16 :goto_0

    .line 442
    :cond_6
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE handling message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const/16 v1, -0x28

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 444
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 445
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v5, -0x28

    .line 461
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_0

    .line 463
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const/16 v1, -0x29

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 498
    :goto_0
    return-void

    .line 468
    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 469
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->enableMessageProcessing()V

    .line 471
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 475
    :try_start_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-nez v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x1e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 494
    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-eqz v1, :cond_2

    .line 495
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const/16 v1, -0x28

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 482
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 486
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 489
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntCommandFailedException in dowork(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 491
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 497
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    const-string v0, "Data Page Request"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 548
    const/16 v0, -0x28

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 549
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 454
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 5
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 290
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->senderDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 292
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 296
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 299
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 306
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 309
    :pswitch_3
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    .line 310
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    .line 312
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-nez v0, :cond_1

    .line 314
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 315
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 317
    :cond_1
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    if-le v0, v1, :cond_0

    .line 320
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto :goto_0

    .line 324
    :pswitch_4
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isTransferInProgress:Z

    .line 325
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto :goto_0

    .line 330
    :pswitch_5
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-eqz v0, :cond_2

    .line 332
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    .line 334
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    if-le v0, v1, :cond_0

    .line 337
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto :goto_0

    .line 340
    :cond_2
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-nez v0, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto :goto_0

    .line 344
    :pswitch_6
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-eqz v0, :cond_0

    .line 345
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    goto :goto_0

    .line 357
    :pswitch_7
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    if-eqz v0, :cond_6

    .line 360
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v3

    if-ne v0, v1, :cond_4

    .line 362
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    aget-byte v0, v0, v4

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    aget-byte v0, v0, v4

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_0

    .line 364
    :cond_3
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    .line 365
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 366
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 369
    :cond_4
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-eqz v0, :cond_0

    .line 371
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->msgsFailedSincePayloadSent:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->MSGS_FAILED_SINCE_TRANSFER_LIMIT:I

    if-gt v0, v1, :cond_5

    .line 373
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->disableMessageProcessing()V

    .line 374
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 377
    :cond_5
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto/16 :goto_0

    .line 379
    :cond_6
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isMessageSent:Z

    if-nez v0, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->startTransfer()V

    goto/16 :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 296
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public processRequest([B)V
    .locals 6
    .param p1, "incomingMessagePayload"    # [B

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 508
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    .line 511
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    array-length v2, v2

    rem-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    .line 513
    const/16 v2, -0x32

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    .line 542
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    const/16 v3, 0x46

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    aget-byte v2, v2, v5

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->messagePayload:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    if-ne v2, v5, :cond_1

    .line 519
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->isDataPageRequest:Z

    .line 522
    :cond_1
    const/4 v1, 0x0

    .line 525
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x2710

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 532
    :goto_1
    if-nez v1, :cond_2

    .line 534
    const/16 v2, -0x14

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->sendResponse(I)V

    goto :goto_0

    .line 526
    :catch_0
    move-exception v0

    .line 529
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 539
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->senderDevice:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->setCurrentState(I)V

    goto :goto_0
.end method

.method public sendResponse(I)V
    .locals 3
    .param p1, "statusCode"    # I

    .prologue
    .line 553
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v2

    .line 555
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->responseSent:Z

    if-eqz v1, :cond_0

    .line 556
    monitor-exit v2

    .line 564
    :goto_0
    return-void

    .line 558
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 559
    .local v0, "b":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->requestStatusIpc:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 561
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->finishedEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 562
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->responseSent:Z

    .line 563
    monitor-exit v2

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
