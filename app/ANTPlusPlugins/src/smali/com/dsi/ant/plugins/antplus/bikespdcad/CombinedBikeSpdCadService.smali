.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "CombinedBikeSpdCadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$2;,
        Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;,
        Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;,
        Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;,
        Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isLastAccessRequestCadencePcc:Z

.field private isReqForComboDeviceOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    .line 688
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mPccHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected addExisitingDevicesToSearchParams(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "searchParams"    # Landroid/os/Bundle;
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v0, "connectedDevicesCanConnectToIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v1, "connectedDevicesCanConnectToNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v5

    .line 172
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 173
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 175
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 178
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    iget-object v4, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 182
    instance-of v4, v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    if-eqz v4, :cond_1

    .line 184
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/high16 v6, 0x20000000

    add-int/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    :goto_1
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 186
    .restart local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_1
    :try_start_1
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    if-eqz v4, :cond_2

    instance-of v4, v2, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;

    if-nez v4, :cond_3

    :cond_2
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    if-nez v4, :cond_0

    instance-of v4, v2, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;

    if-eqz v4, :cond_0

    .line 189
    :cond_3
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 198
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :cond_4
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    const-string v4, "intarl_AvailableConnectedDevices"

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 201
    const-string v4, "intarl_AvailableConnectedDevNames"

    invoke-virtual {p1, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 202
    return-void
.end method

.method protected composeActivitySearchParams(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 148
    invoke-super {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->composeActivitySearchParams(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 149
    .local v1, "b":Landroid/os/Bundle;
    if-nez v1, :cond_1

    move-object v1, v2

    .line 159
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-object v1

    .line 153
    .restart local v1    # "b":Landroid/os/Bundle;
    :cond_1
    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {p0, v3, v2, v2, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 154
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v0, :cond_0

    .line 156
    const-string v2, "antchannel_AlternativeSearchChannel"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Bundle;)V
    .locals 23
    .param p1, "asyncScanInfo"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 955
    const-string v5, "parcelable_AsyncScanResultDeviceInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v19

    check-cast v19, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    .line 956
    .local v19, "resultToConnect":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    const-string v5, "msgr_ReqAccResultReceiver"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/Messenger;

    .local v17, "msgr_ResultMessenger":Landroid/os/Messenger;
    move-object/from16 v5, p1

    .line 958
    check-cast v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->getResultByScanInfo(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;

    move-result-object v12

    .line 959
    .local v12, "bscResultToConnect":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    if-nez v12, :cond_1

    .line 961
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v10, "Couldn\'t match requested async result with result list"

    invoke-static {v5, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v18

    .line 963
    .local v18, "response":Landroid/os/Message;
    const/4 v5, -0x4

    move-object/from16 v0, v18

    iput v5, v0, Landroid/os/Message;->what:I

    .line 964
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1096
    .end local v18    # "response":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 969
    :cond_1
    iget-boolean v5, v12, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->isSpdAndCadComboSensor:Z

    if-eqz v5, :cond_2

    const/high16 v16, 0x20000000

    .line 970
    .local v16, "flagToAdd":I
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->getAntDeviceNumber()I

    move-result v5

    add-int v5, v5, v16

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v13

    .line 971
    .local v13, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v13, :cond_4

    .line 973
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v13, v5}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 975
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v18

    .line 976
    .restart local v18    # "response":Landroid/os/Message;
    const/4 v5, -0x8

    move-object/from16 v0, v18

    iput v5, v0, Landroid/os/Message;->what:I

    .line 977
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 969
    .end local v13    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v16    # "flagToAdd":I
    .end local v18    # "response":Landroid/os/Message;
    :cond_2
    const/16 v16, 0x0

    goto :goto_1

    .line 980
    .restart local v13    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .restart local v16    # "flagToAdd":I
    :cond_3
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v13, v1, v10}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    .line 982
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 986
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v21

    .line 987
    .local v21, "searchParams":Landroid/os/Bundle;
    const-string v5, "int_DevType"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 988
    .local v8, "devType":I
    const-string v5, "int_TransType"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 989
    .local v9, "transType":I
    const-string v5, "int_Period"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 990
    .local v7, "period":I
    const-string v5, "int_RfFreq"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 992
    .local v6, "rfFreq":I
    iget-boolean v5, v12, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->isSpdAndCadComboSensor:Z

    if-eqz v5, :cond_5

    .line 994
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v8

    .line 995
    const/16 v7, 0x1f96

    :cond_5
    move-object/from16 v5, p1

    .line 998
    check-cast v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    iget-object v0, v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v20, v0

    .line 999
    .local v20, "scanControl":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    if-eqz v5, :cond_6

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v5, :cond_6

    .line 1000
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 1001
    :cond_6
    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    invoke-virtual/range {v19 .. v19}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->getAntDeviceNumber()I

    move-result v5

    const/4 v10, 0x0

    new-instance v11, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    move-object/from16 v3, p1

    invoke-direct {v11, v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$1;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;)V

    invoke-direct/range {v4 .. v11}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 1075
    .local v4, "connectTask":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    :try_start_0
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v10, 0xbb8

    invoke-virtual {v5, v4, v10}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v22

    .line 1076
    .local v22, "taskStarted":Z
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1083
    :goto_2
    if-nez v22, :cond_0

    .line 1085
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v15

    .line 1086
    .local v15, "failMsg":Landroid/os/Message;
    const/4 v5, -0x4

    iput v5, v15, Landroid/os/Message;->what:I

    .line 1089
    :try_start_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1094
    :goto_3
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 1077
    .end local v15    # "failMsg":Landroid/os/Message;
    .end local v22    # "taskStarted":Z
    :catch_0
    move-exception v14

    .line 1079
    .local v14, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v10, "Plugin async scan connect InterruptedException trying to start connect task"

    invoke-static {v5, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const/16 v22, 0x0

    .restart local v22    # "taskStarted":Z
    goto :goto_2

    .line 1090
    .end local v14    # "e":Ljava/lang/InterruptedException;
    .restart local v15    # "failMsg":Landroid/os/Message;
    :catch_1
    move-exception v14

    .line 1092
    .local v14, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v10, "Plugin async scan connect failed to start connect task"

    invoke-static {v5, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 6
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    const/4 v3, 0x0

    .line 111
    :try_start_0
    invoke-virtual {p1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 114
    .local v1, "idMsg":Lcom/dsi/ant/message/ChannelId;
    :try_start_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$DeviceType:[I

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v4

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 123
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v4, "Device instantiation failed: Unknown device type returned from search"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 140
    .end local v1    # "idMsg":Lcom/dsi/ant/message/ChannelId;
    :goto_0
    return-object v2

    .line 117
    .restart local v1    # "idMsg":Lcom/dsi/ant/message/ChannelId;
    :pswitch_0
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    invoke-direct {v2, p2, p1, p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    :try_start_2
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v4, "Device instantiation failed: Constructor threw ClosedChannelException"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v2, v3

    .line 130
    goto :goto_0

    .line 119
    .end local v0    # "e":Ljava/nio/channels/ClosedChannelException;
    :pswitch_1
    :try_start_3
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;

    invoke-direct {v2, p2, p1}, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    :try_end_3
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 132
    .end local v1    # "idMsg":Lcom/dsi/ant/message/ChannelId;
    :catch_1
    move-exception v0

    .line 134
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v4, "Device instantiation failed: RemoteException on channel"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 135
    goto :goto_0

    .line 121
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "idMsg":Lcom/dsi/ant/message/ChannelId;
    :pswitch_2
    :try_start_4
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;

    invoke-direct {v2, p2, p1}, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    :try_end_4
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 136
    .end local v1    # "idMsg":Lcom/dsi/ant/message/ChannelId;
    :catch_2
    move-exception v0

    .line 139
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device instantiation failed: ACFE requesting channel ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 140
    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 5
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "appNamePkg"    # Ljava/lang/String;

    .prologue
    .line 208
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isReqForComboDeviceOnly:Z

    if-nez v3, :cond_0

    const/high16 v3, 0x20000000

    and-int/2addr v3, p1

    if-lez v3, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 209
    .local v2, "isComboDevice":Z
    :goto_0
    const v3, -0x20000001

    and-int/2addr p1, v3

    .line 211
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v4

    .line 213
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 215
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v2, :cond_2

    instance-of v3, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    if-eqz v3, :cond_1

    .line 219
    :cond_2
    if-nez p1, :cond_4

    .line 221
    monitor-exit v4

    .line 237
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :goto_1
    return-object v0

    .line 208
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isComboDevice":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 223
    .restart local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "isComboDevice":Z
    :cond_4
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, p1, :cond_1

    if-nez v2, :cond_6

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    if-eqz v3, :cond_5

    instance-of v3, v0, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;

    if-nez v3, :cond_6

    :cond_5
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    if-nez v3, :cond_1

    instance-of v3, v0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;

    if-eqz v3, :cond_1

    .line 229
    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 232
    :cond_7
    monitor-exit v4

    goto :goto_1

    .line 235
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_8
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected getAlreadyConnectedDevices(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "query"    # Landroid/os/Bundle;

    .prologue
    .line 243
    const-string v1, "DevType_int"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 245
    .local v0, "queryDevtype":I
    if-nez v0, :cond_0

    .line 247
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {p0, p1, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevices(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V

    .line 248
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {p0, p1, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevices(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V

    .line 249
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {p0, p1, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevices(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V

    .line 252
    :cond_0
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevices(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V

    .line 253
    return-void
.end method

.method protected getAlreadyConnectedDevices(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V
    .locals 13
    .param p1, "query"    # Landroid/os/Bundle;
    .param p2, "deviceType"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .prologue
    .line 257
    const/4 v10, 0x0

    .line 258
    .local v10, "targetClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v11, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$DeviceType:[I

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 311
    :goto_0
    return-void

    .line 261
    :pswitch_0
    const-class v10, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;

    .line 273
    :goto_1
    const/4 v1, 0x0

    .line 274
    .local v1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    iget-object v12, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v12

    .line 276
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-direct {v2, v11}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 277
    .end local v1    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .local v2, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :try_start_1
    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 279
    .local v0, "dev":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    invoke-virtual {v10, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 281
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 284
    .end local v0    # "dev":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v5    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v11

    move-object v1, v2

    .end local v2    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .restart local v1    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :goto_3
    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v11

    .line 264
    .end local v1    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :pswitch_1
    const-class v10, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;

    .line 265
    goto :goto_1

    .line 267
    :pswitch_2
    const-class v10, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    .line 268
    goto :goto_1

    .line 284
    .restart local v2    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 286
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v6, v11, [Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 287
    .local v6, "infos":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v4, v11, :cond_2

    .line 289
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    iget-object v11, v11, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    aput-object v11, v6, v4

    .line 287
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 292
    :cond_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    .line 293
    .local v8, "retMsg":Landroid/os/Message;
    const/4 v11, 0x0

    iput v11, v8, Landroid/os/Message;->what:I

    .line 294
    const-string v11, "CmdSeqNum_int"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    iput v11, v8, Landroid/os/Message;->arg1:I

    .line 295
    const/4 v11, 0x0

    iput v11, v8, Landroid/os/Message;->arg2:I

    .line 296
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 297
    .local v7, "retData":Landroid/os/Bundle;
    invoke-virtual {v8, v7}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 298
    const-string v11, "DevType_int"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v12

    invoke-virtual {v7, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 301
    const-string v11, "DevDbInfoList_parcelableArray"

    invoke-virtual {v7, v11, v6}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 303
    const-string v11, "ResultMsgr_messenger"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Messenger;

    .line 306
    .local v9, "retMsgr":Landroid/os/Messenger;
    :try_start_4
    invoke-virtual {v9, v8}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 307
    :catch_0
    move-exception v3

    .line 309
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v11, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v12, "RemoteException attempting to send getAlreadyConnnectedDevice broadcast request"

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    .end local v2    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "infos":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v7    # "retData":Landroid/os/Bundle;
    .end local v8    # "retMsg":Landroid/os/Message;
    .end local v9    # "retMsgr":Landroid/os/Messenger;
    .restart local v1    # "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :catchall_1
    move-exception v11

    goto :goto_3

    .line 258
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/16 v5, 0x39

    const/4 v4, 0x0

    .line 73
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v1, "deviceParams":Landroid/os/Bundle;
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    if-eqz v2, :cond_0

    .line 76
    const-string v2, "str_PluginName"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "predefinednetwork_NetKey"

    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    const-string v2, "int_DevType"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v2, "int_Period"

    const/16 v3, 0x1fa6

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    :goto_0
    const-string v2, "int_TransType"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 91
    const-string v2, "int_RfFreq"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v0, "comboParams":Landroid/os/Bundle;
    const-string v2, "int_DevType"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    const-string v2, "int_Period"

    const/16 v3, 0x1f96

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v2, "int_TransType"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string v2, "int_RfFreq"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    const-string v2, "bundle_AlternativeSearchParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 103
    return-object v1

    .line 84
    .end local v0    # "comboParams":Landroid/os/Bundle;
    :cond_0
    const-string v2, "str_PluginName"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v2, "predefinednetwork_NetKey"

    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 86
    const-string v2, "int_DevType"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    const-string v2, "int_Period"

    const/16 v3, 0x1fb6

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected handleAsyncAntDevNumberSearchRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 318
    const-string v0, "bool_IsSpdCadCombinedSensor"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isReqForComboDeviceOnly:Z

    .line 320
    invoke-super {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAsyncAntDevNumberSearchRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    .line 321
    return-void
.end method

.method protected handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 40
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 800
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    move-object/from16 v0, p1

    iput-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 801
    new-instance v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 802
    .local v9, "si":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;
    move-object/from16 v0, p2

    iput-object v0, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    .line 803
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v5, v6, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v25

    .line 807
    .local v25, "deviceParams":Landroid/os/Bundle;
    const-string v5, "int_DevType"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 808
    .local v13, "devType":I
    const-string v5, "int_TransType"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 809
    .local v14, "transType":I
    const-string v5, "int_Period"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 810
    .local v12, "period":I
    const-string v5, "int_RfFreq"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 811
    .local v11, "rfFreq":I
    const-string v5, "predefinednetwork_NetKey"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v33

    check-cast v33, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 812
    .local v33, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    const-string v5, "int_ProximityBin"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 813
    .local v15, "proxThreshold":I
    const-string v5, "str_PluginName"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 815
    .local v35, "pluginName":Ljava/lang/String;
    if-ltz v15, :cond_0

    const/16 v5, 0xa

    if-le v15, v5, :cond_1

    .line 818
    :cond_0
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Proximity threshold out of range, value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v36

    .line 820
    .local v36, "response":Landroid/os/Message;
    const/16 v5, -0x9

    move-object/from16 v0, v36

    iput v5, v0, Landroid/os/Message;->what:I

    .line 821
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 950
    .end local v36    # "response":Landroid/os/Message;
    :goto_0
    return-void

    .line 826
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v6

    .line 828
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v32

    .line 829
    .local v32, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_2
    :goto_1
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 831
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 834
    .local v29, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 840
    move-object/from16 v0, v29

    instance-of v5, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    if-eqz v5, :cond_3

    .line 842
    const/16 v31, 0x1

    .line 854
    .local v31, "isSpdCadCombinedSensor":Z
    :goto_2
    move-object/from16 v0, v29

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    const/16 v17, 0x1

    if-eqz v31, :cond_6

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    :goto_3
    move/from16 v0, v17

    invoke-virtual {v9, v7, v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->putResultInfo(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;ZLcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;

    move-result-object v34

    .line 856
    .local v34, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    new-instance v24, Landroid/os/Bundle;

    invoke-direct/range {v24 .. v24}, Landroid/os/Bundle;-><init>()V

    .line 857
    .local v24, "b":Landroid/os/Bundle;
    const-string v5, "parcelable_AsyncScanResultDeviceInfo"

    move-object/from16 v0, v34

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->resultInfo:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 858
    const-string v5, "bool_IsCombinedSensor"

    move-object/from16 v0, v34

    iget-boolean v7, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->isSpdAndCadComboSensor:Z

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 860
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v37

    .line 861
    .local v37, "resultMsg":Landroid/os/Message;
    const/4 v5, 0x2

    move-object/from16 v0, v37

    iput v5, v0, Landroid/os/Message;->what:I

    .line 862
    move-object/from16 v0, v37

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 865
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 866
    :catch_0
    move-exception v26

    .line 868
    .local v26, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v7, "RemoteException sending async scan already connected devices, closing scan."

    invoke-static {v5, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    .line 870
    monitor-exit v6

    goto/16 :goto_0

    .line 873
    .end local v24    # "b":Landroid/os/Bundle;
    .end local v26    # "e":Landroid/os/RemoteException;
    .end local v29    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v31    # "isSpdCadCombinedSensor":Z
    .end local v32    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .end local v34    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    .end local v37    # "resultMsg":Landroid/os/Message;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 844
    .restart local v29    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .restart local v32    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_3
    :try_start_3
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v5

    if-ne v13, v5, :cond_4

    move-object/from16 v0, v29

    instance-of v5, v0, Lcom/dsi/ant/plugins/antplus/bikecadence/BikeCadenceDevice;

    if-nez v5, :cond_5

    :cond_4
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v5

    if-ne v13, v5, :cond_7

    move-object/from16 v0, v29

    instance-of v5, v0, Lcom/dsi/ant/plugins/antplus/bikespeeddistance/BikeSpeedDistanceDevice;

    if-eqz v5, :cond_7

    .line 847
    :cond_5
    const/16 v31, 0x0

    .restart local v31    # "isSpdCadCombinedSensor":Z
    goto :goto_2

    .line 854
    :cond_6
    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v5

    goto :goto_3

    .line 873
    .end local v29    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v31    # "isSpdCadCombinedSensor":Z
    :cond_7
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 875
    new-instance v8, Ljava/util/concurrent/Semaphore;

    const/4 v5, 0x1

    invoke-direct {v8, v5}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 876
    .local v8, "resultSentSemaphore":Ljava/util/concurrent/Semaphore;
    new-instance v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v4 .. v9}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;)V

    iput-object v4, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    .line 877
    new-instance v10, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    iget-object v0, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v16, v0

    invoke-direct/range {v10 .. v16}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;-><init>(IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 882
    .local v10, "search1":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    const/4 v5, 0x0

    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v23

    .line 883
    .local v23, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v23, :cond_9

    .line 885
    invoke-virtual {v8}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v5

    if-nez v5, :cond_8

    .line 886
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v6, "Could not acquire resultSent semaphore on failed channel acquire"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_8
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 892
    .end local v23    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :catch_1
    move-exception v26

    .line 894
    .restart local v26    # "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v6, "RemoteException setting up async scan controller executor, closing scan."

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 891
    .end local v26    # "e":Landroid/os/RemoteException;
    .restart local v23    # "antChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_9
    :try_start_5
    new-instance v27, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v5, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    .line 899
    .local v27, "executor1":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    new-instance v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v4 .. v9}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;)V

    .line 900
    .local v4, "searchCtrl2":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;
    new-instance v16, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    const/16 v17, 0x39

    const/16 v18, 0x1f96

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v19

    const/16 v20, 0x0

    move/from16 v21, v15

    move-object/from16 v22, v4

    invoke-direct/range {v16 .. v22}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;-><init>(IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 909
    .local v16, "search2":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    const/16 v39, 0x0

    .line 910
    .local v39, "secondAntChannel":Lcom/dsi/ant/channel/AntChannel;
    sget-object v5, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v6, v7, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v39

    .line 911
    if-eqz v39, :cond_a

    .line 916
    :try_start_6
    new-instance v28, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_3

    .line 922
    .local v28, "executor2":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    iget-object v5, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v0, v27

    invoke-virtual {v5, v10, v0, v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V

    .line 923
    iget-object v5, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v1, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V

    .line 933
    .end local v28    # "executor2":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :goto_4
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v30

    .line 934
    .local v30, "initMsg":Landroid/os/Message;
    const/4 v5, 0x0

    move-object/from16 v0, v30

    iput v5, v0, Landroid/os/Message;->what:I

    .line 935
    new-instance v38, Landroid/os/Bundle;

    invoke-direct/range {v38 .. v38}, Landroid/os/Bundle;-><init>()V

    .line 936
    .local v38, "retInfo":Landroid/os/Bundle;
    const-string v5, "uuid_AccessToken"

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, v38

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 937
    const-string v5, "msgr_PluginComm"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->mPccMsgHandler:Landroid/os/Messenger;

    move-object/from16 v0, v38

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 938
    move-object/from16 v0, v30

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 941
    :try_start_7
    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 942
    :catch_2
    move-exception v26

    .line 944
    .restart local v26    # "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v6, "RemoteException sending async scan init info."

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 917
    .end local v26    # "e":Landroid/os/RemoteException;
    .end local v30    # "initMsg":Landroid/os/Message;
    .end local v38    # "retInfo":Landroid/os/Bundle;
    :catch_3
    move-exception v26

    .line 919
    .restart local v26    # "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->onExecutorDeath()V

    goto/16 :goto_0

    .line 928
    .end local v26    # "e":Landroid/os/RemoteException;
    :cond_a
    iget-object v5, v9, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v5, v10, v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->beginConsecutiveSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto :goto_4
.end method

.method public isLastAccessRequestCadencePcc()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    return v0
.end method

.method public requestAccessToken(Landroid/os/Bundle;Landroid/os/Messenger;)V
    .locals 1
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 56
    const-string v0, "bool_IsCadencePcc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isReqForComboDeviceOnly:Z

    .line 58
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->requestAccessToken(Landroid/os/Bundle;Landroid/os/Messenger;)V

    .line 59
    return-void
.end method

.method public startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 29
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v22

    .line 330
    .local v22, "deviceParams":Landroid/os/Bundle;
    const-string v6, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "int_DevType"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "int_TransType"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "int_Period"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "int_RfFreq"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 336
    :cond_0
    sget-object v6, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    const-string v14, "Bundle is missing parameters"

    invoke-static {v6, v14}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v26

    .line 338
    .local v26, "response":Landroid/os/Message;
    const/4 v6, -0x4

    move-object/from16 v0, v26

    iput v6, v0, Landroid/os/Message;->what:I

    .line 339
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 435
    .end local v26    # "response":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    sget-object v6, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v6, v14, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v21

    .line 345
    .local v21, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v21, :cond_1

    .line 349
    new-instance v27, Ljava/util/concurrent/Semaphore;

    const/4 v6, 0x1

    move-object/from16 v0, v27

    invoke-direct {v0, v6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 351
    .local v27, "resultSentSemaphore":Ljava/util/concurrent/Semaphore;
    const-string v6, "int_TransType"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 352
    .local v10, "transType":I
    const-string v6, "int_RfFreq"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 353
    .local v7, "rfFreq":I
    if-eqz p1, :cond_4

    const/4 v11, 0x0

    .line 358
    .local v11, "proximityThreshold":I
    :goto_1
    if-ltz v11, :cond_3

    const/16 v6, 0xa

    if-le v11, v6, :cond_5

    .line 361
    :cond_3
    sget-object v6, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Proximity threshold out of range, value: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v6, v14}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v26

    .line 363
    .restart local v26    # "response":Landroid/os/Message;
    const/16 v6, -0x9

    move-object/from16 v0, v26

    iput v6, v0, Landroid/os/Message;->what:I

    .line 364
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 353
    .end local v11    # "proximityThreshold":I
    .end local v26    # "response":Landroid/os/Message;
    :cond_4
    const-string v6, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 369
    .restart local v11    # "proximityThreshold":I
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isReqForComboDeviceOnly:Z

    if-eqz v6, :cond_6

    .line 371
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v9

    .line 372
    .local v9, "devType":I
    const/16 v8, 0x1f96

    .line 381
    .local v8, "period":I
    :goto_2
    new-instance v12, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v27

    invoke-direct {v12, v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V

    .line 382
    .local v12, "searchCtrl1":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    new-instance v5, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    move/from16 v6, p1

    invoke-direct/range {v5 .. v12}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 386
    .local v5, "search1":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    :try_start_0
    new-instance v24, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v12}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    .local v24, "executor1":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    if-nez p1, :cond_8

    .line 396
    new-instance v20, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V

    .line 397
    .local v20, "searchCtrl2":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    new-instance v13, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    const/16 v15, 0x39

    const/16 v16, 0x1f96

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v17

    const/16 v18, 0x0

    move/from16 v14, p1

    move/from16 v19, v11

    invoke-direct/range {v13 .. v20}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 407
    .local v13, "search2":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    const/16 v28, 0x0

    .line 408
    .local v28, "secondAntChannel":Lcom/dsi/ant/channel/AntChannel;
    sget-object v6, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v6, v14, v15, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v28

    .line 409
    if-eqz v28, :cond_7

    .line 414
    :try_start_1
    new-instance v25, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 420
    .local v25, "executor2":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v12, v5, v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V

    .line 421
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v13, v1, v12}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V

    goto/16 :goto_0

    .line 376
    .end local v5    # "search1":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .end local v8    # "period":I
    .end local v9    # "devType":I
    .end local v12    # "searchCtrl1":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    .end local v13    # "search2":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .end local v20    # "searchCtrl2":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    .end local v24    # "executor1":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .end local v25    # "executor2":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .end local v28    # "secondAntChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_6
    const-string v6, "int_DevType"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 377
    .restart local v9    # "devType":I
    const-string v6, "int_Period"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .restart local v8    # "period":I
    goto :goto_2

    .line 387
    .restart local v5    # "search1":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .restart local v12    # "searchCtrl1":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    :catch_0
    move-exception v23

    .line 389
    .local v23, "e":Landroid/os/RemoteException;
    invoke-virtual {v12}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->onExecutorDeath()V

    goto/16 :goto_0

    .line 415
    .end local v23    # "e":Landroid/os/RemoteException;
    .restart local v13    # "search2":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .restart local v20    # "searchCtrl2":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    .restart local v24    # "executor1":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .restart local v28    # "secondAntChannel":Lcom/dsi/ant/channel/AntChannel;
    :catch_1
    move-exception v23

    .line 417
    .restart local v23    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v20 .. v20}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->onExecutorDeath()V

    goto/16 :goto_0

    .line 426
    .end local v23    # "e":Landroid/os/RemoteException;
    :cond_7
    move-object/from16 v0, v24

    invoke-virtual {v12, v5, v0, v13}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->beginConsecutiveSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto/16 :goto_0

    .line 431
    .end local v13    # "search2":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .end local v20    # "searchCtrl2":Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;
    .end local v28    # "secondAntChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_8
    move-object/from16 v0, v24

    invoke-virtual {v12, v5, v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SingleSearchTaskControl;->beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V

    goto/16 :goto_0
.end method

.method public subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "returnBundle"    # Landroid/os/Bundle;

    .prologue
    .line 634
    if-nez p4, :cond_0

    .line 635
    new-instance p4, Landroid/os/Bundle;

    .end local p4    # "returnBundle":Landroid/os/Bundle;
    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    .line 636
    .restart local p4    # "returnBundle":Landroid/os/Bundle;
    :cond_0
    const-string v0, "bool_IsSpdCadCombinedSensor"

    instance-of v1, p2, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 638
    instance-of v0, p2, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 639
    check-cast v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc:Z

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->prepTypeForClientAdd(Z)V

    .line 641
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
