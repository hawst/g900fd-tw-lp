.class Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;
.super Ljava/lang/Object;
.source "DialogPref_ModifyDevice.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->showDialog(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

.field final synthetic val$d:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->val$d:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 126
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mEditText_Name:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$000(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "devNameText":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->val$d:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Must specify device name"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 159
    :goto_0
    return-void

    .line 133
    :cond_1
    const-string v2, "^[\\p{L}\\p{N}]+$"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 135
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->val$d:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Name must not contain special charcters"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 139
    :cond_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 143
    .local v0, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mCheckBox_Preferred:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$200(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->updateDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 145
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    const/4 v3, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->callChangeListener(Ljava/lang/Object;)Z
    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$300(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Ljava/lang/Object;)Z

    .line 146
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Saved changes to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 153
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->val$d:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    goto :goto_0

    .line 151
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "No Changes To Save"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 157
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v2
.end method
