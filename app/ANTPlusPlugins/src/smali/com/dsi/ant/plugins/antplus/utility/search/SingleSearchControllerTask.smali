.class public Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
.super Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;
.source "SingleSearchControllerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private channelFailureOccurred:Z

.field private searchResult:Lcom/dsi/ant/message/ChannelId;

.field private searchResultRSSI:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    .locals 6
    .param p1, "deviceNumber"    # I
    .param p2, "rfFreq"    # I
    .param p3, "period"    # I
    .param p4, "devType"    # I
    .param p5, "transType"    # I
    .param p6, "proximityThreshold"    # I
    .param p7, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    .prologue
    .line 43
    new-instance v4, Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v4, p1, p4, p5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;-><init>(IIILcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 44
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 119
    const/4 v1, 0x0

    .line 120
    .local v1, "interrupted":Z
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->channelFailureOccurred:Z

    .line 121
    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 122
    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;

    .line 123
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->initSearch()Z

    move-result v2

    if-nez v2, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->enableMessageProcessing()V

    .line 127
    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->setPerSearchParamsAndOpenSearch(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_2

    .line 133
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    .line 143
    :cond_3
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->dumbfireCloseChannel()V

    .line 146
    :cond_4
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->channelFailureOccurred:Z

    if-eqz v2, :cond_5

    .line 147
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2

    .line 135
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->disableMessageProcessing()V

    .line 138
    const/4 v1, 0x1

    goto :goto_1

    .line 149
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->cancelled:Z

    if-nez v2, :cond_0

    .line 151
    if-eqz v1, :cond_6

    .line 153
    const/16 v2, -0x16

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 155
    :cond_6
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    if-nez v2, :cond_7

    .line 157
    const/4 v2, -0x4

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->reportFailure(I)V

    goto :goto_0

    .line 161
    :cond_7
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;

    const/4 v5, 0x1

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->reportResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;Z)V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "Single Search Controller"

    return-object v0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 51
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 54
    :pswitch_0
    new-instance v3, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v3, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    .line 57
    .local v0, "channelEvent":Lcom/dsi/ant/message/EventCode;
    sget-object v3, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_1

    .line 59
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timed out"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->disableMessageProcessing()V

    .line 110
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->channelFailureOccurred:Z

    .line 111
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 61
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_0

    .line 63
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->disableMessageProcessing()V

    .line 65
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 72
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :pswitch_1
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->cancelled:Z

    if-nez v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 78
    sget-object v3, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v3, :cond_2

    .line 80
    new-instance v2, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 86
    .local v2, "msg":Lcom/dsi/ant/message/fromant/DataMessage;
    :goto_1
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->hasExtendedData()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getMeasurementType()Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    if-ne v3, v4, :cond_3

    .line 90
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getRssiValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;

    .line 96
    :goto_2
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->disableMessageProcessing()V

    .line 97
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 83
    .end local v2    # "msg":Lcom/dsi/ant/message/fromant/DataMessage;
    :cond_2
    new-instance v2, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .restart local v2    # "msg":Lcom/dsi/ant/message/fromant/DataMessage;
    goto :goto_1

    .line 93
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->searchResultRSSI:Ljava/lang/Integer;
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
