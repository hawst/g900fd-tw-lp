.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;
.super Ljava/lang/Object;
.source "AntFsPasskeyDatabase.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "default_saved_antfs_passkey.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dbContext:Landroid/content/Context;

.field private mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "contextForDb"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    .line 69
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->dbContext:Landroid/content/Context;

    .line 70
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;->close()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    .line 87
    :cond_0
    return-void
.end method

.method public getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    .locals 14
    .param p1, "antFsManufacturerId"    # I
    .param p2, "antFsDeviceType"    # I
    .param p3, "antDeviceNumber"    # I
    .param p4, "antFsSerialNumber"    # J

    .prologue
    .line 99
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    .line 100
    .local v12, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT AntFsDeviceInfo_Id, Passkey FROM AntFsDeviceInfo WHERE (AntFsManufacturerId == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntFsDeviceType == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntDeviceNumber == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND AntFsSerialNumber == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ");"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 108
    .local v11, "cPlugin":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 109
    .local v2, "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 112
    .local v13, "passkey_dbID":I
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 114
    .local v5, "passkey":[B
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .end local v2    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    int-to-long v3, v13

    move v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move-wide/from16 v9, p4

    invoke-direct/range {v2 .. v10}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;-><init>(J[BIIIJ)V

    .line 117
    .end local v5    # "passkey":[B
    .end local v13    # "passkey_dbID":I
    .restart local v2    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 118
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 119
    return-object v2
.end method

.method public insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 12
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    const/4 v11, 0x0

    .line 129
    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    iget v2, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    iget v3, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    iget-wide v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    move-result-object v9

    .line 130
    .local v9, "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 131
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v9, :cond_1

    .line 133
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 134
    .local v10, "values":Landroid/content/ContentValues;
    const-string v0, "Passkey"

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 135
    const-string v0, "AntFsDeviceInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AntFsDeviceInfo_Id == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v10, v1, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 137
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->TAG:Ljava/lang/String;

    const-string v1, "SQL Update() did not change exactly 1 passkey row"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    iget-wide v0, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    iput-wide v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    .line 152
    :goto_0
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 153
    return-void

    .line 143
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_1
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 144
    .restart local v10    # "values":Landroid/content/ContentValues;
    const-string v0, "Passkey"

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 145
    const-string v0, "AntFsManufacturerId"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    const-string v0, "AntFsDeviceType"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147
    const-string v0, "AntDeviceNumber"

    iget v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 148
    const-string v0, "AntFsSerialNumber"

    iget-wide v1, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 149
    const-string v0, "AntFsDeviceInfo"

    invoke-virtual {v6, v0, v11, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    .line 150
    .local v7, "dbID":J
    iput-wide v7, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    goto :goto_0
.end method

.method public invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 5
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    .line 161
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 162
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AntFsDeviceInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntFsDeviceInfo_Id == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 164
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->TAG:Ljava/lang/String;

    const-string v2, "SQL Delete() failed to remove exactly 1 passkey"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 167
    return-void
.end method

.method public open()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->dbContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase$DbHelper;

    .line 77
    :cond_0
    return-void
.end method
