.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;
.super Ljava/lang/Object;
.source "BloodPressureResetDataAndSetTimeProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResetRequestor"
.end annotation


# instance fields
.field public client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

.field public final doSetTime:Z

.field public isCancelled:Z

.field msgLock:Ljava/lang/Object;

.field public final useAntFsProgressUpdates:Z


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ZZLcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;)V
    .locals 1
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "doSetTime"    # Z
    .param p3, "useAntFsProgressUpdates"    # Z
    .param p4, "device"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->msgLock:Ljava/lang/Object;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->isCancelled:Z

    .line 43
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 44
    iput-boolean p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->doSetTime:Z

    .line 45
    iput-boolean p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->useAntFsProgressUpdates:Z

    .line 46
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    .line 47
    return-void
.end method


# virtual methods
.method public reportAntFsStatus(IJJ)V
    .locals 6
    .param p1, "antFsStateCode"    # I
    .param p2, "transferredBytes"    # J
    .param p4, "totalBytes"    # J

    .prologue
    .line 76
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->msgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 78
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->isCancelled:Z

    if-nez v2, :cond_0

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_stateCode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v2, "long_transferredBytes"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 83
    const-string v2, "long_totalBytes"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 85
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 86
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 87
    const/16 v2, 0xbe

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 88
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 90
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v2, v4, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 96
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    monitor-exit v3

    .line 97
    return-void

    .line 94
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download Measurement ANTFS state message sent after finished sent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public sendResult(I)V
    .locals 6
    .param p1, "antFsStateCode"    # I

    .prologue
    .line 51
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->msgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 53
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->isCancelled:Z

    if-nez v2, :cond_0

    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 59
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 60
    const/16 v2, 0xce

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 61
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 63
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v2, v4, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 65
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->isCancelled:Z

    .line 71
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    monitor-exit v3

    .line 72
    return-void

    .line 69
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ResetDataAndSetTime extraneous result sent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
