.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
.super Ljava/lang/Enum;
.source "AudioVideoCommandNumber.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum AHEAD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum BACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum CUSTOM_REPEAT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum CUSTOM_SHUFFLE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum FAST_FORWARD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum FAST_REWIND:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum MUTE_UNMUTE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum PAUSE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum PLAY:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum RECORD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum REPEAT_ALL:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum REPEAT_CURRENT_TRACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum REPEAT_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum SHUFFLE_ALBUMS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum SHUFFLE_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum SHUFFLE_TRACKS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum STOP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum VOLUME_DOWN:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

.field public static final enum VOLUME_UP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;


# instance fields
.field private audioCommand:Z

.field private intValue:I

.field private videoPlaybackCommand:Z

.field private videoRecorderCommand:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 14
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "PLAY"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->PLAY:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 18
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "PAUSE"

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->PAUSE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 22
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "STOP"

    const/4 v2, 0x2

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->STOP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "VOLUME_UP"

    const/4 v2, 0x3

    const/4 v3, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->VOLUME_UP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "VOLUME_DOWN"

    const/4 v2, 0x4

    const/4 v3, 0x5

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->VOLUME_DOWN:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 34
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "MUTE_UNMUTE"

    const/4 v2, 0x5

    const/4 v3, 0x6

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->MUTE_UNMUTE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 38
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "AHEAD"

    const/4 v2, 0x6

    const/4 v3, 0x7

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->AHEAD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 42
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "BACK"

    const/4 v2, 0x7

    const/16 v3, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->BACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 46
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "REPEAT_CURRENT_TRACK"

    const/16 v2, 0x8

    const/16 v3, 0x9

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_CURRENT_TRACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 50
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "REPEAT_ALL"

    const/16 v2, 0x9

    const/16 v3, 0xa

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_ALL:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 54
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "REPEAT_OFF"

    const/16 v2, 0xa

    const/16 v3, 0xb

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 58
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "SHUFFLE_TRACKS"

    const/16 v2, 0xb

    const/16 v3, 0xc

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_TRACKS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 62
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "SHUFFLE_ALBUMS"

    const/16 v2, 0xc

    const/16 v3, 0xd

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_ALBUMS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 66
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "SHUFFLE_OFF"

    const/16 v2, 0xd

    const/16 v3, 0xe

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 70
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "FAST_FORWARD"

    const/16 v2, 0xe

    const/16 v3, 0xf

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->FAST_FORWARD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 74
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "FAST_REWIND"

    const/16 v2, 0xf

    const/16 v3, 0x10

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->FAST_REWIND:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 78
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "CUSTOM_REPEAT"

    const/16 v2, 0x10

    const/16 v3, 0x11

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->CUSTOM_REPEAT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 82
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "CUSTOM_SHUFFLE"

    const/16 v2, 0x11

    const/16 v3, 0x12

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->CUSTOM_SHUFFLE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 86
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "RECORD"

    const/16 v2, 0x12

    const/16 v3, 0x13

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->RECORD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 105
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const-string v1, "UNRECOGNIZED"

    const/16 v2, 0x13

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;-><init>(Ljava/lang/String;IIZZZ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 9
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    const/4 v1, 0x0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->PLAY:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->PAUSE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->STOP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->VOLUME_UP:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->VOLUME_DOWN:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->MUTE_UNMUTE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->AHEAD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->BACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_CURRENT_TRACK:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_ALL:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->REPEAT_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_TRACKS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_ALBUMS:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->SHUFFLE_OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->FAST_FORWARD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->FAST_REWIND:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->CUSTOM_REPEAT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->CUSTOM_SHUFFLE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->RECORD:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZZ)V
    .locals 0
    .param p3, "intValue"    # I
    .param p4, "audioCommand"    # Z
    .param p5, "videoPlaybackCommand"    # Z
    .param p6, "videoRecorderCommand"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZZ)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->intValue:I

    .line 115
    iput-boolean p4, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->audioCommand:Z

    .line 116
    iput-boolean p5, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->videoPlaybackCommand:Z

    .line 117
    iput-boolean p6, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->videoRecorderCommand:Z

    .line 118
    return-void
.end method

.method public static getAudioCommands()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v1, "audioCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v0

    .line 168
    .local v0, "allCommands":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    .line 170
    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->isAudioCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 176
    :cond_1
    return-object v1
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 151
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 153
    .local v1, "cmd":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 160
    .end local v1    # "cmd":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    :goto_1
    return-object v1

    .line 151
    .restart local v1    # "cmd":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 157
    .end local v1    # "cmd":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    move-object v1, v4

    .line 160
    goto :goto_1
.end method

.method public static getVideoPlaybackCommands()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v2, "videoPlaybackCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v0

    .line 184
    .local v0, "allCommands":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 186
    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->isVideoPlaybackCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 192
    :cond_1
    return-object v2
.end method

.method public static getVideoRecorderCommands()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v2, "videoRecorderCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v0

    .line 200
    .local v0, "allCommands":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 202
    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->isVideoRecorderCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_1
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->intValue:I

    return v0
.end method

.method public isAudioCommand()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->audioCommand:Z

    return v0
.end method

.method public isVideoPlaybackCommand()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->videoPlaybackCommand:Z

    return v0
.end method

.method public isVideoRecorderCommand()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->videoRecorderCommand:Z

    return v0
.end method
