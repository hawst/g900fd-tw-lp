.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->connectArsAndRepost(Landroid/os/Message;Landroid/os/Messenger;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final connLock:Ljava/lang/Object;

.field private serviceHasDied:Z

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$origRequest:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 269
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->val$origRequest:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->connLock:Ljava/lang/Object;

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->serviceHasDied:Z

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arsBinder"    # Landroid/os/IBinder;

    .prologue
    .line 276
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->connLock:Ljava/lang/Object;

    monitor-enter v2

    .line 278
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->serviceHasDied:Z

    if-nez v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    new-instance v3, Lcom/dsi/ant/AntService;

    invoke-direct {v3, p2}, Lcom/dsi/ant/AntService;-><init>(Landroid/os/IBinder;)V

    iput-object v3, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsComm:Lcom/dsi/ant/AntService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccReqAccessHandler:Landroid/os/Messenger;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->val$origRequest:Landroid/os/Message;

    invoke-static {v3}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 290
    return-void

    .line 284
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v3, "RemoteException sending message to local messenger, inconceivable"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 295
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->connLock:Ljava/lang/Object;

    monitor-enter v3

    .line 297
    :try_start_0
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Plugin-ARS binder OnServiceDisconnected!"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->serviceHasDied:Z

    .line 299
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->unbindFromArs()V

    .line 303
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 305
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    goto :goto_0

    .line 307
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    return-void
.end method
