.class public abstract Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
.source "PluginDownloadFilesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TypicalPluginDownloadController"
.end annotation


# instance fields
.field protected finishedEventCode:I

.field stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field protected useProgressUpdates:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "finishedEventCode"    # I
    .param p2, "useProgressUpdates"    # Z

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;-><init>()V

    .line 47
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .line 68
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->finishedEventCode:I

    .line 69
    iput-boolean p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->useProgressUpdates:Z

    .line 70
    return-void
.end method


# virtual methods
.method public getAntFsStateReceiver()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->stateReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    return-object v0
.end method

.method public handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V
    .locals 2
    .param p1, "file"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .param p2, "targetDownloadBytes"    # J
    .param p4, "fileDownloadedBytes"    # [B

    .prologue
    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_targetBytes"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    const-string v1, "arrayByte_rawFileBytes"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 93
    const/16 v1, 0xbf

    invoke-virtual {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->sendClientDownloadEvent(ILandroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method public handleFinished(I)V
    .locals 2
    .param p1, "antFsRequestStatusCode"    # I

    .prologue
    .line 99
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_statusCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->finishedEventCode:I

    invoke-virtual {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->sendClientDownloadEvent(ILandroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method public handleStateChange(IJJ)V
    .locals 2
    .param p1, "antfsStateCode"    # I
    .param p2, "transferredBytes"    # J
    .param p4, "totalBytes"    # J

    .prologue
    .line 107
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->useProgressUpdates:Z

    if-eqz v1, :cond_0

    .line 109
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_stateCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    const-string v1, "long_transferredBytes"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 112
    const-string v1, "long_totalBytes"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 113
    const/16 v1, 0xbe

    invoke-virtual {p0, v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->sendClientDownloadEvent(ILandroid/os/Bundle;)V

    .line 115
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected sendClientDownloadEvent(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "eventCode"    # I
    .param p2, "b"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 75
    .local v0, "response":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 76
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 77
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;->sendMessageToClient(Landroid/os/Message;)V

    .line 79
    return-void
.end method

.method protected abstract sendMessageToClient(Landroid/os/Message;)V
.end method
