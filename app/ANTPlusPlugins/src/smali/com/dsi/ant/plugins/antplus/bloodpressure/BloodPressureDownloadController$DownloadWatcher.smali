.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadWatcher"
.end annotation


# instance fields
.field public client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

.field public deviceDbId:J

.field public downloadNewOnly:Z

.field public gotMeasurementThisRound:Z

.field public hasSynced:Z

.field public isCancelled:Z

.field public lastSeenFileTimeThreshold:J

.field public lastSeenMeasurementTime:J

.field public monitorForNewMeasurements:Z

.field msgLock:Ljava/lang/Object;

.field public needsDownloadThisRound:Z

.field public useAntFsProgressUpdates:Z


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ZZZLcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;)V
    .locals 2
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "downloadNewOnly"    # Z
    .param p3, "monitorForNewMeasurements"    # Z
    .param p4, "useAntFsProgressUpdates"    # Z
    .param p5, "device"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->msgLock:Ljava/lang/Object;

    .line 56
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    .line 58
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->hasSynced:Z

    .line 67
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 68
    iput-boolean p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->downloadNewOnly:Z

    .line 69
    iput-boolean p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->monitorForNewMeasurements:Z

    .line 70
    iput-boolean p4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->useAntFsProgressUpdates:Z

    .line 71
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    .line 72
    return-void
.end method

.method private sendStatusMessage(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;I)V
    .locals 6
    .param p1, "status"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;
    .param p2, "antFsStateCode"    # I

    .prologue
    .line 86
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->msgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 88
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    if-nez v2, :cond_1

    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->getIntValue()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    const-string v2, "int_finishedCode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 95
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 96
    const/16 v2, 0xcc

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 97
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 99
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v2, v4, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 101
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->FINISHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    if-ne p1, v2, :cond_0

    .line 102
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    .line 108
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    monitor-exit v3

    .line 109
    return-void

    .line 106
    :cond_1
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download Measurement status message sent after finished sent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public reportAntFsStatus(IJJ)V
    .locals 6
    .param p1, "antFsStateCode"    # I
    .param p2, "transferredBytes"    # J
    .param p4, "totalBytes"    # J

    .prologue
    .line 113
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->msgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 115
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    if-nez v2, :cond_0

    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_stateCode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v2, "long_transferredBytes"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 120
    const-string v2, "long_totalBytes"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 122
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 123
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 124
    const/16 v2, 0xbe

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 125
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 127
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v2, v4, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 133
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    monitor-exit v3

    .line 134
    return-void

    .line 131
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download Measurement ANTFS state message sent after finished sent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public sendFinished(I)V
    .locals 1
    .param p1, "antFsStatusfailureCode"    # I

    .prologue
    .line 76
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->FINISHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    invoke-direct {p0, v0, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendStatusMessage(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;I)V

    .line 77
    return-void
.end method

.method public sendMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;)Z
    .locals 6
    .param p1, "measurement"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;

    .prologue
    .line 138
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->msgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 140
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    if-nez v2, :cond_0

    .line 142
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "parcelable_measurement"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 146
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 147
    const/16 v2, 0xcd

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 148
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->device:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v2, v4, v1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    move-result v2

    monitor-exit v3

    .line 155
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    return v2

    .line 154
    :cond_0
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download Measurement measurement sent after finished sent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public sendStatusUpdate(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;)V
    .locals 1
    .param p1, "currentState"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendStatusMessage(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;I)V

    .line 82
    return-void
.end method
