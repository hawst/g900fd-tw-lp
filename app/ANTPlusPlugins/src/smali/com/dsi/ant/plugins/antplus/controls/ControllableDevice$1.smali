.class Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;
.super Ljava/lang/Object;
.source "ControllableDevice.java"

# interfaces
.implements Lcom/dsi/ant/channel/IAntChannelEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChannelDeath()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->onChannelDeath()V

    .line 127
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 3
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 117
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;->this$0:Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v1, p1, p2}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RemoteException handling channel messages"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
