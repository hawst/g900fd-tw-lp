.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P17_WheelTorqueData.java"


# static fields
.field private static MAX_ACC_TIMEOUT:I


# instance fields
.field private decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private torqueAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private wheelPeriodAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private wheelTickAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private wheelTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x2ee0

    sput v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->MAX_ACC_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V
    .locals 4
    .param p1, "decoder"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .prologue
    const v3, 0xffff

    const/16 v2, 0xff

    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v2, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v2, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTickAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v3, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelPeriodAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    sget v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v3, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->torqueAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 36
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 37
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 18
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 57
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v14

    .line 58
    .local v14, "receivedEventCount":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v14, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 61
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v17

    .line 62
    .local v17, "receivedWheelTicks":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTickAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move/from16 v0, v17

    move-wide/from16 v1, p1

    invoke-virtual {v3, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 65
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v16

    .line 66
    .local v16, "receivedWheelPeriod":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelPeriodAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v3, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 69
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v15

    .line 70
    .local v15, "receivedTorque":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->torqueAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v15, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 72
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v8

    .line 73
    .local v8, "pageNumber":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTickAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelPeriodAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->torqueAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v12}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodeTorquePage(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 76
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v10, p5

    invoke-virtual/range {v3 .. v10}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodeInstantaneousCadence(JJILcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 78
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v13, "b":Landroid/os/Bundle;
    const-string v3, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v13, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 82
    const-string v3, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v13, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 84
    const-string v3, "long_wheelTorqueUpdateEventCount"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v4

    invoke-virtual {v13, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 86
    const-string v3, "long_accumulatedWheelTicks"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTickAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v4

    invoke-virtual {v13, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88
    const-string v3, "decimal_accumulatedWheelPeriod"

    new-instance v4, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelPeriodAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x800

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v6, 0xb

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 91
    const-string v3, "decimal_accumulatedWheelTorque"

    new-instance v4, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->torqueAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x20

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v6, 0x5

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v3, v13}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 96
    .end local v13    # "b":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;->wheelTorqEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
