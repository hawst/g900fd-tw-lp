.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "FitnessEquipmentDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$3;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private P16_GeneralFeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;

.field private P17_GeneralSettings:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;

.field private P18_GeneralMetabolicData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P18_GeneralMetabolicData;

.field private P19_TreadmillData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P19_TreadmillData;

.field private P1_CalibrationResponse:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;

.field private P20_EllipticalData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;

.field private P21_BikeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;

.field private P22_RowerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P22_RowerData;

.field private P23_ClimberData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P23_ClimberData;

.field private P24_NordicSkierData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P24_NordicSkierData;

.field private P25_TrainerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;

.field private P26_TrainerTorqueData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;

.field private P2_CalibrationInProgress:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P2_CalibrationInProgress;

.field private P48_BasicResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P48_BasicResistance;

.field private P49_TargetPower:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P49_TargetPower;

.field private P50_WindResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P50_WindResistance;

.field private P51_TrackResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P51_TrackResistance;

.field private P54_Capabilities:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;

.field private P55_UserConfiguration:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;

.field private P71_CommandStatus:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;

.field private antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field private channelDeathLock:Ljava/lang/Object;

.field private channelsDied:Z

.field private deviceNumber:I

.field private isSessionConnected:Z

.field private powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 100
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelDeathLock:Ljava/lang/Object;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelsDied:Z

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isSessionConnected:Z

    .line 169
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V
    .locals 12
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "antFsChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "broadcastChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p4, "settingsFile"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .param p5, "selectedFiles"    # [Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 100
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelDeathLock:Ljava/lang/Object;

    .line 101
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelsDied:Z

    .line 108
    const-string v1, "Fitness Equipment Session"

    iput-object v1, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 110
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isSessionConnected:Z

    .line 111
    iget-object v1, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->deviceNumber:I

    .line 114
    new-instance v9, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;

    invoke-direct {v9, p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;)V

    .line 127
    .local v9, "antFsDeathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v1, p2, v9}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    new-instance v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->deviceNumber:I

    const/16 v2, 0x39

    const/16 v3, 0x2000

    const/4 v4, 0x1

    const/4 v5, 0x5

    new-instance v6, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$2;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice$2;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;)V

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;-><init>(IIIIILcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController$AntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V

    .line 156
    .local v0, "antFsTask":Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 163
    return-void

    .line 128
    .end local v0    # "antFsTask":Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;
    :catch_0
    move-exception v11

    .line 130
    .local v11, "e1":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "Executor blew up in constructor"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1

    .line 157
    .end local v11    # "e1":Landroid/os/RemoteException;
    .restart local v0    # "antFsTask":Lcom/dsi/ant/plugins/antplus/fitnessequipment/tasks/PassthroughAntFsClientTransportController;
    :catch_1
    move-exception v10

    .line 159
    .local v10, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "Failed to start ANTFS task on executor"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 161
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    return-object p1
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->startBroadcastReceiver()V

    return-void
.end method

.method private startBroadcastReceiver()V
    .locals 6

    .prologue
    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 186
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v2, 0x39

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 187
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v2, 0x2000

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 188
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/message/ChannelId;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->deviceNumber:I

    const/16 v4, 0x11

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 189
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    .line 193
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 194
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isSessionConnected:Z

    .line 195
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->setCurrentState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 207
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException trying to configure and open receiver channel"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->onChannelDeath()V

    goto :goto_0

    .line 201
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 203
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE trying to configure and open receiver channel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 29
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->token_ClientMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 282
    .local v5, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v14

    .line 283
    .local v14, "response":Landroid/os/Message;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 284
    .local v11, "params":Landroid/os/Bundle;
    move-object/from16 v0, p2

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->what:I

    .line 285
    const/16 v19, 0xe4

    .line 286
    .local v19, "whatReqEvt":I
    const-string v20, "int_requestStatus"

    .line 288
    .local v20, "whatReqStatus":Ljava/lang/String;
    move-object/from16 v0, p2

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    sparse-switch v25, :sswitch_data_0

    .line 687
    invoke-virtual {v14}, Landroid/os/Message;->recycle()V

    .line 688
    const/4 v14, 0x0

    .line 689
    invoke-super/range {p0 .. p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 692
    :goto_0
    return-void

    .line 291
    :sswitch_0
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 292
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 293
    const/16 v25, 0x36

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 297
    :sswitch_1
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 298
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 299
    sget-object v25, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->BASIC_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v25

    move/from16 v0, v25

    int-to-byte v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 303
    :sswitch_2
    const-string v25, "decimal_totalResistance"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v17

    check-cast v17, Ljava/math/BigDecimal;

    .line 305
    .local v17, "totalResistance":Ljava/math/BigDecimal;
    if-nez v17, :cond_0

    .line 307
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 308
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetBasicResistance failed to start because total resistance was null"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 310
    :cond_0
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    .line 312
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 313
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetBasicResistance failed to start because total resistance was under 0%"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 315
    :cond_1
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "100"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2

    .line 317
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 318
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetBasicResistance failed to start because total resistance exceeded 100%"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 322
    :cond_2
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 323
    invoke-static/range {v17 .. v17}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetBasicResistance(Ljava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_1

    .line 330
    .end local v17    # "totalResistance":Ljava/math/BigDecimal;
    :sswitch_3
    const-string v25, "decimal_targetPower"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v16

    check-cast v16, Ljava/math/BigDecimal;

    .line 332
    .local v16, "targetPower":Ljava/math/BigDecimal;
    if-nez v16, :cond_3

    .line 334
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 335
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTargetPower failed to start because target power was null"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 337
    :cond_3
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    .line 339
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 340
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTargetPower failed to start because target power was under 0 W"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 342
    :cond_4
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "1000"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    .line 344
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 345
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTargetPower failed to start because target power exceeded 1000 W"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 349
    :cond_5
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 350
    invoke-static/range {v16 .. v16}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetTargetPower(Ljava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_2

    .line 357
    .end local v16    # "targetPower":Ljava/math/BigDecimal;
    :sswitch_4
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 358
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 359
    sget-object v25, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TARGET_POWER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v25

    move/from16 v0, v25

    int-to-byte v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 363
    :sswitch_5
    const-string v25, "decimal_grade"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v10

    check-cast v10, Ljava/math/BigDecimal;

    .line 364
    .local v10, "grade":Ljava/math/BigDecimal;
    const-string v25, "decimal_rollingResistanceCoefficient"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v15

    check-cast v15, Ljava/math/BigDecimal;

    .line 367
    .local v15, "rollingResistanceCoeff":Ljava/math/BigDecimal;
    if-eqz v10, :cond_7

    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "65535"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    if-eqz v25, :cond_7

    .line 369
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "-200"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 371
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 372
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTrackResistance failed to start because grade was under -200 %"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 376
    :cond_6
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "200"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    .line 378
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 379
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTrackResistance failed to start because grade exceeded 200 %"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 385
    :cond_7
    new-instance v10, Ljava/math/BigDecimal;

    .end local v10    # "grade":Ljava/math/BigDecimal;
    const-string v25, "65535"

    move-object/from16 v0, v25

    invoke-direct {v10, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 387
    .restart local v10    # "grade":Ljava/math/BigDecimal;
    :cond_8
    if-eqz v15, :cond_a

    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    if-eqz v25, :cond_a

    .line 389
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_9

    .line 391
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 392
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTrackResistance failed to start because the rolling resistance coefficient was under 0"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 396
    :cond_9
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0.0127"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 398
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 399
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetTrackResistance failed to start because the rolling resistance coefficient exceeded 0.0127"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 405
    :cond_a
    new-instance v15, Ljava/math/BigDecimal;

    .end local v15    # "rollingResistanceCoeff":Ljava/math/BigDecimal;
    const-string v25, "255"

    move-object/from16 v0, v25

    invoke-direct {v15, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 407
    .restart local v15    # "rollingResistanceCoeff":Ljava/math/BigDecimal;
    :cond_b
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 408
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 409
    invoke-static {v10, v15}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 413
    .end local v10    # "grade":Ljava/math/BigDecimal;
    .end local v15    # "rollingResistanceCoeff":Ljava/math/BigDecimal;
    :sswitch_6
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 414
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 415
    sget-object v25, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TRACK_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v25

    move/from16 v0, v25

    int-to-byte v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 418
    :sswitch_7
    const-string v25, "decimal_windResistanceCoefficient"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v21

    check-cast v21, Ljava/math/BigDecimal;

    .line 420
    .local v21, "windResistanceCoeff":Ljava/math/BigDecimal;
    const-string v25, "int_windSpeed"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v23

    .line 421
    .local v23, "windSpeed":I
    const-string v25, "decimal_draftingFactor"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    .line 424
    .local v6, "draftingFactor":Ljava/math/BigDecimal;
    if-eqz v21, :cond_d

    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    if-eqz v25, :cond_d

    .line 426
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_c

    .line 428
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 429
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind resistance coefficient was under 0 kg/m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 433
    :cond_c
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "1.86"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_e

    .line 435
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 436
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind resistance coefficient exceeded 1.86 kg/m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 442
    :cond_d
    new-instance v21, Ljava/math/BigDecimal;

    .end local v21    # "windResistanceCoeff":Ljava/math/BigDecimal;
    const-string v25, "255"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 444
    .restart local v21    # "windResistanceCoeff":Ljava/math/BigDecimal;
    :cond_e
    const/16 v25, 0xff

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_10

    .line 446
    const/16 v25, -0x7f

    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_f

    .line 448
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 449
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind speed was under 0 km/h"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 453
    :cond_f
    const/16 v25, 0x7f

    move/from16 v0, v23

    move/from16 v1, v25

    if-le v0, v1, :cond_10

    .line 455
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 456
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind speed exceeded 127 km/h"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 462
    :cond_10
    if-eqz v6, :cond_12

    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    if-eqz v25, :cond_12

    .line 464
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_11

    .line 466
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 467
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the drafting factor was under 0"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 471
    :cond_11
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "1.00"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_13

    .line 473
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 474
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the drafting factor exceeded 1.00"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 480
    :cond_12
    new-instance v6, Ljava/math/BigDecimal;

    .end local v6    # "draftingFactor":Ljava/math/BigDecimal;
    const-string v25, "255"

    move-object/from16 v0, v25

    invoke-direct {v6, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 483
    .restart local v6    # "draftingFactor":Ljava/math/BigDecimal;
    :cond_13
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 484
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 485
    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v21

    move/from16 v1, v25

    invoke-static {v0, v1, v6}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetWindResistance(Ljava/math/BigDecimal;BLjava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 490
    .end local v6    # "draftingFactor":Ljava/math/BigDecimal;
    .end local v21    # "windResistanceCoeff":Ljava/math/BigDecimal;
    .end local v23    # "windSpeed":I
    :sswitch_8
    const-string v25, "decimal_frontalSurfaceArea"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Ljava/math/BigDecimal;

    .line 492
    .local v9, "frontalSurfaceArea":Ljava/math/BigDecimal;
    const-string v25, "decimal_dragCoefficient"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 494
    .local v8, "dragCoefficient":Ljava/math/BigDecimal;
    const-string v25, "decimal_airDensity"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    .line 496
    .local v4, "airDensity":Ljava/math/BigDecimal;
    const-string v25, "int_windSpeed"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 497
    .local v24, "windSpeedParam":I
    const-string v25, "decimal_draftingFactor"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/math/BigDecimal;

    .line 500
    .local v7, "draftingFactorParam":Ljava/math/BigDecimal;
    if-nez v9, :cond_14

    .line 501
    new-instance v9, Ljava/math/BigDecimal;

    .end local v9    # "frontalSurfaceArea":Ljava/math/BigDecimal;
    const-string v25, "0.40"

    move-object/from16 v0, v25

    invoke-direct {v9, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 503
    .restart local v9    # "frontalSurfaceArea":Ljava/math/BigDecimal;
    :cond_14
    if-nez v8, :cond_15

    .line 504
    new-instance v8, Ljava/math/BigDecimal;

    .end local v8    # "dragCoefficient":Ljava/math/BigDecimal;
    const-string v25, "1.0"

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 506
    .restart local v8    # "dragCoefficient":Ljava/math/BigDecimal;
    :cond_15
    if-nez v4, :cond_16

    .line 507
    new-instance v4, Ljava/math/BigDecimal;

    .end local v4    # "airDensity":Ljava/math/BigDecimal;
    const-string v25, "1.275"

    move-object/from16 v0, v25

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 509
    .restart local v4    # "airDensity":Ljava/math/BigDecimal;
    :cond_16
    invoke-virtual {v9, v8}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v22

    .line 511
    .local v22, "windResistanceCoeffParam":Ljava/math/BigDecimal;
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_17

    .line 513
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 514
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the calculated wind resistance coefficient was under 0 kg/m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 518
    :cond_17
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "1.86"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_18

    .line 520
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 521
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the calculated wind resistance coefficient exceeded 1.86 kg/m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 526
    :cond_18
    const/16 v25, 0xff

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_1a

    .line 528
    const/16 v25, -0x7f

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_19

    .line 530
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 531
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind speed was under -127 km/h"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 535
    :cond_19
    const/16 v25, 0x7f

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_1a

    .line 537
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 538
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the wind speed exceeded 127 km/h"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 544
    :cond_1a
    if-eqz v7, :cond_1c

    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    if-eqz v25, :cond_1c

    .line 546
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "0"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1b

    .line 548
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 549
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the drafting factor was under 0"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 553
    :cond_1b
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "1.00"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    .line 555
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 556
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetWindResistance failed to start because the drafting factor exceeded 1.00"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 562
    :cond_1c
    new-instance v7, Ljava/math/BigDecimal;

    .end local v7    # "draftingFactorParam":Ljava/math/BigDecimal;
    const-string v25, "255"

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 564
    .restart local v7    # "draftingFactorParam":Ljava/math/BigDecimal;
    :cond_1d
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 565
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 566
    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v1, v7}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetWindResistance(Ljava/math/BigDecimal;BLjava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 571
    .end local v4    # "airDensity":Ljava/math/BigDecimal;
    .end local v7    # "draftingFactorParam":Ljava/math/BigDecimal;
    .end local v8    # "dragCoefficient":Ljava/math/BigDecimal;
    .end local v9    # "frontalSurfaceArea":Ljava/math/BigDecimal;
    .end local v22    # "windResistanceCoeffParam":Ljava/math/BigDecimal;
    .end local v24    # "windSpeedParam":I
    :sswitch_9
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 572
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 573
    sget-object v25, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->WIND_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v25

    move/from16 v0, v25

    int-to-byte v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 577
    :sswitch_a
    const-string v25, "parcelable_UserConfiguration"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;

    .line 579
    .local v18, "userConfig":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1f

    .line 581
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "0"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1e

    .line 583
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 584
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the user weight was under 0 kg"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 588
    :cond_1e
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "655.34"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_20

    .line 590
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 591
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the user weight exceeded 655.34 kg"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 597
    :cond_1f
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "65535"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    .line 599
    :cond_20
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    if-eqz v25, :cond_22

    .line 601
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "0"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_21

    .line 603
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 604
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the bicycle weight was under 0 kg"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 608
    :cond_21
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "50"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_23

    .line 610
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 611
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the bicycle weight exceeded 50 kg"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 617
    :cond_22
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "4095"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    .line 619
    :cond_23
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    if-eqz v25, :cond_25

    .line 621
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "0"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_24

    .line 623
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 624
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the bicycle wheel diameter was under 0 m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 628
    :cond_24
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "2.54"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_26

    .line 630
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 631
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the bicycle wheel diamter exceeded 2.54 m"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 637
    :cond_25
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    .line 639
    :cond_26
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    if-eqz v25, :cond_28

    .line 641
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "0.03"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_27

    .line 643
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 644
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the gear ratio was under 0.03"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 648
    :cond_27
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    new-instance v26, Ljava/math/BigDecimal;

    const-string v27, "7.65"

    invoke-direct/range {v26 .. v27}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_29

    .line 650
    const/16 v25, -0x3

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 651
    sget-object v25, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v26, "Cmd requestSetUserConfiguration failed to start because the gear ratio exceeded 7.65"

    invoke-static/range {v25 .. v26}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 657
    :cond_28
    new-instance v25, Ljava/math/BigDecimal;

    const-string v26, "255"

    invoke-direct/range {v25 .. v26}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    .line 659
    :cond_29
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 660
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 661
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->userWeight:Ljava/math/BigDecimal;

    move-object/from16 v25, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWeight:Ljava/math/BigDecimal;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->bicycleWheelDiameter:Ljava/math/BigDecimal;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;->gearRatio:Ljava/math/BigDecimal;

    move-object/from16 v28, v0

    invoke-static/range {v25 .. v28}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getSetUserConfiguration(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 667
    .end local v18    # "userConfig":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$UserConfiguration;
    :sswitch_b
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 668
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 669
    const/16 v25, 0x37

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 673
    :sswitch_c
    const-string v25, "bool_zeroOffsetCalibration"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    .line 674
    .local v13, "requestZeroOffsetCalibration":Z
    const-string v25, "bool_spinDownCalibration"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 675
    .local v12, "requestSpinDownCalibration":Z
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 676
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 677
    invoke-static {v13, v12}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;->getRequestCalibration(ZZ)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 682
    .end local v12    # "requestSpinDownCalibration":Z
    .end local v13    # "requestZeroOffsetCalibration":Z
    :sswitch_d
    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v14, Landroid/os/Message;->arg1:I

    .line 683
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 684
    const/16 v25, 0x47

    invoke-static/range {v25 .. v25}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 288
    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_d
        0x4e21 -> :sswitch_1
        0x4e22 -> :sswitch_2
        0x4e23 -> :sswitch_4
        0x4e24 -> :sswitch_3
        0x4e25 -> :sswitch_9
        0x4e26 -> :sswitch_7
        0x4e27 -> :sswitch_8
        0x4e28 -> :sswitch_6
        0x4e29 -> :sswitch_5
        0x4e2a -> :sswitch_0
        0x4e2b -> :sswitch_b
        0x4e2c -> :sswitch_a
        0x4e2d -> :sswitch_c
    .end sparse-switch
.end method

.method public addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 1
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 214
    const/16 v0, 0xca

    invoke-virtual {p0, v0, p1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->subscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    .line 217
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v0

    return v0
.end method

.method public checkChannelState(Z)V
    .locals 1
    .param p1, "isInit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isSessionConnected:Z

    if-eqz v0, :cond_0

    .line 175
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->checkChannelState(Z)V

    .line 176
    :cond_0
    return-void
.end method

.method public closeDevice()V
    .locals 0

    .prologue
    .line 745
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->onChannelDeath()V

    .line 746
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->closeDevice()V

    .line 747
    return-void
.end method

.method public getPageList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 226
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P16_GeneralFeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;

    .line 227
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P17_GeneralSettings:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;

    .line 228
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P18_GeneralMetabolicData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P18_GeneralMetabolicData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P18_GeneralMetabolicData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P18_GeneralMetabolicData;

    .line 229
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P19_TreadmillData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P19_TreadmillData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P19_TreadmillData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P19_TreadmillData;

    .line 230
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P20_EllipticalData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;

    .line 231
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P21_BikeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;

    .line 232
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P22_RowerData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P22_RowerData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P22_RowerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P22_RowerData;

    .line 233
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P23_ClimberData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P23_ClimberData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P23_ClimberData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P23_ClimberData;

    .line 234
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P24_NordicSkierData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P24_NordicSkierData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P24_NordicSkierData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P24_NordicSkierData;

    .line 237
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 238
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P1_CalibrationResponse:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;

    .line 239
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P2_CalibrationInProgress;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P2_CalibrationInProgress;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P2_CalibrationInProgress:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P2_CalibrationInProgress;

    .line 240
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P25_TrainerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;

    .line 241
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->CommonLapStateData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;-><init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P26_TrainerTorqueData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;

    .line 242
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P48_BasicResistance;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P48_BasicResistance;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P48_BasicResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P48_BasicResistance;

    .line 243
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P49_TargetPower;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P49_TargetPower;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P49_TargetPower:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P49_TargetPower;

    .line 244
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P50_WindResistance;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P50_WindResistance;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P50_WindResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P50_WindResistance;

    .line 245
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P51_TrackResistance;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P51_TrackResistance;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P51_TrackResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P51_TrackResistance;

    .line 246
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P54_Capabilities:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;

    .line 247
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P55_UserConfiguration:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;

    .line 248
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P71_CommandStatus:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P16_GeneralFeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P17_GeneralSettings:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P17_GeneralSettings;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P18_GeneralMetabolicData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P18_GeneralMetabolicData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P19_TreadmillData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P19_TreadmillData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P20_EllipticalData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P21_BikeData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P22_RowerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P22_RowerData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P23_ClimberData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P23_ClimberData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P24_NordicSkierData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P24_NordicSkierData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P1_CalibrationResponse:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P1_CalibrationResponse;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P2_CalibrationInProgress:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P2_CalibrationInProgress;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P25_TrainerData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P26_TrainerTorqueData:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P26_TrainerTorqueData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P48_BasicResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P48_BasicResistance;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P49_TargetPower:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P49_TargetPower;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P50_WindResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P50_WindResistance;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P51_TrackResistance:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P51_TrackResistance;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P54_Capabilities:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P54_Capabilities;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P55_UserConfiguration:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P55_UserConfiguration;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->P71_CommandStatus:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P71_CommandStatus;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 275
    return-object v0
.end method

.method public onChannelDeath()V
    .locals 3

    .prologue
    .line 726
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelDeathLock:Ljava/lang/Object;

    monitor-enter v1

    .line 729
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelsDied:Z

    if-nez v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->antFsExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 734
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 735
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 736
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->onChannelDeath()V

    .line 737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->channelsDied:Z

    .line 739
    :cond_1
    monitor-exit v1

    .line 740
    return-void

    .line 739
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 3
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 698
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v1, :cond_2

    .line 700
    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    .line 704
    .local v0, "eventCode":Lcom/dsi/ant/message/EventCode;
    sget-object v1, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v1, :cond_0

    .line 708
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "Broadcast reception search timed out"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 721
    .end local v0    # "eventCode":Lcom/dsi/ant/message/EventCode;
    :goto_0
    return-void

    .line 710
    .restart local v0    # "eventCode":Lcom/dsi/ant/message/EventCode;
    :cond_0
    sget-object v1, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isSessionConnected:Z

    if-eqz v1, :cond_1

    .line 711
    sget-object v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->TAG:Ljava/lang/String;

    const-string v2, "Channel closed."

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->onChannelDeath()V

    goto :goto_0

    .line 715
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0

    .line 719
    .end local v0    # "eventCode":Lcom/dsi/ant/message/EventCode;
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0
.end method
