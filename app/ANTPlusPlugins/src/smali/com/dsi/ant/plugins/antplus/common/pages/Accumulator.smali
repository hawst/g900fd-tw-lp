.class public Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
.super Ljava/lang/Object;
.source "Accumulator.java"


# instance fields
.field private accumValue:J

.field private delta:I

.field private isInitialValue:Z

.field private isInitialized:Z

.field private previousValue:I

.field private final rolloverValue:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "rolloverValue"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialized:Z

    .line 7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialValue:Z

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumValue:J

    .line 15
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->rolloverValue:I

    .line 16
    return-void
.end method

.method private reinitialize(I)V
    .locals 2
    .param p1, "initialValue"    # I

    .prologue
    const/4 v1, 0x1

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->delta:I

    .line 87
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->previousValue:I

    .line 88
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialized:Z

    .line 89
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialValue:Z

    .line 90
    return-void
.end method


# virtual methods
.method public accumulate(B)V
    .locals 2
    .param p1, "newValue"    # B

    .prologue
    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must explicitly cast byte to int to avoid unintended casting errors"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public accumulate(I)V
    .locals 8
    .param p1, "newValue"    # I

    .prologue
    .line 44
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialized:Z

    if-nez v4, :cond_1

    .line 46
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->reinitialize(I)V

    .line 53
    :goto_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v2

    .line 54
    .local v2, "originalValue":J
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    int-to-long v6, p1

    add-long/2addr v4, v6

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->previousValue:I

    int-to-long v6, v6

    sub-long v0, v4, v6

    .line 56
    .local v0, "cumulativeValue":J
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->previousValue:I

    if-le v4, p1, :cond_0

    .line 57
    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->rolloverValue:I

    int-to-long v6, v6

    add-long v0, v4, v6

    .line 58
    :cond_0
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->previousValue:I

    .line 59
    sub-long v4, v0, v2

    long-to-int v4, v4

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->delta:I

    .line 60
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumValue:J

    .line 61
    return-void

    .line 50
    .end local v0    # "cumulativeValue":J
    .end local v2    # "originalValue":J
    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialValue:Z

    goto :goto_0
.end method

.method public getDelta()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->delta:I

    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumValue:J

    return-wide v0
.end method

.method public isIntialValue()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialValue:Z

    return v0
.end method

.method public reset(JI)V
    .locals 0
    .param p1, "accumulatedValue"    # J
    .param p3, "initialValue"    # I

    .prologue
    .line 69
    iput-wide p1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumValue:J

    .line 70
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->reinitialize(I)V

    .line 71
    return-void
.end method

.method public uninitialize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->delta:I

    .line 79
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->previousValue:I

    .line 80
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialized:Z

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isInitialValue:Z

    .line 82
    return-void
.end method
