.class public Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;
.super Landroid/app/Activity;
.source "Activity_OpenChannelsList.java"


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field chanPoolChangeStateReceiver:Landroid/content/BroadcastReceiver;

.field chanProvider:Lcom/dsi/ant/AntService;

.field chanProviderConn:Landroid/content/ServiceConnection;

.field isActivityVisible:Z

.field isBound:Z

.field needsRefresh:Z

.field numChans:Landroid/widget/TextView;

.field status:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isBound:Z

    .line 27
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->needsRefresh:Z

    .line 28
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isActivityVisible:Z

    .line 34
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProviderConn:Landroid/content/ServiceConnection;

    .line 68
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanPoolChangeStateReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;Ljava/lang/Integer;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->updateChannelsAvailableDisplay(Ljava/lang/Integer;)V

    return-void
.end method

.method private updateChannelsAvailableDisplay(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "numChannelsAvailable"    # Ljava/lang/Integer;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->status:Landroid/widget/TextView;

    const-string v1, "Detected Channels Available: "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 183
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->setContentView(I)V

    .line 110
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->status:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f060003

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->status:Landroid/widget/TextView;

    const-string v1, "Connecting to binder..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanPoolChangeStateReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.dsi.ant.intent.action.CHANNEL_PROVIDER_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->needsRefresh:Z

    .line 122
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isBound:Z

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProviderConn:Landroid/content/ServiceConnection;

    invoke-static {p0, v0}, Lcom/dsi/ant/AntService;->bindService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isBound:Z

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProviderConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->unbindService(Landroid/content/ServiceConnection;)V

    .line 131
    const-string v0, "Bind failed"

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->setError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanPoolChangeStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 157
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->unbindFromArs()V

    .line 159
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 160
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isActivityVisible:Z

    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 150
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isActivityVisible:Z

    .line 143
    return-void
.end method

.method setError(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->status:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    const-string v1, "!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->numChans:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 176
    return-void
.end method

.method unbindFromArs()V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isBound:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProviderConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->unbindService(Landroid/content/ServiceConnection;)V

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->isBound:Z

    .line 168
    :cond_0
    return-void
.end method
