.class public final Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P0_DefaultHrData.java"


# static fields
.field private static final ZERO_DETECT_EVENT_COUNT:I = 0x8


# instance fields
.field private heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private hrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

.field private timeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private unchangedCounter:I


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;)V
    .locals 2
    .param p1, "heartBeatEventTime_1024ths"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p2, "rrIntervalDecoder"    # Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->hrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->timeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->unchangedCounter:I

    .line 38
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 39
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    .line 40
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 14
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 63
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v12

    .line 65
    .local v12, "rawHeartBeatEventTime":I
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 69
    .local v11, "rawHeartBeatCount":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 70
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v12}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 72
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->isIntialValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->reset(JI)V

    .line 75
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 77
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4, v12}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->reset(JI)V

    .line 78
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v12}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 80
    const/4 v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->unchangedCounter:I

    .line 81
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->INITIAL_VALUE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .line 97
    .local v13, "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    if-ne v13, v3, :cond_5

    const/4 v9, 0x1

    :goto_1
    move-wide v3, p1

    move-wide/from16 v5, p3

    invoke-virtual/range {v2 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeDefaultPageAccumulators(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Z)V

    .line 100
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->hrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 103
    .local v10, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 104
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 106
    const-string v2, "int_computedHeartRate"

    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const-string v2, "long_heartBeatCounter"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v10, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 110
    const-string v2, "decimal_timestampOfLastEvent"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0x400

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v5, 0xa

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 112
    const-string v2, "int_dataState"

    invoke-virtual {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->getIntValue()I

    move-result v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->hrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 118
    .end local v10    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->timeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 121
    .restart local v10    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide v0, p1

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 122
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v10, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 123
    const-string v2, "decimal_timestampOfLastEvent"

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    const/16 v5, 0x400

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v5, 0xa

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 125
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->timeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 127
    .end local v10    # "b":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 83
    .end local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getDelta()I

    move-result v2

    if-lez v2, :cond_3

    .line 85
    const/4 v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->unchangedCounter:I

    .line 86
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .restart local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    goto/16 :goto_0

    .line 88
    .end local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_3
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->unchangedCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->unchangedCounter:I

    const/16 v3, 0x8

    if-le v2, v3, :cond_4

    .line 90
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .restart local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    goto/16 :goto_0

    .line 94
    .end local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_4
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .restart local v13    # "state":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    goto/16 :goto_0

    .line 97
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 46
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->hrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->timeEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 137
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;->heartBeatCount:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 139
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 140
    return-void
.end method
