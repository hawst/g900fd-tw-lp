.class public final Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P1_MainInfoAndWeight.java"


# static fields
.field private static final AVERAGECOMPARISONDIFFTHRESHOLD:Ljava/math/BigDecimal;

.field private static final AVERAGECOMPARISONSEQCOUNTTHRESHOLD:I = 0x4

.field public static USER_PROFILE_UNASSIGNED:I

.field public static final WEIGHT_VALUE_COMPUTING:Ljava/math/BigDecimal;

.field public static final WEIGHT_VALUE_INVALID:Ljava/math/BigDecimal;


# instance fields
.field bodyWeight:Ljava/math/BigDecimal;

.field public bodyWeightBroadcastEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field computedBodyWeight:Ljava/math/BigDecimal;

.field displayUserProfileExchange:Z

.field private lastAverage:Ljava/math/BigDecimal;

.field scaleAntFs:Z

.field scaleUserProfileExchange:Z

.field scaleUserProfileSelected:Z

.field private sequentialAverageComparisonsPassThreshold:I

.field userProfileID:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 24
    sput v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->USER_PROFILE_UNASSIGNED:I

    .line 25
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_INVALID:Ljava/math/BigDecimal;

    .line 26
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_COMPUTING:Ljava/math/BigDecimal;

    .line 47
    new-instance v0, Ljava/math/BigDecimal;

    const-wide v1, 0x3f1a36e2eb1c432dL    # 1.0E-4

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(D)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->AVERAGECOMPARISONDIFFTHRESHOLD:Ljava/math/BigDecimal;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeightBroadcastEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 30
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    .line 46
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 75
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->userProfileID:I

    .line 76
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v3

    .line 77
    .local v3, "rawWeightValue":I
    const v5, 0xffff

    if-ne v3, v5, :cond_1

    .line 78
    sget-object v5, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_INVALID:Ljava/math/BigDecimal;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    .line 145
    :goto_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeightBroadcastEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 147
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 148
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 149
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 151
    const v5, 0xffff

    if-ne v3, v5, :cond_9

    .line 153
    const-string v5, "int_bodyWeightStatus"

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->getIntValue()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeightBroadcastEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 172
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0x80

    if-lez v5, :cond_b

    const/4 v5, 0x1

    :goto_2
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->displayUserProfileExchange:Z

    .line 173
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    and-int/lit8 v5, v5, 0x4

    if-lez v5, :cond_c

    const/4 v5, 0x1

    :goto_3
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleAntFs:Z

    .line 174
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    and-int/lit8 v5, v5, 0x2

    if-lez v5, :cond_d

    const/4 v5, 0x1

    :goto_4
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleUserProfileExchange:Z

    .line 175
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    and-int/lit8 v5, v5, 0x1

    if-lez v5, :cond_e

    const/4 v5, 0x1

    :goto_5
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleUserProfileSelected:Z

    .line 176
    return-void

    .line 79
    :cond_1
    const v5, 0xfffe

    if-ne v3, v5, :cond_2

    .line 80
    sget-object v5, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_COMPUTING:Ljava/math/BigDecimal;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    goto :goto_0

    .line 83
    :cond_2
    new-instance v5, Ljava/math/BigDecimal;

    invoke-direct {v5, v3}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v6, Ljava/math/BigDecimal;

    const/16 v7, 0x64

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v7, 0x2

    sget-object v8, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7, v8}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    const/4 v6, 0x2

    sget-object v7, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    .line 88
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    if-nez v5, :cond_3

    .line 90
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    .line 91
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    goto/16 :goto_0

    .line 95
    :cond_3
    const/4 v2, 0x0

    .line 96
    .local v2, "computationComplete":Z
    const/4 v4, 0x0

    .line 97
    .local v4, "valueMetThreshold":Z
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v5

    if-nez v5, :cond_5

    .line 99
    if-nez v3, :cond_4

    .line 101
    const/4 v4, 0x1

    .line 104
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    const/16 v6, 0x28

    if-lt v5, v6, :cond_4

    .line 105
    const/4 v2, 0x1

    .line 129
    :cond_4
    :goto_6
    if-eqz v4, :cond_8

    .line 131
    if-eqz v2, :cond_7

    .line 132
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    goto/16 :goto_0

    .line 110
    :cond_5
    if-nez v3, :cond_6

    .line 112
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    goto :goto_6

    .line 116
    :cond_6
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    new-instance v7, Ljava/math/MathContext;

    const/4 v8, 0x6

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-direct {v7, v8, v9}, Ljava/math/MathContext;-><init>(ILjava/math/RoundingMode;)V

    invoke-virtual {v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v1

    .line 117
    .local v1, "comparisonDiff":Ljava/math/BigDecimal;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->AVERAGECOMPARISONDIFFTHRESHOLD:Ljava/math/BigDecimal;

    invoke-virtual {v1, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_4

    .line 119
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    .line 120
    const/4 v4, 0x1

    .line 121
    iget v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    const/4 v6, 0x4

    if-lt v5, v6, :cond_4

    .line 123
    const/4 v2, 0x1

    goto :goto_6

    .line 134
    .end local v1    # "comparisonDiff":Ljava/math/BigDecimal;
    :cond_7
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    goto/16 :goto_0

    .line 138
    :cond_8
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    .line 139
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    .line 140
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    goto/16 :goto_0

    .line 156
    .end local v2    # "computationComplete":Z
    .end local v4    # "valueMetThreshold":Z
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_9
    const v5, 0xfffe

    if-ne v3, v5, :cond_a

    .line 158
    const-string v5, "int_bodyWeightStatus"

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->COMPUTING:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->getIntValue()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 163
    :cond_a
    const-string v5, "int_bodyWeightStatus"

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->VALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;->getIntValue()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    const-string v5, "decimal_bodyWeight"

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    .line 172
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 173
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 174
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 175
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_5
.end method

.method public getComputedBodyWeight()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getDisplayUserProfileExchange()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->displayUserProfileExchange:Z

    return v0
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeightBroadcastEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastBroadcastWeight()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->bodyWeight:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 69
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getScaleAntFs()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleAntFs:Z

    return v0
.end method

.method public getScaleUserProfileExchange()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleUserProfileExchange:Z

    return v0
.end method

.method public getScaleUserProfileSelected()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->scaleUserProfileSelected:Z

    return v0
.end method

.method public getUserProfileID()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->userProfileID:I

    return v0
.end method

.method public resetComputedWeight()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->computedBodyWeight:Ljava/math/BigDecimal;

    .line 53
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->lastAverage:Ljava/math/BigDecimal;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->sequentialAverageComparisonsPassThreshold:I

    .line 55
    return-void
.end method
