.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
.super Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;
.source "WatchCommunicatorController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ListenForNewActivitiesDownloadContoller"
.end annotation


# instance fields
.field lastDownloadCheck:J

.field listeningClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;ZLcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V
    .locals 6
    .param p2, "isDownloadNewOnly"    # Z
    .param p3, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .line 320
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Z)V

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    .line 316
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    .line 321
    return-void
.end method


# virtual methods
.method public addNewWaitClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 325
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    monitor-enter v1

    .line 327
    const-wide/16 v2, -0x1

    :try_start_0
    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    .line 328
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    const/4 v0, 0x0

    monitor-exit v1

    .line 332
    :goto_0
    return v0

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public checkNeedsDownload()V
    .locals 9

    .prologue
    .line 349
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    monitor-enter v4

    .line 351
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 352
    .local v0, "currentTime":J
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    const-wide/16 v7, -0x1

    cmp-long v3, v5, v7

    if-eqz v3, :cond_0

    const-wide/32 v5, 0x493e0

    sub-long v5, v0, v5

    iget-wide v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    cmp-long v3, v5, v7

    if-lez v3, :cond_1

    .line 356
    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;-><init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 372
    .local v2, "t":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 381
    monitor-exit v4

    .line 382
    .end local v2    # "t":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 378
    :cond_1
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    .line 379
    monitor-exit v4

    goto :goto_0

    .line 381
    .end local v0    # "currentTime":J
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public bridge synthetic getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .locals 1

    .prologue
    .line 313
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;

    move-result-object v0

    return-object v0
.end method

.method public handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V
    .locals 10
    .param p1, "file"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .param p2, "targetDownloadBytes"    # J
    .param p4, "fileDownloadedBytes"    # [B

    .prologue
    .line 428
    iget-wide v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    .line 429
    .local v4, "lastDownloadTime":J
    iget-wide v6, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    iget-wide v8, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 430
    iget-wide v6, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    iget v8, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->directoryDownloadTime:I

    int-to-long v8, v8

    add-long v4, v6, v8

    .line 432
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 433
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "uuid_targetDeviceUUID"

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 434
    const-string v6, "long_targetBytes"

    invoke-virtual {v0, v6, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 435
    const-string v6, "arrayByte_rawFileBytes"

    invoke-virtual {v0, v6, p4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 438
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    monitor-enter v7

    .line 440
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 442
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 443
    .local v1, "downloadEvent":Landroid/os/Message;
    const/4 v6, 0x1

    iput v6, v1, Landroid/os/Message;->what:I

    .line 444
    const/16 v6, 0xcb

    iput v6, v1, Landroid/os/Message;->arg1:I

    .line 445
    const/16 v6, 0xbf

    iput v6, v1, Landroid/os/Message;->arg2:I

    .line 446
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 448
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    invoke-virtual {v6, v2, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 453
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v8, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v6, v8, v9}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getLastDownloadGarminTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->downloadFilesSinceTime:J

    .line 454
    iget-wide v8, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->downloadFilesSinceTime:J

    cmp-long v6, v4, v8

    if-lez v6, :cond_1

    .line 455
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v8, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v6, v8, v9, v4, v5}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->setLastDownloadTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;J)V

    goto :goto_0

    .line 458
    .end local v1    # "downloadEvent":Landroid/os/Message;
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459
    return-void
.end method

.method public handleFinished(I)V
    .locals 3
    .param p1, "antFsRequestStatusCode"    # I

    .prologue
    .line 399
    # getter for: Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Check for new activities on device "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " finished. Result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    if-nez p1, :cond_0

    .line 401
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    .line 402
    :cond_0
    return-void
.end method

.method public bridge synthetic handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 1
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 313
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z

    move-result v0

    return v0
.end method

.method public handlePreFileHandling(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    .locals 10
    .param p1, "afs"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 408
    const-wide v4, 0x7fffffffffffffffL

    .line 409
    .local v4, "lowestLastDownloadTimeOfClients":J
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    monitor-enter v7

    .line 411
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 413
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v8, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    invoke-virtual {v6, v8, v9}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getLastDownloadGarminTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J

    move-result-wide v0

    .line 414
    .local v0, "dt":J
    cmp-long v6, v0, v4

    if-gez v6, :cond_0

    .line 415
    move-wide v4, v0

    goto :goto_0

    .line 417
    .end local v0    # "dt":J
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    iput-wide v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->downloadFilesSinceTime:J

    .line 420
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastRequestDuration()I

    move-result v6

    iput v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->directoryDownloadTime:I

    .line 421
    const/4 v6, 0x1

    return v6

    .line 417
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public bridge synthetic includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z
    .locals 1
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .prologue
    .line 313
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$RequestBasedDownloadController;->includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z

    move-result v0

    return v0
.end method

.method public removeWaitClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 338
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    monitor-enter v2

    .line 340
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 341
    .local v0, "wasInList":Z
    :goto_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->listeningClients:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    :cond_0
    monitor-exit v2

    return v0

    .line 340
    .end local v0    # "wasInList":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public reportNewDataFlagSeen()V
    .locals 2

    .prologue
    .line 386
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->lastDownloadCheck:J

    .line 387
    return-void
.end method

.method protected sendMessageToClient(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 392
    # getter for: Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Check for new activities attempting to send unexpected message, event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    .line 394
    return-void
.end method
