.class final Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;
.super Ljava/lang/Object;
.source "PluginDownloadFilesHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$antDeviceNumber:I

.field final synthetic val$beaconInterval:I

.field final synthetic val$channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field final synthetic val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

.field final synthetic val$linkRfFreq:I

.field final synthetic val$linkRfPeriod:I

.field final synthetic val$passkeys:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

.field final synthetic val$serialNumberForHost:J


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIIILcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$serialNumberForHost:J

    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$antDeviceNumber:I

    iput p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$linkRfFreq:I

    iput p6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$linkRfPeriod:I

    iput p7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$beaconInterval:I

    iput-object p8, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iput-object p9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$passkeys:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 27

    .prologue
    .line 134
    new-instance v15, Ljava/lang/Object;

    invoke-direct {v15}, Ljava/lang/Object;-><init>()V

    .line 135
    .local v15, "finishedLock":Ljava/lang/Object;
    const/16 v18, 0x0

    .line 138
    .local v18, "isFinished":Z
    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->getAntFsStateReceiver()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$serialNumberForHost:J

    move-wide/from16 v25, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$antDeviceNumber:I

    move-wide/from16 v0, v25

    invoke-direct {v8, v2, v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JI)V

    .line 139
    .local v8, "afs":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$linkRfFreq:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$linkRfPeriod:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$beaconInterval:I

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v8, v2, v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->setLinkChannelParameters(III)V

    .line 143
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v8, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v3, -0x14

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    move-object/from16 v0, v25

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    invoke-static {v2, v3, v15, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$000(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;

    move-result-object v19

    .line 153
    .local v19, "prepTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    if-eqz v19, :cond_2

    .line 155
    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestStartAntFsMode(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 156
    .local v20, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/4 v3, 0x0

    move-object/from16 v0, v20

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    invoke-static {v0, v2, v15, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$100(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 238
    :try_start_2
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 246
    .end local v19    # "prepTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .end local v20    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :catch_0
    move-exception v12

    .line 248
    .local v12, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ANTFS request InterruptedException"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v3, -0x28

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    move-object/from16 v0, v25

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    invoke-static {v2, v3, v15, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$000(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 160
    .end local v12    # "e":Ljava/lang/InterruptedException;
    .restart local v19    # "prepTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v3, 0x64

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handleStateChange(IJJ)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$passkeys:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;

    invoke-virtual {v8, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestConnectToTransport(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 163
    .restart local v20    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/4 v3, 0x0

    move-object/from16 v0, v20

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    invoke-static {v0, v2, v15, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$100(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 238
    :try_start_4
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 168
    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v3, 0x320

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handleStateChange(IJJ)V

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handlePreDirectory(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    if-nez v2, :cond_4

    .line 238
    :try_start_6
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 173
    :cond_4
    const/4 v2, 0x0

    :try_start_7
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;)V

    invoke-virtual {v8, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v10

    .line 181
    .local v10, "downloadResult":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/4 v3, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsDlResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    invoke-static {v10, v2, v15, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v2

    if-eqz v2, :cond_5

    .line 238
    :try_start_8
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_0

    .line 187
    :cond_5
    :try_start_9
    new-instance v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;

    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastDownloadedData()[B

    move-result-object v2

    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastRequestStartedTimeUtc()Ljava/util/GregorianCalendar;

    move-result-object v3

    invoke-direct {v9, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;-><init>([BLjava/util/GregorianCalendar;)V
    :try_end_9
    .catch Ljava/util/zip/DataFormatException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 195
    .local v9, "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handlePreFileHandling(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v2

    if-nez v2, :cond_6

    .line 238
    :try_start_b
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0

    goto/16 :goto_0

    .line 188
    .end local v9    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    :catch_1
    move-exception v12

    .line 190
    .local v12, "e":Ljava/util/zip/DataFormatException;
    :try_start_c
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "ANTFS directory DataFormatException: "

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/16 v3, -0x28

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    move-object/from16 v0, v25

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    invoke-static {v2, v3, v15, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$000(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 238
    :try_start_d
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0

    goto/16 :goto_0

    .line 198
    .end local v12    # "e":Ljava/util/zip/DataFormatException;
    .restart local v9    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    :cond_6
    const-wide/16 v23, 0x0

    .line 199
    .local v23, "totalDlBytes":J
    :try_start_e
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v14, "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    iget-object v2, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_8

    .line 202
    iget-object v2, v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 203
    .local v13, "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    invoke-virtual {v2, v13}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 205
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    iget-wide v2, v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    add-long v23, v23, v2

    .line 200
    :cond_7
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 211
    .end local v13    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    :cond_8
    const-wide/16 v21, 0x0

    .line 212
    .local v21, "totalBytesDownloaded":J
    move-wide/from16 v6, v23

    .line 213
    .local v6, "totalBytesToDownload":J
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 215
    .restart local v13    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    move-wide/from16 v4, v21

    .line 216
    .local v4, "cachedCurrentCumulativeBytes":J
    iget v0, v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    move/from16 v25, v0

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;JJ)V

    move/from16 v0, v25

    invoke-virtual {v8, v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v10

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/4 v3, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsDlResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    invoke-static {v10, v2, v15, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result v2

    if-eqz v2, :cond_9

    .line 238
    :try_start_f
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0

    goto/16 :goto_0

    .line 228
    :cond_9
    :try_start_10
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->getLastDownloadedData()[B

    move-result-object v11

    .line 229
    .local v11, "downloadedFile":[B
    iget-wide v2, v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    add-long v21, v21, v2

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    iget-wide v0, v13, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    move-wide/from16 v25, v0

    move-wide/from16 v0, v25

    invoke-virtual {v2, v13, v0, v1, v11}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handleFileDownloadFinished(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;J[B)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_2

    .line 238
    .end local v4    # "cachedCurrentCumulativeBytes":J
    .end local v6    # "totalBytesToDownload":J
    .end local v9    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .end local v10    # "downloadResult":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .end local v11    # "downloadedFile":[B
    .end local v13    # "file":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v14    # "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    .end local v16    # "i":I
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "prepTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .end local v20    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .end local v21    # "totalBytesDownloaded":J
    .end local v23    # "totalDlBytes":J
    :catchall_0
    move-exception v2

    :try_start_11
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    .restart local v20    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v3, :cond_a

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v25, "DownloadAll Failed to close ANTFS session."

    move-object/from16 v0, v25

    invoke-static {v3, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    throw v2
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_0

    .line 233
    .restart local v6    # "totalBytesToDownload":J
    .restart local v9    # "directory":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
    .restart local v10    # "downloadResult":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .restart local v14    # "filesToDownloadQueue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;>;"
    .restart local v16    # "i":I
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v19    # "prepTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .restart local v21    # "totalBytesDownloaded":J
    .restart local v23    # "totalDlBytes":J
    :cond_b
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;->val$controller:Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;

    const/4 v3, 0x0

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    move-object/from16 v0, v25

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    invoke-static {v2, v3, v15, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$000(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 238
    :try_start_13
    invoke-virtual {v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v20

    .line 239
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-object/from16 v0, v20

    if-eq v0, v2, :cond_0

    .line 242
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DownloadAll Failed to close ANTFS session."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_13} :catch_0

    goto/16 :goto_0
.end method
