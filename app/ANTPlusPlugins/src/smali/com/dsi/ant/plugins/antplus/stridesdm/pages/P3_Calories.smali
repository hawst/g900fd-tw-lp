.class public Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;
.super Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;
.source "P3_Calories.java"


# instance fields
.field private calorieAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private clEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;-><init>()V

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->clEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->calorieAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 5
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 46
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    .line 48
    .local v1, "receivedCalories":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->calorieAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 50
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->clEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 54
    const-string v2, "long_EventFlags"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 55
    const-string v2, "long_cumulativeCalories"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->calorieAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 59
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->clEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 62
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 63
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v0, "e3":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->clEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P2to15_Base_CadenceAndStatusSupplementary;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 34
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/stridesdm/pages/P3_Calories;->calorieAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 70
    return-void
.end method
