.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;
.super Ljava/lang/Object;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BikeMethods"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V
    .locals 0

    .prologue
    .line 3555
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestBasicResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    .line 3664
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestBasicResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestCommandStatus(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    .line 3651
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestCommandStatus(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetBasicResistance(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "totalResistance"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 3680
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetBasicResistance(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetTargetPower(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "targetPower"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 3710
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetTargetPower(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "grade"    # Ljava/math/BigDecimal;
    .param p2, "rollingResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 3822
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetWindResistance(Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "windResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p2, "windSpeed"    # Ljava/lang/Integer;
    .param p3, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p4, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 3746
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetWindResistance(Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetWindResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 7
    .param p1, "frontalSurfaceArea"    # Ljava/math/BigDecimal;
    .param p2, "dragCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "airDensity"    # Ljava/math/BigDecimal;
    .param p4, "windSpeed"    # Ljava/lang/Integer;
    .param p5, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p6, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 3781
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetWindResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestTargetPower(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    .line 3695
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestTargetPower(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestTrackResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    .line 3796
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestTrackResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestWindResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    .line 3725
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestWindResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 1
    .param p1, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    .line 3599
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeBikeDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;)V
    .locals 2
    .param p1, "BikeDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    .prologue
    const/16 v1, 0xd0

    .line 3565
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    .line 3566
    if-eqz p1, :cond_0

    .line 3568
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    .line 3574
    :goto_0
    return-void

    .line 3572
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    goto :goto_0
.end method

.method public subscribeCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 1
    .param p1, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    .line 3586
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 1
    .param p1, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    .line 3612
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 1
    .param p1, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    .line 3638
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 1
    .param p1, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    .line 3625
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z

    move-result v0

    return v0
.end method
