.class public Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.source "GeocacheReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver$2;
    }
.end annotation


# static fields
.field public static final DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field public deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

.field public final mDevType:I

.field public final mPeriod:I

.field public final mRfFreq:I

.field public final mTransType:I

.field private updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->TAG:Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/math/BigDecimal;

    const/16 v1, 0xb4

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v1, Ljava/math/BigDecimal;

    const-wide v2, 0x80000000L

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

    .line 41
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;)V
    .locals 6
    .param p1, "antChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "geocacheService"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x2000

    const/16 v4, 0x39

    .line 62
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;I)V

    .line 43
    const/16 v2, 0x13

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->mDevType:I

    .line 44
    const/4 v2, 0x0

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->mTransType:I

    .line 45
    iput v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->mPeriod:I

    .line 46
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->mRfFreq:I

    .line 63
    new-instance v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 64
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 65
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    const-string v3, "GeocacheScanner"

    iput-object v3, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 67
    new-instance v2, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-direct {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    .line 71
    :try_start_0
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver$1;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver$1;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;)V

    invoke-direct {v2, p1, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 80
    invoke-virtual {p1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    .line 81
    .local v0, "currentState":Lcom/dsi/ant/message/ChannelState;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    invoke-virtual {v0}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 100
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {p1, v2}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 101
    const/16 v2, 0x39

    invoke-virtual {p1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 102
    const/16 v2, 0x2000

    invoke-virtual {p1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 103
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {p1, v2}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 116
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;)V

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 117
    return-void

    .line 90
    :pswitch_0
    :try_start_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Channel passed to GeocacheReceiver constructor was open, it must be closed first"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 104
    .end local v0    # "currentState":Lcom/dsi/ant/message/ChannelState;
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException during initizalization"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->closeDevice()V

    .line 108
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2

    .line 92
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "currentState":Lcom/dsi/ant/message/ChannelState;
    :pswitch_1
    :try_start_2
    invoke-virtual {p1}, Lcom/dsi/ant/channel/AntChannel;->unassign()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 109
    .end local v0    # "currentState":Lcom/dsi/ant/message/ChannelState;
    :catch_1
    move-exception v1

    .line 111
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE during initialization: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 113
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 21
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->token_ClientMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 148
    .local v4, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v20

    .line 149
    .local v20, "response":Landroid/os/Message;
    move-object/from16 v0, p2

    iget v3, v0, Landroid/os/Message;->what:I

    move-object/from16 v0, v20

    iput v3, v0, Landroid/os/Message;->what:I

    .line 150
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 151
    move-object/from16 v0, p2

    iget v3, v0, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 221
    invoke-virtual/range {v20 .. v20}, Landroid/os/Message;->recycle()V

    .line 222
    const/16 v20, 0x0

    .line 223
    invoke-super/range {p0 .. p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 226
    :goto_0
    return-void

    .line 155
    :pswitch_0
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 156
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->requestInstantUpdate(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    goto :goto_0

    .line 162
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    .line 163
    .local v18, "params":Landroid/os/Bundle;
    new-instance v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;

    const-string v3, "int_TARGETDEVICEID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v3, "bool_updateVisitCount"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const-string v3, "bool_subscribeProgressUpdates"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IZZ)V

    .line 169
    .local v2, "req":Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 170
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 172
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;->processRequest()V

    goto :goto_0

    .line 177
    .end local v2    # "req":Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_DownloadData;
    .end local v18    # "params":Landroid/os/Bundle;
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    .line 180
    .restart local v18    # "params":Landroid/os/Bundle;
    iget v3, v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->pluginLibVersion:I

    if-nez v3, :cond_0

    .line 182
    const-string v3, "bundle_programmingData"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v19

    .line 183
    .local v19, "pgdd_b":Landroid/os/Bundle;
    invoke-static/range {v19 .. v19}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyGeocacheCompat$GeocacheDeviceDataCompat_v1;->readPgddFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    move-result-object v9

    .line 190
    .end local v19    # "pgdd_b":Landroid/os/Bundle;
    .local v9, "pgdd":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    :goto_1
    new-instance v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;

    const-string v3, "int_TARGETDEVICEID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v3, "long_ProgrammingPIN"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v3, "bool_clearAllExistingData"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    const-string v3, "bool_subscribeProgressUpdates"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IJZLcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Z)V

    .line 198
    .local v2, "req":Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 199
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 201
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->processRequest()V

    goto/16 :goto_0

    .line 187
    .end local v2    # "req":Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;
    .end local v9    # "pgdd":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    :cond_0
    const-string v3, "parcelable_ProgrammableGeocacheDeviceData"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    .restart local v9    # "pgdd":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    goto :goto_1

    .line 206
    .end local v9    # "pgdd":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    .end local v18    # "params":Landroid/os/Bundle;
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v18

    .line 207
    .restart local v18    # "params":Landroid/os/Bundle;
    new-instance v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;

    const-string v3, "int_TARGETDEVICEID"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    const-string v3, "int_nonce"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    const-string v3, "long_serialNumber"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v15

    const-string v3, "bool_subscribeProgressUpdates"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v17

    move-object v10, v2

    move-object/from16 v11, p0

    move-object v12, v4

    invoke-direct/range {v10 .. v17}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IIJZ)V

    .line 214
    .local v2, "req":Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 215
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 217
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->processRequest()V

    goto/16 :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x4e22
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 3
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 133
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v0

    return v0
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 141
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 142
    return-void
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v2, 0xc9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 123
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 124
    .local v0, "eventSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->updateDeviceList:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    return-object v0
.end method
