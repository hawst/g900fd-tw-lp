.class Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;
.super Landroid/os/Handler;
.source "ClientInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 403
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 404
    .local v0, "b":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 405
    const-string v4, "uuid_AccessToken"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/UUID;

    .line 407
    .local v2, "msguid":Ljava/util/UUID;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->uid:Ljava/util/UUID;

    invoke-virtual {v4, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$000(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 410
    .local v3, "reply":Landroid/os/Message;
    iget v4, p1, Landroid/os/Message;->what:I

    iput v4, v3, Landroid/os/Message;->what:I

    .line 411
    const/4 v4, -0x3

    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 414
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$100(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    .end local v3    # "reply":Landroid/os/Message;
    :cond_1
    :goto_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 422
    .restart local v3    # "reply":Landroid/os/Message;
    iget v4, p1, Landroid/os/Message;->what:I

    iput v4, v3, Landroid/os/Message;->what:I

    .line 424
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    .line 437
    const v4, -0x5f5e0ff

    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 443
    :goto_1
    :try_start_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$100(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 448
    :goto_2
    return-void

    .line 415
    :catch_0
    move-exception v1

    .line 417
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    move-result-object v4

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_0

    .line 427
    .end local v1    # "e":Landroid/os/RemoteException;
    :sswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mState_LOCK:Ljava/lang/Object;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$300(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 429
    :try_start_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    const/4 v6, 0x1

    # setter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mReleased:Z
    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$002(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;Z)Z

    .line 430
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 433
    :sswitch_1
    const/4 v4, 0x0

    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 434
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->USER_CANCELLED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getIntValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->onScanStopped(I)V

    goto :goto_1

    .line 430
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 444
    :catch_1
    move-exception v1

    .line 446
    .restart local v1    # "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->mService:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    move-result-object v4

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->removeClient(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V

    goto :goto_2

    .line 424
    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_0
        0x2775 -> :sswitch_1
    .end sparse-switch
.end method
