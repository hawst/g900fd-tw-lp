.class public Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;
.super Ljava/lang/Object;
.source "ScanController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    }
.end annotation


# static fields
.field private static final ANTPLUS_FREQ:I = 0x39

.field private static final SEARCH_PERIOD:I = 0x2000


# instance fields
.field private mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field private mExecutorDeathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

.field private final mReceivers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private mResultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

.field private final mState_LOCK:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    .line 160
    new-instance v0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutorDeathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

    .line 180
    new-instance v0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$2;-><init>(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mResultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    return-object p1
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public isStarted()Z
    .locals 2

    .prologue
    .line 126
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerReceiver(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;)V
    .locals 2
    .param p1, "receiver"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .prologue
    .line 140
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 143
    monitor-exit v1

    .line 144
    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public start(Lcom/dsi/ant/channel/AntChannel;)I
    .locals 10
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannel;

    .prologue
    const/4 v8, 0x0

    .line 76
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v9

    .line 78
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v8

    .line 101
    :goto_0
    return v1

    .line 85
    :cond_0
    :try_start_1
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutorDeathHandler:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;

    invoke-direct {v1, p1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :try_start_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    const/16 v1, 0x39

    const/16 v2, 0x2000

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mResultReceiver:Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;-><init>(IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 99
    .local v0, "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 101
    monitor-exit v9

    move v1, v8

    goto :goto_0

    .line 86
    .end local v0    # "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    :catch_0
    move-exception v7

    .line 88
    .local v7, "e":Landroid/os/RemoteException;
    const/4 v1, -0x4

    monitor-exit v9

    goto :goto_0

    .line 102
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 110
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 117
    :cond_0
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterReceiver(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;)V
    .locals 2
    .param p1, "receiver"    # Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .prologue
    .line 154
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 157
    monitor-exit v1

    .line 158
    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
