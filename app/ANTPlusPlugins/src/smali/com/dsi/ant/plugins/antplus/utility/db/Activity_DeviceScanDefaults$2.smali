.class Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;
.super Ljava/lang/Object;
.source "Activity_DeviceScanDefaults.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->refreshProximityUI(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

.field final synthetic val$proxBin:I


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;I)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->val$proxBin:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->val$proxBin:I

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    const-string v1, "Proximity Threshold:  OFF"

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 102
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Proximity Threshold:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;->val$proxBin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
