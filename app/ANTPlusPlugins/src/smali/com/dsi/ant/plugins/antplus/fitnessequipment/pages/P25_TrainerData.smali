.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P25_TrainerData.java"


# static fields
.field private static final ACCUMULATOR_TIMEOUT:I = 0x2ee0


# instance fields
.field private accumPower:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private statusEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private tnrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private updateEventCount:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V
    .locals 3
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;
    .param p2, "powerDecoder"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .prologue
    const/16 v2, 0x2ee0

    .line 33
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->tnrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->statusEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const/16 v1, 0xff

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->updateEventCount:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 30
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const v1, 0xffff

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->accumPower:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 34
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 35
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 36
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 15
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 57
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 59
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v12

    .line 60
    .local v12, "rawAccumPower":I
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v13

    .line 61
    .local v13, "rawUpdateEventCount":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->updateEventCount:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v13, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 63
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    and-int/lit16 v10, v2, 0xfff

    .line 65
    .local v10, "instantPower":I
    const/16 v2, 0xfff

    if-eq v10, v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->accumPower:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v12, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 68
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->updateEventCount:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->accumPower:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    invoke-virtual/range {v2 .. v8}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->decodePwrOnlyPage(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->tnrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v9, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 75
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 77
    const-string v2, "long_updateEventCount"

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->updateEventCount:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v3

    invoke-virtual {v9, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 79
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 80
    .local v11, "instantaneousCadence":I
    const-string v2, "int_instantaneousCadence"

    const/16 v3, 0xff

    if-eq v11, v3, :cond_8

    .end local v11    # "instantaneousCadence":I
    :goto_0
    invoke-virtual {v9, v2, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v4, "long_accumulatedPower"

    const/16 v2, 0xfff

    if-eq v10, v2, :cond_9

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->accumPower:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v2

    :goto_1
    invoke-virtual {v9, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 83
    const-string v2, "int_instantaneousPower"

    const/16 v3, 0xfff

    if-eq v10, v3, :cond_a

    .end local v10    # "instantPower":I
    :goto_2
    invoke-virtual {v9, v2, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->tnrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 88
    .end local v9    # "b":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->statusEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 90
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 91
    .restart local v9    # "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 94
    const-class v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v14

    .line 96
    .local v14, "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 97
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->BICYCLE_POWER_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v14, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_2
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_3

    .line 100
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->RESISTANCE_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v14, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_3
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_4

    .line 103
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->USER_CONFIGURATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v14, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_4
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    .line 106
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MAXIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v14, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_5
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_6

    .line 109
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MINIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v14, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_6
    const-string v2, "long_trainerStatusFlags"

    invoke-static {v14}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->getLong(Ljava/util/EnumSet;)J

    move-result-wide v3

    invoke-virtual {v9, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 113
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->statusEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 115
    .end local v9    # "b":Landroid/os/Bundle;
    .end local v14    # "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    :cond_7
    return-void

    .line 80
    .restart local v9    # "b":Landroid/os/Bundle;
    .restart local v10    # "instantPower":I
    .restart local v11    # "instantaneousCadence":I
    :cond_8
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 82
    .end local v11    # "instantaneousCadence":I
    :cond_9
    const-wide/16 v2, -0x1

    goto/16 :goto_1

    .line 83
    :cond_a
    const/4 v10, -0x1

    goto/16 :goto_2
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->tnrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->statusEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P25_TrainerData;->powerDecoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 45
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
