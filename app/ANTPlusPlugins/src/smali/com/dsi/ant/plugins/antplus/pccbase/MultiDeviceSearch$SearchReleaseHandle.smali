.class Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;
.super Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
.source "MultiDeviceSearch.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchReleaseHandle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;",
        ">;",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;",
        ">;",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;"
    }
.end annotation


# instance fields
.field protected successReceived:Z

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V
    .locals 1
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 452
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;>;"
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .line 453
    invoke-direct {p0, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V

    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->successReceived:Z

    .line 454
    return-void
.end method


# virtual methods
.method protected isActive()Z
    .locals 2

    .prologue
    .line 466
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 468
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->isClosed:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->resultSent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->successReceived:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 2
    .param p1, "newDeviceState"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    .prologue
    const/4 v1, 0x0

    .line 476
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->DEAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    if-ne p1, v0, :cond_0

    .line 478
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v1, v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 480
    :cond_0
    return-void
.end method

.method public bridge synthetic onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .param p2, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    .param p3, "x2"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    .prologue
    .line 442
    check-cast p1, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    .end local p1    # "x0":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-virtual {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    return-void
.end method

.method public onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 2
    .param p1, "result"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;
    .param p2, "resultCode"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    .param p3, "initialDeviceState"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    .prologue
    .line 486
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 488
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->resultSink:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    invoke-interface {v0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 490
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    if-ne p2, v0, :cond_0

    .line 493
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->resultSent:Z

    .line 495
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->successReceived:Z

    .line 497
    :cond_0
    monitor-exit v1

    .line 498
    return-void

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected requestCancelled()V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$700(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->closePluginConnection()V

    .line 460
    return-void
.end method
