.class public Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "ChannelTask_CheckAndRequestAntFs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private antfsSupport:Z

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private msgsSinceFirstPageRequest:I

.field private requestAntFsSessionCmd:[B

.field private requestSent:Z

.field private scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

.field private seenCapabilities:Z

.field private seenLinkBeacon:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V
    .locals 1
    .param p1, "scaleDevice"    # Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 29
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestSent:Z

    .line 30
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenCapabilities:Z

    .line 31
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    .line 37
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestAntFsSessionCmd:[B

    .line 52
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    .line 53
    return-void

    .line 37
    nop

    :array_0
    .array-data 1
        0x46t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x43t
        0x2t
    .end array-data
.end method


# virtual methods
.method public doWork()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 182
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_0

    .line 184
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 270
    :goto_0
    return-void

    .line 189
    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 190
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->enableMessageProcessing()V

    .line 193
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 195
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenCapabilities:Z

    if-eqz v1, :cond_3

    .line 197
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->antfsSupport:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestSent:Z
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_2

    .line 201
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestAntFsSessionCmd:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 239
    :goto_1
    :try_start_2
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 240
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->enableMessageProcessing()V

    .line 243
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0xf

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 245
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 247
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    if-eqz v1, :cond_5

    .line 249
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Successfully entered ANT-FS mode"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE occurred: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 203
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 205
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_3
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_1

    .line 209
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Failed to send ANT-FS session request: TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 264
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_2
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 268
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 213
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_1
    :try_start_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send ANT-FS session request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 221
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Device does not support ANT-FS"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 226
    :cond_3
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    if-eqz v1, :cond_4

    .line 228
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Device is already in ANT-FS mode"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 234
    :cond_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Timed out waiting for capabilities"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_0

    .line 254
    :cond_5
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v2, "Timed out waiting for link beacon"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    const-string v0, "ANT-FS Session Request"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 281
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 284
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x43

    const/4 v8, 0x1

    .line 59
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 63
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 73
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 76
    :pswitch_3
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestSent:Z

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 85
    :pswitch_4
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v1, v1, 0x3

    rsub-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 95
    :pswitch_5
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenCapabilities:Z

    if-nez v0, :cond_2

    .line 97
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v8

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 98
    .local v7, "pageNumber":I
    if-ne v7, v8, :cond_1

    .line 100
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "Received weight page"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 102
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleAntFs()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->antfsSupport:Z

    .line 103
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenCapabilities:Z

    .line 104
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 105
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 107
    :cond_1
    if-ne v7, v2, :cond_0

    .line 109
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "Received beacon"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    .line 111
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 112
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 117
    .end local v7    # "pageNumber":I
    :cond_2
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestSent:Z

    if-nez v0, :cond_5

    .line 119
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    .line 120
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_3

    .line 122
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "Tx retries exceeded sending ANT-FS Session request"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 124
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 129
    :cond_3
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestAntFsSessionCmd:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 135
    :catch_0
    move-exception v6

    .line 137
    .local v6, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v0, v1, :cond_4

    .line 141
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    const-string v1, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 145
    :cond_4
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACFE handling message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 147
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 156
    .end local v6    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v8

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 159
    .restart local v7    # "pageNumber":I
    if-ne v7, v2, :cond_6

    .line 161
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->seenLinkBeacon:Z

    .line 162
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->disableMessageProcessing()V

    .line 163
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 166
    :cond_6
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    .line 167
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->msgsSinceFirstPageRequest:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;->requestAntFsSessionCmd:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto/16 :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 63
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
