.class public Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
.super Ljava/lang/Object;
.source "ConnectedDeviceInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

.field private static p81_prodInfo:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

.field private static p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;


# instance fields
.field public deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

.field hintStringBuf:Ljava/lang/StringBuilder;

.field lastHintStringPage:I

.field private lastSeenTime:J

.field pagesDownloaded:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private visitCountPageNum:I

.field private visitCountUpdated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p81_prodInfo:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    .line 28
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "deviceID"    # I

    .prologue
    const/4 v1, -0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountUpdated:Z

    .line 37
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountPageNum:I

    .line 40
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastHintStringPage:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->hintStringBuf:Ljava/lang/StringBuilder;

    .line 45
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    .line 46
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iput p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    .line 47
    return-void
.end method

.method private decodeProgrammablePage(ILcom/dsi/ant/message/ipc/AntMessageParcel;)Z
    .locals 13
    .param p1, "pageNum"    # I
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 163
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x2

    aget-byte v7, v9, v10

    .line 164
    .local v7, "progPageType":I
    packed-switch v7, :pswitch_data_0

    .line 232
    :pswitch_0
    sget-object v9, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown programmed page type received: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_0
    :goto_0
    const/4 v9, 0x1

    :goto_1
    return v9

    .line 168
    :pswitch_1
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom4LeBytes([BI)I

    move-result v3

    .line 169
    .local v3, "lat_semicircles":I
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    new-instance v10, Ljava/math/BigDecimal;

    invoke-direct {v10, v3}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v11, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

    invoke-virtual {v10, v11}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v10

    const/16 v11, 0x9

    sget-object v12, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v10, v11, v12}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v10

    iput-object v10, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    goto :goto_0

    .line 176
    .end local v3    # "lat_semicircles":I
    :pswitch_2
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->SignedNumFrom4LeBytes([BI)I

    move-result v4

    .line 177
    .local v4, "long_semicircles":I
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    new-instance v10, Ljava/math/BigDecimal;

    invoke-direct {v10, v4}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v11, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->SEMICIRCLE_MULTIPLIER:Ljava/math/BigDecimal;

    invoke-virtual {v10, v11}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v10

    const/16 v11, 0x9

    sget-object v12, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v10, v11, v12}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v10

    iput-object v10, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    goto :goto_0

    .line 185
    .end local v4    # "long_semicircles":I
    :pswitch_3
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    add-int/lit8 v10, p1, -0x1

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    .line 186
    .local v6, "pageBefore":Ljava/lang/Boolean;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1

    iget v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastHintStringPage:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_2

    iget v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastHintStringPage:I

    add-int/lit8 v9, v9, 0x1

    if-eq p1, v9, :cond_2

    .line 189
    :cond_1
    sget-object v9, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Decoder "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v11, v11, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - out of order hint string page not decoding...lhspg: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastHintStringPage:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", pageNum"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", pbf"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", p5"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 193
    :cond_2
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x1

    aget-byte v9, v9, v10

    iput v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastHintStringPage:I

    .line 195
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->hintStringBuf:Ljava/lang/StringBuilder;

    if-nez v9, :cond_3

    .line 196
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->hintStringBuf:Ljava/lang/StringBuilder;

    .line 199
    :cond_3
    const/4 v2, 0x2

    .local v2, "i":I
    :goto_2
    const/16 v9, 0x8

    if-ge v2, v9, :cond_0

    .line 201
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    add-int/lit8 v10, v2, 0x1

    aget-byte v9, v9, v10

    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v9

    int-to-char v5, v9

    .line 202
    .local v5, "nextChar":C
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->hintStringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    if-nez v5, :cond_4

    .line 205
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->hintStringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    goto/16 :goto_0

    .line 199
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 215
    .end local v2    # "i":I
    .end local v5    # "nextChar":C
    .end local v6    # "pageBefore":Ljava/lang/Boolean;
    :pswitch_4
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountPageNum:I

    .line 216
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v0

    .line 217
    .local v0, "garminTime":J
    new-instance v8, Ljava/util/GregorianCalendar;

    const-string v9, "UTC"

    invoke-static {v9}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 218
    .local v8, "realTime":Ljava/util/GregorianCalendar;
    const-wide/16 v9, 0x3e8

    mul-long/2addr v9, v0

    const-wide v11, 0x92ee70e000L

    add-long/2addr v9, v11

    invoke-virtual {v8, v9, v10}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 220
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iput-object v8, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    .line 221
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x7

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 227
    .end local v0    # "garminTime":J
    .end local v8    # "realTime":Ljava/util/GregorianCalendar;
    :pswitch_5
    sget-object v9, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Detected device broadcasting the invalid page type within the valid page range on page "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v11

    const/4 v12, 0x1

    aget-byte v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public DecodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 11
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x7

    const/4 v6, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastSeenTime:J

    .line 58
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v8

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    .line 61
    .local v3, "pageNum":I
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    sparse-switch v3, :sswitch_data_0

    .line 145
    if-le v3, v8, :cond_7

    const/16 v4, 0x1f

    if-ge v3, v4, :cond_7

    .line 147
    invoke-direct {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->decodeProgrammablePage(ILcom/dsi/ant/message/ipc/AntMessageParcel;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 157
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 70
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v1, "id":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v10

    shr-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3f

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v10

    shl-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0x30

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    aget-byte v5, v5, v6

    shr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v6

    shl-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3c

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    shr-int/lit8 v5, v5, 0x6

    and-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x4

    aget-byte v4, v4, v5

    shr-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0x3f

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x5

    aget-byte v4, v4, v5

    shr-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3f

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x5

    aget-byte v4, v4, v5

    shl-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0x30

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x6

    aget-byte v5, v5, v6

    shr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x6

    aget-byte v4, v4, v5

    shl-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3c

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    aget-byte v5, v5, v9

    shr-int/lit8 v5, v5, 0x6

    and-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v9

    shr-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0x3f

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/16 v5, 0x8

    aget-byte v4, v4, v5

    shr-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3f

    add-int/lit8 v4, v4, 0x20

    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    .line 87
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got page 0, string ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 91
    .end local v1    # "id":Ljava/lang/StringBuilder;
    :sswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    .line 94
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v9

    and-int/lit16 v2, v4, 0xff

    .line 95
    .local v2, "numProgPages":I
    const/16 v4, 0xff

    if-eq v2, v4, :cond_2

    .line 99
    const/16 v4, 0x20

    if-le v2, v4, :cond_3

    .line 101
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    const-string v5, "Device reported more programmable pages than 32, ignoring."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/16 v2, 0x20

    .line 105
    :cond_3
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_2
    if-ge v0, v2, :cond_5

    .line 108
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_4

    .line 109
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 113
    :cond_5
    const/16 v0, 0x50

    :goto_3
    const/16 v4, 0x53

    if-ge v0, v4, :cond_2

    .line 116
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_6

    .line 117
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 113
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 123
    .end local v0    # "i":I
    .end local v2    # "numProgPages":I
    :sswitch_2
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    invoke-virtual {v4, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 124
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;->manufacturerID:I

    iput v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->manufacturerID:I

    .line 125
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;->hardwareRevision:I

    iput v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->hardwareRevision:I

    .line 126
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p80_manfId:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P80_ManufacturerIdentification$P80_ManufacturerIdentification_CachedDecoder;->modelNumber:I

    iput v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->modelNumber:I

    goto/16 :goto_1

    .line 130
    :sswitch_3
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p81_prodInfo:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    invoke-virtual {v4, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 131
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p81_prodInfo:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    iget-wide v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->serialNumber:J

    iput-wide v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->serialNumber:J

    .line 133
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p81_prodInfo:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P81_ProductInformation$P81_ProductInformation_CachedDecoder;->mainSoftwareRevision:I

    iput v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->softwareRevision:I

    goto/16 :goto_1

    .line 137
    :sswitch_4
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    invoke-virtual {v4, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 138
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryStatusCode:I

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    move-result-object v5

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->batteryStatus:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 139
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryVoltage:Ljava/math/BigDecimal;

    iput-object v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->batteryVoltage:Ljava/math/BigDecimal;

    .line 140
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget-wide v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTime:J

    iput-wide v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->cumulativeOperatingTime:J

    .line 141
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->p82_battStatus:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTimeResolution:I

    iput v5, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->cumulativeOperatingTimeResolution:I

    goto/16 :goto_1

    .line 152
    :cond_7
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown page received, page "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x50 -> :sswitch_2
        0x51 -> :sswitch_3
        0x52 -> :sswitch_4
    .end sparse-switch
.end method

.method public composePageRequest(I)[B
    .locals 5
    .param p1, "pageNum"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 245
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 246
    .local v0, "txBuffer":[B
    const/4 v1, 0x0

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    .line 247
    aput-byte v3, v0, v4

    .line 248
    const/4 v1, 0x2

    aput-byte v3, v0, v1

    .line 249
    const/4 v1, 0x3

    aput-byte v3, v0, v1

    .line 250
    const/4 v1, 0x4

    aput-byte v3, v0, v1

    .line 251
    const/4 v1, 0x5

    const/16 v2, 0xf

    aput-byte v2, v0, v1

    .line 252
    const/4 v1, 0x6

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 253
    const/4 v1, 0x7

    aput-byte v4, v0, v1

    .line 255
    return-object v0
.end method

.method public getNextMissingPageNum()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 275
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 292
    :goto_0
    return v1

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v3

    .line 281
    goto :goto_0

    .line 285
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 287
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 288
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    goto :goto_0

    .line 285
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 292
    :cond_3
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getTimeSinceLastSeen()J
    .locals 4

    .prologue
    .line 51
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->lastSeenTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getTotalPageCount()I
    .locals 3

    .prologue
    .line 297
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    const/4 v0, -0x1

    .line 300
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getVisitCountPageNum()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountPageNum:I

    return v0
.end method

.method public isVisitCountUpdated()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountUpdated:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v0

    if-gez v0, :cond_1

    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountPageNum:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pagesDownloaded()I
    .locals 3

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 306
    .local v0, "dlTotal":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 308
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->pagesDownloaded:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    add-int/lit8 v0, v0, 0x1

    .line 306
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 311
    :cond_1
    return v0
.end method

.method public reportVisitCountUpdated([B)V
    .locals 7
    .param p1, "programmingPayload"    # [B

    .prologue
    .line 325
    const/4 v3, 0x2

    invoke-static {p1, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v0

    .line 326
    .local v0, "garminTime":J
    new-instance v2, Ljava/util/GregorianCalendar;

    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 327
    .local v2, "realTime":Ljava/util/GregorianCalendar;
    const-wide/16 v3, 0x3e8

    mul-long/2addr v3, v0

    const-wide v5, 0x92ee70e000L

    add-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 329
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iput-object v2, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    .line 330
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    const/4 v4, 0x6

    invoke-static {p1, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    .line 332
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->TAG:Ljava/lang/String;

    const-string v4, "Updated visit count"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountUpdated:Z

    .line 334
    return-void
.end method

.method public setVisitCountPageNum(I)V
    .locals 0
    .param p1, "visitCountPageNum"    # I

    .prologue
    .line 343
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->visitCountPageNum:I

    .line 344
    return-void
.end method
