.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 0

    .prologue
    .line 817
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 11
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    const/4 v10, 0x0

    .line 821
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Async Search rcv result: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    sparse-switch p1, :sswitch_data_0

    .line 865
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Search reported failure: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 867
    .local v3, "failMsg":Landroid/os/Message;
    const/16 v7, -0x64

    iput v7, v3, Landroid/os/Message;->what:I

    .line 868
    iput p1, v3, Landroid/os/Message;->arg1:I

    .line 871
    :try_start_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    .line 879
    .end local v3    # "failMsg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 826
    :sswitch_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v8

    invoke-virtual {v7, v8, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v7

    if-nez v7, :cond_0

    .line 829
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 830
    .local v5, "resultMsg":Landroid/os/Message;
    const/4 v7, 0x2

    iput v7, v5, Landroid/os/Message;->what:I

    .line 831
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 832
    .local v0, "b":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v8

    invoke-virtual {v7, v8, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v1

    .line 833
    .local v1, "dbResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    const/4 v7, 0x0

    invoke-direct {v4, v10, v1, v7}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;-><init>(Ljava/util/UUID;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    .line 837
    .local v4, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    const-string v7, "parcelable_AsyncScanResultDeviceInfo"

    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 838
    invoke-virtual {v5, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 841
    :try_start_1
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v5}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 842
    :catch_0
    move-exception v2

    .line 844
    .local v2, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "RemoteException sending async scan result, closing scan."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v7, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 851
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "dbResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v2    # "e":Landroid/os/RemoteException;
    .end local v4    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    .end local v5    # "resultMsg":Landroid/os/Message;
    :sswitch_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 852
    .local v6, "timeoutMsg":Landroid/os/Message;
    const/4 v7, -0x7

    iput v7, v6, Landroid/os/Message;->what:I

    .line 855
    :try_start_2
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 856
    :catch_1
    move-exception v2

    .line 858
    .restart local v2    # "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "RemoteException sending async scan timeout ping, closing scan."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v7, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 872
    .end local v2    # "e":Landroid/os/RemoteException;
    .end local v6    # "timeoutMsg":Landroid/os/Message;
    .restart local v3    # "failMsg":Landroid/os/Message;
    :catch_2
    move-exception v2

    .line 874
    .restart local v2    # "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "RemoteException sending async scan result, closing scan."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v8, v8, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v7, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 822
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0x9 -> :sswitch_0
    .end sparse-switch
.end method
