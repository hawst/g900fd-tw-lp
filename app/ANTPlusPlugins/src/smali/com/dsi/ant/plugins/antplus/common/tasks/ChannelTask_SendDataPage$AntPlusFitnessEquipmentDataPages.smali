.class public Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusFitnessEquipmentDataPages;
.super Ljava/lang/Object;
.source "ChannelTask_SendDataPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AntPlusFitnessEquipmentDataPages"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRequestCalibration(ZZ)[B
    .locals 3
    .param p0, "requestZeroOffsetCalibration"    # Z
    .param p1, "requestSpinDownCalibration"    # Z

    .prologue
    const/4 v2, 0x1

    .line 224
    const/16 v1, 0x8

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 226
    .local v0, "messagePayload":[B
    if-eqz p0, :cond_0

    .line 227
    aget-byte v1, v0, v2

    or-int/lit8 v1, v1, 0x40

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    .line 229
    :cond_0
    if-eqz p1, :cond_1

    .line 230
    aget-byte v1, v0, v2

    or-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    .line 232
    :cond_1
    return-object v0

    .line 224
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method public static getSetBasicResistance(Ljava/math/BigDecimal;)[B
    .locals 6
    .param p0, "totalResistance"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 119
    const/16 v2, 0x8

    new-array v1, v2, [B

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->BASIC_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    const/4 v2, 0x1

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    aput-byte v3, v1, v2

    const/4 v2, 0x3

    aput-byte v3, v1, v2

    const/4 v2, 0x4

    aput-byte v3, v1, v2

    const/4 v2, 0x5

    aput-byte v3, v1, v2

    const/4 v2, 0x6

    aput-byte v3, v1, v2

    aput-byte v3, v1, v5

    .line 121
    .local v1, "messagePayload":[B
    new-instance v2, Ljava/math/BigDecimal;

    const-string v3, "2"

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v4, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->byteValue()B

    move-result v0

    .line 122
    .local v0, "byteTotalResistance":B
    aput-byte v0, v1, v5

    .line 123
    return-object v1
.end method

.method public static getSetTargetPower(Ljava/math/BigDecimal;)[B
    .locals 6
    .param p0, "targetPower"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 128
    const/16 v2, 0x8

    new-array v1, v2, [B

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TARGET_POWER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    const/4 v2, 0x1

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    aput-byte v3, v1, v2

    const/4 v2, 0x3

    aput-byte v3, v1, v2

    const/4 v2, 0x4

    aput-byte v3, v1, v2

    const/4 v2, 0x5

    aput-byte v3, v1, v2

    aput-byte v4, v1, v5

    const/4 v2, 0x7

    aput-byte v4, v1, v2

    .line 130
    .local v1, "messagePayload":[B
    new-instance v2, Ljava/math/BigDecimal;

    const-string v3, "4"

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v4, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    .line 131
    .local v0, "intTargetPower":I
    invoke-static {v1, v5, v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 132
    return-object v1
.end method

.method public static getSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)[B
    .locals 9
    .param p0, "grade"    # Ljava/math/BigDecimal;
    .param p1, "rollingResistanceCoeff"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v4, -0x1

    .line 164
    const/16 v3, 0x8

    new-array v2, v3, [B

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TRACK_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v2, v6

    const/4 v3, 0x1

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    aput-byte v4, v2, v3

    aput-byte v4, v2, v7

    const/4 v3, 0x6

    aput-byte v4, v2, v3

    aput-byte v4, v2, v8

    .line 169
    .local v2, "messagePayload":[B
    invoke-virtual {p0}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    const v4, 0xffff

    if-eq v3, v4, :cond_0

    .line 170
    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0xc8

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {p0, v3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "100"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v6, v4}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValue()I

    move-result v1

    .line 174
    .local v1, "intGrade":I
    :goto_0
    invoke-virtual {p1}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    const/16 v4, 0xff

    if-eq v3, v4, :cond_1

    .line 175
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "20000"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v6, v4}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->byteValue()B

    move-result v0

    .line 179
    .local v0, "byteRollingResistanceCoeff":B
    :goto_1
    invoke-static {v2, v7, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 180
    aput-byte v0, v2, v8

    .line 182
    return-object v2

    .line 172
    .end local v0    # "byteRollingResistanceCoeff":B
    .end local v1    # "intGrade":I
    :cond_0
    const v1, 0xffff

    .restart local v1    # "intGrade":I
    goto :goto_0

    .line 177
    :cond_1
    const/4 v0, -0x1

    .restart local v0    # "byteRollingResistanceCoeff":B
    goto :goto_1
.end method

.method public static getSetUserConfiguration(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)[B
    .locals 8
    .param p0, "userWeight"    # Ljava/math/BigDecimal;
    .param p1, "bicycleWeight"    # Ljava/math/BigDecimal;
    .param p2, "bicycleWheelDiameter"    # Ljava/math/BigDecimal;
    .param p3, "gearRatio"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v7, 0x0

    .line 187
    const/16 v5, 0x8

    new-array v4, v5, [B

    fill-array-data v4, :array_0

    .line 193
    .local v4, "messagePayload":[B
    invoke-virtual {p0}, Ljava/math/BigDecimal;->intValue()I

    move-result v5

    const v6, 0xffff

    if-eq v5, v6, :cond_0

    .line 194
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "100"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v7, v6}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    .line 198
    .local v3, "intUserWeight":I
    :goto_0
    invoke-virtual {p1}, Ljava/math/BigDecimal;->intValue()I

    move-result v5

    const/16 v6, 0xfff

    if-eq v5, v6, :cond_1

    .line 199
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "20"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v7, v6}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    .line 204
    .local v2, "intBicycleWeight":I
    :goto_1
    invoke-virtual {p2}, Ljava/math/BigDecimal;->intValue()I

    move-result v5

    const/16 v6, 0xff

    if-eq v5, v6, :cond_2

    .line 205
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "100"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v5, v7, v6}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->byteValue()B

    move-result v0

    .line 209
    .local v0, "byteBicycleWheelDiameter":B
    :goto_2
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v5

    if-eqz v5, :cond_3

    .line 210
    new-instance v5, Ljava/math/BigDecimal;

    const-string v6, "0.03"

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {p3, v5, v7, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->byteValue()B

    move-result v1

    .line 214
    .local v1, "byteGearRatio":B
    :goto_3
    const/4 v5, 0x1

    invoke-static {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 215
    const/4 v5, 0x4

    invoke-static {v4, v5, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumInUpper1And1HalfLeBytes([BII)V

    .line 216
    const/4 v5, 0x6

    aput-byte v0, v4, v5

    .line 217
    const/4 v5, 0x7

    aput-byte v1, v4, v5

    .line 219
    return-object v4

    .line 196
    .end local v0    # "byteBicycleWheelDiameter":B
    .end local v1    # "byteGearRatio":B
    .end local v2    # "intBicycleWeight":I
    .end local v3    # "intUserWeight":I
    :cond_0
    const v3, 0xffff

    .restart local v3    # "intUserWeight":I
    goto :goto_0

    .line 202
    :cond_1
    const/16 v2, 0xfff

    .restart local v2    # "intBicycleWeight":I
    goto :goto_1

    .line 207
    :cond_2
    const/4 v0, -0x1

    .restart local v0    # "byteBicycleWheelDiameter":B
    goto :goto_2

    .line 212
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "byteGearRatio":B
    goto :goto_3

    .line 187
    :array_0
    .array-data 1
        0x37t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
    .end array-data
.end method

.method public static getSetWindResistance(Ljava/math/BigDecimal;BLjava/math/BigDecimal;)[B
    .locals 10
    .param p0, "windResistanceCoeff"    # Ljava/math/BigDecimal;
    .param p1, "windSpeed"    # B
    .param p2, "draftingFactor"    # Ljava/math/BigDecimal;

    .prologue
    const/4 v9, 0x7

    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 137
    const/16 v3, 0x8

    new-array v2, v3, [B

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->WIND_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v2, v6

    const/4 v3, 0x1

    aput-byte v5, v2, v3

    const/4 v3, 0x2

    aput-byte v5, v2, v3

    const/4 v3, 0x3

    aput-byte v5, v2, v3

    const/4 v3, 0x4

    aput-byte v5, v2, v3

    aput-byte v5, v2, v7

    aput-byte v5, v2, v8

    aput-byte v5, v2, v9

    .line 142
    .local v2, "messagePayload":[B
    invoke-virtual {p0}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    const/16 v4, 0xff

    if-eq v3, v4, :cond_1

    .line 143
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "100"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v6, v4}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->byteValue()B

    move-result v1

    .line 147
    .local v1, "byteWindResistanceCoeff":B
    :goto_0
    invoke-virtual {p2}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    const/16 v4, 0xff

    if-eq v3, v4, :cond_2

    .line 148
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "100"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v6, v4}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->byteValue()B

    move-result v0

    .line 152
    .local v0, "byteDraftingFactor":B
    :goto_1
    if-eq p1, v5, :cond_0

    .line 153
    add-int/lit8 v3, p1, 0x7f

    int-to-byte p1, v3

    .line 155
    :cond_0
    aput-byte v1, v2, v7

    .line 156
    aput-byte p1, v2, v8

    .line 157
    aput-byte v0, v2, v9

    .line 159
    return-object v2

    .line 145
    .end local v0    # "byteDraftingFactor":B
    .end local v1    # "byteWindResistanceCoeff":B
    :cond_1
    const/4 v1, -0x1

    .restart local v1    # "byteWindResistanceCoeff":B
    goto :goto_0

    .line 150
    :cond_2
    const/4 v0, -0x1

    .restart local v0    # "byteDraftingFactor":B
    goto :goto_1
.end method
