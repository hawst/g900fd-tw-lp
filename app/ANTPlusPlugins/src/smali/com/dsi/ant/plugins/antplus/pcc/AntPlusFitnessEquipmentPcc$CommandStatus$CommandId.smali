.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
.super Ljava/lang/Enum;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommandId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum BASIC_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum NO_CONTROL_PAGE_RECEIVED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum TARGET_POWER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum TRACK_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

.field public static final enum WIND_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 531
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "BASIC_RESISTANCE"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->BASIC_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 536
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "TARGET_POWER"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TARGET_POWER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 541
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "WIND_RESISTANCE"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->WIND_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 546
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "TRACK_RESISTANCE"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TRACK_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 551
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "NO_CONTROL_PAGE_RECEIVED"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->NO_CONTROL_PAGE_RECEIVED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 556
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 526
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->BASIC_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TARGET_POWER:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->WIND_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->TRACK_RESISTANCE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->NO_CONTROL_PAGE_RECEIVED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 562
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->intValue:I

    .line 563
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 581
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 583
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 589
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    :goto_1
    return-object v3

    .line 581
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 587
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    .line 588
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->intValue:I

    move-object v3, v4

    .line 589
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 526
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 571
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CommandStatus$CommandId;->intValue:I

    return v0
.end method
