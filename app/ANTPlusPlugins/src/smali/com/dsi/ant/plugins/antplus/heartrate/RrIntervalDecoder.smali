.class public Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "RrIntervalDecoder.java"


# instance fields
.field private mCachedPreviousEventTime:J

.field private mCurrentEventTime:J

.field private mCurrentHeartBeatCount:J

.field private mPreviousEventTime:J

.field private rrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 19
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->rrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 25
    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 27
    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    .line 29
    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    .line 31
    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    return-void
.end method

.method private calculateRR(JJ)Ljava/math/BigDecimal;
    .locals 6
    .param p1, "previousEventTime"    # J
    .param p3, "currentEventTime"    # J

    .prologue
    .line 157
    sub-long v1, p3, p1

    .line 158
    .local v1, "ulRrInterval_1024ths":J
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    .line 159
    .local v0, "result":Ljava/math/BigDecimal;
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "1000"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    .line 160
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "1024"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x7

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    .line 161
    return-object v0
.end method

.method private decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V
    .locals 3
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "previousEventTime"    # J
    .param p7, "currentEventTime"    # J
    .param p9, "flag"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    .prologue
    .line 139
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->rrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 142
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 145
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 147
    const-string v1, "decimal_calculatedRrInterval"

    invoke-direct {p0, p5, p6, p7, p8}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->calculateRR(JJ)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 150
    const-string v1, "int_rrFlag"

    invoke-virtual {p9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->getIntValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->rrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public decodeDefaultPageAccumulators(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Z)V
    .locals 14
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "heartBeatCount"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p6, "eventTime_1024ths"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .param p7, "zeroDetected"    # Z

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 61
    invoke-virtual/range {p6 .. p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 62
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    .line 124
    :goto_0
    return-void

    .line 65
    :cond_0
    if-eqz p7, :cond_1

    .line 68
    const-wide/16 v5, -0x1

    const-wide/16 v7, 0x1

    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->HEART_RATE_ZERO_DETECTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v12

    .line 74
    .local v12, "newBeatCount":J
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    cmp-long v0, v0, v12

    if-lez v0, :cond_3

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 78
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    .line 79
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    .line 80
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    .line 123
    :cond_2
    :goto_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    goto :goto_0

    .line 82
    :cond_3
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    cmp-long v0, v0, v12

    if-eqz v0, :cond_2

    .line 86
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    cmp-long v0, v0, v12

    if-nez v0, :cond_5

    .line 89
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    .line 90
    invoke-virtual/range {p6 .. p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 91
    iput-wide v12, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    .line 93
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 94
    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    iget-wide v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->DATA_SOURCE_CACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    goto :goto_1

    .line 97
    :cond_4
    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    iget-wide v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->DATA_SOURCE_PAGE_4:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    goto :goto_1

    .line 100
    :cond_5
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    cmp-long v0, v0, v12

    if-gez v0, :cond_2

    .line 103
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    .line 104
    invoke-virtual/range {p6 .. p6}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 106
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 108
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    sub-long v2, v12, v2

    div-long v10, v0, v2

    .line 110
    .local v10, "mAverageIncrement":J
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    sub-long v5, v0, v10

    iget-wide v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->DATA_SOURCE_AVERAGED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    .line 119
    .end local v10    # "mAverageIncrement":J
    :goto_2
    iput-wide v12, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    goto :goto_1

    .line 115
    :cond_6
    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    iget-wide v7, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->DATA_SOURCE_PAGE_4:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->decodeAndFireEvent(JJJJLcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    goto :goto_2
.end method

.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 0
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 168
    return-void
.end method

.method public decodePageFourAccumulators(JJLcom/dsi/ant/plugins/antplus/common/pages/Accumulator;)V
    .locals 2
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "previousEventTime_1024ths"    # Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .prologue
    .line 133
    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    .line 134
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->rrEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 174
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentEventTime:J

    .line 175
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mPreviousEventTime:J

    .line 176
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCachedPreviousEventTime:J

    .line 177
    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;->mCurrentHeartBeatCount:J

    .line 178
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 179
    return-void
.end method
