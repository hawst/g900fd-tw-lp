.class Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;
.source "WeightScaleDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeightScaleDownloadAllController"
.end annotation


# instance fields
.field private client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 1
    .param p2, "useProgressUpdates"    # Z
    .param p3, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->this$0:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    .line 200
    const/16 v0, 0xcc

    invoke-direct {p0, v0, p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;-><init>(IZ)V

    .line 201
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 202
    return-void
.end method


# virtual methods
.method public getStartAntFsModeTask()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .locals 3

    .prologue
    .line 213
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->this$0:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->getAntFsStateReceiver()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CheckAndRequestAntFs;-><init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    return-object v0
.end method

.method public includeFileInDownloadList(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;)Z
    .locals 2
    .param p1, "fileEntry"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .prologue
    .line 219
    iget v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->FIT_DATA_TYPE:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFileDataType;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected sendMessageToClient(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->this$0:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 208
    return-void
.end method
