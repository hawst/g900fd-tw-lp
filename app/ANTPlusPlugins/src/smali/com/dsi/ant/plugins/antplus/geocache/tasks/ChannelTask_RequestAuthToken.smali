.class public Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_RequestAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private authTokenResult:J

.field private deviceID:I

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private final geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

.field private msgsSinceStartedWait:I

.field private nonce:I

.field private final reqAuthTokenFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private reqAuthTokenProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private reqTokenBytes:[B

.field private requestMade:Z

.field private responseSent:Z

.field private serialNumber:J

.field private targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IIJZ)V
    .locals 3
    .param p1, "geocacheReceiver"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "deviceID"    # I
    .param p4, "nonce"    # I
    .param p5, "serialNumber"    # J
    .param p7, "useProgressUpdates"    # Z

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 35
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 36
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->authTokenResult:J

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    .line 48
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->requestMade:Z

    .line 50
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->responseSent:Z

    .line 58
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .line 59
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->deviceID:I

    .line 60
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->nonce:I

    .line 61
    iput-wide p5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->serialNumber:J

    .line 63
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 64
    if-eqz p7, :cond_0

    .line 65
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 66
    :cond_0
    return-void
.end method

.method private reportProgress(II)V
    .locals 2
    .param p1, "done"    # I
    .param p2, "total"    # I

    .prologue
    .line 87
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 94
    :goto_0
    return-void

    .line 90
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_workUnitsFinished"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    const-string v1, "int_totalUnitsWork"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 93
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 253
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->flushAndEnsureClosedChannel()V

    .line 255
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 256
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    .line 257
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/message/ChannelId;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->deviceID:I

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v4, 0x13

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 259
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    .line 260
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    const/4 v2, 0x0

    const/16 v3, 0x20

    invoke-static {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 261
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    const/4 v2, 0x1

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 262
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    const/4 v2, 0x2

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->nonce:I

    invoke-static {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 263
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->serialNumber:J

    invoke-static {v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 265
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->enableMessageProcessing()V

    .line 266
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 268
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 295
    :goto_0
    iget-wide v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->authTokenResult:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 297
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 303
    :goto_1
    return-void

    .line 288
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 287
    :try_start_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 288
    :catch_2
    move-exception v0

    .line 291
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_3
    move-exception v0

    .line 275
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE initializing channel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 287
    :try_start_5
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_5
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    .line 288
    :catch_4
    move-exception v0

    .line 291
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 278
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_5
    move-exception v0

    .line 280
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_6
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 281
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 285
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    .line 287
    :try_start_7
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v4, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_7
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_7 .. :try_end_7} :catch_6

    .line 292
    :goto_2
    throw v1

    .line 288
    :catch_6
    move-exception v0

    .line 291
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE resetting search timeout: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 301
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto/16 :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Requesting Auth Token from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 308
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 309
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 145
    :try_start_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 149
    :pswitch_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 152
    :pswitch_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v2, "Timed out attempting to find device"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_3

    .line 222
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v2, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v2, "Channel closed."

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->deviceID:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->removeDeviceFromList(I)Z

    .line 159
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->disableMessageProcessing()V

    .line 160
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 164
    :pswitch_3
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reportProgress(II)V

    .line 165
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->requestMade:Z

    .line 166
    const/4 v1, 0x0

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    goto :goto_0

    .line 174
    :pswitch_4
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->requestMade:Z

    goto :goto_0

    .line 186
    :pswitch_5
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->requestMade:Z

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_1

    .line 189
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom7LeBytes([BI)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->authTokenResult:J

    .line 190
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->disableMessageProcessing()V

    .line 191
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 195
    :cond_1
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    .line 196
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    const/16 v2, 0x21

    if-le v1, v2, :cond_2

    .line 198
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v2, "Did not receive message after 10 requests"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 200
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->disableMessageProcessing()V

    .line 201
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 204
    :cond_2
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->requestMade:Z

    if-nez v1, :cond_0

    .line 206
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->msgsSinceStartedWait:I

    rem-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqTokenBytes:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 226
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE handling message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 228
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->disableMessageProcessing()V

    .line 229
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 149
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public processRequest()V
    .locals 6

    .prologue
    .line 99
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->nonce:I

    const/high16 v3, -0x10000

    and-int/2addr v2, v3

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->serialNumber:J

    const-wide v4, -0x100000000L

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 102
    :cond_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->TAG:Ljava/lang/String;

    const-string v3, "ReqAuth Paramaters out of valid range"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_BAD_PARAMS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 138
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->deviceID:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 111
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-nez v2, :cond_2

    .line 113
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_NOT_IN_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 118
    :cond_2
    const/4 v1, 0x0

    .line 121
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 128
    :goto_1
    if-nez v1, :cond_3

    .line 130
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 135
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->setCurrentState(I)V

    goto :goto_0
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V
    .locals 5
    .param p1, "statusCode"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    .prologue
    .line 70
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v2

    .line 72
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->responseSent:Z

    if-eqz v1, :cond_0

    .line 73
    monitor-exit v2

    .line 83
    :goto_0
    return-void

    .line 75
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_statusCode"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    if-ne p1, v1, :cond_1

    .line 78
    const-string v1, "long_authToken"

    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->authTokenResult:J

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->reqAuthTokenFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 81
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_RequestAuthToken;->responseSent:Z

    .line 82
    monitor-exit v2

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
