.class Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
.source "CombinedBikeSpdCadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpdCadAsyncScanInfo"
.end annotation


# instance fields
.field private resultIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 1
    .param p2, "arg0"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 654
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    .line 655
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 651
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->resultIdMap:Ljava/util/HashMap;

    .line 656
    return-void
.end method


# virtual methods
.method public closeAsyncScan()V
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->scanTaskControl:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->shutdownSearch()V

    .line 662
    return-void
.end method

.method public getResultByScanInfo(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    .locals 2
    .param p1, "scanInfo"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    .prologue
    .line 666
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->resultIdMap:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->scanResultInternalIdentifier:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;

    return-object v0
.end method

.method public putResultInfo(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;ZLcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    .locals 8
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "isAlreadyConnected"    # Z
    .param p3, "devType"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .prologue
    .line 676
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    new-instance v1, Ljava/util/UUID;

    iget-object v3, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->longValue()J

    move-result-wide v3

    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v5

    int-to-long v5, v5

    const/16 v7, 0x20

    shl-long/2addr v5, v7

    add-long/2addr v3, v5

    const-wide/16 v5, 0x0

    invoke-direct {v1, v3, v4, v5, v6}, Ljava/util/UUID;-><init>(JJ)V

    invoke-direct {v2, v1, p1, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;-><init>(Ljava/util/UUID;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    if-ne p3, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v0, v2, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;Z)V

    .line 683
    .local v0, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->resultIdMap:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->resultInfo:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->scanResultInternalIdentifier:Ljava/util/UUID;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 684
    return-object v0

    .line 676
    .end local v0    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
