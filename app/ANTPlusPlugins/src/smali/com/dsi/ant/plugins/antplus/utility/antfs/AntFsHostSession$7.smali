.class Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;
.super Ljava/lang/Object;
.source "AntFsHostSession.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 339
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkRf:I
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$400(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkPeriod:I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$500(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;II)V

    .line 340
    .local v0, "disconnectTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    invoke-static {v2, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    .line 341
    .local v1, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    return-object v1
.end method
