.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostEraseChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask$1;
    }
.end annotation


# static fields
.field private static final BEACON_BUSY:I = 0x3

.field private static final BEACON_ID:B = 0x43t

.field private static final BEACON_TRANS:I = 0x2

.field private static final BEACON_UNKNOWN:I = -0x1

.field private static final COMMAND_RESPONSE_ID:B = 0x44t

.field private static final ERASE_REQUEST_ID:B = 0xbt

.field private static final PING_REQUEST_ID:B = 0x5t

.field private static final TAG:Ljava/lang/String;

.field private static final pingCommand:[B


# instance fields
.field private beaconState:I

.field private burstResponse:[B

.field private burstRx:Ljava/io/ByteArrayOutputStream;

.field private busyCount:I

.field private eraseCommand:[B

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private isEraseCommandSent:Z

.field private mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field private msgRetries:I

.field private noResponseCount:I

.field private transferInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    .line 38
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->pingCommand:[B

    return-void

    :array_0
    .array-data 1
        0x44t
        0x5t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;I)V
    .locals 2
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->beaconState:I

    .line 41
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 47
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    .line 49
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    .line 50
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    .line 51
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    .line 52
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    .line 53
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->noResponseCount:I

    .line 58
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->eraseCommand:[B

    .line 67
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->eraseCommand:[B

    const/4 v1, 0x2

    invoke-static {v0, v1, p2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 68
    return-void

    .line 58
    :array_0
    .array-data 1
        0x44t
        0xbt
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private processEraseResponse()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 3

    .prologue
    .line 353
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    array-length v1, v1

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    const/16 v2, 0x44

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    const/16 v2, 0x8b

    if-eq v1, v2, :cond_1

    .line 354
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_ERASE_BAD_RESPONSE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 363
    :goto_0
    return-object v1

    .line 356
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    .line 358
    .local v0, "responseCode":I
    if-nez v0, :cond_2

    .line 359
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    goto :goto_0

    .line 360
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 361
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_ERASE_NOT_READY:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    goto :goto_0

    .line 363
    :cond_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    goto :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 262
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_4

    .line 268
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 278
    .local v0, "channelState":Lcom/dsi/ant/message/ChannelState;
    :try_start_1
    sget-object v3, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v3, :cond_0

    .line 281
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed: Connection lost"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 283
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v3, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 284
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 349
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :goto_1
    return-void

    .line 270
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE occurred requesting status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 274
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 275
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 343
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v1

    .line 345
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Interrupted waiting for result"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 347
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 289
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_0
    :try_start_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Wait for transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/4 v3, -0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->beaconState:I

    .line 291
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 292
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->enableMessageProcessing()V

    .line 293
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0xa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 295
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->beaconState:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 297
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Timed out waiting for transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 301
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    .line 302
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    .line 303
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    .line 304
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 305
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 306
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->enableMessageProcessing()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 310
    :try_start_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->eraseCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 316
    :goto_3
    :try_start_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 318
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    if-nez v3, :cond_3

    .line 320
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed: Erase Tx retries exceeded"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 311
    :catch_2
    move-exception v1

    .line 314
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception sending burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 324
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_3
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Wait for erase response"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    .line 326
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->noResponseCount:I

    .line 327
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 328
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 329
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->enableMessageProcessing()V

    .line 330
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 333
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    if-eqz v3, :cond_1

    .line 336
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->processEraseResponse()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto/16 :goto_1

    .line 341
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_4
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    const-string v0, "ANT-FS Host Erase Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 72
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->TRANSPORT_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x43

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 83
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 87
    :pswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v4, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v4, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 90
    :pswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timeout occured"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 227
    :catch_0
    move-exception v1

    .line 229
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_8

    .line 233
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 96
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 250
    :catch_1
    move-exception v1

    .line 252
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException receiving burst: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 254
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 99
    .end local v1    # "e":Ljava/io/IOException;
    :pswitch_3
    const/4 v3, 0x0

    :try_start_2
    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    .line 100
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    .line 101
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    if-nez v3, :cond_0

    .line 105
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    .line 106
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 107
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 114
    :pswitch_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    goto :goto_0

    .line 117
    :pswitch_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Transfer Rx fail"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 119
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 129
    :pswitch_6
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_2

    .line 131
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v2, v3, v4

    .line 132
    .local v2, "state":I
    if-ne v6, v2, :cond_1

    .line 133
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    .line 135
    :cond_1
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    const/16 v4, 0x28

    if-le v3, v4, :cond_2

    .line 137
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "No response. Client seems stuck in busy state. Ping."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->pingCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 139
    const/4 v3, 0x0

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->busyCount:I

    .line 140
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 141
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 145
    .end local v2    # "state":I
    :cond_2
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->beaconState:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 148
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    if-ne v7, v3, :cond_0

    .line 151
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Got transport beacon"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v3, 0x2

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->beaconState:I

    .line 153
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 154
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 159
    :cond_3
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    if-nez v3, :cond_5

    .line 161
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    .line 163
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    const/16 v4, 0x1e

    if-le v3, v4, :cond_4

    .line 166
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 167
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 172
    :cond_4
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    if-ne v7, v3, :cond_0

    .line 175
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->msgRetries:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_0

    .line 177
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    .line 178
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Retrying erase command"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->eraseCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto/16 :goto_0

    .line 186
    :cond_5
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    if-ne v8, v3, :cond_0

    .line 188
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v2, v3, v4

    .line 190
    .restart local v2    # "state":I
    if-ne v7, v2, :cond_6

    .line 191
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->noResponseCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->noResponseCount:I

    .line 193
    :cond_6
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->noResponseCount:I

    const/16 v4, 0x28

    if-le v3, v4, :cond_0

    .line 195
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "No response."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 197
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 204
    .end local v2    # "state":I
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 205
    .local v0, "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 207
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    .line 208
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 211
    :cond_7
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getPayload()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 213
    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->getSequenceNumber()I

    move-result v3

    and-int/lit8 v3, v3, 0x4

    if-lez v3, :cond_0

    .line 215
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstRx:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->burstResponse:[B

    .line 216
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->isEraseCommandSent:Z

    .line 217
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 218
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 235
    .end local v0    # "burst":Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_8
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_9

    .line 239
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->transferInProgress:Z

    .line 240
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_FAILED error sending ack msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 244
    :cond_9
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->disableMessageProcessing()V

    .line 246
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 87
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
