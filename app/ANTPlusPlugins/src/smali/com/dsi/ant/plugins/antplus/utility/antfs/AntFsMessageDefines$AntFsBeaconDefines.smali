.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;
.super Ljava/lang/Object;
.source "AntFsMessageDefines.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AntFsBeaconDefines"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAntFsDeviceType([B)I
    .locals 1
    .param p0, "antMessageBeacon"    # [B

    .prologue
    .line 20
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v0

    return v0
.end method

.method public static getAntFsManufacturerId([B)I
    .locals 1
    .param p0, "antMessageBeacon"    # [B

    .prologue
    .line 15
    const/4 v0, 0x7

    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v0

    and-int/lit16 v0, v0, 0x7fff

    return v0
.end method

.method public static getBeaconChannelPeriod([B)I
    .locals 1
    .param p0, "antMessageBeacon"    # [B

    .prologue
    .line 25
    const/4 v0, 0x2

    aget-byte v0, p0, v0

    and-int/lit8 v0, v0, 0x7

    packed-switch v0, :pswitch_data_0

    .line 40
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 28
    :pswitch_1
    const v0, 0xffff

    goto :goto_0

    .line 30
    :pswitch_2
    const v0, 0x8000

    goto :goto_0

    .line 32
    :pswitch_3
    const/16 v0, 0x4000

    goto :goto_0

    .line 34
    :pswitch_4
    const/16 v0, 0x2000

    goto :goto_0

    .line 36
    :pswitch_5
    const/16 v0, 0x1000

    goto :goto_0

    .line 38
    :pswitch_6
    const/4 v0, -0x2

    goto :goto_0

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static getDataAvailableFlag([B)Z
    .locals 1
    .param p0, "antMessageBeacon"    # [B

    .prologue
    .line 46
    const/4 v0, 0x2

    aget-byte v0, p0, v0

    and-int/lit8 v0, v0, 0x20

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
