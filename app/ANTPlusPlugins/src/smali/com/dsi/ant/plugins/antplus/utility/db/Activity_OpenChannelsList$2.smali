.class Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;
.super Landroid/content/BroadcastReceiver;
.source "Activity_OpenChannelsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.dsi.ant.intent.action.CHANNEL_PROVIDER_STATE_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 76
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NUM_CHANNELS_AVAILABLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->TAG:Ljava/lang/String;

    const-string v3, "Channel Provider State Change intent did not contain channel available data"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    const-string v2, "com.dsi.ant.intent.extra.CHANNEL_PROVIDER_NUM_CHANNELS_AVAILABLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 82
    .local v1, "numChannels":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->updateChannelsAvailableDisplay(Ljava/lang/Integer;)V
    invoke-static {v2, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->access$000(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;Ljava/lang/Integer;)V

    .line 84
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->needsRefresh:Z

    .line 97
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "numChannels":Ljava/lang/Integer;
    :goto_0
    return-void

    .line 93
    :cond_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received unexpected intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
