.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExecutorDeath()V
    .locals 3

    .prologue
    .line 1239
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Plugin search by deviceNumber search failed: channel died"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1241
    .local v0, "response":Landroid/os/Message;
    const/4 v1, -0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1242
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1243
    return-void
.end method
