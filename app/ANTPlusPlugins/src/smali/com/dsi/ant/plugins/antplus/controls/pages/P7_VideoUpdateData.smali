.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P7_VideoUpdateData.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# static fields
.field public static final DATA_PAGE_NUMBER:I = 0x7


# instance fields
.field public deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

.field public muted:Z

.field public timeProgressed:I

.field public timeRemaining:I

.field private videoEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public videoPlaybackSupport:Z

.field public videoRecorderSupport:Z

.field public volume:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0xffff

    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 24
    const/16 v0, 0xff

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->volume:I

    .line 25
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->muted:Z

    .line 26
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeRemaining:I

    .line 27
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeProgressed:I

    .line 28
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;->OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    .line 30
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoPlaybackSupport:Z

    .line 31
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoRecorderSupport:Z

    .line 33
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 83
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    :goto_0
    return-void

    .line 86
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 91
    const-string v1, "int_volume"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v5

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    and-int/lit8 v4, v4, 0x7f

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    const-string v4, "bool_muted"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v5

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    if-lez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    const-string v1, "int_timeRemaining"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v4

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    const-string v1, "int_timeProgressed"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v4

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 103
    const-string v4, "boolVideoPlaybackSupported"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v6

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-lez v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    const-string v1, "boolVideoRecorderSupported"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v6

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-lez v4, :cond_3

    :goto_3
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    const-string v1, "int_videoState"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_1
    move v1, v3

    .line 94
    goto :goto_1

    :cond_2
    move v1, v3

    .line 103
    goto :goto_2

    :cond_3
    move v2, v3

    .line 106
    goto :goto_3
.end method

.method public encodePage([B)V
    .locals 4
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 55
    const/4 v0, 0x1

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->volume:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 56
    const/4 v0, 0x2

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeRemaining:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 57
    const/4 v0, 0x3

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeRemaining:I

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 58
    const/4 v0, 0x4

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeProgressed:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 59
    const/4 v0, 0x5

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeProgressed:I

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 60
    aput-byte v2, p1, v3

    .line 61
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoPlaybackSupport:Z

    if-eqz v0, :cond_0

    .line 62
    aget-byte v0, p1, v3

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 63
    :cond_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoRecorderSupport:Z

    if-eqz v0, :cond_1

    .line 64
    aget-byte v0, p1, v3

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 65
    :cond_1
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;->getIntValue()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 66
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateVideoCapabilities(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const-string v0, "boolVideoPlaybackSupported"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoPlaybackSupport:Z

    .line 48
    const-string v0, "boolVideoRecorderSupported"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->videoRecorderSupport:Z

    .line 49
    return-void
.end method

.method public updateVideoStatus(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 38
    const-string v0, "int_volume"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->volume:I

    .line 39
    const-string v0, "bool_muted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->muted:Z

    .line 40
    const-string v0, "int_timeRemaining"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeRemaining:I

    .line 41
    const-string v0, "int_timeProgressed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->timeProgressed:I

    .line 42
    const-string v0, "int_videoState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    .line 43
    return-void
.end method
