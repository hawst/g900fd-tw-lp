.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;
.super Ljava/lang/Object;
.source "BloodPressureResetDataAndSetTimeProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$1;,
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;,
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field final antDeviceNumber:I

.field final beaconInterval:I

.field final bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

.field final executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field final linkRfFreq:I

.field final linkRfPeriod:I

.field final requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

.field final serialNumberForHost:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;JIIII)V
    .locals 0
    .param p1, "requestor"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;
    .param p2, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p3, "bpmDownloadDb"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;
    .param p4, "serialNumberForHost"    # J
    .param p6, "antDeviceNumber"    # I
    .param p7, "linkRfFreq"    # I
    .param p8, "linkRfPeriod"    # I
    .param p9, "beaconInterval"    # I

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    .line 121
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 122
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    .line 123
    iput-wide p4, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->serialNumberForHost:J

    .line 124
    iput p6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->antDeviceNumber:I

    .line 125
    iput p7, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->linkRfFreq:I

    .line 126
    iput p8, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->linkRfPeriod:I

    .line 127
    iput p9, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->beaconInterval:I

    .line 128
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public handleMonitorFailure(I)V
    .locals 1
    .param p1, "antFsStatusfailureCode"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;->requestor:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetRequestor;->sendResult(I)V

    .line 143
    return-void
.end method

.method public processRequest()V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor$ResetDataAndSetTimeRunner;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureResetDataAndSetTimeProcessor;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 133
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 134
    return-void
.end method
