.class public abstract Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;
.source "AntPluginAntPlusSender.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field private isRequestInProgress:Z

.field private reqDataPageQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 31
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    .line 32
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->isRequestInProgress:Z

    .line 36
    :try_start_0
    invoke-static {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->createPluginDeviceExecutor(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException during initizalization"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->onChannelDeath()V

    .line 41
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
.end method

.method private checkSendDataPageQueue()V
    .locals 4

    .prologue
    .line 102
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    monitor-enter v3

    .line 104
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->isRequestInProgress:Z

    if-eqz v2, :cond_0

    .line 105
    monitor-exit v3

    .line 118
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;

    .line 110
    .local v1, "requestedDataPage":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    if-eqz v1, :cond_1

    .line 112
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v0, v1, p0, v2}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V

    .line 114
    .local v0, "req":Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->isRequestInProgress:Z

    .line 115
    iget-object v2, v1, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->dataPage:[B

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;->processRequest([B)V

    .line 117
    .end local v0    # "req":Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
    :cond_1
    monitor-exit v3

    goto :goto_0

    .end local v1    # "requestedDataPage":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 7
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 48
    iget v4, p2, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 73
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 76
    :goto_0
    return-void

    .line 51
    :pswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 52
    .local v0, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 53
    .local v3, "response":Landroid/os/Message;
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 54
    .local v1, "params":Landroid/os/Bundle;
    iget v4, p2, Landroid/os/Message;->what:I

    iput v4, v3, Landroid/os/Message;->what:I

    .line 56
    const-string v4, "int_requestedDataPage"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 57
    .local v2, "requestedDataPage":I
    if-ltz v2, :cond_0

    const/16 v4, 0xff

    if-le v2, v4, :cond_1

    .line 59
    :cond_0
    const/4 v4, -0x3

    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 60
    sget-object v4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->TAG:Ljava/lang/String;

    const-string v5, "Cmd requestDataPage failed to start because the requested data page number was invalid."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0, v0, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 65
    :cond_1
    const/4 v4, 0x0

    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 66
    invoke-virtual {p0, v0, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 67
    int-to-byte v4, v2

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(B)[B

    move-result-object v4

    const/16 v5, 0x6b

    const-string v6, "int_requestStatus"

    invoke-virtual {p0, v4, v0, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x6a
        :pswitch_0
    .end packed-switch
.end method

.method public checkChannelState(Z)V
    .locals 2
    .param p1, "isInit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 125
    if-eqz p1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    monitor-enter v1

    .line 130
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->isRequestInProgress:Z

    .line 131
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->checkSendDataPageQueue()V

    .line 132
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V
    .locals 7
    .param p1, "dataPage"    # [B
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "whatReqEvt"    # I
    .param p4, "whatReqStatus"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v3, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;

    invoke-direct {v3, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;-><init>([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    .line 81
    .local v3, "requestedDataPage":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    const/4 v2, 0x0

    .line 83
    .local v2, "isAlreadyPresent":Z
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    monitor-enter v5

    .line 85
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;

    .line 86
    .local v0, "existing":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 88
    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->clientList:Ljava/util/List;

    iget-object v6, v3, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->clientList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 89
    const/4 v2, 0x1

    .line 93
    .end local v0    # "existing":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    :cond_1
    if-nez v2, :cond_2

    .line 94
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->reqDataPageQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusSender;->checkSendDataPageQueue()V

    .line 98
    return-void

    .line 95
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method
