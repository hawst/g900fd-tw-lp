.class Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;
.super Ljava/lang/Object;
.source "DialogPref_ModifyDevice.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->showDialog(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

.field final synthetic val$d:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->val$d:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 169
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forget "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "No"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Yes"

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 193
    return-void
.end method
