.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
.super Ljava/lang/Enum;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TemperatureCondition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

.field public static final enum CURRENT_TEMPERATURE_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

.field public static final enum CURRENT_TEMPERATURE_TOO_HIGH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

.field public static final enum CURRENT_TEMPERATURE_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

.field public static final enum NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 866
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    const-string v1, "NOT_APPLICABLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 871
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    const-string v1, "CURRENT_TEMPERATURE_TOO_LOW"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 876
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    const-string v1, "CURRENT_TEMPERATURE_OK"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 881
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    const-string v1, "CURRENT_TEMPERATURE_TOO_HIGH"

    invoke-direct {v0, v1, v6, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_TOO_HIGH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 886
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 861
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->CURRENT_TEMPERATURE_TOO_HIGH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 891
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 892
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->intValue:I

    .line 893
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 911
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 913
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 919
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    :goto_1
    return-object v3

    .line 911
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 917
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    .line 918
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->intValue:I

    move-object v3, v4

    .line 919
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 861
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;
    .locals 1

    .prologue
    .line 861
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 901
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$TemperatureCondition;->intValue:I

    return v0
.end method
