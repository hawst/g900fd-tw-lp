.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusEnvironmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$IpcDefines;
    }
.end annotation


# instance fields
.field mTemperatureDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 154
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 252
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;-><init>()V

    .line 284
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    const-string v0, "ANT+ Plugin: Environment"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 293
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 294
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.environment.EnvironmentService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 295
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 12
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 307
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 326
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 311
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->mTemperatureDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    .line 315
    .local v9, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 316
    .local v1, "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 317
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_currentTemperature"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    .line 318
    .local v4, "currentTemperature":Ljava/math/BigDecimal;
    const-string v0, "long_eventCount"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 319
    .local v5, "eventCount":J
    const-string v0, "decimal_lowLast24Hours"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/math/BigDecimal;

    .line 320
    .local v7, "lowLast24Hours":Ljava/math/BigDecimal;
    const-string v0, "decimal_highLast24Hours"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 321
    .local v8, "highLast24Hours":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->mTemperatureDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;

    invoke-interface/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;->onNewTemperatureData(JLjava/util/EnumSet;Ljava/math/BigDecimal;JLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
    .end packed-switch
.end method

.method public subscribeTemperatureDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;)V
    .locals 1
    .param p1, "TemperatureDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 339
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->mTemperatureDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc$ITemperatureDataReceiver;

    .line 340
    if-eqz p1, :cond_0

    .line 342
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->subscribeToEvent(I)Z

    .line 348
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusEnvironmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
