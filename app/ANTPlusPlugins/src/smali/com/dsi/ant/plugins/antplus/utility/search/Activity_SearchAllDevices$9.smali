.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->refreshProximityUi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    if-nez v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxOffLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 756
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxValueLabel:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 763
    :goto_0
    return-void

    .line 760
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxOffLabel:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 761
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->textView_ProxValueLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$9;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
