.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

.field final synthetic val$this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->val$this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 112
    sparse-switch p1, :sswitch_data_0

    .line 124
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Search reported failure: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 126
    .local v0, "failMsg":Landroid/os/Message;
    const/16 v1, -0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 127
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 128
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V
    invoke-static {v1, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/os/Message;)V

    .line 129
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 132
    .end local v0    # "failMsg":Landroid/os/Message;
    :goto_0
    :sswitch_0
    return-void

    .line 115
    :sswitch_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 116
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->thisSearchListIndex:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->deviceFound(II)V
    invoke-static {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$200(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;II)V

    goto :goto_0

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->thisSearchListIndex:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->deviceFound(II)V
    invoke-static {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$200(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;II)V

    goto :goto_0

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method
