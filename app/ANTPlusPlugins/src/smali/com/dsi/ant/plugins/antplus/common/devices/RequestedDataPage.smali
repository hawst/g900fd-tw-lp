.class public Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
.super Ljava/lang/Object;
.source "RequestedDataPage.java"


# instance fields
.field public clientList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field public dataPage:[B

.field public whatReqEvtIpc:I

.field public whatReqStatusIpc:Ljava/lang/String;


# direct methods
.method public constructor <init>([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V
    .locals 1
    .param p1, "dataPage"    # [B
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "whatReqEvtIpc"    # I
    .param p4, "whatReqStatusIpc"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->dataPage:[B

    .line 19
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqEvtIpc:I

    .line 20
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->clientList:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->clientList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-ne p0, p1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 40
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 41
    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 43
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;

    .line 45
    .local v0, "other":Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->dataPage:[B

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->dataPage:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 46
    goto :goto_0

    .line 47
    :cond_4
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqEvtIpc:I

    iget v4, v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqEvtIpc:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 48
    goto :goto_0

    .line 49
    :cond_5
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 51
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 52
    goto :goto_0

    .line 53
    :cond_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 54
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 27
    const/16 v0, 0x1f

    .line 28
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 29
    .local v1, "result":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->dataPage:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 30
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqEvtIpc:I

    add-int v1, v2, v3

    .line 31
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 33
    return v1

    .line 31
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/RequestedDataPage;->whatReqStatusIpc:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method
