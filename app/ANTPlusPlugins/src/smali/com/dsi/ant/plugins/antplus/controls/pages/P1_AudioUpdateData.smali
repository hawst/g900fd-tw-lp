.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P1_AudioUpdateData.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# static fields
.field public static final DATA_PAGE_NUMBER:I = 0x1


# instance fields
.field private audioEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public capabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

.field public currentTrackTime:I

.field public deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

.field public repeatState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

.field public shuffleState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

.field public songTitleSupport:Z

.field public totalTrackTime:I

.field public volume:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0xffff

    .line 22
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 27
    const/16 v0, 0xff

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->volume:I

    .line 28
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->totalTrackTime:I

    .line 29
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->currentTrackTime:I

    .line 30
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;->OFF:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    .line 31
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;->OFF_UNSUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->repeatState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    .line 32
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;->OFF_UNSUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->shuffleState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->songTitleSupport:Z

    .line 37
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->audioEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 96
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->audioEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-nez v5, :cond_0

    .line 133
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v1, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v1, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 101
    const-string v5, "long_EventFlags"

    invoke-virtual {v1, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 103
    const-string v5, "int_volume"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x2

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v5, "int_totalTrackTime"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 107
    const-string v5, "int_currentTrackTime"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    and-int/lit16 v5, v5, 0x80

    if-lez v5, :cond_2

    const/4 v4, 0x1

    .line 111
    .local v4, "ignore":Z
    :goto_1
    if-nez v4, :cond_1

    .line 113
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    and-int/lit8 v5, v5, 0x4

    if-lez v5, :cond_3

    const/4 v2, 0x1

    .line 116
    .local v2, "customRepeatSupported":Z
    :goto_2
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x7

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-lez v5, :cond_4

    const/4 v3, 0x1

    .line 119
    .local v3, "customShuffleSupported":Z
    :goto_3
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;-><init>()V

    .line 120
    .local v0, "audioCapabilities":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
    iput-boolean v2, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customRepeatModeSupport:Z

    .line 121
    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customShuffleModeSupport:Z

    .line 122
    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->writeToBundle(Landroid/os/Bundle;)V

    .line 125
    .end local v0    # "audioCapabilities":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
    .end local v2    # "customRepeatSupported":Z
    .end local v3    # "customShuffleSupported":Z
    :cond_1
    const-string v5, "int_audioState"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/16 v7, 0x8

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    and-int/lit16 v6, v6, 0xf0

    shr-int/lit8 v6, v6, 0x4

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    const-string v5, "int_repeatState"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/16 v7, 0x8

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    and-int/lit8 v6, v6, 0xc

    shr-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 129
    const-string v5, "int_shuffleState"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v6

    const/16 v7, 0x8

    aget-byte v6, v6, v7

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v6

    and-int/lit8 v6, v6, 0x3

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->audioEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 110
    .end local v4    # "ignore":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 113
    .restart local v4    # "ignore":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 116
    .restart local v2    # "customRepeatSupported":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public encodePage([B)V
    .locals 5
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 60
    const/4 v0, 0x1

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->volume:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 61
    const/4 v0, 0x2

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->totalTrackTime:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 62
    const/4 v0, 0x3

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->totalTrackTime:I

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 63
    const/4 v0, 0x4

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->currentTrackTime:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 64
    const/4 v0, 0x5

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->currentTrackTime:I

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 65
    aput-byte v2, p1, v3

    .line 66
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->songTitleSupport:Z

    if-eqz v0, :cond_0

    .line 67
    aget-byte v0, p1, v3

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->capabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customShuffleModeSupport:Z

    if-eqz v0, :cond_1

    .line 69
    aget-byte v0, p1, v3

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->capabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->customRepeatModeSupport:Z

    if-eqz v0, :cond_2

    .line 71
    aget-byte v0, p1, v3

    or-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 75
    :cond_2
    aput-byte v2, p1, v4

    .line 76
    aget-byte v0, p1, v4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->shuffleState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;->getIntValue()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    int-to-byte v1, v1

    or-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 77
    aget-byte v0, p1, v4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->repeatState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;->getIntValue()I

    move-result v1

    shl-int/lit8 v1, v1, 0x2

    and-int/lit8 v1, v1, 0xc

    int-to-byte v1, v1

    or-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 78
    aget-byte v0, p1, v4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;->getIntValue()I

    move-result v1

    shl-int/lit8 v1, v1, 0x4

    and-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    or-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 79
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->audioEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 90
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateAudioCapabilities(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-static {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->readFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->capabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    .line 54
    return-void
.end method

.method public updateAudioStatus(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 41
    const-string v0, "int_volume"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->volume:I

    .line 42
    const-string v0, "int_totalTrackTime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->totalTrackTime:I

    .line 43
    const-string v0, "int_currentTrackTime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->currentTrackTime:I

    .line 44
    const-string v0, "int_audioState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->deviceState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;

    .line 45
    const-string v0, "int_repeatState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->repeatState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;

    .line 46
    const-string v0, "int_shuffleState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->shuffleState:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    .line 47
    return-void
.end method
