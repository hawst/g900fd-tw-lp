.class Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;
.super Ljava/lang/Object;
.source "RemoteControlService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

.field final synthetic val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 9
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v7, -0x4

    .line 417
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 418
    .local v3, "response":Landroid/os/Message;
    sparse-switch p1, :sswitch_data_0

    .line 457
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 458
    # getter for: Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Plugin search by deviceNumber search failed internally with search result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iput v7, v3, Landroid/os/Message;->what:I

    .line 460
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 421
    :sswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 422
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_1

    .line 424
    # getter for: Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin search by deviceNumber failed to shutdown executor cleanly"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iput v7, v3, Landroid/os/Message;->what:I

    .line 426
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 429
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    # invokes: Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v4, v5, v8}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->access$500(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    .line 430
    .local v2, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    invoke-virtual {v4, v0, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v1

    .line 431
    .local v1, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-nez v1, :cond_2

    .line 433
    # getter for: Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin search by deviceNumber search failed internally: Device instantiation failed."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iput v7, v3, Landroid/os/Message;->what:I

    .line 435
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 438
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v1, v6, v8}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 439
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0

    .line 443
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v1    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v2    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :sswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 444
    const/4 v4, -0x7

    iput v4, v3, Landroid/os/Message;->what:I

    .line 445
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 449
    :sswitch_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 450
    # getter for: Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin search by deviceNumber search failed: Detected modes do not match requested modes."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    iput v7, v3, Landroid/os/Message;->what:I

    .line 452
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->this$0:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 418
    :sswitch_data_0
    .sparse-switch
        -0x1a -> :sswitch_2
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
