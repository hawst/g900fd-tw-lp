.class public abstract Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusCommonPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IpcDefines;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;

.field protected mCommandLock:Ljava/util/concurrent/Semaphore;

.field mManufacturerIdentificationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;

.field mManufacturerSpecificDataReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;

.field mProductInformationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;

.field protected mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 311
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    return-void
.end method


# virtual methods
.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 31
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 320
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 404
    :pswitch_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unrecognized event received: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    iget v9, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 324
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerIdentificationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;

    if-eqz v1, :cond_0

    .line 327
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v27

    .line 328
    .local v27, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 329
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 330
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_hardwareRevision"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 331
    .local v5, "hardwareRevision":I
    const-string v1, "int_manufacturerID"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 332
    .local v6, "manufacturerID":I
    const-string v1, "int_modelNumber"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 333
    .local v7, "modelNumber":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerIdentificationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;

    invoke-interface/range {v1 .. v7}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;->onNewManufacturerIdentification(JLjava/util/EnumSet;III)V

    goto :goto_0

    .line 339
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "hardwareRevision":I
    .end local v6    # "manufacturerID":I
    .end local v7    # "modelNumber":I
    .end local v27    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mProductInformationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;

    if-eqz v1, :cond_0

    .line 342
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v27

    .line 343
    .restart local v27    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 344
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 345
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_softwareRevision"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 346
    .local v12, "mainSoftwareRevision":I
    const-string v1, "int_supplementaryRevision"

    const/4 v8, -0x2

    move-object/from16 v0, v27

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 347
    .local v13, "supplementaryRevision":I
    const-string v1, "long_serialNumber"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 348
    .local v14, "serialNumber":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mProductInformationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;

    move-wide v9, v2

    move-object v11, v4

    invoke-interface/range {v8 .. v15}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;->onNewProductInformation(JLjava/util/EnumSet;IIJ)V

    goto :goto_0

    .line 354
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v12    # "mainSoftwareRevision":I
    .end local v13    # "supplementaryRevision":I
    .end local v14    # "serialNumber":J
    .end local v27    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;

    if-eqz v1, :cond_0

    .line 357
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v27

    .line 358
    .restart local v27    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 359
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 360
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeOperatingTime"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 361
    .local v20, "cumulativeOperatingTime":J
    const-string v1, "decimal_batteryVoltage"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v22

    check-cast v22, Ljava/math/BigDecimal;

    .line 362
    .local v22, "batteryVoltage":Ljava/math/BigDecimal;
    const-string v1, "int_batteryStatusCode"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    move-result-object v23

    .line 363
    .local v23, "batteryStatus":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    const-string v1, "int_cumulativeOperatingTimeResolution"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 365
    .local v24, "cumulativeOperatingTimeResolution":I
    const-string v1, "int_numberOfBatteries"

    const/4 v8, -0x2

    move-object/from16 v0, v27

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v25

    .line 366
    .local v25, "numberOfBatteries":I
    const-string v1, "int_batteryIdentifier"

    const/4 v8, -0x2

    move-object/from16 v0, v27

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v26

    .line 367
    .local v26, "batteryIdentifier":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;

    move-object/from16 v16, v0

    move-wide/from16 v17, v2

    move-object/from16 v19, v4

    invoke-interface/range {v16 .. v26}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;->onNewBatteryStatus(JLjava/util/EnumSet;JLjava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;III)V

    goto/16 :goto_0

    .line 373
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "cumulativeOperatingTime":J
    .end local v22    # "batteryVoltage":Ljava/math/BigDecimal;
    .end local v23    # "batteryStatus":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    .end local v24    # "cumulativeOperatingTimeResolution":I
    .end local v25    # "numberOfBatteries":I
    .end local v26    # "batteryIdentifier":I
    .end local v27    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerSpecificDataReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;

    if-eqz v1, :cond_0

    .line 376
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v27

    .line 377
    .restart local v27    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 379
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 381
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "arrayByte_rawDataBytes"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v28

    .line 385
    .local v28, "rawDataBytes":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerSpecificDataReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;

    move-object/from16 v0, v28

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;->onNewManufacturerSpecificData(JLjava/util/EnumSet;[B)V

    .line 391
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v27    # "b":Landroid/os/Bundle;
    .end local v28    # "rawDataBytes":[B
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    move-object/from16 v30, v0

    .line 392
    .local v30, "tempReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 393
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 395
    if-eqz v30, :cond_0

    .line 397
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v27

    .line 398
    .restart local v27    # "b":Landroid/os/Bundle;
    const-string v1, "int_requestStatus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v29

    .line 399
    .local v29, "requestStatus":I
    invoke-static/range {v29 .. v29}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v1

    move-object/from16 v0, v30

    invoke-interface {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;->onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto/16 :goto_0

    .line 320
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestCommonDataPage(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "commonDataPage"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 508
    const-string v1, "requestCommonDataPage"

    .line 510
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x6a

    .line 511
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 512
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "int_requestedDataPage"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->getIntValue()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 514
    const/16 v0, 0x4ef1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "cmdName"    # Ljava/lang/String;
    .param p2, "whatCmd"    # I
    .param p3, "params"    # Landroid/os/Bundle;
    .param p4, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 581
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z
    .locals 7
    .param p1, "cmdName"    # Ljava/lang/String;
    .param p2, "whatCmd"    # I
    .param p3, "params"    # Landroid/os/Bundle;
    .param p4, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p5, "requiredServiceVersion"    # Ljava/lang/Integer;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 519
    if-eqz p5, :cond_1

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->reportedServiceVersion:I

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 521
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " requires ANT+ Plugins Service >="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", installed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->reportedServiceVersion:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    if-eqz p4, :cond_0

    .line 523
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    invoke-interface {p4, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;->onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    .line 571
    :cond_0
    :goto_0
    return v2

    .line 527
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_2

    .line 529
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed to start because a local command is still processing."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 533
    :cond_2
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 535
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 536
    .local v0, "cmdMsg":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    .line 538
    if-eqz p3, :cond_3

    .line 539
    invoke-virtual {v0, p3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 541
    :cond_3
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 543
    .local v1, "ret":Landroid/os/Message;
    if-nez v1, :cond_4

    .line 545
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " died in sendPluginCommand()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 550
    :cond_4
    iget v3, v1, Landroid/os/Message;->arg1:I

    const/4 v4, -0x3

    if-ne v3, v4, :cond_5

    .line 552
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    .line 554
    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 555
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 556
    if-eqz p4, :cond_0

    .line 557
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->FAIL_BAD_PARAMS:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    invoke-interface {p4, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;->onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto/16 :goto_0

    .line 560
    :cond_5
    iget v2, v1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_6

    .line 563
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cmd "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    .line 565
    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 566
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 567
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cmd failed internally"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 570
    :cond_6
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    .line 571
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method protected sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "cmdName"    # Ljava/lang/String;
    .param p2, "whatCmd"    # I
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    const/4 v3, 0x0

    .line 576
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method protected sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z
    .locals 6
    .param p1, "cmdName"    # Ljava/lang/String;
    .param p2, "whatCmd"    # I
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p4, "requiredServiceVersion"    # Ljava/lang/Integer;

    .prologue
    .line 586
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public subscribeBatteryStatusEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;)V
    .locals 1
    .param p1, "BatteryStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;

    .prologue
    const/16 v0, 0x66

    .line 455
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mBatteryStatusReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IBatteryStatusReceiver;

    .line 456
    if-eqz p1, :cond_0

    .line 458
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeToEvent(I)Z

    .line 464
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeManufacturerIdentificationEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;)V
    .locals 1
    .param p1, "ManufacturerIdentificationReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;

    .prologue
    const/16 v0, 0x64

    .line 417
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerIdentificationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;

    .line 418
    if-eqz p1, :cond_0

    .line 420
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeToEvent(I)Z

    .line 426
    :goto_0
    return-void

    .line 424
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeManufacturerSpecificDataEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;)Z
    .locals 3
    .param p1, "ManufacturerSpecificDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;

    .prologue
    const/16 v2, 0x67

    .line 479
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->reportedServiceVersion:I

    const/16 v1, 0x4eee

    if-ge v0, v1, :cond_0

    .line 481
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeManufacturerSpecificDataEvent requires ANT+ Plugins Service >20206, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const/4 v0, 0x0

    .line 494
    :goto_0
    return v0

    .line 485
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mManufacturerSpecificDataReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerSpecificDataReceiver;

    .line 486
    if-eqz p1, :cond_1

    .line 488
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeToEvent(I)Z

    move-result v0

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->unsubscribeFromEvent(I)V

    .line 494
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeProductInformationEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;)V
    .locals 1
    .param p1, "ProductInformationReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;

    .prologue
    const/16 v0, 0x65

    .line 436
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->mProductInformationReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;

    .line 437
    if-eqz p1, :cond_0

    .line 439
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeToEvent(I)Z

    .line 445
    :goto_0
    return-void

    .line 443
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
