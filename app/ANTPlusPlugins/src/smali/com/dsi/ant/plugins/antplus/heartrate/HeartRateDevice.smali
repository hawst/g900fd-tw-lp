.class public Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;
.source "HeartRateDevice.java"


# instance fields
.field private heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private mPage0:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;

.field private mPage4:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;

.field private mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 0
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusLegacyReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 26
    return-void
.end method


# virtual methods
.method public addNonLegacyPages(Ljava/util/List;Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;)V
    .locals 3
    .param p2, "defaultPage"    # Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "addToThisList":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->getHeartBeatEventTimeAccumulator()Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->getRrDecoder()Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;-><init>(Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mPage4:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;

    .line 32
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mPage4:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P4_BaseData;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public getDefaultPage()Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->getHeartBeatEventTimeAccumulator()Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->getRrDecoder()Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;-><init>(Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mPage0:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;

    .line 39
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mPage0:Lcom/dsi/ant/plugins/antplus/heartrate/pages/P0_DefaultHrData;

    return-object v0
.end method

.method public getHeartBeatEventTimeAccumulator()Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    const v1, 0xffff

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->heartBeatEventTime_1024ths:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    return-object v0
.end method

.method public getRrDecoder()Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;->mRrIntervalDecoder:Lcom/dsi/ant/plugins/antplus/heartrate/RrIntervalDecoder;

    return-object v0
.end method
