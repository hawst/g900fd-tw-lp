.class public Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;
.super Landroid/app/Activity;
.source "Dialog_SearchPreferredDevice.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field mDisplayName:Ljava/lang/String;

.field mIsConfigChange:Z

.field mIsReplyMessageSent:Z

.field private mIsTransitionToSearchAll:Z

.field private final replySent:Ljava/lang/Object;

.field reqParams:Landroid/os/Bundle;

.field searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field searchTask:Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->replySent:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->startSearchTask()V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reportCancelled()V

    return-void
.end method

.method private ensureCanceledSearch()V
    .locals 3

    .prologue
    .line 370
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 373
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 376
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsTransitionToSearchAll:Z

    if-nez v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v2, "antchannel_AlternativeSearchChannel"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/AntChannel;

    .line 379
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v0, :cond_0

    .line 380
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 383
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    :cond_0
    return-void
.end method

.method private initExecutorAndSearchTask()V
    .locals 11

    .prologue
    .line 146
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v1, "antchannel_Channel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/dsi/ant/channel/AntChannel;

    .line 147
    .local v8, "channel":Lcom/dsi/ant/channel/AntChannel;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v1, "int_DevType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 148
    .local v4, "devType":I
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v1, "int_TransType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 149
    .local v5, "transType":I
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v1, "int_Period"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 150
    .local v3, "period":I
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v1, "int_RfFreq"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 151
    .local v2, "rfFreq":I
    const/4 v6, 0x0

    .line 154
    .local v6, "proximityThreshold":I
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-lez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v7, -0x20000001

    and-int/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 157
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v4

    .line 158
    const/16 v3, 0x1f96

    .line 161
    :cond_0
    new-instance v9, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$3;

    invoke-direct {v9, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$3;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V

    .line 175
    .local v9, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_0
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v0, v8, v9}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v7, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;

    invoke-direct {v7, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V

    invoke-direct/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchTask:Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    .line 270
    :goto_0
    return-void

    .line 176
    :catch_0
    move-exception v10

    .line 178
    .local v10, "e":Landroid/os/RemoteException;
    invoke-interface {v9}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    goto :goto_0
.end method

.method private reportCancelled()V
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 323
    .local v0, "respToPlugin":Landroid/os/Message;
    const/4 v1, -0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 325
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V

    .line 326
    return-void
.end method

.method private sendReply(Landroid/os/Message;)V
    .locals 6
    .param p1, "respToPlugin"    # Landroid/os/Message;

    .prologue
    .line 330
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->replySent:Ljava/lang/Object;

    monitor-enter v3

    .line 332
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    if-eqz v2, :cond_0

    .line 334
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Prevented erroneous sending of an extra reply: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    monitor-exit v3

    .line 350
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v4, "msgr_SearchResultReceiver"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    .local v1, "retMsgr":Landroid/os/Messenger;
    :try_start_1
    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    :goto_1
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    .line 349
    monitor-exit v3

    goto :goto_0

    .end local v1    # "retMsgr":Landroid/os/Messenger;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 343
    .restart local v1    # "retMsgr":Landroid/os/Messenger;
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    const-string v4, "RemoteException trying to send result to client"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private startSearchTask()V
    .locals 5

    .prologue
    .line 276
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchTask:Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    const/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    const-string v3, "InterruptedException trying to start task"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 281
    .local v1, "failMsg":Landroid/os/Message;
    const/16 v2, -0x64

    iput v2, v1, Landroid/os/Message;->what:I

    .line 282
    invoke-direct {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V

    .line 283
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    goto :goto_0
.end method


# virtual methods
.method launchSearchAll(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "pluginInfo"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 355
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsTransitionToSearchAll:Z

    .line 356
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->ensureCanceledSearch()V

    .line 359
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 360
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.dsi.ant.plugins.antplus.pcc.plugindata"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 361
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->startActivity(Landroid/content/Intent;)V

    .line 363
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    .line 365
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    .line 366
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->ensureCanceledSearch()V

    .line 315
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reportCancelled()V

    .line 316
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 317
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v9, 0x7f030006

    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->setContentView(I)V

    .line 57
    iput-boolean v11, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsConfigChange:Z

    .line 58
    iput-boolean v11, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    .line 59
    iput-boolean v11, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsTransitionToSearchAll:Z

    .line 62
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "com.dsi.ant.plugins.antplus.pcc.plugindata"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    .line 66
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    if-nez v9, :cond_0

    .line 67
    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;

    const-string v10, "Attempt to start activity with illegal arguments"

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput-boolean v12, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    .line 69
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    .line 142
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v10, "str_PluginName"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 76
    .local v6, "pluginName":Ljava/lang/String;
    new-instance v3, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 79
    .local v3, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_0
    invoke-virtual {v3, v6}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->getPreferredDevice(Ljava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v9

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 87
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    if-nez v9, :cond_1

    .line 89
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->launchSearchAll(Landroid/os/Bundle;)V

    .line 123
    :goto_1
    const v9, 0x7f060014

    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 124
    .local v2, "button_SearchAll":Landroid/widget/Button;
    new-instance v9, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$1;

    invoke-direct {v9, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V

    invoke-virtual {v2, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v9, 0x7f060015

    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 134
    .local v1, "button_CancelSearch":Landroid/widget/Button;
    new-instance v9, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$2;

    invoke-direct {v9, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V

    invoke-virtual {v1, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 83
    .end local v1    # "button_CancelSearch":Landroid/widget/Button;
    .end local v2    # "button_SearchAll":Landroid/widget/Button;
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v9

    .line 95
    :cond_1
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reqParams:Landroid/os/Bundle;

    const-string v10, "intarl_AvailableConnectedDevices"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 96
    .local v0, "alreadyConnectedDeviceNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 98
    .local v4, "i":Ljava/lang/Integer;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v4, v9}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 100
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 101
    .local v7, "respToPlugin":Landroid/os/Message;
    iput v11, v7, Landroid/os/Message;->what:I

    .line 102
    iput v12, v7, Landroid/os/Message;->arg1:I

    .line 103
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v7, Landroid/os/Message;->arg2:I

    .line 105
    invoke-direct {p0, v7}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V

    .line 107
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    goto/16 :goto_0

    .line 113
    .end local v4    # "i":Ljava/lang/Integer;
    .end local v7    # "respToPlugin":Landroid/os/Message;
    :cond_3
    const v9, 0x7f060001

    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 114
    .local v8, "statusDisplay":Landroid/widget/TextView;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v9, v9, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    .line 115
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_5

    .line 116
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-no name saved- ID <"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->devToSearchFor:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v10, v10, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    .line 117
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Searching for preferred device: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->initExecutorAndSearchTask()V

    .line 120
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->startSearchTask()V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->ensureCanceledSearch()V

    .line 304
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsReplyMessageSent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsConfigChange:Z

    if-nez v0, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->reportCancelled()V

    .line 307
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 308
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mIsConfigChange:Z

    .line 293
    invoke-super {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
