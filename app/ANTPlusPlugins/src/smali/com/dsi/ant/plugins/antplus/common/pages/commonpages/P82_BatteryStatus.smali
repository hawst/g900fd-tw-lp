.class public Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P82_BatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;
    }
.end annotation


# instance fields
.field private batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 86
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    .line 87
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0x66

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    invoke-virtual {v1, p5}, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 109
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 111
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 112
    const-string v1, "long_cumulativeOperatingTime"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget-wide v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 113
    const-string v1, "decimal_batteryVoltage"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryVoltage:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 114
    const-string v1, "int_batteryStatusCode"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryStatusCode:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v1, "int_cumulativeOperatingTimeResolution"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTimeResolution:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 116
    const-string v1, "int_numberOfBatteries"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->numberOfBatteries:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v1, "int_batteryIdentifier"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->decoder:Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryIdentifier:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;->batEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
