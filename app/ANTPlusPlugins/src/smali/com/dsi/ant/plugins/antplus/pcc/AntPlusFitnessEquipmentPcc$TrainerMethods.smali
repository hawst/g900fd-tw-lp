.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;
.super Ljava/lang/Object;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrainerMethods"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V
    .locals 0

    .prologue
    .line 3960
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestBasicResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    .line 4218
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestBasicResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestCommandStatus(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    .line 4205
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestCommandStatus(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetBasicResistance(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "totalResistance"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4234
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetBasicResistance(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetTargetPower(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "targetPower"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4264
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetTargetPower(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "grade"    # Ljava/math/BigDecimal;
    .param p2, "rollingResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4376
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetTrackResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetWindResistance(Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "windResistanceCoefficient"    # Ljava/math/BigDecimal;
    .param p2, "windSpeed"    # Ljava/lang/Integer;
    .param p3, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p4, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4300
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetWindResistance(Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetWindResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 7
    .param p1, "frontalSurfaceArea"    # Ljava/math/BigDecimal;
    .param p2, "dragCoefficient"    # Ljava/math/BigDecimal;
    .param p3, "airDensity"    # Ljava/math/BigDecimal;
    .param p4, "windSpeed"    # Ljava/lang/Integer;
    .param p5, "draftingFactor"    # Ljava/math/BigDecimal;
    .param p6, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 4335
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestSetWindResistance(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/Integer;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestTargetPower(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    .line 4249
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestTargetPower(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestTrackResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    .line 4350
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestTrackResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestWindResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    .line 4279
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestWindResistance(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z
    .locals 1
    .param p1, "basicResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;

    .prologue
    .line 4153
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeBasicResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBasicResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeCalculatedTrainerDistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;)Z
    .locals 3
    .param p1, "calculatedTrainerDistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;

    .prologue
    const/16 v2, 0xd8

    .line 4084
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 4086
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeCalculatedTrainerDistanceEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4087
    const/4 v0, 0x0

    .line 4098
    :goto_0
    return v0

    .line 4090
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerDistanceReceiver;

    .line 4091
    if-eqz p1, :cond_1

    .line 4093
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 4097
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 4098
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeCalculatedTrainerPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;)Z
    .locals 3
    .param p1, "calculatedTrainerPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;

    .prologue
    const/16 v2, 0xd9

    .line 4028
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 4030
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeCalculatedTrainerPowerEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4031
    const/4 v0, 0x0

    .line 4042
    :goto_0
    return v0

    .line 4034
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerPowerReceiver;

    .line 4035
    if-eqz p1, :cond_1

    .line 4037
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 4041
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 4042
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeCalculatedTrainerSpeedEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;)Z
    .locals 3
    .param p1, "calculatedTrainerSpeedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;

    .prologue
    const/16 v2, 0xd7

    .line 4056
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 4058
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeCalculatedTrainerSpeedEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4059
    const/4 v0, 0x0

    .line 4070
    :goto_0
    return v0

    .line 4062
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mCalculatedTrainerSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;

    .line 4063
    if-eqz p1, :cond_1

    .line 4065
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 4069
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 4070
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z
    .locals 1
    .param p1, "commandStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;

    .prologue
    .line 4140
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeCommandStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICommandStatusReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeRawTrainerDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;)Z
    .locals 3
    .param p1, "rawTrainerDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;

    .prologue
    const/16 v2, 0xd4

    .line 3972
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$1600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 3974
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeRawTrainerDataEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$1700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 3975
    const/4 v0, 0x0

    .line 3986
    :goto_0
    return v0

    .line 3978
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerDataReceiver;

    .line 3979
    if-eqz p1, :cond_1

    .line 3981
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$1800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 3985
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$1900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 3986
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeRawTrainerTorqueDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;)Z
    .locals 3
    .param p1, "rawTrainerTorqueDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;

    .prologue
    const/16 v2, 0xd6

    .line 4000
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 4002
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeRawTrainerTorqueDataEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4003
    const/4 v0, 0x0

    .line 4014
    :goto_0
    return v0

    .line 4006
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRawTrainerTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRawTrainerTorqueDataReceiver;

    .line 4007
    if-eqz p1, :cond_1

    .line 4009
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 4013
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$2300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 4014
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z
    .locals 1
    .param p1, "targetPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;

    .prologue
    .line 4166
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeTargetPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITargetPowerReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z
    .locals 1
    .param p1, "trackResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;

    .prologue
    .line 4192
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeTrackResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrackResistanceReceiver;)Z

    move-result v0

    return v0
.end method

.method public subscribeTrainerStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;)Z
    .locals 3
    .param p1, "trainerStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;

    .prologue
    const/16 v2, 0xd5

    .line 4112
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v0

    const/16 v1, 0x4ef1

    if-ge v0, v1, :cond_0

    .line 4114
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subscribeTrainerStatusEvent requires ANT+ Plugins Service >20209, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->reportedServiceVersion:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4115
    const/4 v0, 0x0

    .line 4126
    :goto_0
    return v0

    .line 4118
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    iput-object p1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTrainerStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITrainerStatusReceiver;

    .line 4119
    if-eqz p1, :cond_1

    .line 4121
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z

    move-result v0

    goto :goto_0

    .line 4125
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V
    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->access$3900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V

    .line 4126
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z
    .locals 1
    .param p1, "windResistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;

    .prologue
    .line 4179
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerMethods;->this$0:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeWindResistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IWindResistanceReceiver;)Z

    move-result v0

    return v0
.end method
