.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostLinkChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask$1;
    }
.end annotation


# static fields
.field static BEACON_AUTH:I

.field static BEACON_ID:B

.field static BEACON_LINK:I

.field static BEACON_UNKNOWN:I

.field private static final TAG:Ljava/lang/String;

.field static final freqRandom:Ljava/util/Random;

.field static final transportFrequencyList:[B


# instance fields
.field beaconDevType:I

.field beaconMfgId:I

.field beaconState:I

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field isLinkCommandSent:Z

.field linkCommand:[B

.field mBeaconInterval:I

.field mLinkChannelPeriod:I

.field mLinkRadioFreq:I

.field mSerialNumber:J

.field mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field msgRetries:I

.field transferInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    .line 27
    const/16 v0, 0x43

    sput-byte v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_ID:B

    .line 28
    const/4 v0, -0x1

    sput v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    .line 29
    const/4 v0, 0x0

    sput v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_LINK:I

    .line 30
    const/4 v0, 0x1

    sput v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_AUTH:I

    .line 47
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transportFrequencyList:[B

    .line 48
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->freqRandom:Ljava/util/Random;

    return-void

    .line 47
    :array_0
    .array-data 1
        0x3t
        0x7t
        0xft
        0x14t
        0x19t
        0x1dt
        0x22t
        0x28t
        0x2dt
        0x31t
        0x36t
        0x3ct
        0x41t
        0x46t
        0x4bt
        0x50t
    .end array-data
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JIII)V
    .locals 2
    .param p1, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "serialNumber"    # J
    .param p4, "linkRF"    # I
    .param p5, "linkPeriod"    # I
    .param p6, "beaconInterval"    # I

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 35
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mBeaconInterval:I

    .line 37
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->LINK_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 40
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    .line 41
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconMfgId:I

    .line 42
    sget v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconDevType:I

    .line 43
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->isLinkCommandSent:Z

    .line 44
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    .line 63
    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mSerialNumber:J

    .line 64
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mLinkRadioFreq:I

    .line 65
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mLinkChannelPeriod:I

    .line 66
    iput p6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mBeaconInterval:I

    .line 67
    return-void

    .line 50
    nop

    :array_0
    .array-data 1
        0x44t
        0x2t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public doWork()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 216
    :try_start_0
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->LINK_CONNECTING:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 217
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_REQUESTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 218
    const/4 v2, 0x0

    .line 219
    .local v2, "retryCount":I
    const/16 v3, 0xa

    .line 220
    .local v3, "retryLimit":I
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mBeaconInterval:I

    if-eqz v4, :cond_0

    .line 221
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mBeaconInterval:I

    int-to-double v4, v4

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 225
    :cond_0
    :goto_0
    add-int/lit8 v2, v2, 0x1

    .line 226
    if-le v2, v3, :cond_1

    .line 228
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Failed: Could not link before retry limit reached"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 230
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 231
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 344
    .end local v2    # "retryCount":I
    .end local v3    # "retryLimit":I
    :goto_1
    return-void

    .line 235
    .restart local v2    # "retryCount":I
    .restart local v3    # "retryLimit":I
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    .line 236
    .local v0, "channelState":Lcom/dsi/ant/message/ChannelState;
    sget-object v4, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v4, :cond_3

    sget-object v4, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v4, :cond_3

    .line 238
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mBeaconInterval:I

    if-nez v4, :cond_2

    .line 241
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Failed: Connection lost"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 243
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 244
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 334
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .end local v2    # "retryCount":I
    .end local v3    # "retryLimit":I
    :catch_0
    move-exception v1

    .line 336
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACFE occurred: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v4, Landroid/os/RemoteException;

    invoke-direct {v4}, Landroid/os/RemoteException;-><init>()V

    throw v4

    .line 250
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .restart local v2    # "retryCount":I
    .restart local v3    # "retryLimit":I
    :cond_2
    :try_start_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Restarting search..."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 256
    :cond_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mLinkChannelPeriod:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 257
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mLinkRadioFreq:I

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 260
    sget v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    .line 261
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Wait for link beacon"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 263
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->enableMessageProcessing()V

    .line 264
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v5, 0xa

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 266
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_LINK:I

    if-eq v4, v5, :cond_4

    .line 268
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Time out waiting for link beacon."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 338
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .end local v2    # "retryCount":I
    .end local v3    # "retryLimit":I
    :catch_1
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Interrupted waiting for result"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 342
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_1

    .line 273
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .restart local v2    # "retryCount":I
    .restart local v3    # "retryLimit":I
    :cond_4
    :try_start_2
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Sending link command"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->isLinkCommandSent:Z

    .line 275
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    .line 276
    const/4 v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    .line 278
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    const/4 v5, 0x4

    iget-wide v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mSerialNumber:J

    invoke-static {v4, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 279
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    const/4 v5, 0x2

    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transportFrequencyList:[B

    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->freqRandom:Ljava/util/Random;

    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transportFrequencyList:[B

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 282
    :try_start_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 299
    :goto_2
    :try_start_4
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 300
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->enableMessageProcessing()V

    .line 301
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v5, 0xa

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 303
    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->isLinkCommandSent:Z

    if-nez v4, :cond_6

    .line 305
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Link Tx retries exceeded"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    :catch_2
    move-exception v1

    .line 286
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v4

    sget-object v5, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v4, v5, :cond_5

    .line 290
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Failed to send Link Command: TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 294
    :cond_5
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to send Link Command: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 310
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_6
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v5, 0x1000

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 311
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    const/4 v6, 0x2

    aget-byte v5, v5, v6

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 314
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Wait for auth beacon"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 316
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->enableMessageProcessing()V

    .line 317
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v5, 0xa

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 319
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_AUTH:I

    if-eq v4, v5, :cond_7

    .line 321
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v5, "Time out waiting for auth beacon."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :cond_7
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->AUTH_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 330
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->LINK_PASS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 331
    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1
.end method

.method public getClientDeviceType()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconDevType:I

    return v0
.end method

.method public getClientManufacturerId()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconMfgId:I

    return v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    const-string v0, "ANT-FS Host Link Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 355
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->LINK_IDLE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-ne p1, v0, :cond_1

    .line 356
    :cond_0
    const/4 v0, 0x1

    .line 358
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 89
    :pswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v4, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v4, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 92
    :pswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timeout occured"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_6

    .line 198
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 99
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 102
    :pswitch_3
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->isLinkCommandSent:Z

    .line 103
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    .line 104
    const/4 v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    .line 105
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 106
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 113
    :pswitch_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    goto :goto_0

    .line 122
    :pswitch_5
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_UNKNOWN:I

    if-ne v3, v4, :cond_2

    .line 125
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_0

    .line 127
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    .line 128
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_LINK:I

    if-eq v3, v4, :cond_1

    .line 133
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Beacon in incorrect state"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsManufacturerId([B)I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconMfgId:I

    .line 138
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsDeviceType([B)I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconDevType:I

    .line 140
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 141
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 146
    :cond_2
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->isLinkCommandSent:Z

    if-nez v3, :cond_4

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_LINK:I

    if-ne v3, v4, :cond_4

    .line 148
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    .line 149
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    const/16 v4, 0x1e

    if-le v3, v4, :cond_3

    .line 152
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 153
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 155
    :cond_3
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->msgRetries:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_0

    .line 157
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->transferInProgress:Z

    .line 158
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Retrying link command"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->linkCommand:[B

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto/16 :goto_0

    .line 164
    :cond_4
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I

    sget v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_LINK:I

    if-ne v3, v4, :cond_0

    .line 167
    sget-byte v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_ID:B

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_0

    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_AUTH:I

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_0

    .line 170
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v1

    .line 171
    .local v1, "hostSerial":J
    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->mSerialNumber:J

    cmp-long v3, v1, v3

    if-eqz v3, :cond_5

    .line 173
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    const-string v4, "Serial number mismatch - client connected to another host"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :goto_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 180
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 177
    :cond_5
    sget v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->BEACON_AUTH:I

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->beaconState:I
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 202
    .end local v1    # "hostSerial":J
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_6
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->disableMessageProcessing()V

    .line 204
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostLinkChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 89
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
