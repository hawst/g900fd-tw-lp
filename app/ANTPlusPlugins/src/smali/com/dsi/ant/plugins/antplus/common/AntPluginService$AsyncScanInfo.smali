.class public Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
.super Ljava/lang/Object;
.source "AntPluginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AsyncScanInfo"
.end annotation


# instance fields
.field public client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field public currentResultHandler:Landroid/os/Messenger;

.field public executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 0
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 153
    return-void
.end method


# virtual methods
.method public closeAsyncScan()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 166
    :cond_0
    return-void
.end method

.method public getCurrentResultHandler()Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    return-object v0
.end method
