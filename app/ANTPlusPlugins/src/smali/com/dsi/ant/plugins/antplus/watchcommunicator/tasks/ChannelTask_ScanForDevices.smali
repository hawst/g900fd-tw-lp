.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_ScanForDevices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$1;,
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field static final mDevType:I = 0x0

.field static final mPeriod:I = 0x2000

.field static final mRfFreq:I = 0x32

.field static final mTransType:I


# instance fields
.field cancelLock:Ljava/lang/Object;

.field cancelled:Z

.field currentIeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;",
            ">;"
        }
    .end annotation
.end field

.field deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

.field ieIndex:I

.field private final watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)V
    .locals 1
    .param p1, "watchDownloaderController"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelLock:Ljava/lang/Object;

    .line 53
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    .line 54
    return-void
.end method

.method private addToIeList(Lcom/dsi/ant/message/ChannelId;)V
    .locals 3
    .param p1, "newDev"    # Lcom/dsi/ant/message/ChannelId;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 332
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 333
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 335
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 336
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;

    invoke-direct {v1, p1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;-><init>(Lcom/dsi/ant/message/ChannelId;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 340
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    invoke-virtual {v0, p1, v1}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    .line 343
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 345
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    .line 351
    :goto_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IE list using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " devices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_0
.end method

.method private closeIeList()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 358
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :cond_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE trying to clear IE list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method

.method private initIeList()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 265
    const/4 v9, -0x1

    iput v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 266
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    iput-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    .line 268
    const-wide/16 v3, -0x1

    .line 271
    .local v3, "lastFoundTime":J
    sget-object v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v10, "IE list init - current devs: (ID, time, DlFinish) "

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v10, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v10

    .line 274
    :try_start_0
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 275
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    sget-object v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "   "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v12}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->getTimeSinceLastSeen()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 280
    :cond_1
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v9

    const/4 v10, 0x4

    if-ge v9, v10, :cond_4

    .line 282
    const-wide/16 v5, 0x2710

    .line 283
    .local v5, "lowest":J
    const/4 v0, 0x0

    .line 286
    .local v0, "devNextLowest":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v10, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v10

    .line 288
    :try_start_2
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 290
    .restart local v1    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->getTimeSinceLastSeen()J

    move-result-wide v7

    .line 293
    .local v7, "timeSince":J
    cmp-long v9, v7, v3

    if-lez v9, :cond_2

    cmp-long v9, v7, v5

    if-gez v9, :cond_2

    .line 295
    move-wide v5, v7

    .line 296
    move-object v0, v1

    goto :goto_1

    .line 299
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v7    # "timeSince":J
    :cond_3
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 301
    if-eqz v0, :cond_4

    .line 303
    move-wide v3, v5

    .line 304
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    new-instance v10, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v10, v11}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;-><init>(Lcom/dsi/ant/message/ChannelId;)V

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 311
    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v9, :cond_1

    .line 328
    .end local v0    # "devNextLowest":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v5    # "lowest":J
    :goto_2
    return-void

    .line 299
    .restart local v0    # "devNextLowest":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .restart local v5    # "lowest":J
    :catchall_1
    move-exception v9

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v9

    .line 316
    .end local v0    # "devNextLowest":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v5    # "lowest":J
    :cond_4
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;

    .line 318
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;
    iget v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 320
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v10, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    iget v11, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    invoke-virtual {v9, v10, v11}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    .line 322
    iget-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v9, :cond_5

    goto :goto_2

    .line 327
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$IeListEntry;
    :cond_6
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->currentIeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    goto :goto_2
.end method


# virtual methods
.method public doWork()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->setCurrentState(I)V

    .line 190
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->flushAndEnsureUnassignedChannel()V

    .line 191
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-nez v7, :cond_0

    .line 193
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v8, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 194
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v8, 0x32

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 195
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/16 v8, 0x2000

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 196
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v8, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIFTEEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->initIeList()V

    .line 200
    :goto_0
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-nez v7, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->flushAndEnsureClosedChannel()V

    .line 204
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 206
    :try_start_1
    new-instance v7, Ljava/util/concurrent/CountDownLatch;

    const/4 v9, 0x1

    invoke-direct {v7, v9}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    .line 207
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    :try_start_2
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v7, :cond_2

    .line 254
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->closeIeList()V

    .line 255
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->close()V

    .line 256
    return-void

    .line 207
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v7
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    .line 242
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACFE handling message: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 247
    new-instance v7, Landroid/os/RemoteException;

    invoke-direct {v7}, Landroid/os/RemoteException;-><init>()V

    throw v7

    .line 212
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    :try_start_5
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v8, Lcom/dsi/ant/message/ChannelId;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v8, v9, v10, v11}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 213
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->enableMessageProcessing()V

    .line 214
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 216
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-nez v7, :cond_1

    .line 219
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v7}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 221
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-nez v7, :cond_1

    .line 226
    const-wide/32 v3, 0x15f90

    .line 227
    .local v3, "limitTime":J
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v8, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    monitor-enter v8
    :try_end_5
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 229
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    :try_start_6
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 231
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->currentDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 232
    .local v0, "dev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->getTimeSinceLastSeen()J

    move-result-wide v5

    .line 233
    .local v5, "timeSince":J
    cmp-long v7, v5, v3

    if-lez v7, :cond_3

    .line 235
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    invoke-virtual {v7, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->reportDeviceExpired(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V

    .line 236
    add-int/lit8 v2, v2, -0x1

    .line 229
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 239
    .end local v0    # "dev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .end local v5    # "timeSince":J
    :cond_4
    monitor-exit v8

    goto/16 :goto_0

    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v7
    :try_end_7
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1

    .line 249
    .end local v2    # "i":I
    .end local v3    # "limitTime":J
    :catch_1
    move-exception v1

    .line 251
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    const-string v0, "ANTFS Watch Scan"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 94
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->handleInterruptRequest(I)Z

    .line 95
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 3
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v2, 0x1

    .line 81
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->disableMessageProcessing()V

    .line 82
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelLock:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    .line 85
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 87
    :cond_0
    monitor-exit v1

    .line 88
    return v2

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initTask()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->open()V

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    .line 181
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 182
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    :try_start_0
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 106
    :pswitch_0
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v5, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v5, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 109
    :pswitch_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v5, "Search timeout occured"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    :catch_0
    move-exception v1

    .line 161
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACFE handling message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 164
    new-instance v4, Landroid/os/RemoteException;

    invoke-direct {v4}, Landroid/os/RemoteException;-><init>()V

    throw v4

    .line 112
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v5, "Channel closed"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v5, Lcom/dsi/ant/message/ChannelId;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v4, v5}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 116
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->open()V

    goto :goto_0

    .line 127
    :pswitch_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v4}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v2

    .line 128
    .local v2, "id":Lcom/dsi/ant/message/ChannelId;
    invoke-direct {p0, v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->addToIeList(Lcom/dsi/ant/message/ChannelId;)V

    .line 130
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    const/16 v5, 0x43

    if-ne v4, v5, :cond_2

    .line 132
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->deviceList:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v4, v2, v5, v6}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;->reportDeviceFound(Lcom/dsi/ant/message/ChannelId;[BLcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    move-result-object v0

    .line 133
    .local v0, "dev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    if-eqz v0, :cond_1

    .line 135
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->watchDownloaderController:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->newActivityListeners:Ljava/util/Map;

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    .line 137
    .local v3, "listener":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    if-eqz v3, :cond_1

    .line 139
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getDataAvailableFlag([B)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->reportNewDataFlagSeen()V

    .line 141
    :cond_0
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->checkNeedsDownload()V

    .line 145
    .end local v3    # "listener":Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;
    :cond_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->disableMessageProcessing()V

    .line 146
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 150
    .end local v0    # "dev":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    :cond_2
    sget-object v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v5, "Message not a beacon, ignoring"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->disableMessageProcessing()V

    .line 152
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_ScanForDevices;->deviceFoundLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 106
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
