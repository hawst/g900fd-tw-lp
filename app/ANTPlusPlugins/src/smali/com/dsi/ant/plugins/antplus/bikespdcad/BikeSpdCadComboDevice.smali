.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "BikeSpdCadComboDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$1;,
        Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    }
.end annotation


# instance fields
.field private dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

.field private devicesSubscribedAsMode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;",
            ">;"
        }
    .end annotation
.end field

.field private nextAddIsCadenceRequest:Ljava/lang/Boolean;

.field private nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

.field private parentService:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "parentService"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 26
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextAddIsCadenceRequest:Ljava/lang/Boolean;

    .line 27
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    .line 42
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->parentService:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    .line 43
    return-void
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 2
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 107
    iget v0, p2, Landroid/os/Message;->what:I

    const/16 v1, 0x2712

    if-ne v0, v1, :cond_0

    .line 108
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "bool_IsCadencePcc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    .line 110
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 111
    return-void
.end method

.method public addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 6
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 80
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextAddIsCadenceRequest:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 81
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "prepForClientAddOrRemove() must be called before client is added"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 84
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    monitor-enter v3

    .line 86
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextAddIsCadenceRequest:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 87
    .local v0, "currentAddMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const/4 v1, 0x1

    .line 98
    .local v1, "ret":Z
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextAddIsCadenceRequest:Ljava/lang/Boolean;

    .line 100
    return v1

    .line 86
    .end local v0    # "currentAddMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .end local v1    # "ret":Z
    :cond_1
    :try_start_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    goto :goto_0

    .line 94
    .restart local v0    # "currentAddMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v1

    .restart local v1    # "ret":Z
    goto :goto_1

    .line 98
    .end local v0    # "currentAddMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .end local v1    # "ret":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public getPageList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 57
    return-void
.end method

.method public hasAccess(Ljava/lang/String;)Z
    .locals 6
    .param p1, "appNamePkg"    # Ljava/lang/String;

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->connectedClients:Ljava/util/ArrayList;

    monitor-enter v5

    .line 198
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->connectedClients:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 200
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 202
    move-object v0, v2

    .line 206
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    if-eqz v0, :cond_4

    .line 210
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 211
    .local v1, "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-eq v1, v4, :cond_3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->parentService:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-eq v1, v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->parentService:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->isLastAccessRequestCadencePcc()Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-ne v1, v4, :cond_4

    .line 215
    :cond_3
    const/4 v4, 0x1

    .line 219
    .end local v1    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :goto_0
    return v4

    .line 206
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 219
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isAccessCollision(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;)Z
    .locals 4
    .param p1, "clientRequestingAccess"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "modeToConnect"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .prologue
    .line 61
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    monitor-enter v2

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 64
    .local v0, "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-eq v1, v0, :cond_0

    if-ne p2, v0, :cond_1

    .line 66
    :cond_0
    const/4 v1, 0x1

    monitor-exit v2

    .line 68
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 69
    .end local v0    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public prepTypeForClientAdd(Z)V
    .locals 1
    .param p1, "isCadenceRequest"    # Z

    .prologue
    .line 74
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextAddIsCadenceRequest:Ljava/lang/Boolean;

    .line 75
    return-void
.end method

.method public removeClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 10
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 145
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    if-nez v6, :cond_0

    .line 146
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Can\'t call BikeSpdCad removeClient without prep\'ing deviceType"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 148
    :cond_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 149
    .local v5, "removeMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :goto_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    monitor-enter v7

    .line 151
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v8, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 152
    .local v0, "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    if-eqz v0, :cond_1

    .line 154
    if-ne v5, v0, :cond_3

    .line 156
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->removeClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 157
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 159
    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v9, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v6, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :cond_1
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 189
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    .line 190
    return-void

    .line 148
    .end local v0    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .end local v5    # "removeMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :cond_2
    sget-object v5, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    goto :goto_0

    .line 160
    .restart local v0    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    .restart local v5    # "removeMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6

    .line 187
    .end local v0    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v6

    .line 162
    .restart local v0    # "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :cond_3
    :try_start_5
    sget-object v6, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->BOTH:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-ne v0, v6, :cond_1

    .line 164
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->nextRemoveIsCadenceRequest:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 165
    .local v4, "newMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :goto_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v8, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v6, v8, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const/4 v1, 0x0

    .line 169
    .local v1, "eventsToRemoveFrom":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    sget-object v6, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-ne v5, v6, :cond_6

    .line 171
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->getCadenceEventList()Ljava/util/List;

    move-result-object v1

    .line 177
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 179
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v6, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v2, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->unsubscribeFromEvent(Ljava/util/UUID;)Z

    goto :goto_3

    .line 164
    .end local v1    # "eventsToRemoveFrom":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "newMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :cond_5
    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->CADENCE_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    goto :goto_1

    .line 173
    .restart local v1    # "eventsToRemoveFrom":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    .restart local v4    # "newMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    :cond_6
    sget-object v6, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->SPEED_ONLY:Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    if-ne v5, v6, :cond_4

    .line 175
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->getSpeedEventList()Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v1

    goto :goto_2
.end method

.method public subscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 6
    .param p1, "eventID"    # I
    .param p2, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->devicesSubscribedAsMode:Ljava/util/HashMap;

    iget-object v5, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;

    .line 117
    .local v0, "currentMode":Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$1;->$SwitchMap$com$dsi$ant$plugins$antplus$bikespdcad$BikeSpdCadComboDevice$ConnectionMode:[I

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice$ConnectionMode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 139
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->subscribeClientToEvent(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    move-result v3

    :goto_0
    return v3

    .line 121
    :pswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->getSpeedEventList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 123
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v4, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne p1, v4, :cond_1

    goto :goto_0

    .line 129
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    .end local v2    # "i$":Ljava/util/Iterator;
    :pswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/BikeSpdCadComboDevice;->dataPage:Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/P0_SpdCadCombinedDefault;->getCadenceEventList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 131
    .restart local v1    # "i":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v4, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne p1, v4, :cond_2

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
