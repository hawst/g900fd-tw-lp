.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

.field final synthetic val$channelOneSearchInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->val$channelOneSearchInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 363
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isSingleSearchInProgress:Z

    .line 364
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResultCode:I
    invoke-static {v0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$702(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;I)I

    .line 365
    const/16 v0, 0xa

    if-eq p1, v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 369
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->val$channelOneSearchInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->val$channelOneSearchInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 371
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :goto_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iput-object p2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->singleSearchResult:Lcom/dsi/ant/message/ChannelId;

    .line 376
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mLatch_DevConnected:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 377
    return-void

    .line 371
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 374
    :cond_1
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Single search result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
