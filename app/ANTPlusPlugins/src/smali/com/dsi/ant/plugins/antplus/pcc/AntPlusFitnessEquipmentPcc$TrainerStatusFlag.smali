.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
.super Ljava/lang/Enum;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrainerStatusFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum BICYCLE_POWER_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum MAXIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum MINIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum RESISTANCE_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

.field public static final enum USER_CONFIGURATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;


# instance fields
.field private final longValue:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 257
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "UNRECOGNIZED_FLAG_PRESENT"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 262
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "BICYCLE_POWER_CALIBRATION_REQUIRED"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->BICYCLE_POWER_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 267
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "RESISTANCE_CALIBRATION_REQUIRED"

    const-wide/16 v2, 0x4

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->RESISTANCE_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 272
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "USER_CONFIGURATION_REQUIRED"

    const-wide/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->USER_CONFIGURATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 277
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "MAXIMUM_POWER_LIMIT_REACHED"

    const-wide/16 v2, 0x10

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MAXIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 282
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    const-string v1, "MINIMUM_POWER_LIMIT_REACHED"

    const/4 v2, 0x5

    const-wide/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MINIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 252
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->BICYCLE_POWER_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->RESISTANCE_CALIBRATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->USER_CONFIGURATION_REQUIRED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v1, v0, v8

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MAXIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->MINIMUM_POWER_LIMIT_REACHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .param p3, "longValue"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 288
    iput-wide p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->longValue:J

    .line 289
    return-void
.end method

.method public static getEnumSet(J)Ljava/util/EnumSet;
    .locals 9
    .param p0, "longValue"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307
    const-class v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-static {v7}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v6

    .line 309
    .local v6, "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 311
    .local v1, "flag":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->getLongValue()J

    move-result-wide v2

    .line 313
    .local v2, "flagValue":J
    and-long v7, v2, p0

    cmp-long v7, v7, v2

    if-nez v7, :cond_0

    .line 315
    invoke-virtual {v6, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 316
    sub-long/2addr p0, v2

    .line 309
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 320
    .end local v1    # "flag":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    .end local v2    # "flagValue":J
    :cond_1
    const-wide/16 v7, 0x0

    cmp-long v7, p0, v7

    if-eqz v7, :cond_2

    .line 321
    sget-object v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v6, v7}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 323
    :cond_2
    return-object v6
.end method

.method public static getLong(Ljava/util/EnumSet;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 333
    .local p0, "trainerStatusFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;>;"
    const-wide/16 v2, 0x0

    .line 335
    .local v2, "longValue":J
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    .line 336
    .local v0, "flag":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->getLongValue()J

    move-result-wide v4

    or-long/2addr v2, v4

    goto :goto_0

    .line 338
    .end local v0    # "flag":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    :cond_0
    return-wide v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 252
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;
    .locals 1

    .prologue
    .line 252
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;

    return-object v0
.end method


# virtual methods
.method public getLongValue()J
    .locals 2

    .prologue
    .line 297
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerStatusFlag;->longValue:J

    return-wide v0
.end method
