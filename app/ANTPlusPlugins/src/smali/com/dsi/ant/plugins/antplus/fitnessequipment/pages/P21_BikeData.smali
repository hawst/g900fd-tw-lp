.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P21_BikeData.java"


# instance fields
.field private bikeDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V
    .locals 2
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 19
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->bikeDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 26
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 27
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->bikeDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 80
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v6, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v6, v0, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 54
    const-string v0, "long_EventFlags"

    invoke-virtual {v6, v0, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 57
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 60
    .local v7, "receivedCadence":I
    const/16 v0, 0xff

    if-ne v7, v0, :cond_1

    .line 61
    const/4 v7, -0x1

    .line 64
    :cond_1
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v8

    .line 67
    .local v8, "receivedPower":I
    const v0, 0xffff

    if-ne v8, v0, :cond_2

    .line 68
    const/4 v8, -0x1

    .line 70
    :cond_2
    const-string v0, "int_instantaneousCadence"

    invoke-virtual {v6, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    const-string v0, "int_instantaneousPower"

    invoke-virtual {v6, v0, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->bikeDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 79
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->bikeDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P21_BikeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->onDropToSearch()V

    .line 87
    return-void
.end method
