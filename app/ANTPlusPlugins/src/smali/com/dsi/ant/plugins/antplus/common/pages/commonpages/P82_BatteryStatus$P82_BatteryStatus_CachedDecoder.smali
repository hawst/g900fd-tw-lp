.class public Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;
.super Ljava/lang/Object;
.source "P82_BatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "P82_BatteryStatus_CachedDecoder"
.end annotation


# instance fields
.field public batteryIdentifier:I

.field public batteryStatusCode:I

.field public batteryVoltage:Ljava/math/BigDecimal;

.field public cumulativeOperatingTime:J

.field public cumulativeOperatingTimeResolution:I

.field public numberOfBatteries:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0xf

    const/4 v5, -0x1

    const/16 v4, 0x8

    .line 67
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom3LeBytes([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTime:J

    .line 69
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v4

    and-int/lit8 v0, v0, 0xf

    if-eq v0, v6, :cond_2

    .line 70
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v4

    and-int/lit8 v1, v1, 0xf

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v2, Ljava/math/BigDecimal;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v1, v2, v4, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryVoltage:Ljava/math/BigDecimal;

    .line 75
    :goto_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v4

    and-int/lit8 v0, v0, 0x70

    shr-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryStatusCode:I

    .line 76
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v4

    and-int/lit16 v0, v0, 0x80

    if-lez v0, :cond_3

    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->cumulativeOperatingTimeResolution:I

    .line 78
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v7

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->numberOfBatteries:I

    .line 79
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->numberOfBatteries:I

    if-ne v0, v6, :cond_0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->numberOfBatteries:I

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v7

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryIdentifier:I

    .line 82
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryIdentifier:I

    if-ne v0, v6, :cond_1

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryIdentifier:I

    .line 83
    :cond_1
    return-void

    .line 74
    :cond_2
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v5}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/pages/commonpages/P82_BatteryStatus$P82_BatteryStatus_CachedDecoder;->batteryVoltage:Ljava/math/BigDecimal;

    goto :goto_0

    .line 76
    :cond_3
    const/16 v0, 0x10

    goto :goto_1
.end method
