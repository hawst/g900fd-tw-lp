.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;
.super Ljava/lang/Object;
.source "AntPlusWeightScalePcc.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserProfile"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_DEFAULT_USERPROFILEKEY:Ljava/lang/String; = "parcelable_UserProfile"


# instance fields
.field public activityLevel:I

.field public age:I

.field public gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

.field public height:I

.field private final ipcVersionNumber:I

.field public lifetimeAthlete:Z

.field private final userProfileID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 183
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    .line 187
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    .line 191
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    .line 195
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    .line 203
    const/4 v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->ipcVersionNumber:I

    .line 206
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 207
    .local v0, "r":Ljava/util/Random;
    const v1, 0xfeff

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->userProfileID:I

    .line 208
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 183
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    .line 187
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    .line 191
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    .line 195
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    .line 216
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->ipcVersionNumber:I

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 218
    .local v0, "incomingVersion":I
    if-eq v0, v2, :cond_0

    .line 219
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Decoding version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UserProfile parcel with version 1 parser."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->userProfileID:I

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    .line 227
    return-void

    :cond_1
    move v1, v2

    .line 226
    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    return v0
.end method

.method public getUserProfileID()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->userProfileID:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 232
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->ipcVersionNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->userProfileID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 234
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->getIntValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 235
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 236
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 237
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 239
    return-void

    .line 238
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
