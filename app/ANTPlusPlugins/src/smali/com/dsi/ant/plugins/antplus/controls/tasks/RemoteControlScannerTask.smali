.class public Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;
.super Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;
.source "RemoteControlScannerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDeviceFoundCount:Landroid/util/SparseIntArray;

.field private mExclusionListCutoff:I

.field private targetEndTime_ms:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/EnumSet;IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    .locals 9
    .param p2, "rfFreq"    # I
    .param p3, "period"    # I
    .param p4, "devType"    # I
    .param p5, "transType"    # I
    .param p6, "proximityThreshold"    # I
    .param p7, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;IIIII",
            "Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;-><init>(Ljava/util/EnumSet;IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 47
    return-void
.end method

.method private getLowestSatisfyingTimeoutMaxTen(J)Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .locals 2
    .param p1, "seconds"    # J

    .prologue
    .line 236
    const-wide/16 v0, 0x1d4c

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 237
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 243
    :goto_0
    return-object v0

    .line 238
    :cond_0
    const-wide/16 v0, 0x1388

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 239
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->SEVEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    goto :goto_0

    .line 240
    :cond_1
    const-wide/16 v0, 0x9c4

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 241
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    goto :goto_0

    .line 243
    :cond_2
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    goto :goto_0
.end method

.method private updateExclusionList(Lcom/dsi/ant/message/ChannelId;)V
    .locals 10
    .param p1, "deviceChanID"    # Lcom/dsi/ant/message/ChannelId;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v9, 0x4

    .line 250
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    invoke-virtual {v4, v5, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 251
    .local v0, "connectCount":I
    if-ne v0, v6, :cond_1

    .line 253
    const/4 v0, 0x1

    .line 254
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 256
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-gt v4, v9, :cond_0

    .line 258
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, p1, v5}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    .line 259
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    .line 297
    :cond_0
    return-void

    .line 265
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 268
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-le v4, v9, :cond_0

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    add-int/lit8 v4, v4, 0x1

    if-ne v0, v4, :cond_0

    .line 270
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    .line 272
    const/4 v1, 0x0

    .line 273
    .local v1, "curListIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    if-ge v1, v9, :cond_3

    .line 275
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    if-lt v4, v5, :cond_2

    .line 277
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v5, Lcom/dsi/ant/message/ChannelId;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v7}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v7

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v8}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "curListIndex":I
    .local v2, "curListIndex":I
    invoke-virtual {v4, v5, v1}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    move v1, v2

    .line 273
    .end local v2    # "curListIndex":I
    .restart local v1    # "curListIndex":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 281
    :cond_3
    if-ge v1, v9, :cond_0

    .line 283
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    .line 286
    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    if-ge v1, v9, :cond_0

    .line 288
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mExclusionListCutoff:I

    if-ne v4, v5, :cond_4

    .line 290
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v5, Lcom/dsi/ant/message/ChannelId;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v7}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v7

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mChanId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v8}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "curListIndex":I
    .restart local v2    # "curListIndex":I
    invoke-virtual {v4, v5, v1}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    move v1, v2

    .line 286
    .end local v2    # "curListIndex":I
    .restart local v1    # "curListIndex":I
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public doWork()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, -0x4

    const/4 v5, 0x0

    .line 131
    const/4 v1, 0x0

    .line 132
    .local v1, "interrupted":Z
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    .line 133
    new-instance v5, Landroid/util/SparseIntArray;

    invoke-direct {v5}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mDeviceFoundCount:Landroid/util/SparseIntArray;

    .line 134
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const-wide/16 v7, 0x2710

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->targetEndTime_ms:J

    .line 136
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->initSearch()Z

    move-result v5

    if-nez v5, :cond_3

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 164
    .local v2, "timeLeft_ms":J
    .local v4, "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    :cond_1
    :try_start_0
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mModesMatchCapabilities:Z

    if-eqz v5, :cond_2

    .line 166
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResultRSSI:Ljava/lang/Integer;

    const/4 v8, 0x0

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->reportResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;Z)V

    .line 170
    :cond_2
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->flushAndEnsureClosedChannel()V

    .line 172
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setAudioControlSupported(Z)V

    .line 173
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setBurstCommandSupported(Z)V

    .line 174
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setGenericControlSupported(Z)V

    .line 175
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setVideoControlSupported(Z)V

    .line 176
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setMaximumNumberRemotesConnected(Z)V

    .line 177
    const/4 v5, 0x0

    iput v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mReceivedPageCounter:I

    .line 178
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilitiesDetermined:Z

    .line 179
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mModesMatchCapabilities:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    .end local v2    # "timeLeft_ms":J
    .end local v4    # "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    :cond_3
    :try_start_1
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v5, :cond_7

    .line 143
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 144
    iget-wide v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->targetEndTime_ms:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    sub-long v2, v5, v7

    .line 145
    .restart local v2    # "timeLeft_ms":J
    const-wide/16 v5, 0x3e8

    cmp-long v5, v2, v5

    if-gez v5, :cond_4

    .line 147
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->reportFailure(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-nez v5, :cond_0

    .line 194
    :try_start_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACFE resetting inclusion/exclusion list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5

    .line 151
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_4
    :try_start_3
    invoke-direct {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->getLowestSatisfyingTimeoutMaxTen(J)Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    move-result-object v4

    .line 152
    .restart local v4    # "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->enableMessageProcessing()V

    .line 153
    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->setPerSearchParamsAndOpenSearch(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v5

    if-nez v5, :cond_5

    .line 190
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-nez v5, :cond_0

    .line 194
    :try_start_4
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 195
    :catch_1
    move-exception v0

    .line 197
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACFE resetting inclusion/exclusion list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5

    .line 158
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    :try_start_5
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v5, :cond_6

    .line 159
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 161
    :cond_6
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v5, :cond_7

    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v5, :cond_1

    .line 190
    .end local v2    # "timeLeft_ms":J
    .end local v4    # "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    :cond_7
    :goto_1
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-nez v5, :cond_8

    .line 194
    :try_start_6
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_6
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_6 .. :try_end_6} :catch_3

    .line 203
    :cond_8
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    if-nez v5, :cond_a

    .line 205
    :cond_9
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->dumbfireCloseChannel()V

    .line 208
    :cond_a
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-eqz v5, :cond_c

    .line 209
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5

    .line 180
    .restart local v2    # "timeLeft_ms":J
    .restart local v4    # "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    :catch_2
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_7
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->disableMessageProcessing()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 183
    const/4 v1, 0x1

    .line 184
    goto :goto_1

    .line 195
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "timeLeft_ms":J
    .end local v4    # "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    :catch_3
    move-exception v0

    .line 197
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACFE resetting inclusion/exclusion list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5

    .line 190
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catchall_0
    move-exception v5

    iget-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    if-nez v6, :cond_b

    .line 194
    :try_start_8
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_8
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_8 .. :try_end_8} :catch_4

    .line 198
    :cond_b
    throw v5

    .line 195
    :catch_4
    move-exception v0

    .line 197
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACFE resetting inclusion/exclusion list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v5, Landroid/os/RemoteException;

    invoke-direct {v5}, Landroid/os/RemoteException;-><init>()V

    throw v5

    .line 211
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_c
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v5, :cond_0

    .line 213
    if-eqz v1, :cond_d

    .line 215
    const/16 v5, -0x16

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->reportFailure(I)V

    goto/16 :goto_0

    .line 217
    :cond_d
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    if-nez v5, :cond_e

    .line 219
    invoke-virtual {p0, v9}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->reportFailure(I)V

    goto/16 :goto_0

    .line 221
    :cond_e
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mModesMatchCapabilities:Z

    if-nez v5, :cond_0

    .line 223
    const/16 v5, -0x1a

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->reportFailure(I)V

    goto/16 :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const-string v0, "Scan Search Controller"

    return-object v0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 69
    :pswitch_0
    new-instance v3, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v3, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    .line 72
    .local v0, "channelEvent":Lcom/dsi/ant/message/EventCode;
    sget-object v3, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_1

    .line 74
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    const-string v4, "Search timeout occured"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->disableMessageProcessing()V

    .line 122
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channelFailureOccurred:Z

    .line 123
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 76
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v3, :cond_0

    .line 78
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->TAG:Ljava/lang/String;

    const-string v4, "Channel closed"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->disableMessageProcessing()V

    .line 80
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 87
    .end local v0    # "channelEvent":Lcom/dsi/ant/message/EventCode;
    :pswitch_1
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->cancelled:Z

    if-nez v3, :cond_0

    .line 89
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mReceivedPageCounter:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mReceivedPageCounter:I

    .line 90
    invoke-virtual {p0, p2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->DetermineCapabilities(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 91
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mCapabilitiesDetermined:Z

    if-eqz v3, :cond_0

    .line 94
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    .line 97
    invoke-static {p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/message/fromant/DataMessage;

    .line 98
    .local v2, "msg":Lcom/dsi/ant/message/fromant/DataMessage;
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->hasRssi()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getMeasurementType()Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->DBM:Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    if-ne v3, v4, :cond_2

    .line 101
    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/Rssi;->getRssiValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResultRSSI:Ljava/lang/Integer;

    .line 107
    :goto_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->disableMessageProcessing()V

    .line 108
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResult:Lcom/dsi/ant/message/ChannelId;

    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->updateExclusionList(Lcom/dsi/ant/message/ChannelId;)V

    .line 109
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchFinishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 104
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->searchResultRSSI:Ljava/lang/Integer;
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public setProximityThreshold(I)V
    .locals 3
    .param p1, "thresholdValue"    # I

    .prologue
    .line 56
    const/16 v0, 0xa

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Threshold value outside of range 0-10: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;->mProximityThreshold:I

    .line 59
    return-void
.end method
