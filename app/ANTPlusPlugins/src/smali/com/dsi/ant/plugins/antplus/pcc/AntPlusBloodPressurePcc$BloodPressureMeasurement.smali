.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
.super Ljava/lang/Object;
.source "AntPlusBloodPressurePcc.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodPressureMeasurement"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_DEFAULT_ADVANCEDMEASUREMENTKEY:Ljava/lang/String; = "parcelable_bloodPressureMeasurement"


# instance fields
.field public bloodPressureStatus:Lcom/garmin/fit/BpStatus;

.field public diastolicPressure:Ljava/lang/Integer;

.field public heartRate:Ljava/lang/Integer;

.field public heartRateType:Lcom/garmin/fit/HrType;

.field private final ipcVersionNumber:I

.field public map3SampleMean:Ljava/lang/Integer;

.field public mapEveningValues:Ljava/lang/Integer;

.field public mapMorningValues:Ljava/lang/Integer;

.field public meanArterialPressure:Ljava/lang/Integer;

.field public systolicPressure:Ljava/lang/Integer;

.field public timestamp_UTC:Ljava/util/GregorianCalendar;

.field public userProfileIndex:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->ipcVersionNumber:I

    .line 289
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/high16 v6, -0x80000000

    const/4 v3, 0x0

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->ipcVersionNumber:I

    .line 347
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 348
    .local v0, "incomingVersion":I
    if-eq v0, v2, :cond_0

    .line 349
    # getter for: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Decoding version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AdvancedMeasurement parcel with version 1 parser."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/GregorianCalendar;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    .line 352
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->systolicPressure:Ljava/lang/Integer;

    .line 353
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->diastolicPressure:Ljava/lang/Integer;

    .line 354
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->meanArterialPressure:Ljava/lang/Integer;

    .line 355
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->map3SampleMean:Ljava/lang/Integer;

    .line 356
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapMorningValues:Ljava/lang/Integer;

    .line 357
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapEveningValues:Ljava/lang/Integer;

    .line 358
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRate:Ljava/lang/Integer;

    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 360
    .local v1, "temp":I
    if-ne v1, v6, :cond_1

    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 362
    if-ne v1, v6, :cond_2

    move-object v2, v3

    :goto_1
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    .line 363
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->userProfileIndex:Ljava/lang/Integer;

    .line 364
    return-void

    .line 360
    :cond_1
    invoke-static {}, Lcom/garmin/fit/HrType;->values()[Lcom/garmin/fit/HrType;

    move-result-object v2

    aget-object v2, v2, v1

    goto :goto_0

    .line 362
    :cond_2
    invoke-static {}, Lcom/garmin/fit/BpStatus;->values()[Lcom/garmin/fit/BpStatus;

    move-result-object v2

    aget-object v2, v2, v1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/garmin/fit/BloodPressureMesg;)V
    .locals 5
    .param p1, "fitMesg"    # Lcom/garmin/fit/BloodPressureMesg;

    .prologue
    const/4 v2, 0x0

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    const/4 v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->ipcVersionNumber:I

    .line 298
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v1

    .line 299
    .local v1, "tempTimeStamp":Lcom/garmin/fit/DateTime;
    if-nez v1, :cond_0

    .line 301
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    .line 307
    :goto_0
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getSystolicPressure()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->systolicPressure:Ljava/lang/Integer;

    .line 308
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getDiastolicPressure()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->diastolicPressure:Ljava/lang/Integer;

    .line 309
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMeanArterialPressure()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->meanArterialPressure:Ljava/lang/Integer;

    .line 310
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMap3SampleMean()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->map3SampleMean:Ljava/lang/Integer;

    .line 311
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapMorningValues()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapMorningValues:Ljava/lang/Integer;

    .line 312
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapEveningValues()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapEveningValues:Ljava/lang/Integer;

    .line 313
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getHeartRate()Ljava/lang/Short;

    move-result-object v0

    .line 314
    .local v0, "tempHeartRate":Ljava/lang/Short;
    if-nez v0, :cond_1

    :goto_1
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRate:Ljava/lang/Integer;

    .line 315
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getHeartRateType()Lcom/garmin/fit/HrType;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    .line 316
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getStatus()Lcom/garmin/fit/BpStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    .line 317
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getUserProfileIndex()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->userProfileIndex:Ljava/lang/Integer;

    .line 318
    return-void

    .line 304
    .end local v0    # "tempHeartRate":Ljava/lang/Short;
    :cond_0
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    .line 305
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/garmin/fit/DateTime;->getDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    .line 314
    .restart local v0    # "tempHeartRate":Ljava/lang/Short;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Short;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public asBloodPressureFitMesg()Lcom/garmin/fit/BloodPressureMesg;
    .locals 3

    .prologue
    .line 325
    new-instance v0, Lcom/garmin/fit/BloodPressureMesg;

    invoke-direct {v0}, Lcom/garmin/fit/BloodPressureMesg;-><init>()V

    .line 326
    .local v0, "retMesg":Lcom/garmin/fit/BloodPressureMesg;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/garmin/fit/DateTime;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/garmin/fit/DateTime;-><init>(Ljava/util/Date;)V

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setTimestamp(Lcom/garmin/fit/DateTime;)V

    .line 327
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->systolicPressure:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->systolicPressure:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setSystolicPressure(Ljava/lang/Integer;)V

    .line 328
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->diastolicPressure:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->diastolicPressure:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setDiastolicPressure(Ljava/lang/Integer;)V

    .line 329
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->meanArterialPressure:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->meanArterialPressure:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setMeanArterialPressure(Ljava/lang/Integer;)V

    .line 330
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->map3SampleMean:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->map3SampleMean:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setMap3SampleMean(Ljava/lang/Integer;)V

    .line 331
    :cond_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapMorningValues:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapMorningValues:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setMapMorningValues(Ljava/lang/Integer;)V

    .line 332
    :cond_5
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapEveningValues:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapEveningValues:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setMapEveningValues(Ljava/lang/Integer;)V

    .line 333
    :cond_6
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRate:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRate:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setHeartRate(Ljava/lang/Short;)V

    .line 334
    :cond_7
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setHeartRateType(Lcom/garmin/fit/HrType;)V

    .line 335
    :cond_8
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setStatus(Lcom/garmin/fit/BpStatus;)V

    .line 336
    :cond_9
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->userProfileIndex:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->userProfileIndex:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BloodPressureMesg;->setUserProfileIndex(Ljava/lang/Integer;)V

    .line 337
    :cond_a
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/high16 v1, -0x80000000

    .line 369
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->ipcVersionNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 370
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->timestamp_UTC:Ljava/util/GregorianCalendar;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 371
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->systolicPressure:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 372
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->diastolicPressure:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 373
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->meanArterialPressure:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 374
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->map3SampleMean:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 375
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapMorningValues:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->mapEveningValues:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 377
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRate:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 380
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->userProfileIndex:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 381
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->heartRateType:Lcom/garmin/fit/HrType;

    invoke-virtual {v0}, Lcom/garmin/fit/HrType;->ordinal()I

    move-result v0

    goto :goto_0

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;->bloodPressureStatus:Lcom/garmin/fit/BpStatus;

    invoke-virtual {v0}, Lcom/garmin/fit/BpStatus;->ordinal()I

    move-result v1

    goto :goto_1
.end method
