.class public interface abstract Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ICalculatedTrainerTorqueReceiver;
.super Ljava/lang/Object;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ICalculatedTrainerTorqueReceiver"
.end annotation


# virtual methods
.method public abstract onNewCalculatedTrainerTorque(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation
.end method
