.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "CommonLapStateData.java"


# instance fields
.field private fitnessEquipmentType:I

.field private lapEventCounter:Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;

.field private lapEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private stateEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 18
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->stateEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 24
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEventCounter:Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;

    .line 26
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->fitnessEquipmentType:I

    return-void
.end method

.method private convertIntToBoolean(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/16 v3, 0x8

    .line 41
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 45
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 47
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEventCounter:Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0x80

    invoke-direct {p0, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->convertIntToBoolean(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->toggleChanged(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string v1, "int_lapCount"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEventCounter:Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/ToggleCounter;->getToggleCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 57
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->stateEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 59
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 60
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 61
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 63
    const-string v1, "int_stateCode"

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x70

    ushr-int/lit8 v2, v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    const/16 v2, 0x10

    if-ne v1, v2, :cond_1

    .line 67
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    and-int/lit8 v1, v1, 0x1f

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->fitnessEquipmentType:I

    .line 69
    :cond_1
    const-string v1, "int_equipmentTypeCode"

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->fitnessEquipmentType:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->stateEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 74
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_2
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->stateEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->lapEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method
