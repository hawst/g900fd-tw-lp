.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P3_MeasurementOutputData.java"


# static fields
.field private static MAX_ACC_TIMEOUT:I


# instance fields
.field private outEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private timestampAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0x7d00

    sput v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->MAX_ACC_TIMEOUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 24
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->outEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const v1, 0xffff

    sget v2, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->MAX_ACC_TIMEOUT:I

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->timestampAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 16
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 45
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x5

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v8

    .line 47
    .local v8, "timestamp":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->timestampAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    move-wide/from16 v0, p1

    invoke-virtual {v9, v8, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->accumulate(IJ)V

    .line 49
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->outEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v9

    if-nez v9, :cond_0

    .line 72
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v2, "b":Landroid/os/Bundle;
    const-string v9, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 54
    const-string v9, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 56
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x2

    aget-byte v9, v9, v10

    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromUpper4Bits(B)I

    move-result v6

    .line 57
    .local v6, "numOfTypes":I
    const-string v9, "int_numOfDataTypes"

    invoke-virtual {v2, v9, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x3

    aget-byte v9, v9, v10

    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    .line 60
    .local v3, "dataType":I
    const-string v9, "int_dataType"

    invoke-virtual {v2, v9, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    const-string v9, "decimal_timeStamp"

    new-instance v10, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->timestampAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v11}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v11, Ljava/math/BigDecimal;

    const-string v12, "2048"

    invoke-direct {v11, v12}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/16 v12, 0xb

    sget-object v13, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v10, v11, v12, v13}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x7

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    .line 66
    .local v5, "measurementValue":I
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v9

    const/4 v10, 0x4

    aget-byte v7, v9, v10

    .line 67
    .local v7, "scaleFactor":I
    if-gez v7, :cond_1

    mul-int/lit8 v4, v7, -0x1

    .line 68
    .local v4, "decimalScale":I
    :goto_1
    const-string v9, "decimal_measurementValue"

    new-instance v10, Ljava/math/BigDecimal;

    invoke-direct {v10, v5}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v11, Ljava/math/BigDecimal;

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    int-to-double v14, v7

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    invoke-direct {v11, v12, v13}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v10, v11}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v10

    sget-object v11, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v10, v4, v11}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->outEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v9, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 67
    .end local v4    # "decimalScale":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;->outEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
