.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P16_GeneralFeData.java"


# instance fields
.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private generalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V
    .locals 3
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .prologue
    const/16 v2, 0xff

    .line 32
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->generalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 27
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 33
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 34
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 19
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 55
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v17

    .line 57
    .local v17, "receivedTime_4ths_s":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 61
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    const/4 v11, 0x1

    .line 63
    .local v11, "distanceSupport":Z
    :goto_0
    if-eqz v11, :cond_0

    .line 65
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v13

    .line 67
    .local v13, "receivedDistance_1_m":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v13}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 70
    .end local v13    # "receivedDistance_1_m":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->generalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-nez v2, :cond_2

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 123
    :goto_1
    return-void

    .line 61
    .end local v11    # "distanceSupport":Z
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 76
    .restart local v11    # "distanceSupport":Z
    :cond_2
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 77
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    const-string v2, "decimal_elapsedTime"

    new-instance v3, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v4, Ljava/math/BigDecimal;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v5, 0x2

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    if-eqz v11, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v9

    .line 86
    .local v9, "distance":J
    :goto_2
    const-string v2, "long_cumulativeDistance"

    invoke-virtual {v8, v2, v9, v10}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v12

    .line 95
    .local v12, "intSpeed":I
    const v2, 0xffff

    if-ne v12, v2, :cond_5

    .line 96
    new-instance v16, Ljava/math/BigDecimal;

    const/4 v2, -0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 100
    .local v16, "receivedSpeed":Ljava/math/BigDecimal;
    :goto_3
    const-string v2, "decimal_instantaneousSpeed"

    move-object/from16 v0, v16

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 104
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v14

    .line 106
    .local v14, "receivedHeartRate":I
    const/16 v2, 0xff

    if-ne v14, v2, :cond_3

    .line 107
    const/4 v14, -0x1

    .line 109
    :cond_3
    const-string v2, "int_instantaneousHeartRate"

    invoke-virtual {v8, v2, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    and-int/lit8 v15, v2, 0x3

    .line 114
    .local v15, "receivedHeartRateSource":I
    const-string v2, "int_heartRateDataSourceCode"

    invoke-virtual {v8, v2, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_6

    const/16 v18, 0x0

    .line 118
    .local v18, "virtualSpeed":Z
    :goto_4
    const-string v2, "bool_virtualInstantaneousSpeed"

    move/from16 v0, v18

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->generalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto/16 :goto_1

    .line 85
    .end local v9    # "distance":J
    .end local v12    # "intSpeed":I
    .end local v14    # "receivedHeartRate":I
    .end local v15    # "receivedHeartRateSource":I
    .end local v16    # "receivedSpeed":Ljava/math/BigDecimal;
    .end local v18    # "virtualSpeed":Z
    :cond_4
    const-wide/16 v9, -0x1

    goto :goto_2

    .line 98
    .restart local v9    # "distance":J
    .restart local v12    # "intSpeed":I
    :cond_5
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v12}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0x3e8

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v4, 0x3

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v16

    .restart local v16    # "receivedSpeed":Ljava/math/BigDecimal;
    goto :goto_3

    .line 117
    .restart local v14    # "receivedHeartRate":I
    .restart local v15    # "receivedHeartRateSource":I
    :cond_6
    const/16 v18, 0x1

    goto :goto_4
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->generalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->getEventList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 42
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->timeAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 130
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->distanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 131
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P16_GeneralFeData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->onDropToSearch()V

    .line 132
    return-void
.end method
