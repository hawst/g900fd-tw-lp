.class public abstract Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;
.super Ljava/lang/Object;
.source "AntPlusBikePowerPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CalculatedWheelSpeedReceiver"
.end annotation


# instance fields
.field wheelCircumference:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>(Ljava/math/BigDecimal;)V
    .locals 0
    .param p1, "wheelCircumference"    # Ljava/math/BigDecimal;

    .prologue
    .line 1556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1557
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->wheelCircumference:Ljava/math/BigDecimal;

    .line 1558
    return-void
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/util/EnumSet;
    .param p4, "x3"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .param p5, "x4"    # Ljava/math/BigDecimal;

    .prologue
    .line 1547
    invoke-direct/range {p0 .. p5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->onNewRawCalculatedWheelSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    return-void
.end method

.method private onNewRawCalculatedWheelSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V
    .locals 6
    .param p1, "estTimestamp"    # J
    .param p4, "dataSource"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .param p5, "rawCalculatedWheelSpeed"    # Ljava/math/BigDecimal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1578
    .local p3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->wheelCircumference:Ljava/math/BigDecimal;

    invoke-virtual {p5, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->onNewCalculatedWheelSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    .line 1579
    return-void
.end method


# virtual methods
.method public abstract onNewCalculatedWheelSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation
.end method
