.class final Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;
.super Ljava/lang/Object;
.source "DialogBuilder_ProximityPicker.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker;->build(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$checkbox_Enable:Landroid/widget/CheckBox;

.field final synthetic val$initialValue:I

.field final synthetic val$resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;

.field final synthetic val$seekbar_ProxValue:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(Landroid/widget/CheckBox;Landroid/widget/SeekBar;ILcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$checkbox_Enable:Landroid/widget/CheckBox;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$seekbar_ProxValue:Landroid/widget/SeekBar;

    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$initialValue:I

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "newProxValue":I
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$checkbox_Enable:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$seekbar_ProxValue:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 101
    :cond_0
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$initialValue:I

    if-eq v0, v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$resultReceiver:Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$3;->val$initialValue:I

    invoke-interface {v1, v2, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogBuilder_ProximityPicker$IValueChangedReceiver;->onValueChanged(II)V

    .line 103
    :cond_1
    return-void
.end method
