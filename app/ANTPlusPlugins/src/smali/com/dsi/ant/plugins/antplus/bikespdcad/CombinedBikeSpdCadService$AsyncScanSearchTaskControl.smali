.class public Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;
.super Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;
.source "CombinedBikeSpdCadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncScanSearchTaskControl"
.end annotation


# instance fields
.field scanInfo:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;)V
    .locals 0
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "resultSentSemaphore"    # Ljava/util/concurrent/Semaphore;
    .param p5, "scanInfo"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    .prologue
    .line 694
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    .line 695
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;-><init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V

    .line 696
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->scanInfo:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    .line 697
    return-void
.end method


# virtual methods
.method public bridge synthetic beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p3, "x2"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    .prologue
    .line 688
    invoke-super {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V

    return-void
.end method

.method public bridge synthetic beginConsecutiveSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p3, "x2"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .prologue
    .line 688
    invoke-super {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->beginConsecutiveSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    return-void
.end method

.method public beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
    .locals 1
    .param p1, "task"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .prologue
    .line 718
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 719
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 720
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    invoke-virtual {p2, v0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 721
    return-void
.end method

.method public bridge synthetic handleSearchFailure(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 688
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->handleSearchFailure(I)V

    return-void
.end method

.method public bridge synthetic onExecutorDeath()V
    .locals 0

    .prologue
    .line 688
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->onExecutorDeath()V

    return-void
.end method

.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 13
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    .line 726
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->resultSentLock:Ljava/lang/Object;

    monitor-enter v10

    .line 728
    :try_start_0
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v9}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v9

    if-nez v9, :cond_0

    .line 729
    monitor-exit v10

    .line 785
    :goto_0
    return-void

    .line 731
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 779
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v9

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Plugin async controller scan search failed internally with search result: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->handleSearchFailure(I)V

    .line 781
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v11, v11, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$600(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V

    .line 784
    :goto_1
    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .line 734
    :sswitch_0
    :try_start_1
    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v9

    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v2

    .line 735
    .local v2, "devType":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    if-ne v2, v9, :cond_1

    const/4 v5, 0x1

    .line 736
    .local v5, "isComboDevice":Z
    :goto_2
    if-eqz v5, :cond_2

    const/high16 v4, 0x20000000

    .line 738
    .local v4, "flagToAdd":I
    :goto_3
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v11

    add-int/2addr v11, v4

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 739
    monitor-exit v10

    goto :goto_0

    .line 735
    .end local v4    # "flagToAdd":I
    .end local v5    # "isComboDevice":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    .line 736
    .restart local v5    # "isComboDevice":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 741
    .restart local v4    # "flagToAdd":I
    :cond_3
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v11

    const/4 v12, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v9, v11, v12}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$300(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v1

    .line 742
    .local v1, "dbResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->scanInfo:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;

    const/4 v11, 0x0

    invoke-virtual {v9, v1, v11, v2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$SpdCadAsyncScanInfo;->putResultInfo(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;ZLcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;

    move-result-object v6

    .line 744
    .local v6, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 745
    .local v0, "b":Landroid/os/Bundle;
    const-string v9, "parcelable_AsyncScanResultDeviceInfo"

    iget-object v11, v6, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->resultInfo:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    invoke-virtual {v0, v9, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 746
    const-string v9, "bool_IsCombinedSensor"

    iget-boolean v11, v6, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;->isSpdAndCadComboSensor:Z

    invoke-virtual {v0, v9, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 748
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 749
    .local v7, "resultMsg":Landroid/os/Message;
    const/4 v9, 0x2

    iput v9, v7, Landroid/os/Message;->what:I

    .line 750
    invoke-virtual {v7, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 753
    :try_start_2
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v9, v7}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 754
    :catch_0
    move-exception v3

    .line 756
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_3
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v11, "RemoteException sending async scan result, closing scan."

    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v9}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    .line 758
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v11, v11, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$400(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V

    goto :goto_1

    .line 764
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "dbResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v2    # "devType":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "flagToAdd":I
    .end local v5    # "isComboDevice":Z
    .end local v6    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanResultDeviceInfo;
    .end local v7    # "resultMsg":Landroid/os/Message;
    :sswitch_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    .line 765
    .local v8, "timeoutMsg":Landroid/os/Message;
    const/4 v9, -0x7

    iput v9, v8, Landroid/os/Message;->what:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 768
    :try_start_4
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v9, v8}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 769
    :catch_1
    move-exception v3

    .line 771
    .restart local v3    # "e":Landroid/os/RemoteException;
    :try_start_5
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v11, "RemoteException sending async scan timeout ping, closing scan."

    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v9}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    .line 773
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v11, v11, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v9, v11}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$500(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 731
    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0x9 -> :sswitch_0
    .end sparse-switch
.end method

.method public sendFailure(I)V
    .locals 4
    .param p1, "resultToSend"    # I

    .prologue
    .line 702
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 703
    .local v1, "failMsg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 704
    const/16 v2, -0x64

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 707
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    :goto_0
    return-void

    .line 708
    :catch_0
    move-exception v0

    .line 710
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RemoteException sending async scan result, closing scan."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    # invokes: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$200(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Ljava/util/UUID;)V

    goto :goto_0
.end method

.method public shutdownSearch()V
    .locals 2

    .prologue
    const/4 v1, -0x4

    .line 789
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->handleSearchFailure(I)V

    .line 790
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$AsyncScanSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->handleSearchFailure(I)V

    .line 792
    :cond_0
    return-void
.end method
