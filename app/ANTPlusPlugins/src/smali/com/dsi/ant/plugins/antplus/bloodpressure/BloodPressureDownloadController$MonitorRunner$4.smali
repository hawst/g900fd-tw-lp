.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$4;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;->doDownloadLoop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/garmin/fit/BloodPressureMesg;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$4;->this$1:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/garmin/fit/BloodPressureMesg;Lcom/garmin/fit/BloodPressureMesg;)I
    .locals 3
    .param p1, "a"    # Lcom/garmin/fit/BloodPressureMesg;
    .param p2, "b"    # Lcom/garmin/fit/BloodPressureMesg;

    .prologue
    .line 527
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v0

    .line 528
    .local v0, "al":Ljava/lang/Long;
    invoke-virtual {p2}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v1

    .line 529
    .local v1, "bl":Ljava/lang/Long;
    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 523
    check-cast p1, Lcom/garmin/fit/BloodPressureMesg;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/garmin/fit/BloodPressureMesg;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner$4;->compare(Lcom/garmin/fit/BloodPressureMesg;Lcom/garmin/fit/BloodPressureMesg;)I

    move-result v0

    return v0
.end method
