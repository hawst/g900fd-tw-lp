.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->promptToSaveDevice(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

.field final synthetic val$dlg:Landroid/app/AlertDialog;

.field final synthetic val$isPreferredCheck:Landroid/widget/CheckBox;

.field final synthetic val$savedName:Landroid/widget/EditText;

.field final synthetic val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$savedName:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$isPreferredCheck:Landroid/widget/CheckBox;

    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$dlg:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 462
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$savedName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 463
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$isPreferredCheck:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    .line 465
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Must specify device name"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 504
    :goto_0
    return-void

    .line 470
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    const-string v2, "^[\\p{L}\\p{N}]+$"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 472
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Name must not contain spaces or special characters"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 476
    :cond_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 478
    .local v0, "db2":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->reqParams:Landroid/os/Bundle;

    const-string v2, "predefinednetwork_NetKey"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 479
    .local v3, "mNetKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 484
    .local v9, "parentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mPluginName:Ljava/lang/String;

    iget v4, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    iget v5, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->transType:I

    iget v6, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->period:I

    iget v7, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->rfFreq:I

    invoke-virtual/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->addDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Lcom/dsi/ant/channel/PredefinedNetwork;IIII)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 498
    :goto_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 500
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/high16 v2, 0x20000000

    and-int/2addr v1, v2

    if-lez v1, :cond_3

    .line 501
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " (S&C)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 503
    :cond_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->this$1:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$selectedDevInfo:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->waitForNewThenFinish(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 487
    :catch_0
    move-exception v8

    .line 490
    .local v8, "e":Landroid/database/SQLException;
    :try_start_1
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SQL error saving device to db: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;->val$dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Database error, device not saved"

    const/4 v4, 0x0

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    goto :goto_1

    .end local v8    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v1
.end method
