.class public Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;
.super Ljava/lang/Object;
.source "BitManipulation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static PutSignedNumIn4LeBytes([BIJ)V
    .locals 5
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I
    .param p2, "value"    # J

    .prologue
    const-wide/16 v3, 0xff

    .line 155
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const-wide/32 v0, -0x80000000

    cmp-long v0, p2, v0

    if-gez v0, :cond_1

    .line 156
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 4 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_1
    and-long v0, v3, p2

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 159
    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x8

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 160
    add-int/lit8 v0, p1, 0x2

    const/16 v1, 0x10

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 161
    add-int/lit8 v0, p1, 0x3

    const/16 v1, 0x18

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 162
    return-void
.end method

.method public static PutUnsignedNumIn1LeBytes([BII)V
    .locals 2
    .param p0, "array"    # [B
    .param p1, "index"    # I
    .param p2, "value"    # I

    .prologue
    .line 109
    const/16 v0, 0xff

    if-gt p2, v0, :cond_0

    if-gez p2, :cond_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 1 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    and-int/lit16 v0, p2, 0xff

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 113
    return-void
.end method

.method public static PutUnsignedNumIn2BeBytes([BII)V
    .locals 2
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I
    .param p2, "value"    # I

    .prologue
    .line 135
    const v0, 0xffff

    if-gt p2, v0, :cond_0

    if-gez p2, :cond_1

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 2 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_1
    add-int/lit8 v0, p1, 0x1

    and-int/lit16 v1, p2, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 139
    shr-int/lit8 v0, p2, 0x8

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 140
    return-void
.end method

.method public static PutUnsignedNumIn2LeBytes([BII)V
    .locals 2
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I
    .param p2, "value"    # I

    .prologue
    .line 126
    const v0, 0xffff

    if-gt p2, v0, :cond_0

    if-gez p2, :cond_1

    .line 127
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 2 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_1
    and-int/lit16 v0, p2, 0xff

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 130
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 131
    return-void
.end method

.method public static PutUnsignedNumIn4LeBytes([BIJ)V
    .locals 5
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I
    .param p2, "value"    # J

    .prologue
    const-wide/16 v3, 0xff

    .line 144
    const-wide v0, 0xffffffffL

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_1

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 4 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_1
    and-long v0, v3, p2

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 148
    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x8

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 149
    add-int/lit8 v0, p1, 0x2

    const/16 v1, 0x10

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 150
    add-int/lit8 v0, p1, 0x3

    const/16 v1, 0x18

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 151
    return-void
.end method

.method public static PutUnsignedNumInUpper1And1HalfLeBytes([BII)V
    .locals 2
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I
    .param p2, "value"    # I

    .prologue
    .line 117
    const/16 v0, 0xfff

    if-gt p2, v0, :cond_0

    if-gez p2, :cond_1

    .line 118
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value outside the bounds of unsigned 1.5 byte integer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    aget-byte v0, p0, p1

    and-int/lit8 v0, v0, 0xf

    shl-int/lit8 v1, p2, 0x4

    and-int/lit16 v1, v1, 0xf0

    add-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 121
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x4

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 122
    return-void
.end method

.method public static SignedNumFrom2LeBytes([BI)S
    .locals 2
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 94
    invoke-static {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v0

    .line 95
    .local v0, "temp":I
    const v1, 0xffff

    and-int/2addr v1, v0

    int-to-short v1, v1

    return v1
.end method

.method public static SignedNumFrom4LeBytes([BI)I
    .locals 3
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 100
    add-int/lit8 v0, p1, 0x3

    aget-byte v0, p0, v0

    shl-int/lit8 v0, v0, 0x18

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    const v1, 0xff00

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x8

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    const/high16 v1, 0xff0000

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x10

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static UnsignedNumFrom1LeByte(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 38
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static UnsignedNumFrom2BeBytes([BI)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 55
    const v0, 0xffff

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    const v2, 0xff00

    aget-byte v3, p0, p1

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    and-int/2addr v0, v1

    return v0
.end method

.method public static UnsignedNumFrom2LeBytes([BI)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 43
    const v0, 0xffff

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    const v2, 0xff00

    add-int/lit8 v3, p1, 0x1

    aget-byte v3, p0, v3

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    and-int/2addr v0, v1

    return v0
.end method

.method public static UnsignedNumFrom3BeBytes([BI)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 68
    const v0, 0xffffff

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    const v2, 0xff00

    add-int/lit8 v3, p1, 0x1

    aget-byte v3, p0, v3

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    const/high16 v2, 0xff0000

    aget-byte v3, p0, p1

    shl-int/lit8 v3, v3, 0x10

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    and-int/2addr v0, v1

    return v0
.end method

.method public static UnsignedNumFrom3LeBytes([BI)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 61
    const v0, 0xffffff

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    const v2, 0xff00

    add-int/lit8 v3, p1, 0x1

    aget-byte v3, p0, v3

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    const/high16 v2, 0xff0000

    add-int/lit8 v3, p1, 0x2

    aget-byte v3, p0, v3

    shl-int/lit8 v3, v3, 0x10

    and-int/2addr v2, v3

    add-int/2addr v1, v2

    and-int/2addr v0, v1

    return v0
.end method

.method public static UnsignedNumFrom4LeBytes([BI)J
    .locals 6
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 75
    const-wide v0, 0xffffffffL

    const-wide/16 v2, 0xff

    aget-byte v4, p0, p1

    int-to-long v4, v4

    and-long/2addr v2, v4

    const v4, 0xff00

    add-int/lit8 v5, p1, 0x1

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x8

    and-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    const/high16 v4, 0xff0000

    add-int/lit8 v5, p1, 0x2

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x10

    and-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    const/high16 v4, -0x1000000

    add-int/lit8 v5, p1, 0x3

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x18

    and-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public static UnsignedNumFrom7LeBytes([BI)J
    .locals 9
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 83
    const-wide v0, 0xffffffffffffffL

    const-wide/16 v2, 0xff

    aget-byte v4, p0, p1

    int-to-long v4, v4

    and-long/2addr v2, v4

    const v4, 0xff00

    add-int/lit8 v5, p1, 0x1

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x8

    and-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    const/high16 v4, 0xff0000

    add-int/lit8 v5, p1, 0x2

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x10

    and-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide v4, 0xff000000L

    add-int/lit8 v6, p1, 0x3

    aget-byte v6, p0, v6

    shl-int/lit8 v6, v6, 0x18

    int-to-long v6, v6

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    const-wide v4, 0xff00000000L

    add-int/lit8 v6, p1, 0x4

    aget-byte v6, p0, v6

    int-to-long v6, v6

    const/16 v8, 0x20

    shl-long/2addr v6, v8

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    const-wide v4, 0xff0000000000L

    add-int/lit8 v6, p1, 0x5

    aget-byte v6, p0, v6

    int-to-long v6, v6

    const/16 v8, 0x28

    shl-long/2addr v6, v8

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    const-wide/high16 v4, 0xff000000000000L

    add-int/lit8 v6, p1, 0x6

    aget-byte v6, p0, v6

    int-to-long v6, v6

    const/16 v8, 0x30

    shl-long/2addr v6, v8

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public static UnsignedNumFromLower2BitsOfLowerNibble(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 23
    and-int/lit8 v0, p0, 0x3

    return v0
.end method

.method public static UnsignedNumFromLower2BitsOfUpperNibble(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 13
    shr-int/lit8 v0, p0, 0x4

    and-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public static UnsignedNumFromLower4Bits(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 33
    and-int/lit8 v0, p0, 0xf

    return v0
.end method

.method public static UnsignedNumFromUpper1And1HalfLeBytes([BI)I
    .locals 2
    .param p0, "array"    # [B
    .param p1, "startIndex"    # I

    .prologue
    .line 49
    aget-byte v0, p0, p1

    ushr-int/lit8 v0, v0, 0x4

    and-int/lit8 v0, v0, 0xf

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x4

    and-int/lit16 v1, v1, 0xff0

    add-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xfff

    return v0
.end method

.method public static UnsignedNumFromUpper2BitsOfLowerNibble(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 18
    shr-int/lit8 v0, p0, 0x2

    and-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public static UnsignedNumFromUpper2BitsOfUpperNibble(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 8
    shr-int/lit8 v0, p0, 0x6

    and-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public static UnsignedNumFromUpper4Bits(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 28
    shr-int/lit8 v0, p0, 0x4

    and-int/lit8 v0, v0, 0xf

    return v0
.end method
