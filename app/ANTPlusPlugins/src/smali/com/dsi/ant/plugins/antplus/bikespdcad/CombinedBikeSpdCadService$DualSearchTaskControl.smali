.class abstract Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;
.super Ljava/lang/Object;
.source "CombinedBikeSpdCadService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "DualSearchTaskControl"
.end annotation


# instance fields
.field currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

.field executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field isFailed:Z

.field msgr_ResultMessenger:Landroid/os/Messenger;

.field otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

.field prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field protected final resultSentLock:Ljava/lang/Object;

.field resultSentSemaphore:Ljava/util/concurrent/Semaphore;

.field secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Ljava/util/concurrent/Semaphore;)V
    .locals 2
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "resultSentSemaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    const/4 v1, 0x0

    .line 453
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->this$0:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentLock:Ljava/lang/Object;

    .line 443
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    .line 447
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    .line 448
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 454
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 455
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->msgr_ResultMessenger:Landroid/os/Messenger;

    .line 456
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    .line 457
    return-void
.end method


# virtual methods
.method public beginConcurrentSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;)V
    .locals 0
    .param p1, "firstTask"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p3, "otherSearch"    # Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    .prologue
    .line 467
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    .line 468
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V

    .line 469
    return-void
.end method

.method public beginConsecutiveSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V
    .locals 0
    .param p1, "firstTask"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    .param p2, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p3, "secondTask"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .prologue
    .line 461
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 462
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V

    .line 463
    return-void
.end method

.method public abstract beginGivenSearch(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
.end method

.method public handleSearchFailure(I)V
    .locals 3
    .param p1, "resultToSend"    # I

    .prologue
    .line 475
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentLock:Ljava/lang/Object;

    monitor-enter v1

    .line 477
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    if-nez v0, :cond_0

    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    .line 480
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    if-nez v0, :cond_1

    .line 485
    monitor-exit v1

    .line 509
    :goto_0
    return-void

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->otherSearch:Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    if-nez v0, :cond_2

    .line 490
    monitor-exit v1

    goto :goto_0

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 493
    :cond_2
    const/4 v0, -0x7

    if-ne p1, v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    if-eqz v0, :cond_3

    .line 496
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 497
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->secondTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    .line 498
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->isFailed:Z

    .line 499
    monitor-exit v1

    goto :goto_0

    .line 504
    :cond_3
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->sendFailure(I)V

    .line 505
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->resultSentSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_4

    .line 506
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Could not acquire resultSent semaphore in handleFailure()"

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onExecutorDeath()V
    .locals 2

    .prologue
    .line 516
    # getter for: Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Plugin search by deviceNumber failed: executor died"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CombinedBikeSpdCadService$DualSearchTaskControl;->handleSearchFailure(I)V

    .line 518
    return-void
.end method

.method public abstract sendFailure(I)V
.end method
