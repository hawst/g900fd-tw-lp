.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P20_EllipticalData.java"


# instance fields
.field private commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

.field private ellipticalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private posVerticalDistanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

.field private strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;)V
    .locals 3
    .param p1, "commonDataDecoder"    # Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .prologue
    const/16 v2, 0xff

    .line 31
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 22
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->ellipticalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->posVerticalDistanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-direct {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    .line 32
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    .line 33
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 18
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 54
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v10, 0x1

    .line 56
    .local v10, "posVerticalDistanceSupport":Z
    :goto_0
    if-eqz v10, :cond_0

    .line 58
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v12

    .line 60
    .local v12, "receivedPosVerticalDistance":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->posVerticalDistanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v12}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 65
    .end local v12    # "receivedPosVerticalDistance":I
    :cond_0
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const/16 v17, 0x1

    .line 67
    .local v17, "strideCountSupport":Z
    :goto_1
    if-eqz v17, :cond_1

    .line 69
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v14

    .line 71
    .local v14, "receivedStrideCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2, v14}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->accumulate(I)V

    .line 74
    .end local v14    # "receivedStrideCount":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->ellipticalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-nez v2, :cond_4

    .line 76
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 122
    :goto_2
    return-void

    .line 54
    .end local v10    # "posVerticalDistanceSupport":Z
    .end local v17    # "strideCountSupport":Z
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 65
    .restart local v10    # "posVerticalDistanceSupport":Z
    :cond_3
    const/16 v17, 0x0

    goto :goto_1

    .line 80
    .restart local v17    # "strideCountSupport":Z
    :cond_4
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 82
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 84
    if-eqz v10, :cond_7

    new-instance v2, Ljava/math/BigDecimal;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->posVerticalDistanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v4, 0x1

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v4, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v9

    .line 88
    .local v9, "posVerticalDistance":Ljava/math/BigDecimal;
    :goto_3
    const-string v2, "decimal_cumulativePosVertDistance"

    invoke-virtual {v8, v2, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 92
    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->getValue()J

    move-result-wide v15

    .line 93
    .local v15, "strideCount":J
    :goto_4
    const-string v2, "long_cumulativeStrides"

    move-wide v0, v15

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 97
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 100
    .local v11, "receivedCadence":I
    const/16 v2, 0xff

    if-ne v11, v2, :cond_5

    .line 101
    const/4 v11, -0x1

    .line 103
    :cond_5
    const-string v2, "int_instantaneousCadence"

    invoke-virtual {v8, v2, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v13

    .line 111
    .local v13, "receivedPower":I
    const v2, 0xffff

    if-ne v13, v2, :cond_6

    .line 112
    const/4 v13, -0x1

    .line 114
    :cond_6
    const-string v2, "int_instantaneousPower"

    invoke-virtual {v8, v2, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->ellipticalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto/16 :goto_2

    .line 84
    .end local v9    # "posVerticalDistance":Ljava/math/BigDecimal;
    .end local v11    # "receivedCadence":I
    .end local v13    # "receivedPower":I
    .end local v15    # "strideCount":J
    :cond_7
    new-instance v9, Ljava/math/BigDecimal;

    const/4 v2, -0x1

    invoke-direct {v9, v2}, Ljava/math/BigDecimal;-><init>(I)V

    goto :goto_3

    .line 92
    .restart local v9    # "posVerticalDistance":Ljava/math/BigDecimal;
    :cond_8
    const-wide/16 v15, -0x1

    goto :goto_4
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, "el":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->ellipticalDataEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->strideCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 129
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->posVerticalDistanceAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/Accumulator;->uninitialize()V

    .line 130
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/P20_EllipticalData;->commonDataDecoder:Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/pages/CommonLapStateData;->onDropToSearch()V

    .line 131
    return-void
.end method
