.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchInfo"
.end annotation


# instance fields
.field devType:I

.field executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field period:I

.field rfFreq:I

.field scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

.field thisSearchListIndex:I

.field transType:I


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V
    .locals 9
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "searchParams"    # Landroid/os/Bundle;
    .param p4, "searchListIndex"    # I

    .prologue
    .line 72
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->thisSearchListIndex:I

    .line 74
    const-string v0, "int_DevType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    .line 75
    const-string v0, "int_TransType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->transType:I

    .line 76
    const-string v0, "int_Period"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->period:I

    .line 77
    const-string v0, "int_RfFreq"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->rfFreq:I

    .line 79
    if-eqz p2, :cond_0

    .line 81
    new-instance v7, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$1;

    invoke-direct {v7, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    .line 96
    .local v7, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_0
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v0, p2, v7}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v7    # "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :cond_0
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->rfFreq:I

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->period:I

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->transType:I

    iget v5, p1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->currentProximityThreshold:I

    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;

    invoke-direct {v6, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V

    invoke-direct/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;-><init>(IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    .line 134
    :goto_0
    return-void

    .line 97
    .restart local v7    # "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :catch_0
    move-exception v8

    .line 99
    .local v8, "e":Landroid/os/RemoteException;
    invoke-interface {v7}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    goto :goto_0
.end method
