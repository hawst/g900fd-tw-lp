.class public final Lcom/dsi/ant/plugins/antplus/bikecadence/pages/P0_RawCadenceData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P0_RawCadenceData.java"


# instance fields
.field private decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 18
    new-instance v0, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikecadence/pages/P0_RawCadenceData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikecadence/pages/P0_RawCadenceData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v5

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->decodeCadence(JJII)V

    .line 38
    return-void
.end method

.method public getEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikecadence/pages/P0_RawCadenceData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->getEventList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikecadence/pages/P0_RawCadenceData;->decoder:Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/bikespdcad/CadenceDecoder;->onDropToSearch()V

    .line 44
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->onDropToSearch()V

    .line 45
    return-void
.end method
