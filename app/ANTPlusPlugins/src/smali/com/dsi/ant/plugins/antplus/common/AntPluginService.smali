.class public abstract Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.super Landroid/app/Service;
.source "AntPluginService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/common/AntPluginService$9;,
        Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;,
        Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;,
        Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field alreadyConnectedDeviceRequestReceiver:Landroid/content/BroadcastReceiver;

.field private final mArsBoundChange_LOCK:Ljava/lang/Object;

.field public mArsComm:Lcom/dsi/ant/AntService;

.field public mArsConnection:Landroid/content/ServiceConnection;

.field public volatile mClosed:Z

.field public final mConnectedDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mIsArsBound:Z

.field protected mPccHandler:Landroid/os/Handler;

.field public mPccMsgHandler:Landroid/os/Messenger;

.field mPccReqAccessHandler:Landroid/os/Messenger;

.field public mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mToken_DeviceList:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;",
            ">;"
        }
    .end annotation
.end field

.field pccHandlerThread:Landroid/os/HandlerThread;

.field reqAccessHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsComm:Lcom/dsi/ant/AntService;

    .line 63
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mIsArsBound:Z

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsBoundChange_LOCK:Ljava/lang/Object;

    .line 66
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mClosed:Z

    .line 68
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_DeviceList:Ljava/util/TreeMap;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 102
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$1;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->alreadyConnectedDeviceRequestReceiver:Landroid/content/BroadcastReceiver;

    .line 1433
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Message;Landroid/os/Messenger;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
    .param p1, "x1"    # Landroid/os/Message;
    .param p2, "x2"    # Landroid/os/Messenger;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->connectArsAndRepost(Landroid/os/Message;Landroid/os/Messenger;)V

    return-void
.end method

.method private bindToArs(Landroid/os/Messenger;)V
    .locals 4
    .param p1, "msgr_ResultMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 344
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsBoundChange_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 346
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mIsArsBound:Z

    if-nez v1, :cond_0

    .line 348
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mIsArsBound:Z

    .line 350
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v1}, Lcom/dsi/ant/AntService;->bindService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 352
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v3, "Binding to ARS failed"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 354
    .local v0, "bindFailedResponse":Landroid/os/Message;
    const/4 v1, -0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 355
    invoke-virtual {p0, p1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 359
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->unbindFromArs()V

    .line 360
    monitor-exit v2

    .line 364
    .end local v0    # "bindFailedResponse":Landroid/os/Message;
    :goto_0
    return-void

    .line 363
    :cond_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private connectArsAndRepost(Landroid/os/Message;Landroid/os/Messenger;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 267
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v3

    .line 268
    .local v3, "origRequest":Landroid/os/Message;
    new-instance v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;

    invoke-direct {v8, p0, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$2;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Message;)V

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsConnection:Landroid/content/ServiceConnection;

    .line 313
    const/4 v2, 0x0

    .line 314
    .local v2, "isArsInstalled":Z
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 315
    .local v6, "pm":Landroid/content/pm/PackageManager;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v5

    .line 316
    .local v5, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    .line 318
    .local v4, "packageInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v8, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v9, "com.dsi.ant.service.socket"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 320
    iget-boolean v8, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v8, :cond_1

    .line 321
    const/4 v2, 0x1

    .line 325
    .end local v4    # "packageInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    if-nez v2, :cond_2

    .line 327
    sget-object v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v9, "Binding to ARS failed, not installed"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 329
    .local v0, "bindFailedResponse":Landroid/os/Message;
    const/4 v8, -0x5

    iput v8, v0, Landroid/os/Message;->what:I

    .line 330
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 331
    .local v7, "s":Landroid/os/Bundle;
    const-string v8, "string_DependencyPackageName"

    const-string v9, "com.dsi.ant.service.socket"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v8, "string_DependencyName"

    const-string v9, "ANT Radio Service"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {v0, v7}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 334
    invoke-virtual {p0, p2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 340
    .end local v0    # "bindFailedResponse":Landroid/os/Message;
    .end local v7    # "s":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 338
    :cond_2
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->bindToArs(Landroid/os/Messenger;)V

    goto :goto_0
.end method

.method private handleActivitySearchAccessRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 549
    const-string v5, "int_ProximityBin"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 551
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "Bundle is missing proximity parameter"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 553
    .local v2, "response":Landroid/os/Message;
    const/4 v5, -0x4

    iput v5, v2, Landroid/os/Message;->what:I

    .line 554
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 583
    .end local v2    # "response":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->composeActivitySearchParams(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 559
    .local v4, "searchParams":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 563
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 564
    .local v0, "ArsSearchIntent":Landroid/content/Intent;
    const-string v5, "com.dsi.ant.plugins.antplus.pcc.plugindata"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 566
    const-string v5, "b_ForceManualSelect"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 568
    .local v1, "bForceManualSelect":Z
    if-eqz v1, :cond_2

    .line 570
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.dsi.ant.plugins.antplus"

    const-string v7, "com.dsi.ant.plugins.antplus.utility.search.Activity_SearchAllDevices"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 577
    :goto_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 578
    .restart local v2    # "response":Landroid/os/Message;
    const/4 v5, 0x1

    iput v5, v2, Landroid/os/Message;->what:I

    .line 579
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 580
    .local v3, "retB":Landroid/os/Bundle;
    const-string v5, "intent_ActivityToLaunch"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 581
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 582
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 574
    .end local v2    # "response":Landroid/os/Message;
    .end local v3    # "retB":Landroid/os/Bundle;
    :cond_2
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.dsi.ant.plugins.antplus"

    const-string v7, "com.dsi.ant.plugins.antplus.utility.search.Dialog_SearchPreferredDevice"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_1
.end method


# virtual methods
.method public acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;
    .locals 10
    .param p1, "network"    # Lcom/dsi/ant/channel/PredefinedNetwork;
    .param p2, "caps"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "failureReportReceiver"    # Landroid/os/Messenger;
    .param p4, "clientInfo"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 393
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 398
    .local v4, "startTime_ms":J
    :goto_0
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v4

    const-wide/16 v8, 0x3a98

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 400
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v7, "Acquire channel stuck in acquire loop for 15s, aborting."

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 402
    .local v3, "response":Landroid/os/Message;
    const/4 v6, -0x4

    iput v6, v3, Landroid/os/Message;->what:I

    .line 403
    if-eqz p3, :cond_0

    .line 404
    invoke-virtual {p0, p3, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_4

    .line 405
    :cond_0
    const/4 v6, 0x0

    .line 482
    .end local v3    # "response":Landroid/os/Message;
    :goto_1
    return-object v6

    .line 412
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsComm:Lcom/dsi/ant/AntService;

    invoke-virtual {v6}, Lcom/dsi/ant/AntService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProvider;

    move-result-object v6

    invoke-virtual {v6, p0, p1, p2}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    .line 428
    :try_start_2
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->disableEventBuffer()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    move-object v6, v0

    .line 435
    goto :goto_1

    .line 414
    :catch_0
    move-exception v1

    .line 416
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_3
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v7, "UnexpectedError, acquireChannel_helper called with null ArsComm"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 418
    .restart local v3    # "response":Landroid/os/Message;
    const/4 v6, -0x4

    iput v6, v3, Landroid/os/Message;->what:I

    .line 419
    if-eqz p3, :cond_2

    .line 420
    invoke-virtual {p0, p3, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 421
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 430
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v3    # "response":Landroid/os/Message;
    :catch_1
    move-exception v1

    .line 432
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not set event buffer: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v8

    invoke-virtual {v8}, Lcom/dsi/ant/channel/AntCommandFailureReason;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_2

    .line 436
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_2
    move-exception v1

    .line 438
    .local v1, "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    :try_start_4
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$9;->$SwitchMap$com$dsi$ant$channel$ChannelNotAvailableReason:[I

    iget-object v7, v1, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 464
    :cond_3
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not acquire channel: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 466
    .restart local v3    # "response":Landroid/os/Message;
    const/4 v6, -0x3

    iput v6, v3, Landroid/os/Message;->what:I

    .line 467
    if-eqz p3, :cond_4

    .line 468
    invoke-virtual {p0, p3, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    .line 469
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 445
    .end local v3    # "response":Landroid/os/Message;
    :pswitch_0
    const-wide/16 v6, 0xc8

    :try_start_5
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 446
    :catch_3
    move-exception v2

    .line 448
    .local v2, "e2":Ljava/lang/InterruptedException;
    :try_start_6
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v7, "Sleep interupted attempting to acquire not yet ready channel, trying again"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 474
    .end local v1    # "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    .end local v2    # "e2":Ljava/lang/InterruptedException;
    :catch_4
    move-exception v1

    .line 477
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v7, "RemoteException acquiring channel from ARS"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 479
    .restart local v3    # "response":Landroid/os/Message;
    const/4 v6, -0x4

    iput v6, v3, Landroid/os/Message;->what:I

    .line 480
    if-eqz p3, :cond_5

    .line 481
    invoke-virtual {p0, p3, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 482
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 452
    .end local v3    # "response":Landroid/os/Message;
    .local v1, "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    :pswitch_1
    :try_start_7
    iget v6, p4, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->pluginLibVersion:I

    const/16 v7, 0x4e84

    if-lt v6, v7, :cond_3

    .line 454
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v7, "ARS reports no ANT adapters available"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 456
    .restart local v3    # "response":Landroid/os/Message;
    const/16 v6, -0xa

    iput v6, v3, Landroid/os/Message;->what:I

    .line 457
    if-eqz p3, :cond_6

    .line 458
    invoke-virtual {p0, p3, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_4

    .line 459
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected addExisitingDevicesToSearchParams(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "searchParams"    # Landroid/os/Bundle;
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 1133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1134
    .local v0, "connectedDevicesCanConnectToIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1136
    .local v1, "connectedDevicesCanConnectToNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v5

    .line 1138
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1139
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1141
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 1144
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    iget-object v4, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1147
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1148
    iget-object v4, v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1150
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1152
    const-string v4, "intarl_AvailableConnectedDevices"

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1153
    const-string v4, "intarl_AvailableConnectedDevNames"

    invoke-virtual {p1, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1154
    return-void
.end method

.method protected composeActivitySearchParams(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 590
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 593
    .local v1, "deviceParams":Landroid/os/Bundle;
    const-string v6, "predefinednetwork_NetKey"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 594
    .local v2, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    invoke-virtual {p0, v2, v5, p2, p1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 595
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_0

    move-object v1, v5

    .line 710
    .end local v1    # "deviceParams":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 597
    .restart local v1    # "deviceParams":Landroid/os/Bundle;
    :cond_0
    const-string v6, "antchannel_Channel"

    invoke-virtual {v1, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 599
    const-string v6, "int_ProximityBin"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 600
    .local v3, "proximityThreshold":I
    const/4 v6, -0x1

    if-lt v3, v6, :cond_1

    const/16 v6, 0xa

    if-le v3, v6, :cond_2

    .line 603
    :cond_1
    sget-object v6, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Proximity threshold out of range, value: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 605
    .local v4, "response":Landroid/os/Message;
    const/16 v6, -0x9

    iput v6, v4, Landroid/os/Message;->what:I

    .line 606
    invoke-virtual {p0, p2, v4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    move-object v1, v5

    .line 607
    goto :goto_0

    .line 610
    .end local v4    # "response":Landroid/os/Message;
    :cond_2
    const-string v5, "int_ProximityBin"

    invoke-virtual {v1, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 613
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->addExisitingDevicesToSearchParams(Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    .line 615
    const-string v5, "msgr_SearchResultReceiver"

    new-instance v6, Landroid/os/Messenger;

    new-instance v7, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;

    invoke-direct {v7, p0, v0, p2, p1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$3;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    invoke-direct {v6, v7}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Bundle;)V
    .locals 18
    .param p1, "asyncScanInfo"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 908
    const-string v3, "parcelable_AsyncScanResultDeviceInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    .line 909
    .local v15, "resultToConnect":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    const-string v3, "msgr_ReqAccResultReceiver"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/Messenger;

    .line 912
    .local v13, "msgr_ResultMessenger":Landroid/os/Messenger;
    invoke-virtual {v15}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->getAntDeviceNumber()I

    move-result v3

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v10

    .line 913
    .local v10, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v10, :cond_2

    .line 915
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {v10, v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 917
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v14

    .line 918
    .local v14, "response":Landroid/os/Message;
    const/4 v3, -0x8

    iput v3, v14, Landroid/os/Message;->what:I

    .line 919
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1024
    .end local v14    # "response":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 922
    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v13, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    .line 924
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 928
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v16

    .line 929
    .local v16, "searchParams":Landroid/os/Bundle;
    const-string v3, "int_DevType"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 930
    .local v6, "devType":I
    const-string v3, "int_TransType"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 931
    .local v7, "transType":I
    const-string v3, "int_Period"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 932
    .local v5, "period":I
    const-string v3, "int_RfFreq"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 933
    .local v4, "rfFreq":I
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    invoke-virtual {v15}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;->getAntDeviceNumber()I

    move-result v3

    const/4 v8, 0x0

    new-instance v9, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1, v13}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$6;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;Landroid/os/Messenger;)V

    invoke-direct/range {v2 .. v9}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 1003
    .local v2, "connectTask":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    :try_start_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v8, 0xbb8

    invoke-virtual {v3, v2, v8}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v17

    .line 1004
    .local v17, "taskStarted":Z
    move-object/from16 v0, p1

    iput-object v13, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011
    :goto_1
    if-nez v17, :cond_0

    .line 1013
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 1014
    .local v12, "failMsg":Landroid/os/Message;
    const/4 v3, -0x4

    iput v3, v12, Landroid/os/Message;->what:I

    .line 1017
    :try_start_1
    invoke-virtual {v13, v12}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1022
    :goto_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 1005
    .end local v12    # "failMsg":Landroid/os/Message;
    .end local v17    # "taskStarted":Z
    :catch_0
    move-exception v11

    .line 1007
    .local v11, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v8, "Plugin async scan connect InterruptedException trying to start connect task"

    invoke-static {v3, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const/16 v17, 0x0

    .restart local v17    # "taskStarted":Z
    goto :goto_1

    .line 1018
    .end local v11    # "e":Ljava/lang/InterruptedException;
    .restart local v12    # "failMsg":Landroid/os/Message;
    :catch_1
    move-exception v11

    .line 1020
    .local v11, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v8, "Plugin async scan connect failed to start connect task"

    invoke-static {v3, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public abstract createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.end method

.method public dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V
    .locals 4
    .param p1, "msgr"    # Landroid/os/Messenger;
    .param p2, "reply"    # Landroid/os/Message;

    .prologue
    .line 1327
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1334
    :goto_0
    return-void

    .line 1328
    :catch_0
    move-exception v0

    .line 1331
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException dumbfiring reply to client. Reply code was: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 4
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "appNamePkg"    # Ljava/lang/String;

    .prologue
    .line 1064
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1066
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 1068
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz p1, :cond_1

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 1072
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1075
    :cond_2
    monitor-exit v3

    .line 1079
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :goto_0
    return-object v0

    .line 1078
    :cond_3
    monitor-exit v3

    .line 1079
    const/4 v0, 0x0

    goto :goto_0

    .line 1078
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method protected getAlreadyConnectedDevices(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "query"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 1084
    const-string v9, "DevType_int"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1086
    .local v4, "queryDevType":I
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1088
    .local v0, "deviceParameters":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 1127
    :cond_0
    :goto_0
    return-void

    .line 1091
    :cond_1
    const-string v9, "int_DevType"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 1092
    .local v8, "serviceDeviceType":I
    if-eqz v4, :cond_2

    if-ne v4, v8, :cond_0

    .line 1095
    :cond_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 1096
    .local v6, "retMsg":Landroid/os/Message;
    iput v10, v6, Landroid/os/Message;->what:I

    .line 1097
    const-string v9, "CmdSeqNum_int"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v6, Landroid/os/Message;->arg1:I

    .line 1098
    iput v10, v6, Landroid/os/Message;->arg2:I

    .line 1099
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1100
    .local v5, "retData":Landroid/os/Bundle;
    invoke-virtual {v6, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1101
    const-string v9, "DevType_int"

    invoke-virtual {v5, v9, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103
    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v10

    .line 1105
    :try_start_0
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-gtz v9, :cond_3

    .line 1107
    invoke-virtual {v6}, Landroid/os/Message;->recycle()V

    .line 1108
    monitor-exit v10

    goto :goto_0

    .line 1117
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .line 1111
    :cond_3
    :try_start_1
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v3, v9, [Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 1112
    .local v3, "listDeviceInfo":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v2, v9, :cond_4

    .line 1114
    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    iget-object v9, v9, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    aput-object v9, v3, v2

    .line 1112
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1116
    :cond_4
    const-string v9, "DevDbInfoList_parcelableArray"

    invoke-virtual {v5, v9, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 1117
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1119
    const-string v9, "ResultMsgr_messenger"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/Messenger;

    .line 1122
    .local v7, "retMsgr":Landroid/os/Messenger;
    :try_start_2
    invoke-virtual {v7, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1123
    :catch_0
    move-exception v1

    .line 1125
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v9, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v10, "RemoteException attempting to send getAlreadyConnnectedDevice broadcast request"

    invoke-static {v9, v10}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 7
    .param p1, "antDeviceNumber"    # I
    .param p2, "legacyDeviceName"    # Ljava/lang/String;

    .prologue
    .line 1039
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1040
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v4, "str_PluginName"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1041
    .local v3, "pluginName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1044
    .local v1, "deviceResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :try_start_0
    invoke-static {p0, p1, v3}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->dbsrvc_getDeviceInfo(Landroid/content/Context;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :try_end_0
    .catch Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1049
    :goto_0
    if-nez v1, :cond_0

    .line 1051
    new-instance v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .end local v1    # "deviceResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-direct {v1}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    .line 1052
    .restart local v1    # "deviceResult":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    .line 1053
    if-eqz p2, :cond_1

    .line 1054
    iput-object p2, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 1057
    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 1059
    :cond_0
    return-object v1

    .line 1045
    :catch_0
    move-exception v2

    .line 1047
    .local v2, "e1":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception querying database for new device: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;->deviceDbQueryResult:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", using basic info."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1056
    .end local v2    # "e1":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    goto :goto_1
.end method

.method public abstract getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 1
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 531
    packed-switch p1, :pswitch_data_0

    .line 543
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 534
    :pswitch_0
    invoke-direct {p0, p3, p2, p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleActivitySearchAccessRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0

    .line 537
    :pswitch_1
    invoke-virtual {p0, p3, p2, p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAsyncAntDevNumberSearchRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0

    .line 540
    :pswitch_2
    invoke-virtual {p0, p3, p2, p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0

    .line 531
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected handleAsyncAntDevNumberSearchRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 1158
    const-string v3, "int_AntDeviceID"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1160
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1161
    .local v1, "response":Landroid/os/Message;
    const/4 v3, -0x4

    iput v3, v1, Landroid/os/Message;->what:I

    .line 1162
    invoke-virtual {p0, p2, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1189
    .end local v1    # "response":Landroid/os/Message;
    :goto_0
    return-void

    .line 1165
    :cond_0
    const-string v3, "int_AntDeviceID"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1167
    .local v2, "targetAntDeviceNumber":I
    if-ltz v2, :cond_1

    const v3, 0xffff

    if-le v2, v3, :cond_2

    .line 1170
    :cond_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Target device number out of range, value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1172
    .restart local v1    # "response":Landroid/os/Message;
    const/16 v3, -0x9

    iput v3, v1, Landroid/os/Message;->what:I

    .line 1173
    invoke-virtual {p0, p2, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1178
    .end local v1    # "response":Landroid/os/Message;
    :cond_2
    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v0

    .line 1180
    .local v0, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz v0, :cond_3

    .line 1182
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v0, p2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 1187
    :cond_3
    invoke-virtual {p0, v2, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 30
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 719
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v10

    move-object/from16 v0, p1

    iput-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 720
    new-instance v26, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 721
    .local v26, "si":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    move-object/from16 v0, p2

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    .line 722
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v10, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v14

    .line 726
    .local v14, "deviceParams":Landroid/os/Bundle;
    const-string v10, "int_DevType"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 727
    .local v7, "devType":I
    const-string v10, "int_TransType"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 728
    .local v8, "transType":I
    const-string v10, "int_Period"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 729
    .local v6, "period":I
    const-string v10, "int_RfFreq"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 730
    .local v5, "rfFreq":I
    const-string v10, "predefinednetwork_NetKey"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v20

    check-cast v20, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 731
    .local v20, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    const-string v10, "int_ProximityBin"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 732
    .local v9, "proxThreshold":I
    const-string v10, "str_PluginName"

    invoke-virtual {v14, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 734
    .local v22, "pluginName":Ljava/lang/String;
    if-ltz v9, :cond_0

    const/16 v10, 0xa

    if-le v9, v10, :cond_1

    .line 737
    :cond_0
    sget-object v10, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Proximity threshold out of range, value: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v23

    .line 739
    .local v23, "response":Landroid/os/Message;
    const/16 v10, -0x9

    move-object/from16 v0, v23

    iput v10, v0, Landroid/os/Message;->what:I

    .line 740
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 903
    .end local v23    # "response":Landroid/os/Message;
    :goto_0
    return-void

    .line 745
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    monitor-enter v27

    .line 747
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 748
    .local v19, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_2
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 750
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 753
    .local v17, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 756
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v24

    .line 757
    .local v24, "resultMsg":Landroid/os/Message;
    const/4 v10, 0x2

    move-object/from16 v0, v24

    iput v10, v0, Landroid/os/Message;->what:I

    .line 758
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 760
    .local v12, "b":Landroid/os/Bundle;
    new-instance v21, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    const/4 v10, 0x0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;-><init>(Ljava/util/UUID;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    .line 764
    .local v21, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    const-string v10, "parcelable_AsyncScanResultDeviceInfo"

    move-object/from16 v0, v21

    invoke-virtual {v12, v10, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 765
    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 769
    :catch_0
    move-exception v15

    .line 771
    .local v15, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v10, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v28, "RemoteException sending async scan already connected devices, closing scan."

    move-object/from16 v0, v28

    invoke-static {v10, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    .line 773
    monitor-exit v27

    goto :goto_0

    .line 776
    .end local v12    # "b":Landroid/os/Bundle;
    .end local v15    # "e":Landroid/os/RemoteException;
    .end local v17    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v19    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .end local v21    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    .end local v24    # "resultMsg":Landroid/os/Message;
    :catchall_0
    move-exception v10

    monitor-exit v27
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .restart local v19    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_3
    :try_start_3
    monitor-exit v27
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 780
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v10, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v11

    .line 781
    .local v11, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v11, :cond_4

    .line 783
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 788
    :cond_4
    new-instance v13, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v13, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;)V

    .line 806
    .local v13, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_4
    new-instance v10, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v10, v11, v13}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    move-object/from16 v0, v26

    iput-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2

    .line 815
    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    new-instance v10, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v10, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$5;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    invoke-direct/range {v4 .. v10}, Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;-><init>(IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 883
    .local v4, "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    move-object/from16 v0, v26

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v10, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 887
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v18

    .line 888
    .local v18, "initMsg":Landroid/os/Message;
    const/4 v10, 0x0

    move-object/from16 v0, v18

    iput v10, v0, Landroid/os/Message;->what:I

    .line 889
    new-instance v25, Landroid/os/Bundle;

    invoke-direct/range {v25 .. v25}, Landroid/os/Bundle;-><init>()V

    .line 890
    .local v25, "retInfo":Landroid/os/Bundle;
    const-string v10, "uuid_AccessToken"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 891
    const-string v10, "msgr_PluginComm"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccMsgHandler:Landroid/os/Messenger;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 892
    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 895
    :try_start_5
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 896
    :catch_1
    move-exception v15

    .line 898
    .restart local v15    # "e":Landroid/os/RemoteException;
    sget-object v10, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v27, "RemoteException sending async scan init info."

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 807
    .end local v4    # "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;
    .end local v15    # "e":Landroid/os/RemoteException;
    .end local v18    # "initMsg":Landroid/os/Message;
    .end local v25    # "retInfo":Landroid/os/Bundle;
    :catch_2
    move-exception v16

    .line 809
    .local v16, "e1":Landroid/os/RemoteException;
    invoke-interface {v13}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    .line 810
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccReqAccessHandler:Landroid/os/Messenger;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AntPluginService ReqAcc Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->reqAccessHandlerThread:Landroid/os/HandlerThread;

    .line 131
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->reqAccessHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 132
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->reqAccessHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;-><init>(Landroid/os/Looper;Lcom/dsi/ant/plugins/antplus/common/AntPluginService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccReqAccessHandler:Landroid/os/Messenger;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccMsgHandler:Landroid/os/Messenger;

    if-nez v0, :cond_1

    .line 136
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AntPluginService PCC cmd handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->pccHandlerThread:Landroid/os/HandlerThread;

    .line 137
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->pccHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 138
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->pccHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$PccCommandHandler;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccHandler:Landroid/os/Handler;

    .line 139
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccMsgHandler:Landroid/os/Messenger;

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccReqAccessHandler:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 91
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BBC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "version":Ljava/lang/String;
    invoke-static {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->setVersion(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v1    # "version":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->alreadyConnectedDeviceRequestReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.dsi.ant.plugins.antplus.queryalreadyconnecteddevices"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 100
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ANT+ Plugins Version not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1547
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mClosed:Z

    .line 1548
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "Entering OnDestroy()"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->reqAccessHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->quit()Z

    .line 1551
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->reqAccessHandlerThread:Landroid/os/HandlerThread;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v6, v7}, Landroid/os/HandlerThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1557
    :goto_0
    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccReqAccessHandler:Landroid/os/Messenger;

    .line 1559
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->pccHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->quit()Z

    .line 1561
    :try_start_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->pccHandlerThread:Landroid/os/HandlerThread;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v6, v7}, Landroid/os/HandlerThread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1567
    :goto_1
    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccMsgHandler:Landroid/os/Messenger;

    .line 1569
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v6

    .line 1571
    :try_start_2
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->alreadyConnectedDeviceRequestReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1574
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 1576
    .local v3, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    goto :goto_2

    .line 1586
    .end local v3    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1552
    :catch_0
    move-exception v2

    .line 1554
    .local v2, "e1":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1562
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 1564
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 1580
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    .line 1581
    .local v0, "accessTokens":Ljava/util/UUID;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_3

    .line 1584
    .end local v0    # "accessTokens":Ljava/util/UUID;
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1585
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_DeviceList:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->clear()V

    .line 1586
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1588
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->unbindFromArs()V

    .line 1590
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1591
    return-void
.end method

.method public requestAccessToken(Landroid/os/Bundle;Landroid/os/Messenger;)V
    .locals 6
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 494
    const-string v3, "int_RequestAccessMode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 495
    .local v1, "requestMode":I
    sget-object v3, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ReqAcc Mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;-><init>()V

    .line 499
    .local v0, "prospectiveClient":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    const-string v3, "str_ApplicationNamePackage"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    .line 500
    const-string v3, "str_ApplicationNameTitle"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNameLabel:Ljava/lang/String;

    .line 501
    const-string v3, "msgr_PluginMsgHandler"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    .line 502
    const-string v3, "int_PluginLibVersion"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->pluginLibVersion:I

    .line 503
    const-string v3, "more"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->membersOnlyVersion:I

    .line 506
    invoke-virtual {p0, v1, p2, v0, p1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 508
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 509
    .local v2, "response":Landroid/os/Message;
    const v3, -0x5f5e0ff

    iput v3, v2, Landroid/os/Message;->what:I

    .line 510
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 513
    .end local v2    # "response":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public revokeAccess(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;)V
    .locals 2
    .param p1, "clientToken"    # Ljava/util/UUID;
    .param p2, "device"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .prologue
    .line 1523
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1525
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_DeviceList:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528
    iget-boolean v0, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->mIsOpen:Z

    if-nez v0, :cond_0

    .line 1530
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1539
    :cond_0
    monitor-exit v1

    .line 1540
    return-void

    .line 1539
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V
    .locals 2
    .param p1, "clientToken"    # Ljava/util/UUID;

    .prologue
    .line 1028
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    .line 1029
    .local v0, "si":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    if-eqz v0, :cond_0

    .line 1031
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1032
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->closeAsyncScan()V

    .line 1034
    :cond_0
    return-void
.end method

.method public startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 21
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 1195
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v14

    .line 1197
    .local v14, "deviceParams":Landroid/os/Bundle;
    const-string v5, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "predefinednetwork_NetKey"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_DevType"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_TransType"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_Period"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_RfFreq"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1204
    :cond_0
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v11, "Bundle is missing parameters"

    invoke-static {v5, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v19

    .line 1206
    .local v19, "response":Landroid/os/Message;
    const/4 v5, -0x4

    move-object/from16 v0, v19

    iput v5, v0, Landroid/os/Message;->what:I

    .line 1207
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1314
    .end local v19    # "response":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void

    .line 1212
    :cond_2
    const-string v5, "predefinednetwork_NetKey"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v18

    check-cast v18, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 1213
    .local v18, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v12

    .line 1214
    .local v12, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v12, :cond_1

    .line 1217
    const-string v5, "int_DevType"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 1218
    .local v8, "devType":I
    const-string v5, "int_TransType"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 1219
    .local v9, "transType":I
    const-string v5, "int_Period"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 1220
    .local v7, "period":I
    const-string v5, "int_RfFreq"

    invoke-virtual {v14, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 1221
    .local v6, "rfFreq":I
    if-eqz p1, :cond_4

    const/4 v10, 0x0

    .line 1224
    .local v10, "proximityThreshold":I
    :goto_1
    if-ltz v10, :cond_3

    const/16 v5, 0xa

    if-le v10, v5, :cond_5

    .line 1227
    :cond_3
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Proximity threshold out of range, value: "

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v19

    .line 1229
    .restart local v19    # "response":Landroid/os/Message;
    const/16 v5, -0x9

    move-object/from16 v0, v19

    iput v5, v0, Landroid/os/Message;->what:I

    .line 1230
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1221
    .end local v10    # "proximityThreshold":I
    .end local v19    # "response":Landroid/os/Message;
    :cond_4
    const-string v5, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1234
    .restart local v10    # "proximityThreshold":I
    :cond_5
    new-instance v13, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v13, v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$7;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Messenger;)V

    .line 1248
    .local v13, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_0
    new-instance v17, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, v17

    invoke-direct {v0, v12, v13}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1255
    .local v17, "executor":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    new-instance v11, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v11, v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;-><init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    move/from16 v5, p1

    invoke-direct/range {v4 .. v11}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 1305
    .local v4, "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1306
    :catch_0
    move-exception v15

    .line 1308
    .local v15, "e":Ljava/lang/InterruptedException;
    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 1309
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v19

    .line 1310
    .restart local v19    # "response":Landroid/os/Message;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v11, "Plugin search by deviceNumber failed to start task on executor"

    invoke-static {v5, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    const/4 v5, -0x4

    move-object/from16 v0, v19

    iput v5, v0, Landroid/os/Message;->what:I

    .line 1312
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 1249
    .end local v4    # "scanTask":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .end local v15    # "e":Ljava/lang/InterruptedException;
    .end local v17    # "executor":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .end local v19    # "response":Landroid/os/Message;
    :catch_1
    move-exception v16

    .line 1251
    .local v16, "e1":Landroid/os/RemoteException;
    invoke-interface {v13}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    goto/16 :goto_0
.end method

.method public subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z
    .locals 9
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "returnBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 1347
    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNameLabel:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    if-nez v5, :cond_1

    .line 1351
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Client missing required info"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1354
    :cond_1
    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    if-nez v5, :cond_2

    .line 1355
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    iput-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 1357
    :cond_2
    iget-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mClosed:Z

    if-eqz v5, :cond_3

    if-eqz p2, :cond_3

    .line 1364
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "Closing new device because service is closing/already closed"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 1368
    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->isDeviceClosed()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1370
    :cond_4
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "Trying to subscribe to dead device"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1371
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 1372
    .local v2, "response":Landroid/os/Message;
    const/4 v5, -0x4

    iput v5, v2, Landroid/os/Message;->what:I

    .line 1373
    invoke-virtual {p0, p3, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1422
    :goto_0
    return v4

    .line 1377
    .end local v2    # "response":Landroid/os/Message;
    :cond_5
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v5

    .line 1379
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->addClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z

    .line 1380
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1381
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1382
    :cond_6
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_DeviceList:Ljava/util/TreeMap;

    iget-object v7, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v6, v7, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1385
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 1386
    .restart local v2    # "response":Landroid/os/Message;
    iput v4, v2, Landroid/os/Message;->what:I

    .line 1387
    move-object v3, p4

    .line 1388
    .local v3, "retInfo":Landroid/os/Bundle;
    if-nez v3, :cond_7

    .line 1389
    new-instance v3, Landroid/os/Bundle;

    .end local v3    # "retInfo":Landroid/os/Bundle;
    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1393
    .restart local v3    # "retInfo":Landroid/os/Bundle;
    :cond_7
    :try_start_1
    const-string v5, "int_ServiceVersion"

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1399
    :goto_1
    const-string v5, "uuid_AccessToken"

    iget-object v6, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1400
    const-string v5, "msgr_PluginComm"

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mPccMsgHandler:Landroid/os/Messenger;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1401
    const-string v5, "int_InitialDeviceStateCode"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->getCurrentState()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1402
    iget v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->pluginLibVersion:I

    if-nez v5, :cond_8

    .line 1404
    const-string v5, "str_DeviceName"

    iget-object v6, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    const-string v5, "int_AntDeviceID"

    iget-object v6, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1411
    :goto_2
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1414
    :try_start_2
    invoke-virtual {p3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1422
    const/4 v4, 0x1

    goto :goto_0

    .line 1383
    .end local v2    # "response":Landroid/os/Message;
    .end local v3    # "retInfo":Landroid/os/Bundle;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 1394
    .restart local v2    # "response":Landroid/os/Message;
    .restart local v3    # "retInfo":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 1396
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "Can\'t retrieve service version from plugin manager!"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397
    const-string v5, "int_ServiceVersion"

    const/4 v6, -0x5

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 1409
    .end local v1    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_8
    const-string v5, "parcelable_DeviceDbInfo"

    iget-object v6, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_2

    .line 1415
    :catch_1
    move-exception v0

    .line 1418
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    const-string v6, "RemoteException sending request access \'SUCCESS\' reply to client."

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {p0, v5, p2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->revokeAccess(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;)V

    goto/16 :goto_0
.end method

.method public unbindFromArs()V
    .locals 5

    .prologue
    .line 370
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsBoundChange_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 371
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mIsArsBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 375
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsConnection:Landroid/content/ServiceConnection;

    .line 383
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsComm:Lcom/dsi/ant/AntService;

    .line 384
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mIsArsBound:Z

    .line 386
    :cond_0
    monitor-exit v2

    .line 387
    return-void

    .line 377
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected error unbinding service, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 386
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
