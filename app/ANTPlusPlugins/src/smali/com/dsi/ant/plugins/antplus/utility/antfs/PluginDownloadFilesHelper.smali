.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;
.super Ljava/lang/Object;
.source "PluginDownloadFilesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$2;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$TypicalPluginDownloadController;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Ljava/lang/Boolean;

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Z

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->isFailedAntFsDlResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static isFailedAntFsDlResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    .locals 4
    .param p0, "downloadResult"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .param p1, "controller"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p2, "finishedLock"    # Ljava/lang/Object;
    .param p3, "isFinished"    # Z

    .prologue
    .line 296
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    if-ne p0, v1, :cond_0

    .line 298
    const/4 v1, 0x0

    .line 307
    :goto_0
    return v1

    .line 302
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS download request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const/16 v0, -0x28

    .line 306
    .local v0, "resultToReport":I
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V

    .line 307
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isFailedAntFsResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;Ljava/lang/Object;Z)Z
    .locals 4
    .param p0, "result"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .param p1, "controller"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p2, "finishedLock"    # Ljava/lang/Object;
    .param p3, "isFinished"    # Z

    .prologue
    .line 260
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    if-ne p0, v1, :cond_0

    .line 262
    const/4 v1, 0x0

    .line 289
    :goto_0
    return v1

    .line 266
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTFS request failed, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$2;->$SwitchMap$com$dsi$ant$plugins$antplus$utility$antfs$AntFsHostSession$AntFsRequestResult:[I

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 284
    const/16 v0, -0x28

    .line 288
    .local v0, "resultToReport":I
    :goto_1
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V

    .line 289
    const/4 v1, 0x1

    goto :goto_0

    .line 272
    .end local v0    # "resultToReport":I
    :pswitch_0
    const/16 v0, -0x3d

    .line 273
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 276
    .end local v0    # "resultToReport":I
    :pswitch_1
    const/16 v0, -0x410

    .line 277
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 280
    .end local v0    # "resultToReport":I
    :pswitch_2
    const/16 v0, -0x29

    .line 281
    .restart local v0    # "resultToReport":I
    goto :goto_1

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;
    .locals 11
    .param p0, "channelExecutor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p1, "passkeys"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;
    .param p2, "controller"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p3, "serialNumberForHost"    # J
    .param p5, "antDeviceNumber"    # I
    .param p6, "linkRfFreq"    # I
    .param p7, "linkRfPeriod"    # I
    .param p8, "beaconInterval"    # I

    .prologue
    .line 128
    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;

    move-object v1, p2

    move-wide v2, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-object v8, p0

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIIILcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 254
    .local v10, "t":Ljava/lang/Thread;
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 255
    return-object v10
.end method

.method private static sendSingleFinishedResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;ILjava/lang/Object;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "controller"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;
    .param p1, "eventCode"    # I
    .param p2, "finishedLock"    # Ljava/lang/Object;
    .param p3, "isFinished"    # Ljava/lang/Boolean;

    .prologue
    .line 313
    monitor-enter p2

    .line 315
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;->handleFinished(I)V

    .line 318
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    .line 324
    :goto_0
    monitor-exit p2

    .line 325
    return-void

    .line 322
    :cond_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempt to send more than one result blocked, what: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
