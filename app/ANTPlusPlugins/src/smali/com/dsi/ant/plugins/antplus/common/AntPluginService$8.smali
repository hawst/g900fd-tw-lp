.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 0

    .prologue
    .line 1258
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 9
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v7, -0x4

    .line 1262
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 1263
    .local v3, "response":Landroid/os/Message;
    sparse-switch p1, :sswitch_data_0

    .line 1295
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 1296
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Plugin search by deviceNumber search failed internally with search result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1298
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 1301
    :cond_0
    :goto_0
    return-void

    .line 1266
    :sswitch_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    .line 1267
    .local v0, "channel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v0, :cond_1

    .line 1269
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin search by deviceNumber failed to shutdown executor cleanly"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1271
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1274
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    invoke-virtual {v4, v5, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v2

    .line 1275
    .local v2, "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v4, v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    move-result-object v1

    .line 1276
    .local v1, "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-nez v1, :cond_2

    .line 1278
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Plugin search by deviceNumber search failed internally: Device instantiation failed."

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    iput v7, v3, Landroid/os/Message;->what:I

    .line 1280
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1283
    :cond_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v1, v6, v8}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1284
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    goto :goto_0

    .line 1288
    .end local v0    # "channel":Lcom/dsi/ant/channel/AntChannel;
    .end local v1    # "deviceToConnectTo":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v2    # "di":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :sswitch_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 1289
    const/4 v4, -0x7

    iput v4, v3, Landroid/os/Message;->what:I

    .line 1290
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$8;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v5, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 1263
    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
