.class public Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_CapabilitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field antfsSupport:Z

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field reqCapabiltiesRequestFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private responseSent:Z

.field scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

.field scaleUserProfileSet:Z

.field scaleUserProfileSupport:Z

.field seenCapabilities:Z

.field userProfileID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 3
    .param p1, "scaleDevice"    # Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->responseSent:Z

    .line 35
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->reqCapabiltiesRequestFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 45
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    .line 46
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->reqCapabiltiesRequestFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 47
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 180
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_0

    .line 182
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 220
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 188
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->enableMessageProcessing()V

    .line 191
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 193
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->disableMessageProcessing()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 208
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->seenCapabilities:Z

    if-eqz v1, :cond_2

    .line 210
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->userProfileID:I

    const v2, 0xffff

    if-ne v1, v2, :cond_1

    .line 211
    sget v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->USER_PROFILE_UNASSIGNED:I

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->userProfileID:I

    .line 212
    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntCommandFailedException in dowork(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 200
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 202
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 216
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    const-string v2, "Timed out waiting for capabilities"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    const-string v0, "Capabilities Request"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 232
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 103
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 107
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 110
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->disableMessageProcessing()V

    .line 117
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 127
    :pswitch_3
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->seenCapabilities:Z

    if-nez v0, :cond_0

    .line 129
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v9

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v8

    .line 130
    .local v8, "pageNumber":I
    if-ne v8, v9, :cond_1

    .line 132
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 133
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleUserProfileExchange()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSupport:Z

    .line 134
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleUserProfileSelected()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSet:Z

    .line 135
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleAntFs()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->antfsSupport:Z

    .line 136
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getUserProfileID()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->userProfileID:I

    .line 137
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->seenCapabilities:Z

    .line 138
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 140
    :cond_1
    const/16 v0, 0x43

    if-ne v8, v0, :cond_0

    .line 143
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v7

    .line 144
    .local v7, "mfgId":I
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v6

    .line 145
    .local v6, "devType":I
    const/16 v0, 0x15

    if-ne v7, v0, :cond_2

    const/16 v0, 0x77

    if-ne v6, v0, :cond_2

    .line 147
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSupport:Z

    .line 148
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSet:Z

    .line 149
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->antfsSupport:Z

    .line 150
    const v0, 0xffff

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->userProfileID:I

    .line 151
    iput-boolean v9, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->seenCapabilities:Z

    .line 158
    :goto_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 155
    :cond_2
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->seenCapabilities:Z

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 107
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public processRequest()V
    .locals 4

    .prologue
    .line 52
    const/4 v1, 0x0

    .line 55
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 62
    :goto_0
    if-nez v1, :cond_0

    .line 64
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 72
    :goto_1
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->setCurrentState(I)V

    goto :goto_1
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    .prologue
    .line 76
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->reqCapabiltiesRequestFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v3

    .line 78
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->responseSent:Z

    if-eqz v2, :cond_0

    .line 79
    monitor-exit v3

    .line 97
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getIntValue()I

    move-result v1

    .line 83
    .local v1, "statusCode":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    if-ne p1, v2, :cond_1

    .line 88
    const-string v2, "bool_historySupport"

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->antfsSupport:Z

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    const-string v2, "bool_userProfileExchangeSupport"

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSupport:Z

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    const-string v2, "bool_userProfileSelected"

    iget-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->scaleUserProfileSet:Z

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    const-string v2, "int_userProfileID"

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->userProfileID:I

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->reqCapabiltiesRequestFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 95
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->responseSent:Z

    .line 96
    monitor-exit v3

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "statusCode":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
