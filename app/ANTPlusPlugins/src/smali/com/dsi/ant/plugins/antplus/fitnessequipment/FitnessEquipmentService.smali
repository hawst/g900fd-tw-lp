.class public Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "FitnessEquipmentService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 2
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 146
    :try_start_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-direct {v1, p2, p1}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-object v1

    .line 147
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v1, "str_PluginName"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->FITNESS_EQUIPMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v1, "predefinednetwork_NetKey"

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 36
    const-string v1, "int_DevType"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->FITNESS_EQUIPMENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    const-string v1, "int_TransType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 38
    const-string v1, "int_Period"

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    const-string v1, "int_RfFreq"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 40
    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 17
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 46
    const/16 v3, 0x12c

    move/from16 v0, p1

    if-eq v0, v3, :cond_0

    .line 47
    invoke-super/range {p0 .. p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z

    move-result v3

    .line 138
    :goto_0
    return v3

    .line 49
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;->isDeviceClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 51
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->TAG:Ljava/lang/String;

    const-string v15, "Client requested a FE session, but one is already in progress"

    invoke-static {v3, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 53
    .local v13, "response":Landroid/os/Message;
    const/4 v3, -0x6

    iput v3, v13, Landroid/os/Message;->what:I

    .line 54
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v13}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 55
    const/4 v3, 0x1

    goto :goto_0

    .line 59
    .end local v13    # "response":Landroid/os/Message;
    :cond_1
    const-string v3, "int_ChannelDeviceId"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 61
    .local v12, "requestedDeviceNumber":I
    if-ltz v12, :cond_2

    const v3, 0xffff

    if-le v12, v3, :cond_3

    .line 64
    :cond_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Requested device number out of range, value: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v3, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 66
    .restart local v13    # "response":Landroid/os/Message;
    const/16 v3, -0x9

    iput v3, v13, Landroid/os/Message;->what:I

    .line 67
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v13}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 68
    const/4 v3, 0x0

    goto :goto_0

    .line 74
    .end local v13    # "response":Landroid/os/Message;
    :cond_3
    if-eqz v12, :cond_5

    .line 77
    move v9, v12

    .line 94
    .local v9, "deviceNumber":I
    :cond_4
    const-string v3, "parcelable_settings"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    .line 95
    .local v7, "settingsFile":Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    const-string v3, "arrayParcelable_fitFiles"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v14

    .line 96
    .local v14, "uncastFiles":[Landroid/os/Parcelable;
    const/4 v8, 0x0

    .line 97
    .local v8, "selectedFiles":[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    if-eqz v14, :cond_6

    array-length v3, v14

    if-eqz v3, :cond_6

    .line 99
    array-length v3, v14

    new-array v8, v3, [Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    .line 100
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    array-length v3, v14

    if-ge v11, v3, :cond_6

    .line 101
    aget-object v3, v14, v11

    check-cast v3, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    aput-object v3, v8, v11

    .line 100
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 81
    .end local v7    # "settingsFile":Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .end local v8    # "selectedFiles":[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .end local v9    # "deviceNumber":I
    .end local v11    # "i":I
    .end local v14    # "uncastFiles":[Landroid/os/Parcelable;
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getTwoByteUniqueId(Landroid/content/Context;)I

    move-result v9

    .line 84
    .restart local v9    # "deviceNumber":I
    const/4 v3, -0x1

    if-ne v9, v3, :cond_4

    .line 86
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 87
    .restart local v13    # "response":Landroid/os/Message;
    const/4 v3, -0x4

    iput v3, v13, Landroid/os/Message;->what:I

    .line 88
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v13}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 89
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 104
    .end local v13    # "response":Landroid/os/Message;
    .restart local v7    # "settingsFile":Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .restart local v8    # "selectedFiles":[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .restart local v14    # "uncastFiles":[Landroid/os/Parcelable;
    :cond_6
    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v15, v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v5

    .line 105
    .local v5, "antFsChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v5, :cond_7

    .line 106
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 107
    :cond_7
    sget-object v3, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v15, v1, v2}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v6

    .line 108
    .local v6, "broadcastChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v6, :cond_8

    .line 110
    invoke-virtual {v5}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 111
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 116
    :cond_8
    :try_start_0
    new-instance v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>()V

    .line 117
    .local v4, "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v4, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 118
    new-instance v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v3, v2, v15}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->subscribeToDeviceAndNotifyClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;Landroid/os/Messenger;Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 133
    invoke-virtual {v5}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 134
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 135
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->feDevice:Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentDevice;

    .line 138
    :cond_9
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 120
    .end local v4    # "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    :catch_0
    move-exception v10

    .line 122
    .local v10, "e":Ljava/nio/channels/ClosedChannelException;
    invoke-virtual {v5}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 123
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 124
    sget-object v3, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->TAG:Ljava/lang/String;

    const-string v15, "Failed to instantiate device: Constructor threw ClosedChannelException."

    invoke-static {v3, v15}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v13

    .line 126
    .restart local v13    # "response":Landroid/os/Message;
    const/4 v3, -0x4

    iput v3, v13, Landroid/os/Message;->what:I

    .line 127
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v13}, Lcom/dsi/ant/plugins/antplus/fitnessequipment/FitnessEquipmentService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 128
    const/4 v3, 0x1

    goto/16 :goto_0
.end method
