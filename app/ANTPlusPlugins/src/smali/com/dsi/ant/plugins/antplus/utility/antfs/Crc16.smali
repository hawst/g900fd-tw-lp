.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;
.super Ljava/lang/Object;
.source "Crc16.java"

# interfaces
.implements Ljava/util/zip/Checksum;


# static fields
.field private static final crc16Table:[I


# instance fields
.field private mCrc:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->crc16Table:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0xcc01
        0xd801
        0x1400
        0xf001
        0x3c00
        0x2800
        0xe401
        0xa001
        0x6c00
        0x7800
        0xb401
        0x5000
        0x9c01
        0x8801
        0x4400
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 38
    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 39
    return-void
.end method

.method public constructor <init>(S)V
    .locals 1
    .param p1, "seed"    # S

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 47
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 48
    return-void
.end method


# virtual methods
.method public getValue()J
    .locals 4

    .prologue
    .line 53
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    int-to-long v0, v0

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 60
    return-void
.end method

.method public reset(I)V
    .locals 0
    .param p1, "seed"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 65
    return-void
.end method

.method public update(I)V
    .locals 6
    .param p1, "b"    # I

    .prologue
    const v5, 0xffff

    .line 70
    move v1, p1

    .line 74
    .local v1, "theByte":I
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->crc16Table:[I

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    and-int/lit8 v3, v3, 0xf

    aget v0, v2, v3

    .line 75
    .local v0, "temp":I
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    ushr-int/lit8 v2, v2, 0x4

    and-int/lit16 v2, v2, 0xfff

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 76
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    xor-int/2addr v2, v0

    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->crc16Table:[I

    and-int/lit8 v4, v1, 0xf

    aget v3, v3, v4

    xor-int/2addr v2, v3

    and-int/2addr v2, v5

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 79
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->crc16Table:[I

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    and-int/lit8 v3, v3, 0xf

    aget v0, v2, v3

    .line 80
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    ushr-int/lit8 v2, v2, 0x4

    and-int/lit16 v2, v2, 0xfff

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 81
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    xor-int/2addr v2, v0

    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->crc16Table:[I

    ushr-int/lit8 v4, v1, 0x4

    and-int/lit8 v4, v4, 0xf

    aget v3, v3, v4

    xor-int/2addr v2, v3

    and-int/2addr v2, v5

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->mCrc:I

    .line 82
    return-void
.end method

.method public update([BII)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 87
    array-length v1, p1

    if-le p3, v1, :cond_0

    .line 88
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Block length should not exceed the data block size"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_0
    array-length v1, p1

    if-le p2, v1, :cond_1

    .line 91
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Block offset should not exceed the data block size"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 93
    :cond_1
    add-int v1, p2, p3

    array-length v2, p1

    if-le v1, v2, :cond_2

    .line 94
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Block offset and length combined should not exceed the data block size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 96
    :cond_2
    move v0, p2

    .local v0, "i":I
    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_3

    .line 98
    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/Crc16;->update(I)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_3
    return-void
.end method
