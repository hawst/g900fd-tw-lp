.class public Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;
.super Ljava/lang/Object;
.source "BloodPressureDownloadController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$1;,
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;,
        Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field final antDeviceNumber:I

.field final beaconInterval:I

.field final bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

.field downloadWatchers_syncList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;",
            ">;"
        }
    .end annotation
.end field

.field final executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field isRunning:Z

.field final linkRfFreq:I

.field final linkRfPeriod:I

.field runningLock:Ljava/lang/Object;

.field final serialNumberForHost:J

.field watchersWaitingForAdd:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;JIIII)V
    .locals 1
    .param p1, "executor"    # Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .param p2, "bpmDownloadDb"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;
    .param p3, "serialNumberForHost"    # J
    .param p5, "antDeviceNumber"    # I
    .param p6, "linkRfFreq"    # I
    .param p7, "linkRfPeriod"    # I
    .param p8, "beaconInterval"    # I

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->runningLock:Ljava/lang/Object;

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    .line 164
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->isRunning:Z

    .line 185
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 186
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->bpmDownloadDb:Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;

    .line 187
    iput-wide p3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->serialNumberForHost:J

    .line 188
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->antDeviceNumber:I

    .line 189
    iput p6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->linkRfFreq:I

    .line 190
    iput p7, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->linkRfPeriod:I

    .line 191
    iput p8, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->beaconInterval:I

    .line 192
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public addWatcher(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;)V
    .locals 3
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .prologue
    .line 196
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 199
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->isRunning:Z

    if-nez v1, :cond_0

    .line 201
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->isRunning:Z

    .line 202
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$MonitorRunner;-><init>(Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 203
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 205
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    monitor-exit v2

    .line 206
    return-void

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public destroyMonitor()V
    .locals 3

    .prologue
    .line 243
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 245
    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->TAG:Ljava/lang/String;

    const-string v2, "Entering DestroyMonitor()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 247
    const/16 v0, -0xa

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->handleMonitorFailure(I)V

    .line 250
    monitor-exit v1

    .line 251
    return-void

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMonitorFailure(I)V
    .locals 4
    .param p1, "antFsStatusfailureCode"    # I

    .prologue
    .line 259
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 261
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 262
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendFinished(I)V

    goto :goto_0

    .line 263
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    return-void
.end method

.method public removeWatcher(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Z
    .locals 7
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    const/4 v4, 0x1

    .line 216
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 218
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->watchersWaitingForAdd:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 220
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 221
    .local v0, "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    iget-object v6, v0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v6, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 223
    const/4 v6, -0x2

    invoke-virtual {v0, v6}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendFinished(I)V

    .line 224
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 225
    monitor-exit v5

    .line 237
    .end local v0    # "dw":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    :goto_0
    return v4

    .line 228
    :cond_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController;->downloadWatchers_syncList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;

    .line 230
    .local v1, "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    iget-object v6, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->client:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-virtual {v6, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 232
    const/4 v6, -0x2

    invoke-virtual {v1, v6}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->sendFinished(I)V

    .line 233
    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;->isCancelled:Z

    .line 234
    monitor-exit v5

    goto :goto_0

    .line 238
    .end local v1    # "i":Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 237
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadController$DownloadWatcher;>;"
    :cond_3
    const/4 v4, 0x0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
