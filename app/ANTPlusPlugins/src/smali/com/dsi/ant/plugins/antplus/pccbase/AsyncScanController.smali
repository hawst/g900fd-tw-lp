.class public Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
.super Ljava/lang/Object;
.source "AsyncScanController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>.AsyncPccReleaseHandle;"
        }
    .end annotation
.end field

.field private isClosed:Z

.field private isRunning:Z

.field private retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected scanResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;

.field private shouldShutDown:Z

.field private final stateLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    .local p1, "pccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    const/4 v1, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    .line 270
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    .line 274
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->shouldShutDown:Z

    .line 276
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    .line 291
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 292
    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V
    .locals 2
    .param p1, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    .local p2, "pccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    const/4 v1, 0x0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    .line 270
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    .line 274
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->shouldShutDown:Z

    .line 276
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    .line 282
    if-nez p1, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ScanResultReceiver passed from client was null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->scanResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;

    .line 286
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 287
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    return-object v0
.end method

.method static synthetic access$202(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    return-object p1
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->closeScanControllerDelayed()V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->closeScanControllerInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->shouldShutDown:Z

    return v0
.end method

.method static synthetic access$602(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    return p1
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z

    return v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->reportScanFailure(I)V

    return-void
.end method

.method private closeScanControllerDelayed()V
    .locals 1

    .prologue
    .line 336
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->shouldShutDown:Z

    .line 337
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V

    .line 343
    :cond_0
    return-void
.end method

.method private closeScanControllerInternal()V
    .locals 2

    .prologue
    .line 318
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 332
    :goto_0
    return-void

    .line 321
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z

    .line 323
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->stopAsyncScan()V

    .line 326
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->reportScanFailure(I)V

    .line 331
    :cond_1
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 327
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public static getMissingDependencyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 589
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyName:Ljava/lang/String;

    return-object v0
.end method

.method public static getMissingDependencyPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    return-object v0
.end method

.method private reportScanFailure(I)V
    .locals 4
    .param p1, "resultCode"    # I

    .prologue
    .line 347
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 349
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    .line 350
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    .line 352
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected Event: ScanFailure on already null object, code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    monitor-exit v1

    .line 364
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 359
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    if-nez v0, :cond_1

    .line 361
    invoke-static {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->sendFailureToReceiver(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 363
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public closeScanController()V
    .locals 2

    .prologue
    .line 304
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 306
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;->close()V

    .line 313
    :goto_0
    monitor-exit v1

    .line 314
    return-void

    .line 311
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->closeScanControllerInternal()V

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getScanResultHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 459
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-direct {v0, v1, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V

    return-object v0
.end method

.method protected handleReqAccSuccess(Landroid/os/Message;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            "TT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    .local p2, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    const/4 v8, 0x0

    .line 381
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "int_ServiceVersion"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 382
    .local v5, "serviceVersion":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "msgr_PluginComm"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 383
    .local v3, "pluginComm":Landroid/os/Messenger;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "uuid_AccessToken"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/UUID;

    .line 384
    .local v2, "accessToken":Ljava/util/UUID;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "int_InitialDeviceStateCode"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 385
    .local v4, "initialStateCode":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "parcelable_DeviceDbInfo"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 386
    .local v1, "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    if-nez v1, :cond_0

    .line 388
    new-instance v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .end local v1    # "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-direct {v1, v8}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;-><init>(I)V

    .line 389
    .restart local v1    # "deviceInfo":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "int_AntDeviceID"

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    .line 390
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "str_DeviceName"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 391
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    :cond_0
    move-object v0, p2

    .line 394
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->init(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/util/UUID;Landroid/os/Messenger;II)V

    .line 395
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    move-result-object v6

    invoke-interface {p3, p2, v0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 396
    iget-object v0, p2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 397
    return-void
.end method

.method public requestDeviceAccess(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p1, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 428
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    if-nez p1, :cond_0

    .line 429
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "deviceToConnectTo parameter was null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 431
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;

    monitor-enter v3

    .line 433
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    if-eqz v2, :cond_1

    .line 435
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v4, "Cannot request access while an access request is already in progress"

    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 454
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 440
    :cond_1
    :try_start_1
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    invoke-direct {v1, p0, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V

    .line 441
    .local v1, "ret":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>.AsyncPccReleaseHandle;"
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iput-object v1, v2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    .line 443
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z

    if-nez v2, :cond_2

    .line 445
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;

    const-string v4, "Attempted to connect to a device when the scan was no longer connected"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/4 v2, 0x0

    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->DEAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    invoke-virtual {v1, v2, v4, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 447
    monitor-exit v3

    .line 453
    :goto_0
    return-object v1

    .line 450
    :cond_2
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->currentRequest:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;

    .line 451
    new-instance v0, Landroid/os/Messenger;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-direct {v2, v1, v4, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ConnectResultHandler;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V

    invoke-direct {v0, v2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 452
    .local v0, "commChannel":Landroid/os/Messenger;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v4, v1, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncPccReleaseHandle;->stateSink:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-virtual {v2, p1, v0, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V

    .line 453
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected sendFailureToReceiver(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    .locals 1
    .param p1, "requestAccessResult"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    .prologue
    .line 368
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->scanResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;

    invoke-interface {v0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;->onSearchStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 369
    return-void
.end method

.method protected sendResultToReceiver(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 373
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    const-string v1, "parcelable_AsyncScanResultDeviceInfo"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    .line 374
    .local v0, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->scanResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;

    invoke-interface {v1, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;->onSearchResult(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;)V

    .line 375
    return-void
.end method
