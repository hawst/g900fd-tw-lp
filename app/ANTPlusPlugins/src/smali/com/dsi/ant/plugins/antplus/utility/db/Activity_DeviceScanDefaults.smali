.class public Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;
.super Landroid/app/Activity;
.source "Activity_DeviceScanDefaults.java"


# static fields
.field public static final INDEX_PROXIMITYTHRESHOLD:I


# instance fields
.field private mCurrentSearchProxBinDefault:I

.field mSettingsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSettingsListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;

    .prologue
    .line 16
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mCurrentSearchProxBinDefault:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->setContentView(I)V

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    .line 33
    const/high16 v1, 0x7f060000

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 34
    .local v0, "listView_settingsList":Landroid/widget/ListView;
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003

    const v3, 0x1020014

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsListAdapter:Landroid/widget/ArrayAdapter;

    .line 36
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 37
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$1;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 60
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 66
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 67
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mSettingsList:Ljava/util/ArrayList;

    const-string v2, "Proximity Threshold"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 71
    .local v0, "preferences":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mCurrentSearchProxBinDefault:I

    .line 73
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mCurrentSearchProxBinDefault:I

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->refreshProximityUI(I)V

    .line 74
    return-void
.end method

.method public refreshProximityUI(I)V
    .locals 1
    .param p1, "proxBin"    # I

    .prologue
    .line 92
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;

    invoke-direct {v0, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;I)V

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 104
    return-void
.end method

.method public saveProximityValue(I)V
    .locals 4
    .param p1, "proxBin"    # I

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 81
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 84
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 86
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_DeviceScanDefaults;->mCurrentSearchProxBinDefault:I

    .line 87
    return-void
.end method
