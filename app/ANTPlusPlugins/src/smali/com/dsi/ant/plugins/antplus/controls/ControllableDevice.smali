.class public Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.source "ControllableDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final mAntChannel:Lcom/dsi/ant/channel/AntChannel;

.field mToken_ModeList:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field msgTxCounter:I

.field msgsReceivedCounter:I

.field msgsToSend:I

.field p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

.field p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

.field p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

.field p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

.field p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

.field p81:Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

.field pageIndex:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation
.end field

.field pagesToSend:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;",
            ">;"
        }
    .end annotation
.end field

.field requestPending:Z

.field txBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/os/Bundle;I)V
    .locals 9
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "antChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "deviceSearchParams"    # Landroid/os/Bundle;
    .param p4, "softwareVersionNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 100
    const/4 v6, 0x1

    invoke-direct {p0, p1, v6}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;I)V

    .line 53
    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6}, Ljava/util/TreeMap;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    .line 56
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    .line 57
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    .line 58
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    .line 59
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    .line 60
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    .line 61
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p81:Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

    .line 63
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    .line 66
    iput v7, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    .line 68
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->requestPending:Z

    .line 69
    iput v7, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsToSend:I

    .line 71
    const/16 v6, 0x8

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    .line 101
    const-string v6, "Controllable Device"

    iput-object v6, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .line 103
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    if-nez v6, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->initPageIndex()V

    .line 106
    :cond_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    .line 111
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v7, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;

    invoke-direct {v7, p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;)V

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannel;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 131
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v4

    .line 132
    .local v4, "statusMsg":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 146
    :goto_0
    const-string v6, "int_RfFreq"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 147
    .local v3, "rfFreq":I
    const-string v6, "int_TransType"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 148
    .local v5, "txType":I
    const-string v6, "int_DevType"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 149
    .local v1, "devType":I
    const-string v6, "int_Period"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 152
    .local v0, "chanPeriod":I
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p81:Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

    iget-object v7, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;->serialNumber:I

    .line 153
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p81:Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

    and-int/lit16 v7, p4, 0xff

    int-to-byte v7, v7

    iput v7, v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;->swRev:I

    .line 154
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    const/16 v7, 0xf

    iput v7, v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;->mfgId:I

    .line 155
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    const/4 v7, 0x0

    iput v7, v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;->modelNum:I

    .line 156
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    const/4 v7, 0x0

    iput v7, v6, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;->hwRev:I

    .line 159
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v7, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 160
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6, v3}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 161
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6, v0}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 162
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v7, Lcom/dsi/ant/message/ChannelId;

    iget-object v8, p1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct {v7, v8, v1, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v6, v7}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 163
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 164
    const/4 v6, 0x3

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->setCurrentState(I)V

    .line 176
    return-void

    .line 136
    .end local v0    # "chanPeriod":I
    .end local v1    # "devType":I
    .end local v3    # "rfFreq":I
    .end local v5    # "txType":I
    :pswitch_0
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->close()V

    .line 137
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->unassign()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 165
    .end local v4    # "statusMsg":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :catch_0
    move-exception v2

    .line 167
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    const-string v7, "RemoteException during initizalization"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->onChannelDeath()V

    .line 169
    new-instance v6, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v6}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v6

    .line 140
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v4    # "statusMsg":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :pswitch_1
    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->unassign()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 170
    .end local v4    # "statusMsg":Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :catch_1
    move-exception v2

    .line 172
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v6, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACFE during initizalization: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 174
    new-instance v6, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v6}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v6

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v0, "pl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    return-object v0
.end method

.method private initPageIndex()V
    .locals 8

    .prologue
    .line 545
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->getPageList()Ljava/util/List;

    move-result-object v3

    .line 546
    .local v3, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v5, Landroid/util/SparseArray;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    .line 547
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    .line 549
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getPageNumbers()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 551
    .local v4, "pn":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v5

    if-ltz v5, :cond_1

    .line 552
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Page number collision on page number "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 553
    :cond_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 556
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "pn":Ljava/lang/Integer;
    :cond_2
    return-void
.end method

.method public static isModeValid(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z
    .locals 2
    .param p0, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .prologue
    .line 205
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 212
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 210
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 6
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x4

    .line 415
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 416
    .local v0, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 417
    .local v2, "response":Landroid/os/Message;
    iget v3, p2, Landroid/os/Message;->what:I

    iput v3, v2, Landroid/os/Message;->what:I

    .line 418
    iget v3, p2, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 519
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 533
    :goto_0
    return-void

    .line 422
    :sswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 424
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->updateAudioStatus(Landroid/os/Bundle;)V

    .line 425
    iput v5, v2, Landroid/os/Message;->arg1:I

    .line 526
    :goto_1
    :try_start_0
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 527
    :catch_0
    move-exception v1

    .line 531
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remote Exception sending cmd response to caller pcc with token "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 435
    :sswitch_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 437
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->updateVideoStatus(Landroid/os/Bundle;)V

    .line 438
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 442
    :cond_1
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 448
    :sswitch_2
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 450
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;->updateAudioCapabilities(Landroid/os/Bundle;)V

    .line 451
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 455
    :cond_2
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 461
    :sswitch_3
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 463
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;->updateVideoCapabilities(Landroid/os/Bundle;)V

    .line 464
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 468
    :cond_3
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 474
    :sswitch_4
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 476
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->updateFromAudioCommand(Landroid/os/Bundle;)V

    .line 477
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 481
    :cond_4
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 487
    :sswitch_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 489
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->updateFromGenericCommand(Landroid/os/Bundle;)V

    .line 490
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto/16 :goto_1

    .line 494
    :cond_5
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto/16 :goto_1

    .line 500
    :sswitch_6
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 502
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->updateFromVideoCommand(Landroid/os/Bundle;)V

    .line 503
    iput v5, v2, Landroid/os/Message;->arg1:I

    goto/16 :goto_1

    .line 507
    :cond_6
    iput v4, v2, Landroid/os/Message;->arg1:I

    goto/16 :goto_1

    .line 513
    :sswitch_7
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->disableMode(Ljava/util/UUID;)V

    .line 514
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 418
    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_7
        0x4e21 -> :sswitch_5
        0x4e22 -> :sswitch_4
        0x4e23 -> :sswitch_6
        0x4e24 -> :sswitch_2
        0x4e25 -> :sswitch_0
        0x4e26 -> :sswitch_3
        0x4e27 -> :sswitch_1
    .end sparse-switch
.end method

.method public closeDevice()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 183
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 184
    return-void
.end method

.method public disableMode(Ljava/util/UUID;)V
    .locals 6
    .param p1, "accessToken"    # Ljava/util/UUID;

    .prologue
    const/4 v5, 0x0

    .line 271
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 272
    .local v2, "requestMode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {v3, p1}, Ljava/util/TreeMap;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 274
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 276
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/UUID;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 278
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getControlsModesFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "requestMode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    check-cast v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 284
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/UUID;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "requestMode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    :cond_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 308
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 309
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_3
    return-void

    .line 287
    :pswitch_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setAudioControlSupported(Z)V

    .line 288
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 289
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 290
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 291
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 294
    :pswitch_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setVideoControlSupported(Z)V

    .line 295
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 296
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 297
    :cond_5
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 298
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0

    .line 301
    :pswitch_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setGenericControlSupported(Z)V

    .line 302
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v4

    xor-int/2addr v3, v4

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 303
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public enableMode(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)V
    .locals 4
    .param p1, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .param p2, "accessToken"    # Ljava/util/UUID;

    .prologue
    const/4 v3, 0x1

    .line 239
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 243
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setAudioControlSupported(Z)V

    .line 244
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p1:Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 248
    :cond_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setVideoControlSupported(Z)V

    .line 253
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p7:Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_3
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 256
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 257
    :cond_4
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 261
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setGenericControlSupported(Z)V

    .line 262
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p2:Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->initPageIndex()V

    .line 80
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 82
    .local v0, "eventSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 84
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->getEventList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_1
    return-object v0
.end method

.method public isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z
    .locals 4
    .param p1, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .param p2, "accessToken"    # Ljava/util/UUID;

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not enabled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 231
    :goto_0
    return v0

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    invoke-virtual {v0, p2}, Ljava/util/UUID;->compareTo(Ljava/util/UUID;)I

    move-result v0

    if-nez v0, :cond_1

    .line 227
    const/4 v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client not authorized for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 231
    goto :goto_0
.end method

.method public isModeInUse(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z
    .locals 3
    .param p1, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onChannelDeath()V
    .locals 1

    .prologue
    .line 192
    const/16 v0, -0x64

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->setCurrentState(I)V

    .line 194
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 15
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->isDeviceClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 322
    :pswitch_0
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v3, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 327
    :pswitch_1
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->requestPending:Z

    if-eqz v2, :cond_2

    .line 329
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p71:Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/pages/P71_CommandStatus;->encodePage([B)V

    .line 330
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 332
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsToSend:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsToSend:I

    if-nez v2, :cond_0

    .line 333
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->requestPending:Z

    goto :goto_0

    .line 337
    :cond_2
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    .line 338
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    const/16 v3, 0x41

    if-ne v2, v3, :cond_3

    .line 340
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p80:Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/pages/P80_ManufacturerIdentificationEncoder;->encodePage([B)V

    .line 341
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto :goto_0

    .line 345
    :cond_3
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    const/16 v3, 0x82

    if-ne v2, v3, :cond_4

    .line 347
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->p81:Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/pages/P81_ProductInformationEncoder;->encodePage([B)V

    .line 348
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 349
    const/4 v2, 0x0

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    goto :goto_0

    .line 353
    :cond_4
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgTxCounter:I

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    rem-int v8, v2, v3

    .line 354
    .local v8, "modulo":I
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pagesToSend:Ljava/util/ArrayList;

    div-int/lit8 v3, v8, 0x2

    int-to-double v13, v3

    invoke-static {v13, v14}, Ljava/lang/Math;->floor(D)D

    move-result-wide v13

    double-to-int v3, v13

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;

    .line 355
    .local v9, "page":Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-interface {v9, v2}, Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;->encodePage([B)V

    .line 356
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->txBuffer:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    goto/16 :goto_0

    .line 368
    .end local v8    # "modulo":I
    .end local v9    # "page":Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;
    :pswitch_2
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsReceivedCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsReceivedCounter:I

    .line 370
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v10

    .line 372
    .local v10, "pageNum":I
    const/16 v2, 0x46

    if-ne v10, v2, :cond_7

    .line 375
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v2, v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v12

    .line 376
    .local v12, "requestedResponse":I
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 377
    .local v11, "requestedPageNumber":I
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 378
    .local v7, "commandType":I
    const/16 v2, 0x47

    if-ne v11, v2, :cond_6

    const/4 v2, 0x1

    if-ne v7, v2, :cond_6

    .line 380
    iput v12, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsToSend:I

    .line 381
    if-nez v12, :cond_5

    .line 383
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    const-string v3, "Invalid number of times to transmit page requested. Will send once."

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const/4 v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->msgsToSend:I

    .line 386
    :cond_5
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->requestPending:Z

    goto/16 :goto_0

    .line 390
    :cond_6
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported data page request for page "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " received"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 395
    .end local v7    # "commandType":I
    .end local v11    # "requestedPageNumber":I
    .end local v12    # "requestedResponse":I
    :cond_7
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->pageIndex:Landroid/util/SparseArray;

    invoke-virtual {v2, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;

    .line 396
    .local v1, "p":Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
    if-eqz v1, :cond_8

    .line 399
    const-wide/16 v4, 0x0

    .line 400
    .local v4, "eventFlags":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto/16 :goto_0

    .line 403
    .end local v4    # "eventFlags":J
    :cond_8
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown page received, page "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 322
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
