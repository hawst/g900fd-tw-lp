.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
.super Ljava/lang/Object;
.source "AntFsDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AntFsFileEntry"
.end annotation


# instance fields
.field public adjustedTimeStamp:J

.field public dataType:I

.field public dataTypeSpecificFlags:I

.field public fileIndex:I

.field public fileSize:J

.field public generalFlags:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;",
            ">;"
        }
    .end annotation
.end field

.field public identifier:[B

.field public timeStamp:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    return-void
.end method
