.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "AntFsHostDisconnectChannelSubTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field disconnectCommand:[B

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field mLinkPeriod:I

.field mLinkRf:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;II)V
    .locals 1
    .param p1, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "linkRf"    # I
    .param p3, "linkPeriod"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 24
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->disconnectCommand:[B

    .line 41
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkPeriod:I

    .line 42
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkRf:I

    .line 43
    return-void

    .line 24
    nop

    :array_0
    .array-data 1
        0x44t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    .line 86
    .local v0, "channelState":Lcom/dsi/ant/message/ChannelState;
    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v0, v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkPeriod:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 90
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkRf:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 93
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed: Connection lost"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 95
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_DEVICE_TRANSMISSION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 142
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :goto_0
    return-void

    .line 101
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->disconnectCommand:[B

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 119
    :goto_1
    :try_start_2
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 120
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->enableMessageProcessing()V

    .line 121
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 124
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkPeriod:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 125
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mLinkRf:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 127
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->mStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->DISCONNECT_PASS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 128
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 130
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE occurred disconnecting: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2

    .line 103
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    :catch_1
    move-exception v1

    .line 105
    .restart local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_3
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v2, v3, :cond_1

    .line 111
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed to send Link Command: TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 135
    .end local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_2
    move-exception v1

    .line 137
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    const-string v3, "Interrupted waiting for result"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 139
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 115
    .restart local v0    # "channelState":Lcom/dsi/ant/message/ChannelState;
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_1
    :try_start_4
    throw v1
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "ANT-FS Host Disconnect Channel Task"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 153
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-ne p1, v0, :cond_0

    .line 154
    const/4 v0, 0x1

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    return-void

    .line 53
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 58
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->disableMessageProcessing()V

    .line 60
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :pswitch_3
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->disableMessageProcessing()V

    .line 67
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDisconnectChannelSubTask;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 53
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
