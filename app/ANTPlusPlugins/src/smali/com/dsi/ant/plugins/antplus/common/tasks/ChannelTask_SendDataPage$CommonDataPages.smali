.class public Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;
.super Ljava/lang/Object;
.source "ChannelTask_SendDataPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommonDataPages"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCommandBurstDataPage(BBIIJ[B)[B
    .locals 6
    .param p0, "requestedCmdId"    # B
    .param p1, "cmdSequenceNum"    # B
    .param p2, "manufacturerNumber"    # I
    .param p3, "productId"    # I
    .param p4, "serialNumber"    # J
    .param p6, "cmdData"    # [B

    .prologue
    const/4 v5, 0x0

    .line 54
    array-length v3, p6

    add-int/lit8 v2, v3, 0x10

    .line 55
    .local v2, "pageLength":I
    new-array v1, v2, [B

    .line 56
    .local v1, "messagePayload":[B
    const/16 v3, 0x48

    aput-byte v3, v1, v5

    .line 57
    const/4 v3, 0x1

    aput-byte p0, v1, v3

    .line 58
    const/4 v3, 0x2

    aput-byte p1, v1, v3

    .line 59
    const/4 v3, 0x3

    div-int/lit8 v4, v2, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 60
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x7

    if-gt v0, v3, :cond_0

    .line 61
    const/4 v3, -0x1

    aput-byte v3, v1, v0

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_0
    const/16 v3, 0x8

    invoke-static {v1, v3, p2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 63
    const/16 v3, 0xa

    invoke-static {v1, v3, p3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 64
    const/16 v3, 0xc

    invoke-static {v1, v3, p4, p5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 65
    const/16 v3, 0x10

    array-length v4, p6

    invoke-static {p6, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    return-object v1
.end method

.method public static getRequestDataPage(B)[B
    .locals 1
    .param p0, "dataPageNumber"    # B

    .prologue
    .line 49
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(BB)[B

    move-result-object v0

    return-object v0
.end method

.method public static getRequestDataPage(BB)[B
    .locals 5
    .param p0, "dataPageNumber"    # B
    .param p1, "subPageNumber"    # B

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x2

    aput-byte v3, v0, v1

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    const/4 v1, 0x4

    aput-byte v3, v0, v1

    const/4 v1, 0x5

    aput-byte v4, v0, v1

    const/4 v1, 0x6

    aput-byte p0, v0, v1

    const/4 v1, 0x7

    aput-byte v4, v0, v1

    return-object v0
.end method
