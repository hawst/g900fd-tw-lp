.class public Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "BikePowerDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cachedCtfOffset:I

.field private calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

.field private calDbUpdateThread:Landroid/os/HandlerThread;

.field private ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

.field private ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

.field private decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

.field private handler:Landroid/os/Handler;

.field private isManualCalibrationRequested:Z

.field private mCommandBurstSequenceNumber:S

.field private mDeviceNumber:I

.field private mP16_PowerOnly:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;

.field private mP17_WheelTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;

.field private mP18_CrankTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P18_CrankTorqueData;

.field private mP19_TeAndPsData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;

.field private mP1_CalibrationData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;

.field private mP2_BikeParameters:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;

.field private mP32_CtfData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;

.field private mP3_MeasurementOutputData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;

.field private powerOnlyEvtCntAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private final resetCtfOffsetBuffer:Ljava/lang/Runnable;

.field private final saveLiveCtfOffset:Ljava/lang/Runnable;

.field private savedAutoCtfOffset:Z

.field private serialNumber:J

.field private final unflagManualCalibration:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/content/Context;)V
    .locals 5
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "serviceContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 61
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->INVALID_CTF_CAL_REQ:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getIntValue()I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I

    .line 66
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "calDb update thread"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDbUpdateThread:Landroid/os/HandlerThread;

    .line 68
    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;-><init>(I)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    .line 69
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->savedAutoCtfOffset:Z

    .line 74
    iput-short v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mCommandBurstSequenceNumber:S

    .line 76
    iput-boolean v4, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z

    .line 77
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->saveLiveCtfOffset:Ljava/lang/Runnable;

    .line 98
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$2;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$2;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->unflagManualCalibration:Ljava/lang/Runnable;

    .line 107
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$3;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$3;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->resetCtfOffsetBuffer:Ljava/lang/Runnable;

    .line 123
    invoke-static {p3}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->serialNumber:J

    .line 127
    :try_start_0
    invoke-virtual {p2}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mDeviceNumber:I

    .line 130
    new-instance v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    invoke-direct {v2, p3}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    .line 131
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mDeviceNumber:I

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;->getCalibration(I)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;

    move-result-object v0

    .line 133
    .local v0, "calInfo":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    if-eqz v0, :cond_0

    .line 134
    iget v2, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;->calibrationValue:I

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I

    .line 137
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDbUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 138
    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDbUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    .end local v0    # "calInfo":Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase$CalibrationInfo;
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException during initizalization: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 146
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AntCommandFailedException during initizalization: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z

    return v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z

    return p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->savedAutoCtfOffset:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->resetCtfOffsetBuffer:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->savedAutoCtfOffset:Z

    return p1
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mDeviceNumber:I

    return v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I

    return v0
.end method

.method static synthetic access$302(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I

    return p1
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->calDb:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerCalDatabase;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetBuffer:Lcom/dsi/ant/plugins/antplus/common/pages/FifoIntBuffer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->saveLiveCtfOffset:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->unflagManualCalibration:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 20
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->token_ClientMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 223
    .local v10, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v15

    .line 224
    .local v15, "response":Landroid/os/Message;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    .line 225
    .local v13, "params":Landroid/os/Bundle;
    move-object/from16 v0, p2

    iget v3, v0, Landroid/os/Message;->what:I

    iput v3, v15, Landroid/os/Message;->what:I

    .line 226
    const/16 v18, 0xda

    .line 227
    .local v18, "whatReqEvt":I
    const-string v19, "int_requestStatus"

    .line 230
    .local v19, "whatReqStatus":Ljava/lang/String;
    move-object/from16 v0, p2

    iget v3, v0, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 438
    invoke-virtual {v15}, Landroid/os/Message;->recycle()V

    .line 439
    const/4 v15, 0x0

    .line 440
    invoke-super/range {p0 .. p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 233
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->unflagManualCalibration:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->unflagManualCalibration:Ljava/lang/Runnable;

    const-wide/16 v5, 0x2710

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 235
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->isManualCalibrationRequested:Z

    .line 237
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 238
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 240
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getManualCalibrationRequestDataPage()[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 244
    :sswitch_1
    const-string v3, "arrayByte_manufacturerSpecificParameters"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v17

    .line 247
    .local v17, "specificParams":[B
    if-eqz v17, :cond_1

    move-object/from16 v0, v17

    array-length v3, v0

    const/4 v4, 0x6

    if-eq v3, v4, :cond_2

    .line 249
    :cond_1
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 250
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestCustomCalibrationParameters failed to start because the manufacturer specific parameters must be 6 bytes long."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto :goto_0

    .line 255
    :cond_2
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 256
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 258
    invoke-static/range {v17 .. v17}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getRequestCustomCalParamsDataPage([B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 263
    .end local v17    # "specificParams":[B
    :sswitch_2
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 264
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 266
    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getRequestDataPage(BB)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto :goto_0

    .line 270
    :sswitch_3
    const-string v3, "arrayByte_manufacturerSpecificParameters"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v17

    .line 273
    .restart local v17    # "specificParams":[B
    if-eqz v17, :cond_3

    move-object/from16 v0, v17

    array-length v3, v0

    const/4 v4, 0x6

    if-eq v3, v4, :cond_4

    .line 275
    :cond_3
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 276
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCustomCalibrationParameters failed to start because the manufacturer specific parameters must be 6 bytes long."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 281
    :cond_4
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 282
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 284
    invoke-static/range {v17 .. v17}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getSetCustomCalParamDataPage([B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 289
    .end local v17    # "specificParams":[B
    :sswitch_4
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 290
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 292
    const-string v3, "bool_autoZeroEnable"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getAutoZeroConfigDataPage(Z)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 297
    :sswitch_5
    const-string v3, "int_crankLengthSetting"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    move-result-object v11

    .line 300
    .local v11, "crankLengthSetting":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;
    const-string v3, "decimal_fullCrankLength"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v12

    check-cast v12, Ljava/math/BigDecimal;

    .line 303
    .local v12, "fullCrankLength":Ljava/math/BigDecimal;
    if-nez v11, :cond_5

    .line 305
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 306
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCrankParameters failed to start because crankLengthSetting was null."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 311
    :cond_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->MANUAL_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    if-ne v11, v3, :cond_6

    .line 313
    if-nez v12, :cond_6

    .line 315
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 316
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCrankParameters failed to start because fullCrankLength was null."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 322
    :cond_6
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$5;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusBikePowerPcc$CrankLengthSetting:[I

    invoke-virtual {v11}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_0

    .line 325
    :pswitch_0
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 326
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 328
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->AUTO_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->getIntValue()I

    move-result v3

    int-to-byte v3, v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getSetCrankParamsDataPage(B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 332
    :pswitch_1
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "236.5"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 334
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 335
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCrankParameters failed to start because fullCrankLength exceeded 236.5mm."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 338
    :cond_7
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "110"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_8

    .line 340
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 341
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCrankParameters failed to start because fullCrankLength is below 110.0mm."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 346
    :cond_8
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 347
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 349
    const-string v3, "decimal_fullCrankLength"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "110"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "2"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->byteValue()B

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getSetCrankParamsDataPage(B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 356
    :pswitch_2
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 357
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 359
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->getIntValue()I

    move-result v3

    int-to-byte v3, v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getSetCrankParamsDataPage(B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 368
    .end local v11    # "crankLengthSetting":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;
    .end local v12    # "fullCrankLength":Ljava/math/BigDecimal;
    :sswitch_6
    const-string v3, "decimal_slope"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v16

    check-cast v16, Ljava/math/BigDecimal;

    .line 370
    .local v16, "slope":Ljava/math/BigDecimal;
    if-nez v16, :cond_9

    .line 372
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 373
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCtfSlope failed to start because slope was null."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 378
    :cond_9
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "50"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    .line 380
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 381
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCtfSlope failed to start because slope exceeded 50.0Nm/Hz."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 385
    :cond_a
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "10"

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_b

    .line 387
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 388
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestSetCtfSlope failed to start because slope is below 10.0Nm/Hz."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 394
    :cond_b
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 395
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 397
    invoke-static/range {v16 .. v16}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$AntPlusBikePowerDataPages;->getSaveSlopeDataPage(Ljava/math/BigDecimal;)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 402
    .end local v16    # "slope":Ljava/math/BigDecimal;
    :sswitch_7
    const-string v3, "int_requestedCommandId"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 403
    .local v14, "requestedCommandId":I
    const-string v3, "arrayByte_commandData"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v9

    .line 405
    .local v9, "commandData":[B
    if-ltz v14, :cond_c

    const/16 v3, 0xff

    if-le v14, v3, :cond_d

    .line 407
    :cond_c
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 408
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd commandBurst failed to start because Command ID is outside of 0-255 range."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 414
    :cond_d
    if-eqz v9, :cond_e

    array-length v3, v9

    if-eqz v3, :cond_e

    array-length v3, v9

    rem-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_f

    .line 416
    :cond_e
    const/4 v3, -0x3

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 417
    sget-object v3, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->TAG:Ljava/lang/String;

    const-string v4, "Cmd commandBurst failed to start because Command Data length must be a non-zero multiple of 8 bytes."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 424
    :cond_f
    const/4 v3, 0x0

    iput v3, v15, Landroid/os/Message;->arg1:I

    .line 425
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v15}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 427
    int-to-byte v3, v14

    move-object/from16 v0, p0

    iget-short v4, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mCommandBurstSequenceNumber:S

    add-int/lit8 v5, v4, 0x1

    int-to-short v5, v5

    move-object/from16 v0, p0

    iput-short v5, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mCommandBurstSequenceNumber:S

    int-to-byte v4, v4

    const/16 v5, 0xf

    const v6, 0xfffc

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->serialNumber:J

    invoke-static/range {v3 .. v9}, Lcom/dsi/ant/plugins/antplus/common/tasks/ChannelTask_SendDataPage$CommonDataPages;->getCommandBurstDataPage(BBIIJ[B)[B

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v10, v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->sendDataPage([BLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;ILjava/lang/String;)V

    .line 433
    move-object/from16 v0, p0

    iget-short v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mCommandBurstSequenceNumber:S

    const/16 v4, 0xff

    if-le v3, v4, :cond_0

    .line 434
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-short v3, v0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mCommandBurstSequenceNumber:S

    goto/16 :goto_0

    .line 230
    nop

    :sswitch_data_0
    .sparse-switch
        0x68 -> :sswitch_7
        0x4e21 -> :sswitch_0
        0x4e22 -> :sswitch_4
        0x4e23 -> :sswitch_6
        0x4e24 -> :sswitch_1
        0x4e25 -> :sswitch_3
        0x4e26 -> :sswitch_2
        0x4e27 -> :sswitch_5
    .end sparse-switch

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 454
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 455
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 456
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->closeDevice()V

    .line 457
    return-void
.end method

.method public getCtfOffset()I
    .locals 1

    .prologue
    .line 447
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->cachedCtfOffset:I

    return v0
.end method

.method public getPageList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice$4;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

    .line 190
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    .line 193
    new-instance v1, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    const/16 v2, 0xff

    const/16 v3, 0x2ee0

    invoke-direct {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;-><init>(II)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->powerOnlyEvtCntAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 196
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->ctfOffsetReceiver:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData$ICtfOffsetReceiver;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP1_CalibrationData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;

    .line 197
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->powerOnlyEvtCntAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;-><init>(Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP16_PowerOnly:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;

    .line 198
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP17_WheelTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;

    .line 199
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P18_CrankTorqueData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P18_CrankTorqueData;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP18_CrankTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P18_CrankTorqueData;

    .line 200
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->powerOnlyEvtCntAcc:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;-><init>(Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP19_TeAndPsData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;

    .line 201
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->decoder:Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;

    invoke-direct {v1, v2}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;-><init>(Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDecoder;)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP32_CtfData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;

    .line 202
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP3_MeasurementOutputData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;

    .line 203
    new-instance v1, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP2_BikeParameters:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP1_CalibrationData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P1_CalibrationData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP16_PowerOnly:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P16_PowerOnly;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP17_WheelTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P17_WheelTorqueData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP18_CrankTorqueData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P18_CrankTorqueData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP19_TeAndPsData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP32_CtfData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P32_CtfData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP3_MeasurementOutputData:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P3_MeasurementOutputData;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->mP2_BikeParameters:Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/bikepower/BikePowerDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 216
    return-object v0
.end method
