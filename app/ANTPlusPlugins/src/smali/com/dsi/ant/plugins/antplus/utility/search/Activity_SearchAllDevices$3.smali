.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private promptToSaveDevice(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Ljava/lang/String;)V
    .locals 11
    .param p1, "selectedDevInfo"    # Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    .param p2, "devName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 413
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 414
    .local v9, "li":Landroid/view/LayoutInflater;
    const v1, 0x7f030007

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 415
    .local v8, "editView":Landroid/view/View;
    const v1, 0x7f060017

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 416
    .local v3, "savedName":Landroid/widget/EditText;
    const v1, 0x7f060018

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 417
    .local v4, "isPreferredCheck":Landroid/widget/CheckBox;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {v6, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 418
    .local v6, "adlgBldr":Landroid/app/AlertDialog$Builder;
    const-string v1, "New device selected, save info?"

    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 419
    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 420
    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 421
    const-string v1, "Save "

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$2;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;)V

    invoke-virtual {v6, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 429
    const-string v1, "Don\'t Save"

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$3;

    invoke-direct {v2, p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$3;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Ljava/lang/String;)V

    invoke-virtual {v6, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 440
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 443
    .local v5, "dlg":Landroid/app/AlertDialog;
    new-instance v7, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {v7, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 446
    .local v7, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mPluginName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->preferredExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 453
    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 456
    const/4 v0, -0x1

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v10

    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$4;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/app/AlertDialog;)V

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    return-void

    .line 450
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 17
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 332
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Search All, device selected, cancelling scan"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mSavedSearchResults:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;

    .line 334
    .local v14, "selectedDevInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->cancellingLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$500(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)Ljava/lang/Object;

    move-result-object v16

    monitor-enter v16

    .line 336
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-boolean v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mIsUserCancelled:Z

    if-eqz v2, :cond_0

    .line 337
    monitor-exit v16

    .line 408
    :goto_0
    return-void

    .line 339
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    iget v3, v14, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->parentSearch_listIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 343
    .local v13, "parentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isSingleSearchInProgress:Z

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$600(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 350
    .local v9, "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 353
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->mLatch_DevConnected:Ljava/util/concurrent/CountDownLatch;

    .line 354
    iget-object v2, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 355
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    iget-object v2, v14, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, -0x20000001

    and-int/2addr v2, v3

    iget v3, v13, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->rfFreq:I

    iget v4, v13, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->period:I

    iget v5, v13, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->devType:I

    iget v6, v13, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->transType:I

    const/4 v7, 0x0

    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v9}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;)V

    invoke-direct/range {v1 .. v8}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;-><init>(IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 383
    .local v1, "singleSearch":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    :try_start_3
    iget-object v2, v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x7d0

    invoke-virtual {v2, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v15

    .line 390
    .local v15, "singleSearchStarted":Ljava/lang/Boolean;
    :goto_2
    :try_start_4
    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isSingleSearchInProgress:Z

    .line 393
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error trying to start single search task"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 395
    .local v12, "failMsg":Landroid/os/Message;
    const/16 v2, -0x64

    iput v2, v12, Landroid/os/Message;->what:I

    .line 396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->sendReply(Landroid/os/Message;)V
    invoke-static {v2, v12}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;Landroid/os/Message;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->finish()V

    .line 398
    monitor-exit v16

    goto/16 :goto_0

    .line 400
    .end local v1    # "singleSearch":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .end local v9    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v12    # "failMsg":Landroid/os/Message;
    .end local v13    # "parentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v15    # "singleSearchStarted":Ljava/lang/Boolean;
    :catchall_0
    move-exception v2

    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 349
    .restart local v13    # "parentSearch":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_1
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$600(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .restart local v9    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    goto/16 :goto_1

    .line 350
    .end local v9    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v2

    .line 384
    .restart local v1    # "singleSearch":Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;
    .restart local v9    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :catch_0
    move-exception v11

    .line 386
    .local v11, "e":Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    .line 387
    .restart local v15    # "singleSearchStarted":Ljava/lang/Boolean;
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InterruptedException trying to start single search task"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 400
    .end local v11    # "e":Ljava/lang/InterruptedException;
    :cond_2
    monitor-exit v16
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 403
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->adapter_devNameList:Landroid/widget/ArrayAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 404
    .local v10, "devName":Ljava/lang/String;
    iget-object v2, v14, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->device_dbId:Ljava/lang/Long;

    if-nez v2, :cond_3

    .line 405
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v10}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->promptToSaveDevice(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v3, v14, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchResultInfo;->devInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v10}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->waitForNewThenFinish(ILjava/lang/String;)V

    goto/16 :goto_0
.end method
