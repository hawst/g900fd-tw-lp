.class Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;
.super Ljava/lang/Object;
.source "AntChannelExecutor.java"

# interfaces
.implements Lcom/dsi/ant/channel/IAntChannelEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->initExecutorChannel(Lcom/dsi/ant/channel/AntChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChannelDeath()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const-string v1, "AntChannel fired OnChannelDeath()"

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$000(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 5
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 64
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$200(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 69
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->currentTask:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$100(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;

    move-result-object v0

    .line 70
    .local v0, "dest":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    if-eqz v0, :cond_0

    .line 76
    :try_start_1
    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->processMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 84
    .end local v0    # "dest":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :cond_0
    :goto_0
    return-void

    .line 70
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 77
    .restart local v0    # "dest":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " handleMessage()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->access$000(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Ljava/lang/String;)V

    goto :goto_0
.end method
