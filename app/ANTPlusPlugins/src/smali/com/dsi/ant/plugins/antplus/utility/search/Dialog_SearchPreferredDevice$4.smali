.class Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;
.super Ljava/lang/Object;
.source "Dialog_SearchPreferredDevice.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->initExecutorAndSearchTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResult(ILcom/dsi/ant/message/ChannelId;Ljava/lang/Integer;)V
    .locals 7
    .param p1, "result"    # I
    .param p2, "dev_channelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p3, "rssi"    # Ljava/lang/Integer;

    .prologue
    const/16 v6, -0x64

    const/4 v5, 0x0

    .line 189
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchTask:Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/SingleSearchControllerTask;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 190
    sparse-switch p1, :sswitch_data_0

    .line 260
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Search reported failure: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 262
    .local v0, "failMsg":Landroid/os/Message;
    iput v6, v0, Landroid/os/Message;->what:I

    .line 263
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 264
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V
    invoke-static {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;Landroid/os/Message;)V

    .line 265
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    .line 268
    .end local v0    # "failMsg":Landroid/os/Message;
    :goto_0
    return-void

    .line 194
    :sswitch_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->searchExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v3, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 197
    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Could not detach executor cleanly before returning connected channel"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 199
    .restart local v0    # "failMsg":Landroid/os/Message;
    iput v6, v0, Landroid/os/Message;->what:I

    .line 200
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V
    invoke-static {v3, v0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;Landroid/os/Message;)V

    .line 201
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    goto :goto_0

    .line 206
    .end local v0    # "failMsg":Landroid/os/Message;
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 207
    .local v2, "respToPlugin":Landroid/os/Message;
    iput v5, v2, Landroid/os/Message;->what:I

    .line 208
    iput v5, v2, Landroid/os/Message;->arg1:I

    .line 209
    invoke-virtual {p2}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->arg2:I

    .line 210
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 211
    .local v1, "respData":Landroid/os/Bundle;
    const-string v3, "str_SelectedDeviceName"

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 214
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->sendReply(Landroid/os/Message;)V
    invoke-static {v3, v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;Landroid/os/Message;)V

    .line 215
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->finish()V

    goto :goto_0

    .line 220
    .end local v1    # "respData":Landroid/os/Bundle;
    .end local v2    # "respToPlugin":Landroid/os/Message;
    :sswitch_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;

    new-instance v4, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4$1;

    invoke-direct {v4, p0}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice$4;)V

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Dialog_SearchPreferredDevice;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 190
    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
