.class public Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P2_DeviceAvailability.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/controls/IEncodedDataPage;


# static fields
.field public static final DATA_PAGE_NUMBER:I = 0x2


# instance fields
.field private availEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xe9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->availEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 29
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    .line 32
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 49
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v5, 0x2

    aget-byte v1, v1, v5

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setMaximumNumberRemotesConnected(Z)V

    .line 51
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v6

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-lez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setAudioControlSupported(Z)V

    .line 53
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v6

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-lez v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setGenericControlSupported(Z)V

    .line 55
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    aget-byte v1, v1, v6

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    and-int/lit8 v1, v1, 0x20

    if-lez v1, :cond_4

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setVideoControlSupported(Z)V

    .line 57
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v4

    aget-byte v4, v4, v6

    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    and-int/lit8 v4, v4, 0x40

    if-lez v4, :cond_5

    :goto_4
    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setBurstCommandSupported(Z)V

    .line 60
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->availEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 64
    const-string v1, "long_EventFlags"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 65
    const-string v1, "parcelable_ControlDeviceAvailabilities"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->availEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 71
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 49
    goto :goto_0

    :cond_2
    move v1, v3

    .line 51
    goto :goto_1

    :cond_3
    move v1, v3

    .line 53
    goto :goto_2

    :cond_4
    move v1, v3

    .line 55
    goto :goto_3

    :cond_5
    move v2, v3

    .line 57
    goto :goto_4
.end method

.method public encodePage([B)V
    .locals 4
    .param p1, "txBuffer"    # [B

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x7

    const/4 v1, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->getPageNumbers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 77
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getMaximumNumberRemotesConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    aget-byte v0, p1, v3

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 82
    :goto_0
    const/4 v0, 0x2

    aput-byte v1, p1, v0

    .line 83
    const/4 v0, 0x3

    aput-byte v1, p1, v0

    .line 84
    const/4 v0, 0x4

    aput-byte v1, p1, v0

    .line 85
    const/4 v0, 0x5

    aput-byte v1, p1, v0

    .line 86
    const/4 v0, 0x6

    aput-byte v1, p1, v0

    .line 88
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    aget-byte v0, p1, v2

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 93
    :goto_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    aget-byte v0, p1, v2

    or-int/lit8 v0, v0, 0x10

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 98
    :goto_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    aget-byte v0, p1, v2

    or-int/lit8 v0, v0, 0x20

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 102
    :goto_3
    return-void

    .line 80
    :cond_0
    aget-byte v0, p1, v3

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    goto :goto_0

    .line 91
    :cond_1
    aget-byte v0, p1, v2

    and-int/lit16 v0, v0, 0xfe

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    goto :goto_1

    .line 96
    :cond_2
    aget-byte v0, p1, v2

    and-int/lit16 v0, v0, 0xef

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    goto :goto_2

    .line 101
    :cond_3
    aget-byte v0, p1, v2

    and-int/lit16 v0, v0, 0xdf

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    goto :goto_3
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->availEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
