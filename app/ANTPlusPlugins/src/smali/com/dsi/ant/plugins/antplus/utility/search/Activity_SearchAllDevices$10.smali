.class Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;
.super Ljava/lang/Object;
.source "Activity_SearchAllDevices.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->waitForNewThenFinish(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)V
    .locals 0

    .prologue
    .line 888
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 893
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 896
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 899
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$600(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 900
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .line 904
    .local v0, "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isSingleSearchInProgress:Z

    .line 905
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-boolean v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->isScanStopTimeoutTriggered:Z

    if-nez v2, :cond_0

    .line 907
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->scanSearchTask:Lcom/dsi/ant/plugins/antplus/utility/search/ScanSearchControllerTask;

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 910
    :try_start_1
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v4, 0x7d0

    invoke-virtual {v2, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->cancelCurrentTask(I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 916
    :cond_0
    :goto_1
    :try_start_2
    monitor-exit v3

    .line 917
    return-void

    .line 902
    .end local v0    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->searchList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$10;->this$0:Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->consecutiveModeActiveSearch_listIndex:I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;->access$600(Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;

    .restart local v0    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    goto :goto_0

    .line 911
    :catch_0
    move-exception v1

    .line 913
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 916
    .end local v0    # "channelOneSearchInfo":Lcom/dsi/ant/plugins/antplus/utility/search/Activity_SearchAllDevices$SearchInfo;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
