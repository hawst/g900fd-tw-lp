.class public Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;
.super Ljava/lang/Object;
.source "DeviceList.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public deviceDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field deviceIdsList:[I

.field identifierStringList:[Ljava/lang/String;

.field instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private final listLock:Ljava/lang/Object;

.field updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V
    .locals 2
    .param p1, "updateDeviceListEvent"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    .line 18
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    .line 28
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 29
    return-void
.end method

.method private findDeviceIndex(I)I
    .locals 3
    .param p1, "deviceID"    # I

    .prologue
    .line 53
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v2

    .line 55
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 58
    monitor-exit v2

    .line 61
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 55
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, -0x1

    monitor-exit v2

    goto :goto_1

    .line 62
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private makeCurrentEventBundle(II)Landroid/os/Bundle;
    .locals 3
    .param p1, "changeCode"    # I
    .param p2, "changingDeviceId"    # I

    .prologue
    .line 43
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "arrayInt_deviceIDs"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 45
    const-string v1, "arrayString_deviceIdentifierStrings"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 46
    const-string v1, "int_changeCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 47
    const-string v1, "int_changingDeviceID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    return-object v0
.end method


# virtual methods
.method public addDeviceToList(Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;)V
    .locals 8
    .param p1, "devToAdd"    # Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .prologue
    .line 74
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v5

    .line 77
    :try_start_0
    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    move-result-object v0

    .line 78
    .local v0, "existingDev":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    if-eqz v0, :cond_1

    .line 80
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->TAG:Ljava/lang/String;

    const-string v6, "Adding device to list that already exists"

    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v4, v0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    iget-object v6, p1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 82
    sget-object v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->TAG:Ljava/lang/String;

    const-string v6, "Existing device has mismatched identifier string"

    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    monitor-exit v5

    .line 102
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    array-length v3, v4

    .line 87
    .local v3, "oldLength":I
    add-int/lit8 v4, v3, 0x1

    new-array v1, v4, [I

    .line 88
    .local v1, "newDeviceIdsList":[I
    add-int/lit8 v4, v3, 0x1

    new-array v2, v4, [Ljava/lang/String;

    .line 90
    .local v2, "newIdentifierStringList":[Ljava/lang/String;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v6, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    aput v4, v1, v3

    .line 92
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    .line 94
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v6, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 96
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    .line 98
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->ADDED_TO_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getIntValue()I

    move-result v6

    iget-object v7, p1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v7, v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-direct {p0, v6, v7}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->makeCurrentEventBundle(II)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 102
    monitor-exit v5

    goto :goto_0

    .line 103
    .end local v0    # "existingDev":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    .end local v1    # "newDeviceIdsList":[I
    .end local v2    # "newIdentifierStringList":[Ljava/lang/String;
    .end local v3    # "oldLength":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    .locals 3
    .param p1, "deviceID"    # I

    .prologue
    .line 139
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v2

    .line 141
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->findDeviceIndex(I)I

    move-result v0

    .line 142
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 143
    const/4 v1, 0x0

    monitor-exit v2

    .line 145
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    monitor-exit v2

    goto :goto_0

    .line 146
    .end local v0    # "index":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeDeviceFromList(I)Z
    .locals 8
    .param p1, "deviceID"    # I

    .prologue
    const/4 v4, 0x0

    .line 108
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v5

    .line 110
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->findDeviceIndex(I)I

    move-result v0

    .line 111
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 113
    sget-object v6, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->TAG:Ljava/lang/String;

    const-string v7, "Removing device that doesn\'t exist"

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    monitor-exit v5

    .line 134
    :goto_0
    return v4

    .line 117
    :cond_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    array-length v3, v4

    .line 118
    .local v3, "oldLength":I
    add-int/lit8 v4, v3, -0x1

    new-array v1, v4, [I

    .line 119
    .local v1, "newDeviceIdsList":[I
    add-int/lit8 v4, v3, -0x1

    new-array v2, v4, [Ljava/lang/String;

    .line 121
    .local v2, "newIdentifierStringList":[Ljava/lang/String;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v6, v1, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    add-int/lit8 v6, v0, 0x1

    add-int/lit8 v7, v0, 0x1

    sub-int v7, v3, v7

    invoke-static {v4, v6, v1, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceIdsList:[I

    .line 125
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v6, v2, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    add-int/lit8 v6, v0, 0x1

    add-int/lit8 v7, v0, 0x1

    sub-int v7, v3, v7

    invoke-static {v4, v6, v2, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    .line 129
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 131
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->REMOVED_FROM_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getIntValue()I

    move-result v6

    invoke-direct {p0, v6, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->makeCurrentEventBundle(II)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 132
    monitor-exit v5

    .line 134
    const/4 v4, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "index":I
    .end local v1    # "newDeviceIdsList":[I
    .end local v2    # "newIdentifierStringList":[Ljava/lang/String;
    .end local v3    # "oldLength":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public reportDeviceProgrammed(I)V
    .locals 4
    .param p1, "deviceID"    # I

    .prologue
    .line 151
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v2

    .line 153
    :try_start_0
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->findDeviceIndex(I)I

    move-result v0

    .line 154
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 155
    monitor-exit v2

    .line 161
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->identifierStringList:[Ljava/lang/String;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    aput-object v1, v3, v0

    .line 159
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->updateDeviceListEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->PROGRAMMED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getIntValue()I

    move-result v3

    invoke-direct {p0, v3, p1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->makeCurrentEventBundle(II)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 160
    monitor-exit v2

    goto :goto_0

    .end local v0    # "index":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestInstantUpdate(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 4
    .param p1, "client"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    .line 33
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->listLock:Ljava/lang/Object;

    monitor-enter v1

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v3, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v2, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 36
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->NO_CHANGE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getIntValue()I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->makeCurrentEventBundle(II)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 37
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->instantUpdateEvent:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v2, p1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->unsubscribeFromEvent(Ljava/util/UUID;)Z

    .line 38
    monitor-exit v1

    .line 39
    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
