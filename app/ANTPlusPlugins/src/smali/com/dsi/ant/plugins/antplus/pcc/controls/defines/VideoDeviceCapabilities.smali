.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;
.super Ljava/lang/Object;
.source "VideoDeviceCapabilities.java"


# instance fields
.field public videoPlaybackSupport:Z

.field public videoRecorderSupport:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoPlaybackSupport:Z

    .line 17
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoRecorderSupport:Z

    return-void
.end method

.method public static readFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;
    .locals 2
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;-><init>()V

    .line 27
    .local v0, "c":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;
    const-string v1, "boolVideoPlaybackSupported"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoPlaybackSupport:Z

    .line 28
    const-string v1, "boolVideoRecorderSupported"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoRecorderSupport:Z

    .line 29
    return-object v0
.end method


# virtual methods
.method public writeToBundle(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 38
    const-string v0, "boolVideoPlaybackSupported"

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoPlaybackSupport:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 39
    const-string v0, "boolVideoRecorderSupported"

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->videoRecorderSupport:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    return-void
.end method
