.class public abstract Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;
.super Ljava/lang/Object;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CalculatedTrainerSpeedReceiver"
.end annotation


# instance fields
.field wheelCircumference:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>(Ljava/math/BigDecimal;)V
    .locals 0
    .param p1, "wheelCircumference"    # Ljava/math/BigDecimal;

    .prologue
    .line 2270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2271
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->wheelCircumference:Ljava/math/BigDecimal;

    .line 2272
    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/util/EnumSet;
    .param p4, "x3"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .param p5, "x4"    # Ljava/math/BigDecimal;

    .prologue
    .line 2261
    invoke-direct/range {p0 .. p5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->onNewRawCalculatedTrainerSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    return-void
.end method

.method private onNewRawCalculatedTrainerSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    .locals 6
    .param p1, "estTimestamp"    # J
    .param p4, "dataSource"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;
    .param p5, "rawCalculatedTrainerSpeed"    # Ljava/math/BigDecimal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2292
    .local p3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->wheelCircumference:Ljava/math/BigDecimal;

    invoke-virtual {p5, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalculatedTrainerSpeedReceiver;->onNewCalculatedTrainerSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V

    .line 2293
    return-void
.end method


# virtual methods
.method public abstract onNewCalculatedTrainerSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;Ljava/math/BigDecimal;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TrainerDataSource;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation
.end method
