.class public Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "HeartRateService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    return-void
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 2
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 37
    :try_start_0
    new-instance v1, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;

    invoke-direct {v1, p2, p1}, Lcom/dsi/ant/plugins/antplus/heartrate/HeartRateDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 22
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v1, "str_PluginName"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->HEARTRATE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v1, "predefinednetwork_NetKey"

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 24
    const-string v1, "int_DevType"

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->HEARTRATE:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 25
    const-string v1, "int_TransType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 26
    const-string v1, "int_Period"

    const/16 v2, 0x1f86

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 27
    const-string v1, "int_RfFreq"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    return-object v0
.end method
